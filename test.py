# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 11:42:19 2021
A test scripts trying out new things.
@author: RhysPoolman
"""
import numpy as np
import matplotlib.pyplot as plt
import cv2

# loads png
path = "R:\\InputImages\\"
filename = "Pattern_GridHex_Green.png"
test_img = np.array(cv2.imread(path+filename))
test_img_max = np.max(test_img)
test_img_min = np.min(test_img)
plt.figure("test")
plt.clf()
plt.imshow(test_img)
