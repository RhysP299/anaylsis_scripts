# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 16:25:09 2022

@author: MohammedChoudhury and RhysPoolman
"""
import os
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as skc


def rotate_image(image, angle):
    """
    Takes an image and use the opencv package to rotate it by the specified
    angle.

    Parameters
    ----------
    image: 2D numpy.array
        The image to be rotated.
    angle: float
        The angle by which the image is to be rotated.
    Returns
    -------
    rotated_image: 2D numpy.array
        The rotated image.
    """
    image_center = tuple(np.array(image.shape) / 2)
    rotation_matrix = cv.getRotationMatrix2D(image_center, angle, 1.0)
    rotated_image = cv.warpAffine(image, rotation_matrix, image.shape[1::-1],
                                  flags=cv.INTER_LINEAR)
    return rotated_image


# Reading files and sorting them in the right order
path = ".\\sensitivity_analysis\\Yaw only\\"
all_files = os.listdir(path)
all_images_names = [file_name for file_name in all_files
                    if file_name.endswith(".png")]
all_images_names.sort(key=lambda k: k.split(".")[0][-1])
all_images = {name.split(".")[0]: cv.imread(path + name)
              for name in all_images_names}

# Set up the SimpleBlobdetector with default parameters.
params = cv.SimpleBlobDetector_Params()
# Define thresholds
params.minThreshold = 0
params.maxThreshold = 255
# Filter by Area.
params.filterByArea = True
params.minArea = 50
params.maxArea = 900
# Filter by Color (black=0)
params.filterByColor = False  # set true for cast_iron to detect black regions
params.blobColor = 255
# Filter by Circularity
params.filterByCircularity = True
params.minCircularity = 0.5
params.maxCircularity = 1
# Filter by Convexity
params.filterByConvexity = True
params.minConvexity = 0.5
params.maxConvexity = 1
# Filter by InertiaRatio(elongation)
params.filterByInertia = True
params.minInertiaRatio = 0
params.maxInertiaRatio = 1
# Distance Between Blobs
params.minDistBetweenBlobs = 0
# Setup the detector with parameters
detector = cv.SimpleBlobDetector_create(params)

# get dot coordinates
coordinates = {}
keypoints_dict = {}
angles = {}
rotated_images = {}
for ii, (image_name, rgb_image) in enumerate(all_images.items()):
    gray_image = cv.cvtColor(rgb_image, cv.COLOR_BGR2GRAY)
    th, img1_1 = cv.threshold(gray_image, 100, 255, cv.THRESH_BINARY)
    # rotate images so rows are horizontal if yaw is altered
    if "Tz" in image_name:
        angles[image_name] = -float(image_name.split("Tz(")[1][0])
        rotated_images[image_name] = rotate_image(img1_1, angles[image_name])
        img1_1 = rotated_images[image_name]
    # Detect blobs
    keypoints = detector.detect(img1_1)
    keypoints_dict[image_name] = keypoints
    # order coordinates from top left to bottom right
    points = []
    keypoints_to_search = keypoints[:]
    number_of_rows = 27  # should be mostly constant
    coordinate = np.zeros((len(keypoints), 2))
    # find row y values
    values = \
        np.array([[keypoint_to_search.pt[1]] for keypoint_to_search
                  in keypoints_to_search]).reshape(-1, 1)  # (n, 1) shape array
    row_y_position_kmeans = skc.KMeans(n_clusters=number_of_rows).fit(values)
    row_y_positions = row_y_position_kmeans.cluster_centers_[:, 0]
    # sort y values
    row_y_positions = np.sort(row_y_positions)
    # find x values of points within +/-blob_diameter of rows
    max_blob_diameter = np.max([blob.size for blob in keypoints])
    # sort row points by x value
    if "Tz" in image_name:
        image_centre = tuple(np.array(img1_1.shape) / 2)
    kk = 0
    for ii, row_y_position in enumerate(row_y_positions):
        col_x_positions = \
            np.array([keypoint.pt[0] for keypoint in keypoints
                      if ((keypoint.pt[1] - row_y_position) >
                          -max_blob_diameter) and
                      ((keypoint.pt[1] - row_y_position) <
                       max_blob_diameter)])
        col_x_positions = np.sort(col_x_positions)
        # store coordinate values
        if "Tz" in image_name:
            # if yaw is altered rotate back for d to be calculated
            for col_x_position in col_x_positions:
                # apply rotation in opposite direction
                angle = np.deg2rad(angles[image_name])
                coordinate[kk, 0] = \
                    col_x_position*np.cos(angle) - \
                    row_y_position*np.sin(angle)
                coordinate[kk, 1] = \
                    col_x_position*np.sin(angle) + \
                    row_y_position*np.cos(angle)
                print(kk)
                kk += 1
        else:
            # if no yaw  just store
            for col_x_position in col_x_positions:
                coordinate[kk, 0] = col_x_position
                coordinate[kk, 1] = row_y_position
                kk += 1
    # append to output
    coordinates[image_name] = coordinate

# generate keypoints from coordiantes for diagnostic image
coordinate_keypoints_dict = \
    {image_name: None for image_name, coordinate in coordinates.items()}
for image_name, coordinate in coordinates.items():
    coordinate_keypoints_dict[image_name] = \
        [cv.KeyPoint(int(point[0]), int(point[1]), keypoint.size)
         for point, keypoint in zip(coordinate, keypoints_dict[image_name])]

# create image for display
for name, image in all_images.items():
    image_name = name
    img = image
    keypoints = coordinate_keypoints_dict[image_name]
    blobs = cv.drawKeypoints(img, keypoints, 0, (0, 0, 255),
                             cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    number_of_blobs = len(keypoints)
    text = "Circular Blobs:" + str(len(keypoints))
    cv.putText(blobs, text, (10, 100), cv.FONT_HERSHEY_SIMPLEX, 0.8,
               (0, 0, 255), 2)
    for ii, coordinate in enumerate(coordinates[image_name]):
        x = int(coordinate[0])
        y = int(coordinate[1])
        cv.putText(blobs, str(ii), (x, y), cv.FONT_HERSHEY_SIMPLEX, 0.8,
                   (0, 0, 255), 2)
    # display image
    blobs_resized = cv.resize(blobs, (1800, 1000))
    cv.imshow("window1", blobs_resized)
    cv.waitKey(0)

# calculate d
image_names = list(all_images.keys())
max_number_of_dots = 800
d = {image_name: np.zeros((max_number_of_dots, 3))
     for image_name in image_names[1:]}
# d += np.nan
for ii, (image_name, coordinate) in enumerate(coordinates.items()):
    if ii == 0:
        continue
    previous_name = image_names[ii - 1]
    previous_x = coordinates[previous_name][:, 0]
    previous_y = coordinates[previous_name][:, 1]
    for jj, current in enumerate(coordinate):
        current_x = current[0]
        current_y = current[1]
        d_steps = np.sqrt((previous_x - current_x)**2 +
                          (previous_y - current_y)**2)
        index = np.argmin(d_steps)
        d[image_name][jj, 0] = current_x
        d[image_name][jj, 1] = current_y
        d[image_name][jj, 2] = d_steps[index]

# # show images
# for key, image in all_images.items():
#     plt.figure(key)
#     plt.clf()
#     plt.imshow(image)
#     plt.draw()

# d scatter plot
plt.figure("Scatter Plot")
plt.clf()
for image_name, d_step in d.items():
    label = image_name
    x = d_step[:, 0]
    y = d_step[:, 1]
    plt.scatter(x, y, label=label)
plt.legend()
plt.xlim(-140, 2125)
plt.ylim(-100, 3925)
plt.draw()

# d histograms
for ii, (image_name, d_step) in enumerate(d.items()):
    ii += 1
    title = "Histogram of Point Shift Between for "
    title += "{0} and {1}".format(image_names[ii - 1], image_name)
    plt.figure(title)
    plt.clf()
    plt.title(title)
    plt.hist(d_step[~np.isnan(d_step[:, 2]), 2])
    plt.ylabel('frequency')
    plt.xlabel('d class')
    if ii < len(d) - 1:
        plt.draw()
    else:
        plt.show()
