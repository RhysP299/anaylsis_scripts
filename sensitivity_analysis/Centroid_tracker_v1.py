# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 11:24:39 2022

@author: MohammedChoudhury
"""
import os
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import math
import seaborn as sns
import pandas as pd
from statistics import mean
from scipy import stats


# Reading files and sorting them in the right order
all_files = os.listdir(".")
all_images = [file_name for file_name in all_files if file_name.endswith(".png")]
all_images.sort(key=lambda k: k.split(".")[0][-1])
print(all_images) # 


# C:/Users/MohammedChoudhury/Documents/OpenCV/Centroid_tracker/Translation in 1 Axis(X)
# Set up the SimpleBlobdetector with default parameters.
params=cv.SimpleBlobDetector_Params()

# Define thresholds
params.minThreshold = 0
params.maxThreshold = 255


# Filter by Area.
params.filterByArea = True
params.minArea = 50
params.maxArea = 900

# Filter by Color (black=0)
params.filterByColor = False  #Set true for cast_iron as we'll be detecting black regions
params.blobColor = 255

# Filter by Circularity
params.filterByCircularity= True
params.minCircularity= 0.5
params.maxCircularity=1

# Filter by Convexity
params.filterByConvexity = True
params.minConvexity = 0.5
params.maxConvexity = 1

# Filter by InertiaRatio(elongation)
params.filterByInertia = True
params.minInertiaRatio = 0
params.maxInertiaRatio = 1

# Distance Between Blobs
params.minDistBetweenBlobs = 0

# Setup the detector with parameters
detector = cv.SimpleBlobDetector_create(params)
# Initially, no centroid information is available. 
# previous_centroid_x = -1
# previous_centroid_y = -1

# Set arrays
x=[]
y=[]
coordinate=[]

x_2=[]
y_2=[]
coordinate_2=[]

#arrays
blank = np.zeros((1,1))
func_dist=[]
dis_array=np.array([])
coordinate_array=[]



d=[]
xc=[]
distanceByTwoPoints_2=[]
dcoord=[]
 
DIST_THRESHOLD = 30

coordinate_dix=[]
coordinate_diy=[]
coordinate_djx=[]
coordinate_djy=[]
coordinate_di=[]
coordinate_dj=[]
index_min=[]
d2=[]
dsc=[]
#read images
for i, image_name in enumerate(all_images):
    rgb_image = cv.imread(image_name)
    # height, width = rgb_image.shape[:2]
    gray_image = cv.cvtColor(rgb_image, cv.COLOR_BGR2GRAY)
    th, img1_1 = cv.threshold(gray_image, 100, 255, cv.THRESH_BINARY)
    
    # Detect blobs
    keypoints = detector.detect(img1_1)
    coordinate=[]
    for i in range(len(keypoints)):
        x.append(keypoints[i].pt[0]) #i is the index of the blob you want to get the position
        y.append(keypoints[i].pt[1])
        
        # cv.putText(blobs, f"{coordinate[i]}", ((x[i])- 25, (y[i]) - 25),cv.FONT_HERSHEY_SIMPLEX, 0.25, (255, 255, 255), 1)
    coordinate.append(list(zip(x,y))) 
    coordinate=coordinate[0]
    
    # contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)  
    
   
    dcoord=[]
    blankImage = np.zeros_like(rgb_image)
    tcoord=coordinate[450]
    tcoordx=tcoord[0] # get this checked
    tcoordy=tcoord[1]
    if i == 0:
    #blob tracking
        for index,itemi in enumerate(coordinate):
            # for indexj,itemj in enumerate(coordinate_2):
           
           
            distanceByTwoPoints=(math.sqrt((tcoord[0]-itemi[0])**2 + (tcoord[1]-itemi[1])**2))
            dcoord.append([distanceByTwoPoints,tcoordx,tcoordy,itemi[0],itemi[1]])
            dsc.append(distanceByTwoPoints)
            index_min.append(np.argmin(dcoord,axis=0))
            
            d2.append(index_min[index][0])
            d.append(dcoord[d2[index]][0])
            coordinate_dix.append(dcoord[d2[index]][1])
            coordinate_diy.append(dcoord[d2[index]][2])
            coordinate_djx.append(dcoord[d2[index]][3])
            coordinate_djy.append(dcoord[d2[index]][4])
           
        dm=min(dsc)
        di=dsc.index(dm)
            # if 
           
            
            
            # First frame - Assuming that you can find the correct blob accurately in the first frame
                # Here, I am using a simple logic of checking if the blob is close to the centre of the image. 
                # if abs(cY - (height / 2)) < DIST_THRESHOLD: # Check if the blob centre is close to the half the image's height
                   
                  
                    
        coordinate_di.append(list(zip(coordinate_dix,coordinate_diy)))
        coordinate_di=coordinate_di[0] 
        coordinate_dj.append(list(zip(coordinate_djx,coordinate_djy)))
        coordinate_dj=coordinate_dj[0]  
        
        previous_centroid_x = coordinate_djx # Update variables for finding the next blob correctly
        previous_centroid_y = coordinate_djy
    #%%
    else:
        # if abs(cY - previous_centroid_y) < DIST_THRESHOLD:
            
            # Compare with previous centroid y and see if it lies within Distance threshold
        for index,itemi in enumerate(coordinate):
            # for indexj,itemj in enumerate(coordinate_2):
            # dcoord=[]
            distanceByTwoPoints=(math.sqrt((tcoord[0]-itemi[0])**2 + (tcoord[1]-itemi[1])**2))
            dcoord.append([distanceByTwoPoints,previous_centroid_x,previous_centroid_y,itemi[0],itemi[1]])
            index_min.append(np.argmin(dcoord,axis=0))
            
            d2.append(index_min[index][0])
            d.append(dcoord[d2[index]][0])
            coordinate_dix.append(dcoord[d2[index]][1])
            coordinate_diy.append(dcoord[d2[index]][2])
            coordinate_djx.append(dcoord[d2[index]][3])
            coordinate_djy.append(dcoord[d2[index]][4])
            # dcoord=[] 
            # if 
            
            
        # coordinate_di.append(list(zip(coordinate_dix,coordinate_diy)))
        # coordinate_di=coordinate_di[0] 
        # coordinate_dj.append(list(zip(coordinate_djx,coordinate_djy)))
        # coordinate_dj=coordinate_dj[0]  
        
        # previous_centroid_x = coordinate_djx # Update variables for finding the next blob correctly
        # previous_centroid_y = coordinate_djy
        
# plt.hist(d,bins=30) 
# plt.ylabel('frequency')
# plt.xlabel('d class');
# plt.show()      
        
#Tagging method

    # cv2.drawContours(blankImage, [cnt], 0, color, -1)
    #cv.putText(blobs,f"{coordinate[350]}",(int(x[350]) , int(y[350])),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
    #cv.circle(blobs, (int(x[i]) , int(y[i])), 5, (255, 0, 0), -1)
    # cv2.circle(blankImage, (cX, cY), 3, (255, 0, 0), -1)
    # cv2.imwrite("result_" + image_name, blankImage)
