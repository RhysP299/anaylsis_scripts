# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 10:50:06 2022

@author: MohammedChoudhury
"""

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import math
import numpy as np
import seaborn as sns
import pandas as pd
from statistics import mean
from scipy import stats

def distance(kpt1, kpt2):
    #create numpy array with keypoint positions
    arr = np.array([kpt1.pt, kpt2.pt])
    #scale array to mm
    arr = arr*40/1280
    #return distance, calculted by pythagoras
    arr_dis=np.sqrt(np.sum((arr[0]-arr[1])**2))
    print(arr_dis)
    return arr_dis

image=cv.imread('C:/Users/MohammedChoudhury/Documents/OpenCV/adis images/new images/Translation/Translation/Translation in 1 Axis(X)/x(+1mm)y0.png',0)
image2=cv.imread('C:/Users/MohammedChoudhury/Documents/OpenCV/adis images/new images/Translation/Translation/Translation in 1 Axis(X)/x(+2mm)y0.png',0)


th, img1_1 = cv.threshold(image, 100, 255, cv.THRESH_BINARY)
th, img1_2 = cv.threshold(image2, 100, 255, cv.THRESH_BINARY)
# img1_1=cv.bitwise_not(threshed)

# Set up the SimpleBlobdetector with default parameters.
params=cv.SimpleBlobDetector_Params()

# Define thresholds
params.minThreshold = 0
params.maxThreshold = 255


# Filter by Area.
params.filterByArea = True
params.minArea = 50
params.maxArea = 900

# Filter by Color (black=0)
params.filterByColor = False  #Set true for cast_iron as we'll be detecting black regions
params.blobColor = 255

# Filter by Circularity
params.filterByCircularity= True
params.minCircularity= 0.5
params.maxCircularity=1

# Filter by Convexity
params.filterByConvexity = True
params.minConvexity = 0.5
params.maxConvexity = 1

# Filter by InertiaRatio(elongation)
params.filterByInertia = True
params.minInertiaRatio = 0
params.maxInertiaRatio = 1

# Distance Between Blobs
params.minDistBetweenBlobs = 0

# Setup the detector with parameters
detector = cv.SimpleBlobDetector_create(params)

# Set arrays
x=[]
y=[]
coordinate=[]

x_2=[]
y_2=[]
coordinate_2=[]

# Detect blobs
keypoints = detector.detect(img1_1)
keypoints_2 = detector.detect(img1_2)



#arrays
blank = np.zeros((1,1))
func_dist=[]
dis_array=np.array([])
coordinate_array=[]



d=[]
xc=[]
distanceByTwoPoints_2=[]
dcoord=[]

for i in range(len(keypoints)):
    x.append(keypoints[i].pt[0]) #i is the index of the blob you want to get the position
    y.append(keypoints[i].pt[1])
    
    # cv.putText(blobs, f"{coordinate[i]}", ((x[i])- 25, (y[i]) - 25),cv.FONT_HERSHEY_SIMPLEX, 0.25, (255, 255, 255), 1)
coordinate.append(list(zip(x,y))) 
coordinate=coordinate[0]
tcoord=coordinate[450]

#%%

for i in range(len(keypoints_2)):
    x_2.append(keypoints_2[i].pt[0]) #i is the index of the blob you want to get the position
    y_2.append(keypoints_2[i].pt[1])
    
coordinate_2.append(list(zip(x_2,y_2))) 
coordinate_2=coordinate_2[0]

#%%
# Draw blobs
blobs =cv.drawKeypoints(img1_1, keypoints, blank, (0,0,255),cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
blobs_2 =cv.drawKeypoints(img1_2, keypoints_2, blank, (0,0,255),cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)



coordinate_dix=[]
coordinate_diy=[]
coordinate_djx=[]
coordinate_djy=[]
coordinate_di=[]
coordinate_dj=[]
index_min=[]
d2=[]
#calculation of d

  

                         
#%%                         
for index,itemi in enumerate(coordinate):
    for indexj,itemj in enumerate(coordinate_2):
        distanceByTwoPoints=(math.sqrt((itemi[0]-itemj[0])**2 + (itemi[1]-itemj[1])**2))
        dcoord.append([distanceByTwoPoints,itemi[0],itemi[1],itemj[0],itemj[1]])
    index_min.append(np.argmin(dcoord,axis=0))
    
    d2.append(index_min[index][0])
    d.append(dcoord[d2[index]][0])
    coordinate_dix.append(dcoord[d2[index]][1])
    coordinate_diy.append(dcoord[d2[index]][2])
    coordinate_djx.append(dcoord[d2[index]][3])
    coordinate_djy.append(dcoord[d2[index]][4])
  
    # if 
    dcoord=[]


coordinate_di.append(list(zip(coordinate_dix,coordinate_diy)))
coordinate_di=coordinate_di[0] 
coordinate_dj.append(list(zip(coordinate_djx,coordinate_djy)))
coordinate_dj=coordinate_dj[0]  








# descriptive stats
d_mean=mean(d)
d_std=np.std(d)
d_iqr=stats.iqr(d)



number_of_blobs=len(keypoints)
number_of_blobs_2=len(keypoints_2)

text="Circular Blobs:" + str(len(keypoints))
text_2="Circular Blobs:" + str(len(keypoints_2))


cv.putText(blobs,text,(10,100),cv.FONT_HERSHEY_SIMPLEX,0.8,(0,0,255),2)
cv.putText(blobs_2,text_2,(10,100),cv.FONT_HERSHEY_SIMPLEX,0.8,(0,0,255),2)

# for i in range(450):
#     cv.putText(blobs,f"{coordinate[350]}",(int(x[350]) , int(y[350])),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
      # cv.circle(blobs, (int(x[i]) , int(y[i])), 5, (255, 0, 0), -1)


#heat map
           
X=x
Y=y
Z=d            
data = pd.DataFrame({'X': X, 'Y': Y, 'Z': Z})
data_pivoted = data.pivot("X", "Y", "Z")
ax = sns.heatmap(data_pivoted,cmap='coolwarm',annot=True)
plt.show()


plt.hist(d,bins=30) 
plt.ylabel('frequency')
plt.xlabel('d class');
plt.show()

plt.imshow(blobs)
plt.imshow(blobs_2)

#%%
cv.imshow("window1",blobs)
cv.imshow("window2",blobs_2)

cv.waitKey(0)
cv.destroyAllWindows()


