# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 09:59:00 2023

@author: FionaLoftus
"""

# %%
import os
import pandas as pd
from data_loading import results_to_df, reference_to_df, get_full_df
from statistics import mean, stdev
from math import sqrt
from scipy import stats
# %%
filepath = r'D:\TheiaClinicalTrials'
results_name = r'Clinical Image Study(1-5).xlsx'
ref_name = r'Pathology Log.xlsx'
sheetname = 'Sheet2'

results_df = results_to_df(results_name, filepath)
ref_df = reference_to_df(ref_name, filepath, sheetname)

full_df = get_full_df(ref_df, results_df)

# full_df.to_csv(os.path.join(filepath, 'study results.csv'))

# %%
# =============================================================================
# 1. PAIRED T-TEST
# H0: No difference between mean values of 2 devices (two-sided)
# =============================================================================

# =============================================================================
# 1. IMAGE CLARITY
# =============================================================================
rdsl_clarity_df = full_df[full_df['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = full_df[full_df['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

stats.ttest_rel(clarity_df['Clarity Score RDSL'], clarity_df['Clarity Score SLD4'])
cohensd = (mean(clarity_df['Clarity Score RDSL'])-mean(clarity_df['Clarity Score SLD4']))\
    / (sqrt((stdev(clarity_df['Clarity Score RDSL'])**2 + stdev(clarity_df['Clarity Score SLD4'])**2)/2))

# %%
# =============================================================================
# # IMAGE CLARITY - REMOVE PROBLEM IMAGES
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
df_remove_pairs = full_df[~full_df['Pair'].isin(remove_pairs)]
rdsl_clarity_df = df_remove_pairs[df_remove_pairs['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = df_remove_pairs[df_remove_pairs['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

stats.ttest_rel(clarity_df['Clarity Score RDSL'], clarity_df['Clarity Score SLD4'])
cohensd = (mean(clarity_df['Clarity Score RDSL'])-mean(clarity_df['Clarity Score SLD4']))\
    / (sqrt((stdev(clarity_df['Clarity Score RDSL'])**2 + stdev(clarity_df['Clarity Score SLD4'])**2)/2))

# %%
# =============================================================================
# # IMAGE CLARITY - REMOVE PROBLEM DRS
# =============================================================================
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]
rdsl_clarity_df = df_4_drs[df_4_drs['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = df_4_drs[df_4_drs['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

stats.ttest_rel(clarity_df['Clarity Score RDSL'], clarity_df['Clarity Score SLD4'])
cohensd = (mean(clarity_df['Clarity Score RDSL'])-mean(clarity_df['Clarity Score SLD4']))\
    / (sqrt((stdev(clarity_df['Clarity Score RDSL'])**2 + stdev(clarity_df['Clarity Score SLD4'])**2)/2))
# %%
# =============================================================================
# # IMAGE CLARITY - REMOVE PROBLEM IMAGES AND DRS
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]
df_remove_drs_pairs = full_df[(~full_df['Pair'].isin(remove_pairs)) & \
                              (full_df['DoctorID'].isin(include_drs))]

rdsl_clarity_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

stats.ttest_rel(clarity_df['Clarity Score RDSL'], clarity_df['Clarity Score SLD4'])

cohensd = (mean(clarity_df['Clarity Score RDSL'])-mean(clarity_df['Clarity Score SLD4']))\
    / (sqrt((stdev(clarity_df['Clarity Score RDSL'])**2 + stdev(clarity_df['Clarity Score SLD4'])**2)/2))
# %%
# =============================================================================
# # 2. DIAGNOSTIC CONFIDENCE
# =============================================================================
rdsl_diag_df = full_df[full_df['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = full_df[full_df['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

stats.ttest_rel(diag_df['Diagnostic Score RDSL'], diag_df['Diagnostic Score SLD4'])

cohensd = (mean(diag_df['Diagnostic Score RDSL'])-mean(diag_df['Diagnostic Score SLD4']))\
    / (sqrt((stdev(diag_df['Diagnostic Score RDSL'])**2 + stdev(diag_df['Diagnostic Score SLD4'])**2)/2))
# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE - REMOVE PROBLEM IMAGES
# =============================================================================
# 
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
df_remove_pairs = full_df[~full_df['Pair'].isin(remove_pairs)]

rdsl_diag_df = df_remove_pairs[df_remove_pairs['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = df_remove_pairs[df_remove_pairs['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

from scipy import stats

stats.ttest_rel(diag_df['Diagnostic Score RDSL'], diag_df['Diagnostic Score SLD4'])
cohensd = (mean(diag_df['Diagnostic Score RDSL'])-mean(diag_df['Diagnostic Score SLD4']))\
    / (sqrt((stdev(diag_df['Diagnostic Score RDSL'])**2 + stdev(diag_df['Diagnostic Score SLD4'])**2)/2))
# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE - REMOVE PROBLEM DRS
# =============================================================================
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]

rdsl_diag_df = df_4_drs[df_4_drs['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = df_4_drs[df_4_drs['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

from scipy import stats

stats.ttest_rel(diag_df['Diagnostic Score RDSL'], diag_df['Diagnostic Score SLD4'])

cohensd = (mean(diag_df['Diagnostic Score RDSL'])-mean(diag_df['Diagnostic Score SLD4']))\
    / (sqrt((stdev(diag_df['Diagnostic Score RDSL'])**2 + stdev(diag_df['Diagnostic Score SLD4'])**2)/2))

# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE - REMOVE PROBLEM DRS AND IMAGES
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]
df_remove_drs_pairs = full_df[(~full_df['Pair'].isin(remove_pairs)) & \
                              (full_df['DoctorID'].isin(include_drs))]
    
rdsl_diag_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

stats.ttest_rel(diag_df['Diagnostic Score RDSL'], diag_df['Diagnostic Score SLD4'])

cohensd = (mean(diag_df['Diagnostic Score RDSL'])-mean(diag_df['Diagnostic Score SLD4']))\
    / (sqrt((stdev(diag_df['Diagnostic Score RDSL'])**2 + stdev(diag_df['Diagnostic Score SLD4'])**2)/2))