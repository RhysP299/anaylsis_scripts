# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 09:51:59 2023

@author: FionaLoftus
"""

import os
import pandas as pd

def results_to_df(filename, filepath):
   
    raw_data = pd.read_excel(os.path.join(filepath,filename), header=[0,1])

    header_list = list(raw_data.columns.levels[0])
    qu_device_codes = [x for x in header_list if not x.startswith('Unnamed')]
    required_cols = ['DoctorID', 'Question Number', 'Diagnosed Pathology', 'Device',\
                     'Pathology Result', 'Clarity Score', 'Diagnostic Score']

    study_results_df = pd.DataFrame(columns=required_cols)

    doctorid_col = 'ID'
    diag_path_col = 'Please select the pathology'
    clarity_col = 'On a scale of 1-5 please rate this image for clarity'
    diag_conf_col = 'On a scale of 1-5 rate this image for Diagnostic Confidence'

    cols_to_use = [diag_path_col, clarity_col, diag_conf_col]

    second_header = list(raw_data.columns.levels[1])
    sec_head_to_use = second_header
    for col in cols_to_use:
        sec_head_to_use = [s if s.startswith(col) == False else col for s in sec_head_to_use]
        
    second_header_dict = dict(zip(second_header,sec_head_to_use ))

    raw_data.rename(columns=second_header_dict, level=1, inplace=True)

    for index, row in raw_data.iterrows():
        doctorid = row['Unnamed: 0_level_0', doctorid_col]
        
        row_dict = {'DoctorID': doctorid}
        
        for qu_device in qu_device_codes:
            # if qu_device == '1111539 SLD4  Corneal Staining':
                qu_num = qu_device.split()[0]
                device = qu_device.split()[1]
                
                row_dict['Question Number'] = qu_num
                row_dict['Device'] = device
                
                diag_path = row[qu_device,diag_path_col]
                clarity = row[qu_device,clarity_col]
                diag_conf = row[qu_device,diag_conf_col]
                
                row_dict['Diagnosed Pathology'] = diag_path
                row_dict['Clarity Score'] = clarity
                row_dict['Diagnostic Score'] = diag_conf
                
                row_df = pd.DataFrame.from_dict([row_dict])
                
                study_results_df = pd.concat([study_results_df,row_df], ignore_index=True)
                
    study_results_df['Question Number'] = study_results_df['Question Number'].astype(int)
    study_results_df['Diagnosed Pathology'] = study_results_df['Diagnosed Pathology'].fillna('HEALTHY')
    
    return study_results_df

def reference_to_df(filename, filepath, sheetname):
            
    reference_data_df = pd.read_excel(os.path.join(filepath,filename), sheet_name=sheetname, usecols='A:F')
    # reference_data_df = reference_data_df[reference_data_df['Device Type']=='RDSL']
    reference_data_df['Question Number'] = reference_data_df['Question Number'].astype(int)
    
    return reference_data_df

def get_full_df(ref_df, results_df):
    
    ref_cols_to_use = ['Question Number', 'Pathology Type', 'Pathology Name', 'Pair']
    full_df = results_df.merge(ref_df[ref_cols_to_use], on='Question Number', how='left')
    
    full_df['Pathology Result'] = full_df.apply(lambda x: True if x['Pathology Name']==x['Diagnosed Pathology']\
                                           else False, axis=1)
    
    return full_df