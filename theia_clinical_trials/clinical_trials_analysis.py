# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 13:46:33 2023

@author: FionaLoftus
"""
    
# %%
import os
import pandas as pd
from data_loading import results_to_df, reference_to_df, get_full_df
filepath = r'D:\TheiaClinicalTrials'
results_name = r'Clinical Image Study(1-5).xlsx'
ref_name = r'Pathology Log.xlsx'
sheetname = 'Sheet2'

results_df = results_to_df(results_name, filepath)
ref_df = reference_to_df(ref_name, filepath, sheetname)

full_df = get_full_df(ref_df, results_df)

full_df.to_csv(os.path.join(filepath, 'study results.csv'))
# %%
# =============================================================================
# # DIAGNOSTIC ACCURACY
# =============================================================================
# 1. PER DEVICE
df_acc_by_device = pd.DataFrame(columns=['Device', 'Accuracy'])
devices = full_df['Device'].unique()
for device in devices:
    full_count = len(full_df[full_df['Device']==device])
    true_count = full_df[full_df['Device']==device]['Pathology Result'].sum()
    accuracy = true_count/full_count
    temp_dict = {'Device': device, 'Accuracy': accuracy}
    temp_df = pd.DataFrame([temp_dict])
    df_acc_by_device = pd.concat([df_acc_by_device,temp_df ])
# %%
# =============================================================================
# # 2. ACCURACY PER DEVICE PER DOCTOR
# =============================================================================

df_acc_per_dr = pd.DataFrame(columns=['Device', 'DoctorID', 'Accuracy'])
doctorids = full_df['DoctorID'].unique()
devices = full_df['Device'].unique()
for device in devices:
    temp_dict = {'Device': device}
    for doctorid in doctorids:
        temp_dict['DoctorID'] = doctorid
        full_count = len(full_df[(full_df['Device']==device) & (full_df['DoctorID']==doctorid)])
        true_count = full_df[(full_df['Device']==device) & (full_df['DoctorID']==doctorid)]\
            ['Pathology Result'].sum()
        accuracy = true_count/full_count
        temp_dict['Accuracy'] = accuracy
        temp_df = pd.DataFrame([temp_dict])
        df_acc_per_dr = pd.concat([df_acc_per_dr,temp_df])
        
# %%
# =============================================================================
# # 3. SELECT 4 DOCTORS
# =============================================================================
include_drs = [2,4,6,7]
df_acc_4_drs = pd.DataFrame(columns=['Device', 'Accuracy'])
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]
devices = full_df['Device'].unique()
for device in devices:
    full_count = len(df_4_drs[df_4_drs['Device']==device])
    true_count = df_4_drs[df_4_drs['Device']==device]['Pathology Result'].sum()
    accuracy = true_count/full_count
    temp_dict = {'Device': device, 'Accuracy': accuracy}
    temp_df = pd.DataFrame([temp_dict])
    df_acc_4_drs = pd.concat([df_acc_4_drs,temp_df ])

# %%
# =============================================================================
# # 3. SELECT 4 DOCTORS - ACCURACY PER DEVICE
# =============================================================================
df_acc_per_dr = pd.DataFrame(columns=['Device', 'DoctorID', 'Accuracy'])
doctorids = df_4_drs['DoctorID'].unique()
devices = df_4_drs['Device'].unique()
for device in devices:
    temp_dict = {'Device': device}
    for doctorid in doctorids:
        temp_dict['DoctorID'] = doctorid
        full_count = len(df_4_drs[(df_4_drs['Device']==device) & \
                                         (df_4_drs['DoctorID']==doctorid)])
        true_count = df_4_drs[(df_4_drs['Device']==device) \
                                     & (df_4_drs['DoctorID']==doctorid)]\
            ['Pathology Result'].sum()
        accuracy = true_count/full_count
        temp_dict['Accuracy'] = accuracy
        temp_df = pd.DataFrame([temp_dict])
        df_acc_per_dr = pd.concat([df_acc_per_dr,temp_df])
# %%
# =============================================================================
# # DEVICE AND PATHOLOGY TYPE
# =============================================================================

pathologys = full_df['Pathology Type'].unique()
devices = full_df['Device'].unique()
df_path_device = pd.DataFrame(columns=['Device', 'Pathology Type', 'Count', 'Accuracy'])
for device in devices:
    for pathology in pathologys:
        full_count = len(full_df[(full_df['Device']==device) & (full_df['Pathology Type']==pathology)])
        true_count = full_df[(full_df['Device']==device) & (full_df['Pathology Type']==pathology)]\
            ['Pathology Result'].sum()
        accuracy = true_count/full_count
        temp_dict = {'Device': device, 
                     'Pathology Type': pathology , 
                     'Accuracy': accuracy,
                     'Count': full_count}
        temp_df = pd.DataFrame([temp_dict])
        df_path_device = pd.concat([df_path_device,temp_df ])

# %%
# ACCURACY PER IMAGE
questions = full_df['Question Number'].unique()
df_qu_device = pd.DataFrame(columns=['Device', 'Question Number', 'Count', 'Accuracy'])
devices = full_df['Device'].unique()
df_qu_device = pd.DataFrame(columns=['Device', 'Question Number', 'Count', 'Accuracy'])
for device in devices:
    for question in questions:
        full_count = len(full_df[(full_df['Device']==device) & (full_df['Question Number']==question)])
        true_count = full_df[(full_df['Device']==device) & (full_df['Question Number']==question)]\
            ['Pathology Result'].sum()
        accuracy = true_count/full_count
        temp_dict = {'Device': device, 
                     'Question Number': question , 
                     'Accuracy': accuracy,
                     'Count': full_count}
        temp_df = pd.DataFrame([temp_dict])
        df_qu_device = pd.concat([df_qu_device,temp_df ])

# # %%
# # REMOVE PROBLEM IMAGES
# image_no_correct = [2293599, 2714020, 8336285, 9352547]
# images_one_correct = [1742041, 3832402, 7311279]
# remove_images = image_no_correct + images_one_correct
# df_remove_qus = full_df[~full_df['Question Number'].isin(remove_images)]
# df_acc_by_device_remove_images = pd.DataFrame(columns=['Device', 'Accuracy'])
# devices = df_remove_qus['Device'].unique()
# for device in devices:
#     full_count = len(df_remove_qus[df_remove_qus['Device']==device])
#     true_count = df_remove_qus[df_remove_qus['Device']==device]['Pathology Result'].sum()
#     accuracy = true_count/full_count
#     temp_dict = {'Device': device, 'Accuracy': accuracy}
#     temp_df = pd.DataFrame([temp_dict])
#     df_acc_by_device_remove_images = pd.concat([df_acc_by_device_remove_images,temp_df ])
    
# %%
# =============================================================================
# # REMOVE PROBLEM PAIRS
# =============================================================================

pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
df_remove_pairs = full_df[~full_df['Pair'].isin(remove_pairs)]
df_acc_by_device_remove_pairs = pd.DataFrame(columns=['Device', 'Accuracy'])
devices = df_remove_pairs['Device'].unique()
for device in devices:
    full_count = len(df_remove_pairs[df_remove_pairs['Device']==device])
    true_count = df_remove_pairs[df_remove_pairs['Device']==device]['Pathology Result'].sum()
    accuracy = true_count/full_count
    temp_dict = {'Device': device, 'Accuracy': accuracy}
    temp_df = pd.DataFrame([temp_dict])
    df_acc_by_device_remove_pairs = pd.concat([df_acc_by_device_remove_pairs,temp_df ])
    
# %%
# =============================================================================
# # REMOVE PROBLEM PAIRS ACCURACY PER DEVICE PER DOCTOR
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
df_remove_pairs = full_df[~full_df['Pair'].isin(remove_pairs)]
df_acc_per_dr = pd.DataFrame(columns=['Device', 'DoctorID', 'Accuracy'])
doctorids = df_remove_pairs['DoctorID'].unique()
devices = df_remove_pairs['Device'].unique()
for device in devices:
    temp_dict = {'Device': device}
    for doctorid in doctorids:
        temp_dict['DoctorID'] = doctorid
        full_count = len(df_remove_pairs[(df_remove_pairs['Device']==device) & \
                                         (df_remove_pairs['DoctorID']==doctorid)])
        true_count = df_remove_pairs[(df_remove_pairs['Device']==device) \
                                     & (df_remove_pairs['DoctorID']==doctorid)]\
            ['Pathology Result'].sum()
        accuracy = true_count/full_count
        temp_dict['Accuracy'] = accuracy
        temp_df = pd.DataFrame([temp_dict])
        df_acc_per_dr = pd.concat([df_acc_per_dr,temp_df])
# %%
# =============================================================================
# # REMOVE PROBLEM PAIRS AND DOCTORS
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct

include_drs = [2,4,6,7]
df_acc_4_drs = pd.DataFrame(columns=['Device', 'Accuracy'])

df_remove_pairs_drs = full_df[(~full_df['Pair'].isin(remove_pairs)) & (full_df['DoctorID'].isin(include_drs))]

df_acc_by_device = pd.DataFrame(columns=['Device', 'Accuracy'])
devices = df_remove_pairs_drs['Device'].unique()
for device in devices:
    full_count = len(df_remove_pairs_drs[df_remove_pairs_drs['Device']==device])
    true_count = df_remove_pairs_drs[df_remove_pairs_drs['Device']==device]['Pathology Result'].sum()
    accuracy = true_count/full_count
    temp_dict = {'Device': device, 'Accuracy': accuracy}
    temp_df = pd.DataFrame([temp_dict])
    df_acc_by_device = pd.concat([df_acc_by_device,temp_df ])
# %%
# =============================================================================
# # REMOVE PROBLEM PAIRS AND DOCTORS - ACCURACY PER DEVICE PER DOCTOR
# =============================================================================
df_acc_per_dr = pd.DataFrame(columns=['Device', 'DoctorID', 'Accuracy'])
doctorids = df_remove_pairs_drs['DoctorID'].unique()
devices = df_remove_pairs_drs['Device'].unique()
for device in devices:
    temp_dict = {'Device': device}
    for doctorid in doctorids:
        temp_dict['DoctorID'] = doctorid
        full_count = len(df_remove_pairs_drs[(df_remove_pairs_drs['Device']==device) & \
                                         (df_remove_pairs['DoctorID']==doctorid)])
        true_count = df_remove_pairs_drs[(df_remove_pairs_drs['Device']==device) \
                                     & (df_remove_pairs_drs['DoctorID']==doctorid)]\
            ['Pathology Result'].sum()
        accuracy = true_count/full_count
        temp_dict['Accuracy'] = accuracy
        temp_df = pd.DataFrame([temp_dict])
        df_acc_per_dr = pd.concat([df_acc_per_dr,temp_df])