# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 11:48:30 2023

@author: FionaLoftus
"""

import os
import pandas as pd
from data_loading import results_to_df, reference_to_df, get_full_df
import matplotlib.pyplot as plt
import numpy as np
filepath = r'D:\TheiaClinicalTrials'
results_name = r'Clinical Image Study(1-5).xlsx'
ref_name = r'Pathology Log.xlsx'
sheetname = 'Sheet2'

results_df = results_to_df(results_name, filepath)
ref_df = reference_to_df(ref_name, filepath, sheetname)

full_df = get_full_df(ref_df, results_df)
# %%
# =============================================================================
# IMAGE CLARITY SCORES
# =============================================================================
rdsl_clarity_df = full_df[full_df['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = full_df[full_df['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = clarity_df['Clarity Score RDSL'].value_counts()
sld4 = clarity_df['Clarity Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='SLD4')
x_lims = ax.get_xlim()
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Image Clarity Scores (Full Dataset)')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Image Clarity Scores Full DataSet.png'))

# %%
# =============================================================================
# IMAGE CLARITY SCORES - REMOVE PROBLEM IMAGES
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
df_remove_pairs = full_df[~full_df['Pair'].isin(remove_pairs)]

rdsl_clarity_df = df_remove_pairs[df_remove_pairs['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = df_remove_pairs[df_remove_pairs['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = clarity_df['Clarity Score RDSL'].value_counts()
sld4 = clarity_df['Clarity Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='Predicate Device')
ax.set_xticks([1,2,3,4,5])
ax.set_xlim(x_lims)
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Image Clarity Scores')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Image Clarity Scores FINAL.png'))
# %%
# =============================================================================
# IMAGE CLARITY SCORES - REMOVE PROBLEM DOCTORS
# =============================================================================
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]

rdsl_clarity_df = df_4_drs[df_4_drs['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = df_4_drs[df_4_drs['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = clarity_df['Clarity Score RDSL'].value_counts()
sld4 = clarity_df['Clarity Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='SLD4')
ax.set_xticks([1,2,3,4,5])
ax.set_xlim(x_lims)
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Image Clarity Scores (Remove Problem Doctors)')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Image Clarity Scores Remove Problem Doctors.png'))
# %%
# =============================================================================
# IMAGE CLARITY SCORES - REMOVE PROBLEM DOCTORS AND IMAGES
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]
df_remove_drs_pairs = full_df[(~full_df['Pair'].isin(remove_pairs)) & \
                              (full_df['DoctorID'].isin(include_drs))]
    
rdsl_clarity_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='RDSL'][['Pair', 'Clarity Score']]
sld4_clarity_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='SLD4'][['Pair', 'Clarity Score']]

clarity_df = rdsl_clarity_df.merge(sld4_clarity_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = clarity_df['Clarity Score RDSL'].value_counts()
sld4 = clarity_df['Clarity Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='SLD4')
ax.set_xticks([1,2,3,4,5])
ax.set_xlim(x_lims)
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Image Clarity Scores (Remove Problem Doctors and Images)')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Image Clarity Scores Remove Problem Doctors And Images.png'))
# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE SCORES
# =============================================================================
rdsl_diag_df = full_df[full_df['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = full_df[full_df['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = diag_df['Diagnostic Score RDSL'].value_counts()
sld4 = diag_df['Diagnostic Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='SLD4')
x_lims = ax.get_xlim()
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Diagnostic Confidence Scores (Full Dataset)')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Diagnostic Confidence Scores Full DataSet.png'))
# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE SCORES - REMOVE PROBLEM IMAGES
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
df_remove_pairs = full_df[~full_df['Pair'].isin(remove_pairs)]

rdsl_diag_df = df_remove_pairs[df_remove_pairs['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = df_remove_pairs[df_remove_pairs['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = diag_df['Diagnostic Score RDSL'].value_counts()
sld4 = diag_df['Diagnostic Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='Predicate Device')
ax.set_xticks([1,2,3,4,5])
ax.set_xlim(x_lims)
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Diagnostic Confidence Scores')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Diagnostic Confidence Scores FINAL.png'))
# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE SCORES - REMOVE PROBLEM DOCTORS
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]

rdsl_diag_df = df_4_drs[df_4_drs['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = df_4_drs[df_4_drs['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = diag_df['Diagnostic Score RDSL'].value_counts()
sld4 = diag_df['Diagnostic Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='SLD4')
ax.set_xticks([1,2,3,4,5])
ax.set_xlim(x_lims)
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Diagnostic Confidence Scores (Remove Problem Doctors)')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Diagnostic Confidence Scores Remove Problem Doctors.png'))
# %%
# =============================================================================
# DIAGNOSTIC CONFIDENCE SCORES - REMOVE PROBLEM DOCTORS AND IMAGES
# =============================================================================
pair_no_correct = [9, 1, 23, 21]
pairs_one_correct = [5, 16, 17]
remove_pairs = pair_no_correct + pairs_one_correct
include_drs = [2,4,6,7]
df_4_drs = full_df[full_df['DoctorID'].isin(include_drs)]
df_remove_drs_pairs = full_df[(~full_df['Pair'].isin(remove_pairs)) & \
                              (full_df['DoctorID'].isin(include_drs))]
    
rdsl_diag_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='RDSL'][['Pair', 'Diagnostic Score']]
sld4_diag_df = df_remove_drs_pairs[df_remove_drs_pairs['Device']=='SLD4'][['Pair', 'Diagnostic Score']]

diag_df = rdsl_diag_df.merge(sld4_diag_df, how='left', on='Pair', suffixes=(' RDSL', ' SLD4'))

rdsl = diag_df['Diagnostic Score RDSL'].value_counts()
sld4 = diag_df['Diagnostic Score SLD4'].value_counts()

fig, ax = plt.subplots()

ax.bar(rdsl.index-0.25, rdsl.values, width=0.3, label='RDSL')
ax.bar(sld4.index+0.25, sld4.values, width=0.3, label='SLD4')
ax.set_xticks([1,2,3,4,5])
ax.set_xlim(x_lims)
ax.set_xlabel('Score')
ax.set_ylabel('Count')
ax.set_title('Diagnostic Confidence Scores (Remove Problem Doctors and Images)')
ax.legend()
fig.savefig(os.path.join(filepath, 'figs', 'Diagnostic Confidence Scores Remove Problem Doctors and Images.png'))