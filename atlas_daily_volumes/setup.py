# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 15:40:20 2022

@author: FionaLoftus
"""

from setuptools import find_packages, setup

setup(
    name='atlas_daily_volumes',
    packages=find_packages(),
    version='1.0',
    description='''Automated production of daily jobs volumes for Atlas clients''',
    author='fiona loftus',
    license='MIT',
)