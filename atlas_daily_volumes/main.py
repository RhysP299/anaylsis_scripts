# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 16:00:04 2022

@author: FionaLoftus
"""

from src.daily_volumes import DailyVolumes
from datetime import date
import sys

def run_scripts():
    print('Enter type of script to run')
    print('For daily script, enter "daily" or "d", for weekend script enter "weekend" or "w"')
    print('Otherwise enter "bespoke" or "b" to set start and end date ')
    dv = DailyVolumes()
    script_type = input()
    
    if (script_type == 'daily') or (script_type == 'd'):
        today = date.today()
        days = 1
        dv.run_script_daily(today, days)
    elif (script_type == 'weekend') or (script_type == 'w'):
        today = date.today()
        days = 3
        # dv.run_script_daily(today, days)
        dv.run_plots(today)
    elif (script_type == 'bespoke') or (script_type == 'b'):
        print('Enter start date (inclusive) (YYYY/MM/DD):')
        start_date = input()
        print('Enter end date (exclusive) (YYYY/MM/DD):')
        end_date = input()
        dv.run_script_historical(start_date, end_date)
        
def run_daily_script(todays_date):
    
    dv = DailyVolumes()
    
    dv.run_script_daily(todays_date)
    
def run_weekend_script(todays_date):
    
    dv = DailyVolumes()
    
    dv.run_script_daily(todays_date, days=3)
    
def run_plots(todays_date):
    
    dv = DailyVolumes()
    
    dv.run_plots(todays_date)
        

if __name__ == '__main__':

    args = sys.argv
    globals()[args[1]](*args[2:])