# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 16:06:48 2022

@author: FionaLoftus
"""

"""
Scripts to run daily jobs volume and plots
"""

from numpy.lib.arraysetops import isin
import openpyxl
import pandas as pd
import numpy as np
import os
import sys
import yaml
import math
from datetime import date, timedelta
from typing import Iterable
import csv
import click
from src.utils import get_project_root, theme_and_tint_to_rgb, add_hash_to_hex, remove_hash_from_hex
from openpyxl import load_workbook
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import PatternFill, colors, Font
from copy import copy
import time
from src.data_elt import flatten, sql_query_to_df, db_to_portal_labels, load_yaml
from src.data_elt import lens_pairs_df, get_average_measurements_per_hour, add_column_to_xlsx_ws
from src.data_elt import sql_query_to_df_ma_all_accounts, sql_query_to_df_all_accounts, get_we_wd_xlsx_colors
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, MO
import matplotlib.dates as mdates
import itertools
from collections import OrderedDict

PATH = get_project_root()
CONFIG_PATH = os.path.join(PATH, 'configs')
CONFIG_NAME = 'system_settings.yml'
DB_PORTAL_MAP = 'db_to_portal_dict.yml'

DAYS_IN_A_WEEK = 7

class DailyVolumes():
    
    def __init__(self, **kwargs):
        
        self.__load_data_config(CONFIG_PATH, 
                                CONFIG_NAME, 
                                DB_PORTAL_MAP,
                                **kwargs)
        
        
    def __load_data_config(self, config_path, config_file, db_map):
        
        try:
            config = load_yaml(config_path, config_file)
        except OSError as e:
            raise(e)
        self._us_configs = config['US']
        self._uk_configs = config['UK']
        self._lens_type_dict = config['lens_type_dict']
        self._client_start_date_dict = self.__get_client_start_date_dict()
        self._color_dict = config['color_dict']
        self._combined_col_dict = config['combined_col_dict']
        self._top_header_row = config['top_header_row']
        self._second_header_row = config['second_header_row']
        self._date_column = config['date_column']
        self._first_weekday_row = config['first_weekday_row']
        self._first_weekend_row = config['first_weekend_row']
        self._job_type_column = config['job_type_column']
        
        self._db_portal_dict = load_yaml(config_path, db_map)
        self._path_to_save = config['path_to_save']
        self._filename = config['filename']
        self.__check_for_new_clients()
        
    def __get_client_start_date_dict(self):
        
        clients = list(self._us_configs['clients'].keys())
        client_start_date_dict = {f'{client} Total': self._us_configs['clients'][client]['date'] \
                                  for client in clients}
            
        clients_uk = list(self._uk_configs['clients'].keys())
        client_start_date_dict.update({f'{client} Total': self._uk_configs['clients'][client]['date'] \
                                  for client in clients_uk})
        # get earliest date
        dates_list = [pd.to_datetime(v).date() for v in client_start_date_dict.values()]
        dates_list = np.sort(dates_list)
        client_start_date_dict.update({'Combined': dates_list[0]})
        return client_start_date_dict
        
    def __check_for_new_clients(self):
        
        # compare config with xlsx columns
        clients = list(self._us_configs['clients'].keys())
        accounts_dict = {client: self._us_configs['clients'][client]['accounts'] for client in clients}
        client_accounts = []
        for client in clients:
            if isinstance(self._us_configs['clients'][client]['accounts'],dict):
                for key in self._us_configs['clients'][client]['accounts']:
                    client_accounts.append(f'{client} {key}')
            # else:
            client_accounts.append(f'{client} Total')
        clients_uk = list(self._uk_configs['clients'].keys())
        accounts_dict.update({client: self._uk_configs['clients'][client]['accounts'] for client in clients_uk})
        
        clients = clients + clients_uk
        for client in clients_uk:
            if isinstance(self._uk_configs['clients'][client]['accounts'],dict):
                for key in self._uk_configs['clients'][client]['accounts']:
                    client_accounts.append(f'{client} {key}')
            client_accounts.append(f'{client} Total')

        config_clients = client_accounts
        
        ws_client_accounts_list = self.__get_xlsx_column_headers()[0]
        
        new_clients = set(config_clients).difference(set(ws_client_accounts_list))
        
        if len(list(new_clients)) > 0:
            self.__add_new_client_to_xlsx(list(new_clients))
        
    def run_script_daily(self, todays_date, days=1, add_uk=True):
        
        """ 
        Pulls previous day's data from database for all clients, and updates job volumes xlsx file

        Inputs:
        todays_date: (str) 
        record_time: (bool) Record query duration

        Returns: void
        """
        start_date = pd.to_datetime(todays_date) - timedelta(days=int(days))
        end_date = pd.to_datetime(todays_date)
        
        df = None
        df_uk = None
        if self._us_configs is not None:
            df = self.__sql_to_df(start_date, end_date, self._us_configs, pairs=True)
        if self._uk_configs is not None:
            df_uk = self.__sql_to_df(start_date, end_date, self._uk_configs, pairs=True)
            
        df = pd.concat([df,df_uk],ignore_index=True,sort=False)

        self.__append_to_xlsx(df, self._us_configs, start_date, end_date, formatting=True, \
                               config_uk=self._uk_configs)
    
    def run_script_historical(self, 
                              start_date, 
                              end_date,
                              add_uk=True):
        """ 
        Pulls historical data from database for all clients, and updates job volumes xlsx file

        Inputs:
        start_date: (str) First date in period (inclusive)
        end_date: (str) Last date in period (exclusive)
        record_time: (bool) Record query duration

        Returns: void

        """
        start_date = pd.to_datetime(start_date)
        end_date = pd.to_datetime(end_date)
        
        df = None
        df_uk = None
        if self._us_configs is not None:
            df = self.__sql_to_df(start_date, end_date, self._us_configs, pairs=True)
        if self._uk_configs is not None:
            df_uk = self.__sql_to_df(start_date, end_date, self._uk_configs, pairs=True)
            df = pd.concat([df,df_uk],ignore_index=True,sort=False)

        self.__append_to_xlsx(df, self._us_configs, start_date, end_date, formatting=True, \
                              config_uk=self._uk_configs)
    
    def plot_daily_job_count(self, client, color, column_names, start_date, end_date):

        """ 
        Plots daily job count for given client over given period

        Inputs:
        client: (str) Name of client/account totals to plot
        color: (str) line color to plot
        start_date: (Timestamp) First date in period to append (inclusive)
        end_date: (Timestamp) End date in period to append (exclusive)

        Returns: void

        """

        file_path = self._path_to_save
        df = pd.read_excel(os.path.join(self._path_to_save, self._filename), names=column_names, \
                           skiprows=self._second_header_row, parse_dates=True)
        df = df[(df['Date'] >= pd.to_datetime(start_date)) & (df['Date'] < pd.to_datetime(end_date))]
        
        df_both = df[df['Job type']=='Both']
        df_sv = df[df['Job type']=='SV']
        df_pal = df[df['Job type']=='PAL']

        fig, ax = plt.subplots(figsize=(10,8))


        ax.set_ylabel('Jobs (pairs)')
        ax.plot(df_both['Date'], df_both[client], color=color, label='Both')
        ax.plot(df_sv['Date'], df_sv[client], color=color, linestyle='--', label='SV')
        ax.plot(df_pal['Date'], df_pal[client], color=color, linestyle=':', label='PAL')

        xl = ax.set_xlabel('Date')
        ax.legend()
        ax.set_xticks(df_both['Date'])
        ax.set_xticklabels(df_both['Date'], rotation=90)
        date_form = DateFormatter("%Y-%m-%d")
        ax.xaxis.set_major_formatter(date_form)
        ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
        ax.set_title(f'{client} Jobs')
        figname = os.path.join(file_path, f'{client} Jobs {start_date}-{end_date}.jpg')
        fig.savefig(figname, box_extra_artists=(xl,), bbox_inches='tight')
        
    def run_plots(self, end_date):
        """ 
        Runs line plots for jobs volume for all clients from specified date period (uses saved xlsx file for data)

        Inputs:
        start_date: (str) First date in period (inclusive)
        end_date: (str) Last date in period (exclusive)

        Returns: void

        """
        column_headers, client_color_dict_theme = self.__get_xlsx_column_headers(get_colors=True)
        # remove k,v where not plotting
        keys_to_keep = [k for k in client_color_dict_theme.keys() if (k not in ['Date', 'Job type']) and \
                        (k.endswith('Total') or k=='Combined')]
        
        # color_theme to hex dictionary including for combined
        color_dict_plus_combined = {**self._color_dict['weekday'], **self._combined_col_dict}
       
        # client hex color dictionary
        client_color_dict_hex = {}
        for key in keys_to_keep:
            client_color_dict_hex[key] = list(color_dict_plus_combined.keys())\
                  [list(color_dict_plus_combined.values()).index(client_color_dict_theme[key])]
            
        
        for k,v in client_color_dict_hex.items():
            start_date = self._client_start_date_dict[k]
            hex_value = f'#{v}'
            self.plot_daily_job_count(k, hex_value, column_headers, start_date, end_date)
            
    def __add_new_client_to_xlsx(self, clients):
        """ 
        Appends a new client(s) column to the xlsx spreadsheet and fills with zeros up til specified date
        Inputs:
        clients: list(str) Name of clients to append to worksheet
        end_date: (str) Last date in period (exclusive)
        pairs: (bool) Combine jobs to lens pairs

        Returns:
        df: (DataFrame) 1 row per job pair
        config: (dict) config dictionary

        """
        # load workbook
        path_to_save = self._path_to_save
        file_path = os.path.join(f'{path_to_save}', self._filename)
        wb = load_workbook(file_path)
        ws = wb.active
        
        for client in clients:
            # 1. insert column based on column to the right
            ws, combined_col_index = add_column_to_xlsx_ws(ws, self._top_header_row, 'Combined')
            new_col_index = combined_col_index-1
           
            
            # 2. get colour based on column to left and color_dict
            new_wd_color, new_we_color = get_we_wd_xlsx_colors(ws, 
                                                               self._color_dict, 
                                                               new_col_index,
                                                               self._first_weekday_row,
                                                               self._first_weekend_row)
            # 3. iterate over rows and fill format and values
            for row in ws.iter_rows(min_row=self._top_header_row, 
                                    max_row=ws.max_row, 
                                    min_col=combined_col_index, 
                                    max_col=combined_col_index):
                for cell in row:
                    # add left and right borders
                    cell.border = Border(top=Side(style='thin'),
                                            left=Side(style='thin'),
                                            right=Side(style='thin'))
                    # HEADER ROWS
                    if cell.row <= self._second_header_row:
                        cell.fill = PatternFill(fill_type='solid', start_color=new_wd_color, end_color=new_wd_color)
                        if cell.row == self._top_header_row:
                            cell.border = Border(top=Side(style='thin'),
                                                    left=Side(style='thin'),
                                                    right=Side(style='thin'))
                            cell.value = client.split(' ')[0]
                            cell.font = Font(bold=True)
                        elif cell.row == self._second_header_row:
                            cell.border = Border(bottom=Side(style='thin'),
                                                    left=Side(style='thin'),
                                                    right=Side(style='thin'))
                            cell.value = client.split(' ')[1]
                    # MAIN DATA ROWS
                    elif cell.row >= self._second_header_row:
                        cell.value = 0
                        cell.font = Font(bold=True)
                        job_type = ws.cell(row=row[0].row,column=self._job_type_column).value
                        # PAL
                        if job_type == 'PAL':
                            cell.border = Border(top=Side(style='thin'),
                                                    left=Side(style='thin'),
                                                    right=Side(style='thin'))
                        elif job_type == 'SV':
                            cell.border = Border(bottom=Side(style='thin'),
                                                    left=Side(style='thin'),
                                                    right=Side(style='thin'))  
                        elif job_type == 'Both':
                            cell.border = Border(bottom=Side(style='thin'),
                                                 top=Side(style='thin'),
                                                    left=Side(style='thin'),
                                                    right=Side(style='thin'))   
                        row_date = pd.to_datetime(ws.cell(row=row[0].row,column=self._date_column).value)
                        get_weekday = row_date.weekday()
                        # WEEKDAYS
                        if (get_weekday < 5):
                            cell.fill = PatternFill(fill_type='solid', start_color=new_wd_color, \
                                                    end_color=new_wd_color)
                        # WEEKENDS
                        else:
                            cell.fill = PatternFill(fill_type='solid', start_color=new_we_color, \
                                                    end_color=new_we_color)
        
        wb.save(os.path.join(self._filename, file_path))
        
    def __sql_to_df(self, start_date, end_date, configs, pairs=True):
        """ 
        Pulls data from database for given period and puts in pandas dataframe

        Inputs:
        start_date: (str) First date in period (inclusive)
        end_date: (str) Last date in period (exclusive)
        pairs: (bool) Combine jobs to lens pairs

        Returns:
        df: (DataFrame) 1 row per job pair
        config: (dict) config dictionary

        """
        
        if configs['sql_configs']['authentication'] == 'sql':
            df = sql_query_to_df_all_accounts(configs, start_date, end_date)
        elif configs['sql_configs']['authentication'] == 'MFA':
            df = sql_query_to_df_ma_all_accounts(configs, start_date, end_date)
        df = db_to_portal_labels(df, self._db_portal_dict)

        if pairs:
            cols = ['LensType', 'AccountId']
            df = lens_pairs_df(df, False, cols=cols)

        return df
    
    def __job_volume_by_day(self, df, config, measurement_date, lens_type, config_uk=None):
        """ 
        Collates measurements into total jobs performed on specified measurement day, for a given lens type 

        Inputs:
        df: (DataFrame) Data exported from portal to pandas df 
        config: (dict) config dict used for list of clients and accounts
        measurement_date: (Timestamp) date of jobs to collate
        lens_type: (dict) lens type name: list(lens types under lens name)

        Returns:
        results_dict: (dict)
        {
            Date: date of measurements
            Lens Type: name of lens types included
            {
                Client: {
                    Account: Total jobs per client and account
                    Total: Total jobs per client
                }
        }
        """
        df_day = df[(pd.to_datetime(df['MeasurementDate']) >= pd.to_datetime(measurement_date)) \
                    & (pd.to_datetime(df['MeasurementDate']) < pd.to_datetime(measurement_date) + timedelta(days=1))]

        df_day = df_day[df_day['LensType'].isin(list(lens_type.values())[0])]

        results_dict = {}
        results_dict['Date'] = measurement_date.date()
        results_dict['Job type'] = list(lens_type.keys())[0]
        
        clients = list(self._us_configs['clients'].keys())
        accounts_dict = {client: self._us_configs['clients'][client]['accounts'] for client in clients}

        if config_uk is not None:
            clients_uk = list(self._uk_configs['clients'].keys())
            accounts_dict.update({client: self._uk_configs['clients'][client]['accounts'] \
                                  for client in clients_uk})
           
            clients = clients + clients_uk
        all_accounts = []
        for client in clients:
            client_accountids = []
            if isinstance(accounts_dict[client],dict):
                for k, v in accounts_dict[client].items():
                    if isinstance(v, list):
                        accounts_ids = v
                    else:
                        accounts_ids = [v]
                    client_accountids.append(accounts_ids)
                    results_dict[f'{client} {k}'] = len(df_day[df_day['AccountId'].isin(accounts_ids)])
                results_dict[f'{client} Total'] = len(df_day[df_day['AccountId'].isin(\
                                                                    [x for x in flatten(client_accountids)])])
                all_accounts.append(client_accountids)
            else:
                accounts_ids = accounts_dict[client]
                results_dict[f'{client} Total'] = len(df_day[df_day['AccountId'].isin(accounts_ids)])
                all_accounts.append(accounts_ids)
        results_dict['Combined'] = len(df_day[df_day['AccountId'].isin([x for x in flatten(all_accounts)])])
        
        # order clients by excel spreadsheet columns
        ws_column_headers = self.__get_xlsx_column_headers()[0]
        
        results_dict_ordered = {k: results_dict[k] for k in ws_column_headers}
        
        return results_dict_ordered
    
    def __append_to_xlsx(self, df, config, start_date, end_date, formatting=True, config_uk=None):
        """ 
        Appends jobs volume to xlsx file for given date period

        Inputs:
        df: (Dataframe) Data exported from portal to pandas df 
        config: (dict) config dict used for list of clients and accounts
        start_date: (Timestamp) First date in period to append (inclusive)
        end_date: (Timestamp) End date in period to append (exclusive)
        formatting: (bool) Format new xlsx rows using existing formatting

        Returns:
        results_dict: (dict)
        {
            Date: date of measurements
            Lens Type: name of lens types included
            {
                Client: {
                    Account: Total jobs per client and account
                    Total: Total jobs per client
                }
        }
        """
        num_days = (pd.to_datetime(end_date) - pd.to_datetime(start_date)).days
        path_to_save = self._path_to_save

        file_path = os.path.join(f'{path_to_save}', self._filename)

        wb = load_workbook(file_path)

        ws = wb.active

        for day in np.arange(0, num_days):
            measurement_date = pd.to_datetime(start_date) + timedelta(days=int(day))

            for k, v in self._lens_type_dict.items():
                results_dict = self.__job_volume_by_day(df, config, measurement_date, {k:v}, config_uk)

                ws.append(list(results_dict.values()))

                if formatting:

                    # excel row with new data reference for formatting
                    new_row = ws.max_row
        
                    # for each cell in row, copy style from previous week's entry
                    row_for_style = ws.iter_rows(min_row=new_row-(len(self._lens_type_dict)*DAYS_IN_A_WEEK), \
                                                  max_row=new_row-(len(self._lens_type_dict)*DAYS_IN_A_WEEK))

                    for row in row_for_style:
                        for cell in row:
                            new_cell = ws.cell(row=new_row, column=cell.col_idx)
                            if k == 'Both':
                                new_cell.border = Border(top=Side(style='thin'),
                                                        bottom=Side(style='thin'),
                                                        right=Side(style='thin'))
                            elif k == 'PAL':
                                new_cell.border = Border( 
                                                top=Side(style='thin'),
                                                right=Side(style='thin'))
                            elif k == 'SV':
                                new_cell.border = Border( 
                                                bottom=Side(style='thin'),
                                                right=Side(style='thin'))

                            if cell.has_style:
                                new_cell.font = copy(cell.font)
                                new_cell.fill = copy(cell.fill)
                                new_cell.number_format = copy(cell.number_format)
                                new_cell.protection = copy(cell.protection)
                                new_cell.alignment = copy(cell.alignment)

        wb.save(os.path.join(self._filename, file_path))

        return results_dict
    
    def __get_xlsx_column_headers(self, get_colors=False):
        
        path_to_save = self._path_to_save
        file_path = os.path.join(f'{path_to_save}', self._filename)
        wb = load_workbook(file_path)
        ws = wb.active
        ws_column_headers = {'top_header': [row for row in ws.iter_rows(min_row=self._top_header_row, \
                                                            max_row=self._top_header_row, values_only=True)],
                              'second_header':  [row for row in ws.iter_rows(min_row=self._second_header_row, \
                                                            max_row=self._second_header_row, values_only=True)]}
        # flatten inner list
        for key in ws_column_headers.keys():
           ws_column_headers[key] = list(itertools.chain(*ws_column_headers[key]))
        
        # where client name is None, client = previous columns client
        last_not_none = 0
        for index,value in enumerate(ws_column_headers['top_header']):
            if value is not None:
                last_not_none = value
            else:
                ws_column_headers['top_header'][index] = last_not_none
        
        ws_column_headers_list = [m+' '+n if n is not None else m for m, n in \
                                   zip(ws_column_headers['top_header'], ws_column_headers['second_header'])]
        
        column_colors_dict = {}
        if get_colors:
            for i, header in enumerate(ws_column_headers_list):
                col_color_tint = ws.cell(row=self._second_header_row, column=i+1).fill.start_color.tint
                col_color_theme = ws.cell(row=self._second_header_row, column=i+1).fill.start_color.theme
                column_colors_dict[header] = [col_color_theme, col_color_tint]
                
        return ws_column_headers_list, column_colors_dict

def concat_dfs(df1, df2):
    """ 
    Concatenates 2 dataframes based on 

    Inputs:
    df1: (Dataframe) Data exported from portal to pandas df 
    df2: (Dataframe) Data exported from portal to pandas df 
    start_date: (Timestamp) First date in period to append (inclusive)
    end_date: (Timestamp) End date in period to append (exclusive)
    formatting: (bool) Format new xlsx rows using existing formatting

    Returns:
    results_dict: (dict)
    {
        Date: date of measurements
        Lens Type: name of lens types included
        {
            Client: {
                Account: Total jobs per client and account
                Total: Total jobs per client
            }
    }
    """


