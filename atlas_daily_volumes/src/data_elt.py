"""
General functions to extract, load and transform data from database using Pandas
"""
import pandas as pd 
import numpy as np
import pyodbc
import os
import yaml
import math
from datetime import date, timedelta, datetime
from typing import Iterable
import csv
import click
from src.utils import get_project_root
import time
import itertools
from openpyxl.styles import PatternFill, colors

def flatten(items):
    """Yield items from any nested iterable"""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x

def load_yaml(file_path, filename):
    """
    Load yaml file given path and filename
    Parameters
    ---------
    path: str
        path to yaml file directory
    filename: str
        filename of yaml file
    
    Returns
    ---------
    yaml_dict: dict
        yaml as dictionary
        
    """
    try:
        with open(os.path.join(file_path, filename)) as stream:
            yaml_dict = yaml.safe_load(stream)
    except OSError as e:
        raise(e)
        
    return yaml_dict

def sql_query_to_df(config, start_date, end_date, accountids, record_time=None):
    """ Run sql query to retrieve selected database entries, given a start date, end date and accountids
    
    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns: 
    df: (Pandas DataFrame)
    """

    server = config['sql_configs']['server']
    db = config['sql_configs']['db']
    user = config['sql_configs']['user']
    pwd = config['sql_configs']['pwd']

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";PWD="+pwd+";")

    query = config['sql_configs']['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    try:
     
        df = pd.read_sql_query(query, conn,  params=params)

    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    conn.close()

    return df

def sql_query_to_df_ma_all_accounts(config, start_date, end_date):
    """ Run sql query to retrieve selected database entries, given a start date, end date and accountids
    
    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns: 
    df: (Pandas DataFrame)
    """

    server = config['sql_configs']['server']
    db = config['sql_configs']['db']
    user = config['sql_configs']['user']
    accountids = []
    clients = list(config['clients'].keys())

    # for client in config['clients']:
    for client in clients:
        if isinstance(config['clients'][client]['accounts'],dict):
            accountids.append(list(config['clients'][client]['accounts'].values()))
        else:
            accountids.append(config['clients'][client]['accounts'])

    accountids = [x for x in flatten(accountids)]

    params = tuple(flatten((accountids, start_date, end_date)))
    # params = (start_date, end_date)

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";Authentication=ActiveDirectoryInteractive")

    query = config['sql_configs']['query']
    # query = query.format('?')
    query = query.format('?', ','.join('?' * len(accountids)))
    try:
        df = pd.read_sql_query(query, conn,  params=params)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
   
    conn.close()

    return df

def sql_query_to_df_all_accounts(config, start_date, end_date, record_time=None):
    """ Run sql query to retrieve selected database entries, given a start date, end date
    
    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns: 
    df: (Pandas DataFrame)
    """

    server = config['sql_configs']['server']
    db = config['sql_configs']['db']
    user = config['sql_configs']['user']
    pwd = config['sql_configs']['pwd']
    # accounts = config['accounts']
    # clients = config['clients']
    clients = list(config['clients'].keys())

    accountids = []
    
    for client in clients:
    # for client in config['clients']:
        if isinstance(config['clients'][client]['accounts'],dict):
            accountids.append(list(config['clients'][client]['accounts'].values()))
        else:
            accountids.append(config['clients'][client]['accounts'])

    accountids = [x for x in flatten(accountids)]

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";PWD="+pwd+";")

    query = config['sql_configs']['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    
    try:
        df = pd.read_sql_query(query, conn,  params=params)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    conn.close()

    return df

def append_time_csv(path, filename, start, duration, size_df, days):

    row = {'start_time':start, 'duration': duration, 'file_size': size_df, 'days': days}
    row['start_time'] = row['start_time'].strftime('%Y/%m/%d %H:%M:%S')

    if not os.path.exists(os.path.join(path, filename)):

        with open(os.path.join(path, filename), 'w', newline='') as csvfile:
            fieldnames = ['start_time','duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow(row)
    else:
        with open(os.path.join(path, filename), 'a', newline='') as csvfile:
            fieldnames = ['start_time', 'duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writerow(row)

def db_to_portal_labels(df, db_portal_dict):
    """ Convert database values to recognised portal values using dictionary 
    
    Inputs:
    df: (Pandas DataFrame)
    db_portal_dict: (dict) db_to_portal dictionary
   
    Returns: 
    df: (Pandas DataFrame)
    """

    for step in db_portal_dict:
        if step in df.columns:
            db_portal_dict[step] = {k: 'NaN' if not v else v for k, v in db_portal_dict[step].items()}
            df[step] = df[step].replace(db_portal_dict[step])
    return df

def lens_pairs_df(df, ignorena=False, cols=None, status_type=False, status_list=[], status_type_list=[]):
    """ Consolidate individual lens measurements into lens pairs

    Inputs:
    df: (Pandas DataFrame) Contains separate rows for left and right lenses 
    ignorena: (bool) If True, 
    cols: list(str) 
    status_type: (bool) If True, separate results into Automatic and Manual
    status_list: list(str)
    status_type_list: list(str)

    Returns: 
    df_lr: (Pandas DataFrame)
    
    """

    df_left = df[df['LensLocation']=='OculusSinister']
    df_right = df[df['LensLocation']=='OculusDexter']
    df_left = df_left.add_prefix('left_')
    df_left = df_left.rename(columns={'left_SerialId': 'SerialId', 'left_MeasurementDate': 'MeasurementDate'})
    df_right = df_right.add_prefix('right_')
    df_right = df_right.rename(columns={'right_SerialId': 'SerialId', 'right_MeasurementDate': 'MeasurementDate'})

    df_lr = pd.merge(df_left, df_right, on=['SerialId', 'MeasurementDate'], how='inner')
    if cols is not None:
        for col in cols:
            df_lr = df_lr.rename(columns={f'right_{col}': f'{col}'})
    
    for status in status_list:
        left_col = 'left_'+ status
        right_col = 'right_'+ status
        df_lr[status] = df_lr.apply(lambda x: calculate_status(x, [left_col, right_col], ignorena), axis=1)

    if status_type:
        for status_type in status_type_list:
            left_col = 'left_'+ status_type
            right_col = 'right_'+ status_type
            df_lr[status_type] = df_lr.apply(lambda x: calculate_type(x, [left_col, right_col]), axis=1)

    return df_lr

def calculate_type(x, columns):

    status_type = 'Automatic'
    for column in columns:
        if x[column] == 'Manual':
            status_type = 'Manual'
            return status_type
    return status_type

def calculate_status(x, columns, ignorena=False):
    """ Calculate status for multiple columns, ie all individual statuses to Pass for combined status Pass

    Inputs:
    x: (Pandas Series)  
    columns: list(str) Names of columns to be used for combined status 
    ignorena: (bool) If True, entries with NaN automatically Pass 
    status_type: (bool) If True, separate results into Automatic and Manual

    Returns: 
    df_lr: (Pandas DataFrame)
    
    """

    status_fail_options = ['Automatic Fail', 'Manual Fail', 'Fail']
    status_na_options = []
    returnstatus = 'Pass'
    if ignorena == False:
        status_na_options.extend(['NaN', 'None'])
    for column in columns:
        if ignorena == False:
            if (isinstance(x[column], float)) or (x[column] in status_na_options):
                current_status = 'NaN'
            elif (x[column] in status_fail_options):
                current_status = 'Fail'
            else:
                current_status = 'Pass'
        else:
            if (x[column] in status_fail_options):
                current_status  = 'Fail'
            else:
                current_status = 'Pass'
        if (current_status == 'NaN') or (returnstatus == 'NaN'):
            returnstatus = 'NaN'
        elif (current_status == 'Fail') or (returnstatus == 'Fail'):
            returnstatus = 'Fail'
        else:
            returnstatus = 'Pass'
    return returnstatus

def get_average_measurements_per_hour(df):

    df = measurement_hour(df)

    df = df_working_hours(df)

    average_measurements_per_hour = len(df) / 10

    return average_measurements_per_hour

def df_working_hours(df):

    working_hours = {12, 13, 14, 15, 16, 17, 18, 19, 20, 21}

    df = df[df['MeasurementHour'].isin(working_hours)]

    return df

def measurement_hour(df):

    df['MeasurementHour'] = pd.to_datetime(df['MeasurementDate']).dt.hour.astype('category')

    return df

def add_column_to_xlsx_ws(ws, header_row_index, right_col_name):
    
    # identify 'Combined' column in header row
    header_cells = [row for row in ws.iter_rows(min_row=header_row_index, max_row=header_row_index)]
    header_cells = list(itertools.chain(*header_cells))
    values = [x.value for x in header_cells]
    right_col_cell = [i for i, j in zip(header_cells, values) if j==right_col_name]
    right_col_index = right_col_cell[0].column
    
    # insert column before Combined column
    ws.insert_cols(right_col_index, 1)
    
    return ws, right_col_index

def get_we_wd_xlsx_colors(ws, 
                          color_dict, 
                          previous_col_index,
                          first_weekday_row,
                          first_weekend_row):
    
    wd_previous_color_tint = ws.cell(row=first_weekday_row,column=previous_col_index).fill.start_color.tint
    wd_previous_color_theme = ws.cell(row=first_weekday_row,column=previous_col_index).fill.start_color.theme
    
    wd_previous_dict_key = list(color_dict['weekday'].keys())[\
                list(color_dict['weekday'].values()).index([wd_previous_color_theme,wd_previous_color_tint])]
    
    # get next key in dictionary
    temp = list(color_dict['weekday'])
    try:
        new_wd_col_key = temp[temp.index(wd_previous_dict_key) + 1]
    except (IndexError):
        new_wd_col_key = temp[0]
    new_wd_color_theme = color_dict['weekday'][new_wd_col_key][0]
    new_wd_color_tint = color_dict['weekday'][new_wd_col_key][1]
    new_wd_color = colors.Color(theme=new_wd_color_theme, tint=new_wd_color_tint)
    
    # get previous col weekend colour
    we_previous_color_tint = ws.cell(row=first_weekend_row,column=previous_col_index).fill.start_color.tint
    we_previous_color_theme = ws.cell(row=first_weekend_row,column=previous_col_index).fill.start_color.theme
    
    we_previous_dict_key = list(color_dict['weekend'].keys())[\
                list(color_dict['weekend'].values()).index([we_previous_color_theme,we_previous_color_tint])]
    
    # get next key in dictionary
    temp = list(color_dict['weekend'])
    try:
        new_we_col_key = temp[temp.index(we_previous_dict_key) + 1]
    except (IndexError):
        new_we_col_key = temp[0]
    new_we_color_theme = color_dict['weekend'][new_we_col_key][0]
    new_we_color_tint = color_dict['weekend'][new_we_col_key][1]
    new_we_color = colors.Color(theme=new_we_color_theme, tint=new_we_color_tint)
    
    
    # for row in ws.iter_rows(min_row=min_row, max_row=max_row, min_col=min_col, max_col=max_col):
    #     for cell in row:
    #         # if header row
    #         if cell.row < 5:
    #             cell.fill = PatternFill(fill_type='solid', start_color=new_wd_color, end_color=new_wd_color)
    #         # else if weekday, use weekday color
    #         elif cell.row >= 5:
                
    #             row_date = pd.to_datetime(ws.cell(row=row[0].row,column=1).value)
    #             get_weekday = row_date.weekday()
    #             # print(get_weekday)
    #             if (get_weekday < 5):
    #                 cell.fill = PatternFill(fill_type='solid', start_color=new_wd_color, end_color=new_wd_color)
    #             else:
    #                 cell.fill = PatternFill(fill_type='solid', start_color=new_we_color, end_color=new_we_color)
                    
    return new_wd_color, new_we_color