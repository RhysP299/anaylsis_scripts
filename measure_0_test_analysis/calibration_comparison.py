# -*- coding: utf-8 -*-
"""
Spyder Editor
A script to access the contense of the manufactoring folder and graph
histograms of the rotate and stitch calibration.
@author: RhysPoolman
"""
import os
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import tools.utilities as tlu

px2mm = 0.0024


def get_data(data, key):
    """
    Extracts arrays of data from dict created from rotate and stitch cal files

    Parameters
    ----------
    data : nested dict
        Must have keys that correspond to rotate and stitch cal file entries.
    key : string
        Key for the lower level dictionary..

    Returns
    -------
    1D numpy.ndarray
        The requested data.
    """
    data_list = []
    for _, entry in data.items():
        data_list.append(entry.get_data_from_cal_syntax(key))
    return np.array(data_list)


# set paths
path = "E:\\eMap\\eMap Manufacturing\\"
data_folders = os.listdir(path)
sub_path = "\\CalibrationData\\RotateStitch\\"
cal_filename = "DualCameraAlignmentCalibration.dat"

# get manufacturing data
demo_room_serial_numbers = ["HR1922002003", "HR1922001002", "LG2029001003",
                            "MS1831004002", "MS1837002003"]
data = {}
error_dict = {}
for folder in data_folders:
    try:
        cal_path = path + folder + sub_path
        date_folders = os.listdir(cal_path)
        date_folder = date_folders[0] + "\\"
        if len(date_folders) > 1:
            dates = [datetime.strptime(date_folder, "%Y%m%d_%H%M")
                     for date_folder in date_folders]
            date_folder = date_folders[np.argmax(dates)] + "\\"
        if date_folder == "":
            raise ValueError("More than one cal attempt on {}".format(folder))
        filename = cal_path + date_folder + cal_filename
        serial_number = filename.split("\\")[3]
        data[serial_number] = \
            tlu.RotateStitchCalibration.from_file(filename, serial_number)
    except OSError as err:
        error_dict[filename.split("\\")[3]] = err
        continue

# extract manufacturing histogram data
dx1_data = get_data(data, "xxxdx1")
dx2_data = get_data(data, "xxxdx2")
dy_data = get_data(data, "xxxdy")
clippling_margin_left_cam1 = get_data(data, "xcm1l")
clippling_margin_left_cam2 = get_data(data, "xcm2l")
clippling_margin_right_cam1 = get_data(data, "xcm1r")
clippling_margin_right_cam2 = get_data(data, "xcm2r")

# create manufactoring histograms
plt.figure("Histogram of X Translation Calibration Values")
plt.clf()
plt.rc("font", size=22)
plt.hist(dx1_data*px2mm, 50, alpha=0.5, label="Cam 1")
plt.hist(dx2_data*px2mm, 50, alpha=0.5, label="Cam 2")
for key, item in data.items():
    if key in demo_room_serial_numbers:
        print(key)
        x = item.get_data_from_cal_syntax("xxxdx1")*px2mm
        plt.plot([x, x], [0.0, 10.0], label=key)
        x = item.get_data_from_cal_syntax("xxxdx2")*px2mm
        plt.plot([x, x], [0.0, 10.0], "--", label=key)
plt.legend()
plt.xlabel("X Translation Values (mm)")

plt.figure("Histogram of Y Translation Calibration Values")
plt.clf()
plt.rc("font", size=22)
plt.hist(dy_data*px2mm, 50)
for key, item in data.items():
    if key in demo_room_serial_numbers:
        print(key)
        x = item.get_data_from_cal_syntax("xxxdy")*px2mm
        plt.plot([x, x], [0.0, 10.0], label=key)
plt.legend()
plt.xlabel("Y Translation Values (mm)")

plt.figure("Histogram of Left Clipping Margin")
plt.clf()
plt.rc("font", size=22)
plt.hist(clippling_margin_left_cam1*px2mm, 30, alpha=0.5, label="Cam 1")
plt.hist(clippling_margin_left_cam2*px2mm, 30, alpha=0.5, label="Cam 2")
for key, item in data.items():
    if key in demo_room_serial_numbers:
        print(key)
        x = item.get_data_from_cal_syntax("xcm1l")*px2mm
        plt.plot([x, x], [0.0, 10.0], label=key)
        x = item.get_data_from_cal_syntax("xcm2l")*px2mm
        plt.plot([x, x], [0.0, 10.0], "--", label=key)
plt.legend()
plt.xlabel("Left Clipping Margin (mm)")

plt.figure("Histogram of Right Clipping Margin")
plt.clf()
plt.rc("font", size=22)
plt.hist(clippling_margin_right_cam1*px2mm, 30, alpha=0.5, label="Cam 1")
plt.hist(clippling_margin_right_cam2*px2mm, 30, alpha=0.5, label="Cam 2")
for key, item in data.items():
    if key in demo_room_serial_numbers:
        print(key)
        x = item.get_data_from_cal_syntax("xcm1r")*px2mm
        plt.plot([x, x], [0.0, 10.0], label=key)
        x = item.get_data_from_cal_syntax("xcm2r")*px2mm
        plt.plot([x, x], [0.0, 10.0], "--", label=key)
plt.legend()
plt.xlabel("Right Clipping Margin (mm)")
