# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 13:57:58 2022
Comparison of results to golden measurements taken on a traditional lens meter.
@author: RhysPoolman
"""
import numpy as np
import pandas as pd
from collections import Counter
import scipy.stats as sps
import matplotlib.pyplot as plt
import utilities as utl


def split_db_data_by_lens_location(df):
    """
    Takes a DataFrame with an EPLensLocation column and creates a dictionary of
    two DataFrames.  One DataFrame is the oculus dexter rows and the other is
    the oculuis sinister.

    Parameters
    ----------
    df : pandas.DataFrame
        A data frame with as column headed EPLensLocation.

    Returns
    -------
    data : dictionary of two DataFrames
        A diction with two DataFrames as erntries one the data for oculus
        dexter and the other for oculus sinister.
    """
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    lens_location_col = "EPLensLocation"
    od_condition = \
        list(df[lens_location_col].str.strip() == od_str)
    os_condition = \
        list(df[lens_location_col].str.strip() == os_str)
    data = {od_str: df.loc[od_condition, :], os_str: df.loc[os_condition, :]}
    return data


def add_prism_col_to_db_data(data):
    diopters = np.array([float(value.split("/")[0])
                         if len(value) > 3 else np.nan
                         for value in data["MPPrism"].values]) \
        if "MPPrism" in data.keys() else data["MPPrismDiopters"].values
    bases = np.array([float(value.split("/")[1])
                      if len(value) > 3 else np.nan
                     for value in data["MPPrism"].values]) \
        if "MPPrism" in data.keys() else data["MPPrismBaseAngleDegrees"].values
    h, v = \
        utl.calculate_components(diopters, bases)
    data = data.assign(prism_h=h)
    data = data.assign(prism_v=v)
    return data


def _calculate_difference_with_golden_data(gold_data, data, pos_str, index,
                                           column, database_column):
    length_of_input = len(data[pos_str].loc[index, database_column])
    # mean over worksheet in golden data
    comparitor_data = [entry.loc[index, column]
                       for _, entry in gold_data[pos_str].items()
                       if not np.isnan(entry.loc[index, column])]
    comparitor = np.mean(comparitor_data)
    # calculate difference
    if "prism" in column:
        comparison = \
            pd.Series(np.ones(length_of_input)*np.nan) \
            if np.isnan(comparitor) else \
            np.abs(pd.to_numeric(data[pos_str].loc[index, database_column])) - \
            np.abs(comparitor)
    else:
        comparison = \
            pd.Series(np.ones(length_of_input)*np.nan) \
            if np.isnan(comparitor) else \
            pd.to_numeric(data[pos_str].loc[index, database_column]) - \
            comparitor
    return [comparison] if type(comparison) == float else comparison.values


def generate_comparison_data(gold_data, data, database_columns):
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    indices = gold_data[od_str]["ROB  NIdek"].index
    difference = \
        {od_str: {index: None for index in indices},
         os_str: {index: None for index in indices}}
    num_indecies = Counter(list(data[od_str].index))
    results_columns = list(database_columns.keys())
    for index in list(indices):
        if index not in data[od_str].index:
            continue
        if len(data[od_str].loc[index, "EPSph"]) != len(data[os_str].loc[index, "EPSph"]):
            print("Job {0:s} has different numbers of OD an OS measurements".format(index))
            continue
        num_rows = num_indecies[index]
        num_cols = len(results_columns)
        difference[od_str][index] = \
            pd.DataFrame(np.zeros((num_rows, num_cols)),
                         columns=results_columns)
        difference[os_str][index] = \
            pd.DataFrame(np.zeros((num_rows, num_cols)),
                         columns=results_columns)
        try:
            for column in results_columns:
                database_column = database_columns[column]
                difference[od_str][index][column] = \
                    _calculate_difference_with_golden_data(gold_data, data,
                                                           od_str, index,
                                                           column,
                                                           database_column)
                difference[os_str][index][column] = \
                    _calculate_difference_with_golden_data(gold_data, data,
                                                           os_str, index,
                                                           column,
                                                           database_column)
        except KeyError as err:
            msg = str(err)
            if msg.isdigit():
                print("{0:s} not in baseline data".format(str(err)))
            else:
                raise err
    return difference


def test_normaility(data, database_columns):
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    normality_pvalues = \
        {od_str: pd.DataFrame({jobid: sps.normaltest(entry)[-1]
                               for jobid, entry in data[od_str].items()
                               if entry is not None and len(entry) > 6}).T,
         os_str: pd.DataFrame({jobid: sps.normaltest(entry)[-1]
                               for jobid, entry in data[os_str].items()
                               if entry is not None and len(entry) > 6}).T}
    normality_pvalues[os_str].columns = database_columns.keys()
    normality_pvalues[od_str].columns = database_columns.keys()
    return normality_pvalues


def calculate_medians(data):
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    median = \
        {od_str: pd.DataFrame({jobid: entry.median()
                               for jobid, entry in data[od_str].items()
                               if entry is not None}).T,
         os_str: pd.DataFrame({jobid: entry.median()
                               for jobid, entry in data[os_str].items()
                               if entry is not None}).T}
    return median


def calculate_u_values(data1, data2, columns):
    results = {}
    for key in data1.keys():
        if entry is None or data1[key] is None or data2[key] is None:
            continue
        results[key] = {column: {"statistic": None, "p-value": None}
                        for column in columns}
        for column in columns:
            x = data1[key].loc[:, column].values
            y = data2[key].loc[:, column].values
            if np.isnan(np.sum(x)) or np.isnan(np.sum(y)):
                continue
            result = sps.mannwhitneyu(x, y)
            results[key][column]["statistic"] = result[0]
            results[key][column]["p-value"] = result[1]
    return results


def plot_p_value_results(data, kind, jobids, title_suffix):
    fig, ax = plt.subplots(1, 2)
    title = "{0:s} P-Value Comparison {1:s}".format(kind, title_suffix)
    fig.canvas.set_window_title(title)
    for ii, (key, item) in enumerate(data.items()):
        ax[ii].set_title(key)
        plt.suptitle(title)
        ylabel = "P-Value"
        x = np.linspace(0, len(item), len(item))
        y = item
        ax[ii].scatter(x, y)
        ax[ii].plot([x[0], x[-1]], [0.05, 0.05], "r")
        ax[ii].set_ylabel(ylabel)
        ax[ii].set_xticks(x)
        ax[ii].set_xticklabels(jobids, rotation=45, ha="right")


def plot_stat_results(data1, data2, kind, jobids):
    fig, ax = plt.subplots(1, 2)
    title = "{0:s} Statistic Comparison".format(kind)
    fig.canvas.set_window_title(title)
    for ii, (key, item) in enumerate(data1.items()):
        ax[ii].set_title(key)
        plt.suptitle(title)
        ylabel = "Statistic"
        data1_array = np.array(item)
        data2_array = np.array(data2[key])
        y = data1_array[data1_array != np.array([None])] - \
            data2_array[data2_array != np.array([None])]
        x = np.linspace(0, len(y), len(y))
        ax[ii].scatter(x, y)
        ax[ii].set_ylabel(ylabel)
        ax[ii].set_xticks(x)
        ax[ii].set_xticklabels(np.array(jobids)[data1_array !=
                                                np.array([None])],
                               rotation=45, ha="right")
    # for ii, (key, item) in enumerate(data2.items()):
    #     y = item
    #     ax[ii].scatter(x, y, label="Post-Fix Wins")
    #     ax[ii].set_ylabel(ylabel)
    #     ax[ii].set_xticks(x)
    #     ax[ii].set_xticklabels(jobids, rotation=45, ha="right")


# load golden measurement data
golden_path = "G:\\GoldStandards\\"
golden_filename = "eMap - Manual Measurment.xlsx"
worksheet_names = ["ROB  NIdek", "ROB  B&S", "JRC NIdek", "JRC B&S"]
raw_golden_data = {worksheet_name: pd.read_excel(golden_path + golden_filename,
                                                 sheet_name=worksheet_name)
                   for worksheet_name in worksheet_names}
golden_columns = list(raw_golden_data[worksheet_names[0]].columns)

# organise data from file
for key, entry in raw_golden_data.items():
    # drop enty first row and relabel index from 0
    entry.drop(axis=0, labels=[0], inplace=True)
    num_rows = len(entry.index)
    entry.index = np.linspace(0, num_rows, num_rows, dtype=int)
    # duplicate data in odd rows of combined prism and pass/fail columns from
    # even rows
    row_to_duplicate = None
    for idx, row in entry.iterrows():
        if idx % 2 == 0:  # index is even
            value_to_duplicate = row
        else:
            entry.loc[idx, golden_columns[-6:-2] + [golden_columns[-1]]] = \
                value_to_duplicate[golden_columns[-6:-2] +
                                   [golden_columns[-1]]]

# load baseline data
pre_fix_path = "E:\\eMap\\Data Folder\\Baseline EMap\\"
pre_fix_filename = "Paul Clapton - 03-11-21.csv"
raw_baseline_data = pd.read_csv(pre_fix_path + pre_fix_filename)

# load post fix data
post_fix_path = "G:\\EngineeringTest\\"
post_fix_filename = "EngineeringTest_2022-01-10_2022-01-14.csv"
raw_post_fix_data = pd.read_csv(post_fix_path + post_fix_filename)
raw_post_fix_data = raw_post_fix_data.astype({"JobID": str})

# set job id as index
golden_data_jobid_index = {key: entry.astype({"JobID": int})
                           for key, entry in raw_golden_data.items()}
golden_data_jobid_index = {key: entry.astype({"JobID": str})
                           for key, entry in golden_data_jobid_index.items()}
golden_data_jobid_index = {key: entry.set_index("JobID")
                           for key, entry in golden_data_jobid_index.items()}
baseline_data_jobid_index = raw_baseline_data.set_index("JobID")
post_fix_data_jobid_index = raw_post_fix_data.set_index("JobID")

# calculate prism components
baseline_data_jobid_index = add_prism_col_to_db_data(baseline_data_jobid_index)
post_fix_data_jobid_index = add_prism_col_to_db_data(post_fix_data_jobid_index)

# split as od and os
od_str = "OculusDexter"
os_str = "OculusSinister"
golden_data = {}
golden_data[od_str] = \
    {name: sheet.loc[list(sheet["Side"].str.strip() == "OD"), :]
     for name, sheet in golden_data_jobid_index.items()}
golden_data[os_str] = \
    {name: sheet.loc[list(sheet["Side"].str.strip() == "OS"), :]
     for name, sheet in golden_data_jobid_index.items()}
baseline_data = split_db_data_by_lens_location(baseline_data_jobid_index)
post_fix_data = split_db_data_by_lens_location(post_fix_data_jobid_index)

# calculate difference between golden data mean and baseline
database_columns = {golden_columns[2]: "MPSph",
                    golden_columns[3]: "MPCyl",
                    golden_columns[4]: "MPAxis",
                    golden_columns[5]: "MPAdd",
                    golden_columns[6]: "prism_h",
                    golden_columns[8]: "prism_v"}
baseline_difference = \
    generate_comparison_data(golden_data, baseline_data, database_columns)
post_fix_difference = \
    generate_comparison_data(golden_data, post_fix_data, database_columns)

# perform normality test
baseline_difference_normality_pvalues = test_normaility(baseline_difference,
                                                        database_columns)
post_fix_difference_normality_pvalues = test_normaility(post_fix_difference,
                                                        database_columns)

# plot normality test
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Baseline Normality Test")
# baseline_difference_normality_pvalues[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)
# baseline_difference_normality_pvalues[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Post-Fix Normality Test")
# post_fix_difference_normality_pvalues[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)
# post_fix_difference_normality_pvalues[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)

# perform median calculation
baseline_difference_median = calculate_medians(baseline_difference)
post_fix_difference_median = calculate_medians(post_fix_difference)

# plot medians
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Baseline Median Differences")
# baseline_difference_median[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                         ylabel="Median")
# baseline_difference_median[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                         ylabel="Median")
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Post-Fix Median Differences")
# post_fix_difference_median[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                         ylabel="Median")
# post_fix_difference_median[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                         ylabel="Median")

# calculate p-value and stat for pre-fix wins
columns = golden_columns[2:7]
columns.append(golden_columns[8])
results_pre_fix = {key: calculate_u_values(entry, post_fix_difference[key],
                                           columns)
                   for key, entry in baseline_difference.items()}
sphere_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
cylinder_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
axis_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
add_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
sphere_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
cylinder_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
axis_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
add_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
prism_h_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
prism_v_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
for key, entry in results_pre_fix.items():
    for jobid, result in entry.items():
        sphere_p_value_pre_fix[key].append(result["Sphere"]["p-value"])
        cylinder_p_value_pre_fix[key].append(result["Cylinder"]["p-value"])
        axis_p_value_pre_fix[key].append(result["Axis"]["p-value"])
        add_p_value_pre_fix[key].append(result["Add"]["p-value"])
        sphere_stat_pre_fix[key].append(result["Sphere"]["statistic"])
        cylinder_stat_pre_fix[key].append(result["Cylinder"]["statistic"])
        axis_stat_pre_fix[key].append(result["Axis"]["statistic"])
        add_stat_pre_fix[key].append(result["Add"]["statistic"])
        prism_h_stat_pre_fix[key].append(result["H Prism Mag"]["statistic"])
        prism_v_stat_pre_fix[key].append(result["V Prism Mag"]["statistic"])

# calculate p-value and stat for post-fix wins
results_pst_fix = {key: calculate_u_values(entry, baseline_difference[key],
                                           columns)
                   for key, entry in post_fix_difference.items()}
sphere_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
cylinder_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
axis_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
add_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
sphere_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
cylinder_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
axis_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
add_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
prism_h_stat_pst_fix = {key: [] for key in results_pre_fix.keys()}
prism_v_stat_pst_fix = {key: [] for key in results_pre_fix.keys()}
for key, entry in results_pst_fix.items():
    for jobid, result in entry.items():
        sphere_p_value_pst_fix[key].append(result["Sphere"]["p-value"])
        cylinder_p_value_pst_fix[key].append(result["Cylinder"]["p-value"])
        axis_p_value_pst_fix[key].append(result["Axis"]["p-value"])
        add_p_value_pst_fix[key].append(result["Add"]["p-value"])
        sphere_stat_pst_fix[key].append(result["Sphere"]["statistic"])
        cylinder_stat_pst_fix[key].append(result["Cylinder"]["statistic"])
        axis_stat_pst_fix[key].append(result["Axis"]["statistic"])
        add_stat_pst_fix[key].append(result["Add"]["statistic"])
        prism_h_stat_pst_fix[key].append(result["H Prism Mag"]["statistic"])
        prism_v_stat_pst_fix[key].append(result["V Prism Mag"]["statistic"])

jobids = list(results_pre_fix[od_str].keys())
# # plot p-value pre fix
# title_suffix = "Pre-fix"
# plot_p_value_results(sphere_p_value_pre_fix, "Sphere", jobids, title_suffix)
# plot_p_value_results(cylinder_p_value_pre_fix, "Cylinder", jobids,
#                      title_suffix)
# plot_p_value_results(axis_p_value_pre_fix, "Axis", jobids, title_suffix)
# plot_p_value_results(add_p_value_pre_fix, "Add", jobids, title_suffix)

# # plot p-value post fix
# title_suffix = "Post-fix"
# jobids = list(results_pst_fix[od_str].keys())
# plot_p_value_results(sphere_p_value_pst_fix, "Sphere", jobids, title_suffix)
# plot_p_value_results(cylinder_p_value_pst_fix, "Cylinder", jobids,
#                      title_suffix)
# plot_p_value_results(axis_p_value_pst_fix, "Axis", jobids, title_suffix)
# plot_p_value_results(add_p_value_pst_fix, "Add", jobids, title_suffix)

# plot u stat comparison
plot_stat_results(sphere_stat_pre_fix, sphere_stat_pst_fix, "Sphere", jobids)
plot_stat_results(cylinder_stat_pre_fix, cylinder_stat_pst_fix, "Cylinder",
                  jobids)
plot_stat_results(axis_stat_pre_fix, axis_stat_pst_fix, "Axis", jobids)
plot_stat_results(add_stat_pre_fix, add_stat_pst_fix, "Add", jobids)
plot_stat_results(prism_h_stat_pre_fix, prism_h_stat_pst_fix, "H Prism", jobids)
plot_stat_results(prism_v_stat_pre_fix, prism_v_stat_pst_fix, "V Prism", jobids)
