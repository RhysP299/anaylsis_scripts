# -*- coding: utf-8 -*-
# %%
"""
Created on Thu Jan 13 13:57:58 2022
Comparison of results to golden measurements taken on a traditional lens meter.
@author: RhysPoolman
"""
import numpy as np
import pandas as pd
from collections import Counter
import scipy.stats as sps
import matplotlib.pyplot as plt
import utilities as utl
import database_interface as db_i
import statsmodels.api as sm
from statsmodels.stats.power import TTestIndPower
from statsmodels.stats.weightstats import ttost_ind, ttest_ind, ztest, ttost_paired
from random import sample


# load golden measurement data
golden_path = r"D:\Measure_{0}\GoldStandards"
golden_filename = "\eMap - Manual Measurment.xlsx"
pre_fix_path = r"D:\Measure_{0}\Baseline EMap"
pre_fix_filename = "\Paul Clapton - 03-11-21.csv"
post_fix_path = r"D:\Measure_{0}\EngineeringTest"
post_fix_filename = "\EngineeringTest_2022-01-10_2022-01-14.csv"
# %%
def split_db_data_by_lens_location(df):
    """
    Takes a DataFrame with an EPLensLocation column and creates a dictionary of
    two DataFrames.  One DataFrame is the oculus dexter rows and the other is
    the oculuis sinister.

    Parameters
    ----------
    df : pandas.DataFrame
        A data frame with as column headed EPLensLocation.

    Returns
    -------
    data : dictionary of two DataFrames
        A diction with two DataFrames as erntries one the data for oculus
        dexter and the other for oculus sinister.
    """
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    lens_location_col = "EPLensLocation"
    od_condition = \
        list(df[lens_location_col].str.strip() == od_str)
    os_condition = \
        list(df[lens_location_col].str.strip() == os_str)
    data = {od_str: df.loc[od_condition, :], os_str: df.loc[os_condition, :]}
    return data


def add_prism_col_to_db_data(data):
    diopters = np.array([float(value.split("/")[0])
                         if len(value) > 3 else np.nan
                         for value in data["MPPrism"].values]) \
        if "MPPrism" in data.keys() else data["MPPrismDiopters"].values
    bases = np.array([float(value.split("/")[1])
                      if len(value) > 3 else np.nan
                     for value in data["MPPrism"].values]) \
        if "MPPrism" in data.keys() else data["MPPrismBaseAngleDegrees"].values
    h, v = \
        utl.calculate_components(diopters, bases)
    data = data.assign(prism_h=h)
    data = data.assign(prism_v=v)
    return data


def _calculate_difference_with_golden_data(gold_data, data, pos_str, index,
                                           column, database_column):
    length_of_input = len(data[pos_str].loc[index, database_column])
    # mean over worksheet in golden data
    comparitor_data = [entry.loc[index, column]
                       for _, entry in gold_data[pos_str].items()
                        if not np.isnan(entry.loc[index, column])]
    comparitor = np.mean(comparitor_data)
    # calculate difference
    if "prism" in column:
        comparison = \
            pd.Series(np.ones(length_of_input)*np.nan) \
            if np.isnan(comparitor) else \
            np.abs(pd.to_numeric(data[pos_str].loc[index, database_column])) - \
            np.abs(comparitor)
    else:
        comparison = \
            pd.Series(np.ones(length_of_input)*np.nan) \
            if np.isnan(comparitor) else \
            pd.to_numeric(data[pos_str].loc[index, database_column]) - \
            comparitor
    return [comparison] if type(comparison) == float else comparison.values


def generate_comparison_data(gold_data, data, database_columns):
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    indices = gold_data[od_str]["ROB  NIdek"].index
    difference = \
        {od_str: {index: None for index in indices},
         os_str: {index: None for index in indices}}
    num_indecies = Counter(list(data[od_str].index))
    results_columns = list(database_columns.keys())
    for index in list(indices):
        if index not in data[od_str].index:
            continue
        if len(data[od_str].loc[index, "EPSph"]) != len(data[os_str].loc[index, "EPSph"]):
            print("Job {0:s} has different numbers of OD an OS measurements".format(index))
            continue
        num_rows = num_indecies[index]
        num_cols = len(results_columns)
        difference[od_str][index] = \
            pd.DataFrame(np.zeros((num_rows, num_cols)),
                         columns=results_columns)
        difference[os_str][index] = \
            pd.DataFrame(np.zeros((num_rows, num_cols)),
                         columns=results_columns)
        try:
            for column in results_columns:
                database_column = database_columns[column]
                difference[od_str][index][column] = \
                    _calculate_difference_with_golden_data(gold_data, data,
                                                           od_str, index,
                                                           column,
                                                           database_column)
                difference[os_str][index][column] = \
                    _calculate_difference_with_golden_data(gold_data, data,
                                                           os_str, index,
                                                           column,
                                                           database_column)
        except KeyError as err:
            msg = str(err)
            if msg.isdigit():
                print("{0:s} not in baseline data".format(str(err)))
            else:
                raise err
    return difference

def test_normaility(data, database_columns):
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    normality_pvalues = \
        {od_str: pd.DataFrame({jobid: sps.normaltest(entry)[-1]
                               for jobid, entry in data[od_str].items()
                               if entry is not None and len(entry) > 6}).T,
         os_str: pd.DataFrame({jobid: sps.normaltest(entry)[-1]
                               for jobid, entry in data[os_str].items()
                               if entry is not None and len(entry) > 6}).T}
    normality_pvalues[os_str].columns = database_columns.keys()
    normality_pvalues[od_str].columns = database_columns.keys()
    return normality_pvalues


def calculate_medians(data):
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    median = \
        {od_str: pd.DataFrame({jobid: entry.median()
                               for jobid, entry in data[od_str].items()
                               if entry is not None}).T,
         os_str: pd.DataFrame({jobid: entry.median()
                               for jobid, entry in data[os_str].items()
                               if entry is not None}).T}
    return median


def calculate_u_values(data1, data2, columns):
    results = {}
    for key in data1.keys():
        if entry is None or data1[key] is None or data2[key] is None:
            continue
        results[key] = {column: {"statistic": None, "p-value": None}
                        for column in columns}
        for column in columns:
            x = data1[key].loc[:, column].values
            y = data2[key].loc[:, column].values
            if np.isnan(np.sum(x)) or np.isnan(np.sum(y)):
                continue
            result = sps.mannwhitneyu(x, y)
            results[key][column]["statistic"] = result[0]
            results[key][column]["p-value"] = result[1]
    return results


def plot_p_value_results(data, kind, jobids, title_suffix):
    fig, ax = plt.subplots(1, 2)
    title = "{0:s} P-Value Comparison {1:s}".format(kind, title_suffix)
    fig.canvas.set_window_title(title)
    for ii, (key, item) in enumerate(data.items()):
        ax[ii].set_title(key)
        plt.suptitle(title)
        ylabel = "P-Value"
        x = np.linspace(0, len(item), len(item))
        y = item
        ax[ii].scatter(x, y)
        ax[ii].plot([x[0], x[-1]], [0.05, 0.05], "r")
        ax[ii].set_ylabel(ylabel)
        ax[ii].set_xticks(x)
        ax[ii].set_xticklabels(jobids, rotation=45, ha="right")


def plot_stat_results(data1, data2, kind, jobids):
    fig, ax = plt.subplots(1, 2)
    title = "{0:s} Statistic Comparison".format(kind)
    fig.canvas.set_window_title(title)
    for ii, (key, item) in enumerate(data1.items()):
        ax[ii].set_title(key)
        plt.suptitle(title)
        ylabel = "Statistic"
        data1_array = np.array(item)
        data2_array = np.array(data2[key])
        y = data1_array[data1_array != np.array([None])] - \
            data2_array[data2_array != np.array([None])]
        x = np.linspace(0, len(y), len(y))
        ax[ii].scatter(x, y)
        ax[ii].set_ylabel(ylabel)
        ax[ii].set_xticks(x)
        ax[ii].set_xticklabels(np.array(jobids)[data1_array !=
                                                np.array([None])],
                               rotation=45, ha="right")
    # for ii, (key, item) in enumerate(data2.items()):
    #     y = item
    #     ax[ii].scatter(x, y, label="Post-Fix Wins")
    #     ax[ii].set_ylabel(ylabel)
    #     ax[ii].set_xticks(x)
    #     ax[ii].set_xticklabels(jobids, rotation=45, ha="right")
    
def get_golden_data_means(gold_data, database_columns):
    
    od_str = 'OculusDexter'
    os_str = 'OculusSinister'
    
    indices = golden_data[od_str]["ROB  NIdek"].index
    columns = list(database_columns.keys())

    users = golden_data[od_str].keys()
    golden_data[od_str]["ROB  NIdek"].loc[[indices[0]]]

    golden_means = {od_str: pd.DataFrame(data=np.zeros(shape=(len(indices), len(columns))), index=indices, columns=columns),\
                    os_str: pd.DataFrame(data=np.zeros(shape=(len(indices), len(columns))), index=indices, columns=columns)}

    for index in indices:
        for column in columns:
            golden_means[od_str].loc[index, column] = \
                np.mean([golden_data[od_str][user].loc[index,column] for user in users])
            golden_means[os_str].loc[index, column] = \
                np.mean([golden_data[os_str][user].loc[index,column] for user in users])
                
    return golden_means

def comparison_data_grouped(diff_data, golden_means, golden_columns):
    
    expected_columns = {golden_columns[2]: f'EP {golden_columns[2]}',
                        golden_columns[3]: f'EP {golden_columns[3]}',
                        golden_columns[4]: f'EP {golden_columns[4]}',
                        golden_columns[5]: f'EP {golden_columns[5]}',
                        golden_columns[6]: f'EP {golden_columns[6]}',
                        golden_columns[8]: f'EP {golden_columns[8]}'}
    
    od_str = 'OculusDexter'
    os_str = 'OculusSinister'
    
    baseline_df_od = pd.DataFrame()
    baseline_df_os = pd.DataFrame()
    for jobid, df in diff_data[od_str].items():
        if df is not None:
            new_df_od = df
            new_df_os = diff_data[os_str][jobid]
            new_df_od['JobId'] = jobid
            new_df_os['JobId'] = jobid
            new_df_od['LensLocation'] = od_str
            new_df_os['LensLocation'] = os_str
            for g_col, ep_col in expected_columns.items():
                new_df_od[ep_col] = golden_means[od_str].loc[jobid, g_col]
                new_df_os[ep_col] = golden_means[os_str].loc[jobid, g_col] 
            baseline_df_od = pd.concat([baseline_df_od,new_df_od])
            baseline_df_os = pd.concat([baseline_df_os,new_df_os])
    # baseline_diff_grouped = {od_str: baseline_df_od, os_str: baseline_df_os}
    baseline_diff_grouped = pd.concat([baseline_df_od, baseline_df_os])
    
    return baseline_diff_grouped

def comparison_post_fix_baseline(baseline_data, post_fix_data, database_columns):
    
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    indices = post_fix_data[od_str].index.unique()
    results_columns = list(database_columns.values())
    row_list = []

    for index in list(indices):
        if index not in baseline_data[od_str].index:
            continue
        if len(baseline_data[od_str].loc[index, "EPSph"]) != len(baseline_data[os_str].loc[index, "EPSph"]):
            print("Job {0:s} has different numbers of OD an OS measurements".format(index))
            continue
        baseline_od_index_df = baseline_data[od_str].loc[index]
        baseline_os_index_df = baseline_data[os_str].loc[index]
        
        post_fix_od_index_df = post_fix_data[od_str].loc[index]
        post_fix_os_index_df = post_fix_data[os_str].loc[index]
        for repeat in np.arange(0, len(post_fix_od_index_df)):
            if repeat < len(baseline_od_index_df):
                row_dict_od = {'JobId': index, 'LensLocation': od_str}
                row_dict_os = {'JobId': index, 'LensLocation': os_str}
                for column in results_columns:
                    row_dict_od[f'{column}_diff'] = pd.to_numeric(post_fix_od_index_df.iloc[repeat][column], errors='coerce') - \
                        pd.to_numeric(baseline_od_index_df.iloc[repeat][column], errors='coerce')
                    row_dict_od[f'{column}_baseline'] = pd.to_numeric(baseline_od_index_df.iloc[repeat][column], errors='coerce')
                    row_dict_od[f'{column}_postfix'] = pd.to_numeric(post_fix_od_index_df.iloc[repeat][column], errors='coerce')
                    row_dict_os[f'{column}_diff'] = pd.to_numeric(post_fix_os_index_df.iloc[repeat][column], errors='coerce') - \
                        pd.to_numeric(baseline_os_index_df.iloc[repeat][column], errors='coerce')
                    row_dict_os[f'{column}_baseline'] = pd.to_numeric(baseline_os_index_df.iloc[repeat][column], errors='coerce')
                    row_dict_os[f'{column}_postfix'] = pd.to_numeric(post_fix_os_index_df.iloc[repeat][column], errors='coerce')
                    
            row_list.append(row_dict_od)
            row_list.append(row_dict_os)
            
    post_fix_baseline_diff = pd.DataFrame(row_list)
    
    return post_fix_baseline_diff

def comparison_of_differences_dists(data, prescriptive_col, lims=None):
    
    prescriptive_data = data.loc[:,prescriptive_col]
    prescriptive_data = prescriptive_data[prescriptive_data.notnull()]
    
    dist_mean = np.mean(prescriptive_data)
    dist_std = np.std(prescriptive_data)
    
    fig, ax = plt.subplots(figsize=(10,8))
    (n, bins, patchs) = ax.hist(prescriptive_data, bins=100)
    ax.plot(bins, sps.norm.pdf(bins, dist_mean, dist_std)*\
            (prescriptive_data.count()*(bins[-1]-bins[0])/len(bins)))
    
    ks, p = sps.kstest(prescriptive_data, 'norm')    
    
    dist_mean_lim = np.NAN
    dist_std_lim = np.NAN
    ks_lim = np.NAN
    p_lim = np.NAN
    
    if lims is not None:
        prescriptive_data_lim = prescriptive_data[(prescriptive_data > lims[0]) & (prescriptive_data < lims[1])]
        
        dist_mean_lim = np.mean(prescriptive_data_lim)
        dist_std_lim = np.std(prescriptive_data_lim)
    
        fig, ax = plt.subplots(figsize=(10,8))
        (n, bins, patchs) = ax.hist(prescriptive_data_lim, bins=100)
        ax.plot(bins, sps.norm.pdf(bins, dist_mean_lim, dist_std_lim)*\
                (prescriptive_data_lim.count()*(bins[-1]-bins[0])/len(bins)))
            
        ks_lim, p_lim = sps.kstest(prescriptive_data_lim, 'norm')    
    
    stats_dict = {'mean': dist_mean, 
                  'std': dist_std, 
                  'ks': ks,
                  'p_value': p,
                  'lim_mean': dist_mean_lim, 
                  'lim_std': dist_std_lim,
                  'lim_ks': ks_lim,
                  'lim_p_value': p_lim}
    
    return stats_dict

def cohend(d1, d2, diff=None):
    
    n1 = len(d1)
    n2 = len(d2)
    
    s1 = np.var(d1,ddof=1)
    s2 = np.var(d2,ddof=1)
    
    s = np.sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))
    
    if diff is None:
        u1 = np.mean(d1)
        u2 = np.mean(d2)
    else:
        u1 = np.mean(d1)
        u2 = u1+diff
        
    return s, (u1-u2)/s

def cohens_d_paired_given_diff(d_diff, fixed_diff=0):
    
    # n = len(d_diff)
    
    s = np.std(d_diff)
    
    
    u1 = np.mean(d_diff)
    u2 = u1 + fixed_diff
    
    return s, (u1-u2)/s

def effect_and_sample_size(d1, d2, tol, power, alpha):
    
    s, cohens_d = cohend(d1, d2, tol)
    
    analysis = TTestIndPower()
    result = analysis.solve_power(cohens_d, power=power, nobs1=None, ratio=1.0, alpha=alpha)
    
    results_dict = {'sample size': result, 'effect': cohens_d}
    
    return results_dict
    
def effect_and_sample_size_paired(d1, tol, power, alpha):
    
    s, cohens_d = cohens_d_paired_given_diff(d1 , tol)
    
    analysis = TTestIndPower()
    result = analysis.solve_power(cohens_d, power=power, nobs1=None, ratio=1.0, alpha=alpha)
    
    results_dict = {'sample size': result, 'effect': cohens_d}
    
    return results_dict
    
def effect_and_p_non_equivalence(d1, d2, low, upp):
    
    # t-test for non equivalence
    
    pvalue, (t1, p1, df1), (t2, p2, df2) = ttost_ind(d1, d2, low, upp)
    
    upper = {'tstat': t1, 'pvalue': p1, 'df': df1}
    lower = {'tstat': t2, 'pvalue': p2, 'df': df2}
    
    results_dict = {'pvalue': pvalue, 'lower': lower, 'upper': upper}
    
    return results_dict
        
def two_samples_t(data1, data2, column, n=50, lims=None):
    
    d1 = data1.loc[:,column]
    d1 = d1[d1.notnull()]
    
    d1_sample = sample(list(d1),n)
    
    d2 = data2.loc[:,column]
    d2 = d2[d2.notnull()]
    
    d2_sample = sample(list(d2.values),n)
    
    cohen_d = cohend(d1_sample, d2_sample)
    
    tstat, p, df = ttest_ind(d1_sample, d2_sample, usevar='unequal')
    results_dict = {}
    results_dict['all data'] = {'tstat': tstat, 'pvalue': p, 'df': df, 'cohens d': cohen_d}
    
    if lims is not None:
        
        d1 = d1[(d1>lims[0]) & (d1<lims[1])]
        d2 = d2[(d2>lims[0]) & (d2<lims[1])]
        tstat, p, df = ttest_ind(d1, d2)
        results_dict[f'{lims}'] = {'tstat': tstat, 'pvalue': p, 'df': df}
    return results_dict

def two_samples_t_non_equiv(data1, data2, column, low, upp, n=50):
    
    d1 = data1.loc[:,column]
    d1 = d1[d1.notnull()]
    
    d1_sample = sample(list(d1),n)
    
    d2 = data2.loc[:,column]
    d2 = d2[d2.notnull()]
    
    d2_sample = sample(list(d2.values),n)
    
    cohen_d = cohend(d1_sample, d2_sample)
    
    p, (t1, p1, df1), (t2, p2, df2) = ttost_ind(d1_sample, d2_sample, usevar='unequal', low=low, upp=upp)
    results_dict = {}
    results_dict['both'] = {'p-value': p, 'cohens d': cohen_d}
    results_dict['lower'] = {'tstat': t1, 'pvalue': p1, 'df': df1}
    results_dict['upper'] = {'tstat': t2, 'pvalue': p2, 'df': df2}
    
    return results_dict
    
def paired_t_test(data1, column, n=50):
    
    d1 = data1.loc[:,f'{column}_diff']
    d1 = d1[d1.notnull()]
    
    d1_sample = sample(list(d1),n)
    
    cohen_d = cohens_d_paired_given_diff(d1_sample)
    
    tstat, p = ztest(d1_sample)
    
    results_dict = {'tstat': tstat, 'pvalue': p, 'effect': cohen_d} 
    
    return results_dict
    
    
def paired_t_non_equiv(data, column, low, upp, n=50):
    
    d1 = data.loc[:,f'{column}_baseline']
    d1 = d1[d1.notnull()]
    
    print(d1)
    
    d2 = data.loc[:,f'{column}_postfix']
    d2 = d2[d2.notnull()]
    
    print(d2)
    
    samples = sample(range(len(d2)), n)
    
    d1_sample = d1.iloc[samples]
    d2_sample = d2.iloc[samples]
    
    p, (t1, p1, df1), (t2, p2, df2) = ttost_paired(d1_sample, d2_sample, low, upp)
    
    results_dict = {}
    results_dict['both'] = {'p-value': p}
    results_dict['lower'] = {'tstat': t1, 'pvalue': p1, 'df': df1}
    results_dict['upper'] = {'tstat': t2, 'pvalue': p2, 'df': df2}
    
    return results_dict
    

    
# %%
# golden data
worksheet_names = ["ROB  NIdek", "ROB  B&S", "JRC NIdek", "JRC B&S"]
raw_golden_data = {worksheet_name: pd.read_excel(golden_path + golden_filename,
                                                 sheet_name=worksheet_name)
                   for worksheet_name in worksheet_names}
golden_columns = list(raw_golden_data[worksheet_names[0]].columns)

# organise data from file
for key, entry in raw_golden_data.items():
    # drop enty first row and relabel index from 0
    entry.drop(axis=0, labels=[0], inplace=True)
    num_rows = len(entry.index)
    entry.index = np.linspace(0, num_rows, num_rows, dtype=int)
    # duplicate data in odd rows of combined prism and pass/fail columns from
    # even rows
    row_to_duplicate = None
    for idx, row in entry.iterrows():
        if idx % 2 == 0:  # index is even
            value_to_duplicate = row
        else:
            entry.loc[idx, golden_columns[-6:-2] + [golden_columns[-1]]] = \
                value_to_duplicate[golden_columns[-6:-2] +
                                   [golden_columns[-1]]]

# load baseline data
# pre_fix_path = "E:\\eMap\\Data Folder\\Baseline EMap\\"
# pre_fix_filename = "Paul Clapton - 03-11-21.csv"
raw_baseline_data = pd.read_csv(pre_fix_path + pre_fix_filename)

# load post fix data
# post_fix_path = "G:\\EngineeringTest\\"
# post_fix_filename = "EngineeringTest_2022-01-10_2022-01-14.csv"
raw_post_fix_data = pd.read_csv(post_fix_path + post_fix_filename)
raw_post_fix_data = raw_post_fix_data.astype({"JobID": str})

# set job id as index
golden_data_jobid_index = {key: entry.astype({"JobID": int})
                           for key, entry in raw_golden_data.items()}
golden_data_jobid_index = {key: entry.astype({"JobID": str})
                           for key, entry in golden_data_jobid_index.items()}
golden_data_jobid_index = {key: entry.set_index("JobID")
                           for key, entry in golden_data_jobid_index.items()}
baseline_data_jobid_index = raw_baseline_data.set_index("JobID")
post_fix_data_jobid_index = raw_post_fix_data.set_index("JobID")

# calculate prism components
baseline_data_jobid_index = add_prism_col_to_db_data(baseline_data_jobid_index)
post_fix_data_jobid_index = add_prism_col_to_db_data(post_fix_data_jobid_index)

# split as od and os
od_str = "OculusDexter"
os_str = "OculusSinister"
golden_data = {}
golden_data[od_str] = \
    {name: sheet.loc[list(sheet["Side"].str.strip() == "OD"), :]
     for name, sheet in golden_data_jobid_index.items()}
golden_data[os_str] = \
    {name: sheet.loc[list(sheet["Side"].str.strip() == "OS"), :]
     for name, sheet in golden_data_jobid_index.items()}
baseline_data = split_db_data_by_lens_location(baseline_data_jobid_index)
post_fix_data = split_db_data_by_lens_location(post_fix_data_jobid_index)

# calculate difference between golden data mean and baseline
database_columns = {golden_columns[2]: "MPSph",
                    golden_columns[3]: "MPCyl",
                    golden_columns[4]: "MPAxis",
                    golden_columns[5]: "MPAdd",
                    golden_columns[6]: "prism_h",
                    golden_columns[8]: "prism_v"}
baseline_difference = \
    generate_comparison_data(golden_data, baseline_data, database_columns)
post_fix_difference = \
    generate_comparison_data(golden_data, post_fix_data, database_columns)

# %%
# perform normality test
baseline_difference_normality_pvalues = test_normaility(baseline_difference,
                                                        database_columns)
post_fix_difference_normality_pvalues = test_normaility(post_fix_difference,
                                                        database_columns)

# plot normality test
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Baseline Normality Test")
# baseline_difference_normality_pvalues[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)
# baseline_difference_normality_pvalues[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Post-Fix Normality Test")
# post_fix_difference_normality_pvalues[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)
# post_fix_difference_normality_pvalues[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                                    ylabel="P-Value", logy=True)

# perform median calculation
baseline_difference_median = calculate_medians(baseline_difference)
post_fix_difference_median = calculate_medians(post_fix_difference)

# plot medians
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Baseline Median Differences")
# baseline_difference_median[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                         ylabel="Median")
# baseline_difference_median[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                         ylabel="Median")
# fig, ax = plt.subplots(1, 2)
# fig.canvas.set_window_title("Post-Fix Median Differences")
# post_fix_difference_median[od_str].plot(ax=ax[0], xlabel="Job ID",
#                                         ylabel="Median")
# post_fix_difference_median[os_str].plot(ax=ax[1], xlabel="Job ID",
#                                         ylabel="Median")

# calculate p-value and stat for pre-fix wins
columns = golden_columns[2:7]
columns.append(golden_columns[8])
results_pre_fix = {key: calculate_u_values(entry, post_fix_difference[key],
                                           columns)
                   for key, entry in baseline_difference.items()}
sphere_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
cylinder_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
axis_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
add_p_value_pre_fix = {key: [] for key in results_pre_fix.keys()}
sphere_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
cylinder_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
axis_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
add_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
prism_h_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
prism_v_stat_pre_fix = {key: [] for key in results_pre_fix.keys()}
for key, entry in results_pre_fix.items():
    for jobid, result in entry.items():
        sphere_p_value_pre_fix[key].append(result["Sphere"]["p-value"])
        cylinder_p_value_pre_fix[key].append(result["Cylinder"]["p-value"])
        axis_p_value_pre_fix[key].append(result["Axis"]["p-value"])
        add_p_value_pre_fix[key].append(result["Add"]["p-value"])
        sphere_stat_pre_fix[key].append(result["Sphere"]["statistic"])
        cylinder_stat_pre_fix[key].append(result["Cylinder"]["statistic"])
        axis_stat_pre_fix[key].append(result["Axis"]["statistic"])
        add_stat_pre_fix[key].append(result["Add"]["statistic"])
        prism_h_stat_pre_fix[key].append(result["H Prism Mag"]["statistic"])
        prism_v_stat_pre_fix[key].append(result["V Prism Mag"]["statistic"])

# calculate p-value and stat for post-fix wins
results_pst_fix = {key: calculate_u_values(entry, baseline_difference[key],
                                           columns)
                   for key, entry in post_fix_difference.items()}
sphere_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
cylinder_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
axis_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
add_p_value_pst_fix = {key: [] for key in results_pst_fix.keys()}
sphere_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
cylinder_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
axis_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
add_stat_pst_fix = {key: [] for key in results_pst_fix.keys()}
prism_h_stat_pst_fix = {key: [] for key in results_pre_fix.keys()}
prism_v_stat_pst_fix = {key: [] for key in results_pre_fix.keys()}
for key, entry in results_pst_fix.items():
    for jobid, result in entry.items():
        sphere_p_value_pst_fix[key].append(result["Sphere"]["p-value"])
        cylinder_p_value_pst_fix[key].append(result["Cylinder"]["p-value"])
        axis_p_value_pst_fix[key].append(result["Axis"]["p-value"])
        add_p_value_pst_fix[key].append(result["Add"]["p-value"])
        sphere_stat_pst_fix[key].append(result["Sphere"]["statistic"])
        cylinder_stat_pst_fix[key].append(result["Cylinder"]["statistic"])
        axis_stat_pst_fix[key].append(result["Axis"]["statistic"])
        add_stat_pst_fix[key].append(result["Add"]["statistic"])
        prism_h_stat_pst_fix[key].append(result["H Prism Mag"]["statistic"])
        prism_v_stat_pst_fix[key].append(result["V Prism Mag"]["statistic"])

jobids = list(results_pre_fix[od_str].keys())
# # plot p-value pre fix
# title_suffix = "Pre-fix"
# plot_p_value_results(sphere_p_value_pre_fix, "Sphere", jobids, title_suffix)
# plot_p_value_results(cylinder_p_value_pre_fix, "Cylinder", jobids,
#                      title_suffix)
# plot_p_value_results(axis_p_value_pre_fix, "Axis", jobids, title_suffix)
# plot_p_value_results(add_p_value_pre_fix, "Add", jobids, title_suffix)

# # plot p-value post fix
# title_suffix = "Post-fix"
# jobids = list(results_pst_fix[od_str].keys())
# plot_p_value_results(sphere_p_value_pst_fix, "Sphere", jobids, title_suffix)
# plot_p_value_results(cylinder_p_value_pst_fix, "Cylinder", jobids,
#                      title_suffix)
# plot_p_value_results(axis_p_value_pst_fix, "Axis", jobids, title_suffix)
# plot_p_value_results(add_p_value_pst_fix, "Add", jobids, title_suffix)

# plot u stat comparison
plot_stat_results(sphere_stat_pre_fix, sphere_stat_pst_fix, "Sphere", jobids)
plot_stat_results(cylinder_stat_pre_fix, cylinder_stat_pst_fix, "Cylinder",
                  jobids)
plot_stat_results(axis_stat_pre_fix, axis_stat_pst_fix, "Axis", jobids)
plot_stat_results(add_stat_pre_fix, add_stat_pst_fix, "Add", jobids)
plot_stat_results(prism_h_stat_pre_fix, prism_h_stat_pst_fix, "H Prism", jobids)
plot_stat_results(prism_v_stat_pre_fix, prism_v_stat_pst_fix, "V Prism", jobids)

# %%
# distribution of spherical difference
sph_diff = []
od_str = 'OculusDexter'
os_str = 'OculusSinister'
for jobid, entry in baseline_difference[od_str].items():
    if entry is not None:
        print(type(entry['Sphere'].values))
        sph_diff.append(entry['Sphere'].values)
sph_diff = list(db_i.flatten(sph_diff))


# %%
# golden data mean values per job id
golden_means = get_golden_data_means(golden_data, database_columns)
# %%
# distribution of expected spherical power
bins = [-6.1,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10.1]
fig, ax = plt.subplots(figsize=(10,8))
ax.hist(golden_means[os_str].loc[:,'Sphere'], bins=bins)
        
# %%
# =============================================================================
# Comparison of differences (using gold data)
# =============================================================================
# TODO: post_fix difference, histograms for each type, can we use normal approx? if yes, what is mean and std?
baseline_diff_grouped = comparison_data_grouped(baseline_difference, golden_means, golden_columns)
# %%
t2_sph_b = comparison_of_differences_dists(baseline_diff_grouped, 'Sphere', lims=[-0.5,0.5])
t2_cyl_b = comparison_of_differences_dists(baseline_diff_grouped, 'Cylinder', lims=[-0.5,0.5])
# %%
t2_ax_b = comparison_of_differences_dists(baseline_diff_grouped, 'Axis', lims=[-10,10])
t2_ad_b = comparison_of_differences_dists(baseline_diff_grouped, 'Add', lims=[-1,1])
# %%
t2_ph_b = comparison_of_differences_dists(baseline_diff_grouped, 'H Prism Mag', lims=[-2,2])
t2_pv_b = comparison_of_differences_dists(baseline_diff_grouped, 'V Prism Mag', lims=[-2,2])
# %%
post_fix_diff_grouped = comparison_data_grouped(post_fix_difference, golden_means, golden_columns)
# %%
t2_sph_pf = comparison_of_differences_dists(post_fix_diff_grouped, 'Sphere', lims=[-0.5,0.5])
t2_cyl_pf = comparison_of_differences_dists(post_fix_diff_grouped, 'Cylinder', lims=[-0.5,0.5])
# %%
t2_ax_pf = comparison_of_differences_dists(post_fix_diff_grouped, 'Axis', lims=[-10,10])
t2_ad_pf = comparison_of_differences_dists(post_fix_diff_grouped, 'Add', lims=[-1,1])
# %%
t2_ph_pf = comparison_of_differences_dists(post_fix_diff_grouped, 'H Prism Mag', lims=[-2,2])
t2_pv_pf = comparison_of_differences_dists(post_fix_diff_grouped, 'V Prism Mag', lims=[-2,2])

# %%
# Power of test, sphere:
t2_s, t2_d = cohend(baseline_diff_grouped.loc[:,'Sphere'], post_fix_diff_grouped.loc[:,'Sphere'], 0.13)
t2_s, t2_d = cohend(baseline_diff_grouped.loc[:,'Cylinder'], post_fix_diff_grouped.loc[:,'Cylinder'], 0.13)
# t2_d = cohens_d_paired_given_diff(baseline_diff_grouped.loc[:,'Sphere'], 0.2)
# %%
sph_t2 = effect_and_sample_size\
    (baseline_diff_grouped.loc[:,'Sphere'], post_fix_diff_grouped.loc[:,'Sphere'], 0.13, 0.8, 0.05)
cyl_t2 = effect_and_sample_size\
    (baseline_diff_grouped.loc[:,'Cylinder'], post_fix_diff_grouped.loc[:,'Cylinder'], 0.13, 0.8, 0.05)
add_t2 = effect_and_sample_size\
    (baseline_diff_grouped.loc[:,'Add'], post_fix_diff_grouped.loc[:,'Add'], 0.12, 0.8, 0.05)
prismh_t2 = effect_and_sample_size\
    (baseline_diff_grouped.loc[:,'H Prism Mag'], post_fix_diff_grouped.loc[:,'H Prism Mag'], 0.33, 0.8, 0.05)
prismv_t2 = effect_and_sample_size\
    (baseline_diff_grouped.loc[:,'V Prism Mag'], post_fix_diff_grouped.loc[:,'V Prism Mag'], 0.33, 0.8, 0.05)
axis_t2 = effect_and_sample_size\
    (baseline_diff_grouped.loc[:,'Axis'], post_fix_diff_grouped.loc[:,'Axis'], 2, 0.8, 0.05)
    
# %%
sph_t2_pilot = effect_and_p_non_equivalence(\
                        baseline_diff_grouped.loc[:,'Sphere'], post_fix_diff_grouped.loc[:,'Sphere'], -0.13, 0.13)
cyl_t2_pilot = effect_and_p_non_equivalence(\
                        baseline_diff_grouped.loc[:,'Cylinder'], post_fix_diff_grouped.loc[:,'Cylinder'], -0.13, 0.13)
    
# %%
sph_t2_pilot = two_samples_t(baseline_diff_grouped, post_fix_diff_grouped, 'Sphere', lims=[-0.1, 0.1], n=83)
cyl_t2_pilot = two_samples_t(baseline_diff_grouped, post_fix_diff_grouped, 'Cylinder', lims=None, n=50)
add_t2_pilot = two_samples_t(baseline_diff_grouped, post_fix_diff_grouped, 'Add', lims=None, n=100)
hprism_t2_pilot = two_samples_t(baseline_diff_grouped, post_fix_diff_grouped, 'H Prism Mag', lims=None, n=100)
vprism_t2_pilot = two_samples_t(baseline_diff_grouped, post_fix_diff_grouped, 'V Prism Mag', lims=None, n=100)
axis_t2_pilot = two_samples_t(baseline_diff_grouped, post_fix_diff_grouped, 'Axis', lims=None, n=50)

# %%
sph_t2_pilot_no_eq = two_samples_t_non_equiv(\
                        baseline_diff_grouped, post_fix_diff_grouped, 'Sphere', low=-0.13, upp=0.13, n=50)
cyl_t2_pilot_no_eq = two_samples_t_non_equiv(\
                        baseline_diff_grouped, post_fix_diff_grouped, 'Cylinder', low=-0.13, upp=0.13, n=52)
add_t2_pilot_no_eq = two_samples_t_non_equiv(\
                        baseline_diff_grouped, post_fix_diff_grouped, 'Add', low=-0.12, upp=0.12, n=100)
hprism_t2_pilot_no_eq = two_samples_t_non_equiv(\
                        baseline_diff_grouped, post_fix_diff_grouped, 'H Prism Mag', low=-0.33, upp=0.33, n=100)
vprism_t2_pilot_no_eq = two_samples_t_non_equiv(\
                        baseline_diff_grouped, post_fix_diff_grouped, 'V Prism Mag', low=-0.33, upp=0.33, n=100)
axis_t2_pilot_no_eq = two_samples_t_non_equiv(\
                        baseline_diff_grouped, post_fix_diff_grouped, 'Axis', low=-2, upp=2, n=50)
# %%
# =============================================================================
# Direct comparison (post fix vs baseline) paired test
# =============================================================================
# TODO: histograms for each type, can we use normal approx? if yes, what is mean and std? (ignore axis for now)
post_fix_baseline_diff = comparison_post_fix_baseline(baseline_data, post_fix_data, database_columns)
# %%
t1_sph = comparison_of_differences_dists(post_fix_baseline_diff, 'MPSph_diff', lims=[-0.5,0.5])
t1_cyl = comparison_of_differences_dists(post_fix_baseline_diff, 'MPCyl_diff', lims=[-0.5,0.5])
t1_ax = comparison_of_differences_dists(post_fix_baseline_diff, 'MPAxis_diff', lims=[-10,10])
# %%
t1_ad = comparison_of_differences_dists(post_fix_baseline_diff, 'MPAdd_diff', lims=[-1,1])
t1_ph = comparison_of_differences_dists(post_fix_baseline_diff, 'prism_h_diff', lims=[-1,1])
t1_pv = comparison_of_differences_dists(post_fix_baseline_diff, 'prism_v_diff', lims=[-1,1])
# %%
sph_t1 = effect_and_sample_size_paired\
    (post_fix_baseline_diff.loc[:,'MPSph_diff'], 0.13, 0.8, 0.05)
cyl_t1 = effect_and_sample_size\
    (post_fix_baseline_diff.loc[:,'MPCyl_diff'], post_fix_baseline_diff.loc[:,'MPCyl_diff'], 0.13, 0.8, 0.05)
add_t1 = effect_and_sample_size\
    (post_fix_baseline_diff.loc[:,'MPAdd_diff'], post_fix_baseline_diff.loc[:,'MPAdd_diff'], 0.12, 0.8, 0.05)
prismh_t1 = effect_and_sample_size\
    (post_fix_baseline_diff.loc[:,'prism_h_diff'], post_fix_baseline_diff.loc[:,'prism_h_diff'], 0.33, 0.8, 0.05)
prismv_t1 = effect_and_sample_size\
    (post_fix_baseline_diff.loc[:,'prism_v_diff'], post_fix_baseline_diff.loc[:,'prism_v_diff'], 0.33, 0.8, 0.05)
axis_t1 = effect_and_sample_size\
    (post_fix_baseline_diff.loc[:,'MPAxis_diff'], post_fix_baseline_diff.loc[:,'MPAxis_diff'], 2, 0.8, 0.05)
# %%
t1_s, t1_d = cohend(post_fix_baseline_diff.loc[:,'MPSph'], post_fix_baseline_diff.loc[:,'MPSph'], 0.13)
# t1_s, t1_d = cohend(post_fix_baseline_diff.loc[:,'MPCyl'], post_fix_baseline_diff.loc[:,'MPCyl'], 0.13)
analysis = TTestIndPower()
result = analysis.solve_power(t1_d, power=0.8, nobs1=None, ratio=1.0, alpha=0.05)
print('Sample Size: %.3f' % result)
# %%
sph_t1_pilot = paired_t_test(post_fix_baseline_diff, 'MPSph', n=79)
cyl_t1_pilot = paired_t_test(post_fix_baseline_diff, 'MPCyl', n=50)
add_t1_pilot = paired_t_test(post_fix_baseline_diff, 'MPAdd', n=50)
prismh_t1_pilot = paired_t_test(post_fix_baseline_diff, 'prism_h', n=50)
# %%
# sph_t1_pilot_no_eq = paired_t_non_equiv(post_fix_baseline_diff, 'MPSph', -0.13, 0.13, n=50)
# cyl_t1_pilot_no_eq = paired_t_non_equiv(post_fix_baseline_diff, 'MPCyl', -0.13, 0.13, n=50)
# add_t1_pilot_no_eq = paired_t_non_equiv(post_fix_baseline_diff, 'MPAdd', -0.12, 0.12, n=50)
prismh_t1_pilot_no_eq = paired_t_non_equiv(post_fix_baseline_diff, 'prism_h', -0.33, 0.33, n=100)
# prismv_t1_pilot_no_eq = paired_t_non_equiv(post_fix_baseline_diff, 'prism_v', -0.33, 0.33, n=50)