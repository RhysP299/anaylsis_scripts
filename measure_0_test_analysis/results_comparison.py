# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 13:35:26 2021
A script to compare the output of the measure_{0} fix with a pre fix output to
test that the proposed fix does not alter the results.
@author: RhysPoolman
"""
import tools.prescription as tlp

# load prescription data
serial_number = "13285091"
path = "R:\\measure_{0} test data\\"
path_with_fix = path + "with_fix_" + serial_number + "\\"
path_without_fix = path + "without_fix_" + serial_number + "\\"
prescription_without_fix = tlp.Prescription(path_without_fix)
prescription_with_fix = tlp.Prescription(path_with_fix)
diff = prescription_without_fix.diff(prescription_with_fix)
for key, values in diff.items():
    print("{0} : {1}".format(key, values))
