# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 14:31:35 2023

@author: FionaLoftus
"""

from src.statistical_tests import DataSample, GoldenData, StatsTest
from tools.data_elt import load_yaml
import tools.utilities as utl
import os
import numpy as np
# %%
file_path = os.path.join(utl.get_project_root())
filename = 'local_config_new_gs.yml'
local_configs = load_yaml(file_path, filename)

sql_dict = {'datasource_type':'sql',
            'start_date':'2022-01-10',
            'end_date':'2022-01-14',
            'client':'Engineering Test',
            'user':'fiona.loftus@eyoto.com'}
csv_dict = {'datasource_type':'csv',
            'path': local_configs['pre_fix_path'],
            'filename': local_configs['pre_fix_filename']}
csv_dict2 = {'datasource_type':'csv',
            'path': local_configs['post_fix_path'],
            'filename': local_configs['post_fix_filename']}

golden_dict = {'datasource_type': 'xlsx',
               'path': local_configs['golden_path'],
               'filename': local_configs['golden_filename'],
               'settings_file': 'system_settings_new_gs.yml'}
               # 'worksheet_names': local_configs['golden_worksheets']}
golden_path = local_configs['golden_path']
golden_filename = local_configs['golden_filename']
golden_data = GoldenData(**golden_dict)
# d1 = DataSample(golden_data=golden_data, **csv_dict)
# %%
d1_comp = d1.datadiff

