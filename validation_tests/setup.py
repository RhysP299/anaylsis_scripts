from setuptools import find_packages, setup

setup(
    name='Validation Tool',
    packages=find_packages(),
    version='1.0',
    description='''Validation tool for alpha testing''',
    author='Fiona Loftus, Mohammed Choudhury',
    license='',
)