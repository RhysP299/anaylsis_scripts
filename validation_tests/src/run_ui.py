# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window1.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QTableWidgetItem, QMainWindow
from src.components_passed_ui import Ui_Components_Passed
from src.test_checker_ui import Ui_Test_Checker
from src.data_export import UI_data_export
from PyQt5 import uic
from src.statistical_tests import DataSample, StatsTest, GoldenData
import logging
import numpy as np
import os
from src.main_window_ui import Ui_MainWindow_File
import tools.utilities as utl
from tools.data_elt import load_yaml

logger = logging.getLogger(__name__)

CONFIGS_DIR = os.path.join(utl.get_project_root(), 'configs')
UI_DIR = os.path.join(utl.get_project_root(), 'ui')

class Window(QMainWindow, Ui_MainWindow_File):
  
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        
        # get stats test parameters and tolerances
        self.__get_test_default_params(CONFIGS_DIR, 'system_settings.yml')
        
        self._d1dict = None
        self._d2dict = None
        self._gsdict = None
        self._data1 = None
        self._data2 = None
        self._gsdata = None
        self._statstest = None
        
        # Disable buttons until previous steps completed
        self.Load_GS_Data.setEnabled(False)
        self.Load_sample1.setEnabled(False)
        self.Load_sample2.setEnabled(False)
        self.Submit.setEnabled(False)
        self.Reset.setEnabled(False)
        
        self.__load_main_window_actions()
        
    def __get_test_default_params(self, path, settings_file):
        
        configs_dict = load_yaml(path, settings_file)
        self._tolerances_dict = configs_dict['tolerances']
        self._significance = configs_dict['significance']
        self._power = configs_dict['power']
        self._test_options = configs_dict['test_options']
        self._test_type = configs_dict['test_type']
        self._allow_trim = configs_dict['trim']
        self.__populate_default_values()
        
        
    def __load_main_window_actions(self):
        
        self.__load_sample1_actions()
        self.__load_sample2_actions()
        self.__load_golden_standard_actions()
        self.__select_test_actions()
        self.__submit_actions()
        self.__reset_actions()
        
    def __load_sample1_actions(self):
        
        self.Load_sample1.clicked.connect(lambda: self.__open_data_export1())
        
    def __load_sample2_actions(self):
        
        self.Load_sample2.clicked.connect(lambda: self.__open_data_export2())
        
    def __select_test_actions(self):
        
        self.__on_test_list_change(self.test_list.currentText())
            
        self.test_list.currentTextChanged.connect(self.__on_test_list_change)
        
    def __on_test_list_change(self, value):
        
        if value == 'Paired T-Test' or value == 'Two Samples T-Test':
            self.Load_sample1.setEnabled(True)
            self.Load_sample2.setEnabled(True)
            self.Load_GS_Data.setEnabled(False)
        elif value == 'Single Sample Golden Standard T-Test':
            self.Load_sample1.setEnabled(True)
            self.Load_sample2.setEnabled(False)
            self.Load_GS_Data.setEnabled(True)
        
    def __load_golden_standard_actions(self):
        
        self.Load_GS_Data.clicked.connect(lambda: self.__open_gold_standard_import())
        
    def __submit_actions(self):
        
        self.Submit.clicked.connect(lambda: self.__on_submit())
        
    def __reset_actions(self):
        
        self.Reset.clicked.connect(lambda: self.__on_reset())
    
    def __tests_passed(self):
        self.results_pass=QtWidgets.QDialog()
        self.ui_components_passed=Ui_Components_Passed()
        self.ui_components_passed.setupUi(self.results_pass)
        self.ui_components_passed.label.setPixmap(\
                        QtGui.QPixmap(os.path.join(UI_DIR, "resources/greentickcircle.png")))
        self.ui_components_passed.label.setScaledContents(True)
        if self.statstest.trim:
            self.ui_components_passed.trim_label.setText( \
                    ("Warning: Some data was trimmed, see log file for details"))
        self.results_pass.show()
        self.results_pass.activateWindow()
        # self.Reset.setEnabled(False)
        
    def __tests_not_passed(self):
        self.results_not_pass=QtWidgets.QWidget()
        self.ui_test_checker=Ui_Test_Checker()
        self.ui_test_checker.setupUi(self.results_not_pass)
        
        TEST_CHECKER_PASS_DICT = {
            'Sphere': self.ui_test_checker.Sphere_pass,
            'Cylinder': self.ui_test_checker.Cylinder_pass,
            'Axis': self.ui_test_checker.Axis_pass,
            'Add': self.ui_test_checker.Add_pass,
            'H Prism Mag': self.ui_test_checker.HPrismMag_pass,
            'V Prism Mag': self.ui_test_checker.VPrismMag_pass}
        
        TEST_CHECKER_FAIL_DICT = {
            'Sphere': self.ui_test_checker.Sphere_fail,
            'Cylinder': self.ui_test_checker.Cylinder_fail,
            'Axis': self.ui_test_checker.Axis_fail,
            'Add': self.ui_test_checker.Add_fail,
            'H Prism Mag': self.ui_test_checker.HPrismMag_fail,
            'V Prism Mag': self.ui_test_checker.VPrismMag_fail}
        
        for key, value in self.statstest.results_table.items():
            if value:
                TEST_CHECKER_PASS_DICT[key].setPixmap(\
                                        QtGui.QPixmap(os.path.join(UI_DIR, "resources/greentick.png")))
                TEST_CHECKER_PASS_DICT[key].setScaledContents(True)
            else:
                TEST_CHECKER_FAIL_DICT[key].setPixmap(\
                                        QtGui.QPixmap(os.path.join(UI_DIR, "resources/redcross.png")))
                TEST_CHECKER_FAIL_DICT[key].setScaledContents(True)
       
        if self.statstest.trim:
            self.ui_test_checker.trim_label.setText( \
                    ("Warning: Some data was trimmed, see log file for details"))
                
        self.results_not_pass.show()
        self.results_not_pass.activateWindow()
        # self.Reset.setEnabled(False)
    
    def __open_data_export1(self):
        self.data_export1=UI_data_export()
        if self.data_export1.exec_() == UI_data_export.Accepted:
            self._d1dict = self.data_export1.get_output()
            self.Load_sample1.setEnabled(False)
            self.Submit.setEnabled(True)
            self.Reset.setEnabled(True)
            self.test_list.setEnabled(False)
            
    def __open_gold_standard_import(self):
        self.gs_data_import=UI_data_export(datasource_type='xlsx')
        if self.gs_data_import.exec_() == UI_data_export.Accepted:
            self._gsdict = self.gs_data_import.get_output()
            self.Load_GS_Data.setEnabled(False)
            
    def __open_data_export2(self):
        self.data_export2=UI_data_export()
        if self.data_export2.exec_() == UI_data_export.Accepted:
            self._d2dict = self.data_export2.get_output()
            self.Load_sample2.setEnabled(False)
                       
    def __on_submit(self):
        
        if self._gsdict is not None:
            self.__initialise_goldenData()
        self.__initialise_datasample1()
        if self._d2dict is not None:
            self.__initialise_datasample2()
        self.__initialise_statsTest()
        self.__set_stats_test_values()
        self.statstest.run_statistical_test()
        if self.statstest.statistical_result:
            self.__tests_passed()
        else:
            self.__tests_not_passed()
            
    def __on_reset(self):

        self._gsdict = None
        self._d1dict = None
        self._d2dict = None
        self._gsdata = None
        self._data1 = None
        self._data2 = None
        self._gsdata = None
        self.test_list.setEnabled(True)
        
        self.__on_test_list_change(self.test_list.currentText())
        self.Reset.setEnabled(False)
        self.Submit.setEnabled(False)
        self._statstest = None
            
    def __initialise_datasample1(self):
        try:
            self._data1 = DataSample(golden_data=self._gsdata, **self._d1dict)
        except Exception:
            logger.exception('Failed to load first data sample. See traceback') 
        
    def __initialise_datasample2(self):

        try:
            self._data2 = DataSample(golden_data=self._gsdata, **self._d2dict)
        except Exception:
            logger.exception('Failed to load second data sample. See traceback')
            
    def __initialise_goldenData(self):
        
        try:
            self._gsdata = GoldenData(**self._gsdict)
        except Exception:
            logger.exception('Failed to load golden standard sample. See traceback')
      
    def __initialise_statsTest(self):
        
        try:
            self._statstest = StatsTest(data1=self._data1, data2=self._data2)
        except Exception:
            logger.exception('Failed to initialise statistical tests.  See traceback.  Reload both datasets')
        
    def __populate_default_values(self):
        
        # use self.statstest properties to populate GUI default values
        
        # 1. populate tolerance tables
        self.__populate_tolerances_table()
        # 2. populate test list
        self.__populate_test_list()
        # 3. populate trim box
        self.__populate_trim_box()
        # 4. populate power table
        self.__populate_power_table()
        
        
    def __populate_tolerances_table(self):
       
        row_count = (len(self._tolerances_dict))
        for row in range(row_count):  # loop to add text items to the table
            # check dictionary key and table label
           
            table_label = self.Tolerances_table.item(row, 0).text()
            dictionary_value = self._tolerances_dict[table_label]
            item = QtWidgets.QTableWidgetItem()
            self.Tolerances_table.setItem(row, 1, item)
            item =self.Tolerances_table.item(row, 1)
            item.setText(f'{dictionary_value}')
       
        
    def __populate_test_list(self):
        
        test_menu=self._test_options
        test_assign=self._test_type
        item=list(test_menu)
        #text added from array
     
        self.test_list.addItems(item)  
        for i in item:
            
            if i== test_assign:
                self.test_list.setCurrentText(i)
                
    def __populate_power_table(self):
        
        for row_num in np.arange(0, self.power_table.rowCount()):
            row_name = self.power_table.item(row_num,0).text()
            if row_name == 'Significance':
                self.power_table.setItem(row_num, 1, QTableWidgetItem(str(self._significance)))
            elif row_name == 'Power':
                self.power_table.setItem(row_num, 1, QTableWidgetItem(str(self._power)))
        
    def __populate_trim_box(self):
        
        trim_status = self._allow_trim
        if trim_status:
            self.Allow_trim.setChecked(True)
        else:
            self.Allow_trim.setChecked(False)
    
    def __set_stats_test_values(self):
        
       # use GUI values to set self.statstest properties
        self.__set_tolerances_property()
        self.__set_test_property()
        self.__set_trim_property()
        self.__set_power_properties()
    
    def __set_tolerances_property(self):
        
        tolerances_dict = {k: None for k in self._tolerances_dict.keys()}
        for row in np.arange(0, self.Tolerances_table.rowCount()):
            prescriptive_component = self.Tolerances_table.item(row,0).text()
            tolerances_dict[prescriptive_component] = float(self.Tolerances_table.item(row,1).text())
            
        self.statstest.tolerances = tolerances_dict
        
    def __set_test_property(self):
        
        self.statstest.test_type = self.test_list.currentText()
        
    def __set_power_properties(self):
        
        for row_num in np.arange(0, self.power_table.rowCount()):
            row_name = self.power_table.item(row_num,0).text()
            if row_name == 'Significance':
                self.statstest.significance = float(self.power_table.item(row_num,1).text())
            elif row_name == 'Power':
                self.statstest.power = float(self.power_table.item(row_num,1).text())
        
    def __set_trim_property(self):
        
        if self.Allow_trim.isChecked():
            self.statstest.allow_trim = True
        else:
            self.statstest.allow_trim = False
        
        
    @property
    def data1(self):
        """
        A property containing sample data 1

        Returns
        -------
        Datasample
            A DataSample object for dataset 1
        """
        return self._data1
    
    @property
    def data2(self):
        """
        A property containing sample data 2

        Returns
        -------
        Datasample
            A DataSample object for dataset 2
        """
        return self._data2
    
    @property
    def gsdata(self):
        """
        A property containing golden sample data

        Returns
        -------
        GoldenData
            A GoldenData object for golden standard data
        """
        return self._gsdata
    
    @property
    def statstest(self):
        """
        A property containing a StatsTest object

        Returns
        -------
        StatsTest
            A StatsTest object for datasets 1 and 2 
        """
        return self._statstest
   

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow_File = QtWidgets.QMainWindow()
    ui = Ui_MainWindow_File()
    ui.setupUi(MainWindow_File)
    MainWindow_File.show()
    sys.exit(app.exec_())
    
