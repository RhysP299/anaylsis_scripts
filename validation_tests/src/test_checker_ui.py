# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/test_checker.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Test_Checker(object):
    def setupUi(self, Test_Checker):
        Test_Checker.setObjectName("Test_Checker")
        Test_Checker.setEnabled(True)
        Test_Checker.resize(435, 382)
        self.ResultsTable = QtWidgets.QTableWidget(Test_Checker)
        self.ResultsTable.setGeometry(QtCore.QRect(50, 100, 311, 211))
        self.ResultsTable.setObjectName("ResultsTable")
        self.ResultsTable.setColumnCount(3)
        self.ResultsTable.setRowCount(6)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setVerticalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setVerticalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(0, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setFamily("MS Shell Dlg 2")
        item.setFont(font)
        self.ResultsTable.setItem(0, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(0, 2, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(1, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(2, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(3, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(4, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.ResultsTable.setItem(5, 0, item)
        self.line = QtWidgets.QFrame(Test_Checker)
        self.line.setGeometry(QtCore.QRect(50, 120, 311, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.Cylinder_pass = QtWidgets.QLabel(Test_Checker)
        self.Cylinder_pass.setGeometry(QtCore.QRect(190, 160, 31, 21))
        self.Cylinder_pass.setText("")
        self.Cylinder_pass.setScaledContents(True)
        self.Cylinder_pass.setObjectName("Cylinder_pass")
        self.Axis_pass = QtWidgets.QLabel(Test_Checker)
        self.Axis_pass.setGeometry(QtCore.QRect(190, 190, 31, 21))
        self.Axis_pass.setText("")
        self.Axis_pass.setScaledContents(True)
        self.Axis_pass.setObjectName("Axis_pass")
        self.Add_pass = QtWidgets.QLabel(Test_Checker)
        self.Add_pass.setGeometry(QtCore.QRect(190, 220, 31, 21))
        self.Add_pass.setText("")
        self.Add_pass.setScaledContents(True)
        self.Add_pass.setObjectName("Add_pass")
        self.HPrismMag_pass = QtWidgets.QLabel(Test_Checker)
        self.HPrismMag_pass.setGeometry(QtCore.QRect(190, 250, 31, 21))
        self.HPrismMag_pass.setText("")
        self.HPrismMag_pass.setScaledContents(True)
        self.HPrismMag_pass.setObjectName("HPrismMag_pass")
        self.VPrismMag_pass = QtWidgets.QLabel(Test_Checker)
        self.VPrismMag_pass.setGeometry(QtCore.QRect(190, 280, 31, 21))
        self.VPrismMag_pass.setText("")
        self.VPrismMag_pass.setScaledContents(True)
        self.VPrismMag_pass.setObjectName("VPrismMag_pass")
        self.Sphere_fail = QtWidgets.QLabel(Test_Checker)
        self.Sphere_fail.setGeometry(QtCore.QRect(280, 120, 51, 41))
        self.Sphere_fail.setText("")
        self.Sphere_fail.setScaledContents(True)
        self.Sphere_fail.setObjectName("Sphere_fail")
        self.Cylinder_fail = QtWidgets.QLabel(Test_Checker)
        self.Cylinder_fail.setGeometry(QtCore.QRect(280, 150, 51, 41))
        self.Cylinder_fail.setText("")
        self.Cylinder_fail.setScaledContents(True)
        self.Cylinder_fail.setObjectName("Cylinder_fail")
        self.Axis_fail = QtWidgets.QLabel(Test_Checker)
        self.Axis_fail.setGeometry(QtCore.QRect(280, 180, 51, 41))
        self.Axis_fail.setText("")
        self.Axis_fail.setScaledContents(True)
        self.Axis_fail.setObjectName("Axis_fail")
        self.Add_fail = QtWidgets.QLabel(Test_Checker)
        self.Add_fail.setGeometry(QtCore.QRect(280, 210, 51, 41))
        self.Add_fail.setText("")
        self.Add_fail.setScaledContents(True)
        self.Add_fail.setObjectName("Add_fail")
        self.HPrismMag_fail = QtWidgets.QLabel(Test_Checker)
        self.HPrismMag_fail.setGeometry(QtCore.QRect(280, 240, 51, 41))
        self.HPrismMag_fail.setText("")
        self.HPrismMag_fail.setScaledContents(True)
        self.HPrismMag_fail.setObjectName("HPrismMag_fail")
        self.VPrismMag_fail = QtWidgets.QLabel(Test_Checker)
        self.VPrismMag_fail.setGeometry(QtCore.QRect(280, 270, 51, 41))
        self.VPrismMag_fail.setText("")
        self.VPrismMag_fail.setScaledContents(True)
        self.VPrismMag_fail.setObjectName("VPrismMag_fail")
        self.title_label = QtWidgets.QLabel(Test_Checker)
        self.title_label.setGeometry(QtCore.QRect(30, 10, 351, 81))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.title_label.setFont(font)
        self.title_label.setAlignment(QtCore.Qt.AlignCenter)
        self.title_label.setObjectName("title_label")
        self.trim_label = QtWidgets.QLabel(Test_Checker)
        self.trim_label.setEnabled(True)
        self.trim_label.setGeometry(QtCore.QRect(40, 330, 351, 21))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.trim_label.setFont(font)
        self.trim_label.setText("")
        self.trim_label.setObjectName("trim_label")
        self.Sphere_pass = QtWidgets.QLabel(Test_Checker)
        self.Sphere_pass.setGeometry(QtCore.QRect(190, 130, 31, 21))
        self.Sphere_pass.setText("")
        self.Sphere_pass.setScaledContents(True)
        self.Sphere_pass.setObjectName("Sphere_pass")

        self.retranslateUi(Test_Checker)
        QtCore.QMetaObject.connectSlotsByName(Test_Checker)

    def retranslateUi(self, Test_Checker):
        _translate = QtCore.QCoreApplication.translate
        Test_Checker.setWindowTitle(_translate("Test_Checker", "Form"))
        item = self.ResultsTable.horizontalHeaderItem(0)
        item.setText(_translate("Test_Checker", "Component"))
        item = self.ResultsTable.horizontalHeaderItem(1)
        item.setText(_translate("Test_Checker", "Pass"))
        item = self.ResultsTable.horizontalHeaderItem(2)
        item.setText(_translate("Test_Checker", "Fail"))
        __sortingEnabled = self.ResultsTable.isSortingEnabled()
        self.ResultsTable.setSortingEnabled(False)
        item = self.ResultsTable.item(0, 0)
        item.setText(_translate("Test_Checker", "Sphere"))
        item = self.ResultsTable.item(1, 0)
        item.setText(_translate("Test_Checker", "Cylinder"))
        item = self.ResultsTable.item(2, 0)
        item.setText(_translate("Test_Checker", "Axis"))
        item = self.ResultsTable.item(3, 0)
        item.setText(_translate("Test_Checker", "Add"))
        item = self.ResultsTable.item(4, 0)
        item.setText(_translate("Test_Checker", "H Prism Mag"))
        item = self.ResultsTable.item(5, 0)
        item.setText(_translate("Test_Checker", "V Prism Mag"))
        self.ResultsTable.setSortingEnabled(__sortingEnabled)
        self.title_label.setText(_translate("Test_Checker", "Some components \n"
" did not pass validation"))
