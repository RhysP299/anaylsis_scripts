# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QLabel, QFileDialog,QDialog,QLineEdit
from PyQt5 import uic
from PyQt5.QtGui import QPixmap
import sys
import os
from tools import utilities as utl

GUI_PATH = os.path.join(utl.get_project_root(), 'ui')

class UI_data_export(QDialog):
    def __init__(self, datasource_type='csv'):
        super(UI_data_export,self).__init__()
        
        #load the ui file
        uic.loadUi(os.path.join(GUI_PATH, "Data_export.ui"),self)
        
        #Define our widgets
        self.button = self.findChild(QPushButton, "Upload_file")
        self.lineEdit = self.findChild(QLineEdit, "lineEdit_2")
        
        self._datasource_type = datasource_type
        
        #Click the dropdown Box
        self.button.clicked.connect(self.clicker)
        
        #Show The App
        self.show()
        
    def clicker(self):
        #self.label.setText("you Clicked the Button!")
        # Open File Dialog
        self._path = QFileDialog.getOpenFileName(self,"Open File","C:\\Users\MohammedChoudhury\\Documents\\qt designer\\Icons\\Icons\\Icons","All Files(*);;Python Files (*.py)")
        
        #output filename to screen                                    
        if self._path:
            self.lineEdit.setText(self._path[0])
            self._filename=os.path.basename(self._path[0])
            self._pathname = os.path.dirname(self._path[0])
            self._csv_dict = {'datasource_type':self._datasource_type,
                        'path':self._pathname,
                        'filename':self._filename}
            
    def accept(self):        
        self._output = self._csv_dict 
        super(UI_data_export, self).accept()
        
    def get_output(self):
        return self._output
                                            
        
if __name__ == "__main__":
# Intialise The App
    app =QApplication(sys.argv)
    UIWindow = UI_data_export()
    app.exec_()
