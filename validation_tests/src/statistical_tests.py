# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:22:21 2022
Classes for statistical tests, including sample data and golden standard data for eMap validation study
@author: FionaLoftus
"""

import pandas as pd
import numpy as np
from numpy.polynomial import Polynomial
import tools.utilities as utl
import warnings
from statsmodels.stats.power import TTestIndPower
from statsmodels.stats.weightstats import ttost_ind, ttest_ind, ztest, ttost_paired
from scipy.stats.mstats import winsorize
from scipy.stats import shapiro
from scipy.stats import chi2_contingency
import scipy.stats as sts
from scipy.optimize import curve_fit
from random import sample
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.stats import norm, gaussian_kde
import random
from tools.data_elt import sql_query_to_df, xlsx_to_df, load_yaml, csv_to_df, measurement_hour_conversion
from tools.data_elt import sql_query_to_df_all_accounts, sql_query_to_df_ma_all_accounts, \
    lms_df, gallifrey_ini_to_df, calibration_to_df, calibration_comparison, calculate_r1, \
        calculate_baseline_radius
import os
from datetime import datetime
import logging
import itertools

logger = logging.getLogger(__name__)

DATA_LOADING_DICT = {'sql': sql_query_to_df,
                     'xlsx': xlsx_to_df,
                     'csv': csv_to_df,
                     'sqlall': sql_query_to_df_all_accounts,
                     'sqlallma': sql_query_to_df_ma_all_accounts,
                     'ini': gallifrey_ini_to_df,
                     'calibration': calibration_to_df,
                     'calibration_comparison': calibration_comparison}

CONFIG_PATH = os.path.join(utl.get_project_root(), 'configs')
FIG_PATH = os.path.join(utl.get_project_root(),  'figures')
LOG_PATH = os.path.join(utl.get_project_root(),  'log')
LMS_PATH = os.path.join(utl.get_project_root(), 'lms')

class GoldenData():

    def __init__(self, **kwargs):
        
        self._datasource_type = kwargs['datasource_type']
        if kwargs['settings_file']:
            self.__load_data_config(CONFIG_PATH, kwargs['settings_file'], **kwargs)
        else:
            self.__load_data_config(CONFIG_PATH, 'system_settings.yml', **kwargs)

        self.__data_elt()
        self._jobIds = self.__get_jobIds()
        self._datameans = self.prescription_means()
        
    def __load_data_config(self, config_path, config_file, **kwargs):
        
        try:
            config = load_yaml(config_path, config_file)
        except OSError as e:
            logger.error(f'OSError: {e}')
        self._data_loading_dict = kwargs
        try:
            self._columns_dict = config['columns_mapping']
        except KeyError as e:
            logger.error(f'KeyError: {e}')
        self._data_loading_dict['worksheet_names'] = config['golden_worksheets']
        
    def __data_elt(self):
        
        try:
            self._data = DATA_LOADING_DICT[self._datasource_type](**self._data_loading_dict)['data']
        except Exception as e:
            logger.error(f'Exception: {e}')
        
    def __get_jobIds(self):
        """
        Creates a list of unique job ids
        
        Returns
        ---------
        list(str)
            A list of JobId strings
            
        """
        od_str = "OculusDexter"
        worksheet = self._data_loading_dict['worksheet_names'][0]
        job_ids = self.data[od_str][worksheet].index
        
        return job_ids
        
        
    def prescription_means(self):
        """
        Calculate mean values for each prescriptive component for each job id
        ---------
        goldenstandard
        
        Returns
        ---------
        prescription_means: dict
            Dictionary of DataFrames with mean values for each prescriptive component, by unique Job ID
            
        """
        od_str = 'OculusDexter'
        os_str = 'OculusSinister'
        
        
        indices = self.jobIds
        columns = list(self.columns_dict.keys())
        
        users = list(self.data[od_str].keys())

        golden_means = \
            {od_str: pd.DataFrame(data=np.zeros(shape=(len(indices), len(columns))), index=indices, columns=columns),\
             os_str: pd.DataFrame(data=np.zeros(shape=(len(indices), len(columns))), index=indices, columns=columns)}

        for index in indices:
            for column in columns:
                golden_means[od_str].loc[index, column] = \
                    np.mean([self.data[od_str][user].loc[index,column] for user in users])
                golden_means[os_str].loc[index, column] = \
                    np.mean([self.data[os_str][user].loc[index,column] for user in users])
            golden_means[od_str].loc[index, 'Lens Type'] = self.data[od_str][users[0]].loc[index,'Lens Type']
            golden_means[os_str].loc[index, 'Lens Type'] = self.data[os_str][users[0]].loc[index,'Lens Type']
                    
        return golden_means
        
    @property
    def data(self):
        """
        A property containing data in suitable format for validation study

        Returns
        -------
        dict / df
            data in suitable format for validation study
        """
        return self._data
    
    @property
    def datameans(self):
        """
        A property containing mean values of all prescriptive components by JobID

        Returns
        -------
        df
            DataFrame with mean values for each prescriptive component, by unique Job ID
        """
        return self._datameans
    
    @property
    def jobIds(self):
        """
        A property containing list of JobIds in sample data

        Returns
        -------
        list(str)
            A list of JobId strings
        """
        return self._jobIds
    
    @property
    def columns_dict(self):
        """
        A property containing the columns used for validation study
        keys: column names in golden data
        values: column names in sample data

        Returns
        -------
        dict
            A dictionary of column names
        """
        return self._columns_dict
    
class DataSample():
    """
    A class to manage the sample data and difference between sample and gold standard
    incoming: data sample, gold standard
    what we want to compute: population graphs, mean calculation, standard deviation, difference populations
    
    """
    
    def __init__(self, golden_data=None, extra_db_dict=None, **kwargs):
        
        self._datasource_type = kwargs['datasource_type']
        if self._datasource_type != 'calibration':
            self.__load_data_config(CONFIG_PATH, 'system_settings.yml', **kwargs)
        self.__data_elt()
        self.__get_jobIds()
        self._comparison_data = golden_data
        self.__db_extra_data_append(CONFIG_PATH, extra_db_dict)
        self.__prescription_diffs()
       

    def __load_data_config(self, config_path, config_file, **kwargs):
        """
        Loads config file for data loading settings
        
        Returns
        ---------
        void
            
        """
       
        self._config_dict = load_yaml(config_path, config_file)
        self._columns_mapping = self._config_dict['columns_mapping']
        self._tolerances = load_yaml(config_path, self._config_dict['tolerances_dict'])
        if self._datasource_type == 'sql':
            self._data_loading_dict = load_yaml(config_path, self._config_dict['sql_config'])
            self._data_loading_dict.update(kwargs)
            self._data_loading_dict['data_labels'] = load_yaml(config_path, self._config_dict['db_to_portal_labels'])
            self._data_loading_dict['columns_mapping'] = self._config_dict['columns_mapping']
        elif self._datasource_type == 'csv':
            self._data_loading_dict = kwargs
            self._data_loading_dict['columns_mapping'] = self._config_dict['columns_mapping']
        elif self._datasource_type == 'ini':
            self._data_loading_dict = kwargs
            self._data_loading_dict.update(load_yaml(config_path, self._config_dict['ini_config']))
        else:
            e = 'Failed to load data.  Datasource type must be either "sql" or "csv" or "ini"'
            logger.error(e)
    
    def __data_elt(self):
        """
        Exports, loads and transforms data from source
        
        Returns
        ---------
        void
            
        """

        data = DATA_LOADING_DICT[self._datasource_type](**self._data_loading_dict)
    
        self._data = data['data']
        
        if 'start_date' and 'end_date' in list(data.keys()):
            self._start_date = measurement_hour_conversion(data['start_date'], -1)
            self._end_date = measurement_hour_conversion(data['end_date'], -1)
            self._end_date = self._end_date + pd.DateOffset(minutes=1)
            
        if 'jobids' in self._config_dict.keys():
            jobids = self._config_dict['jobids']
            
            for side in ['OculusDexter', 'OculusSinister']:
                self._data[side] = self._data[side].loc[jobids]
            
    
    def __db_extra_data_append(self, config_path, extra_db_dict):
        
        if extra_db_dict is not None:
            
            self._extra_data_loading_dict = load_yaml(config_path, self._config_dict['sql_extras'])
            self._extra_data_loading_dict['data_labels'] = load_yaml(config_path, self._config_dict['db_to_portal_labels'])
            self._extra_data_loading_dict.update(extra_db_dict)
            self._extra_data_loading_dict['start_date'] = self._start_date
            self._extra_data_loading_dict['end_date'] = self._end_date
            
            extra_data = DATA_LOADING_DICT['sql'](**self._extra_data_loading_dict)['data']
                 
            
            # convert measurement data to match xlsx
            extra_data['OculusDexter']['MeasurementDate'] = \
                measurement_hour_conversion(extra_data['OculusDexter']['MeasurementDate'], +1)
            extra_data['OculusSinister']['MeasurementDate'] = \
                measurement_hour_conversion(extra_data['OculusSinister']['MeasurementDate'], +1)
            
            self._extra_data = extra_data
            
            self._data['OculusDexter'] = pd.merge(self._data['OculusDexter'], \
                                    self._extra_data['OculusDexter'], how='left', on=['JobID', 'MeasurementDate'])
            
            self._data['OculusSinister'] = pd.merge(self._data['OculusSinister'], \
                                    self._extra_data['OculusSinister'], how='left', on=['JobID', 'MeasurementDate'])
            
            if extra_db_dict['lms']:
                self.__load_lms_data()
                
                
    def __load_lms_data(self):
        
        measurement_ids = self.data['OculusDexter']['mID'].unique()
        measurement_ids_lower = [m.lower() for m in measurement_ids]
        
        lms_data = lms_df(measurement_ids_lower, LMS_PATH, self._extra_data_loading_dict['data_labels'])
        
        self._lms_data = lms_data
        
        self._data['OculusDexter'] = pd.merge(self._data['OculusDexter'], self._lms_data['OculusDexter'], \
                                    how='left', left_index=True, right_index=True).drop_duplicates()
        self._data['OculusSinister'] = pd.merge(self._data['OculusSinister'], self._lms_data['OculusSinister'],\
                                    how='left', left_index=True, right_index=True).drop_duplicates()
            
        self._data['OculusDexter'].index.rename('JobID')
        self._data['OculusSinister'].index.rename('JobID')
                
    def __get_jobIds(self):
        """
        Creates a list of unique job ids
        
        Returns
        ---------
        list(str)
            A list of JobId strings
            
        """
        od_str = "OculusDexter"
        job_ids = self.data[od_str].index.tolist()
        unique_jobids = list(set(job_ids))
        self._jobIds =  unique_jobids
        
    def __prescription_diffs(self):
        """
        Calculate differences between golden standard data mean and data sample for each prescriptive component
       
        
        Returns
        ---------
        data_diffs: dict
            A dictionary containing arrays of differences between the two instances.
        
        """
       
        if self.comparison_data is None:
            self._datadiff = None
        else:
            od_str = "OculusDexter"
            os_str = "OculusSinister"
            if len(list(set(self.jobIds) - set(self.comparison_data.jobIds))) > 0:
                e = f"Job Ids {list(set(self.jobIds) - set(self.comparison_data.jobIds))} not in golden standard data, will be excluded from analysis"
                logger.warning(e)
            indices = list(set(self.jobIds).intersection(set(self.comparison_data.jobIds)))
            data_diffs = {column: [] for column in self.columns_mapping.keys()}
            
            for index in indices:
                # make dataframes consistent if 1 entry or multiple entries
                data_od_index_df = self.data[od_str].loc[index]
                data_os_index_df = self.data[os_str].loc[index]
                if isinstance(data_od_index_df, pd.Series):
                    data_od_index_df = pd.DataFrame([self.data[od_str].loc[index]], \
                            columns=list(self.columns_mapping.values()).append('MeasurementDate'))
                if isinstance(data_os_index_df, pd.Series):
                    data_os_index_df = pd.DataFrame([self.data[os_str].loc[index]], \
                            columns=list(self.columns_mapping.values()).append('MeasurementDate'))
                
                for repeat in np.arange(0, len(data_od_index_df)):
                    for column in self.columns_mapping.keys():
                        
                        d1 = pd.to_numeric(data_od_index_df.iloc[[repeat]][self.columns_mapping[column]], \
                                           errors='coerce').values[0]
                        d2 = pd.to_numeric(self.comparison_data.datameans[od_str].loc[index,column], \
                                           errors='coerce')
                        diff_od = d1 - d2
                        m_date_od = data_od_index_df.iloc[[repeat]]['MeasurementDate'].values[0]
                        if column == 'Axis':
                            if diff_od > 90:
                                diff_od = diff_od - 180
                            elif diff_od < -90:
                                diff_od = diff_od + 180
                        if not np.isnan(diff_od):
                            tol_check_od = self.__check_if_in_tolerance(index, od_str, column, diff_od)
                            # data_diffs[column].append((diff_od, index, od_str, m_date_od))
                            data_diffs[column].append((diff_od, index, od_str, m_date_od, tol_check_od))
                        d1 = pd.to_numeric(data_os_index_df.iloc[[repeat]][self.columns_mapping[column]], \
                                           errors='coerce').values[0]
                        d2 = pd.to_numeric(self.comparison_data.datameans[os_str].loc[index,column], \
                                           errors='coerce')
                        diff_os = d1 - d2
                        m_date_os = data_os_index_df.iloc[[repeat]]['MeasurementDate'].values[0]
                        if column == 'Axis':
                            if diff_os > 90:
                                diff_os = diff_os - 180
                            elif diff_os < -90:
                                diff_os = diff_os + 180
                        if not np.isnan(diff_os):
                            tol_check_os = self.__check_if_in_tolerance(index, os_str, column, diff_os)
                            data_diffs[column].append((diff_os, index, os_str, m_date_os, tol_check_os))
                            # data_diffs[column].append((diff_os, index, os_str, m_date_os))
            self._datadiff =  data_diffs
    
    def sample_representative_test(self, prescriptive_component):
        """
        Test if sample is representative of population of lenses
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component be assessed
        
        Returns
        ---------
        representative_test: bool
        
        """
        
    def __population_difference_mean(self):
        """
        Calculate the absolute mean of each prescriptive component difference (sample-comparison)
        ---------
        
        Returns
        ---------
        dict: dictionary of floats
            mean of each prescriptive component difference
        
        """
        prescription_diff_values = {}
        if self._datadiff is not None:
            for k in self._datadiff.keys():
                prescription_diff_values[k] = [i[0] for i in self._datadiff[k]]
                datadiff_means = population_mean(prescription_diff_values, absolute=True)
        else:
            datadiff_means = None
            
        return datadiff_means
            
        
    def __population_difference_std(self):
        """
        Calculate the std of each prescriptive component difference (sample-comparison)
        ---------
    
        
        Returns
        ---------
        dict: dictionary of floats
            std of each prescriptive component difference
        
        """
        
        prescription_diff_values = {}
        if self._datadiff is not None:
            for k in self._datadiff.keys():
                prescription_diff_values[k] = [i[0] for i in self._datadiff[k]]
                datadiff_std = population_std(prescription_diff_values)
        else:
            datadiff_std = None
            
        return datadiff_std
            
    def __check_if_in_tolerance(self, jobid, side, prescriptive_component, data_diff):
        
        # 1. get relevant golden standard mean value
        gs_value = self.comparison_data.datameans[side].loc[jobid, prescriptive_component]
        # 2. get lens type
        lens_type = self.comparison_data.datameans[side].loc[jobid, 'Lens Type']
        # 2. get tolerance dict for this band
        if prescriptive_component in self._tolerances[lens_type].keys():
            tol_bands = self._tolerances[lens_type][prescriptive_component]
            power_ref = tol_bands['power_ref']
            gs_value_for_tol = self.comparison_data.datameans[side].loc[jobid, power_ref]
            tol_type = None
            tol_value = None
            for key, value in tol_bands['bands'].items():
                if (abs(gs_value_for_tol) >= value['min']) & (abs(gs_value_for_tol) < value['max']):
                    tol_type = value['type']
                    tol_value = value['value']
            if tol_type:
                if tol_type == 'absolute':
                    if abs(data_diff) <= tol_value:
                        return True
                    else:
                        return False
                elif tol_type == 'percent':
                    if abs(data_diff) * 100 / abs(gs_value) <= tol_value:
                        return True
                    else:
                        return False
            else:
                return np.NAN
        else:
            return np.NAN
        
            
    def proportion_lenses_within_tolerance(self):
        """
        Calculate the proportion of lenses within tolerance for each prescriptive component
        ---------
        
        Returns
        ---------
        dict: dictionary of floats
            proportion of each prescriptive component difference within tolerance
        
        """
        proportion_within_tol = {}
        if self._datadiff is not None:
            for k in self._datadiff.keys():
                tols_bool_array = [i[4] for i in self._datadiff[k]]
                print(np.array(tols_bool_array).sum())
                proportion_within_tol[k] = np.array(tols_bool_array).sum() / len(tols_bool_array)
                
        return proportion_within_tol
        
    @property
    def columns_mapping(self):
        """
        A property containing the columns used for validation study
        keys: column names in golden data
        values: column names in sample data

        Returns
        -------
        dict
            A dictionary of column names
        """
        return self._columns_mapping
        
    @property
    def data(self):
        """
        A property containing sample data in suitable format for validation study

        Returns
        -------
        dict / df
            A dictionary / df of data in suitable format
        """
        return self._data
    
    @property
    def jobIds(self):
        """
        A property containing list of JobIds in sample data

        Returns
        -------
        list(str)
            A list of JobId strings
        """
        return self._jobIds
    
    @property
    def comparison_data(self):
        """
        A property containing associated golden standard data mean values in suitable format for validation study

        Returns
        -------
        dict / df
            A dictionary / df of data in suitable format
        """
        return self._comparison_data
    
    @property
    def datadiff(self):
        """
        A property containing differences between golden standard data in suitable format for validation study

        Returns
        -------
        dict / df
            A dictionary / df of data in suitable format
        """
        
        # if self._comparison_data is not None:
        #     self._datadiff = self.__prescription_diffs()
        
        return self._datadiff

    @property
    def datadiff_means(self):
        """
        A property containing means of differences for each prescriptive component

        Returns
        -------
        dict
            A dictionary prescriptive components and their mean difference values
        """
        # if self._datadiff is not None:
        self._datadiff_means = self.__population_difference_mean()
        
        return self._datadiff_means
    
    @property
    def datadiff_std(self):
        """
        A property containing std of differences for each prescriptive component

        Returns
        -------
        dict
            A dictionary prescriptive components and their std difference values
        """
        # if self._datadiff is not None:
        self._datadiff_std = self.__population_difference_std()
        
        return self._datadiff_std
    
    @property
    def tolerances(self):
        """
        A property containing tolerances for prescriptive components

        Returns
        -------
        dict
            A dictionary of prescriptive components and tolerances
        """
        
        return self._tolerances

class CalibrationData(DataSample):
    
    def __init__(self, golden_data=None, extra_db_dict=None, calibration_config=None, **kwargs):
        
        if calibration_config is None:
            self.__load_data_config(CONFIG_PATH, 'calibration_config.yml', **kwargs)
        else:
            self.__load_data_config(CONFIG_PATH, calibration_config, **kwargs)
        super().__init__(golden_data=None, extra_db_dict=None, **kwargs)
        if 'position0MMFromHome' in self._data_loading_dict['comparison_data'].keys():
            self.calculate_r1_r2()
        self.__load_comparison_data()
        self.__prescription_diffs()
        
    def __load_data_config(self, config_path, config_file, **kwargs):
        
        self._config_dict = load_yaml(config_path, config_file)
        self._columns_mapping = self._config_dict['columns_mapping']
        self._tolerances = load_yaml(config_path, self._config_dict['tolerances_dict'])
        self._data_loading_dict = kwargs
        if self._data_loading_dict['cali_type'] == 'ini':
            self._data_loading_dict.update(load_yaml(config_path, self._config_dict['ini_config']))
        self._data_loading_dict.update(self._config_dict)
        
    def __load_comparison_data(self):
        
        self._comparison_data_dict = self._config_dict['comparison_data']
        self._comparison_data_dict.update({'worksheet_names': self._config_dict['worksheet_names']})
        
        data = DATA_LOADING_DICT['calibration_comparison'](**self._comparison_data_dict)
    
        self._comparison_data = data
        
    def __prescription_diffs(self):
        """
        Calculate differences between comparison data and data sample for each prescriptive component
       
        
        Returns
        ---------
        data_diffs: dict
            A dictionary containing arrays of differences between the two instances.
        
        """
        if self.comparison_data is None:
            self._datadiff = None
        else:
            sides = ["OculusDexter", "OculusSinister"]
            indices = list(set(self.jobIds))
            
            data_diffs = {column: [] for column in self.columns_mapping.keys()}
            
            for indice in indices:
                for side in sides:
                    for column in self.columns_mapping.keys():
                        
                        # measured data
                        d1 = self.data[side].loc[indice, self.columns_mapping[column]]
                        # comparison data
                        if side == 'OculusDexter':
                            jobid = indice.partition('_')[0]
                        else:
                            if indice.count('_') == 2:
                                jobid = indice.partition('_')[2].partition('_')[0]
                            elif indice.count('_') == 1:
                                jobid = indice.partition('_')[0]
                        d2 = self.comparison_data[side].loc[jobid, column]
                        
                        diff = d1-d2
                        
                        data_diffs[column].append((diff, indice, side))
                        
        self._datadiff = data_diffs
        
    def __jobid_to_power(self, jobid, side):
        
        if side == 'OculusDexter':
            power = jobid.partition('_')[0]
        else:
            if jobid.count('_') == 2:
                power = jobid.partition('_')[2].partition('_')[0]
            elif jobid.count('_') == 1:
                power = jobid.partition('_')[0]
        
        if power.startswith('P'):
            power_int = int(power[1:])
        else:
            power_int = -int(power[1:])
        
        return power_int
        
    
    def get_error_power_df(self, side, prescriptive_component):
        
        error_power_df = pd.DataFrame(data={'error': [i[0] for i in self.datadiff[prescriptive_component] \
                                                      if i[2]==side],
                                            'id': [i[1] for i in self.datadiff[prescriptive_component] \
                                                   if i[2]==side]})
        error_power_df['power'] = error_power_df['id'].apply(self.__jobid_to_power, side=side)
        
        return error_power_df
    
    def plot_calibration_error(self,
                               side, 
                               prescriptive_component, 
                               dataset,  
                               units='D',
                               tolerance_lim=None,
                               fig_path=None,
                               fig_name=None):
        
        error_power_df = self.get_error_power_df(side, prescriptive_component)
        
        fig, ax = plt.subplots()

        ax.plot(error_power_df['power'], error_power_df['error'], '*')
        xlims = ax.get_xlim()
        if tolerance_lim:
            ax.hlines([0, -tolerance_lim, tolerance_lim],\
                     xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
        ax.set_ylabel(f'{prescriptive_component} Error ({units})')
        ax.set_xlabel(f'Lens Power (Sphere) (D)')
        ax.set_title(f'Calibration Lens Error {prescriptive_component} \n {side} {dataset}')
        if fig_path is not None:
           fig_name_to_use = f'Calibration Lens Error {prescriptive_component} {side} {dataset} {fig_name}.png'
           fig.savefig(os.path.join(fig_path, fig_name_to_use))
     
           
    def r1_r2_error_plots(self,
                          side,
                          dataset,
                          r_type,
                          fig_path=None,
                          fig_name=None):
        
        if r_type == 'r1':
            pos0label = 'Pos0r1'
            pos1label = 'Pos1r1'
        elif r_type == 'r2':
            pos0label = 'Pos0r2'
            pos1label = 'Pos1r2'
        fig, ax = plt.subplots()
        error_power_df_pos0 = self.get_error_power_df(side, pos0label)
        error_power_df_pos1 = self.get_error_power_df(side, pos1label)
        ax.plot(error_power_df_pos0['power'], error_power_df_pos0['error'], '*', label=pos0label)
        ax.plot(error_power_df_pos1['power'], error_power_df_pos1['error'], '*', label=pos1label)
        ax.set_ylabel('R1 error (pixels)')
        ax.set_xlabel('Lens Power (Sphere) (D)')
        ax.set_title(f'Calibration Lens Error {r_type} \n {side} {dataset}')
        ax.legend()
        if fig_path is not None:
           fig_name_to_use = f'Calibration Lens Error {r_type} {side} {dataset} {fig_name}.png'
           fig.savefig(os.path.join(fig_path, fig_name_to_use))
    
    
    def global_mean_error(self, absolute=False):
        """
        Calculate the global (absolute) mean of each prescriptive component difference (sample-comparison) 
        across all power classes
        ---------
        
        Returns
        ---------
        dict: dictionary of dictionaries
           absolute global mean of each prescriptive component difference, for each side
        
        """
        
        
        sides = ['OculusDexter', 'OculusSinister']
        global_mean_error_dict = {}
        
        for side in sides:
            global_mean_error_dict[side] = {}
            for prescriptive_component in self.datadiff.keys():
                error_power_df = self.get_error_power_df(side, prescriptive_component)
                if absolute:
                    global_mean_error_dict[side][prescriptive_component] = \
                        np.mean(np.abs(error_power_df['error']))
                else:
                    global_mean_error_dict[side][prescriptive_component] = \
                        np.mean(error_power_df['error'])
        
        return global_mean_error_dict
    
    def get_within_power_mean(self, absolute=True):
        
        """
        Calculate the (absolute) mean of each prescriptive component difference (sample-comparison) 
        within each power class
        ---------
        
        Returns
        ---------
        dict: dictionary of dictionaries of dfs
           absolute mean of each prescriptive component difference, for each side, grouped by power
        
        """
        sides = ['OculusDexter', 'OculusSinister']
        mean_power_error_dict = {}

        for side in sides:
            mean_power_error_dict[side] = {}
            for prescriptive_component in self.datadiff.keys():
                power_error = self.get_error_power_df(side, prescriptive_component)
                if absolute:
                    power_error['error'] = power_error['error'].apply(np.abs)
                mean_power_error = pd.DataFrame(pd.DataFrame(power_error.groupby('power')['error'].mean()))
                mean_power_error_dict[side][prescriptive_component] = mean_power_error
                
        return mean_power_error_dict
    
    def get_within_power_std(self):
        
        """
        Calculate the standard deviation of each prescriptive component difference (sample-comparison) 
        within each power class
        ---------
        
        Returns
        ---------
        dict: dictionary of dictionaries
           absolute mean of each prescriptive component difference, for each side
        
        """
        sides = ['OculusDexter', 'OculusSinister']
        std_power_error_dict = {}

        for side in sides:
            std_power_error_dict[side] = {}
            for prescriptive_component in self.datadiff.keys():
                power_error = self.get_error_power_df(side, prescriptive_component)
                std_power_error = pd.DataFrame(pd.DataFrame(power_error.groupby('power')['error'].std()))
                std_power_error_dict[side][prescriptive_component] = std_power_error
                
        return std_power_error_dict
    
    def get_within_power_range(self):
        
        """
        Calculate the range (max-min) of each prescriptive component difference (sample-comparison) 
        within each power class
        ---------
        
        Returns
        ---------
        dict: dictionary of dictionaries
           absolute mean of each prescriptive component difference, for each side
        
        """
        sides = ['OculusDexter', 'OculusSinister']
        power_error_range_dict = {}

        for side in sides:
            power_error_range_dict[side] = {}
            for prescriptive_component in self.datadiff.keys():
                power_error = self.get_error_power_df(side, prescriptive_component)
                
                power_error_max_min = power_error.groupby('power').agg({'error': ['min', 'max']})
                power_error_max_min['range'] = power_error_max_min['error', 'max'] - \
                    power_error_max_min['error', 'min']
                power_error_range = pd.DataFrame({'power': power_error_max_min.index,
                                   'error': power_error_max_min['range']})
                power_error_range.drop('power', axis=1, inplace=True)
                
                power_error_range_dict[side][prescriptive_component] = power_error_range
                
        return power_error_range_dict
    
    def get_class_stats_mean(self, stats_type):
        
        """
        Calculate the mean of each prescriptive component difference (sample-comparison) 
        stats, as calulated within power class
        ---------
        
        Returns
        ---------
        dict: dictionary of floats
            mean of each prescriptive component difference
        
        """
        if stats_type == 'mean absolute':
            stats_power_dict = self.get_within_power_mean(absolute=True)
        elif stats_type == 'mean':
            stats_power_dict = self.get_within_power_mean(absolute=False)
        elif stats_type == 'std':
            stats_power_dict = self.get_within_power_std()
        elif stats_type == 'range':
            stats_power_dict = self.get_within_power_range()
        
        sides = ['OculusDexter', 'OculusSinister']
        class_stats_mean_dict = {}

        for side in sides:
            class_stats_mean_dict[side] = {}
            for prescriptive_component in self.datadiff.keys():
               stats_mean = stats_power_dict[side][prescriptive_component].mean()
               class_stats_mean_dict[side][prescriptive_component] = stats_mean
        return class_stats_mean_dict
    
    def calculate_r1_r2(self):
        
        self.data['OculusDexter']['baselineRadius'] = self.data['OculusDexter'].apply(lambda row: \
            calculate_baseline_radius(row.MPBaselineDiameter, row.MPBaseGridsize, row.MPOCGridsize), axis=1)
        self.data['OculusSinister']['baselineRadius'] = self.data['OculusSinister'].apply(lambda row: \
            calculate_baseline_radius(row.MPBaselineDiameter, row.MPBaseGridsize, row.MPOCGridsize), axis=1)
        
        self.data['OculusDexter']['MPPos0r1'] = self.data['OculusDexter'].apply(lambda row: \
                                        calculate_r1(row.MPPos0m1, row.baselineRadius), axis=1)
        self.data['OculusSinister']['MPPos0r1'] = self.data['OculusSinister'].apply(lambda row: \
                                        calculate_r1(row.MPPos0m1, row.baselineRadius), axis=1)
        self.data['OculusDexter']['MPPos0r2'] = self.data['OculusDexter'].apply(lambda row: \
                                        calculate_r1(row.MPPos0m2, row.baselineRadius), axis=1)
        self.data['OculusSinister']['MPPos0r2'] = self.data['OculusSinister'].apply(lambda row: \
                                        calculate_r1(row.MPPos0m2, row.baselineRadius), axis=1)
            
        self.data['OculusDexter']['MPPos1r1'] = self.data['OculusDexter'].apply(lambda row: \
                                         calculate_r1(row.MPPos1m1, row.baselineRadius), axis=1)
        self.data['OculusSinister']['MPPos1r1'] = self.data['OculusSinister'].apply(lambda row: \
                                         calculate_r1(row.MPPos1m1, row.baselineRadius), axis=1)
        self.data['OculusDexter']['MPPos1r2'] = self.data['OculusDexter'].apply(lambda row: \
                                         calculate_r1(row.MPPos1m2, row.baselineRadius), axis=1)
        self.data['OculusSinister']['MPPos1r2'] = self.data['OculusSinister'].apply(lambda row: \
                                         calculate_r1(row.MPPos1m2, row.baselineRadius), axis=1)
        
    
class CalibrationComparison():
    
    def __init__(self, calibration_dict):
        
        self._calibration_dict = calibration_dict
        
    def plot_error_comparison(self, 
                              prescriptive_component,
                              side,
                              tolerance_lim=0.13,
                              fig_name=None,
                              fig_path=None):
        
        cmap = cm.get_cmap('tab10')
        
        fig, ax = plt.subplots()
        
        for i, (key, value) in enumerate(self._calibration_dict.items()):
            error_power_df = value.get_error_power_df(side, prescriptive_component)
            color = cmap(i)

            ax.plot(error_power_df['power'], error_power_df['error'], '*', color=color, label=key)

        xlims = ax.get_xlim()
        ax.hlines([0, -tolerance_lim, tolerance_lim],\
                 xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
        ax.set_ylabel(f'{prescriptive_component} Error (D)')
        ax.set_xlabel(f'Lens Power (Sphere) (D)')
        ax.set_title(f'Calibration Lens Error {prescriptive_component} \n {side} Comparison')
        ax.legend()
        
        if fig_path is not None:
            fig_name_to_use = f'Calibration Lens Error Comparison {prescriptive_component} {side} {fig_name}.png'
            fig.savefig(os.path.join(fig_path, fig_name_to_use))
            
            
    def plot_stats_comparison(self,
                              prescriptive_component,
                              side,
                              stats_type,
                              fig_name=None,
                              fig_path=None):
        
        cmap = cm.get_cmap('tab10')
        
        fig, ax = plt.subplots()
        
        mean_values = {}
        colors = {}
        for i, (cali_name, cali_data) in enumerate(self._calibration_dict.items()):
            
            if stats_type == 'Mean Absolute':
                error_power_stats_dict = cali_data.get_within_power_mean(absolute=True)
            elif stats_type == 'Standard Deviation':
                error_power_stats_dict = cali_data.get_within_power_std()
            elif stats_type == 'Mean':
                error_power_stats_dict = cali_data.get_within_power_mean(absolute=False)
            elif stats_type == 'Range (Max-Min)':
                error_power_stats_dict = cali_data.get_within_power_range()
                
            error_power_stats_specific = error_power_stats_dict[side][prescriptive_component]
            
            mean_values[cali_name] = np.mean(error_power_stats_specific['error'])
            
            colors[cali_name] = cmap(i)

            ax.plot(error_power_stats_specific.index, error_power_stats_specific['error'], \
                    '*', color=colors[cali_name], label=cali_name)
        
        xlims = ax.get_xlim()
        ax.hlines([mean_value for mean_value in mean_values.values()],\
                 xlims[0],xlims[1], linestyles='--', colors=[color for color in colors.values()], alpha=0.5)
        ax.set_ylabel(f'{prescriptive_component} Error {stats_type} (D)')
        ax.set_xlabel(f'Lens Power (Sphere) (D)')
        ax.set_title(f'Calibration Lens Error {stats_type} \n {prescriptive_component} {side} Comparison')
        ax.legend()
        
        if fig_path is not None:
            fig_name_to_use = f'Calibration Lens Error Comparison {stats_type} {prescriptive_component} {side} {fig_name}.png'
            fig.savefig(os.path.join(fig_path, fig_name_to_use))
    
class StatsTest():
    """
    A class to manage statistical tests
    incoming: DataSample1, DataSample2
    
    """

    def __init__(self, data1, data2=None):

        self._data1 = data1
        self._data2 = data2
        self.__load_data_config(CONFIG_PATH, 'system_settings.yml')
        self._test_dict = {'Paired T-Test': self.__paired_t_non_equivalence,
                            'Two Samples T-Test': self.__two_samples_t_non_equivalence,
                           'Single Sample Golden Standard T-Test': self.__single_sample_t_non_equivalence
                           }
        self._test_options = list(self._test_dict.keys())
        self.__get_paired_data()
        self._statistical_result = None
        self._results_table = None
        self._trim = False
        
    def __load_data_config(self, config_path, config_file):
        
        config = load_yaml(config_path, config_file)
        self._tolerances = config['tolerances']
        self._allow_trim = config['trim']
        self._significance = config['significance']
        self._power = config['power']
        self._test_type = config['test_type']
        self._sample_size = config['sample_size']
    
    def __get_paired_data(self):
        """
        Get the paired results from data1 and data2, grouped by JobId and Lens location
        ---------
        
        Returns
        ---------
        data_paired: dict
            Dictionary of paired results for each prescriptive component, with tuples for each jobid
            (diff, d1, d2, jobid, side)
        
        """
        
        if self.data2 is None:
            self._data_paired = None
        else:
            od_str = "OculusDexter"
            os_str = "OculusSinister"
            data1 = self.data1.data
            data2 = self.data2.data
            indices = list(set(self.data1.jobIds).intersection(set(self.data2.jobIds)))
            if len(indices) < 60:
                logger.warning(f'Only {len(indices)} paired samples provided.  Test is designed for larger data samples (at least 60)')
            data_paired = {column: [] for column in self.data1.columns_mapping.keys()}
    
            for index in list(indices):
                if (index not in data1[od_str].index) or (index not in data1[os_str].index):
                    logger.warning('Job Id {index} only has one side measured, will not be included in analysis')
                    continue
                if index not in data2[od_str].index:
                    logger.warning('Job Id {index} only present in one data sample, will not be included in analysis')
                    continue
                data1_od_index_df = data1[od_str].loc[index]
                data1_os_index_df = data1[os_str].loc[index]
                
                data2_od_index_df = data2[od_str].loc[index]
                data2_os_index_df = data2[os_str].loc[index]
                if isinstance(data1_od_index_df, pd.Series):
                    data1_od_index_df = pd.DataFrame([data1[od_str].loc[index]], \
                            columns=list(self.data1.columns_mapping.values()).append('MeasurementDate'))
                if isinstance(data1_os_index_df, pd.Series):
                    data1_os_index_df = pd.DataFrame([data1[os_str].loc[index]], \
                            columns=list(self.data1.columns_mapping.values()).append('MeasurementDate'))
                if isinstance(data2_od_index_df, pd.Series):
                    data2_od_index_df = pd.DataFrame([data2[od_str].loc[index]], \
                            columns=list(self.data1.columns_mapping.values()).append('MeasurementDate'))
                if isinstance(data2_os_index_df, pd.Series):
                    data2_os_index_df = pd.DataFrame([data2[os_str].loc[index]], \
                            columns=list(self.data1.columns_mapping.values()).append('MeasurementDate'))
                for repeat in np.arange(0, len(data2_od_index_df)):
                    if repeat < len(data1_od_index_df):           
                        for k, v in self.data1.columns_mapping.items():
                            od_d1 = pd.to_numeric(data1_od_index_df.iloc[repeat][v], errors='coerce')
                            od_d2 = pd.to_numeric(data2_od_index_df.iloc[repeat][v], errors='coerce')
                            od_diff = od_d2 - od_d1
                            if k == 'Axis':
                                if od_diff > 90:
                                    od_diff = od_diff - 180
                                elif od_diff < -90:
                                    od_diff = od_diff + 180
                            os_d1 = pd.to_numeric(data1_os_index_df.iloc[repeat][v], errors='coerce')
                            os_d2 = pd.to_numeric(data2_os_index_df.iloc[repeat][v], errors='coerce')
                            os_diff = os_d2 - os_d1
                            if k == 'Axis':
                                if os_diff > 90:
                                    os_diff = os_diff - 180
                                elif os_diff < -90:
                                    os_diff = os_diff + 180
                            tuple_od = (od_diff, od_d1, od_d2, index, od_str)
                            tuple_os = (os_diff, os_d1, os_d2, index, os_str)
                            if (np.isnan(os_diff) == False) and (np.isnan(od_diff) == False):
                                data_paired[k].extend([tuple_od, tuple_os])
    
            self._data_paired = data_paired
    
    def paired_population_plot(self, 
                               prescriptive_component,
                               data_units,
                               data1,
                               data2,
                               density=False,
                               xlims=None,
                               fig_path=None
                               ):
        """
        Plot the distribution of the paired difference between data1 and data2
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component to be plotted
        data_units: str
            units of prescriptive component
        data1: str
            name of 1st dataset (for plot title)
        data2: str
            name of 2nd dataset (for plot title)
        
        Returns
        ---------
        void
        
        """
        data = []
        if len(self.data_paired[f'{prescriptive_component}']) > 0:
            data = list(list(zip(*self.data_paired[f'{prescriptive_component}']))[0])
        n, bins, patches=plt.hist(data, bins=50,color='b',density=density)
      
        plt.title(f'{prescriptive_component} Paired Data Distribution ({data1} - {data2})')
    
        plt.xlabel(f'{prescriptive_component} Paired Difference {data_units}')
      
        plt.ylabel("Job count")
        
        if xlims is not None:
            plt.xlim(xlims)
        
        mu, sigma = norm.fit(data)
        best_fit_line = norm.pdf(bins, mu, sigma)
        if density:
            plt.plot(bins, best_fit_line, color='r')
            plt.ylabel("Job density")
        
        if fig_path is not None:
            file_directory=fig_path
            fig_name = f'{prescriptive_component} Paired Distribution {data1} {data2}'
            plt.savefig(file_directory+f'{fig_name}.png')
        plt.show()
        
        
    def main_sampled_paired_data_normal_distribution_test(self):
        
        data_paired_dictionary={k:[i[0] for i in v]  for k,v in self.data_paired.items()} #first value of each tuple obtained
        
        user_request=input('Would you like to random sample? (y/n): ')
        if user_request== 'y':
            
            sample_size=int(input('input random sample size:'))
            n=sample_size
        
            data_dict={k:(random.sample(v,n)) for k,v in  data_paired_dictionary.items()}
        else:
            data_dict=data_paired_dictionary
        
        ordered_dictionary={k:(shapiro(v)) for k,v in   data_dict.items()} #shapiro test on dictionary
        
        alpha = 0.05
       
        test_dictionary={k: (print('Sample looks Gaussian (fail to reject H0)')
                    if v[1] > alpha 
                    else print('Sample does not look Gaussian (reject H0)')) 
                for k,v in ordered_dictionary.items()}
          
        test_dictionary_results={k: ('true' if v[1] > alpha else 'false' )for k,v in ordered_dictionary.items()}

        return (test_dictionary_results)
        
    def run_statistical_test(self, sample=None, log=True):
        """
        Run selected statistical test on data
        ---------
        Parameters
        ---------
        sample: int
            size of sample to select (if None use full data)
        
        Returns
        ---------
        p_vals_dict: dict (float)
            Dictionary of p-values for each prescriptive component
        trim_dict: dict (list)
            Dictionary with lists of trimmed JobIds
        
        """
        results_df = {k: None for k in list(self.data1.columns_mapping.keys())}
        # trim_dict = {k: [] for k in list(self.data1.columns_mapping.keys())}
        
        if self.data_comparison_type == 'paired':
            results_df = self.__run_paired_test(sample)
        elif self.data_comparison_type == 'single':
            results_df = self.__run_single_sample_golden_comparison_test(sample)
            
        results_dict = {index: (True if results_df.loc[index]['p-value'] < self.significance else False) \
         for index in results_df.index}
        
        self._results_table = results_dict
        self._statistical_result = True
        for k, v in self._results_table.items():
            if not v:
                self._statistical_result = False
                break
        
        if log:
            self.__log_test_results(results_df)
            
        return results_df
    
    def __log_test_results(self, results_df):
        """
        Save results df to csv
        ---------
        Parameters
        ---------
        results_df: dataframe
            statistical results
        
        Returns
        ---------
        void
        
        """
        dateTimeNow = datetime.now()
        filename = f'Results_{dateTimeNow.year}_{dateTimeNow.month}_{dateTimeNow.day}-{dateTimeNow.hour}_{dateTimeNow.minute}_{dateTimeNow.second}.csv'
        results_df.to_csv(os.path.join(LOG_PATH, filename))
        
    
    def run_pilot_tests(self, runs, sample):
        """
        Runs chosen test on pilot data, runs randomly sampled datasets of size sample
        ---------
        Parameters
        --------
        runs: int
            number of runs of test
        sample: int
            size of each sample
 
        
        Returns
        ---------
        p_vals_df: df
            dataframe for test results for each prescriptive value
            test, trim_l, trim_u, sig_percent, n, r
        
        """
        
        columns = list(self.data1.columns_mapping.keys())
        results_dict = {f'{r}': None for r in np.arange(runs)}
        p_vals_df = pd.DataFrame(data=np.zeros(shape=(runs,len(columns))), columns=columns)
        # trim_dict = {k: [] for k in list(self.data1.columns_mapping.keys())}
        
        for r in np.arange(runs):
        
            if self.data_comparison_type == 'paired':
                results_df = self.__run_paired_test(sample)
                results_dict[f'{r}'] = results_df
                
            elif self.data_comparison_type == 'single':
                results_df = self.__run_single_sample_golden_comparison_test(sample)
                results_dict[f'{r}'] = results_df
                
        return results_dict
        
    def __run_paired_test(self, sample=None):
        """
        Run selected statistical test on paired data
        ---------
        Parameters
        ---------
        sample: int
            size of sample to select (if None use full data)
        
        Returns
        ---------
        test_results: df 
            DataFrame of p-values and effect for each prescriptive component
        trims: dict (list)
            Dictionary with lists of trimmed JobIds
        
        """
        result_cols = ['p-value', 'sample size', 'effect', 'effect for tolerance', 'trimmed jobs']
        results_index = [p for p in list(self.data1.columns_mapping.keys()) if p!='Sph Equiv']
        test_results = pd.DataFrame(data=np.zeros(shape=(len(results_index), len(result_cols))), \
                                    index=results_index, \
                                           columns=result_cols)
        test_results['trimmed jobs'] = test_results['trimmed jobs'].astype('object')
        # trimmed_jobs_dict = {p: None for p in list(self.data1.columns_mapping.keys())}
        
        data = self.data_paired
        
        # for each prescriptive component
        for k, v in list(self.data1.columns_mapping.items()): 
            if k == 'Sph Equiv':
                continue
            data_k = data[k]
            data_indices = np.arange(0, len(data_k))

            # sample
            if sample is not None:
                sample_indices = self.__rand_select_data(data_indices, sample)
                data_k = [data_k[i] for i in sample_indices]
            
            if len(data_k)<60:
                warning=f'Only {len(data_k)} samples available for {k}. Recommended to use at least 60'
                logger.warning(warning)
            # trim
            trim_jobs = []
            if self.allow_trim:
                data_diff_k = [pairs[0] for pairs in data_k]
                tol = self._tolerances[k]
                sample_size = self.sample_size
                trim_dict = self.__trim_data_for_required_effect(data_diff_k, tol, sample_size)
            
                if len(trim_dict['trimmed_indices']) > 0:
                    self._trim = True
                    warning=f'{k} test: some data is being trimmed'
                    logger.warning(warning)
                    trim_jobs = [data_k[i][3] for i in trim_dict['trimmed_indices']]
                data_k = [data_k[i] for i in trim_dict['data_indices_to_use']]
            # test
            d1 = [pairs[1] for pairs in data_k]
            d2 = [pairs[2] for pairs in data_k]
            d_diff = [pairs[0] for pairs in data_k]

            low = -self._tolerances[k]
            upp = self._tolerances[k]
           
            p_vals, low_stats, upp_stats = self._test_dict[self._test_type](d1, d2, low, upp)
            _,_,_,cohensd = self.__compute_cohend(d_diff)
    
            _,_,_,cohensd_exp = self.__compute_cohend(d_diff,diff=upp)
            test_results.loc[k,'p-value'] = p_vals
            test_results.loc[k,'effect'] = cohensd
            test_results.loc[k,'effect for tolerance'] = cohensd_exp
            test_results.loc[k,'sample size'] = len(d1)
            test_results.at[k,'trimmed jobs'] = trim_jobs
            
        return test_results
    
    def __run_single_sample_golden_comparison_test(self, sample=None):
        
        """
        Run selected statistical test on single sample data, comparison with golden standard
        ---------
        Parameters
        ---------
        sample: int
            size of sample to select (if None use full data)
        
        Returns
        ---------
        p_vals: dict (float)
            Dictionary of p-values for each prescriptive component
        trims: dict (list)
            Dictionary with lists of trimmed JobIds
        
        """
        result_cols = ['p-value', 'sample size', 'effect', 'effect for tolerance', 'trimmed jobs']
        results_index = [p for p in list(self.data1.columns_mapping.keys()) if p!='Sph Equiv']
        test_results = pd.DataFrame(data=np.zeros(shape=(len(results_index), len(result_cols))), \
                                    index=results_index, \
                                           columns=result_cols)
        test_results['trimmed jobs'] = test_results['trimmed jobs'].astype('object')
        # trimmed_jobs_dict = {p: None for p in list(self.data1.columns_mapping.keys())}
        
        data = self.data1.datadiff
        
        # for each prescriptive component
        for k, v in list(self.data1.columns_mapping.items()):
            if k == 'Sph Equiv':
                continue

            data_k = data[k]
            data_indices = np.arange(0, len(data_k))

            # sample
            if sample is not None:
                sample_indices = self.__rand_select_data(data_indices, sample)
                data_k = [data_k[i] for i in sample_indices]
            
            if len(data_k)<60:
                warning=f'Only {len(data_k)} samples available for {k}. Recommended to use at least 60'
                logger.warning(warning)
            # trim
            trim_jobs = []
            
            if self.allow_trim:
                datadiff_k = [d[0] for d in data_k]
                tol = self._tolerances[k]
                sample_size = self.sample_size
                trim_dict = self.__trim_data_for_required_effect(datadiff_k, tol, sample_size)
                
                if len(trim_dict['trimmed_indices']) > 0:
                    self._trim = True
                    warning=f'{k} test: some data is being trimmed'
                    logger.warning(warning)
                    trim_jobs = [data_k[i][1] for i in trim_dict['trimmed_indices']]
                data_k = [data_k[i] for i in trim_dict['data_indices_to_use']]
                # print(trim_dict)
            
            d1 = [d[0] for d in data_k]
                
            low = -self._tolerances[k]
            upp = self._tolerances[k]
           
            p_vals, low_stats, upp_stats = self._test_dict[self._test_type](d1, None, low, upp)
            _,_,_,cohensd = self.__compute_cohend(d1)
    
            _,_,_,cohensd_exp = self.__compute_cohend(d1,diff=upp)
            test_results.loc[k,'p-value'] = p_vals
            test_results.loc[k,'effect'] = cohensd
            test_results.loc[k,'effect for tolerance'] = cohensd_exp
            test_results.loc[k,'sample size'] = len(data_k)
            test_results.at[k,'trimmed jobs'] = trim_jobs
            
        return test_results
                
        
    def __run_two_sample_test(self, sample=None):
        """
        Run selected statistical test on two independent samples data
        ---------
        Parameters
        ---------
        sample: int
            size of sample to select (if None use full data)
        
        Returns
        ---------
        p_vals: dict (float)
            Dictionary of p-values for each prescriptive component
        trims: dict (list)
            Dictionary with lists of trimmed JobIds
        
        """
        result_cols = ['p-value', 'sample size', 'effect', 'effect for tolerance', 'trimmed jobs']
        results_index = [p for p in list(self.data1.columns_mapping.keys())]
        test_results = pd.DataFrame(data=np.zeros(shape=(len(results_index), len(result_cols))), \
                                    index=results_index, \
                                           columns=result_cols)
        test_results['trimmed jobs'] = test_results['trimmed jobs'].astype('object')
        # trimmed_jobs_dict = {p: None for p in list(self.data1.columns_mapping.keys())}
        
        data = self.data1
    
    # def run_pilot_tests_paired(self, runs, n, alpha, test='paired_t_non_equivalence', trim=False, power=None):
    #     """
    #     Runs chosen test on pilot data, runs randomly sampled datasets of size n
    #     ---------
    #     Parameters
    #     --------
    #     runs: int
    #         number of runs of test
    #     n: int
    #         size of each sample
    #     alpha: float
    #         p-value significance threshold
    #     test: str
    #         test to use
    #     trim: bool
    #         trim data to meet std requirement?
    #     power: float
    #         (optional) statistical power if trimming data
        
        
    #     Returns
    #     ---------
    #     pilot_results: df
    #         dataframe for test results for each prescriptive value
    #         test, trim_l, trim_u, sig_percent, n, r
        
    #     """
    #     data = self.data_paired
        
    #     pilot_results = pd.DataFrame(index=\
    #                 [p for p in list(self.data1.columns_mapping.keys())], \
    #                     columns=['ds_trim_l', 'ds_trim_u', 'sig_percent'])
    #     trim_dict_cols = ['ds_trim_l', 'ds_trim_u', 'ds_s_rqd', 'ds_s_trim', 'sig_percent']
    #     trim_dict = {p: pd.DataFrame(data=np.zeros(shape=(runs,len(trim_dict_cols))), \
    #         columns=trim_dict_cols) \
    #                     for p in list(self.data1.columns_mapping.keys())}
        
    #     pvals_df =  pd.DataFrame(data=np.zeros(shape=(runs,len(list(self.data1.columns_mapping.keys())))), columns=\
    #                 [p for p in list(self.data1.columns_mapping.keys())])
            
    #     for k, v in list(self.data1.columns_mapping.items()):
    #         p_array = []
    #         ds1_l_array = []
    #         ds1_u_array = []
            
    #         d_diff_for_rand = [pairs[0] for pairs in data[k]]
            
    #         low = -self.tolerances[k]
    #         upp = self.tolerances[k]
    #         for r in np.arange(runs):
    #             # 1 get sampled data
    #             # print(n[k])
    #             data_indices = self.__rand_select_data(d_diff_for_rand, n)
    #             data_selected = [data[k][i] for i in data_indices]
    #             d_diff = [pairs[0] for pairs in data_selected]
    #             d_d1 = [pairs[1] for pairs in data_selected]
    #             d_d2 = [pairs[2] for pairs in data_selected]
    #             d1_trim_l = 0 
    #             d1_trim_u = 0
    #             trim_indices = np.arange(len(d_diff))
    #             s1_rqd = None
    #             s1_trim = np.NaN
                
    #             if trim:
                    
    #                 _, d1_trim_l, d1_trim_u, s1_rqd, s1_trim, s, trim_indices = \
    #                     self.__trim_data_for_required_effect(d_diff, upp, int(n[k]/2))
    #                 ds1_l_array.append(d1_trim_l)
    #                 ds1_u_array.append(d1_trim_u)
    #             else:
    #                 ds1_l_array.append(0)
    #                 ds1_u_array.append(0)
                
    #             p_val = \
    #                 self.test_dict[self.test_type]([d_d1[t] for t in trim_indices], [d_d2[t] for t in trim_indices], low, upp)
    #             p_array.append(p_val)
                
    #             trim_dict[k].iloc[r]['ds_trim_l'] = d1_trim_l
    #             trim_dict[k].iloc[r]['ds_trim_u'] = d1_trim_u
    #             trim_dict[k].iloc[r]['ds_s_rqd'] = s1_rqd
    #             trim_dict[k].iloc[r]['ds_s_trim'] = s1_trim
    #             pvals_df.loc[r][k] = p_val
            
    #         ds1_l_array = [0 if np.isnan(x) else x for x in ds1_l_array]
    #         ds1_u_array = [0 if np.isnan(x) else x for x in ds1_u_array]
          
            
    #         pilot_results.loc[k]['ds_trim_l'] = np.mean(ds1_l_array)
    #         pilot_results.loc[k]['ds_trim_u'] = np.mean(ds1_u_array)
                
    #         pilot_results.loc[k]['sig_percent'] = sum(p_v < alpha for p_v in p_array)
                
    #     return pilot_results, trim_dict, pvals_df
    
    def run_pilot_tests_two_samples(self, dataset, runs, n, alpha, test='two_samples_t_non_equivalence', trim=False, power=None):
        """
        Runs chosen test on pilot data, randomly sampled into 2 datasets of size n
        ---------
        Parameters
        ---------
        dataset: str
            'data1' or 'data2'
        runs: int
            number of runs of test
        n: int
            size of each sample
        alpha: float
            p-value signficance threshold
        test: str
            test to use
        trim: bool
            trim data to meet std requirement?
        power: float
            (optional) statistical power if trimming data
        
        
        Returns
        ---------
        pilot_results: df
            dataframe for test results for each prescriptive value
            test, trim_l, trim_u, sig_percent, n, r
        
        """
        
        if dataset == 'data1':
            data = self.data1
        elif dataset == 'data2':
            data = self.data2
        
        pilot_results = pd.DataFrame(index=\
                    [p for p in list(data.columns_dict.keys())], \
                        columns=['ds1_trim_l', 'ds1_trim_u', 'ds2_trim_l', 'ds2_trim_u', 'sig_percent'])
            
        trim_dict_cols = ['ds1_trim_l', 'ds1_trim_u', 'ds1_s_rqd', 'ds1_s_trim',\
                 'ds2_trim_l', 'ds2_trim_u', 'ds2_s_rqd', 'ds2_s_trim', 'sig_percent']
        
        trim_dict = {p: pd.DataFrame(data=np.zeros(shape=(runs,len(trim_dict_cols))), \
            columns=trim_dict_cols) \
                        for p in list(data.columns_dict.keys())}
            
        pvals_df =  pd.DataFrame(data=np.zeros(shape=(runs,len(list(self.data1.columns_dict.keys())))), columns=\
                    [p for p in list(self.data1.columns_dict.keys())])
            
        for p in list(data.columns_dict.keys()):
            p_array = []
            ds1_l_array = []
            ds1_u_array = []
            ds2_l_array = []
            ds2_u_array = []
            
            low = -self.data1.tolerances[p]
            upp = self.data1.tolerances[p]
            data_list = data.datadiff[p]
            for r in np.arange(runs):
                # 1 get sampled data
                indices = self.__rand_select_data(data_list, n[p])
                d1 = [data_list[i] for i in indices[:int(n[p]/2)-1]]
                d2 = [data_list[i] for i in indices[int(n[p]/2):-1]]
                d1_trim_l = 0
                d1_trim_u = 0
                d2_trim_l = 0
                d2_trim_u = 0
                s1_rqd = np.NAN
                s1_trim = None
                s2_rqd = np.NAN
                s2_trim = None
                    
                if trim:
                    
                    d1, d1_trim_l, d1_trim_u, s1_rqd, s1_trim, s, _ = \
                        self._trim_data_for_required_effect(d1, upp, int(n[p]/2), power, alpha)
                    d2, d2_trim_l, d2_trim_u, s2_rqd, s2_trim, s, _ = \
                        self._trim_data_for_required_effect(d2, upp, int(n[p]/2), power, alpha)
                    ds1_l_array.append(d1_trim_l)
                    ds1_u_array.append(d1_trim_u)
                    ds2_l_array.append(d2_trim_l)
                    ds2_u_array.append(d2_trim_u)
                else:
                    ds1_l_array.append(0)
                    ds1_u_array.append(0)
                    ds2_l_array.append(0)
                    ds2_u_array.append(0)
                
                p_val = self.test_dict[test](d1, d2, low, upp)
                p_array.append(p_val)
                
                trim_dict[p].iloc[r]['ds1_trim_l'] = d1_trim_l
                trim_dict[p].iloc[r]['ds1_trim_u'] = d1_trim_u
                trim_dict[p].iloc[r]['ds2_trim_l'] = d2_trim_l
                trim_dict[p].iloc[r]['ds2_trim_u'] = d2_trim_u
                trim_dict[p].iloc[r]['ds1_s_rqd'] = s1_rqd
                trim_dict[p].iloc[r]['ds1_s_trim'] = s1_trim
                trim_dict[p].iloc[r]['ds2_s_rqd'] = s2_rqd
                trim_dict[p].iloc[r]['ds2_s_trim'] = s2_trim
                
                pvals_df.iloc[r][p] = p_val
            
            ds1_l_array = [0 if np.isnan(x) else x for x in ds1_l_array]
            ds1_u_array = [0 if np.isnan(x) else x for x in ds1_u_array]
            ds2_l_array = [0 if np.isnan(x) else x for x in ds2_l_array]
            ds2_u_array = [0 if np.isnan(x) else x for x in ds2_u_array]
            
            pilot_results.loc[p]['ds1_trim_l'] = np.mean(ds1_l_array)
            pilot_results.loc[p]['ds1_trim_u'] = np.mean(ds1_u_array)
            pilot_results.loc[p]['ds2_trim_l'] = np.mean(ds2_l_array)
            pilot_results.loc[p]['ds2_trim_u'] = np.mean(ds2_u_array)
                
            pilot_results.loc[p]['sig_percent'] = sum(p_v < alpha for p_v in p_array)
                
        return pilot_results, trim_dict, pvals_df
    
    def __rand_select_data(self, data, n):
        """
        Randomly selects n values from list and returns list of indices
        ---------
        Parameters
        ---------
        data: list(floats)
            data to sample from
        n: int
            number of samples
        
        Returns
        ---------
        indices_samplee: list(int)
            list of n randomly selected indices
        
        """
        
        indices_array = np.arange(len(data))
        indices_sample = sample(list(indices_array), n)
            
        return indices_sample
    
    def __paired_t_non_equivalence(self, d1, d2, low, upp):
        """
        Two sample t-test for non equivalence
        ---------
        Parameters
        ---------
        d1: list(float)
            list of data - first dataset
        d2: list(float)
            list of data - second dataset
        low: float
            lower interval for equivalence (low < m1-m2)
        upp: float
            upper interval for equivalence (m1-m2 < upp)
        Returns
        ---------
        p: float
            p-value for non equivalence
        
        """

        p, (t1, p1, df1), (t2, p2, df2) = ttost_paired(np.array(d1), np.array(d2), low=low, upp=upp)
        
        return p, (t1, p1, df1), (t2, p2, df2)
    
    def __two_samples_t_non_equivalence(self, d1, d2, low, upp):
        """
        Two sample t-test for non equivalence
        ---------
        Parameters
        ---------
        d1: list(float)
            list of data - first dataset
        d2: list(float)
            list of data - second dataset
        low: float
            lower interval for equivalence (low < m1-m2)
        upp: float
            upper interval for equivalence (m1-m2 < upp)
        Returns
        ---------
        p: float
            p-value for non equivalence
        
        """
        
        p, (t1, p1, df1), (t2, p2, df2) = ttost_ind(d1, d2, usevar='pooled', low=low, upp=upp)
        
        return p
    
    def __single_sample_t_non_equivalence(self, d1, d2, low, upp):
        """
        Single sample t-test for non equivalence
        ---------
        Parameters
        ---------
        d1: list(float)
            list of data - first dataset
        low: float
            lower interval for equivalence (low < m1)
        upp: float
            upper interval for equivalence (m1 < upp)
        Returns
        ---------
        p: float
            p-value for non equivalence
        
        """
        
        (t1, p1) = ztest(d1, x2=None, value=low, alternative='larger')
        (t2, p2) = ztest(d1, x2=None, value=upp, alternative='smaller')
        p = np.maximum(p1,p2)
        
        return p, (t1, p1), (t2, p2)
        
    
    def __trim_data_for_required_effect(self, data, tol, sample_size):
        """
        Trims a data array for a required effect, given fixed sample size, power and signficance
        ---------
        Parameters
        ---------
        data: list(float)
            data sample
        tols: float
            tolerance for mean difference
        sample_size: int
            selected sample size
        power: float
            statistical power
        alpha: float
            p-value signficance threshold
        
        Returns
        ---------
        
        data_trimmed: list(float)
            list of trimmed data values
        
        """
        
        trim_steps = np.arange(0.00,0.2,0.01)
        power = self.power
        alpha = self.significance
        
        analysis = TTestIndPower()
        cohens_d = analysis.solve_power(effect_size=None, power=power, nobs1=sample_size, ratio=1.0, alpha=alpha)
        
        # 1 Get standard deviation of data
        s, d, _ = self.__std_trim(data)
        
        # 2 Get required std for effect
        s_req = self.__s_for_fixed_effect(cohens_d, tol)
 
        d_skew = sts.skew(data, nan_policy='omit')
        trim_l_counter = 0
        trim_u_counter = 0
        data_trim = data
        data_indices = np.arange(len(data))
        trim_indices = data_indices
       
        s_trim = None
        if (s > s_req):
            #  - find direction of skew
            while (trim_l_counter+1 < len(trim_steps)) & (trim_u_counter+1 < len(trim_steps)):
                if d_skew < 0:
                    trim_l_counter += 1
                    s_trim, data_trim, trim_indices = self.__std_trim(data, trim_steps[trim_l_counter], 'Lower')
                    d_skew = sts.skew(data_trim, nan_policy='omit')
                    if s_trim <= s_req:
                        break
                elif d_skew > 0:
                    trim_u_counter += 1
                    s_trim, data_trim, trim_indices = self.__std_trim(data, trim_steps[trim_u_counter], 'Upper')
                    d_skew = sts.skew(data_trim, nan_policy='omit')
                    if s_trim <= s_req:
                        break
        
        trimmed_indices = list(set(data_indices)-set(trim_indices))
        
        trim_dict = {'data_trimmed': data_trim, 
                     'data_indices_to_use': trim_indices,
                     'trimmed_indices': trimmed_indices,
                     'trim_lower': trim_steps[trim_l_counter],
                     'trim_upper': trim_steps[trim_u_counter],
                     'std_original': s,
                     'std_required': s_req,
                     'std_trim': s_trim}
        
        return trim_dict
    
    def trim_data_for_required_effect(self, dataset, tols, sample_size, power, alpha):
        """
        Finds trim required for each prescriptive component to achieve desired effect
        ---------
        Parameters
        ---------
        dataset: str
            'data1' or 'data2' for two samples, 'paired' for paired test
        tols: dict(float)
            tolerances for mean difference for all prescriptive components
        sample_size: int
            selected sample size
        power: float
            statistical power
        alpha: float
            p-value signficance threshold
        
        Returns
        ---------
        study_design_df: dataframe
            statistical parameters and trim for all prescriptive components
        
        """
        study_design_df = pd.DataFrame(index=\
                    [p for p in list(self.data1.columns_dict.keys())], \
                    columns=['Tol', 'd', 's', 's_rqd', 's_trim', 'trim_lower', 'trim_upper'])
       
        if dataset == 'data1':
            data = self.data1.datadiff
        elif dataset == 'data2':
            data = self.data2.datadiff
        elif dataset == 'paired':
            data = self.data_paired
            
       
        analysis = TTestIndPower()
        cohens_d = analysis.solve_power(effect_size=None, power=power, nobs1=sample_size, ratio=1.0, alpha=alpha)
           
        for k in self.data1.columns_dict.keys():
           
            if dataset == 'paired':
                d_p = [d[0] for d in data[k]]
            else:
                d_p = data[k]
            
           
            _, trim_lower, trim_upper, s_req, s_trim, s, _ =\
                self._trim_data_for_required_effect(d_p, tols[k], sample_size, power, alpha)
               
            study_design_df.loc[k]['Tol'] = tols[k]
            study_design_df.loc[k]['d'] = cohens_d
            study_design_df.loc[k]['s'] = s
            study_design_df.loc[k]['s_rqd'] = s_req
            study_design_df.loc[k]['s_trim'] = s_trim
            study_design_df.loc[k]['trim_lower'] = trim_lower
            study_design_df.loc[k]['trim_upper'] = trim_upper
           
        return study_design_df
    
    def __s_for_fixed_effect(self, cohensd, diff):
        """
        Finds standard deviation for given cohen's d and difference in means
        ---------
        Parameters
        ---------
        cohensd: float
            given effect (cohen's d)
        diff: float
            difference in means for effect
        
        Returns
        ---------
        s: float
            std of for given effect and mean difference
        data_sort: list(float)
            data sorted in ascending order
        args_sort: list(int)
            indices of trimmed data
        
        """
        
        s = diff / cohensd
        
        return s
    
    def __std_trim(self, data, trim=None, alternative=None):
        """
        Finds standard deviation of data (optionally trimmed)
        ---------
        Parameters
        ---------
        data: list(float)
            data sample
        trim: float
            proportion of data to trim
        alterative: str
            'upper': trim from upper end, 'lower' trim from lower end
        
        Returns
        ---------
        std: float
            std of optionally trimmed data
        data_sort: list(float)
            data sorted in ascending order
        args_sort: list(int)
            indices of trimmed data
        
        """
        
        args_sort = np.argsort(data)
        data_sort = [data[a] for a in args_sort]
        
        if trim is not None:
            n = len(data_sort)
            g = int(n * trim)
            if alternative == 'Lower':
                data_sort = data_sort[g:]
                args_sort = args_sort[g:]
            elif alternative == 'Upper':
                data_sort = data_sort[:n-g]
                args_sort = args_sort[:n-g]
    
        std = np.std(data_sort)
        return std, data_sort, args_sort
    
    def minimum_sample_size(self, test, mean_diffs, alpha=0.05, power=0.8, trim=None, winsor=None):
        """
        Calculates minimum sample size for a given test, with given mean difference, statistical significance and
        power.  Option to trim / winsorize data
        ---------
        Parameters
        ---------
        test: str
            test type
        mean_diffs: dict(float)
            difference in mean values to be detected as an effect for each prescriptive component
        alpha: float
            statistical significance threshold
        power: float
            statistical power
        trim: tuple(float) 
            proportion of data to winsorize (upp,low)
        winsor: tuple(float) 
            proportion of data to winsorize (upp,low)
        Returns
        ---------
        sample_size_df: DataFrame
            sample size, effect and (pooled) std for each prescriptive component
        
        
        """
        results_cols = ['Sample size', 'Effect', 's', 's_1', 's_2']
        sample_size_df = pd.DataFrame\
            (data=np.zeros(shape=(len(list(self.data1.columns_dict.keys())),len(results_cols))),\
                                      columns=['Sample size', 'Effect', 's', 's_1', 's_2'],\
                                      index=[p for p in list(self.data1.columns_dict.keys())])
        
        for k, v in self.data1.columns_dict.items():
            
            if test == 'TwoSample':
                data1 = self.data1.datadiff[k]
                data2 = self.data2.datadiff[k]
            else:
                data1 = [dd[0] for dd in self.data_paired[k]]
                data2 = None
            trim_k = None
            if trim is not None:
                trim_k = trim[k]
            
                
            s, sd1, sd2, cohens_d = self.__compute_cohend(data1, mean_diffs[k], data2, trim_k)
            
            analysis = TTestIndPower()
            result = analysis.solve_power(cohens_d, power=power, nobs1=None, ratio=1.0, alpha=alpha)
            
            sample_size_df.loc[k]['Sample size'] = result
            sample_size_df.loc[k]['Effect'] = cohens_d
            sample_size_df.loc[k]['s'] = s
            sample_size_df.loc[k]['s_1'] = sd1
            sample_size_df.loc[k]['s_2'] = sd2
        
        return sample_size_df
    
    def __compute_cohend(self, data1, diff=None, data2=None, trim=None, winsor=None):
        """
        Calculates Cohens d effect for given data
        (optionally 2 datasets, or 1 dataset and a fixed mean difference)
        (option to trim or winsorize data)
        
        ---------
        Parameters
        ---------
        data1: list(float)
            first data array
        diff: float
            difference in mean values to be detected as an effect
        data2: list(float)
            second data array
        trim: tuple(float) 
            proportion of data to trim (upp,low)
        winsor: tuple(float) 
            proportion of data to winsorize (upp,low)
        Returns
        ---------
        s: float
            std of tranformed data (pooled if 2 datasets)
        sd1: float
            std of transformed data1
        sd2: float
            std of transformed data2
        cohens_d: float
            computed cohens d
        """
        if data2 is None:
            if winsor is not None:
                data1 = winsorize(data1, limits=winsor)
            elif trim is not None:
                data1 = [d for d in data1 if (d>trim[0]) & (d<trim[1])]
            
            s = np.std(data1)
            sd1 = s
            sd2 = np.NAN
        else:
            if winsor is not None:
                data1 = winsorize(data1, limits=winsor)
                data2 = winsorize(data2, limits=winsor)
            elif trim is not None:
                data1 = [d for d in data1 if (d>trim[0]) & (d<trim[1])]
                data2 = [d for d in data2 if (d>trim[0]) & (d<trim[1])]
            n1 = len(data1)
            n2 = len(data2)
       
            s1 = np.var(data1,ddof=1)
            s2 = np.var(data2,ddof=1)
       
            s = np.sqrt(((n1 - 1) * s1 + (n2 - 1) * s2) / (n1 + n2 - 2))
            sd1 = np.std(data1)
            sd2 = np.std(data2)
       
        u1 = np.mean(data1)
        if diff is None:
            if data2 is not None:
                diff = np.mean(data2)
            else:
                diff=-u1
        u2 = u1+diff
        cohens_d = (u1-u2)/s
            
        return s, sd1, sd2, cohens_d
    
    @property
    def data1(self):
        """
        A property containing sample data 1

        Returns
        -------
        Datasample
            A DataSample object for dataset 1
        """
        return self._data1
    
    @property
    def data2(self):
        """
        A property containing sample data 2

        Returns
        -------
        Datasample
            A DataSample object for dataset 2
        """
        return self._data2
    
    @property
    def data_paired(self):
        """
        A property containing paired dataset

        Returns
        -------
        Dict
            A dictionary of lists of tuples (diff, d1, d2) for each prescriptive component 
        """
        return self._data_paired
    
    @property
    def test_options(self):
        """
        A property containing a list of statistical test options

        Returns
        -------
        List(str)
            A list of statistical test options
        """
        return self._test_options
    
    @property
    def tolerances(self):
        """
        A property containing tolerances for prescriptive components

        Returns
        -------
        dict
            A dictionary of prescriptive components and tolerances
        """
        
        return self._tolerances
    
    @tolerances.setter    
    def tolerances(self, value):
        """
        Sets tolerances property for prescriptive components

        Parameters
        -------
        value: dict
            A dictionary of prescriptive components and tolerances
        """
        
        self._tolerances = value
        
    @property
    def allow_trim(self):
        """
        A property containing data trim boolean

        Returns
        -------
        bool
            A boolean indicating whether or not to trim data
        """
        
        return self._allow_trim
    
    @allow_trim.setter    
    def allow_trim(self, value):
        """
        Sets trim property

        Parameters
        -------
        value: bool 
            A boolean indicating whether or not to trim data
        """
        
        self._allow_trim = value
        
    @property
    def significance(self):
        """
        A property containing statistical test signficance threshold

        Returns
        -------
        float
            Statistical test signficance threshold
        """
        
        return self._significance
    
    @significance.setter    
    def significance(self, value):
        """
        Sets signficance property

        Parameters
        -------
        value: float 
            Statistical test signficance threshold
        """
        
        self._significance = value
        
    @property
    def power(self):
        """
        A property containing statistical test minimum power

        Returns
        -------
        float
            Statistical test minimum power
        """
        
        return self._power
    
    @power.setter    
    def power(self, value):
        """
        Sets power property

        Parameters
        -------
        value: float 
            Statistical test minimum power
        """
        
        self._power = value

    @property
    def test_type(self):
        """
        A property containing type of statistical test to use

        Returns
        -------
        str
            Statistical test specifier
        """
        
        return self._test_type
    
    @test_type.setter    
    def test_type(self, value):
        """
        Sets test_type property

        Parameters
        -------
        value: str 
            Statistical test specifier
        """
        
        self._test_type = value
    
    @property
    def data_comparison_type(self):
        """
        A property containing type of data comparison

        Returns
        -------
        str
            Data comparison type
        """
        if self.test_type.startswith('Paired'):
            return 'paired'
        elif self.test_type.startswith('Single'):
            return 'single'
        
    @property
    def sample_size(self):
        """
        A property indicating desired sample size (for data trimming)

        Returns
        -------
        int
            Desired sample size
        """
        
        return self._sample_size
        
    @property
    def statistical_result(self):
        """
        A property containing the statistical test results boolean

        Returns
        -------
        bool
            Combined statistical test results: True / False
            True if all components passed test, False otherwise
        """
        return self._statistical_result
    
    @property
    def trim(self):
        """
        A property containing boolean for whether any data has been trimmed

        Returns
        -------
        bool
            A boolean indicating whether or not data has been trimmed
        """
        
        return self._trim
    
    @property
    def results_table(self):
        """
        A property containing the statistical test results

        Returns
        -------
        dict
            Dictionary of prescripive components with True / False values for Pass / Fail
        """
        return self._results_table
    
    
class DataPlots():
    
    """
    A class to manage datasample plots
    incoming: DataSample1
    
    """

    def __init__(self, datasample):

        self._datasample = datasample
    
    def prescription_population_plot(self, prescriptive_component):
        """
        Plot the distribution of the prescriptive component in the sample data
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component to be plotted
        
        Returns
        ---------
        void
        
        """

    def correlated_power_plot(self, 
                              prescriptive_component1,
                              prescriptive_component2,
                              data_units1,
                              data_units2='D',
                              singleline_plot=False,
                              prescriptive_component2_tol=0.12,
                              fit=None,
                              polyfit_deg=2,
                              fig_path=None, 
                              fig_name=None):
         
         """
         Plot one prescriptive component and the difference between golden data and another prescriptive component 
         of the data against either i) job number or ii) each other
        
         ---------
         Parameters
         ---------
         prescriptive_component1: str
             first prescriptive component to plot (power values)
         prescriptive_component2: str
             second prescriptive component to plot (power difference)
         data_units: str
             prescriptive_components units
         fig_path: str
             folder to save figures in
         fig_name: str
             figure name stem
         
         Returns
         ---------
         void
         
         """
         
         df_l = self.datasample.data['OculusDexter'][['MeasurementDate', prescriptive_component1]]
         print(df_l)
         df_l.loc[:,'Side'] = 'OculusDexter'
         df_l = df_l.rename_axis('JobId').reset_index()
         df_r = self.datasample.data['OculusSinister'][['MeasurementDate', prescriptive_component1]]
         df_r.loc[:,'Side'] = 'OculusSinister'
         df_r = df_r.rename_axis('JobId').reset_index()
         df_1 = df_l.append(df_r)

         diff_df_2 = pd.DataFrame(self.datasample.datadiff[prescriptive_component2], \
                                  columns=[prescriptive_component2, 'JobId', 'Side', 'MeasurementDate'])
         df = pd.merge(df_1, diff_df_2, how='left', on=['JobId', 'Side', 'MeasurementDate'])
         df = df.drop_duplicates()
         df = df.sort_values(prescriptive_component1, ignore_index=True)
         
         
         if not singleline_plot:
             fig, ax = plt.subplots()
             ax.plot(df.index, df[prescriptive_component1], '*', label=f'{prescriptive_component1}')
             ax.plot(df.index, df[prescriptive_component2], '*', \
                     label=f'{prescriptive_component2} Difference (Atlas-Golden Standard)')
             xlims = ax.get_xlim()
             ax.hlines([0, -prescriptive_component2_tol, prescriptive_component2_tol],\
                       xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
             ax.set_ylabel(f'Power {data_units1}')
             ax.set_xlabel('Job Number')
             ax.set_title('Power by Job Number')
             ax.legend()
             
             if fig_path is not None:
                 fig_name_to_use = f'{fig_name} {prescriptive_component1} {prescriptive_component2}.png'
                 fig.savefig(os.path.join(fig_path, fig_name_to_use))
                 # plt.close()
         else:
             fig, ax = plt.subplots()
             ax.plot(np.round(list(df[prescriptive_component1]),2), df[prescriptive_component2], '*')
             ax.set_ylabel(f'{prescriptive_component2} difference {data_units2}')
             ax.set_xlabel(f'{prescriptive_component1} {data_units1}')
             ax.set_title(f'{prescriptive_component2} Difference vs {prescriptive_component1}')
             xlims = ax.get_xlim()
             ax.hlines([0, -prescriptive_component2_tol, prescriptive_component2_tol], \
                       xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
             if fit == 'poly':
                 x = df[prescriptive_component1].to_numpy()
                 y = df[prescriptive_component2].to_numpy()
                 indices_nonnan = np.nonzero(~np.isnan(x+y))
                 x = x[indices_nonnan]
                 y = y[indices_nonnan]
                 pf = Polynomial.fit(x, y, deg=polyfit_deg)
                 xx = np.linspace(np.min(x), np.max(x), 100)
                 ax.plot(xx, pf(xx), '--', color='r', alpha=0.5)
             elif fit =='expo':
                 x = df[prescriptive_component1].to_numpy()
                 y = df[prescriptive_component2].to_numpy()
                 indices_nonnan = np.nonzero(~np.isnan(x+y))
                 x = x[indices_nonnan]
                 y = y[indices_nonnan]
                 popt, pcov = curve_fit(utl.expo_fit, x, y)
                 xx = np.linspace(np.min(x), np.max(x), 100)
                 ax.plot(xx, utl.expo_fit(xx, *popt), '--', color='r', alpha=0.5)
             if fig_path is not None:
                 fig_name_to_use = f'{fig_name} {prescriptive_component1} {prescriptive_component2} fit_{fit}.png'
                 fig.savefig(os.path.join(fig_path, fig_name_to_use))
                 # plt.close()   
    
    def correlated_power_plot_gs(self, 
                              prescriptive_component1,
                              prescriptive_component2,
                              data_units1,
                              data_units2='D',
                              singleline_plot=False,
                              prescriptive_component2_tol=0.12,
                              fit=None,
                              polyfit_deg=2,
                              fig_path=None, 
                              fig_name=None):
         
         """
         Plot one prescriptive component golden standard and the difference between golden data and another prescriptive component 
         of the data against either i) job number or ii) each other
        
         ---------
         Parameters
         ---------
         prescriptive_component1: str
             first prescriptive component to plot (power values)
         prescriptive_component2: str
             second prescriptive component to plot (power difference)
         data_units: str
             prescriptive_components units
         fig_path: str
             folder to save figures in
         fig_name: str
             figure name stem
         
         Returns
         ---------
         void
         
         """
         
         df_l = pd.DataFrame(self.datasample.comparison_data.datameans['OculusDexter']\
                            [prescriptive_component1])
         df_l['Side'] = 'OculusDexter'
         # df_l.loc[:,'Side'] = 'OculusDexter'
         df_l = df_l.rename_axis('JobId').reset_index()
         df_r = pd.DataFrame(self.datasample.comparison_data.datameans['OculusSinister']\
                            [prescriptive_component1])
         df_r['Side'] = 'OculusSinister'
         df_r = df_r.rename_axis('JobId').reset_index()
         df_1 = df_l.append(df_r)

         diff_df_2 = pd.DataFrame(self.datasample.datadiff[prescriptive_component2], \
                                 columns=[prescriptive_component2, 'JobId', 'Side', 'MeasurementDate', 'Result'])
         df = pd.merge(df_1, diff_df_2, how='left', on=['JobId', 'Side'])
         df = df.drop_duplicates()
         # return df
         if prescriptive_component1 == prescriptive_component2:
             prescriptive_component1_col = f'{prescriptive_component1}_x'
             prescriptive_component2_col = f'{prescriptive_component2}_y'
         else:
             prescriptive_component1_col = prescriptive_component1
             prescriptive_component2_col = prescriptive_component2
         df = df.sort_values(prescriptive_component1_col, ignore_index=True)
         
         tol_results = [i[4] for i in self.datasample.datadiff[prescriptive_component2]]
         color_list = [color_test(i) for i in tol_results] 
         
         if not singleline_plot:
             fig, ax = plt.subplots()
             ax.plot(df.index, df[prescriptive_component1_col], '*', label=f'{prescriptive_component1}')
             ax.plot(df.index, df[prescriptive_component2_col], '*', \
                    label=f'{prescriptive_component2} Difference (Atlas-Golden Standard)')
             xlims = ax.get_xlim()
             ax.hlines([0, -prescriptive_component2_tol, prescriptive_component2_tol],\
                      xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
             ax.set_ylabel(f'Power {data_units1}')
             ax.set_xlabel('Job Number')
             ax.set_title('Power by Job Number')
             ax.legend()
           
             if fig_path is not None:
                fig_name_to_use = f'{fig_name} {prescriptive_component1} {prescriptive_component2}.png'
                fig.savefig(os.path.join(fig_path, fig_name_to_use))
                # plt.close()
         else:
             fig, ax = plt.subplots()
             
             ax.plot(np.round(list(df[df.Result == True][prescriptive_component1_col]),2),\
                     df[df.Result == True][prescriptive_component2_col], '*', color='b')
             ax.plot(np.round(list(df[df.Result == False][prescriptive_component1_col]),2),\
                     df[df.Result == False][prescriptive_component2_col], '*', color='r')
             ax.set_ylabel(f'{prescriptive_component2} difference {data_units2}')
             ax.set_xlabel(f'{prescriptive_component1} {data_units1}')
             ax.set_title(f'{prescriptive_component2} Difference vs {prescriptive_component1}')
             xlims = ax.get_xlim()
             ax.hlines([0, -prescriptive_component2_tol, prescriptive_component2_tol], \
                      xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
             if fit == 'poly':
                x = df[prescriptive_component1].to_numpy()
                y = df[prescriptive_component2].to_numpy()
                indices_nonnan = np.nonzero(~np.isnan(x+y))
                x = x[indices_nonnan]
                y = y[indices_nonnan]
                pf = Polynomial.fit(x, y, deg=polyfit_deg)
                xx = np.linspace(np.min(x), np.max(x), 100)
                ax.plot(xx, pf(xx), '--', color='r', alpha=0.5)
             elif fit =='expo':
                x = df[prescriptive_component1].to_numpy()
                y = df[prescriptive_component2].to_numpy()
                indices_nonnan = np.nonzero(~np.isnan(x+y))
                x = x[indices_nonnan]
                y = y[indices_nonnan]
                popt, pcov = curve_fit(utl.expo_fit, x, y)
                xx = np.linspace(np.min(x), np.max(x), 100)
                ax.plot(xx, utl.expo_fit(xx, *popt), '--', color='r', alpha=0.5)
             if fig_path is not None:
                fig_name_to_use = f'{fig_name} {prescriptive_component1} {prescriptive_component2} fit_{fit}.png'
                fig.savefig(os.path.join(fig_path, fig_name_to_use))
                # plt.close()   
    
    def correlated_power_plot_extra_data(self, 
                              comparison_col,
                              prescriptive_component2,
                              data_units1,
                              data_units2='D',
                              singleline_plot=False,
                              prescriptive_component2_tol=0.12,
                              fit=None,
                              polyfit_deg=2,
                              fig_path=None, 
                              fig_name=None):
        
        
        df_l = pd.DataFrame(self.datasample.data['OculusDexter']\
                           [comparison_col])
        df_l['Side'] = 'OculusDexter'
        # df_l.loc[:,'Side'] = 'OculusDexter'
        df_l = df_l.rename_axis('JobId').reset_index()
        df_r = pd.DataFrame(self.datasample.data['OculusSinister']\
                           [comparison_col])
        df_r['Side'] = 'OculusSinister'
        df_r = df_r.rename_axis('JobId').reset_index()
        df_1 = df_l.append(df_r)

        diff_df_2 = pd.DataFrame(self.datasample.datadiff[prescriptive_component2], \
                                columns=[prescriptive_component2, 'JobId', 'Side', 'MeasurementDate', 'Result'])
        df = pd.merge(df_1, diff_df_2, how='left', on=['JobId', 'Side'])
        df = df.drop_duplicates()
        
        df[comparison_col] = df[comparison_col].apply(pd.to_numeric)
        df = df.sort_values(comparison_col, ignore_index=True)
        
        fig, ax = plt.subplots()
        
        ax.plot(np.round(list(df[df.Result == True][comparison_col]),2),\
                df[df.Result == True][prescriptive_component2], '*', color='b')
        ax.plot(np.round(list(df[df.Result == False][comparison_col]),2),\
                df[df.Result == False][prescriptive_component2], '*', color='r')
        ax.set_ylabel(f'{prescriptive_component2} difference {data_units2}')
        ax.set_xlabel(f'{comparison_col} {data_units1}')
        ax.set_title(f'{prescriptive_component2} Difference vs {comparison_col}')
        xlims = ax.get_xlim()
        ax.hlines([0, -prescriptive_component2_tol, prescriptive_component2_tol], \
                  xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
        if fit == 'poly':
            x = df[comparison_col].to_numpy()
            y = df[prescriptive_component2].to_numpy()
            indices_nonnan = np.nonzero(~np.isnan(x+y))
            x = x[indices_nonnan]
            y = y[indices_nonnan]
            pf = Polynomial.fit(x, y, deg=polyfit_deg)
            xx = np.linspace(np.min(x), np.max(x), 100)
            ax.plot(xx, pf(xx), '--', color='r', alpha=0.5)
        elif fit =='expo':
            x = df[comparison_col].to_numpy()
            y = df[prescriptive_component2].to_numpy()
            indices_nonnan = np.nonzero(~np.isnan(x+y))
            x = x[indices_nonnan]
            y = y[indices_nonnan]
            popt, pcov = curve_fit(utl.expo_fit, x, y)
            xx = np.linspace(np.min(x), np.max(x), 100)
            ax.plot(xx, utl.expo_fit(xx, *popt), '--', color='r', alpha=0.5)
        if fig_path is not None:
            fig_name_to_use = f'{fig_name} {comparison_col} {prescriptive_component2} fit_{fit}.png'
            fig.savefig(os.path.join(fig_path, fig_name_to_use))
    
    def population_difference_plot(self, data_name, data_units, fig_path=None, density=False, xlims=None):
        """
        Plot the distribution of the difference between golden data and prescriptive component of the data 
        (and norm fit?)
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component to be plotted
        
        Returns
        ---------
        void
        
        """
        data = []
        if len(self.datasample.datadiff[f'{data_name}']) > 0:
            data = list(list(zip(*self.datasample.datadiff[f'{data_name}']))[0])
        n, bins, patches=plt.hist(data, bins=50,color='b',density=density)
      
        plt.title(f'{data_name} Difference Distribution (Atlas - golden standard)')
    
        plt.xlabel(f'{data_name} Difference {data_units}')
      
        plt.ylabel("Job count")
        
        if xlims is not None:
            plt.xlim(xlims)
        
        mu, sigma = norm.fit(data)
        best_fit_line = norm.pdf(bins, mu, sigma)
        if density:
            plt.plot(bins, best_fit_line, color='r')
            plt.ylabel("Job density")
        
        if fig_path is not None:
            file_directory=fig_path
            plt.savefig(file_directory+f'{data_name}.png')
        plt.show()
        
    def bucketed_power_plots(self, 
                             power_type, 
                             data_units, 
                             power_tols=0.13,
                             plot_type='hist',
                             fig_path=None, 
                             fig_name=None, 
                             xlims=None):
        """
        Plot the distribution of the difference between golden data and prescriptive component of the data
        grouped according to power_type values
       
        ---------
        Parameters
        ---------
        power_type: str
            plots grouped by positive and negative power_type: either 'Sphere' or 'Sph Equiv'
        data_units: str
            prescription_type units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        xlims: tuple(float,float)
            xlims for plotting
        
        Returns
        ---------
        void
        
        """
        
        cmap = cm.get_cmap('tab10')
        
        data_units = 'D'
        data_to_use = []
        buckets = {'[0,4)': (0,4), '[4,6)': (4,6), '[6,8)': (6,8), '[8,20)': (8,20)}
        colors = {'[0,4)': 0, '[4,6)': 1, '[6,8)': 2, '[8,20)': 3}
        data_buckets = {'[0,4)': None, '[4,6)': None, '[6,8)': None, '[8,20)': None}
        
        for key, value in data_buckets.items():
            data_to_use = []
            for side in ['OculusSinister', 'OculusDexter']:
                datadiff = [(power,jobid) for (power,jobid,side,mdate) in self.datasample.datadiff[f'{power_type}'] if side == side]
            
                # get JobIDs for each power band
                data = self.datasample.comparison_data.datameans[side]
                jobids = data.index[(abs(data[power_type]) >= buckets[key][0]) & \
                                    (abs(data[power_type]) < buckets[key][1])].tolist()
            
                data_to_use = data_to_use + [power for (power,jobid) in datadiff if jobid in jobids]
            data_buckets[key] = data_to_use
            
            if plot_type=='hist':
                fig, ax = plt.subplots()
                label = f'{buckets[key][0]} < |{power_type}| <= {buckets[key][1]}'
                n, bins, patches=ax.hist(data_buckets[key], bins=50,color=cmap(colors[key]),density=False, label=label)
            
                ax.set_xlabel(f'{power_type} {data_units}')
          
                ax.set_ylabel("Job count")
                ax.legend()
                ax.set_title(f'{power_type} Difference Distribution (Atlas - golden standard)')
            
                if xlims is not None:
                    ax.set_xlim(xlims)
                    if fig_path is not None:
                        fig_path = os.path.join(fig_path, power_type)
                        fig_name_to_use = f'{fig_name} {power_type} {buckets[key]}.png'
                        fig.savefig(os.path.join(fig_path, fig_name_to_use))
        if plot_type == 'box':
            ylabel = f'{power_type} difference {data_units}'
            xlabel = f'{power_type} {data_units}'
            fig_name_to_use = f'{fig_name} {power_type} buckets.png'
            dictionary_boxplots(data_buckets, power_tols, ylabel, xlabel)
    
    def positive_negative_power_plots(self, 
                                      power_type, 
                                      prescription_type, 
                                      data_units, 
                                      fig_path=None, 
                                      fig_name=None, 
                                      xlims=None):
        """
        Plot the distribution of the difference between golden data and prescriptive component of the data
        grouped according to +ve and -ve power_type
       
        ---------
        Parameters
        ---------
        power_type: str
            plots grouped by positive and negative power_type: either 'Sphere' or 'Sph Equiv'
        prescription_type: str
            name of prescriptive component to be plotted
        data_units: str
            prescription_type units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        xlims: tuple(float,float)
            xlims for plotting
        
        Returns
        ---------
        void
        
        """
        cmap = cm.get_cmap('tab10')
        
        data_to_use = []
        buckets = {'a': '+ve', 'b': '-ve'}
        colors = {'a': 0, 'b': 1}
        data_buckets = {'a': None, 'b': None}

        fig, ax = plt.subplots()
        for key, value in data_buckets.items():
            data_to_use = []
            for side in ['OculusSinister', 'OculusDexter']:
                datadiff = [(power,jobid) for (power,jobid,side,date) in self.datasample.datadiff[f'{prescription_type}'] if side == side]
            
                # get JobIDs for each power band
                data = self.datasample.comparison_data.datameans[side]
                if buckets[key] == '+ve':
                    jobids = data.index[(data[power_type] >= 0)].tolist()
                else:
                    jobids = data.index[(data[power_type] < 0)].tolist()
                data_to_use = data_to_use + [power for (power,jobid) in datadiff if jobid in jobids]
            data_buckets[key] = data_to_use
            
            fig, ax = plt.subplots()
            label = f'{power_type} {buckets[key]}'
            n, bins, patches=ax.hist(data_buckets[key], bins=50,color=cmap(colors[key]),density=False, label=label)
            
            ax.set_xlabel(f'{prescription_type} Difference {data_units}')
          
            ax.set_ylabel("Job count")
            ax.legend()
            ax.set_title(f'{prescription_type} Difference Distribution (Atlas - golden standard)')
            
            if xlims is not None:
                ax.set_xlim(xlims)
            if fig_path is not None:
                fig_name_to_use = f'{fig_name} {prescription_type} {power_type} {buckets[key]}.png'
                fig.savefig(os.path.join(fig_path, fig_name_to_use))
                plt.close()
    
    def negative_bucketed_power_plots(self,
                                      power_type, 
                                      power_units,
                                      prescription_type,
                                      prescription_units,
                                      plot_type='hist',
                                      power_tols=0.33,
                                      fig_name=None, 
                                      fig_path=None, 
                                      xlims=None):
        """
        Plot the distribution of the difference between golden data and prescriptive component of the data
        grouped according to power_type values
       
        ---------
        Parameters
        ---------
        power_type: str
            plots grouped by positive and negative power_type: either 'Sphere' or 'Sph Equiv'
        prescription_type: str
            name of prescriptive component to be plotted
        data_units: str
            prescription_type units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        xlims: tuple(float,float)
            xlims for plotting
        
        Returns
        ---------
        void
        
        """
        
        cmap = cm.get_cmap('tab10')
        
        data_units = 'D'
        data_to_use = []
        buckets = {'[0,-2)': (0,-2), '[-2,-4)': (-2,-4), '[-4,-8)': (-4,-8), '[-8,-20)': (-8,-20)}
        colors = {'[0,-2)': 4, '[-2,-4)': 5, '[-4,-8)': 6, '[-8,-20)': 7}
        data_buckets = {'[0,-2)': None, '[-2,-4)': None, '[-4,-8)': None, '[-8,-20)': None}

        fig, ax = plt.subplots()
        fig_path = fig_path
        for key, value in data_buckets.items():
            data_to_use = []
            for side in ['OculusSinister', 'OculusDexter']:
                datadiff = [(power,jobid) for (power,jobid,side,time) in self.datasample.datadiff[f'{prescription_type}'] if side == side]
            
                # get JobIDs for each power band
                data = self.datasample.comparison_data.datameans[side]
                jobids = data.index[((data[power_type]) < buckets[key][0]) & \
                                    ((data[power_type]) >= buckets[key][1])].tolist()
            
                data_to_use = data_to_use + [power for (power,jobid) in datadiff if jobid in jobids]
            data_buckets[key] = data_to_use
            
            if plot_type == 'hist':
                fig, ax = plt.subplots()
                label = f'{buckets[key][1]} <= {power_type} < {buckets[key][0]}'
                n, bins, patches=ax.hist(data_buckets[key], bins=50,color=cmap(colors[key]),density=False, label=label)
                
                ax.set_xlabel(f'{prescription_type} Difference {data_units}')
              
                ax.set_ylabel("Job count")
                ax.legend()
                ax.set_title(f'{power_type} Difference Distribution (Atlas - golden standard)')
                
                if xlims is not None:
                    ax.set_xlim(xlims)
                
                if fig_path is not None:
                    fig_name_to_use = f'{fig_name} {prescription_type} {power_type} {buckets[key]}.png'
                    fig.savefig(os.path.join(fig_path, fig_name_to_use))
                    plt.close()
                    
        if plot_type == 'box':
            ylabel = f'{prescription_type} difference {prescription_units}'
            xlabel = f'{power_type} {power_units}'
            fig_name_to_use = f'{fig_name} {prescription_type} difference {power_type} buckets.png'
            
            dictionary_boxplots(data_buckets, power_tols, ylabel, xlabel, fig_name_to_use, fig_path)
    
    def correlated_power_diff_plot(self, 
                                   prescriptive_component1,
                                   prescriptive_component2,
                                   data_units,
                                   fig_path=None, 
                                   fig_name=None):
        
        """
        Plot the difference between golden data and prescriptive components of the data
        against job number
       
        ---------
        Parameters
        ---------
        prescriptive_component1: str
            first prescriptive component to plot
        prescriptive_component2: str
            second prescriptive component to plot
        data_units: str
            prescriptive_components units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        
        Returns
        ---------
        void
        
        """
        
        diff_df_1 = pd.DataFrame(self.datasample.datadiff[prescriptive_component1], \
                                 columns=[prescriptive_component1, 'JobId', 'Side', 'MDate'])
        diff_df_2= pd.DataFrame(self.datasample.datadiff[prescriptive_component2], \
                                columns=[prescriptive_component2, 'JobId', 'Side', 'MDate'])
        df = pd.merge(diff_df_1, diff_df_2, how='left', on=['JobId', 'Side', 'MDate'])
       
        df = df.drop_duplicates()
        df = df.sort_values('JobId', ignore_index=True)

        fig, ax = plt.subplots()
        ax.plot(df.index, df[prescriptive_component1], '*', label=prescriptive_component1)
        ax.plot(df.index, df[prescriptive_component2], '*', label=prescriptive_component2)
        ax.set_ylabel(f'Power difference {data_units}')
        ax.set_xlabel('Job Number')
        ax.set_title('Power Difference by Job Number (Atlas - golden standard)')
        ax.legend()
        
        if fig_path is not None:
            fig_name_to_use = f'{fig_name} {prescriptive_component1} {prescriptive_component2}.png'
            fig.savefig(os.path.join(fig_path, fig_name_to_use))
        return df['JobId']
    
    def attribute_vs_error_plot(self, 
                                attribute, 
                                attribute_units, 
                                prescriptive_component, 
                                error_units,
                                fig_path=None, 
                                fig_name=None):
        """
        Plot one prescriptive component difference against a given attribute
       
        ---------
        Parameters
        ---------
        attribute: str
            lens attribute to plot (x-axis)
        attribute_units: str
            lens attribute units
        prescriptive_component: str
            prescriptive component to plot difference (y-axis)
        error_units: str
            prescriptive_components units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        
        Returns
        ---------
        void
        
        """
        
        df_l = self.datasample.data['OculusDexter'][['MeasurementDate', attribute]]
        df_l.loc[:,'Side'] = 'OculusDexter'
        df_l = df_l.rename_axis('JobId').reset_index()
        df_r = self.datasample.data['OculusSinister'][['MeasurementDate', attribute]]
        df_r.loc[:,'Side'] = 'OculusSinister'
        df_r = df_r.rename_axis('JobId').reset_index()
        df_1 = df_l.append(df_r)
        
        diff_df_2 = pd.DataFrame(self.datasample.datadiff[prescriptive_component], \
                                 columns=[prescriptive_component, 'JobId', 'Side', 'MeasurementDate'])
            
        df = pd.merge(df_1, diff_df_2, how='left', on=['JobId', 'Side', 'MeasurementDate'])
        df = df.drop_duplicates()
        df = df.sort_values(attribute, ignore_index=True)
        
        fig, ax = plt.subplots()
        ax.plot(df[attribute], df[prescriptive_component],'*')
        plt.xticks(rotation=90, ha='right')
        ax.set_ylabel(f'{prescriptive_component} difference {error_units}')
        ax.set_xlabel(f'{attribute} ({attribute_units})')
        ax.set_title(f'{prescriptive_component} difference by {attribute}')
    
    def attribute_plot(self, 
                       attribute, 
                       attribute_units,
                       fig_path=None, 
                       fig_name=None):
        
        df_l = self.datasample.data['OculusDexter'][['MeasurementDate', attribute]]
        df_l.loc[:,'Side'] = 'OculusDexter'
        df_l = df_l.rename_axis('JobId').reset_index()
        df_r = self.datasample.data['OculusSinister'][['MeasurementDate', attribute]]
        df_r.loc[:,'Side'] = 'OculusSinister'
        df_r = df_r.rename_axis('JobId').reset_index()
        df_1 = df_l.append(df_r)
        
        df_1 = df_1.sort_values(attribute, ignore_index=True)
        
        fig, ax = plt.subplots()
        ax.plot(df_1.index, df_1[attribute])
        
    def correlated_power_plot_2_ax(self, 
                                   attribute,
                                   attribute_units,
                                   prescriptive_component,
                                   data_units,
                                   precriptive_tols=0.13,
                                   fig_path=None,
                                   fig_name=None):
        
        """
        Plot one attribute and the difference between golden data and a prescriptive component 
        of the data against job number on split axes
       
        ---------
        Parameters
        ---------
        attribute: str
            lens attribute to plot
        attribute_units: str
            lens attribute units
        prescriptive_component: str
            prescriptive component to plot (difference)
        data_units: str
            prescriptive_components units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        
        Returns
        ---------
        void
        
        """
        
        df_l = self.datasample.data['OculusDexter'][['MeasurementDate', attribute]]
        df_l.loc[:,'Side'] = 'OculusDexter'
        df_l = df_l.rename_axis('JobId').reset_index()
        df_r = self.datasample.data['OculusSinister'][['MeasurementDate', attribute]]
        df_r.loc[:,'Side'] = 'OculusSinister'
        df_r = df_r.rename_axis('JobId').reset_index()
        df_1 = df_l.append(df_r)

        diff_df_2 = pd.DataFrame(self.datasample.datadiff[prescriptive_component], \
                                 columns=[prescriptive_component, 'JobId', 'Side', 'MeasurementDate'])
        df = pd.merge(df_1, diff_df_2, how='left', on=['JobId', 'Side', 'MeasurementDate'])
        df = df.drop_duplicates()
        df = df.sort_values(attribute, ignore_index=True)
        
        data_dict = {'Job Number': list(df.index),
                     f'{attribute}': list(df[attribute]),
                     f'{prescriptive_component} Difference (Atlas-Golden Standard)': \
                         list(df[prescriptive_component])}
            
        data_units_dict = {'Job Number': None,
                     f'{attribute}': f'{attribute_units}',
                     f'{prescriptive_component} Difference (Atlas-Golden Standard)': \
                         f'{data_units}'}
        marker_dict = {f'{attribute}': None,
                         f'{prescriptive_component} Difference (Atlas-Golden Standard)': '*'}
        linetype_dict = {f'{attribute}': '-',
                         f'{prescriptive_component} Difference (Atlas-Golden Standard)': 'None'}
        
        two_line_plot_two_axes(data_dict, data_units_dict, linetype_dict, marker_dict, precriptive_tols)
    
    def attribute_attributediff_plot(self, 
                       attribute1, 
                       attribute2,
                       attribute3,
                       attribute1_units,
                       attribute2_units,
                       attribute3_units,
                       fig_path=None, 
                       fig_name=None):
        
        df_l = self.datasample.data['OculusDexter'][['MeasurementDate', attribute1, attribute2, attribute3]]
        df_l.loc[:,'Side'] = 'OculusDexter'
        df_l = df_l.rename_axis('JobId').reset_index()
        df_r = self.datasample.data['OculusSinister'][['MeasurementDate', attribute1, attribute2, attribute3]]
        df_r.loc[:,'Side'] = 'OculusSinister'
        df_r = df_r.rename_axis('JobId').reset_index()
        df_1 = df_l.append(df_r)
        
        fig, ax = plt.subplots()
        ax.plot(df_1[attribute1], df_1[attribute2]-df_1[attribute3], '*')
        ax.set_ylabel(f'{attribute2}-{attribute3} {attribute2_units}')
        ax.set_xlabel(f'{attribute1} ({attribute2_units})')
        ax.set_title(f'{attribute1} vs {attribute2}-{attribute3}')
        
        if fig_path is not None:
            fig_name_to_use = f'{fig_name} {attribute1} vs {attribute2}-{attribute3}.png'
            fig.savefig(os.path.join(fig_path, fig_name_to_use))
    
    def attribute_2_attributes_plot(self, 
                       attribute1, 
                       attribute2,
                       attribute3,
                       attribute1_units,
                       attribute2_units,
                       attribute3_units,
                       fig_path=None, 
                       fig_name=None):
        
        df_l = self.datasample.data['OculusDexter'][['MeasurementDate', attribute1, attribute2, attribute3]]
        df_l.loc[:,'Side'] = 'OculusDexter'
        df_l = df_l.rename_axis('JobId').reset_index()
        df_r = self.datasample.data['OculusSinister'][['MeasurementDate', attribute1, attribute2, attribute3]]
        df_r.loc[:,'Side'] = 'OculusSinister'
        df_r = df_r.rename_axis('JobId').reset_index()
        df_1 = df_l.append(df_r)
        
        fig, ax = plt.subplots()
        ax.plot(df_1[attribute1], df_1[attribute2], '*', label=f'{attribute2}')
        ax.plot(df_1[attribute1], df_1[attribute3], '*', label=f'{attribute3}')
        ax.set_ylabel(f'{attribute2_units}')
        ax.set_xlabel(f'{attribute1} ({attribute2_units})')
        ax.set_title(f'{attribute1} vs {attribute2} and {attribute3}')
        ax.legend()
        
        
        if fig_path is not None:
            fig_name_to_use = f'{fig_name} {attribute1} vs {attribute2} and {attribute3}.png'
            fig.savefig(os.path.join(fig_path, fig_name_to_use))
    
    def bucketed_box_plots(self, 
                           attribute,
                           attribute_units,
                           attribute_tols,
                           buckets,
                           power_type, 
                           data_units, 
                           fig_path=None, 
                           fig_name=None):
        """
        Plot the distribution of the difference between golden data and prescriptive component of the data
        grouped according to attribute values
       
        ---------
        Parameters
        ---------
        attribute: str
            lens attribute to bucket
        attribute_units
            lens attribute units
        buckets: dict
            dictionary of bucket limits
        power_type: str
            prescriptive component to show difference plots
        data_units: str
            power_type units
        fig_path: str
            folder to save figures in
        fig_name: str
            figure name stem
        xlims: tuple(float,float)
            xlims for plotting
        
        Returns
        ---------
        void
        
        """
        
        colors = {key: i for i, key in enumerate(buckets)}
        data_buckets = {key: None for key, value in buckets.items()}

        fig, ax = plt.subplots()

        for key, value in data_buckets.items():
            data_to_use = []
            for side in ['OculusSinister', 'OculusDexter']:
                datadiff = [(power,jobid) for (power,jobid,side,mdate) in \
                            self.datasample.datadiff[f'{power_type}'] if side == side]
            
                # get JobIDs for each power band
                data = self.datasample.data[side]
                jobids = data.index[(abs(data[attribute].astype('float')) >= buckets[key][0]) & \
                                    (abs(data[attribute].astype('float')) < buckets[key][1])].tolist()
            
                data_to_use = data_to_use + [power for (power,jobid) in datadiff if jobid in jobids]
            data_buckets[key] = data_to_use
            
        ylabel = f'{power_type} Difference {data_units}'
        xlabel = f'{attribute} {attribute_units}'
        fig_name_to_use = f'{fig_name} {attribute} {power_type} buckets.png'
        
        dictionary_boxplots(data_buckets,
                      attribute_tols,
                      ylabel,
                      xlabel,
                      fig_name_to_use,
                      fig_path)
    
    @property
    def datasample(self):
        """
        A property containing the datasample object
        Returns
        -------
        DataSample
            DataSample object
        """
        return self._datasample
    
class StatsTestComparison():
    
    """
    A class to manage comparisons between stats tests with different datasets
    incoming: Dictionary of StatsTest objects to compare
    
    """
    def __init__(self, statstest_dict):

        self._statstest_dict = statstest_dict
        
    def __paired_test_comparison_dictionary(self, prescriptive_component):
        """
        Return dictionary with paired data for each dataset, for a given prescriptive component
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component
        
        Returns
        ---------
        paired_test_diff_dict: dict
            dictionary with list of tuples for each dataset
        
        """
        
        
        paired_test_diff_dict = {}
        for key, value in self._statstest_dict.items():
            
            paired_test_diff_dict[key] = value.data_paired[prescriptive_component]
                
        return paired_test_diff_dict
    
    def __golden_standard_test_comparison_dictionary(self, prescriptive_component):
        """
        Return dictionary with data difference with golden standard (mean) for each dataset, 
        for a given prescriptive component
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component
        
        Returns
        ---------
        golden_standard_test_diff_dict: dict
            dictionary with list of tuples for each dataset
        
        """
        
        golden_standard_test_diff_dict = {}
        for key, value in self._statstest_dict.items():
            
            golden_standard_test_diff_dict[key] = value.data1.datadiff[prescriptive_component]
                
        return golden_standard_test_diff_dict
    
    def paired_data_diff_boxplot(self, prescriptive_component, fig_path=None):
        """
        Plot boxplots for each dataset, with paired data difference for given prescriptive component
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component
        fig_path: str
            directory to save plot (default None: doesn't save)
        
        Returns
        ---------
        void
        
        """

        paired_test_diff_dict = self.__paired_test_comparison_dictionary(prescriptive_component)
        
        ylabel = f'{prescriptive_component} Difference (D) (Paired Data)'
        xlabel = 'Paired Datasets'
        fig_title = f'{prescriptive_component} Paired Data Difference'
        tol = self._statstest_dict[list(self._statstest_dict.keys())[0]].tolerances[prescriptive_component]
        
        bp = dictionary_boxplots(paired_test_diff_dict, tol, ylabel, xlabel, fig_title, fig_path)
        
    def golden_standard_data_diff_boxplot(self, prescriptive_component, fig_path=None):
        """
        Plot boxplots for each dataset, with data - golden standard for given prescriptive component
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component
        fig_path: str
            directory to save plot (default None: doesn't save)
        
        Returns
        ---------
        void
        
        """

        golden_standard_test_diff_dict = self.__golden_standard_test_comparison_dictionary(prescriptive_component)
        
        ylabel = f'{prescriptive_component} Difference (D) (Atlas - Golden Standard)'
        xlabel = 'Datasets'
        fig_title = f'{prescriptive_component} Data Difference (Atlas - Golden Standard)'
        tol = self._statstest_dict[list(self._statstest_dict.keys())[0]].tolerances[prescriptive_component]
        
        bp = dictionary_boxplots(golden_standard_test_diff_dict, tol, ylabel, xlabel, fig_title, fig_path)
        
    def paired_data_diff_outliers(self, prescriptive_component, filepath=None):
        """
        Return dictionary with with paired data difference outliers for given prescriptive component 
        for each dataset
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component
        fig_path: str
            directory to save table (default None: doesn't save)
        
        Returns
        ---------
        outlier_dict: dict
            dictionary with dataframe for each dataset
        
        """
        
        paired_test_diff_dict = self.__paired_test_comparison_dictionary(prescriptive_component)
        
        col_names = ['Comparison', f'{prescriptive_component} Diff', f'{prescriptive_component} 1', \
                   f'{prescriptive_component} 2', 'JobId', 'Side']
        outlier_dict = outlier_dictionary(paired_test_diff_dict, col_names,  f'{prescriptive_component} Diff')
        
        # add golden standard value
        for key in self._statstest_dict.keys():
            golden_data_df = pd.DataFrame(columns=[prescriptive_component, 'Side'])
            for side in ['OculusDexter', 'OculusSinister']:
                golden_data_side = pd.DataFrame(self._statstest_dict[key].data1.\
                                                comparison_data.datameans[side][prescriptive_component])
                golden_data_side.insert(0, 'Side', side)
                golden_data_df = pd.concat([golden_data_df, golden_data_side])
                golden_data_df['JobId'] = golden_data_df.index 

            outlier_dict[key] = pd.merge(outlier_dict[key], golden_data_df, how='left', on=['JobId', 'Side'])
            
            if filepath is not None:
                filename = f'{prescriptive_component} Outliers {key}.csv'
                outlier_dict[key].to_csv(os.path.join(filepath, filename))
            
        return outlier_dict
    
    def golden_standard_diff_outliers(self, prescriptive_component, filepath=None):
        """
        Return dictionary with with data difference Atlas-Golden Standard outliers for given 
        prescriptive component for each dataset
        ---------
        Parameters
        ---------
        prescriptive_component: str
            name of prescriptive component
        fig_path: str
            directory to save table (default None: doesn't save)
        
        Returns
        ---------
        outlier_dict: dict
            dictionary with dataframe for each dataset
        
        """
        
        golden_standard_test_diff_dict = self.__golden_standard_test_comparison_dictionary(prescriptive_component)
        
        col_names = ['Dataset name', f'{prescriptive_component} Diff (Atlas-GS)', 'JobId', 'Side', 'mdate']
        outlier_dict = outlier_dictionary(golden_standard_test_diff_dict, col_names,  f'{prescriptive_component} Diff (Atlas-GS)')
        
        # add golden standard value and measured data value
        for key in self._statstest_dict.keys():
            golden_data_df = pd.DataFrame(columns=[prescriptive_component, 'Side'])
            measured_data_df = pd.DataFrame(columns=[\
                            self._statstest_dict[key].data1.columns_mapping[prescriptive_component], 'Side'])
            for side in ['OculusDexter', 'OculusSinister']:
                golden_data_side = pd.DataFrame(self._statstest_dict[key].data1.\
                                                comparison_data.datameans[side][prescriptive_component])
                golden_data_side.insert(0, 'Side', side)
                golden_data_df = pd.concat([golden_data_df, golden_data_side])
                golden_data_df['JobId'] = golden_data_df.index 
                
                measured_data_side = pd.DataFrame(self._statstest_dict[key].data1.\
                        data[side][self._statstest_dict[key].data1.columns_mapping[prescriptive_component]])
                measured_data_side.insert(0, 'Side', side)
                measured_data_df = pd.concat([measured_data_df, measured_data_side])
                measured_data_df['JobId'] = measured_data_df.index 

            outlier_dict[key] = pd.merge(outlier_dict[key], golden_data_df, how='left', on=['JobId', 'Side'])
            outlier_dict[key] = pd.merge(outlier_dict[key], measured_data_df, how='left', on=['JobId', 'Side'])
            
            if filepath is not None:
                filename = f'{prescriptive_component} Outliers {key}.csv'
                outlier_dict[key].to_csv(os.path.join(filepath, filename))
            
        return outlier_dict
    
    def single_data_golden_standard_diff_boxplot(self, prescriptive_component, fig_path=None):

        paired_test_diff_dict = self.__paired_test_prescriptive_component_dictionary(prescriptive_component)
        
        ylabel = f'{prescriptive_component} Difference (D) (Paired Data)'
        xlabel = 'Paired Datasets'
        fig_title = f'{prescriptive_component} Paired Data Difference'
        tol = self._statstest_dict[list(self._statstest_dict.keys())[0]].tolerances[prescriptive_component]
        
        bp = dictionary_boxplots(paired_test_diff_dict, tol, ylabel, xlabel, fig_title, fig_path)

class DataSampleSelection():
    """
    A class to manage data sample selection choices and methods for validation study
    
    """
    def __init__(self, **kwargs):
        
        self._config_dict = load_yaml(CONFIG_PATH, 'datasamplesettings.yml')
        self._data_labels = load_yaml(CONFIG_PATH, self._config_dict['db_to_portal_labels'])
        self.data_units = self._config_dict['data_units']
        self.__data_elt()
        self._data_sample = None
        # self.__get_jobIds()


    def __data_elt(self):
        
        sql_configs = self._config_dict['sql']
        if 'us' in sql_configs:
            us_sql_configs = sql_configs['us']
           
            us_sql_configs['query'] = sql_configs['query']
            us_sql_configs['start_date'] = sql_configs['start_date']
            us_sql_configs['end_date'] = sql_configs['end_date']
            us_sql_configs['prism_b_col'] = sql_configs['prism_b_col']
            us_sql_configs['prism_d_col'] = sql_configs['prism_d_col']
            us_sql_configs['data_labels'] = self._data_labels
            # if US run query with eyoto credentals
            us_data = sql_query_to_df_all_accounts(**us_sql_configs)
        else:
            us_data = None
        if 'uk' in sql_configs:
            uk_sql_configs = sql_configs['uk']
           
            uk_sql_configs['query'] = sql_configs['query']
            uk_sql_configs['start_date'] = sql_configs['start_date']
            uk_sql_configs['end_date'] = sql_configs['end_date']
            uk_sql_configs['prism_b_col'] = sql_configs['prism_b_col']
            uk_sql_configs['prism_d_col'] = sql_configs['prism_d_col']
            uk_sql_configs['data_labels'] = self._data_labels
            # if UK run query with personal credentals
            uk_data = sql_query_to_df_ma_all_accounts(**uk_sql_configs)
        else:
            uk_data = None 
        data = pd.concat([us_data,uk_data], ignore_index=True)
        self._data = data
        
    def plot_single_distribution(self, 
                                 data_column, 
                                 density=False, 
                                 remove_zeros=False, 
                                 remove_outliers=False,
                                 fig_path=None):
        
        data = self.data[data_column].to_numpy()
        data = data[~np.isnan(data)]
        no_zeros = ''
        if remove_zeros:
            data = data[data!=0]
            no_zeros = 'no zeros'
        no_outliers = ''
        if remove_outliers:
            low_thresh, upp_thresh = get_data_outlier_thresholds(data)
            indices = np.where((data >= low_thresh) & (data <=upp_thresh))
            data = data[indices]
            no_outliers = 'no outliers'
            
        units = self.data_units[data_column]
        xlabel = f'{data_column} {units}'
        if density is False:
            ylabel = 'Jobs Count'
        else:
            ylabel = 'Jobs Density'
        fig_title = f'{data_column} Distribution ({no_zeros} {no_outliers})'
        population_plot(data,
                        ylabel,
                        xlabel,
                        bins=50,
                        density=density,
                        xlims=None,
                        fig_name=fig_title,
                        fig_path=fig_path)
        
    def plot_paired_distribution(self, 
                                 data_column1, 
                                 data_column2, 
                                 density=False, 
                                 remove_zeros=[False,False], 
                                 fig_path=None):
        
        data = self.data[[data_column1, data_column2]]
        data = data.dropna()
        
        xlabel = f'{data_column1} {self.data_units[data_column1]}'
        ylabel = f'{data_column2} {self.data_units[data_column2]}'
        no_zeros1 = ''
        if remove_zeros[0]:
            data = data[data[data_column1]!=0]
            no_zeros1 = '(no zeros)'
        no_zeros2 = ''
        if remove_zeros[1]:
            data = data[data[data_column2]!=0]
            no_zeros2 = '(no zeros)'
        
        data1 = data[data_column1].to_numpy()
        data2 = data[data_column2].to_numpy()
        fig_name = f'Joint Distribution {data_column1} {no_zeros1} and {data_column2} {no_zeros2}'
        
        two_d_population_plot(data1, data2, ylabel, xlabel, fig_name=fig_name, bins=50, fig_path=fig_path)

    def plot_paired_scatter(self, 
                           data_column1, 
                           data_column2, 
                           density=False, 
                           remove_zeros=[False,False], 
                           fig_path=None):
       
        data = self.data[[data_column1, data_column2]]
        data = data.dropna()
        
        xlabel = f'{data_column1} {self.data_units[data_column1]}'
        ylabel = f'{data_column2} {self.data_units[data_column2]}'
        no_zeros1 = ''
        if remove_zeros[0]:
            data = data[data[data_column1]!=0]
            no_zeros1 = '(no zeros)'
        no_zeros2 = ''
        if remove_zeros[1]:
            data = data[data[data_column2]!=0]
            no_zeros2 = '(no zeros)'
        
        data1 = data[data_column1].to_numpy()
        data2 = data[data_column2].to_numpy()
        if density:
            density_str = 'density'
        else:
            density_str = ''
        
        fig_name = f'{data_column1} {no_zeros1} vs {data_column2} {no_zeros2} {density_str}'
        
        twod_scatter(data1, data2, ylabel, xlabel, density=density, fig_name=fig_name, fig_path=fig_path)
        
    def pairwise_chi2_independence(self, col1, col2, sample_size=100, file_path=None):
        """
        A method to test pairwise correlation of multivariate data, given 2 columns

        Returns
        -------
        results_dict
            A dict of results
        """
        
        if sample_size is not None:
            sample_data = self.subsample_data(sample_size).dropna(subset=[col1, col2])
            sample_str = f'n={sample_size}'
        else:
            sample_data = self.data.dropna(subset=[col1, col2])
            sample_str = f'n={len(sample_data)}'
        cat_data = pd.DataFrame(data=[pd.cut(sample_data[col], 20) for col in [col1, col2]])
        cat_data = cat_data.T
        
        
        crosstab = pd.crosstab(cat_data[col1], cat_data[col2])
        chi_sq, p, dof, E = chi2_contingency(crosstab)
        results_dict = {'chi_sq': chi_sq,
                        'p': p,
                        'dof': dof,
                        'E': E,
                        'O': crosstab}
        
        cols = np.sort(cat_data[col2].apply(lambda x: round(x,2) if isinstance(x,float) \
                                            else round(x.mid,2)).unique())
        rows = np.sort(cat_data[col1].apply(lambda x: round(x,2) if isinstance(x,float) \
                                            else round(x.mid,2)).unique())
        xlabel = f'{col2} Category Midpoints'
        ylabel = f'{col1} Category Midpoints'
        fig_title = f'Expected frequencies {col1} vs {col2} {sample_str}'
        fig_path = os.path.join(file_path, f'{fig_title}.png')
        heatmap_with_annotations(E, 
                                 cols,
                                 rows,
                                 fig_title,
                                 xlabel,
                                 ylabel,
                                 annotations=False,
                                 fig_path=fig_path)
        fig_title = f'Observed frequencies {col1} vs {col2} {sample_str}'
        fig_path = os.path.join(file_path, f'{fig_title}.png')
        heatmap_with_annotations(crosstab, 
                                 cols, 
                                 rows,
                                 fig_title,
                                 xlabel,
                                 ylabel,
                                 annotations=False,
                                 fig_path=fig_path)
        
        chi_squared_dist_plot(dof, chi_sq, fig_path=file_path)
        
        return results_dict
    
    def run_chi2_pairwise_multiple_variables(self, columns, sample_size=None, file_path=None):
        
        sample_data = self.subsample_data(sample_size)
        p_vals_df = pd.DataFrame(data=None, columns=columns, index=columns)
        
        
        for pair in itertools.combinations_with_replacement(columns, 2):
            if pair[0]!=pair[1]:
                sample_pair = sample_data[[pair[0], pair[1]]].dropna()
                cat_data = pd.DataFrame(data=[pd.cut(sample_pair[col], 20) \
                                              for col in [pair[0], pair[1]]])
                cat_data = cat_data.T
                crosstab = pd.crosstab(cat_data[pair[0]], cat_data[pair[1]])
            else:
                sample_pair = pd.DataFrame(sample_data[pair[0]].dropna())
                cat_data = pd.DataFrame(data=[pd.cut(sample_pair[pair[0]], 20)])
                cat_data = cat_data.T
                crosstab = pd.crosstab(cat_data[pair[0]], cat_data[pair[0]])
            chi_sq, p, dof, E = chi2_contingency(crosstab)
            
            p_vals_df.loc[pair[0], pair[1]] = p
            p_vals_df.loc[pair[1], pair[0]] = p
            
        p_vals_fig_name = 'Chi-Squared p-values'
        fig_path = os.path.join(file_path, f'{p_vals_fig_name}.png')
        heatmap_with_annotations(p_vals_df.to_numpy(dtype='float'), 
                                columns, 
                                columns, 
                                p_vals_fig_name,
                                cmap_name='jet',
                                color_lims=[0,1],
                                fig_path=fig_path)
            
        return p_vals_df
    
    def pairwise_chi2_multiple_testing(self, columns, alpha=0.05, sample_size=100, runs=100, file_path=None):
        """
        A method to test pairwise independence of multivariate data, multiple data sampling

        Returns
        -------
        df
            A df of sampled data in suitable format
        """
        results_df = pd.DataFrame(data=None, columns=columns, index=columns)
        
        for pair in itertools.combinations_with_replacement(columns, 2):
            p_vals = []
            for run in np.arange(runs):
                if pair[0]!=pair[1]:
                    sample_data = self.subsample_data(sample_size)[[pair[0], pair[1]]].dropna()
                    cat_data = pd.DataFrame(data=[pd.cut(sample_data[col], 20) \
                                                  for col in [pair[0], pair[1]]])
                    cat_data = cat_data.T
                    crosstab = pd.crosstab(cat_data[pair[0]], cat_data[pair[1]])
                else:
                    sample_data = pd.DataFrame(self.subsample_data(sample_size)[pair[0]].dropna())
                    cat_data = pd.DataFrame(data=[pd.cut(sample_data[pair[0]], 20)])
                    cat_data = cat_data.T
                    crosstab = pd.crosstab(cat_data[pair[0]], cat_data[pair[0]])
                chi_sq, p, dof, E = chi2_contingency(crosstab)
                p_vals.append(p)
            results_df.loc[pair[0],pair[1]] = len([p_val for p_val in p_vals if p_val > alpha])
            results_df.loc[pair[1],pair[0]] = len([p_val for p_val in p_vals if p_val > alpha])
        
        fig_title = f'Chi-squared Independence \n(% p > {alpha}; n={sample_size}, r={runs})'
        fig_path = os.path.join(file_path, \
                                f'Chi-squared Independence p gr_th {alpha}, n={sample_size}, r={runs}.png')
        heatmap_with_annotations(results_df.to_numpy(dtype='float'), 
                                     columns, 
                                     columns, 
                                     fig_title,
                                     color_lims=[0,100],
                                     fig_path=fig_path)
       
        return results_df
    
    def pairwise_pearsons_correlation(self, col1, col2, sample_size=None, fig_path=None):
        
        if sample_size is None:
            sample_data = self.data
        else:
            sample_data = self.subsample_data(sample_size)
        
        sample_data = sample_data.dropna(subset=[col1, col2])
        x = sample_data[col1].to_numpy()
        y = sample_data[col2].to_numpy()
        
        n = len(x)

        df = n-2
        
        r, p_val = sts.pearsonr(x,y)
        results_dict = {'cor': r,
                        'p': p_val}
    
        t_distribution_plot(df, r, fig_path)
        
        return results_dict
    
    def run_pearsons_pairwise_multiple_variables(self, columns, sample=None, file_path=None):
        
        sample_data = self.data
        corr_df = pd.DataFrame(data=None, columns=columns, index=columns)
        p_vals_df = pd.DataFrame(data=None, columns=columns, index=columns)
        
        for pair in itertools.combinations_with_replacement(columns, 2):
            
            sample_data = sample_data.dropna(subset=[pair[0], pair[1]])
            
            x = sample_data[pair[0]].to_numpy()
            y = sample_data[pair[1]].to_numpy()
            
            r, p_val = sts.pearsonr(x,y)
            corr_df.loc[pair[0], pair[1]] = r
            corr_df.loc[pair[1], pair[0]] = r
            p_vals_df.loc[pair[0], pair[1]] = p_val
            p_vals_df.loc[pair[1], pair[0]] = p_val
        
        p_vals_fig_name = 'Pearsons p-values'
        fig_path = os.path.join(file_path, f'{p_vals_fig_name}.png')
        heatmap_with_annotations(p_vals_df.to_numpy(dtype='float'), 
                                 columns, 
                                 columns, 
                                 p_vals_fig_name,
                                 cmap_name='jet',
                                 color_lims=[0,1],
                                 fig_path=fig_path)
        corr_fig_name = 'Pearsons correlation coefficients'
        fig_path = os.path.join(file_path, f'{corr_fig_name}.png')
        heatmap_with_annotations(corr_df.to_numpy(dtype='float'), 
                                 columns, 
                                 columns, 
                                 corr_fig_name,
                                 cmap_name='bwr',
                                 color_lims=[-1,1],
                                 annotations_col='k',
                                 fig_path=fig_path)
        
        
    def subsample_data(self, n):
        """
        A method to randomly subsample from the data

        Returns
        -------
        df
            A df of sampled data in suitable format
        """
        
        if n is not None:
            size_data = len(self.data)
            sample_indices = np.random.choice(size_data, n)
            
            self._data_sample = self.data.loc[sample_indices].reset_index(drop=True)
        else:
            self._data_sample = self.data
        return self._data_sample
    
    def factorial_coded_data(self, intervals_dict):
        
        data = self.data
        
        
        
        
    
    @property
    def data(self):
        """
        A property containing sampling data in dataframe

        Returns
        -------
        df
            A df of data in suitable format
        """
        return self._data
    
    @property
    def data_sample(self):
        """
        A property containing subsample of data in dataframe

        Returns
        -------
        df
            A df of data in suitable format
        """
        return self._data_sample
            

def population_plot(data,
                    ylabel,
                    xlabel,
                    bins=50,
                    density=False,
                    xlims=None,
                    fig_name=None,
                    fig_path=None):
    """
    Plot the distribution of the data
    ---------
    Parameters
    ---------
    data: list
        list of data values
    data_name: str
        name of data distribution
    
    Returns
    ---------
    void
    
    """
    
    fig, ax = plt.subplots()

    n, bins, patches=plt.hist(data, bins=bins,color='b',density=density)
  
    ax.set_title(fig_name)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    if xlims is not None:
        plt.xlim(xlims)
    
    mu, sigma = norm.fit(data)
    best_fit_line = norm.pdf(bins, mu, sigma)
    if density:
        pl1 = ax.plot(bins, best_fit_line, color='r')
    
    if fig_path is not None:
        fig_path = os.path.join(fig_path)
        fig.savefig(os.path.join(fig_path, f'{fig_name}.png'))

def two_d_population_plot(data1,
                          data2,
                          ylabel,
                          xlabel,
                          bins=50,
                          density=False,
                          lims=None,
                          fig_name=None,
                          fig_path=None):
    
    fig, ax = plt.subplots()
    
    ax.set_title(fig_name)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    ax.hist2d(data1, data2, bins, density=density, cmap=plt.cm.jet)
    
    if fig_path is not None:
        fig_path = os.path.join(fig_path)
        fig.savefig(os.path.join(fig_path, f'{fig_name}.png'))
        
def twod_scatter(data1, 
                  data2, 
                  ylabel,
                  xlabel,
                  density=False,
                  lims=None,
                  fig_name=None,
                  fig_path=None):
    
    fig, ax = plt.subplots()
    
    ax.set_title(fig_name)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    if density:
        # Calculate the point density
        xy = np.vstack([data1,data2])
        z = gaussian_kde(xy)(xy)
        idx = z.argsort()
        x, y, z = data1[idx], data2[idx], z[idx]

        pl1 = ax.scatter(x, y, c=z, cmap=plt.cm.jet)
        fig.colorbar(pl1, label='Pixel density')
    else:
        ax.scatter(data1, data2)
    
    if fig_path is not None:
        fig_path = os.path.join(fig_path)
        fig.savefig(os.path.join(fig_path, f'{fig_name}.png'))
    

def population_mean(data, absolute=False):
    """
    Find the mean values of a dictionary of arrays
    ---------
    Parameters
    ---------
    data: dict
    dictionary of prescriptive components and their values (list)
    
    
    Returns
    ---------
    data_means: dict
    dictionary of prescriptive components and their mean values (float)
    
    
    """
    mean_dict = {}
    for k, v in data.items():
        if absolute:
            mean_dict[k] = np.mean(np.abs(v))
        else:
            mean_dict[k] = np.mean(v)
        
    return mean_dict
        
    
def population_std(data):

    std_dict = {}
    for k, v in data.items():
        std_dict[k] = np.std(v)
        
    return std_dict
        
def dictionary_boxplots(data_dict,
                  tol_band,
                  ylabel,
                  xlabel,
                  fig_name=None,
                  fig_path=None):
    
    """
    Boxplots of dictionary of list of data tuples, grouped according to jobid and lens location, with tolerance bands
    ---------
    Parameters
    ---------
    data_dict: dict of tuples
        data to be plotted (len of dict = # boxplots, keys of dict dataset source name, 
                            first element in tuple = value for plot)
    tol_band: float
        tolerance of data errors for tolerance bands
    ylabel: str
        y-axis label
    xlabel: str
        x-axis label
    fig_name: str
        name of figure for saving, default None
    fig_dir: str
        directory to save fig, default None
    
    
    Returns
    ---------
    void
    
    """
    
    fig, ax = plt.subplots()
    bp = ax.boxplot([list(list(zip(*data_dict[key]))[0]) for key in data_dict.keys()], \
                    positions=[i for i, key in enumerate(data_dict.keys())])

    ylims = ax.get_ylim()
    for i, key in enumerate(data_dict):
        ax.text(i, ylims[1]+(ylims[1]-ylims[0])*0.1, f'n={len(data_dict[key])}')
    
        
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_xticks([i for i, key in enumerate(data_dict.keys())])
    ax.set_xticklabels([f'{key}' for key, value in data_dict.items()])
    xlims = ax.get_xlim()
    ax.hlines([0, -tol_band, tol_band], xlims[0],xlims[1], \
              linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
    
    if fig_path is not None:
        fig_path = os.path.join(fig_path)
        
        fig.savefig(os.path.join(fig_path, fig_name))
        
    return bp
        
def two_line_plot_two_axes(data_dict, 
                           data_units_dict,
                           linetype_dict,
                           marker_dict,
                           tol_band,
                           fig_path=None,
                           fig_name=None):
    
    """
    Boxplots of dictionary of data arrays with tolerance bands
    ---------
    Parameters
    ---------
    data_dict: dict of arrays
        data to be plotted (keys of dict dataset source name)
    data_units_dict: dict of arrays
        units of data to be plotted (keys of dict dataset source name)
    linetype_dict: dict of arrays
        type of line to be plotted (keys of dict dataset source name)
    marker_dict: dict of arrays
        type of marker to be plotted (keys of dict dataset source name)
    tol_band: float
        tolerance of data errors for tolerance band
    fig_name: str
        name of figure for saving, default None
    fig_dir: str
        directory to save fig, default None
    
    
    Returns
    ---------
    void
    
    """
    
    cmap = cm.get_cmap('tab10')
    
    fig, ax = plt.subplots()
    line1_key = list(data_dict.keys())[1]
    ax.plot(list(data_dict.values())[0], data_dict[line1_key], \
            label=f'{line1_key}', linestyle=linetype_dict[line1_key], \
                marker=marker_dict[line1_key], color=cmap(0))
    ax.set_ylabel(f'{line1_key} {data_units_dict[line1_key]}')
    ax2 = ax.twinx()
    line2_key = list(data_dict.keys())[2]
    ax2.plot(list(data_dict.values())[0], data_dict[line2_key], \
            label=f'{line2_key}',linestyle=linetype_dict[line2_key], \
                marker=marker_dict[line2_key], color=cmap(1))
    ax2.set_ylabel(f'{line2_key} {data_units_dict[line2_key]}')
    ax.set_xlabel(f'{list(data_units_dict.keys())[0]}')
    ax.set_title(f'{line1_key} and {line2_key} vs {list(data_units_dict.keys())[0]}')
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15))
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25))
    xlims = ax2.get_xlim()
    ax2.hlines([0, -tol_band, tol_band], xlims[0],xlims[1], linestyles='--', colors=['k', 'b', 'b'], alpha=0.5)
    
    if fig_path is not None:
        fig.savefig(os.path.join(fig_path, fig_name), bbox_inches="tight") 
    
def outlier_dictionary(data_dict, col_names, col_for_outliers, whisk=1.5):
    
    """
    Create a dictionary of dataframes of outliers from dictionary of lists of data tuples, 
    where outliers are found from the nth element of tuples
    ---------
    Parameters
    ---------
    data_dict: dict of tuples
        data used (len of dict = # boxplots, keys of dict dataset source name, 
                            first element in tuple = value for plot)
                            (value, jobid, lenslocation)
    col_names: list(str)
        list of column headers for dataframe, in order of tuple placement (first col describes test comparison)
    col_for_outliers: str
        name of column to be used for outlier detection
    
    Returns
    ---------
    outlier_dict: DataFrame
        Dictionary of dataframes of outliers with additional properties
    
    """
    outlier_dict = {}
    for key, value in data_dict.items():
        df_temp = pd.DataFrame(data=value, columns=col_names[1:])
        df_temp.insert(0, col_names[0], key)
        low_thresh, upp_thresh = get_data_outlier_thresholds(df_temp[col_for_outliers], whisk)
        
        df_temp = df_temp.loc[(df_temp[col_for_outliers] < low_thresh) | (df_temp[col_for_outliers] > upp_thresh)]
        
        outlier_dict[key] = df_temp
        # df = pd.concat([df, df_temp], ignore_index=True)
        
    return outlier_dict
    
def get_data_outlier_thresholds(data_list, whisk=1.5):
    """
    Get outliers from a list, based on 
    low_thresh = Q1-whisk*IQR
    upp_thresh = Q3 + whisk*IQR
    ---------
    Parameters
    ---------
    data_list: list
        data used for outlier detection
    whisk: float
        reach of whiskers in equivalent boxplot
    
    Returns
    ---------
    (low_thresh, upp_thresh)
    low_thresh: float
        lower threshold
    upp_thresh: float
        upper threshold
    
    """
    
    q3, q1 = np.percentile(data_list, [75,25])
    iqr = q3-q1
    
    low_thresh = q1-whisk*iqr
    upp_thresh = q3+whisk*iqr
    
    return low_thresh, upp_thresh

def heatmap_with_annotations(data, 
                             xlabels, 
                             ylabels,
                             fig_title,
                             xlabel=None,
                             ylabel=None,
                             annotations=True,
                             cmap_name='jet',
                             color_lims=None,
                             annotations_col='w',
                             fig_path=None):
    """
    Plot heat map for 2D data with data annotations and x- and y-labels
    ---------
    Parameters
    ---------
    data: 2D array
        data to plot
    xlabels: list(str)
        x-axis labels
    ylabels: list(str)
        y-axis labels
    
    Returns
    ---------
    void
    
    """
    # fig, ax = plt.subplots()
    fig, ax = plt.subplots(figsize=(6,6))
    if color_lims is None:
        im = ax.imshow(data,cmap=plt.get_cmap(cmap_name))
    else:
        im = ax.imshow(data,cmap=plt.get_cmap(cmap_name), vmin=color_lims[0], vmax=color_lims[1]) 
    
    # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(len(xlabels)), labels=xlabels)
    ax.set_yticks(np.arange(len(ylabels)), labels=ylabels)
    
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    
    # Loop over data dimensions and create text annotations.
    if annotations:
        for i in range(len(xlabels)):
            for j in range(len(ylabels)):
                text = ax.text(j, i, round(data[i, j],2),
                                ha="center", va="center", color=annotations_col)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    ax.set_title(fig_title)
    cbar = plt.colorbar(im)
    fig.tight_layout()
    plt.show()
    
    if fig_path is not None:
        fig.savefig(fig_path)


def chi_squared_dist_plot(df, chi_2, fig_path=None):
    
    # sts.chi2.stats()
    fig, ax = plt.subplots()
    x = np.linspace(sts.chi2.ppf(0.00, df), sts.chi2.ppf(0.9999, df), 100)
    ax.plot(x, sts.chi2.pdf(x, df))
    ax.hlines(sts.chi2.pdf(chi_2, df), x[0], chi_2, alpha=0.5, linestyle='--', colors=['r'])
    ax.vlines(chi_2, sts.chi2.pdf(x[0], df), sts.chi2.pdf(chi_2, df), alpha=0.5, linestyle='--', colors=['r'])
    ax.set_ylabel('Probability Density')
    ax.set_xlabel('Chi-Square Value')
    fig_title = f'Chi-Squared Distribution (dof={df})'
    ax.set_title(fig_title)   
    
    if fig_path is not None:
        fig.savefig(os.path.join(fig_path, f'{fig_title}.png'))
    
def t_distribution_plot(df, r, fig_path=None):
    
    fig, ax = plt.subplots()
    x = np.linspace(sts.t.ppf(0.01, df), sts.t.ppf(0.99, df), 100)
    ax.plot(x, sts.t.pdf(x, df))
    ax.hlines(sts.t.pdf(r, df), x[0], r, alpha=0.5, linestyle='--', colors=['r'])
    ax.vlines(r, sts.t.pdf(x[0], df), sts.t.pdf(r, df), alpha=0.5, linestyle='--', colors=['r'])
    ax.set_ylabel('Probability Density')
    ax.set_xlabel('T-Value')
    fig_title = f'T-Distribution (dof={df})'
    ax.set_title(fig_title)   
    
    if fig_path is not None:
        fig.savefig(os.path.join(fig_path, f'{fig_title}.png'))
        
def color_test(x):
    if x:
        return 'b' 
    else:
        return 'r'