# %%
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 10:55:56 2022
Test scripts for the exploratory work on validation study
@author: FionaLoftus
"""

from src.statistical_tests import DataSample, GoldenData, StatsTest
from tools.data_elt import load_yaml
import tools.utilities as utl
import os
import numpy as np
# %%
file_path = os.path.join(utl.get_project_root())
filename = 'local_config.yml'
local_configs = load_yaml(file_path, filename)

sql_dict = {'datasource_type':'sql',
            'start_date':'2022-01-10',
            'end_date':'2022-01-14',
            'client':'Engineering Test',
            'user':'fiona.loftus@eyoto.com'}
csv_dict = {'datasource_type':'csv',
            'path': local_configs['pre_fix_path'],
            'filename': local_configs['pre_fix_filename']}
csv_dict2 = {'datasource_type':'csv',
            'path': local_configs['post_fix_path'],
            'filename': local_configs['post_fix_filename']}

golden_dict = {'datasource_type': 'xlsx',
               'path': local_configs['golden_path'],
               'filename': local_configs['golden_filename']}
               # 'worksheet_names': local_configs['golden_worksheets']}
golden_path = local_configs['golden_path']
golden_filename = local_configs['golden_filename']
golden_data = GoldenData(**golden_dict)

# %%
d1 = DataSample(golden_data=golden_data,  **csv_dict)
# d2 = DataSample(golden_data=None, **csv_dict2)

# %%
d1.population_difference_plot('Sphere', 'Diopters')
# %%
stats_all = StatsTest(d1)
# %%
stats_all.allow_trim = True
stats_all.test_type = 'Paired T-Test'
# %%
t = stats_all.run_statistical_test(60)
# %%
stats_all.test_type = 'Single Sample Golden Standard T-Test'
t = stats_all.run_statistical_test(60)
# %%
stats_all.paired_population_plot('Sphere', 'D', 'D1', 'D2')

