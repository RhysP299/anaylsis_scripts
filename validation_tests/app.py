# -*- coding: utf-8 -*-
"""
Created on Tue May 24 11:44:10 2022

@author: FionaLoftus
"""

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from src.run_ui import Window
import logging
import os
from src.statistical_tests import LOG_PATH, DataSample

# Handle high resolution displays:
if hasattr(Qt, 'AA_EnableHighDpiScaling'):
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
if hasattr(Qt, 'AA_UseHighDpiPixmaps'):
    QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)
QApplication.setAttribute(Qt.AA_Use96Dpi)
formatter = '%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(lineno)d - %(message)s'
logging.basicConfig(filename=os.path.join(LOG_PATH,'test.log'), format=formatter, level=logging.WARNING)
logger = logging.getLogger(__name__)


app = QApplication(sys.argv)
font = app.font()
font.setPixelSize(11)
app.setFont(font)
win = Window()
win.show()
sys.exit(app.exec_())