# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 11:36:46 2022

@author: FionaLoftus
"""

# 1. Load GS data

# 2. DF of EP Sph, Cyl, Ax, Add, H Prism, V Prism

# 3. create DF of full factorial

# 4. Loop through factorial, ID if lens exists (within interval?)
# %%
from src.statistical_tests import DataSampleSelection
import pandas as pd
import numpy as np
from tools.data_elt import csv_to_df, load_yaml
import tools.utilities as utl
import os
from doepy import build
import random
# %%
def get_GS_data():
    csv_path = 'D:\Measure_{0}\Alex210722'
    csv_file = 'All Devices LMS.csv'
    # columns_path = os.path.join(utl.get_project_root(), 'configs')
    # columns_mapping_yml = 'db_to_portal_mapping.yml'
    columns_mapping = {
     'Sphere': 'EPSph',
     'Cylinder': 'EPCyl',
     'Axis': 'EPAxis',
     'Add': 'EPAdd',
     'H Prism Mag': 'prism_h',
     'V Prism Mag': 'prism_v'
    }
    
    # columns_dict = load_yaml(columns_path, columns_mapping_yml)
    data = csv_to_df(path=csv_path, filename=csv_file, columns_mapping=columns_mapping)
    data_dict = data['data']
    for side in ['OculusSinister', 'OculusDexter']:
        data_dict[side] = data_dict[side].drop('MeasurementDate', axis=1)
        data_dict[side]['EPAdd'] = pd.to_numeric(data_dict[side]['EPAdd'], errors='coerce')
        data_dict[side]['EPAdd'] = data_dict[side]['EPAdd'].fillna(0)
    
    return data_dict

def get_fact_data(columns_dict):
    full_fact = build.full_fact(columns_dict)
    
    return full_fact
def get_fact_data_int():
    columns_dict = {
       'Sphere': [0,1],
       'Cylinder': [0,1],
       'Axis': [0,1],
       'Add': [0,1],
       'H Prism Mag': [0,1],
       'V Prism Mag': [0,1],
        }
    full_fact = build.full_fact(columns_dict)
    
    return full_fact

def lens_pairs(df):
    
    df_OD = pd.DataFrame(columns=df.columns)
    df_OS = pd.DataFrame(columns=df.columns)
    
    indices = np.arange(0, len(df))
    new_indices = list(indices)
    random.shuffle(new_indices)
    
    
    
    for e, i in enumerate(np.arange(0, len(indices),2)):
        
        j = i+1
        df_OD.loc[e] = df.loc[new_indices[i]]
        df_OS.loc[e] = df.loc[new_indices[j]]
        
    return df_OD, df_OS
        
# %%
columns_dict_both = {
   'Sphere': [-10,10],
   'Cylinder': [0,-5],
   'Axis': [0,180],
   'Add': [0,4],
   'H Prism Mag': [-4.24,4.24],
   'V Prism Mag': [-4.24,4.24]
    }
columns_dict_pal = {
   'Sphere': [-10,10],
   'Cylinder': [0,-5],
   'Axis': [0,180],
   'Add': [0.1,4],
   'H Prism Mag': [-4.24,4.24],
   'V Prism Mag': [-4.24,4.24]
    }
columns_dict_sv = {
   'Sphere': [-10,10],
   'Cylinder': [0,-5],
   'Axis': [0,180],
   'H Prism Mag': [-4.24,4.24],
   'V Prism Mag': [-4.24,4.24]
    }
gs_data = get_GS_data()
full_fact_both = get_fact_data(columns_dict_both)
full_fact_pal = get_fact_data(columns_dict_pal)
full_fact_sv = get_fact_data(columns_dict_sv)
full_fact_int = get_fact_data_int()
# %%
full_fact_both.to_csv(os.path.join('D:\Measure_{0}', 'full_fact_both.csv'))
full_fact_pal.to_csv(os.path.join('D:\Measure_{0}', 'full_fact_pal.csv'))
full_fact_sv.to_csv(os.path.join('D:\Measure_{0}', 'full_fact_sv.csv'))

# %%
# AFTER JIM/ROB FEEDBACK
columns_dict_sv_no_cyl = {
    'Sphere': [-10,10],
    'H Prism': [-4.24, 4.24],
    'V Prism': [-4.24,4.24]
    }
columns_dict_sv_cyl = {
    'Sphere': [-10,10],
    'H Prism': [-4.24, 4.24],
    'V Prism': [-4.24,4.24],
    'Axis': [45,135],
    'Cylinder': [0.25,5]
    }
columns_dict_pal_no_cyl = {
    'Sphere': [-10,10],
    'H Prism': [-4.24, 4.24],
    'V Prism': [-4.24,4.24],
    'Add': [-2.5,4]
    }
columns_dict_pal_cyl = {
    'Sphere': [-10,10],
    'H Prism': [-4.24, 4.24],
    'V Prism': [-4.24,4.24],
    'Axis': [45,135],
    'Add': [-2.5,4],
    'Cylinder': [0.25,5]
    }
full_fact_sv_no_cyl = get_fact_data(columns_dict_sv_no_cyl)
full_fact_sv_cyl = get_fact_data(columns_dict_sv_cyl)
full_fact_pal_no_cyl = get_fact_data(columns_dict_pal_no_cyl)
full_fact_pal_cyl = get_fact_data(columns_dict_pal_cyl)
# %%
# full_fact_sv_cyl['Cylinder'] = 5
full_fact_sv_no_cyl['Cylinder'] = 0
full_fact_sv_no_cyl['Axis'] = 0
# full_fact_pal_cyl['Cylinder'] = 5
full_fact_pal_no_cyl['Cylinder'] = 0
full_fact_pal_no_cyl['Axis'] = 0
full_fact_sv_all = full_fact_sv_cyl.append(full_fact_sv_no_cyl, ignore_index=True)
full_fact_sv_all.to_csv(os.path.join('D:\Measure_{0}', 'full_fact_sv_all.csv'))
full_fact_pal_all = full_fact_pal_cyl.append(full_fact_pal_no_cyl, ignore_index=True)
full_fact_pal_all.to_csv(os.path.join('D:\Measure_{0}', 'full_fact_pal_all.csv'))

# %%
df_od_sv_no_cyl, df_os_sv_no_cyl = lens_pairs(full_fact_sv_no_cyl)
df_od_sv_cyl, df_os_sv_cyl = lens_pairs(full_fact_sv_cyl)
df_od_pal_no_cyl, df_os_pal_no_cyl = lens_pairs(full_fact_pal_no_cyl)
df_od_pal_cyl, df_os_pal_cyl = lens_pairs(full_fact_pal_cyl)
df_od_sv_no_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_od_sv_no_cyl.csv'))
df_os_sv_no_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_os_sv_no_cyl.csv'))
df_od_sv_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_od_sv_cyl.csv'))
df_os_sv_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_os_sv_cyl.csv'))
df_od_pal_no_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_od_pal_no_cyl.csv'))
df_os_pal_no_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_os_pal_no_cyl.csv'))
df_od_pal_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_od_pal_cyl.csv'))
df_os_pal_cyl.to_csv(os.path.join('D:\Measure_{0}', 'df_os_pal_cyl.csv'))
# %%
print(gs_data['OculusSinister'].dtypes)
# %%
# 4. Loop through factorial, ID if lens exists (within interval?)
# Two loops (v inefficient!!)
matching_dfs = pd.DataFrame(data=None, \
                            columns=['Side', 'Sphere', 'Cylinder', 'Axis', 'Add', 'H Prism Mag', 'V Prism Mag'])
for index, fact_row in full_fact_both.iterrows():
    fact_vals = list(fact_row)
    
    for side in ['OculusSinister', 'OculusDexter']:
        gs_side = gs_data[side]
        for idx, gs_row in gs_side.iterrows():
            gs_vals = list(gs_row)
            if gs_vals == fact_vals:
                gs_vals.insert(0, side)
                matching_dfs.loc[len(matching_dfs)] = gs_vals
# %%
# 4. Loop through factorial, ID if lens exists within interval
# Two loops (v inefficient!!)
gs_data = get_GS_data()
full_fact_int = get_fact_data_int()
# %%
matching_dfs_int = pd.DataFrame(data=None, \
                    columns=['Side', 'Sphere', 'Cylinder', 'Axis', 'Add', 'H Prism Mag', 'V Prism Mag'])
fact_dict = {
    'EPSph' : [[-10.0,-8.0], [8.0,10.0]],
    'EPCyl': [[-1.0,0.0],[-4.0,-5.0]],
    'EPAxis': [[0,1],[170,180]],
    'EPAdd': [[0.0,0.0],[3.0,4.0]],
    'prism_h': [[-4.24,-3.24],[3.24,4.24]],
    'prism_v': [[-4.24,-3.24],[3.24,4.24]]
    }
columns_mapping = {
    'EPSph':'Sphere',
    'EPCyl': 'Cylinder',
    'EPAxis':'Axis', 
    'EPAdd': 'Add',
    'prism_h':  'H Prism Mag',
    'prism_v': 'V Prism Mag'
}
for index, fact_row in full_fact_int.iterrows():
    # if index == 0:
    fact_vals = fact_row
    for side in ['OculusSinister', 'OculusDexter']:
        gs_side = gs_data[side]
        for idx, gs_row in gs_side.iterrows():
            gs_vals = gs_row
            in_int = True
            for i, gs_val in gs_vals.items():
                # print(i)
                # print(gs_val)
                # print(fact_dict[i][int(fact_vals.at[columns_mapping[i]])][0])
                # print(fact_dict[i][int(fact_vals.at[columns_mapping[i]])][1])
                if gs_val < fact_dict[i][int(fact_vals.at[columns_mapping[i]])][0] or \
                    gs_val > fact_dict[i][int(fact_vals.at[columns_mapping[i]])][1]:
                        print(False)
                        in_int = False
            if in_int:
                gs_list = list(gs_vals)
                gs_list.insert(0, side)
                matching_dfs_int.loc[len(matching_dfs_int)] = gs_list
        
        
