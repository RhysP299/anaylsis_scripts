# -*- coding: utf-8 -*-
"""
Created on Thu Sep  1 14:28:36 2022

@author: FionaLoftus
"""

from src.statistical_tests import DataSampleSelection, population_plot
import pandas as pd
import itertools
import numpy as np
from scipy.stats.contingency import crosstab
from scipy.stats import chi2_contingency
import matplotlib.pyplot as plt
# %%
dss = DataSampleSelection()

# %%
# SINGLE VARIABLE DISTRIBUTIONS
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v']
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection'
for col in cols:
    dss.plot_single_distribution(col, fig_path=None)
# %%
# SINGLE VARIABLE DISTRIBUTIONS - REMOVE ZEROs
no_zeros_dict = {'EPSph': False,
                 'EPCyl': False,
                 'EPAxis': False,
                 'EPAdd': True,
                 'prism_h': True,
                 'prism_v': True}
no_outliers_dict = {'EPSph': True,
                 'EPCyl': True,
                 'EPAxis': True,
                 'EPAdd': True,
                 'prism_h': True,
                 'prism_v': True}
cols = ['EPAdd', 'prism_h', 'prism_v']
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection'
for col in cols:
    dss.plot_single_distribution(col, fig_path=fig_path, 
                                 remove_zeros=no_zeros_dict[col], remove_outliers=no_outliers_dict[col])
# %%
# PAIRED VARIABLE DISTRIBUTIONS
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v']
for pair in itertools.combinations(cols, 2):
    dss.plot_paired_distribution(pair[0], pair[1])
# %%
# PAIRED VARIABLE DISTRIBUTIONS - REMOVE ZEROs
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection'
no_zeros_dict = {'EPSph': False,
                 'EPCyl': False,
                 'EPAxis': False,
                 'EPAdd': True,
                 'prism_h': True,
                 'prism_v': True}
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v']
for pair in itertools.combinations(cols, 2):
    dss.plot_paired_distribution(pair[0], 
                                 pair[1], 
                                 remove_zeros=[no_zeros_dict[pair[0]],no_zeros_dict[pair[1]]],
                                 fig_path=fig_path)
    
# %%
# PAIRED SCATTER - REMOVE ZEROs
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection'
no_zeros_dict = {'EPSph': False,
                 'EPCyl': False,
                 'EPAxis': False,
                 'EPAdd': True,
                 'prism_h': True,
                 'prism_v': True}
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v']
for pair in itertools.combinations(cols, 2):
    dss.plot_paired_scatter(pair[0], 
                            pair[1],
                            density=True,
                            remove_zeros=[no_zeros_dict[pair[0]],no_zeros_dict[pair[1]]],
                            fig_path=fig_path)
# %%
# PROPORTION SV/PAL LENSES
data = dss.data['EPLensType']
ylabel = 'Jobs Count'
xlabel = 'EPLensType'
# population_plot(data, ylabel, xlabel)
print(len(data[data=='Progressive']))
print(len(data[data=='Progressive'])/len(data))

# %%
# CHI2 SQUARE TEST FOR INDEPENDENCE
file_path='C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection\\chi-squared'
sph_cyl = dss.pairwise_chi2_independence('EPSph', 'EPCyl', file_path=file_path)
# %%
file_path='C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection\\chi-squared'
sph_cyl = dss.pairwise_chi2_independence('EPSph', 'EPCyl', file_path=file_path, sample_size=None)
# %%
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v'] 
all_pairs = dss.pairwise_chi2_multiple_testing(columns=cols, file_path=file_path)
# %%
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v'] 
all_pairs = dss.run_chi2_pairwise_multiple_variables(columns=cols, file_path=file_path)

# %%
# PEARSONS CORRELATION
file_path='C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection\\pearsons'
sph_cyl = dss.pairwise_pearsons_correlation('EPSph', 'EPAxis', sample_size=None, fig_path=file_path)
# %%
file_path='C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sample selection\\pearsons'
dss.run_pearsons_pairwise_multiple_variables(cols, file_path=file_path)
# %%
# NORMAL TEST CHI2 
mu, sigma = 0, 0.1
mu2, sigma2 = 1, 0.3
s1 = np.random.normal(mu, sigma, 100)
s2 = np.random.normal(mu2, sigma2, 100)
runs = 100
p_vals = []
for run in np.arange(runs):
    s1 = np.random.normal(mu, sigma, 100)
    s2 = np.random.normal(mu, sigma, 100)
    cat_data = pd.DataFrame(data=[pd.cut(s1, 20), pd.cut(s2,20)])
    crosstab = pd.crosstab(cat_data.iloc[:,0], cat_data.iloc[:,1])
    chi_sq, p, dof, E = chi2_contingency(crosstab)
    p_vals.append(p)
p_prop = len([p_val for p_val in p_vals if p_val > 0.05])
# %%
print(np.sort(cat_data[0].apply(lambda x: x.left).unique()))
# %%
cat_data = pd.DataFrame(data=[pd.cut(s1, 20), pd.cut(s2,20)])
crosstab = pd.crosstab(cat_data.iloc[:,0], cat_data.iloc[:,1])
# %%
test = chi2_contingency(crosstab)
# %%
plt.imshow(test[3])
# plt.imshow(crosstab)
# %%
# MAX AND MIN VALUES
cols = ['EPSph', 'EPCyl', 'EPAxis', 'EPAdd', 'prism_h', 'prism_v']
for col in cols:
    print(f'Max {col}: {np.max(dss.data[col])}')
    print(f'Min {col}: {np.min(dss.data[col])}')
    