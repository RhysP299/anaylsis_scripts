"""
General functions to extract, load and transform data from database using Pandas
"""
import pandas as pd 
import numpy as np
import pyodbc
import os
import yaml
from datetime import datetime
from typing import Iterable
import csv
import time
from tools import utilities as utl
import logging
import pathlib
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient, __version__, DelimitedTextDialect, DelimitedJsonDialect

logger = logging.getLogger(__name__)


def flatten(items):
    """Yield items from any nested iterable"""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x
            
def load_yaml(file_path, filename):
    """
    Load yaml file given path and filename
    Parameters
    ---------
    path: str
        path to yaml file directory
    filename: str
        filename of yaml file
    
    Returns
    ---------
    yaml_dict: dict
        yaml as dictionary
        
    """
    try:
        with open(os.path.join(file_path, filename)) as stream:
            yaml_dict = yaml.safe_load(stream)
    except OSError as e:
        logger.error(f'OSError: {e}')
        raise
        
    return yaml_dict

def csv_to_df(**kwargs):   
    """
    Load csv data from path and transform for use in validation study
    Note csv format expected is that exported from eyoto-portal
    Parameters
    ---------
    path: str
        path to raw data file directory
    filename: str
        filename of raw data file
    column_mappings: dict
        dictionary of column names to be imported (values)
    
    Returns
    ---------
    data: dict
        data as dictionary of 2 dataframes, separated by lens location
        
    """
    path = kwargs['path']
    filename = kwargs['filename']
    
    column_mappings = kwargs['columns_mapping']
    columns_to_use = list(column_mappings.values())
    columns_to_use.append('MeasurementDate')
   
    raw_data = pd.read_csv(os.path.join(path, filename))
    raw_data.dropna(how='all', inplace=True)
    raw_data.columns = raw_data.columns.str.replace(" ","", regex=True)
    raw_data.columns = raw_data.columns.str.replace(".","", regex=True)
    raw_data = raw_data.astype({"JobID": int})
    raw_data['MeasurementDate'] = pd.to_datetime(raw_data['MeasurementDate']).dt.floor('min')
    
    start_date = pd.to_datetime(min(raw_data['MeasurementDate']))
    end_date = pd.to_datetime(max(raw_data['MeasurementDate']))
        
    # columns_required = ['JobID', 'MPLensLocation', 'MPPrism']
    # check_cols_in_df(raw_data, columns_required)
    raw_data = raw_data.astype({"JobID": str})
    raw_data = raw_data.drop_duplicates()
    raw_data_jobid_index = raw_data.set_index("JobID")
    raw_data_jobid_index = add_prism_col_to_db_data(raw_data_jobid_index)
    
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    
    data = split_db_data_by_lens_location(raw_data_jobid_index)
    # [column for column in \
            # list(column_mappings.values())] + 
    
    data[od_str] = data[od_str][[column for column in columns_to_use]]
    data[os_str] = data[os_str][[column for column in columns_to_use]]
    data[od_str] = data[od_str].drop_duplicates()
    data[os_str] = data[os_str].drop_duplicates()

    return {'data': data, 'start_date': start_date, 'end_date': end_date}

def sql_query_to_df(**kwargs):
    """ Run sql query to retrieve selected database entries, given a start date, end date and accountids
    
    Parameters:
    ---------
    server: str
    db: str
    user: str
    query: str
    start_date: str
    end_date: str
    client: list(str)
    data_labels: dict
    
    Returns: 
    ---------
    df: (Pandas DataFrame)
    """

    server = kwargs['server']
    db = kwargs['db']
    user = kwargs['user']
    start_date = pd.to_datetime(kwargs['start_date'])
    end_date = pd.to_datetime(kwargs['end_date'])
    client = kwargs['client']
    db_portal_dict = kwargs['data_labels']
    
    accountids = [v for v in db_portal_dict[client].values()]

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";Authentication=ActiveDirectoryInteractive")

    query = kwargs['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    try:
        df = pd.read_sql_query(query, conn,  params=params)

    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    conn.close()
    
    df = df.astype({"JobID": str})
        
    df = df.set_index("JobID")
    
    if 'prism_b_col' in kwargs.keys():
        prism_b_col = kwargs['prism_b_col']
        prism_d_col = kwargs['prism_d_col']
        df = add_prism_col_to_db_data(df, prism_b_col=prism_b_col, prism_d_col=prism_d_col)
    
    df = db_to_portal_labels(df, db_portal_dict)
    
    df['MeasurementDate'] = pd.to_datetime(df['MeasurementDate']).dt.floor('min')
    
    # if kwargs['split_by_lens_location']:
    df = split_db_data_by_lens_location(df)

    return {'data': df, 'start_date': start_date, 'end_date': end_date}

def xlsx_to_df(**kwargs):
    
    """
    Load data from path and transform for use in validation study
    Parameters
    ---------
    path: str
        path to raw data file directory
    filename: str
        raw data filename
    
    Returns
    ---------
    data: dict
        nested dictionary of DataFrames, dictionary keys lens location and user/device ids
        DataFrames with measurements values for prescriptive measurements
        
    """
    # try:
    path = kwargs['path']
    filename = kwargs['filename']
    worksheet_names = kwargs['worksheet_names']
    raw_data = {worksheet_name: pd.read_excel(os.path.join(path, filename),
                                                     sheet_name=worksheet_name)
                       for worksheet_name in worksheet_names}
 

    columns = list(raw_data[worksheet_names[0]].columns)
    # TODO: check required columns exist in xlsx document

    # organise data from file
    for key, entry in raw_data.items():
        # drop enty first row and relabel index from 0
        entry.drop(axis=0, labels=[0], inplace=True)
        num_rows = len(entry.index)
        entry.index = np.linspace(0, num_rows, num_rows, dtype=int)
        # add spherical equivalent column
        # entry['Sph Equiv'] = utl.calculate_spherical_equivalent(entry['Sphere'], entry['Cylinder'])
        # entry['Lens Type'] = entry['Add'].apply(utl.determine_lens_type)
        # entry.loc[entry['Add'].isnull(),'Lens Type'] = 'SV'
        # entry.loc[entry['Add'].notnull(), 'Lens Type'] = 'PAL'
        # if entry['Add'].value().isnan():
        #     entry['Lens Type'] = 'SV'
        # else:
        #     entry['Lens Type'] = 'PAL'
        # duplicate data in odd rows of combined prism and pass/fail columns from
        # even rows
        # row_to_duplicate = None
        for idx, row in entry.iterrows():
            if idx % 2 == 0:  # index is even
                value_to_duplicate = row
            else:
                entry.loc[idx, columns[-6:-2] + [columns[-1]]] = \
                    value_to_duplicate[columns[-6:-2] +
                                       [columns[-1]]]

    # set job id as index
    data_jobid_index = {key: entry.astype({"JobID": int})
                               for key, entry in raw_data.items()}
    data_jobid_index = {key: entry.astype({"JobID": str})
                               for key, entry in data_jobid_index.items()}
    data_jobid_index = {key: entry.set_index("JobID")
                               for key, entry in data_jobid_index.items()}
    
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    data = {}
    data[od_str] = \
        {name: sheet.loc[list(sheet["Side"].str.strip() == "OD"), :]
         for name, sheet in data_jobid_index.items()}
    data[os_str] = \
        {name: sheet.loc[list(sheet["Side"].str.strip() == "OS"), :]
         for name, sheet in data_jobid_index.items()}
        
    return {'data': data, 'start_date': None, 'end_date': None}

def gallifrey_ini_to_df(**kwargs):
    
    path = kwargs['path']
    file = kwargs['filename']
    # lens_type = kwargs['lens_type']
    # config = kwargs['configs']
    # config = load_yaml(os.path.join(get_project_root(), 'configs'), 'gallifrey_configs.yml')
    left_df = pd.DataFrame(columns=['JobID', 'MPSph', 'MPCyl', 'MPAxis', 'MPSphEq', 'MPAdd', 'prism_v', 'prism_h'])
    right_df = pd.DataFrame(columns=['JobID', 'MPSph', 'MPCyl', 'MPAxis', 'MPSphEq', 'MPAdd', 'prism_v', 'prism_h'])
    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path,name)):
            # jobid is foldername (unless it starts with date)
            if name[:1].isdigit():
                jobid = name.partition('_')[2].partition('_')[2]
            else:
                jobid = name
            # open data.ini file in folder
            with open(os.path.join(path,name,file)) as csvfile:
                test = csv.reader(csvfile)
                # check lens type
                for row in test:
                    lens_type = 'SV'
                    if str.strip(row[0]) == kwargs['ini_PAL']:
                        lens_type = 'PAL'
                        break
                temp_dict_left = {}
                temp_dict_right = {}
                with open(os.path.join(path,name,file)) as csvfile:
                    test = csv.reader(csvfile)
                    for row in test:
                        # if LEFT
                        if str.strip(row[0]) in kwargs['keys_to_use'][lens_type]['OculusSinister']:
                            temp_dict_left[kwargs['ini_to_portal'][lens_type]['OculusSinister'][str.strip(row[0])]] = row[1]
                        # if RIGHT
                        if str.strip(row[0]) in kwargs['keys_to_use'][lens_type]['OculusDexter']:
                            temp_dict_right[kwargs['ini_to_portal'][lens_type]['OculusDexter'][str.strip(row[0])]] = row[1]
                if bool(temp_dict_left):
                    if lens_type == 'PAL':
                        if 'MPSphNRP' in list(temp_dict_left.keys()):
                            temp_dict_left['MPAdd'] = utl.calculate_add(float(temp_dict_left['MPSphNRP']), \
                                                                        float(temp_dict_left['MPSph']))
                        if 'MPSphNRP' in list(temp_dict_right.keys()):
                            temp_dict_right['MPAdd'] = utl.calculate_add(float(temp_dict_right['MPSphNRP']), \
                                                                     float(temp_dict_right['MPSph']))
                    temp_dict_left['JobID'] = jobid
                    temp_dict_left['MeasurementDate'] = pd.to_datetime('2022-11-21 00:00')
                    temp_dict_left['LensType'] = lens_type
                    temp_df_left = pd.DataFrame(temp_dict_left, index=['0'])
                    left_df = pd.concat([left_df, temp_df_left], ignore_index=True)
                if bool(temp_dict_right):
                    temp_dict_right['JobID'] = jobid
                    temp_dict_right['MeasurementDate'] = pd.to_datetime('2022-11-21 00:00')
                    temp_dict_right['LensType'] = lens_type
                    temp_df_right = pd.DataFrame(temp_dict_right, index=['0'])
                    right_df = pd.concat([right_df, temp_df_right], ignore_index=True)
    left_df.set_index('JobID', drop=True, inplace=True)
    cols_to_num = list(kwargs['ini_to_portal'][lens_type]['OculusSinister'].values())
    left_df[cols_to_num] = \
        left_df[cols_to_num].apply(pd.to_numeric)
    right_df.set_index('JobID', drop=True, inplace=True)
    cols_to_num = list(kwargs['ini_to_portal'][lens_type]['OculusDexter'].values())
    right_df[cols_to_num] = \
        right_df[cols_to_num].apply(pd.to_numeric)
    
    data ={'OculusSinister': left_df,
           'OculusDexter': right_df
           }
    
    return {'data': data}

def calibration_to_df(**kwargs):
    
    if kwargs['cali_type'] == 'ini':
        data_dict = gallifrey_ini_to_df(**kwargs)
    elif kwargs['cali_type'] == 'xlsx':
        data_dict = calibration_xlsx_to_df(**kwargs)
    else:
        e = 'Failed to load data.  Calibration type must be either "ini" or "xlsx"'
        logger.error(e)
        
    return data_dict

def calibration_xlsx_to_df(**kwargs):
    
    path = kwargs['path']
    filename = kwargs['filename']
    worksheet_names = kwargs['worksheet_names']
    raw_data = {worksheet_name: pd.read_excel(os.path.join(path, filename),
                                        sheet_name=worksheet_name, skiprows=6, header=[0,1], nrows=8)
                       for worksheet_name in worksheet_names}
    for worksheet_name in worksheet_names:
        raw_data[worksheet_name].drop(labels='Effective Power Results Table:', axis=1, inplace=True, level=0)
        

    data = {}
    for side in ['OculusDexter', 'OculusSinister']:
        data[side] = pd.DataFrame(columns=['MPSph', 'MPCyl', 'MPSphEq', 'JobID'])
        
    sheetname_side = {'Cam1_Pow_Measurement_Cal': 'OculusDexter',
                      'Cam2_Pow_Measurement_Cal': 'OculusSinister'}

    for worksheet_name, side in sheetname_side.items():
        for index, row in raw_data[worksheet_name].iterrows():
            if (row['Values of Reference Lenses', 'Nominal (D)'] > -20) & \
                (row['Values of Reference Lenses', 'Nominal (D)'] < 15):
                power = int(abs(row['Values of Reference Lenses', 'Nominal (D)']))
                if row['Values of Reference Lenses', 'Nominal (D)'] < 0:
                    jobid = f'M{power}'
                else:
                    jobid = f'P{power}'
                jobid_1 = f'{jobid}_1'
                temp_df_1 = pd.DataFrame([{'MPSph': row['Indication Readings by eMap', 'SPH (D)'],
                              'MPCyl': row['Indication Readings by eMap', 'CYL (D)'],
                              'MPSphEq': row['Indication Readings by eMap', 'SPEQ (D)'],
                              'JobID': jobid_1}])
                data[side] = pd.concat([data[side], temp_df_1])
                jobid_2 = f'{jobid}_2'
                temp_df_2 = pd.DataFrame([{'MPSph': row['Indication Readings by eMap', 'SPH (D)'],
                              'MPCyl': row['Indication Readings by eMap', 'CYL (D).1'],
                              'MPSphEq': row['Indication Readings by eMap', 'SPEQ (D).1'],
                              'JobID': jobid_2}])
                data[side] = pd.concat([data[side], temp_df_2])
                jobid_3 = f'{jobid}_3'
                temp_df_3 = pd.DataFrame([{'MPSph': row['Indication Readings by eMap', 'SPH (D)'],
                              'MPCyl': row['Indication Readings by eMap', 'CYL (D).2'],
                              'MPSphEq': row['Indication Readings by eMap', 'SPEQ (D).2'],
                              'JobID': jobid_3}])
                data[side] = pd.concat([data[side], temp_df_3])
        data[side].set_index('JobID', inplace=True)
        
    return {'data': data}
    
def calibration_ini_to_df(**kwargs):
    
    path = kwargs['path']
    filename = kwargs['filename']
    worksheet_names = kwargs['worksheet_names']
    raw_data = {worksheet_name: pd.read_excel(os.path.join(path, filename),
                                                     sheet_name=worksheet_name)
                       for worksheet_name in worksheet_names}
    
def calibration_comparison(**kwargs):
    
    path = kwargs['path']
    filename = kwargs['filename']
    worksheet_names = kwargs['worksheet_names']
    raw_data = {worksheet_name: pd.read_excel(os.path.join(path, filename),
                                        sheet_name=worksheet_name, skiprows=6, header=[0,1], nrows=8)
                       for worksheet_name in worksheet_names}
    for worksheet_name in worksheet_names:
        raw_data[worksheet_name].drop(labels='Effective Power Results Table:', axis=1, inplace=True, level=0)

    data = {}
    for side in ['OculusDexter', 'OculusSinister']:
        data[side] = pd.DataFrame(columns=['Sphere', 'Cylinder', 'Sph Equiv', 'JobID'])
        
    sheetname_side = {'Cam1_Pow_Measurement_Cal': 'OculusDexter',
                      'Cam2_Pow_Measurement_Cal': 'OculusSinister'}

    for worksheet_name, side in sheetname_side.items():
        for index, row in raw_data[worksheet_name].iterrows():
            if (row['Values of Reference Lenses', 'Nominal (D)'] > -20) & \
                (row['Values of Reference Lenses', 'Nominal (D)'] < 15):
                power = int(abs(row['Values of Reference Lenses', 'Nominal (D)']))
                if row['Values of Reference Lenses', 'Nominal (D)'] < 0:
                    jobid = f'M{power}'
                else:
                    jobid = f'P{power}'
                temp_df = pd.DataFrame([{'Sphere': row['Values of Reference Lenses', 'Effective Power (D)'],
                              'Cylinder': 0.0,
                              'Sph Equiv': utl.calculate_spherical_equivalent(row['Values of Reference Lenses', \
                                                                            'Effective Power (D)'],0.0),
                              'JobID': jobid}])
                data[side] = pd.concat([data[side], temp_df])
        data[side].set_index('JobID', inplace=True)
    
    if 'position0MMFromHome' in kwargs.keys():
        position0MMFromDisplay = kwargs['maxPositionMMFromDisplay']-kwargs['position0MMFromHome']
        position0MFromDisplay = position0MMFromDisplay / 1000
        position1MMFromDisplay = kwargs['maxPositionMMFromDisplay']-kwargs['position1MMFromHome']
        position1MFromDisplay = position1MMFromDisplay / 1000
        baselineDiameters = {'OculusDexter': kwargs['baselineDiameterRight'],
                             'OculusSinister': kwargs['baselineDiameterLeft']}
        
        
        for side in ['OculusDexter', 'OculusSinister']:
            
            baselineRadius = calculate_baseline_radius(baselineDiameters[side], \
                                    kwargs['baseGridsize'], kwargs['pointGridsize'])
            data[side]['Pos0m1'] = data[side]['Sphere'].apply(calculate_m1, d1=position0MFromDisplay)
            data[side]['Pos1m1'] = data[side]['Sphere'].apply(calculate_m1, d1=position1MFromDisplay)
            data[side]['Pos0m2'] = data[side].apply(lambda row: \
                            calculate_m2(row.Sphere, row.Cylinder,d1=position0MFromDisplay), axis=1)
            data[side]['Pos1m2'] = data[side].apply(lambda row: \
                            calculate_m2(row.Sphere, row.Cylinder,d1=position1MFromDisplay), axis=1)
            data[side]['Pos0r1'] = data[side]['Pos0m1'].apply(calculate_r1, br=baselineRadius)
            data[side]['Pos1r1'] = data[side]['Pos1m1'].apply(calculate_r1, br=baselineRadius)
            data[side]['Pos0r2'] = data[side]['Pos0m2'].apply(calculate_r1, br=baselineRadius)
            data[side]['Pos1r2'] = data[side]['Pos1m2'].apply(calculate_r1, br=baselineRadius)
         
    return data


def db_to_portal_labels(df, db_portal_dict):
    """ Convert database values to recognised portal values using dictionary 
    
    Inputs:
    df: (Pandas DataFrame)
    db_portal_dict: (dict) db_to_portal dictionary

    Returns: 
    df: (Pandas DataFrame)
    """

    for step in db_portal_dict:
        if step in df.columns:
            db_portal_dict[step] = {k: 'NaN' if not v else v for k, v in db_portal_dict[step].items()}
            df[step] = df[step].astype('int64')
            df[step] = df[step].replace(db_portal_dict[step])
    return df

def add_prism_col_to_db_data(data, 
                             prism_col='MPPrism', 
                             prism_d_col="MPPrismDiopters", 
                             prism_b_col='MPPrismBaseAngleDegrees'):
    """ Convert database / csv prism values into separate H and V components
    
    Inputs:
    data: (Pandas DataFrame)

    Returns: 
    data: (Pandas DataFrame)
    """
    diopters = np.array([float(value.split("/")[0])
                          if len(value) > 3 else np.nan
                          for value in data[prism_col].values]) \
        if prism_col in data.keys() else data[prism_d_col].values
    bases = np.array([float(value.split("/")[1])
                      if len(value) > 3 else np.nan
                      for value in data[prism_col].values]) \
        if prism_col in data.keys() else data[prism_b_col].values
    h, v = \
        utl.calculate_components(diopters, bases)
    data = data.assign(prism_h=h)
    data = data.assign(prism_v=v)
    return data

def split_db_data_by_lens_location(df, lens_location_col="MPLensLocation"):
    """
    Takes a DataFrame with an MPLensLocation column and creates a dictionary of
    two DataFrames.  One DataFrame is the oculus dexter rows and the other is
    the oculuis sinister.

    Parameters
    ----------
    df : pandas.DataFrame
        A data frame with as column headed EPLensLocation.

    Returns
    -------
    data : dictionary of two DataFrames
        A dictionary with two DataFrames as entries one the data for oculus
        dexter and the other for oculus sinister.
    """
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    od_condition = \
        list(df[lens_location_col].str.strip() == od_str)
    os_condition = \
        list(df[lens_location_col].str.strip() == os_str)
    data = {od_str: df.loc[od_condition, :], os_str: df.loc[os_condition, :]}
    return data

def sql_query_to_df_all_accounts(**kwargs):
    """ Run sql query to retrieve selected database entries, given a start date, end date
    
    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns: 
    df: (Pandas DataFrame)
    """

    server = kwargs['server']
    db = kwargs['db']
    user = kwargs['user']
    pwd = kwargs['pwd']
    start_date = kwargs['start_date']
    end_date = kwargs['end_date']
    clients = list(kwargs['clients'].keys())
    db_portal_dict = kwargs['data_labels']

    accountids = []
    
    for client in clients:
    # for client in config['clients']:
        if isinstance(kwargs['clients'][client]['accounts'],dict):
            accountids.append(list(kwargs['clients'][client]['accounts'].values()))
        else:
            accountids.append(kwargs['clients'][client]['accounts'])

    accountids = [x for x in flatten(accountids)]

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";PWD="+pwd+";")

    query = kwargs['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    
    try:
        df = pd.read_sql_query(query, conn,  params=params)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    conn.close()
    
    if 'prism_b_col' in kwargs.keys():
        prism_b_col = kwargs['prism_b_col']
        prism_d_col = kwargs['prism_d_col']
        df = add_prism_col_to_db_data(df, prism_b_col=prism_b_col, prism_d_col=prism_d_col)
     
    df = db_to_portal_labels(df, db_portal_dict)

    return df

def sql_query_to_df_ma_all_accounts(**kwargs):
    """ Run sql query to retrieve selected database entries, given a start date, end date and accountids
    
    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns: 
    df: (Pandas DataFrame)
    """

    server = kwargs['server']
    db = kwargs['db']
    user = kwargs['user']
    accountids = []
    clients = list(kwargs['clients'].keys())
    start_date = kwargs['start_date']
    end_date = kwargs['end_date']
    db_portal_dict = kwargs['data_labels']

    for client in clients:
        if isinstance(kwargs['clients'][client]['accounts'],dict):
            accountids.append(list(kwargs['clients'][client]['accounts'].values()))
        else:
            accountids.append(kwargs['clients'][client]['accounts'])

    accountids = [x for x in flatten(accountids)]

    params = tuple(flatten((accountids, start_date, end_date)))
    # params = (start_date, end_date)

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";Authentication=ActiveDirectoryInteractive")

    query = kwargs['query']
    # query = query.format('?')
    query = query.format('?', ','.join('?' * len(accountids)))
    try:
        df = pd.read_sql_query(query, conn,  params=params)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
   
    conn.close()
    
    if 'prism_b_col' in kwargs.keys():
        prism_b_col = kwargs['prism_b_col']
        prism_d_col = kwargs['prism_d_col']
        df = add_prism_col_to_db_data(df, prism_b_col=prism_b_col, prism_d_col=prism_d_col)
        
    df = db_to_portal_labels(df, db_portal_dict)
    
    return df

def download_lms_file(measurement_id, dest_dir):
    """ Download LMS file from Azure blob storage to CVS file 
    
    Inputs:
    measurement_id: (str)
    dest_dir: (str)
    
    Returns: 
    dest_path: (str) Full path of downloaded CSV file
    """
    try:
        connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)
        blob_container_client = blob_service_client.get_container_client('processed')

        path = measurement_id

        dest_path = os.path.join(dest_dir, f'{path}.csv')
        os.makedirs(os.path.dirname(dest_path), exist_ok=True)

        if not path == '' and not path.endswith('/'):
            path += '/'

        blob_iter = blob_container_client.list_blobs(name_starts_with=path) 

        errors = []
        def on_error(error):
            errors.append(error)

        for blob in blob_iter:
            relative_path = os.path.relpath(blob.name, path)
            if str(relative_path).endswith('.lms'):
                blob_client = blob_container_client.get_blob_client(blob.name)
                with open(dest_path, 'wb') as file:
                    data = blob_client.download_blob()
                    data.readinto(file)
        return dest_path

    except Exception as ex:
        print('Exception:')
        print(ex)
        return None

def csv_lms_to_df(csv_path):
    """ Read CSV version of LMS file to Pandas DataFrame 
    
    Inputs:
    csv_path: (str) Full path to CSV file
    
    Returns: 
    df: (Pandas DataFrame) DataFrame with Left and Right LMS data as separate rows
    """
    df = pd.read_csv(csv_path, sep='=+|;', names=['index', '3', '2'], index_col=0, on_bad_lines='skip', engine='python')
    df = df.T
    df.ffill(inplace=True)
    df.reset_index(inplace=True)
    df.rename(columns={'index': 'LensLocation'}, inplace=True)
    # remove duplicate columns
    df = df.loc[:,~df.columns.duplicated()].copy()
    df = df.astype({"JOB": str})
    df = df.set_index('JOB')
    
    return df

def lms_df(m_ids, path, db_portal_dict):
    
    if not os.path.exists(os.path.join(path, f'{m_ids[0]}.csv')):
        download_lms_file(m_ids[0], path)
    df = csv_lms_to_df(os.path.join(path, f'{m_ids[0]}.csv'))
    
    for m_id in m_ids[1:]:
        try:
            if not os.path.exists(os.path.join(path, f'{m_id}.csv')):
                download_lms_file(m_id, path)
            df_new = csv_lms_to_df(os.path.join(path, f'{m_id}.csv'))
            df = pd.concat([df, df_new], axis=0)
        except Exception as ex:
            print(f'Exception: {ex}')
            
    df = db_to_portal_labels(df, db_portal_dict)
    
    data = split_db_data_by_lens_location(df, "LensLocation")
    
    return data

def append_time_csv(path, filename, start, duration, size_df, days):

    row = {'start_time':start, 'duration': duration, 'file_size': size_df, 'days': days}
    row['start_time'] = row['start_time'].strftime('%Y/%m/%d %H:%M:%S')

    if not os.path.exists(os.path.join(path, filename)):

        with open(os.path.join(path, filename), 'w', newline='') as csvfile:
            fieldnames = ['start_time','duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow(row)
    else:
        with open(os.path.join(path, filename), 'a', newline='') as csvfile:
            fieldnames = ['start_time', 'duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writerow(row)


def lens_pairs_df(df, ignorena=False, cols=None, status_type=False, status_list=[], status_type_list=[]):
    """ Consolidate individual lens measurements into lens pairs

    Inputs:
    df: (Pandas DataFrame) Contains separate rows for left and right lenses 
    ignorena: (bool) If True, 
    cols: list(str) 
    status_type: (bool) If True, separate results into Automatic and Manual
    status_list: list(str)
    status_type_list: list(str)

    Returns: 
    df_lr: (Pandas DataFrame)
    
    """

    df_left = df[df['LensLocation']=='OculusSinister']
    df_right = df[df['LensLocation']=='OculusDexter']
    df_left = df_left.add_prefix('left_')
    df_left = df_left.rename(columns={'left_SerialId': 'SerialId', 'left_MeasurementDate': 'MeasurementDate'})
    df_right = df_right.add_prefix('right_')
    df_right = df_right.rename(columns={'right_SerialId': 'SerialId', 'right_MeasurementDate': 'MeasurementDate'})

    df_lr = pd.merge(df_left, df_right, on=['SerialId', 'MeasurementDate'], how='inner')
    if cols is not None:
        for col in cols:
            df_lr = df_lr.rename(columns={f'right_{col}': f'{col}'})
    
    for status in status_list:
        left_col = 'left_'+ status
        right_col = 'right_'+ status
        df_lr[status] = df_lr.apply(lambda x: calculate_status(x, [left_col, right_col], ignorena), axis=1)

    if status_type:
        for status_type in status_type_list:
            left_col = 'left_'+ status_type
            right_col = 'right_'+ status_type
            df_lr[status_type] = df_lr.apply(lambda x: calculate_type(x, [left_col, right_col]), axis=1)

    return df_lr

def calculate_type(x, columns):

    status_type = 'Automatic'
    for column in columns:
        if x[column] == 'Manual':
            status_type = 'Manual'
            return status_type
    return status_type

def calculate_status(x, columns, ignorena=False):
    """ Calculate status for multiple columns, ie all individual statuses to Pass for combined status Pass

    Inputs:
    x: (Pandas Series)  
    columns: list(str) Names of columns to be used for combined status 
    ignorena: (bool) If True, entries with NaN automatically Pass 
    status_type: (bool) If True, separate results into Automatic and Manual

    Returns: 
    df_lr: (Pandas DataFrame)
    
    """

    status_fail_options = ['Automatic Fail', 'Manual Fail', 'Fail']
    status_na_options = []
    returnstatus = 'Pass'
    if ignorena == False:
        status_na_options.extend(['NaN', 'None'])
    for column in columns:
        if ignorena == False:
            if (isinstance(x[column], float)) or (x[column] in status_na_options):
                current_status = 'NaN'
            elif (x[column] in status_fail_options):
                current_status = 'Fail'
            else:
                current_status = 'Pass'
        else:
            if (x[column] in status_fail_options):
                current_status  = 'Fail'
            else:
                current_status = 'Pass'
        if (current_status == 'NaN') or (returnstatus == 'NaN'):
            returnstatus = 'NaN'
        elif (current_status == 'Fail') or (returnstatus == 'Fail'):
            returnstatus = 'Fail'
        else:
            returnstatus = 'Pass'
    return returnstatus

def get_average_measurements_per_hour(df):

    df = measurement_hour(df)

    df = df_working_hours(df)

    average_measurements_per_hour = len(df) / 10

    return average_measurements_per_hour

def df_working_hours(df):

    working_hours = {12, 13, 14, 15, 16, 17, 18, 19, 20, 21}

    df = df[df['MeasurementHour'].isin(working_hours)]

    return df

def measurement_hour(df):

    df['MeasurementHour'] = pd.to_datetime(df['MeasurementDate']).dt.hour.astype('category')

    return df

def measurement_hour_conversion(measurement_date, hour_offset):
    
    measurement_date_converted = measurement_date + pd.DateOffset(hours=hour_offset)
    
    return measurement_date_converted

def check_cols_in_df(df, cols):
    
    for col in cols:
        if col not in df.columns:
            e = f'Required column ({col}) missing from data'
            logger.error(e)
            raise Exception(e)

def calculate_corridor_length(df):
    
    df['CorridorLengthCal'] = abs(df['ERDRUP'].astype('float')) + abs(df['ERNRUP'].astype('float'))
    
    return df

def calculate_vbox_diff1(df):
    
    df['VBOX/2- ABS[FCOCUP+ERDRUP]'] = df['VBOX'].astype(float)/2 - \
        abs(df['FCOCUP'].astype(float)+df['ERDRUP'].astype('float'))
        
    return df

def calculate_vbox_diff2(df):
    
    df['VBOX/2- ABS[FCOCUP+ERNRUP]'] = df['VBOX'].astype(float)/2 - \
        abs(df['FCOCUP'].astype(float)+df['ERNRUP'].astype('float'))
        
    return df

def corridor_length_from_points(df):
    
    df['MPDistanceCorridorPointY-MPDistanceCorridorPoint1Y'] = df['MPDistanceCorridorPointY'] \
        - df['MPDistanceCorridorPoint1Y']
    
    return df

def calculate_ExpNSphEq(df):
    
    df['ExpNSphEq'] = df['LDNRSPH'].astype('float') + df['LDNRCYL'].astype('float')/2
    
    return df

def calculate_r1(m1, br):
    
    r1 = m1*br
    
    return r1

def calculate_baseline_radius(baselinePixels, baselineGridSize, gridsizeMeasurementPoint):
    
    baselineRadius = (baselinePixels/baselineGridSize)*gridsizeMeasurementPoint
    
    return baselineRadius

def calculate_m1(p, d1):
    
    m1 = 1 / (1-d1*p)
    
    return m1

def calculate_m2(p1, p2, d1):
    
    p = p1 + p2
    
    m2 = 1 / (1-d1*p)
    
    return m2