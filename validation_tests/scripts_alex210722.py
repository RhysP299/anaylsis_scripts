# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 12:03:33 2022
Scripts to run plots etc for exploration of errors between Atlas and Golden Standard data
@author: FionaLoftus
"""

# %%
from src.statistical_tests import DataSample, GoldenData, StatsTest, DataPlots, StatsTestComparison
from src.statistical_tests import dictionary_boxplots, outlier_dictionary
from tools.data_elt import load_yaml, download_lms_file, csv_lms_to_df, lms_df
from tools.data_elt import calculate_corridor_length, calculate_vbox_diff1, calculate_vbox_diff2
from tools.data_elt import corridor_length_from_points, calculate_ExpNSphEq
import tools.utilities as utl
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd
from numpy.polynomial import Polynomial
from scipy.optimize import curve_fit

# FIG_PATH = os.path.join(utl.get_project_root(), 'figures')
FIG_PATH = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722'
file_path = os.path.join(utl.get_project_root())
filename = 'local_config.yml'
local_configs = load_yaml(file_path, filename)

datanames_all = ['all_devices', 'validation1', 'validation2', 'validation3','validation5', 'software_test']
datanames_sv =  ['all_devicesSV', 'validation1SV', 'validation2SV', 'validation3SV', 'validation5SV', 'software_testSV']
datanames_pal =  ['all_devicesPAL', 'validation1PAL', 'validation2PAL', 'validation3PAL', 'validation5PAL', 'software_testPAL']
datanames_sw = ['software_test', 'software_testSV', 'software_testPAL']

# %%
def load_validation_data(data_name, add_golden=False, add_extra=False, add_lms=False):
    
    # golden data
    golden_dict = {'datasource_type': 'xlsx',
                   'path': local_configs['golden_path'],
                   'filename': local_configs['golden_filename'],
                   'worksheet_names': local_configs['golden_worksheets']}
    
    data1_dict = {'datasource_type':'csv',
                'path': local_configs['alex210722_validation_path'],
                'filename': local_configs[data_name]}
    golden_data = None
    if add_golden:
        golden_data = GoldenData(**golden_dict)
    
    sql_dict = None
    
    if add_extra:
        sql_dict = {'datasource_type':'sql',
                    'client':'Eyoto Demo',
                    'user':'fiona.loftus@eyoto.com',
                    'lms': add_lms}

    d1 = DataSample(golden_data=golden_data, extra_db_dict=sql_dict, **data1_dict)
    
    return d1

def get_datadiff_xlims(d1):
    
    xlims_dict = {}
    
    for key, value in d1.datadiff.items():
        xlims_dict[key] = (np.floor(np.min(list(list(zip(*value))[0]))), \
                           np.ceil(np.max((list(list(zip(*value))[0])))))
        
    return xlims_dict
    
def jobIds_datadiff_condition(d1, prescriptive_component, greater_than, less_than):
    
    jobids = [(jobid, side) for (power,jobid,side) in d1.datadiff[f'{prescriptive_component}'] if power > greater_than]

# TASKS
# %%
# Task 1
dataname = 'all_devices'
v1 = load_validation_data(dataname, add_golden=True)
xlims = get_datadiff_xlims(v1)
prescription_dict = {'Sphere': 'D',
                     'Cylinder': 'D',
                     'Axis': u'\N{DEGREE SIGN}',
                     'Add': 'D',
                     'H Prism Mag': '\u0394',
                     'V Prism Mag': '\u0394'}
# %%
for data in datanames_sw:
    fig_path = os.path.join(FIG_PATH, data)
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    for key, value in prescription_dict.items():
        dp1.population_difference_plot(key, value, fig_path=None, xlims=xlims[key])
# %%
# Task 2
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task2'
for data in datanames_all:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    for key in ['Sphere', 'Sph Equiv']:
        dp1.bucketed_power_plots(key, 'D', fig_path=None, fig_name=data)   
# %%
# Task 2 Box
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task2'
for data in datanames_all:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    for key in ['Sphere', 'Sph Equiv']:
        dp1.bucketed_power_plots(key, 'D', plot_type='box', fig_path=fig_path, fig_name=data)
# %%
# Task 3
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3'
for data in datanames_all:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.positive_negative_power_plots('Sph Equiv', 'V Prism Mag', data_units,  fig_name=f'{data}',fig_path=None, xlims=xlims['V Prism Mag'])
# %%
# Task 3 SV
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\SV\\Unscaled'
for data in datanames_sv:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.positive_negative_power_plots('Sph Equiv', 'V Prism Mag', data_units, fig_path=None, fig_name=f'{data}SV')
# %%
# Task 3 PAL
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\PAL\\Unscaled'
for data in datanames_pal:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.positive_negative_power_plots('Sph Equiv', 'V Prism Mag', data_units, fig_path=None, fig_name=f'{data}PAL')
# %%
# Task 3 -ve buckets
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\-ve\\'
for data in datanames_all:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.negative_bucketed_power_plots('Sph Equiv', 
                                      'D',
                                      'V Prism Mag', 
                                      data_units, 
                                      fig_path=None, 
                                      fig_name=f'{data}', 
                                      xlims=xlims['V Prism Mag'])
# %%
# Task 3 -ve buckets SV
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\SV\\-ve'
for data in datanames_sv:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.negative_bucketed_power_plots('Sph Equiv', 
                                     'D',
                                     'V Prism Mag', 
                                     data_units, 
                                     fig_path=None, 
                                     fig_name=f'{data}', 
                                     xlims=xlims['V Prism Mag'])
# %%
# Task 3 -ve buckets PAL
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\PAL\\-ve'
for data in datanames_pal:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.negative_bucketed_power_plots('Sph Equiv', 
                                      'D',
                                      'V Prism Mag', 
                                      data_units, 
                                      fig_path=None, 
                                      fig_name=f'{data}', 
                                      xlims=xlims['V Prism Mag'])
# %%
# Task 3 -ve buckets box
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\-ve\\'
for data in datanames_all:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.negative_bucketed_power_plots('Sph Equiv', 
                                     'D', 
                                     'V Prism Mag', 
                                     data_units, 
                                     plot_type='box', 
                                     fig_path=None, 
                                     fig_name=f'{data}')
# %%
# Task 3 -ve buckets box SV
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\SV\\-ve\\'
for data in datanames_sv:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.negative_bucketed_power_plots('Sph Equiv', 
                                     'D', 
                                     'V Prism Mag', 
                                     data_units, 
                                     plot_type='box', 
                                     fig_path=fig_path, 
                                     fig_name=f'{data}')
# %%
# Task 3 -ve buckets box PAL
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task3\\PAL\\-ve\\'
for data in datanames_pal:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = '\u0394'
    dp1.negative_bucketed_power_plots('Sph Equiv', 
                                     'D', 
                                     'V Prism Mag', 
                                     data_units, 
                                     plot_type='box', 
                                     fig_path=fig_path, 
                                     fig_name=f'{data}')
# %%
# Task 4
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task4'
for data in datanames_pal:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = 'D'
    dp1.correlated_power_diff_plot('Sph Equiv', 'Add', data_units, fig_path=None, fig_name=data)
 
# %%
# Task 4 (Sph Eq vs Add Diff)
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task4\\SphEqAtlasAddDiff'
for data in datanames_pal:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = 'D'
    dp1.correlated_power_plot('MPSphEq', 
                             'Add', 
                             data_units, 
                             data_units,
                             fig_path=None, 
                             fig_name=data)
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task5'
data = 'software_test'
v1 = load_validation_data(data, add_extra=True, add_golden=True)
dp1 = DataPlots(v1)
dp1.attribute_vs_error_plot('MPCorridorLength', 'mm', 'Add', 'D')
# %%
data = 'software_test'
v1 = load_validation_data(data, add_extra=True, add_golden=True)
dp1 = DataPlots(v1)
dp1.attribute_plot('MPCorridorLength', 'mm')
# %%
data = 'software_test'
v1 = load_validation_data(data, add_extra=True, add_golden=True)
dp1 = DataPlots(v1)
dp1.attribute_vs_error_plot('MPCorridorLength', 'mm', 'Add', 'D')
# %%
# Task 5
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task5'
for data in datanames_pal:
    v1 = load_validation_data(data, add_extra=True, add_golden=True)
    dp1 = DataPlots(v1)
    dp1.correlated_power_plot_2_ax('MPCorridorLength', 'mm', 'Add', 'D', 0.12, fig_path=fig_path, fig_name=data)
# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task5'
for data in datanames_pal:
    v1 = load_validation_data(data, add_extra=True, add_golden=True, add_lms=True)
    v1.data['OculusDexter'] = calculate_corridor_length(v1.data['OculusDexter'])
    v1.data['OculusSinister'] = calculate_corridor_length(v1.data['OculusSinister'])
    dp1 = DataPlots(v1)
    dp1.correlated_power_plot_2_ax('CorridorLengthCal', 'mm', 'Add', 'D', fig_path=fig_path, fig_name=data)   
# %%
# Task 5 extras
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task5'
v1 = load_validation_data('all_devicesPAL', add_extra=True, add_golden=True, add_lms=True)
dp1 = DataPlots(v1)
# %%
dp1.attribute_attributediff_plot('MPCorridorLength',
                                'MP_NCPX', 
                                'MP_DCPX', 
                                'mm',
                                'mm', 
                                'mm', 
                                fig_name='all_devicesPAL',
                                fig_path=fig_path)
# %%
dp1.attribute_2_attributes_plot('MP_DRPX',
                                'MP_DCPX', 
                                'MP_NCPX', 
                                'mm',
                                'mm', 
                                'mm', 
                                fig_name='all_devicesPAL',
                                fig_path=fig_path)
# %%
# Task 6
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task6'
for data in datanames_pal:
    v1 = load_validation_data(data, add_extra=True, add_golden=True, add_lms=True)
    v1.data['OculusDexter'] = calculate_vbox_diff1(v1.data['OculusDexter'])
    v1.data['OculusSinister'] = calculate_vbox_diff1(v1.data['OculusSinister'])
    dp1 = DataPlots(v1)
    dp1.correlated_power_plot_2_ax('VBOX/2- ABS[FCOCUP+ERDRUP]',
                                  'mm', 
                                  'Sph Equiv', 
                                  'D', 
                                  fig_path=fig_path, 
                                  fig_name=data)
    
# %%
# Task 7
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task7'
for data in datanames_pal:
    v1 = load_validation_data(data, add_extra=True, add_golden=True, add_lms=True)
    v1.data['OculusDexter'] = calculate_vbox_diff2(v1.data['OculusDexter'])
    v1.data['OculusSinister'] = calculate_vbox_diff2(v1.data['OculusSinister'])
    dp1 = DataPlots(v1)
    dp1.correlated_power_plot_2_ax('VBOX/2- ABS[FCOCUP+ERNRUP]', 
                                  'mm', 
                                  'Add', 
                                  'D',
                                  0.12,
                                  fig_path=fig_path, 
                                  fig_name=data)
# %%
# Task 8
attribute = 'CTHICK'
attribute_units = 'mm'
power_type = 'V Prism Mag'
attribute_tols = 0.33
data_units = '\u0394'
buckets = {'[0,1)': (0,1), '[1,2)': (1,2), '[2,3)': (2,3), '[3,4)': (3,4), '[4,13)': (4,13)}
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task8'
for data in ['all_devicesPAL']:
    v1 = load_validation_data(data, add_extra=True, add_golden=True, add_lms=True)
    dp1 = DataPlots(v1)
    dp1.bucketed_box_plots(attribute, 
                           attribute_units, 
                           attribute_tols,
                           buckets, 
                           power_type, 
                           data_units, 
                           fig_path,
                           data)
# %%
# Task 9 (Sph Eq vs V Prism Diff)
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task9'
for data in datanames_all:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = 'D'
    dp1.correlated_power_plot_2_ax('MPSphEq', 
                                  'D',
                                  'V Prism Mag', 
                                  '\u0394', 
                                  0.33, 
                                  fig_path=fig_path, 
                                  fig_name=data)
# %%
# Task 10 (Sph Eq vs Add Diff)
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task10'
data = 'all_devicesPAL'
v1 = load_validation_data(data, add_golden=True)
dp1 = DataPlots(v1)
data_units = 'D'
dp1.correlated_power_plot('MPSphEq', 
                          'Add', 
                          data_units, 
                          data_units, 
                          singleline_plot=True,
                          prescriptive_component2_tol=0.12,
                          fit='poly',
                          polyfit_deg=4,
                          fig_path=fig_path, 
                          fig_name=data)
# %%
# Task 10b (Sph Eq vs V Prism Mag)
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task10'
for data in ['all_devices', 'all_devicesPAL', 'all_devicesSV']:
    v1 = load_validation_data(data, add_golden=True)
    dp1 = DataPlots(v1)
    data_units = 'D'
    dp1.correlated_power_plot('MPSphEq', 
                              'V Prism Mag', 
                              data_units, 
                              '\u0394', 
                              singleline_plot=True,
                              prescriptive_component2_tol=0.33,
                              fit=None,
                              polyfit_deg=4,
                              fig_path=fig_path, 
                              fig_name=data)
# QUESTIONS
# %%
# Task 11
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\task11'
for data in ['all_devicesPAL']:
    v1 = load_validation_data(data, add_golden=True, add_extra=True, add_lms=True)
    v1.data['OculusSinister'] = calculate_ExpNSphEq(v1.data['OculusSinister'])
    v1.data['OculusDexter'] = calculate_ExpNSphEq(v1.data['OculusDexter'])
    dp1 = DataPlots(v1)
    data_units = 'D'
    dp1.correlated_power_plot('ExpNSphEq',
                              'Add', 
                              data_units, 
                              data_units, 
                              singleline_plot=True,
                              prescriptive_component2_tol=0.12,
                              fit='poly',
                              polyfit_deg=4,
                              fig_path=fig_path, 
                              fig_name=data)
# %%
# Question 3
power_type = 'Sphere'
greater_than = 10
less_than = -10
test = [(jobid, side) for (power,jobid,side,date) in v1.datadiff[f'{power_type}'] if power > greater_than]
test2 = [(jobid, side) for (power,jobid,side,date) in v1.datadiff[f'{power_type}'] if power < less_than]
# %%
# Question 4
power_type = 'H Prism Mag'
greater_than = 50
test = [(jobid, side) for (power,jobid,side,date) in v1.datadiff[f'{power_type}'] if power > greater_than]
# %%
# DEVICE-DEVICE VARIATION
data1 = 'validation1'
data2 = 'validation2'
data3 = 'validation3'
data5 = 'validation5'
v1 = load_validation_data(data1, add_golden=True, add_extra=False, add_lms=False)
v2 = load_validation_data(data2, add_golden=True, add_extra=False, add_lms=False)
v3 = load_validation_data(data3, add_golden=True, add_extra=False, add_lms=False)
v5 = load_validation_data(data5, add_golden=True, add_extra=False, add_lms=False)
st12 = StatsTest(v1, v2)
st13 = StatsTest(v1, v3)
st15 = StatsTest(v1, v5)
st23 = StatsTest(v2, v3)
st25 = StatsTest(v2, v5)
st35 = StatsTest(v3, v5)
st1 = StatsTest(v1)
st2 = StatsTest(v2)
st3 = StatsTest(v3)
st5 = StatsTest(v5)
# %%
st12.paired_population_plot('Add', 'D', 'v1', 'v2')
st13.paired_population_plot('Add', 'D', 'v1', 'v3')
# %%
test = st12.run_statistical_test()
# %%
stats_test_dict = {'v1-v2': st12,
                   'v1-v3': st13,
                   'v1-v5': st15,
                   'v2-v3': st23,
                   'v2-v5': st25,
                   'v3-v5': st35}
stc = StatsTestComparison(stats_test_dict)
stats_test_dict_2 = {'v1': st1,
                   'v2': st2,
                   'v3': st3,
                   'v5': st5,}
stc_2 = StatsTestComparison(stats_test_dict_2)
# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\add'
stc.paired_data_diff_boxplot('Add', fig_path)
# %%
test = stc.paired_data_diff_outliers('Add', fig_path)
# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sphere'
stc_2.golden_standard_data_diff_boxplot('Sphere', fig_path)
# %%
test = stc_2.golden_standard_diff_outliers('Sphere', fig_path)
# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\cylinder'
stc_2.golden_standard_data_diff_boxplot('Cylinder', fig_path)
# %%
test = stc_2.golden_standard_diff_outliers('Cylinder', fig_path)
# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\axis'
stc_2.golden_standard_data_diff_boxplot('Axis', fig_path)
# %%
test = stc_2.golden_standard_diff_outliers('Axis', fig_path)
# %%
# ADD
prescriptive_component = 'Add'
device_add_data_dict = {'v1-v2': [(power_diff,jobid,side) for (power_diff,power1,power2,jobid,side) in \
                                             st12.data_paired[prescriptive_component]],
                 'v1-v3': [(power_diff,jobid,side) for (power_diff,power1,power2,jobid,side) in \
                                             st13.data_paired[prescriptive_component]],
                 'v1-v5':  [(power_diff,jobid,side) for (power_diff,power1,power2,jobid,side) in \
                                             st15.data_paired[prescriptive_component]],
                 'v2-v3': [(power_diff,jobid,side) for (power_diff,power1,power2,jobid,side) in \
                                             st23.data_paired[prescriptive_component]],
                 'v2-v5': [(power_diff,jobid,side) for (power_diff,power1,power2,jobid,side) in \
                                             st25.data_paired[prescriptive_component]],
                 'v3-v5': [(power_diff,jobid,side) for (power_diff,power1,power2,jobid,side) in \
                                             st35.data_paired[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference (D) (Paired Data)'
xlabel = 'Paired Datasets'
fig_title = f'{prescriptive_component} Paired Data Difference'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\add'

bp = dictionary_boxplots(device_add_data_dict, 0.12, ylabel, xlabel, fig_title, fig_path)

# %%
prescriptive_component = 'Add'
filepath = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\add'
filename = f'{prescriptive_component} Paired Data Outliers.csv'
device_add_data_dict = {'v1-v2': st12.data_paired[prescriptive_component],
                 'v1-v3': st13.data_paired[prescriptive_component],
                 'v1-v5':  st15.data_paired[prescriptive_component],
                 'v2-v3': st23.data_paired[prescriptive_component],
                 'v2-v5': st25.data_paired[prescriptive_component],
                 'v3-v5': st35.data_paired[prescriptive_component]}
# Outliers dataframe
columns = ['Comparison', f'{prescriptive_component} Diff', f'{prescriptive_component} 1', \
           f'{prescriptive_component} 2', 'JobID', 'Side']

add_outlier_dict = outlier_dictionary(device_add_data_dict, columns, f'{prescriptive_component} Diff')
# df_new.to_csv(os.path.join(filepath, filename))

# %%
# V PRISM
prescriptive_component = 'V Prism Mag'
device_vprism_data_dict = {'v1-v2': [power_diff for (power_diff,power1,power2,id) in \
                                             st12.data_paired[prescriptive_component]],
                 'v1-v3': [power_diff for (power_diff,power1,power2,id) in \
                                             st13.data_paired[prescriptive_component]],
                 'v1-v5':  [power_diff for (power_diff,power1,power2,id) in \
                                             st15.data_paired[prescriptive_component]],
                 'v2-v3': [power_diff for (power_diff,power1,power2,id) in \
                                             st23.data_paired[prescriptive_component]],
                 'v2-v5': [power_diff for (power_diff,power1,power2,id) in \
                                             st25.data_paired[prescriptive_component]],
                 'v3-v5': [power_diff for (power_diff,power1,power2,id) in \
                                             st35.data_paired[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference (\u0394) (Paired Data)'
xlabel = 'Paired Datasets'
fig_title = f'{prescriptive_component} Paired Data Difference'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\v prism'

dictionary_boxplots(device_vprism_data_dict, 0.33, ylabel, xlabel, fig_title, fig_path)

# %%
# H PRISM
prescriptive_component = 'H Prism Mag'
device_hprism_data_dict = {'v1-v2': [power_diff for (power_diff,power1,power2,id) in \
                                             st12.data_paired[prescriptive_component] if power_diff>-50],
                 'v1-v3': [power_diff for (power_diff,power1,power2,id) in \
                                             st13.data_paired[prescriptive_component] if power_diff>-50],
                 'v1-v5':  [power_diff for (power_diff,power1,power2,id) in \
                                             st15.data_paired[prescriptive_component]],
                 'v2-v3': [power_diff for (power_diff,power1,power2,id) in \
                                             st23.data_paired[prescriptive_component]],
                 'v2-v5': [power_diff for (power_diff,power1,power2,id) in \
                                             st25.data_paired[prescriptive_component]],
                 'v3-v5': [power_diff for (power_diff,power1,power2,id) in \
                                             st35.data_paired[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference (\u0394) (Paired Data)'
xlabel = 'Paired Datasets'
fig_title = f'{prescriptive_component} Paired Data Difference'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\h prism'

dictionary_boxplots(device_hprism_data_dict, 0.33, ylabel, xlabel, fig_title, fig_path)
# %%
# # ATLAS-GS BOXPLOTS
# SPHERE
prescriptive_component = 'Sphere'
gs_sphere_data_dict = {'v1-GS': [power_diff for (power_diff,id,side,mdate) in \
                                             v1.datadiff[prescriptive_component]],
                       'v2-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v2.datadiff[prescriptive_component]],
                       'v3-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v3.datadiff[prescriptive_component]],
                       'v5-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v5.datadiff[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference (D) (Atlas-Golden Standard)'
xlabel = 'Datasets'
fig_title = f'{prescriptive_component} Data Difference (Atlas-Golden Standard)'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\sphere'

dictionary_boxplots(gs_sphere_data_dict, 0.13, ylabel, xlabel, fig_title, fig_path)
# %%
# # CYL
prescriptive_component = 'Cylinder'
gs_cyl_data_dict = {'v1-GS': [power_diff for (power_diff,id,side,mdate) in \
                                             v1.datadiff[prescriptive_component]],
                       'v2-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v2.datadiff[prescriptive_component]],
                       'v3-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v3.datadiff[prescriptive_component]],
                       'v5-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v5.datadiff[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference (D) (Atlas-Golden Standard)'
xlabel = 'Datasets'
fig_title = f'{prescriptive_component} Data Difference (Atlas-Golden Standard)'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\cylinder'

dictionary_boxplots(gs_cyl_data_dict, 0.13, ylabel, xlabel, fig_title, fig_path)
# %%
# # AXIS
prescriptive_component = 'Axis'
units = '\u00b0'
gs_sphere_data_dict = {'v1-GS': [power_diff for (power_diff,id,side,mdate) in \
                                             v1.datadiff[prescriptive_component]],
                       'v2-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v2.datadiff[prescriptive_component]],
                       'v3-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v3.datadiff[prescriptive_component]],
                       'v5-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v5.datadiff[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference ({units}) (Atlas-Golden Standard)'
xlabel = 'Datasets'
fig_title = f'{prescriptive_component} Data Difference (Atlas-Golden Standard)'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\axis'

dictionary_boxplots(gs_sphere_data_dict, 2, ylabel, xlabel, fig_title, fig_path)
# %%
# # ADD
prescriptive_component = 'Add'
units = 'D'
gs_add_data_dict = {'v1-GS': [power_diff for (power_diff,id,side,mdate) in \
                                             v1.datadiff[prescriptive_component]],
                       'v2-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v2.datadiff[prescriptive_component]],
                       'v3-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v3.datadiff[prescriptive_component]],
                       'v5-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v5.datadiff[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference ({units}) (Atlas-Golden Standard)'
xlabel = 'Datasets'
fig_title = f'{prescriptive_component} Data Difference (Atlas-Golden Standard)'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\add'

dictionary_boxplots(gs_add_data_dict, 0.12, ylabel, xlabel, fig_title, fig_path)

# %%
# # H Prism
prescriptive_component = 'H Prism Mag'
units = '\u0394'
gs_hprism_data_dict = {'v1-GS': [power_diff for (power_diff,id,side,mdate) in \
                                             v1.datadiff[prescriptive_component] if power_diff<50],
                       'v2-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v2.datadiff[prescriptive_component]],
                       'v3-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v3.datadiff[prescriptive_component]],
                       'v5-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v5.datadiff[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference ({units}) (Atlas-Golden Standard)'
xlabel = 'Datasets'
fig_title = f'{prescriptive_component} Data Difference (Atlas-Golden Standard)'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\h prism'

dictionary_boxplots(gs_hprism_data_dict, 0.66, ylabel, xlabel, fig_title, fig_path)

# %%
# # V Prism
prescriptive_component = 'V Prism Mag'
units = '\u0394'
gs_vprism_data_dict = {'v1-GS': [power_diff for (power_diff,id,side,mdate) in \
                                             v1.datadiff[prescriptive_component]],
                       'v2-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v2.datadiff[prescriptive_component]],
                       'v3-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v3.datadiff[prescriptive_component]],
                       'v5-GS': [power_diff for (power_diff,id,side,mdate) in \
                                 v5.datadiff[prescriptive_component]]}
ylabel = f'{prescriptive_component} Difference ({units}) (Atlas-Golden Standard)'
xlabel = 'Datasets'
fig_title = f'{prescriptive_component} Data Difference (Atlas-Golden Standard)'
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\ValidationSoftware\\alex210722\\v prism'

dictionary_boxplots(gs_vprism_data_dict, 0.66, ylabel, xlabel, fig_title, fig_path)
# %%
v1 = load_validation_data('all_devicesPAL', add_golden=False, add_extra=False, add_lms=False)
# %%
# MEASUREMENT DURATION
dataname = 'all_devices'
v1 = load_validation_data(dataname, add_golden=False, add_extra=True, add_lms=False)