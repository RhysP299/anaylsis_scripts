# -*- coding: utf-8 -*-
"""
Created on Thu May 26 07:17:04 2022
A script to calcualte BRISQUE scores for image quality results used in Theia
FDA approval.
@author: RhysPoolman
"""
import imquality.brisque
import PIL.Image

im_theia = PIL.Image.open("theia.png")
im_theia_2 = PIL.Image.open("theia_2.png")
im_secondary_predicate = PIL.Image.open("secondary_predicate.jpg")

im_theia_brisque = imquality.brisque.score(im_theia)
im_theia_2_brisque = imquality.brisque.score(im_theia_2)
im_secondary_predicate_brisque = \
    imquality.brisque.score(im_secondary_predicate)
