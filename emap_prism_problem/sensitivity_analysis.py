# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:16:11 2021
A script ot plot the results of a sensitivity analysis of the prism meaurement
to variations in the stitch parameters of the rotate and stitch calibration.
@author: RhysPoolman
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from enum import Enum

px2mm = 0.0024


class CalVariableName(Enum):
    """
    The name of the independent variable.
    """
    xxxdx1 = "#xxxdx1"
    xxxdx2 = "#xxxdx2"
    xxxdy = "#xxxdy"


class PrismVariableName(Enum):
    """
    The name of the depenent variable.
    """
    prism_l = "prism_l"
    base_l = "base_l"
    prism_r = "prism_r"
    base_r = "base_r"


class PrismVariableNameHorizontalVertical(Enum):
    """
    The namee of the depenent variable.
    """
    horizontal_l = "horizontal_l"
    vertical_l = "vertical_l"
    horizontal_r = "horizontal_r"
    vertical_r = "vertical_r"


class PrismTolerance:
    # class variables
    _single_vision_vertical_tolerance = 0.33  # diopters
    _single_vision_horizontal_tolerance = 0.33  # diopters
    _progressive_vertical_tolerance = 0.33  # diopters
    _progressive_horizontal_tolerance = 0.67  # diopters
    _single_vision_vertical_upper_limits = 3.375  # diopters
    _single_vision_horizontal_upper_limits = 2.75  # diopters
    _progressive_vertical_upper_limits = 3.375  # diopters
    _progressive_horizontal_upper_limits = 3.75  # diopters

    def single_vision_vertical_tolerance(cls, value):
        """
        Returns hard coded values for the single vision vertical tolerance for
        a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._single_vision_vertical_tolerance \
                if value <= cls._single_vision_vertical_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._single_vision_vertical_tolerance \
                    if entry <= cls._single_vision_vertical_upper_limits else \
                    None
        return rtv

    def single_vision_horizontal_tolerance(cls, value):
        """
        Returns hard coded values for the single vision horizontal tolerance
        for a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._single_vision_horizontal_tolerance \
                if value <= cls._single_vision_horizontal_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._single_vision_horizontal_tolerance \
                    if entry <= cls._single_vision_horizontal_upper_limits\
                    else None
        return rtv

    def progressive_vertical_tolerance(cls, value):
        """
        Returns hard coded values for the progressive vertical tolerance for
        a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._progressive_vertical_tolerance \
                if value <= cls._progressive_vertical_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._progressive_vertical_tolerance \
                    if entry <= cls._progressive_vertical_upper_limits else \
                    None
        return rtv

    def progressive_horizontal_tolerance(cls, value):
        """
        Returns hard coded values for the progressive horizontal tolerance for
        a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._progressive_horizontal_tolerance \
                if value <= cls._progressive_horizontal_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._progressive_horizontal_tolerance \
                    if entry <= cls._progressive_horizontal_upper_limits else \
                    None
        return rtv

    def plot_trace(self, cal_data, cal_variable_name, prism_variable_name,
                   is_progressive=False):
        """
        A function to plot the traces of the difference in prism values as a
        propotion of the prism tolerance allowance against difference of stitct
        parameters to their baseline values.

        Parameters
        ----------
        data : dict of pandas.DataFrames
            The data from the excel spreadsheet.
        cal_variable_name : CalVariableName
            One of the names of the calibration data for an independent
            variable.
        prism_variable_name : PrismVariableNameHorizontalVertical
            One of the names of the prism data for an dependent variable.
        is_progressive : boolean
            If True the toleranceances used in he plot are for progressive
            lenses, otherwise single vision tolerances are used.  Defaults to
            False.

        Returns
        -------
        None.
        """
        baseline = cal_data["baseline"]
        cal_string = cal_variable_name.value
        prism_string = prism_variable_name.value
        plot_data = cal_data[cal_string]
        tol = None
        prism = plot_data[prism_string]
        if prism_string == \
                PrismVariableNameHorizontalVertical.horizontal_l.value:
            tol = self.progressive_horizontal_tolerance(prism) \
                if is_progressive else \
                self.single_vision_horizontal_tolerance(prism)
        elif prism_string == \
                PrismVariableNameHorizontalVertical.vertical_l.value:
            tol = self.progressive_vertical_tolerance(prism) \
                if is_progressive else \
                self.single_vision_vertical_tolerance(prism)
        elif prism_string == \
                PrismVariableNameHorizontalVertical.horizontal_r.value:
            tol = self.progressive_horizontal_tolerance(prism) \
                if is_progressive else \
                self.single_vision_horizontal_tolerance(prism)
        elif prism_string == \
                PrismVariableNameHorizontalVertical.vertical_r.value:
            tol = self.progressive_vertical_tolerance(prism) \
                if is_progressive else \
                self.single_vision_vertical_tolerance(prism)
        else:
            msg = "PrismTolerance.plot_trace: Unrecognised prism_variable_name"
            msg += ", is the value a PrismVariableNameHorizontalVertical enum?"
            raise ValueError(msg)
        x = plot_data.loc[:, cal_string] - baseline.loc[0, cal_string]
        x *= px2mm  # converts from pixels to mm
        y = 100*(plot_data[prism_string] - baseline.loc[0, prism_string])/tol
        plt.plot(x, y, label=cal_string, linestyle='None', marker="o")


def plot_trace(cal_data, cal_variable_name, prism_variable_name,
               calculate_relative=True):
    """
    A function to plot the traces of the difference in prism and base
    values as a function of stitch parameters from the baseline.

    Parameters
    ----------
    data : dict of pandas.DataFrames
        The data from the excel spreadsheet.
    cal_variable_name : CalVariableName
        One of the names of the calibration data for an independent variable.
    prism_variable_name : PrismVariableName
        One of the names of the prism data for an dependent variable.
    calculate_relative : Boolean
        If true then the relative y values are calculate wrt to the baseline,
        otherwise absolute values are plotted.  Defaults to True.

    Returns
    -------
    None.
    """
    baseline = cal_data["baseline"]
    cal_string = cal_variable_name.value
    prism_string = prism_variable_name.value
    plot_data = cal_data[cal_string]
    x = plot_data.loc[:, cal_string] - baseline.loc[0, cal_string]
    x *= px2mm  # converts from pixels to mm
    y = 100*(plot_data[prism_string] - baseline.loc[0, prism_string]) / \
        baseline.loc[0, prism_string] \
        if calculate_relative else \
        (plot_data[prism_string] - baseline.loc[0, prism_string])
    plt.plot(x, y, label=cal_string)


def calculate_components(prism, base):
    """
    Calculates the vertical and horizontal components of the prism value from
    prism magnitude and base angel.

    Parameters
    ----------
    prism : float or collection of floats
        The magnitude of the prism value in diopters.
    base : TYfloat or collection of floats
        The base angle of the prism in degrees.

    Returns
    -------
    prism_horizontal : float or collection of floats
        The horizontal component of the prism values.
    prism_vertical : float or collection of floats
        The vertical component of the prism values.
    """
    base_rad = np.deg2rad(base)
    prism_horizontal = prism*np.cos(base_rad)
    prism_vertical = prism*np.sin(base_rad)
    return prism_horizontal, prism_vertical


# load excel file with data
path = "E:\\eMap\\Prism Problem\\rotate_and_stitch_cal_investigation\\" + \
    "progressive_results\\"
filename = "prism_sensitivity_data.xlsx"
sheets = ["baseline", "#xxxdx1", "#xxxdx2", "#xxxdy"]
loaded_data = {}
for sheet in sheets:
    loaded_data[sheet] = pd.read_excel(path + filename, sheet)

# calculate horizontal and vertical components
data = {}
prism_cols = ["horizontal_l", "vertical_l", "horizontal_r", "vertical_r"]
for key, item in loaded_data.items():
    if key == "baseline":
        prism_l = item.loc[0, PrismVariableName.prism_l.value]
        base_l = item.loc[0, PrismVariableName.base_l.value]
        horizontal_l, vertical_l = calculate_components(prism_l, base_l)
        prism_r = item.loc[0, PrismVariableName.prism_r.value]
        base_r = item.loc[0, PrismVariableName.base_r.value]
        horizontal_r, vertical_r = calculate_components(prism_r, base_r)
        data[key] = \
            pd.DataFrame([[loaded_data[key].loc[0,
                                                CalVariableName.xxxdx1.value],
                          loaded_data[key].loc[0,
                                               CalVariableName.xxxdx2.value],
                          loaded_data[key].loc[0,
                                               CalVariableName.xxxdy.value],
                          horizontal_l, vertical_l, horizontal_r, vertical_r]],
                         columns=["#xxxdx1", "#xxxdx2", "#xxxdy"] + prism_cols)
    else:
        prism_l = item.loc[:, PrismVariableName.prism_l.value].values
        base_l = item.loc[:, PrismVariableName.base_l.value].values
        horizontal_l, vertical_l = calculate_components(prism_l, base_l)
        prism_r = item.loc[:, PrismVariableName.prism_r.value].values
        base_r = item.loc[:, PrismVariableName.base_r.value].values
        horizontal_r, vertical_r = calculate_components(prism_r, base_r)
        values = np.array([loaded_data[key].loc[:, key].values, horizontal_l,
                           vertical_l, horizontal_r, vertical_r]).T
        data[key] = pd.DataFrame(values, columns=[key] + prism_cols)

# create tolerance values object
tols = PrismTolerance()

# plot prism data for horizontal shift left lens
plt.rc("font", size=22)
plt.figure("Left Lens Horizontal Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdx1,
                PrismVariableNameHorizontalVertical.horizontal_l, True)
tols.plot_trace(data, CalVariableName.xxxdx2,
                PrismVariableNameHorizontalVertical.horizontal_l, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot base data for horizontal shift left lens
plt.figure("Left Lens Vertical Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdx1,
                PrismVariableNameHorizontalVertical.vertical_l, True)
tols.plot_trace(data, CalVariableName.xxxdx2,
                PrismVariableNameHorizontalVertical.vertical_l, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot prism data for horizontal shift right lens
plt.figure("Right Lens Horizontal Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdx1,
                PrismVariableNameHorizontalVertical.horizontal_r, True)
tols.plot_trace(data, CalVariableName.xxxdx2,
                PrismVariableNameHorizontalVertical.horizontal_r, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot base data for horizontal shift right lens
plt.figure("Right Lens Vertical Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdx1,
                PrismVariableNameHorizontalVertical.vertical_r, True)
tols.plot_trace(data, CalVariableName.xxxdx2,
                PrismVariableNameHorizontalVertical.vertical_r, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot prism data for horizontal shift left lens
plt.figure("Left Horizontal Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdy,
                PrismVariableNameHorizontalVertical.horizontal_l, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot prism data for horizontal shift left lens
plt.figure("Right Horizontal Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdy,
                PrismVariableNameHorizontalVertical.horizontal_r, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot base data for horizontal shift left lens
plt.figure("Left Lens Vertical Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdy,
                PrismVariableNameHorizontalVertical.vertical_l, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot base data for horizontal shift left lens
plt.figure("Right Lens Vertical Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, CalVariableName.xxxdy,
                PrismVariableNameHorizontalVertical.vertical_r, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
