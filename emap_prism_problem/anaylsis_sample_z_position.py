# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 09:59:29 2021
Calcualtes the difflection of the chief ray vs the tilt angle of the sample.
This script work specifically with eMap_2_TC13056_plate_sample.zmx.  THE PRISM
CALCULATION WILL GIVE BAD RESULTS WITH ANOTHER FILE
@author: RhysPoolman
"""
import clr, os, winreg
from itertools import islice
import numpy as np
import matplotlib.pyplot as plt

# This boilerplate requires the 'pythonnet' module.
# The following instructions are for installing the 'pythonnet' module via pip:
#    1. Ensure you are running Python 3.4, 3.5, 3.6, or 3.7. PythonNET does
#       not work with Python 3.8 yet.
#    2. Install 'pythonnet' from pip via a command prompt (type 'cmd' from the
#       start menu or press Windows + R and type 'cmd' then enter)
#
#        python -m pip install pythonnet

# determine the Zemax working directory
aKey = winreg.OpenKey(winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER),
                      r"Software\Zemax", 0, winreg.KEY_READ)
zemaxData = winreg.QueryValueEx(aKey, 'ZemaxRoot')
NetHelper = os.path.join(os.sep, zemaxData[0],
                         r'ZOS-API\Libraries\ZOSAPI_NetHelper.dll')
winreg.CloseKey(aKey)

# add the NetHelper DLL for locating the OpticStudio install folder
clr.AddReference(NetHelper)
import ZOSAPI_NetHelper

pathToInstall = ''
# uncomment the following line to use a specific instance of the ZOS-API
# assemblies
# pathToInstall = r'C:\C:\Program Files\Zemax OpticStudio'

# connect to OpticStudio
success = ZOSAPI_NetHelper.ZOSAPI_Initializer.Initialize(pathToInstall)

zemaxDir = ''
if success:
    zemaxDir = ZOSAPI_NetHelper.ZOSAPI_Initializer.GetZemaxDirectory()
    print('Found OpticStudio at:   %s' + zemaxDir)
else:
    raise Exception('Cannot find OpticStudio')

# load the ZOS-API assemblies
clr.AddReference(os.path.join(os.sep, zemaxDir, r'ZOSAPI.dll'))
clr.AddReference(os.path.join(os.sep, zemaxDir, r'ZOSAPI_Interfaces.dll'))
import ZOSAPI

TheConnection = ZOSAPI.ZOSAPI_Connection()
if TheConnection is None:
    raise Exception("Unable to intialize NET connection to ZOSAPI")

TheApplication = TheConnection.ConnectAsExtension(0)
if TheApplication is None:
    raise Exception("Unable to acquire ZOSAPI application")

if TheApplication.IsValidLicenseForAPI is False:
    raise Exception("License is not valid for ZOSAPI use.  " +
                    "Make sure you have enabled " +
                    "'Programming > Interactive Extension' from the " +
                    "OpticStudio GUI.")

TheSystem = TheApplication.PrimarySystem
if TheSystem is None:
    raise Exception("Unable to acquire Primary system")


def transpose(data):
    """Transposes a 2D list (Python3.x or greater).

    Useful for converting mutli-dimensional line series (i.e. FFT PSF)

    Parameters
    ----------
    data      : Python native list (if using System.Data[,] object reshape
                first)

    Returns
    -------
    res       : transposed 2D list
    """
    if type(data) is not list:
        data = list(data)
    return list(map(list, zip(*data)))


def reshape(data, x, y, transpose=False):
    """Converts a System.Double[,] to a 2D list for plotting or post
    processing

    Parameters
    ----------
    data      : System.Double[,] data directly from ZOS-API
    x         : x width of new 2D list [use var.GetLength(0) for dimension]
    y         : y width of new 2D list [use var.GetLength(1) for dimension]
    transpose : transposes data; needed for some multi-dimensional line series
                data

    Returns
    -------
    res       : 2D list; can be directly used with Matplotlib or converted to
                a numpy array using numpy.asarray(res)
    """
    if type(data) is not list:
        data = list(data)
    var_lst = [y] * x
    it = iter(data)
    res = [list(islice(it, i)) for i in var_lst]
    if transpose:
        return transpose(res)
    return res


def trace_ray_with_sample_at_specified_z(hx, hy, px, py, sample_z_pos):
    """
    Traces a ray and recovers the x and y coordinates on the image plane for
    a sample at a specific z postion.

    Parameters
    ----------
    hx : float
        The x-coordinate of the field point from which the ray is traced.
    hy :  float
        The y-coordinate of the field point from which the ray is traced.
    px : float
        The x-coordinate of the pupil location from which the ray is traced.
    py : float
        The y-coordinate of the pupil location from which the ray is traced.
    sample_z_pos : float
        The postion of the sample along the z-axis.

    Returns
    -------
    image_coordinate : tuple of floats
        The x and y coordaintes of the incident ray on the image plane.
    """
    # change thickness on object plane to move sample
    surface_0.Thickness = sample_position

    # get results after tilt change
    return (mfe.GetOperandValue(reax, surface, wave, hx, hy, px, py, 0.0, 0.0),
            mfe.GetOperandValue(reay, surface, wave, hx, hy, px, py, 0.0, 0.0))


def prism_percentage_diff(measured, designed):
    """
    Calculates the percentage difference between the measured and designed
    prism diopters.

    Parameters
    ----------
    measured : numpy.ndarray of floats
        The prism dioptres measured from prism in the OpticStudio simulation.
    designed : numpy.ndarray of floats
        The prism diopters designed into the prism in the OpticStudio
        simulation.

    Returns
    -------
    numpy.ndarray of floats
        The percentage difference between the inputs.
    """
    return 100*(measured/designed - 1.0)


print('Connected to OpticStudio')

# The connection should now be ready to use.  For example:
print('Serial #: ', TheApplication.SerialCode)

# set up variables
screen_telecentric_lens_distance = 204.28  # mm
top_focus_box = 181.05  # mm
bottom_focus_box = 134.55  # mm
start_position = screen_telecentric_lens_distance - top_focus_box
end_position = screen_telecentric_lens_distance - bottom_focus_box
num_positions = 50
num_pupil_locations = 11
py = 0.09
start_pupil_loc = py
end_pupil_loc = py + 0.02
sample_positions = np.linspace(start_position, end_position,
                               num_positions)  # independent variable
image_coordinates = np.empty((num_pupil_locations,
                              num_positions, 2))  # dependent variable

# set file to operate on
path = "E:\\eMap\\OpticStudio Model\\"
zmx_filename = "eMap_2_TC13056_plate_sample.zmx"

# set reay merit function operand field and pupil parameters
mfe = TheSystem.MFE
reax = ZOSAPI.Editors.MFE.MeritOperandType.REAX
reay = ZOSAPI.Editors.MFE.MeritOperandType.REAY
surface = 11  # image plane
wave = 2  # mid-spectrum value
Hx = 0.0
Hy = 1.0
Px = 0.0
Py = np.linspace(start_pupil_loc, end_pupil_loc, num_pupil_locations) \
    if num_pupil_locations > 1 \
    else np.array([py])
chief_ray_index = np.where(Py == py)[0][0]

# hard code initial position from start of deapth of focus box in drawings
surface_0 = TheSystem.LDE.GetSurfaceAt(0)  # object plane
initial_position = 19.95  # mm
print(initial_position)

# alter the z position on the optical flat through depth of focus box
sample_positions_mesh, Py_mesh = np.meshgrid(sample_positions, Py)
for ii in range(num_pupil_locations):
    for jj in range(num_positions):
        py = Py_mesh[ii, jj]
        sample_position = sample_positions_mesh[ii, jj]
        image_coordinates[ii, jj, 0], image_coordinates[ii, jj, 1] = \
            trace_ray_with_sample_at_specified_z(Hx, Hy, Px, py,
                                                 sample_position)

# reset position
surface_0.Thickness = initial_position
print(surface_0.Thickness)

# calculate prism diopters on prism
surface_2 = TheSystem.LDE.GetSurfaceAt(2)
y_tilt_cell_index = ZOSAPI.Editors.LDE.SurfaceColumn.Par2
surface_2_tan_of_tilt_angle_wrt_y_axis = \
    surface_2.GetSurfaceCell(y_tilt_cell_index).DoubleValue
surface_2_tilt_angle_wrt_y_axis = \
    np.arctan(surface_2_tan_of_tilt_angle_wrt_y_axis)
prism_apex_angle = 2.0*surface_2_tilt_angle_wrt_y_axis
indx = ZOSAPI.Editors.MFE.MeritOperandType.INDX
index = mfe.GetOperandValue(indx, 2, wave, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
angular_deviation = -(index - 1)*prism_apex_angle
design_prism_diopter = 100*np.tan(np.abs(angular_deviation))

# get optical system parameters
pmag = ZOSAPI.Editors.MFE.MeritOperandType.PMAG
telecentric_magnification = mfe.GetOperandValue(pmag, 0.0, 0.0, 0.0, 0.0,
                                                0.0, 0.0, 0.0, 0.0)
surface_0_thickness = sample_positions_mesh[:, :]
mm_distance_between_lens_and_object_plane = surface_0_thickness
max_field_height = TheSystem.SystemData.Fields.GetField(2).Y
object_height_difference = \
    image_coordinates[:, :, 1]/telecentric_magnification - Hy*max_field_height

# estimate eMap prism diopters
emap_prism_diopters = 100.0 * \
    np.tan(object_height_difference/mm_distance_between_lens_and_object_plane)

# estimate my prism diopters
my_prism_diopters = 100.0 * \
    object_height_difference/mm_distance_between_lens_and_object_plane

# plot graph of prism vs prism position for emap and my own prims calculations
ray_coordinates_string = \
     "H = ({0:.1f}, {1:.1f}), ".format(Hx, Hy) + \
     "P = ({0:.1f}, {1:.1f})".format(Px, Py[chief_ray_index])
title = "Prism for a Ray " + ray_coordinates_string + \
    " as a Function of Sample to Object Plane Distance"
plt.figure(title)
plt.clf()
plt.rc("font", size=22)
plt.title(title)
plt.plot(sample_positions, emap_prism_diopters[chief_ray_index, :],
         label="emap")
plt.plot(sample_positions, my_prism_diopters[chief_ray_index, :],
         label="rhys")
plt.plot(sample_positions,
         design_prism_diopter*np.ones((len(sample_positions))),
         label="design")
plt.xlabel("Z Position of Prism Sample (mm)")
plt.ylabel("Prism from Prentice's Rule (prism diopters)")
plt.legend()

# graph of prism error vs prism position for emap and my own prism calculations
ray_coordinates_string = \
     "H = ({0:.2f}, {1:.2f}), ".format(Hx, Hy) + \
     "P = ({0:.2f}, {1:.2f})".format(Px, Py[chief_ray_index])
title = "Percentage Error in Prism for a Ray \n" + ray_coordinates_string + \
    " as a Function of Sample to Object Plane Distance"
plt.figure(title)
plt.clf()
plt.rc("font", size=22)
plt.title(title)
plt.plot(sample_positions,
         prism_percentage_diff(emap_prism_diopters[chief_ray_index, :],
                               design_prism_diopter),
         label="emap")
plt.plot(sample_positions,
         prism_percentage_diff(my_prism_diopters[chief_ray_index, :],
                               design_prism_diopter),
         label="rhys")
plt.xlabel("Z Position of Prism Sample (mm)")
plt.ylabel("Desinged and Measured Prism Difference (%)")
plt.ylim((-2.0, 2.0))
plt.legend()

# heat map settings
vmin = -2.0
vmax = 2.0

# plot heatmap for my prism vs postion and pupil y-coordinate
title = "Difference Between Designed Prism and My Calculations" + \
    " as a Function of Pupil Y-Coordinate and Prism Z-Position"
plt.figure(title)
plt.clf()
dependent_variable = \
    prism_percentage_diff(my_prism_diopters, design_prism_diopter)
plt.pcolor(Py_mesh, sample_positions_mesh, dependent_variable,
           vmin=vmin, vmax=vmax)
plt.colorbar()
plt.xlabel("Normalized Pupil Coordiantes (Arb. Units)")
plt.ylabel("Sample Z-Axis Position Measured from Screen (mm)")

# plot heatmap for emap prism vs postion and pupil y-coordinate
title = "Difference Between Designed Prism and eMap Calculations" + \
    " as a Function of Pupil Y-Coordinate and Prism Z-Position"
plt.figure(title)
plt.clf()
dependent_variable = \
    prism_percentage_diff(emap_prism_diopters, design_prism_diopter)
plt.pcolor(Py_mesh, sample_positions_mesh, dependent_variable,
           vmin=vmin, vmax=vmax)
plt.colorbar()
plt.xlabel("Normalized Pupil Coordiantes (Arb. Units)")
plt.ylabel("Sample Z-Axis Position Measured from Screen (mm)")
