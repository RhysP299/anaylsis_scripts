# -*- coding: utf-8 -*-
"""
Spyder Editor
A script to access the contense of the manufactoring folder and graph
histograms of the rotate and stitch calibration.
@author: RhysPoolman
"""
import os
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from rotate_and_stitch import RotateStitchCalibration

px2mm = 0.0024


def get_data(data, key):
    """
    Extracts arrays of data from dict created from rotate and stitch cal files

    Parameters
    ----------
    data : nested dict
        Must have keys that correspond to rotate and stitch cal file entries.
    key : string
        Key for the lower level dictionary..

    Returns
    -------
    1D numpy.ndarray
        The requested data.
    """
    data_list = []
    for _, entry in data.items():
        data_list.append(entry.get_data_from_cal_syntax(key))
    return np.array(data_list)


# set paths
path = "E:\\eMap\\eMap Manufacturing\\"
data_folders = os.listdir(path)
sub_path = "\\CalibrationData\\RotateStitch\\"
cal_filename = "DualCameraAlignmentCalibration.dat"

# get manufacturing data
data = {}
error_dict = {}
for folder in data_folders:
    try:
        cal_path = path + folder + sub_path
        date_folders = os.listdir(cal_path)
        date_folder = date_folders[0] + "\\"
        if len(date_folders) > 1:
            dates = [datetime.strptime(date_folder, "%Y%m%d_%H%M")
                     for date_folder in date_folders]
            date_folder = date_folders[np.argmax(dates)] + "\\"
        if date_folder == "":
            raise ValueError("More than one cal attempt on {}".format(folder))
        filename = cal_path + date_folder + cal_filename
        serial_number = filename.split("\\")[3]
        data[serial_number] = \
            RotateStitchCalibration.from_file(filename, serial_number)
    except OSError as err:
        error_dict[filename.split("\\")[3]] = err
        continue

# get field data
path = "E:\\eMap\\Prism Problem\\rotate_and_stitch_investigation" + \
    "\\field_calibration\\rotate_and_stitch\\"
field_cal_data = {}
for folder in os.listdir(path):
    date_folders = os.listdir(path + folder + "\\RotateStitch\\")
    date_folder = date_folders[0] + "\\"
    if len(date_folders) > 1:
        dates = [datetime.strptime(date_folder, "%Y%m%d_%H%M")
                 for date_folder in date_folders]
        date_folder = date_folders[np.argmax(dates)] + "\\"
    if date_folder == "":
        raise ValueError("More than one cal attempt on {}".format(folder))
    filename = path + folder + "\\" + "RotateStitch\\" + date_folder + \
        cal_filename
    field_cal_data[folder] = RotateStitchCalibration.from_file(filename,
                                                               folder)

# extract manufactoring histogram data
dx1_data = get_data(data, "xxxdx1")
dx2_data = get_data(data, "xxxdx2")
dy_data = get_data(data, "xxxdy")
clippling_margin_left_cam1 = get_data(data, "xcm1l")
clippling_margin_left_cam2 = get_data(data, "xcm2l")
clippling_margin_right_cam1 = get_data(data, "xcm1r")
clippling_margin_right_cam2 = get_data(data, "xcm2r")

# get variation study data
variation_path = "C:\\Users\\RhysPoolman\\Downloads\\Jag\\"
variation_dirs = os.listdir(variation_path)
variation_cals = {}
for folder in variation_dirs:
    filename = \
        variation_path + folder + "\\" + os.listdir(variation_path + folder)[0]
    variation_cals[folder] = \
        RotateStitchCalibration.from_file(filename, folder)

# create manufactoring histograms
plt.figure("Histogram of X Translation Calibration Values")
plt.clf()
plt.rc("font", size=22)
plt.hist(dx1_data*px2mm, 50, alpha=0.5, label="Cam 1")
plt.hist(dx2_data*px2mm, 50, alpha=0.5, label="Cam 2")
for key, item in variation_cals.items():
    x = item.get_data_from_cal_syntax("xxxdx1")*px2mm
    plt.plot([x, x], [0.0, 10.0], label=key)
for key, item in variation_cals.items():
    x = item.get_data_from_cal_syntax("xxxdx2")*px2mm
    plt.plot([x, x], [0.0, 10.0], "--", label=key)
plt.legend()
plt.xlabel("X Translation Values (mm)")

plt.figure("Histogram of Y Translation Calibration Values")
plt.clf()
plt.rc("font", size=22)
plt.hist(dy_data*px2mm, 50)
for key, item in variation_cals.items():
    x = item.get_data_from_cal_syntax("xxxdy")*px2mm
    plt.plot([x, x], [0.0, 10.0], label=key)
plt.legend()
plt.xlabel("Y Translation Values (mm)")

plt.figure("Histogram of Left Clipping Margin")
plt.clf()
plt.rc("font", size=22)
plt.hist(clippling_margin_left_cam1*px2mm, 30, alpha=0.5, label="Cam 1")
plt.hist(clippling_margin_left_cam2*px2mm, 30, alpha=0.5, label="Cam 2")
plt.legend()
plt.xlabel("Left Clipping Margin (mm)")

plt.figure("Histogram of Right Clipping Margin")
plt.clf()
plt.rc("font", size=22)
plt.hist(clippling_margin_right_cam1*px2mm, 30, alpha=0.5, label="Cam 1")
plt.hist(clippling_margin_right_cam2*px2mm, 30, alpha=0.5, label="Cam 2")
plt.legend()
plt.xlabel("Right Clipping Margin (mm)")

# get field data
#path = "E:\\eMap\\field_calibration\\rotate_and_stitch\\"
field_cal_data = {}
manufactoring_data = {}
for folder in os.listdir(path):
    date_folders = os.listdir(path + folder + "\\RotateStitch\\")
    date_folder = date_folders[0] + "\\"
    if len(date_folders) > 1:
        dates = [datetime.strptime(date_folder, "%Y%m%d_%H%M")
                 for date_folder in date_folders]
        date_folder = date_folders[np.argmax(dates)] + "\\"
    if date_folder == "":
        raise ValueError("More than one cal attempt on {}".format(folder))
    filename = path + folder + "\\" + "RotateStitch\\" + date_folder + \
        cal_filename
    field_cal_data[folder] = RotateStitchCalibration.from_file(filename,
                                                               folder)
    manufactoring_data[folder] = data[folder]

# calculate field and manufactoring differences.
diffs = {}
for serial_number, entry in field_cal_data.items():
    diffs[serial_number] = entry - data[serial_number]
dx1_diffs = get_data(diffs, "xxxdx1")
dx2_diffs = get_data(diffs, "xxxdx2")
dy_diffs = get_data(diffs, "xxxdy")
dx1_man = get_data(manufactoring_data, "xxxdx1")
dx2_man = get_data(manufactoring_data, "xxxdx2")
dy_man = get_data(manufactoring_data, "xxxdy")
dx1_field = get_data(field_cal_data, "xxxdx1")
dx2_field = get_data(field_cal_data, "xxxdx2")
dy_field = get_data(field_cal_data, "xxxdy")
timestamp_diff = get_data(diffs, "xxxxt")
serial_number_diff = get_data(diffs, "serial#")
serial_numbers = list(diffs.keys())
x = np.arange(len(serial_numbers))

plt.figure("Horizontal Delta Diffs")
plt.clf()
plt.scatter(x, dx1_diffs*px2mm, label="Cam1")
plt.scatter(x, dx2_diffs*px2mm, label="Cam2")
ax = plt.gca()
ax.xaxis.set_ticks(np.arange(len(serial_numbers)))
ax.xaxis.set_ticklabels(serial_numbers, rotation=15, ha="right")
plt.legend(numpoints=1)
plt.ylabel("Difference from Initial Horizontal Stitch Value (mm)")

plt.figure("Vertical Delta Diffs")
plt.clf()
plt.scatter(x, dy_diffs*px2mm)
ax = plt.gca()
ax.xaxis.set_ticks(np.arange(len(serial_numbers)))
ax.xaxis.set_ticklabels(serial_numbers, rotation=15, ha="right")
plt.ylabel("Difference from Initial Vertical Stitch Value (mm)")

plt.figure("Percentage Horizontal Delta Diffs")
plt.clf()
plt.scatter(x, np.abs(dx1_diffs/dx1_man*100), label="Cam1")
plt.scatter(x, np.abs(dx2_diffs/dx2_man*100), label="Cam2")
ax = plt.gca()
ax.xaxis.set_ticks(np.arange(len(serial_numbers)))
ax.xaxis.set_ticklabels(serial_numbers, rotation=15, ha="right")
plt.ylabel("Difference from Initial Cal (%)")
plt.legend(numpoints=1)

plt.figure("Percentage Vertical Delta Diffs")
plt.clf()
plt.scatter(x, np.abs(dy_diffs/dy_man*100))
ax = plt.gca()
ax.set_yscale("log")
plt.ylim(1, 10**3)
ax.xaxis.set_ticks(np.arange(len(serial_numbers)))
plt.ylabel("Difference from Initial Cal (%)")
ax.xaxis.set_ticklabels(serial_numbers, rotation=15, ha="right")
