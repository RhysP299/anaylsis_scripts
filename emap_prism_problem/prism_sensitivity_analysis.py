# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:16:11 2021
A script ot plot the results of a sensitivity analysis of the prism meaurement
to variations in the stitch parameters of the rotate and stitch calibration.
@author: RhysPoolman
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from enum import Enum
import tools.tolerance as tolerances
import tools.utilities as utl


class PrismVariableName(Enum):
    """
    The name of the depenent variable.
    """
    prism_l = "prism_l"
    base_l = "base_l"
    prism_r = "prism_r"
    base_r = "base_r"


# load excel file with data
path = "E:\\eMap\\Prism Problem\\rotate_and_stitch_cal_investigation\\" + \
    "progressive_results\\"
filename = "prism_sensitivity_data.xlsx"
sheets = ["baseline", "#xxxdx1", "#xxxdx2", "#xxxdy"]
loaded_prism_data = {}
for sheet in sheets:
    loaded_prism_data[sheet] = pd.read_excel(path + filename, sheet)

# calculate horizontal and vertical components
data = {}
prism_cols = ["horizontal_l", "vertical_l", "horizontal_r", "vertical_r"]
for key, item in loaded_prism_data.items():
    if key == "baseline":
        prism_l = item.loc[0, PrismVariableName.prism_l.value]
        base_l = item.loc[0, PrismVariableName.base_l.value]
        horizontal_l, vertical_l = utl.calculate_components(prism_l, base_l)
        prism_r = item.loc[0, PrismVariableName.prism_r.value]
        base_r = item.loc[0, PrismVariableName.base_r.value]
        horizontal_r, vertical_r = utl.calculate_components(prism_r, base_r)
        data[key] = \
            pd.DataFrame([[loaded_prism_data[key].loc[0,
                                                      utl.CalVariableName.xxxdx1.value],
                          loaded_prism_data[key].loc[0,
                                                     utl.CalVariableName.xxxdx2.value],
                          loaded_prism_data[key].loc[0,
                                                     utl.CalVariableName.xxxdy.value],
                          horizontal_l, vertical_l, horizontal_r, vertical_r]],
                         columns=["#xxxdx1", "#xxxdx2", "#xxxdy"] + prism_cols)
    else:
        prism_l = item.loc[:, PrismVariableName.prism_l.value].values
        base_l = item.loc[:, PrismVariableName.base_l.value].values
        horizontal_l, vertical_l = utl.calculate_components(prism_l, base_l)
        prism_r = item.loc[:, PrismVariableName.prism_r.value].values
        base_r = item.loc[:, PrismVariableName.base_r.value].values
        horizontal_r, vertical_r = utl.calculate_components(prism_r, base_r)
        values = np.array([loaded_prism_data[key].loc[:, key].values,
                           horizontal_l, vertical_l,
                           horizontal_r, vertical_r]).T
        data[key] = pd.DataFrame(values, columns=[key] + prism_cols)

# create tolerance values object
tols = tolerances.PrismTolerance()

# plot prism data for horizontal shift left lens
plt.rc("font", size=22)
plt.figure("Left Lens Horizontal Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdx1,
                tols.PrismVariableNameHorizontalVertical.horizontal_l, True)
tols.plot_trace(data, utl.CalVariableName.xxxdx2,
                tols.PrismVariableNameHorizontalVertical.horizontal_l, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot base data for horizontal shift left lens
plt.figure("Left Lens Vertical Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdx1,
                tols.PrismVariableNameHorizontalVertical.vertical_l, True)
tols.plot_trace(data, utl.CalVariableName.xxxdx2,
                tols.PrismVariableNameHorizontalVertical.vertical_l, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot prism data for horizontal shift right lens
plt.figure("Right Lens Horizontal Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdx1,
                tols.PrismVariableNameHorizontalVertical.horizontal_r, True)
tols.plot_trace(data, utl.CalVariableName.xxxdx2,
                tols.PrismVariableNameHorizontalVertical.horizontal_r, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot base data for horizontal shift right lens
plt.figure("Right Lens Vertical Prism Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdx1,
                tols.PrismVariableNameHorizontalVertical.vertical_r, True)
tols.plot_trace(data, utl.CalVariableName.xxxdx2,
                tols.PrismVariableNameHorizontalVertical.vertical_r, True)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot prism data for horizontal shift left lens
plt.figure("Left Horizontal Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdy,
                tols.PrismVariableNameHorizontalVertical.horizontal_l, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot prism data for horizontal shift left lens
plt.figure("Right Horizontal Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdy,
                tols.PrismVariableNameHorizontalVertical.horizontal_r, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Horizontal Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot base data for horizontal shift left lens
plt.figure("Left Lens Vertical Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdy,
                tols.PrismVariableNameHorizontalVertical.vertical_l, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot base data for horizontal shift left lens
plt.figure("Right Lens Vertical Prism Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(data, utl.CalVariableName.xxxdy,
                tols.PrismVariableNameHorizontalVertical.vertical_r, True)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Vertical Prism\nas Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
