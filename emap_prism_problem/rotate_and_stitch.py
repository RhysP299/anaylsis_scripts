# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 11:57:02 2021

@author: RhysPoolman
"""
import numpy as np
from datetime import datetime


class RotateStitchCalibration:
    def __init__(self, serial_number, fiden, version, time_stamp,
                 cam_1_rotation_matrix, cam_2_rotation_matrix,
                 rotated_image_width_cam1, rotated_image_height_cam1,
                 rotated_image_width_cam2, rotated_image_height_cam2,
                 horizontal_delta_cam1, horizontal_delta_cam2, vertical_delta,
                 mm_2_pix_monitor, mm_2_pix_camera, telecentric_magnification,
                 clipping_margin_left_cam1, clipping_margin_right_cam1,
                 clipping_margin_left_cam2, clipping_margin_right_cam2,
                 left_side_camera_label):
        """
        The class constructor.

        Parameters
        ----------
        serial_number : string
            The serial number of the eMap to which the data in the object
            belongs.
        fiden : string
            Name of the file.
        version : int
            The version of the cal file from which the data is created.
        time_stamp : datetime
            The time at which the calibration file was created.
        cam_1_rotation_matrix : 2x3 numpy.ndarray
            The calibration matrix for cam 1.
        cam_2_rotation_matrix : 2x3 numpy.ndarray
            The calibration matrix for cam 2.
        rotated_image_width_cam1 : int
            The width of the image from cam 1 after rotation.
        rotated_image_height_cam1 : int
            The height of the image from cam 1 after rotation.
        rotated_image_width_cam2 : int
            The width of the image from cam 2 after rotation.
        rotated_image_height_cam2 : int
            The height of the image from cam 2 after rotation.
        horizontal_delta_cam1 : int
            The horizontal shift of cam 1 from stitching.
        horizontal_delta_cam2 : int
            The horizontal of cam 2 from stitching.
        vertical_delta : int
            The vertical shift for stitching.
        mm_2_pix_monitor : float
            Factor to convert to mm.
        mm_2_pix_camera : float
            Factor to convert to mm.
        telecentric_magnification : float
            The magnification.
        clipping_margin_left_cam1 : int
            The cam 1 left side clipping margin.
        clipping_margin_right_cam1 : int
            The cam 1 right side clipping margin.
        clipping_margin_left_cam2 : int
            The cam 2 left side clipping margin.
        clipping_margin_right_cam2 : int
            The cam 2 right side clipping margin.
        left_side_camera_label : int
            The label of the left side camera.

        Returns
        -------
        RotateStitchCalibration
            An instance of the class created from valuese passed.
        """
        self._serial_number = serial_number
        self._fiden = fiden
        self._version = version
        self._time_stamp = time_stamp
        self._rotation_matrix_cam1 = cam_1_rotation_matrix
        self._rotation_matrix_cam2 = cam_2_rotation_matrix
        self._rotated_image_width_cam1 = rotated_image_width_cam1
        self._rotated_image_height_cam1 = rotated_image_height_cam1
        self._rotated_image_width_cam2 = rotated_image_width_cam2
        self._rotated_image_height_cam2 = rotated_image_height_cam2
        self._horizontal_delta_cam1 = horizontal_delta_cam1
        self._horizontal_delta_cam2 = horizontal_delta_cam2
        self._vertical_delta = vertical_delta
        self._mm_2_pix_monitor = mm_2_pix_monitor
        self._mm_2_pix_camera = mm_2_pix_camera
        self._telecentric_magnification = telecentric_magnification
        self._clipping_margin_left_cam1 = clipping_margin_left_cam1
        self._clipping_margin_right_cam1 = clipping_margin_right_cam1
        self._clipping_margin_left_cam2 = clipping_margin_left_cam2
        self._clipping_margin_right_cam2 = clipping_margin_right_cam2
        self._left_side_camera_label = left_side_camera_label

    @classmethod
    def from_file(cls, filename, serial_number):
        """
        A constructor for a rotate and stitch calibration object associated
        with an eMap that gets data from the cal file directly

        Parameters
        ----------
        filename : string
            The data loaded from a specified file name.
        serial_number: string
            The serial number of the eMap that the date is taken from.

        Raises
        ------
        ValueError
            If this expection is raised it means the cal file is not correctly
            formated.

        Returns
        -------
        RotateStitchCalibration
            An instance of this class with date from a cal file.
        """
        # setup variables
        fiden = None
        version = None
        time_stamp = None
        cam_1_rotation_matrix = None
        cam_2_rotation_matrix = None
        rotated_image_width_cam1 = None
        rotated_image_height_cam1 = None
        rotated_image_width_cam2 = None
        rotated_image_height_cam2 = None
        horizontal_delta_cam1 = None
        horizontal_delta_cam2 = None
        vertical_delta = None
        mm_2_pix_monitor = None
        mm_2_pix_camera = None
        telecentric_magnification = None
        clipping_margin_left_cam1 = None
        clipping_margin_right_cam1 = None
        clipping_margin_left_cam2 = None
        clipping_margin_right_cam2 = None
        left_side_camera_label = None
        # file variables from file
        with open(filename) as file:
            string_data = file.readlines()
            for line in string_data:
                split_line = line.split(", ")
                if line[0] == ";":
                    continue
                tag = split_line[0][1:]
                if tag[-5:] == "fiden":
                    fiden = split_line[1]
                elif tag == "xxxxv":
                    version = int(split_line[1])
                elif tag == "xxxxt":
                    time_stamp = datetime.strptime(split_line[1][:-3],
                                                   "%Y-%m-%dT%H:%M:%S.%f")
                elif tag == "xxxc1":
                    cam_1_rotation_matrix = np.zeros((2, 6))
                    kk = 1
                    for ii in range(2):
                        for jj in range(3):
                            cam_1_rotation_matrix[ii, jj] = \
                                float(split_line[kk])
                            kk += 1
                elif tag == "xxxc2":
                    cam_2_rotation_matrix = np.zeros((2, 6))
                    kk = 1
                    for ii in range(2):
                        for jj in range(3):
                            cam_2_rotation_matrix[ii, jj] = \
                                float(split_line[kk])
                            kk += 1
                elif tag == "xxrw1":
                    rotated_image_width_cam1 = int(split_line[1])
                elif tag == "xxrh1":
                    rotated_image_height_cam1 = int(split_line[1])
                elif tag == "xxrw2":
                    rotated_image_width_cam2 = int(split_line[1])
                elif tag == "xxrh2":
                    rotated_image_height_cam2 = int(split_line[1])
                elif tag == "xxxdx1":
                    horizontal_delta_cam1 = int(split_line[1])
                elif tag == "xxxdx2":
                    horizontal_delta_cam2 = int(split_line[1])
                elif tag == "xxxdy":
                    vertical_delta = int(split_line[1])
                elif tag == "mmpx0":
                    mm_2_pix_monitor = float(split_line[1])
                elif tag == "mmpx1":
                    mm_2_pix_camera = float(split_line[1])
                elif tag == "lensx":
                    telecentric_magnification = float(split_line[1])
                elif tag == "xcm1l":
                    clipping_margin_left_cam1 = int(split_line[1])
                elif tag == "xcm1r":
                    clipping_margin_right_cam1 = int(split_line[1])
                elif tag == "xcm2l":
                    clipping_margin_left_cam2 = int(split_line[1])
                elif tag == "xcm2r":
                    clipping_margin_right_cam2 = int(split_line[1])
                elif tag == "limcm":
                    left_side_camera_label = int(split_line[1])
                else:
                    msg = \
                        "RotateStitchCalibration.fromfile: {0}".format(tag)
                    msg += " tag not recognised."
                    raise ValueError(msg)
        # create and return object
        return cls(serial_number, fiden, version, time_stamp,
                   cam_1_rotation_matrix, cam_2_rotation_matrix,
                   rotated_image_width_cam1, rotated_image_height_cam1,
                   rotated_image_width_cam2, rotated_image_height_cam2,
                   horizontal_delta_cam1, horizontal_delta_cam2,
                   vertical_delta, mm_2_pix_monitor, mm_2_pix_camera,
                   telecentric_magnification, clipping_margin_left_cam1,
                   clipping_margin_right_cam1, clipping_margin_left_cam2,
                   clipping_margin_right_cam2, left_side_camera_label)

    @classmethod
    def empty(cls):
        """
        Creates and returns an empty instance of the class.

        Returns
        -------
        RotateStitchCalibration
            An empty instance of this class.
        """
        return cls("", "", 0, datetime.min, np.zeros((2, 3)), np.zeros((2, 3)),
                   0, 0, 0, 0, 0, 0, 0, 0.0, 0.0, 1.0,0, 0, 0, 0, 1)

    def get_data_from_cal_syntax(self, tag):
        """
        Returns the data identified by the string used in the cal file syntax
        to tag data.  Note that the tag "serial#" does not appear in the cal
        file.

        Parameters
        ----------
        tag : string
            A label for the type of value to return see EyeTech.Recipes.
            Lensmeter.Calibration.Dual.DualCameraAlignmentCalibrationResults
            for definitions.

        Raises
        ------
        ValueError
            If the tag is not a recognised label then a ValueError is raised.

        Returns
        -------
        rtv : int, string or float
            The value indentified by the tag.
        """
        rtv = None
        if tag == "fiden":
            rtv = self.fiden
        elif tag == "xxxxv":
            rtv = self.version
        elif tag == "xxxxt":
            rtv = self.time_stamp
        elif tag == "xxxc1":
            rtv = self.cam_1_rotation_matrix
        elif tag == "xxxc2":
            rtv = self.cam_2_rotation_matrix
        elif tag == "xxrw1":
            rtv = self.rotated_image_width_cam1
        elif tag == "xxrh1":
            rtv = self.rotated_image_height_cam1
        elif tag == "xxrw2":
            rtv = self.rotated_image_width_cam2
        elif tag == "xxrh2":
            rtv = self.rotated_image_height_cam2
        elif tag == "xxxdx1":
            rtv = self.horizontal_delta_cam1
        elif tag == "xxxdx2":
            rtv = self.horizontal_delta_cam2
        elif tag == "xxxdy":
            rtv = self.vertical_delta
        elif tag == "mmpx0":
            rtv = self.mm_2_pix_monitor
        elif tag == "mmpx1":
            rtv = self.mm_2_pix_camera
        elif tag == "lensx":
            rtv = self.telecentric_magnification
        elif tag == "xcm1l":
            rtv = self.clipping_margin_left_cam1
        elif tag == "xcm1r":
            rtv = self.clipping_margin_right_cam1
        elif tag == "xcm2l":
            rtv = self.clipping_margin_left_cam2
        elif tag == "xcm2r":
            rtv = self.clipping_margin_right_cam2
        elif tag == "limcm":
            rtv = self.left_side_camera_label
        elif tag == "serial#":
            rtv = self.serial_number
        else:
            raise ValueError("tag not recognised.")
        return rtv

    def __sub__(self, other):
        """
        Finds the difference between all numerical attributes and indicates
        the output is a diff in the serial_number attribute.  The fiden is
        attribute is left empty.

        Parameters
        ----------
        other : RotateStitchCalibration
            The other instance of RotateStitchCalibration with which the
            difference proceeds.

        Returns
        -------
        diff : RotateStitchCalibration
            The difference instance.
        """
        diff = RotateStitchCalibration.empty()
        diff._serial_number = \
            self.serial_number +"-" + other.serial_number + "_diff" \
            if self.serial_number != other.serial_number else \
            self.serial_number + "_diff"
        diff._fiden = ""
        diff._version = self.version - other.version
        diff._time_stamp = self._time_stamp - other._time_stamp
        diff._rotation_matrix_cam1 = self.rotation_matrix_cam1 - \
            other.rotation_matrix_cam1
        diff._rotation_matrix_cam2 = self.rotation_matrix_cam2 - \
            other.rotation_matrix_cam2
        diff._rotated_image_width_cam1 = self.rotated_image_width_cam1 - \
            other.rotated_image_width_cam1
        diff._rotated_image_height_cam1 = self.rotated_image_height_cam1 - \
            other.rotated_image_height_cam1
        diff._rotated_image_width_cam2 = self.rotated_image_width_cam2 - \
            other.rotated_image_width_cam2
        diff._rotated_image_height_cam2 = self.rotated_image_height_cam2 - \
            other.rotated_image_height_cam2
        diff._horizontal_delta_cam1 = self.horizontal_delta_cam1 - \
            other.horizontal_delta_cam1
        diff._horizontal_delta_cam2 = self.horizontal_delta_cam2 - \
            other.horizontal_delta_cam2
        diff._vertical_delta = self.vertical_delta - other.vertical_delta
        diff._mm_2_pix_monitor = self.mm_2_pix_monitor - other.mm_2_pix_monitor
        diff._mm_2_pix_cameras = self.mm_2_pix_cameras - other.mm_2_pix_cameras
        diff._telecentric_magnification = self.telecentric_magnification - \
            other.telecentric_magnification
        diff._clipping_margin_left_cam1 = self.clipping_margin_left_cam1 - \
            other.clipping_margin_left_cam1
        diff._clipping_margin_right_cam1 = self.clipping_margin_right_cam1 - \
            other.clipping_margin_right_cam1
        diff._clipping_margin_left_cam2 = self.clipping_margin_left_cam2 - \
            other.clipping_margin_left_cam2
        diff._clipping_margin_right_cam2 = self.clipping_margin_right_cam2 - \
            other.clipping_margin_right_cam2
        diff._left_side_camera_label = self.left_side_camera_label - \
            other.left_side_camera_label
        return diff

    @property
    def serial_number(self):
        """
        Gets the serial number

        Returns
        -------
        string
            The serial number of the eMap to which the data in the object
            belongs.
        """
        return self._serial_number

    @property
    def fiden(self):
        """
        Gets the file identity value.

        Returns
        -------
        string
            Name of the file.
        """
        return self._fiden

    @property
    def version(self):
        """
        Gets the version of the cal file.

        Returns
        -------
        int
            The version of the cal file from which the data is created.
        """
        return self._version

    @property
    def time_stamp(self):
        """
        Gets the time stamp of the cal file.

        Returns
        -------
        datetime
            The time at which the calibration file was created.
        """
        return self._time_stamp

    @property
    def rotation_matrix_cam1(self):
        """
        Gets the rotation matrix for the cam 1 image.

        Returns
        -------
        2x3 numpy.ndarray
            The calibration matrix for cam 1.
        """
        return self._rotation_matrix_cam1

    @property
    def rotation_matrix_cam2(self):
        """
        Gets the rotation matrix for the cam 2 image.

        Returns
        -------
        2x3 numpy.ndarray
            The calibration matrix for cam 2.
        """
        return self._rotation_matrix_cam2

    @property
    def rotated_image_width_cam1(self):
        """
        Gets the rotated image width for the Cam1 image.

        Returns
        -------
        int
            The width of the image from cam 1 after rotation.
        """
        return self._rotated_image_width_cam1

    @property
    def rotated_image_height_cam1(self):
        """
        Gets the rotated image height for the Cam1 image.

        Returns
        -------
        int
            The height of the image from cam 1 after rotation.
        """
        return self._rotated_image_height_cam1

    @property
    def rotated_image_width_cam2(self):
        """
        Gets the rotated image width for the Cam2 image.

        Returns
        -------
        int
            The width of the image from cam 2 after rotation.
        """
        return self._rotated_image_width_cam2

    @property
    def rotated_image_height_cam2(self):
        """
        Gets the rotated image height for the Cam2 image.

        Returns
        -------
        int
            The height of the image from cam 2 after rotation.
        """
        return self._rotated_image_height_cam2

    @property
    def horizontal_delta_cam1(self):
        """
        Gets the horizontal shift of the cam 1 image relative to the center of
        the bounding box.

        Returns
        -------
        int
            The horizontal shift of cam 1 from stitching.
        """
        return self._horizontal_delta_cam1

    @property
    def horizontal_delta_cam2(self):
        """
        Gets the horizontal shift of the cam 2 image relative to the center of
        the bounding box.

        Returns
        -------
        int
            The horizontal of cam 2 from stitching.
        """
        return self._horizontal_delta_cam2

    @property
    def vertical_delta(self):
        """
        Gets the vertical shift of the images relative to the center of
        the bounding box.

        Returns
        -------
        int
            The vertical shift for stitching.
        """
        return self._vertical_delta

    @property
    def mm_2_pix_monitor(self):
        """
        Gets the multiplicative factor to convert from pixel count to
        milliemeters for the monitor.

        Returns
        -------
        float
            Factor to convert to mm.
        """
        return self._mm_2_pix_monitor

    @property
    def mm_2_pix_cameras(self):
        """
        Gets the multiplicative factor to convert from pixel count to
        milliemeters for the cameras.

        Returns
        -------
        float
            Factor to convert to mm.
        """
        return self._mm_2_pix_camera

    @property
    def telecentric_magnification(self):
        """
        Gets the magnification factor for the telecentric lens(es) installed
        in the eMap.

        Returns
        -------
        float
            The magnification.
        """
        return self._telecentric_magnification

    @property
    def clipping_margin_left_cam1(self):
        """
        Gets the clipping margin for the left side of the image created by cam
        1.

        Returns
        -------
        int
            The cam 1 left side clipping margin.
        """
        return self._clipping_margin_left_cam1

    @property
    def clipping_margin_right_cam1(self):
        """
        Gets the clipping margin for the right side of the image created by cam
        1.

        Returns
        -------
        int
            The cam 1 right side clipping margin.
        """
        return self._clipping_margin_right_cam1

    @property
    def clipping_margin_left_cam2(self):
        """
        Gets the clipping margin for the left side of the image created by cam
        2.

        Returns
        -------
        int
            The cam 2 left side clipping margin.
        """
        return self._clipping_margin_left_cam2

    @property
    def clipping_margin_right_cam2(self):
        """
        Gets the clipping margin for the right side of the image created by cam
        2.

        Returns
        -------
        int
            The cam 2 right side clipping margin.
        """
        return self._clipping_margin_right_cam2

    @property
    def left_side_camera_label(self):
        """
        Gets the flag that indicates whether cam 1 or cam 2 is on the left.  If
        cam 1 is on the left then 1 is returned otherwised it's 2.

        Returns
        -------
        int
            The label of the left side camera.
        """
        return self._left_side_camera_label
