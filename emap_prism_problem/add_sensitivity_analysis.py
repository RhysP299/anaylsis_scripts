# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:16:11 2021
A script ot plot the results of a sensitivity analysis of the prism meaurement
to variations in the stitch parameters of the rotate and stitch calibration.
@author: RhysPoolman
"""
import pandas as pd
import matplotlib.pyplot as plt
import tools.tolerance as tolerances
import tools.utilities as utl

# load excel file with data
path = "E:\\eMap\\Prism Problem\\rotate_and_stitch_cal_investigation\\" + \
    "progressive_results\\"
filename = "add_sensitivity_data.xlsx"
sheets = ["baseline", "#xxxdx1", "#xxxdx2", "#xxxdy"]
loaded_add_data = {}
for sheet in sheets:
    loaded_add_data[sheet] = pd.read_excel(path + filename, sheet)

# create tolerance values object
tols = tolerances.AddTolerance()

# plot axis data for left lens against horizontal shift
plt.rc("font", size=22)
plt.figure("Left Lens Add Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(loaded_add_data, utl.CalVariableName.xxxdx1,
                tols.AddVariableName.add_l)
tols.plot_trace(loaded_add_data, utl.CalVariableName.xxxdx2,
                tols.AddVariableName.add_l)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Add as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot axis data for right lens against horizontal shift
plt.figure("Right Lens Add Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(loaded_add_data, utl.CalVariableName.xxxdx1,
                tols.AddVariableName.add_r)
tols.plot_trace(loaded_add_data, utl.CalVariableName.xxxdx2,
                tols.AddVariableName.add_r)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Add as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot axis data for left lens against vertical shift
plt.figure("Left Lens Add Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(loaded_add_data, utl.CalVariableName.xxxdy,
                tols.AddVariableName.add_l)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Add as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot axis data for right lens against horizontal shift
plt.figure("Right Lens Add Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(loaded_add_data, utl.CalVariableName.xxxdy,
                tols.AddVariableName.add_r)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Add as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
