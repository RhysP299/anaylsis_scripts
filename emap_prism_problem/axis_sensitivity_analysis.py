# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 09:16:11 2021
A script ot plot the results of a sensitivity analysis of the prism meaurement
to variations in the stitch parameters of the rotate and stitch calibration.
@author: RhysPoolman
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from enum import Enum
import tools.tolerance as tolerances
import tools.utilities as utl

# load excel file with data
path = "E:\\eMap\\Prism Problem\\rotate_and_stitch_cal_investigation\\" + \
    "single_vision_results\\"
filename = "axis_sensitivity_data.xlsx"
sheets = ["baseline", "#xxxdx1", "#xxxdx2", "#xxxdy"]
loaded_axis_data = {}
for sheet in sheets:
    loaded_axis_data[sheet] = pd.read_excel(path + filename, sheet)

# create tolerance values object
tols = tolerances.AxisTolerance()

# plot axis data for left lens against horizontal shift
#cylinder_power = 0.0  # taken from baseline measurment
plt.rc("font", size=22)
plt.figure("Left Lens Axis Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(loaded_axis_data, utl.CalVariableName.xxxdx1,
                tols.AxisVariableName.axis_l)
tols.plot_trace(loaded_axis_data, utl.CalVariableName.xxxdx2,
                tols.AxisVariableName.axis_l)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Axis as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot axis data for right lens against horizontal shift
plt.figure("Right Lens Axis Values vs. Horizontal Stitch")
plt.clf()
tols.plot_trace(loaded_axis_data, utl.CalVariableName.xxxdx1,
                tols.AxisVariableName.axis_r)
tols.plot_trace(loaded_axis_data, utl.CalVariableName.xxxdx2,
                tols.AxisVariableName.axis_r)
plt.xlabel("Difference in Horizontal Shift from Baseline (mm)")
ylabel = \
    "Difference of Axis as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
plt.legend()

# plot axis data for left lens against vertical shift
plt.figure("Left Lens Axis Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(loaded_axis_data, utl.CalVariableName.xxxdy,
                tols.AxisVariableName.axis_l)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Axis as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)

# plot axis data for right lens against horizontal shift
plt.figure("Right Lens Axis Values vs. Vertical Stitch")
plt.clf()
tols.plot_trace(loaded_axis_data, utl.CalVariableName.xxxdy,
                tols.AxisVariableName.axis_r)
plt.xlabel("Difference in Vertical Shift from Baseline (mm)")
ylabel = \
    "Difference of Axis as Propotion of Tolerance Allowance (%)"
plt.ylabel(ylabel)
