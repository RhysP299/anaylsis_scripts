# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 09:59:29 2021
Calcualtes the difflection of the chief ray vs the tilt angle of the sample.
This script work specifically with eMap_2_TC13056_plate_sample.zmx.  THE PRISM
CALCULATION WILL GIVE BAD RESULTS WITH ANOTHER FILE
@author: RhysPoolman
"""
import clr, os, winreg
from itertools import islice
import numpy as np
import matplotlib.pyplot as plt

# This boilerplate requires the 'pythonnet' module.
# The following instructions are for installing the 'pythonnet' module via pip:
#    1. Ensure you are running Python 3.4, 3.5, 3.6, or 3.7. PythonNET does
#       not work with Python 3.8 yet.
#    2. Install 'pythonnet' from pip via a command prompt (type 'cmd' from the
#       start menu or press Windows + R and type 'cmd' then enter)
#
#        python -m pip install pythonnet

# determine the Zemax working directory
aKey = winreg.OpenKey(winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER),
                      r"Software\Zemax", 0, winreg.KEY_READ)
zemaxData = winreg.QueryValueEx(aKey, 'ZemaxRoot')
NetHelper = os.path.join(os.sep, zemaxData[0],
                         r'ZOS-API\Libraries\ZOSAPI_NetHelper.dll')
winreg.CloseKey(aKey)

# add the NetHelper DLL for locating the OpticStudio install folder
clr.AddReference(NetHelper)
import ZOSAPI_NetHelper

pathToInstall = ''
# uncomment the following line to use a specific instance of the ZOS-API
# assemblies
# pathToInstall = r'C:\C:\Program Files\Zemax OpticStudio'

# connect to OpticStudio
success = ZOSAPI_NetHelper.ZOSAPI_Initializer.Initialize(pathToInstall)

zemaxDir = ''
if success:
    zemaxDir = ZOSAPI_NetHelper.ZOSAPI_Initializer.GetZemaxDirectory()
    print('Found OpticStudio at:   %s' + zemaxDir)
else:
    raise Exception('Cannot find OpticStudio')

# load the ZOS-API assemblies
clr.AddReference(os.path.join(os.sep, zemaxDir, r'ZOSAPI.dll'))
clr.AddReference(os.path.join(os.sep, zemaxDir, r'ZOSAPI_Interfaces.dll'))
import ZOSAPI

TheConnection = ZOSAPI.ZOSAPI_Connection()
if TheConnection is None:
    raise Exception("Unable to intialize NET connection to ZOSAPI")

TheApplication = TheConnection.ConnectAsExtension(0)
if TheApplication is None:
    raise Exception("Unable to acquire ZOSAPI application")

if TheApplication.IsValidLicenseForAPI is False:
    raise Exception("License is not valid for ZOSAPI use.  " +
                    "Make sure you have enabled " +
                    "'Programming > Interactive Extension' from the " +
                    "OpticStudio GUI.")

TheSystem = TheApplication.PrimarySystem
if TheSystem is None:
    raise Exception("Unable to acquire Primary system")


def transpose(data):
    """Transposes a 2D list (Python3.x or greater).

    Useful for converting mutli-dimensional line series (i.e. FFT PSF)

    Parameters
    ----------
    data      : Python native list (if using System.Data[,] object reshape
                first)

    Returns
    -------
    res       : transposed 2D list
    """
    if type(data) is not list:
        data = list(data)
    return list(map(list, zip(*data)))


def reshape(data, x, y, transpose=False):
    """Converts a System.Double[,] to a 2D list for plotting or post
    processing

    Parameters
    ----------
    data      : System.Double[,] data directly from ZOS-API
    x         : x width of new 2D list [use var.GetLength(0) for dimension]
    y         : y width of new 2D list [use var.GetLength(1) for dimension]
    transpose : transposes data; needed for some multi-dimensional line series
                data

    Returns
    -------
    res       : 2D list; can be directly used with Matplotlib or converted to
                a numpy array using numpy.asarray(res)
    """
    if type(data) is not list:
        data = list(data)
    var_lst = [y] * x
    it = iter(data)
    res = [list(islice(it, i)) for i in var_lst]
    if transpose:
        return transpose(res)
    return res


def image_plane_coordinates_from_single_ray_trace_text_file(filename,
                                                            image_surface):
    """
    Gets the coordinates of the incident ray on the image plane from a text
    file saved from a completed Zemax OpticStudio Single RAy Trace analysis.

    Parameters
    ----------
    filename : string
        The name of the txt file that the data is saved in, include the full
        path.
    image_surface : int
        The surface index ofthe image plane.

    Returns
    -------
    coordinates : 2d numpy.ndarray
        The coordinates at which the traced ray was incident upon the image
        plane.
    """
    real_trace_start = 21  # real ray trace data always starts on line 21
    coordinates = np.empty((2))
    with open(filename, 'r', encoding="utf-16") as file:
        lines = file.readlines()
        for line in lines[real_trace_start:]:
            line.strip()
            surface_number = -1
            table_row = line.split("\t")
            if table_row[0] == "OBJ":
                continue
            surface_number = int(table_row[0])
            if surface_number >= image_surface:
                coordinates[0] = float(table_row[1])
                coordinates[1] = float(table_row[2])
                break
            else:
                continue
    return coordinates


print('Connected to OpticStudio')

# The connection should now be ready to use.  For example:
print('Serial #: ', TheApplication.SerialCode)

# set up variables
min_tilt = -45.0
max_tilt = 45.0
num_tilts = 50
x_tilt_values = \
    np.linspace(max_tilt, min_tilt, num_tilts)  # independent variable
image_coordinates = np.empty((num_tilts, 2))  # dependent variable

# set file to operate on
path = "E:\\eMap\\OpticStudio Model\\"
zmx_filename = "eMap_2_TC13056_plate_sample.zmx"

# set reay merit function operand field and pupil parameters
mfe = TheSystem.MFE
reax = ZOSAPI.Editors.MFE.MeritOperandType.REAX
reay = ZOSAPI.Editors.MFE.MeritOperandType.REAY
surface = 11  # image plane
wave = 2  # mid-spectrum value
Hx = 0.0  # chief ray
Hy = 0.0  # chief ray
Px = 0.0  # chief ray
Py = 0.0  # chief ray

# alter the x tilt on the coordinate break on surface 1
for ii, x_tilt in enumerate(x_tilt_values):
    surface_1 = TheSystem.LDE.GetSurfaceAt(1)  # first coordinate break
    x_tilt_cell_index = ZOSAPI.Editors.LDE.SurfaceColumn.Par3
    x_tilt_cell = surface_1.GetSurfaceCell(x_tilt_cell_index)
    x_tilt_cell.DoubleValue = x_tilt

    # get results after tilt change
    image_coordinates[ii, 0] = mfe.GetOperandValue(reax, surface, wave,
                                                   Hx, Hy, Px, Py, 0.0, 0.0)
    image_coordinates[ii, 1] = mfe.GetOperandValue(reay, surface, wave,
                                                   Hx, Hy, Px, Py, 0.0, 0.0)

# reset tilt
surface_1 = TheSystem.LDE.GetSurfaceAt(1)  # first coordinate break
x_tilt_cell_index = ZOSAPI.Editors.LDE.SurfaceColumn.Par3
x_tilt_cell = surface_1.GetSurfaceCell(x_tilt_cell_index)
x_tilt_cell.DoubleValue = 0.0

# plot graph of y image coordinate vs plate tilt
title = "Y Intercept of Ray H = (0.0, 0.0), P = (0.0, 0.0) on Image " + \
    "Plane as a Function of Plate X Tilt"
plt.figure(title)
plt.clf()
plt.rc("font", size=22)
plt.title(title)
plt.plot(x_tilt_values, image_coordinates[:, 1])
plt.xlabel("Tilt on Flat Plate Sample (deg)")
plt.ylabel("Ray Intercept on Image Plane (mm)")

# estimate prism diopters
surface_0_thickness = TheSystem.LDE.GetSurfaceAt(0).Thickness
totr = ZOSAPI.Editors.MFE.MeritOperandType.TOTR
mm_distance_between_lens_and_screen = \
    mfe.GetOperandValue(totr, 0, 2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) - \
    surface_0_thickness
# THE DENOMENATOR IN PRENTICES RULE IS SUSPECT.  DOES NOT TAKE INTO ACCOUNT
# PASSAGE THROUG LENS
prism_diopters = 100.0*image_coordinates/mm_distance_between_lens_and_screen

# plot graph of prism vs plate tilt
title = "Prism for a Ray H = (0.0, 0.0), P = (0.0, 0.0) " + \
    "as a Function of Plate X Tilt"
plt.figure(title)
plt.clf()
plt.rc("font", size=22)
plt.title(title)
plt.plot(x_tilt_values, prism_diopters[:, 1])
plt.xlabel("Tilt on Flat Plate Sample (deg)")
plt.ylabel("Prsim from Prentice's Rule (prism diopters)")
