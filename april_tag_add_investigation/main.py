# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 13:15:30 2022
A script to extract key images from image directory.
@author: RhysPoolman
"""
import os
import pandas as pd
import shutil as shl
import numpy as np
import numpy.matlib as npm
import matplotlib.pyplot as plt
import re
import collections as clt


def plot_bar_chart_detection_rates_vs_position(dict_of_df, title_prefix):
    """
    Plots a bar chart with the detection rates and a scatter plot with the
    absolute value of the number of April tags in the lens area.

    Parameters
    ----------
    dict_of_df : dict of pandas.Dataframes
        Must have the headings "Position", "Scale", "Detection Rate",
        "Faulty Detections", "Number of April Tags in Lens Region" for each
        dataframe.
    title_prefix : string
        A prefix to the title "Detection Rates Against Position.

    Returns
    -------
    None.
    """
    plt.figure(title_prefix + "Detection Rates Against Position")
    plt.clf()
    num_scales = len(dict_of_df.keys())
    colours = iter(plt.cm.rainbow(np.linspace(0, 1, num_scales)))
    for ii, (key, scale_df) in enumerate(dict_of_df.items()):
        num_positions = len(scale_df)
        width = 1/(num_positions + 2)
        offset_factor = (ii - int(num_scales/2)) if ii < num_scales/2 \
            else (ii - int(num_scales/2))
        offset = offset_factor*width
        x = scale_df.loc[:, "Position"].values
        y1 = scale_df.loc[:, "Detection Rate"].values*100
        ax1 = plt.subplot(111)
        label = "{0:.0f}x Scale Factor".format(float(key))
        colour = next(colours)
        ax1.bar(x + offset, y1, width=width, align="center", label=label,
                color=colour)
    x2 = npm.repmat(x, 5, 1)
    y2 = np.stack([data.loc[:, "Number of April Tags in Lens Region"].values
                   for _, data in dict_of_df.items()])
    ax2 = ax1.twinx()
    colours = iter(plt.cm.rainbow(np.linspace(0, 1, num_scales)))
    for x, y in zip(x2, y2):
        colour = next(colours)
        ax2.semilogy(x, y, linestyle="None", marker="o", color=colour)
    ax1.set_ylim((0, 105))
    ax1.set_xlabel("Requested Position from Monitor(mm)")
    ax1.set_ylabel("Detection Rate (%)")
    ax2.set_ylim((0.725, 450))
    ax2.set_ylabel("Number of April Tags in Lens Region")
    ax1.legend()


# load list of files
path = "C:\\Users\\RhysPoolman\\OneDrive - EYOTO GROUP LTD\\Documents\\" + \
    "Gallifrey\\april_tag_detection_data\\" + \
    "sph_10_cyl_n3_ax_160_add_3_b_pre_focus_pre_travel_3\\"
filename = "analysis_images.xlsx"
csv_data = pd.read_excel(path + filename)

# copy files to new directory
copy_files = False
if copy_files:
    # create new directory
    folder_name = "analysis_images"
    if not os.path.exists(path + folder_name):
        os.makedirs(path + folder_name)
    # copy files
    for image_name in csv_data.loc[:, "Filename"]:
        src = image_name.replace("\\", "\\\\").lstrip()
        dst = "analysis_images\\\\" + image_name.split("\\")[1].lstrip()
        shl.copy(path + src, path + dst)

# get scale and position data from filenames
num_rows_for_one_lens = int(csv_data.shape[0]/2)
headings = ["Position", "Scale", "Detection Rate", "Faulty Detections",
            "Number of April Tags in Lens Region"]
left_data = pd.DataFrame(np.zeros((num_rows_for_one_lens, 5)),
                         columns=headings)
right_data = pd.DataFrame(np.zeros((num_rows_for_one_lens, 5)),
                          columns=headings)
for ii, row in csv_data.iterrows():
    jj = ii - num_rows_for_one_lens if ii >= num_rows_for_one_lens else ii
    label_component = row.loc["Label"].split(".")
    chirality = label_component[0]
    independent_variables = re.split(r"(\d+)", label_component[1])
    if chirality == "left":
        left_data.loc[jj, "Position"] = float(independent_variables[1])
        left_data.loc[jj, "Scale"] = float(independent_variables[3])
        left_data.loc[jj, "Detection Rate"] = row[3]/row[2]
        left_data.loc[jj, "Faulty Detections"] = row[4]
        left_data.loc[jj, "Number of April Tags in Lens Region"] = row[2]
    elif chirality == "right":
        right_data.loc[jj, "Position"] = float(independent_variables[1])
        right_data.loc[jj, "Scale"] = float(independent_variables[3])
        right_data.loc[jj, "Detection Rate"] = row[3]/row[2]
        right_data.loc[jj, "Faulty Detections"] = row[4]
        right_data.loc[jj, "Number of April Tags in Lens Region"] = row[2]
    else:
        raise ValueError("Unrecognised chirality")

# sort data by scale for left data
unique_scales = np.sort(left_data.loc[:, "Scale"].unique())
left_scales = {str(unique_scale):
               left_data.loc[left_data["Scale"] == unique_scale, :]
               for unique_scale in unique_scales}
left_scale = clt.OrderedDict(left_scales)

# for right data
unique_scales = np.sort(right_data.loc[:, "Scale"].unique())
right_scales = {str(unique_scale):
                right_data.loc[right_data["Scale"] == unique_scale, :]
                for unique_scale in unique_scales}
right_scale = clt.OrderedDict(right_scales)

# graph detection rates against positions
plot_bar_chart_detection_rates_vs_position(left_scale, "Left ")
plot_bar_chart_detection_rates_vs_position(right_scale, "Right ")
