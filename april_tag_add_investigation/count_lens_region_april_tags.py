# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 12:40:30 2022
Counts the lens region april tags.
@author: RhysPoolman
"""
import os
import pandas as pd
import shutil as shl
import numpy as np
import numpy.matlib as npm
import matplotlib.pyplot as plt
import re
import collections as clt
import cv2


def test_plot(title, image):
    """
    Creates OpenCV image window that is 1800x1000 pixels and prevents excution
    of the script until the window is closed.

    Parameters
    ----------
    title: string
        The name of the window the image is displayed in.
    image : 2 dimensional numpy.ndarray
        The image to be viewed.

    Returns
    -------
    None.
    """
    plt.figure(title)
    plt.clf()
    plt.imshow(image)


# load list of files
path = "C:\\Users\\RhysPoolman\\OneDrive - EYOTO GROUP LTD\\Documents\\" + \
    "Gallifrey\\april_tag_detection_data\\" + \
    "sph_10_cyl_n3_ax_160_add_3_b_post_focus_post_transport_3\\"
filename = "analysis_images.xlsx"
csv_data = pd.read_excel(path + filename)

# copy files to new directory if needed
copy_files = not os.path.exists(path + "analysis_images\\")
if copy_files:
    # create new directory
    folder_name = "analysis_images"
    if not os.path.exists(path + folder_name):
        os.makedirs(path + folder_name)
    # copy files
    for image_name in csv_data.loc[:, "Filename"]:
        src = image_name.replace("\\", "\\\\").lstrip()
        dst = "analysis_images\\\\" + image_name.split("\\")[1].lstrip()
        shl.copy(path + src, path + dst)

# # set up the SimpleBlobdetector with default parameters.
# params = cv2.SimpleBlobDetector_Params()
# # define thresholds
# params.minThreshold = 50
# params.maxThreshold = 255
# # filter by area
# params.filterByArea = True
# params.minArea = 50
# params.maxArea = 10000
# # filter by color (black=0)
# params.filterByColor = False  # set true for cast_iron to detect black regions
# params.blobColor = 255
# # filter by Circularity
# params.filterByCircularity = False
# params.minCircularity = 0.5
# params.maxCircularity = 1
# # filter by Convexity
# params.filterByConvexity = False
# params.minConvexity = 0.5
# params.maxConvexity = 1
# # filter by InertiaRatio(elongation)
# params.filterByInertia = False
# params.minInertiaRatio = 0
# params.maxInertiaRatio = 1
# # distance between blobs
# params.minDistBetweenBlobs = 0
# # setup the detector with parameters
# detector = cv2.SimpleBlobDetector_create(params)

# # create left and right masks
# mask_image = cv2.imread(path + "images\\" +
#                         "left.lensdetect.framelocate_0.tiff")
# contour_indicies = np.where(mask_image[:, :, 1] > 254)
# contour_image = np.zeros(mask_image.shape, dtype=np.uint8)
# contour_image[contour_indicies[0], contour_indicies[1], 1] = 255
# ret, thresh = cv2.threshold(contour_image[:, :, 1], 127, 255, 0)
# contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,
#                                        cv2.CHAIN_APPROX_SIMPLE)
# contour_lengths = [len(contour) for contour in contours]
# longest_contour_index = np.argmax(contour_lengths)
# mask = np.zeros(mask_image.shape, dtype=np.uint8)
# cv2.drawContours(mask, contours, longest_contour_index, (0, 1, 0), -1)

# # begin counting
# image_folder = path + "analysis_images\\"
# for image_name in os.listdir(image_folder):
#     # get scale and ignore large april tags
#     label_component = image_name.split(".")
#     chirality = label_component[0]
#     independent_variables = re.split(r"(\d+)", label_component[1])
#     scale = int(independent_variables[3])
#     if scale > 2:
#         continue
#     # load image
#     image = cv2.imread(image_folder + image_name)
#     # mask image
#     image *= mask
#     # detect blobs
#     blobs = detector.detect(image)
#     output_image = cv2.drawKeypoints(image, blobs, 0, (255, 0, 0),
#                                      flags=cv2.DRAW_MATCHES_FLAGS_NOT_DRAW_SINGLE_POINTS)
#     test_plot("Check Blobs", output_image)