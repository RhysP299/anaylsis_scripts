"""
Create on Wed Jul 20 15:13:28 2022
A script to calculate prism from Atlas images.
@author RhysPoolman
"""
import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
from matplotlib import cm
import tools.utilities as utl
import tools.prescription as pre
from enum import Enum
import scipy.interpolate as spi


class SearchLineDirection(Enum):
    """
    Enum to flag whether search lines are vertical or horizontal.
    """
    horizontal = 0
    vertical = 1


def test_plot(title, image):
    """
    Creates OpenCV image window that is 1800x1000 pixels and prevents excution
    of the script until the window is closed.

    Parameters
    ----------
    title: string
        The name of the window the image is displayed in.
    image : 2 dimensional numpy.ndarray
        The image to be viewed.

    Returns
    -------
    None.
    """
    plt.figure(title)
    plt.clf()
    plt.imshow(image, cmap="gray")


def test_plot_contours(title, contours):
    """
    Plots contours from the contour dicts.

    Parameters
    ----------
    title: string
        The name of the window the image is displayed in.
    contours : dict
        The output of a call to find_contours.

    Returns
    -------
    None.
    """
    plot_image = np.zeros(np.shape(contours[1]))
    for contour in contours[0]:
        cv2.drawContours(plot_image, contour, -1, (255, 255, 255), 3)
    test_plot(title, plot_image)


def test_plot_contours_with_bb(title, contours, bb):
    """
    Plots contours from the contour dicts with bounding box.

    Parameters
    ----------
    title: string
        The name of the window the image is displayed in.
    contours : dict
        The output of a call to find_contours.
    bb: tuple of four ints
        The upper left and lower right verticies of a rectangle.  Used for
        plotting.

    Returns
    -------
    None.
    """
    plot_image = np.zeros(np.shape(contours[1]))
    for contour in contours[0]:
        cv2.drawContours(plot_image, contour, -1, (255, 255, 255), 3)
    cv2.rectangle(plot_image, bb, (255, 255, 255), 3)
    test_plot(title, plot_image)


def test_plot_contours_with_bb_and_search_lines(title, contours, bb,
                                                search_lines):
    """
    Plots contours from the contour dicts with bounding box.

    Parameters
    ----------
    title : string
        The name of the window the image is displayed in.
    contours : dict
        The output of a call to find_contours.
    bb : tuple of four ints
        The upper left and lower right verticies of a rectangle.  Used for
        plotting.
    search_lines : nx4 numpy.ndarray
        The search lines to be plotted.

    Returns
    -------
    None.
    """
    plot_image = np.zeros(np.shape(contours[1]))
    for contour in contours[0]:
        cv2.drawContours(plot_image, contour, -1, (255, 255, 255), 3)
    cv2.rectangle(plot_image, bb, (255, 255, 255), 3)
    for line in search_lines:
        start = (int(line[0]), int(line[1]))
        end = (int(line[2]), int(line[3]))
        cv2.line(plot_image, start, end, (255, 255, 255), 3)
    test_plot(title, plot_image)


def find_contours(image):
    """
    Finds the contours in an image and creates an array to store them
    and an array to dispaly them in an image.

    Parameters
    ----------
    image : 2 Dimensional numpy.ndarray
        The image in which the contours are to be found.

    Returns
    -------
    contours : list of 3 Dimentional numpy.ndarray
        A list containing the contours that were found by the openCV
        findContours function.
    contour_image : 2 Dimensional numpy.ndarray
        An array that has an image of the contours that can be used in the
        openCV imshow function.

    """
    blurred_image = cv2.GaussianBlur(image, (3, 3), 1.5)
    _, binary_image = cv2.threshold(blurred_image, 0, 255, cv2.THRESH_OTSU)
    contours, _ = \
        cv2.findContours(binary_image, cv2.RETR_EXTERNAL,
                         cv2.CHAIN_APPROX_NONE)
    contours = [contour for contour in contours if len(contour) > 50]
    contour_image = np.zeros(np.shape(binary_image))
    cv2.drawContours(contour_image, contours, -1, (255, 0, 0), 3)
    return contours, contour_image.astype(np.uint8)


def isolate_index_line(image):
    """
    Isolates the grey line from the black ones in the prism images. This is the
    index line

    Parameters
    ----------
    image : 2 dimensional numpy.ndarray
        An image with the horizontal or vertical lines used to calcualte prism.
        One line will be grey, this is the index line that will be used as the
        datum to make sure that lines in the reference and measured images are
        correctly paired.

    Returns
    -------
    index_line_with_mask : 2 dimensional numpy.ndarray
        An image with only the index line in it.
    """
    # blur image
    smoothed = cv2.GaussianBlur(image, (5, 5), 1.5)
    # threshold with otsu
    ostu_value_with_mask, thresholded_image = \
        cv2.threshold(smoothed, 0, 255, cv2.THRESH_OTSU)
    # remove out of intensity range features
    lo_ostu_value_with_mask = ostu_value_with_mask - 50
    hi_ostu_value_with_mask = ostu_value_with_mask + 50
    index_line_with_mask = cv2.inRange(smoothed, (lo_ostu_value_with_mask),
                                       (hi_ostu_value_with_mask))
    # erode smaller feature out of image
    erosion_element = cv2.getStructuringElement(cv2.MORPH_RECT,
                                                (15, 15))
    index_line_with_mask = cv2.erode(index_line_with_mask, erosion_element)
    return index_line_with_mask


def calculate_contour_mean_positions(contour_data):
    """
    Takes contour data nad calcualtes the mean position of each contour to
    generate a line through it

    Parameters
    ----------
    contour_data : nested dict
        Contour data sorted in to a  three layer nested dict. The outer layer
        is indexed with jobids.  The next layer is index with the filenames of
        the images used. The inner layer is a tuple of an image as a 2d
        numpy.ndarray and a list of contours expressed as numpy.ndarrays of 2d
        points.

    Returns
    -------
    means : nested dict
        A nested dict with the same two out layers as the input but the tuple
        replaced with list of floats representing the mean position of each
        contour.
    """
    means = {jobid: {name: [np.mean(contour[:, 0, 1])
                            if "Horizontal" in name else
                            np.mean(contour[:, 0, 0])
                            for contour in image[0]]
                     for name, image in contour_item.items()}
             for jobid, contour_item in contour_data.items()}
    return means


def sort_contours_by_distance_from_index_line(contours, contour_means,
                                              index_line_mean):
    """
    Sorts the contours by distance from their mean value so that they might
    be paired by to a reference value.

    Parameters
    ----------
    contours : tuple of list of 3 Dimentional numpy.ndarray and
               2 Dimensional numpy.ndarray
        The output of the find_contours function that will be sorted by
        distance from of the contors from the index line.
    contour_means : nested dict
        Output of the calculate_contour_mean_positions function operating on
        the non-index lines.
    index_line_mean : nested dict
        Output of the calculate_contour_mean_positions function operating on
        the index lines.

    Returns
    -------
    sorted_contours : tuple of list of a 3 Dimentional numpy.ndarray, a
                      2 Dimensional numpy.ndarray and
                      1 Dimensional numpy.ndarray
        The list of contours sorted by distance from the index contour, the
        mean postion of each contour and the contours themselves
    """
    # calculate mean distance between contours and index line for reference
    distance_from_index_line = contour_means - index_line_mean
    # sort contours in those with a negative or positive distance
    parity_groups = {"+ve": [], "-ve": []}
    for ii, distance in enumerate(distance_from_index_line):
        key = "-ve" if distance < 0.0 else "+ve"
        parity_groups[key].append([distance, contours[0][ii], contours[1][ii]])
    # sort contours so that the closest to the index line come first
    sorted_contours = \
        {"+ve":
         [contour[1] for contour in sorted(parity_groups["+ve"])
          if cv2.contourArea(contour[1]) > 29000],
         "-ve":
         [contour[1] for contour in sorted(parity_groups["-ve"],
                                           reverse=True)
          if cv2.contourArea(contour[1]) > 29000],
         "image": contours[1]}
    return sorted_contours


def pair_measured_and_reference_contours(sorted_contours, parity):
    """
    Pairs the sorted contours.

    Parameters
    ----------
    sorted_contours: nested dict
        The contours from the measured and reference data.
    parity: string
        A string used as key in the sorted_contours dict.
        Should be "+ve" or "-ve".

    Returns
    -------
    paired_contours: nested dict
        A nested dict with tuples of paired contours.  The first
        contour in the tuple is the measured and the second the
        reference.
    """
    paired_contours = {}
    for jobid, image_set in sorted_contours.items():
        paired_contours[jobid] = {}
        for image_name, contours in image_set.items():
            paired_contours[jobid][image_name] = \
                {"Measured Image": contours["Measured"]["image"],
                 "Reference Image": contours["Reference"]["image"],
                 "Contours": []}
            reference_contours = contours["Reference"][parity]
            measured_contours = contours["Measured"][parity]
            # ensures that there are the same nnumber of
            # measured and refernece contours
            number_refrence_contours = len(reference_contours)
            number_measured_contours = len(measured_contours)
            if number_measured_contours > number_refrence_contours:
                measured_contours = \
                    measured_contours[:number_refrence_contours]
            elif number_measured_contours < number_refrence_contours:
                reference_contours = \
                    reference_contours[:number_measured_contours]
            # create a list of pair contours
            for reference_contour, measured_contour in \
                    zip(reference_contours, measured_contours):
                paired_contours[jobid][image_name]["Contours"].append((reference_contour,
                                                                       measured_contour))
    return paired_contours


def manage_sort_contours(contours_images, images_means,
                         images_index_line_means, contours_reference,
                         reference_means, reference_index_line_means):
    """
    Creates a nested dict of paired reference and measured contours.

    Parameters
    ----------
    contours_images : nested dict
        The output of the find_contours function for measured images.
    images_means : nested dict
        The mean postion of the measured contours.
    images_index_line_means : nested dict
        The index line contours for the measured images.
    contours_reference : nested dict
        The output of the find_contours function for reference images.
    reference_means : nested dict
        The mean postion of the reference contours.
    reference_index_line_means : nested dict
        The index line contours for the reference images.

    Returns
    -------
    paired_contours : nested dict
        A nested dictionary of paired reference and measured contours.
    """
    # sort contours by distance from index line
    jobids = list(contours_images.keys())
    sorted_contours = {jobid: {name: {"Reference": None, "Measured": None}
                               for name in contours_images[jobid].keys()}
                       for jobid in jobids}
    for jobid, images in contours_images.items():
        reference_names = list(contours_reference[jobid].keys())
        for name, contours in images.items():
            reference_name = \
                get_correct_reference_from_image_name(name, reference_names)
            # get sorted contours
            sorted_contours[jobid][name]["Reference"] = \
                sort_contours_by_distance_from_index_line(contours_reference[jobid][reference_name],
                                                          reference_means[jobid][reference_name],
                                                          reference_index_line_means[jobid][reference_name][0])
            sorted_contours[jobid][name]["Measured"] = \
                sort_contours_by_distance_from_index_line(contours,
                                                          images_means[jobid][name],
                                                          images_index_line_means[jobid][name][0])
    # pair measured and reference contours
    paired_contours = \
        {"+ve": pair_measured_and_reference_contours(sorted_contours, "+ve"),
         "-ve": pair_measured_and_reference_contours(sorted_contours, "-ve")}
    # sort into continuous list of pairs
    output_contours = {}
    for jobid, images in paired_contours["+ve"].items():
        output_contours[jobid] = {}
        for name, contours in images.items():
            output_contours[jobid][name] = {}
            output_contours[jobid][name]["Contours"] = \
                [contour_pair for contour_pair in contours["Contours"]]
            output_contours[jobid][name]["Contours"] += \
                [contour_pair for contour_pair in
                 paired_contours["-ve"][jobid][name]["Contours"]]
            output_contours[jobid][name]["Measured Image"] = \
                contours["Measured Image"]
            output_contours[jobid][name]["Reference Image"] = \
                contours["Reference Image"]
    return output_contours


def get_correct_reference_from_image_name(name, reference_names):
    """
    Using the name of the png files for the measured images the correct
    reference image is selected.  All file extensitons houdl be stripped or
    this function will not work correctly.

    Parameters
    ----------
    name : string
        The measured filename.
    reference_names : list of strings
        The reference filename.

    Returns
    -------
    reference_name: string
        The name of the reference to use.
    """
    cam_name = name.split("_")[-1][4:]
    orientation = "Horizontal" if "Horizontal" in name else "Vertical"
    reference_name = [reference_name for reference_name in reference_names
                      if (cam_name in reference_name and
                          orientation in reference_name)][0]
    return reference_name


def shift_by_delta_x(left_image, right_image,
                     left_image_delta_x, right_image_delta_x):
    """
    PArt of the rotate and stitch cal.  Moves images across columns.

    Parameters
    ----------
    left_image : 2d numpy.ndarray of ints
        The left image to be translated.
    right_image :  2d numpy.ndarray of ints
        The right image to be translated.
    left_image_delta_x : int
        The number of pixels to translate the left image.
    right_image_delta_x :  int
        The number of pixels to translate the right image.

    Returns
    -------
    left_image : 2d numpy.ndarray of ints
        The translated left iamge.
    right_image : 2d numpy.ndarray of ints
        The translated right iamge.

    """
    abs_left_image_delta_x = np.abs(left_image_delta_x)
    if left_image_delta_x < 0:
        height, width = np.shape(left_image)
        left_image = left_image[:, :width - left_image_delta_x]
    elif left_image_delta_x > 0:
        left_image = cv2.copyMakeBorder(left_image, 0, 0, 0,
                                        abs_left_image_delta_x,
                                        cv2.BORDER_CONSTANT, 0)
    abs_right_image_delta_x = np.abs(right_image_delta_x)
    if right_image_delta_x < 0:
        height, width = np.shape(right_image)
        right_image = right_image[:, :width - right_image_delta_x]
    elif left_image_delta_x > 0:
        right_image = cv2.copyMakeBorder(right_image, 0, 0, 0,
                                         abs_right_image_delta_x,
                                         cv2.BORDER_CONSTANT, 0)
    return left_image, right_image


def shift_by_delta_y(right_image, delta_y):
    """
    Move the image along rows by an ammount set from the rotate and
    stitch calibration.

    Parameters
    ----------
    right_image: numpy.ndarray
        The right image from an Atlas Duo to be shifted to matach the left
        according the vertical offset calcualted during rotate and stitch
        calibration.
    delta_y: int
        The offset to be applied.

    Returns
    -------
    right_image: numpy.ndarray
        The vertically shifted input image.
    """
    abs_delta_y = np.abs(delta_y)
    if delta_y < 0:
        # a negative delta_y means that the dots in the left image were lower
        # than the dots in the right image which needs to be moved down to
        # match the left image
        right_image = cv2.copyMakeBorder(right_image, abs_delta_y, 0, 0, 0,
                                         cv2.BORDER_CONSTANT, (0))
    elif delta_y > 0:
        # a positive delta Y means that the dots of the left image were higher
        # than the dots of the right image which needs to be moved up to match
        # the left image by cutting pixels off the top of the right image
        right_image_height = np.shape(right_image)[0]
        right_image = \
            right_image[abs_delta_y:right_image_height-abs_delta_y, :]
    return right_image


def combine_images(left_image, right_image):
    """
    Takes the left and right images and combine thems into one long image by
    padding the samllest images so that both are the same height and then
    concatentating horizontally.

    Parameters
    ----------
    left_image : 2d numpy.ndarray
        The image from the left camera.
    right_image : 2d numpy.ndarray
        The image from the right camera.

    Returns
    -------
    combined_image : 2d numpy.ndarray
        The combined image.
    left_image_roi : tuple of 4 ints
        A rectangle that shows the periphery of the left image.
    right_image_roi : tuple of 4 ints
        A rectangle that shows the periphery of the left image.
    """
    # get image shapes
    left_image_height = left_image.shape[0]
    left_image_width = left_image.shape[1]
    right_image_height = right_image.shape[0]
    right_image_width = right_image.shape[1]
    # set combined image size
    combined_image_height = np.max((left_image_height, right_image_height))
    combined_image_size = (combined_image_height,
                           left_image_width + right_image_width)
    # create combined image
    combined_image = np.zeros(combined_image_size, left_image.dtype)
    # get left image with new ROI
    combined_image[:left_image_height, :left_image_width] = left_image
    left_image_roi = (0, 0, left_image_width, combined_image_height)
    # get right image with new ROI
    combined_image[:right_image_height,
                   left_image_width:left_image_width+right_image_width] = \
        right_image
    right_image_roi = (left_image_width, 0,
                       right_image_width, combined_image_height)
    return combined_image, left_image_roi, right_image_roi


def create_contour_images(jobids, input_contours_and_images):
    """
    Creates images of the contours for ease of analysis later.

    Parameters
    ----------
    jobids : string
        The jobid we are doing.
    input_contours_and_images : nested_dict
        The data used to allocate the array for the image and populate it with
        contours.

    Returns
    -------
    contour_images : nested dict
        The images of the contours.
    """
    contour_images = {}
    for jobid in jobids:
        contour_images[jobid] = {}
        for name, contours in input_contours_and_images[jobid].items():
            # create inner dict
            contour_images[jobid][name] = {}
            # add measured contour image
            measured_image = np.zeros(contours["Measured Image"].shape)
            cv2.drawContours(measured_image,
                             [contour[1] for contour in contours["Contours"]],
                             -1, (255, 255, 255), 3)
            contour_images[jobid][name]["Measured"] = measured_image
            contour_images[jobid][name]["Measured Individual Contours"] = []
            for contour in contours["Contours"]:
                contour_image = np.zeros(contours["Measured Image"].shape)
                cv2.drawContours(contour_image, contour[1], -1,
                                 (255, 255, 255), 3)
                contour_images[jobid][name]["Measured Individual Contours"].append(contour_image)
            # add reference contour image
            reference_image = np.zeros(contours["Reference Image"].shape)
            cv2.drawContours(reference_image,
                             [contour[0] for contour in contours["Contours"]],
                             -1, (255, 255, 255), 3)
            contour_images[jobid][name]["Reference"] = reference_image
            contour_images[jobid][name]["Reference Individual Contours"] = []
            for contour in contours["Contours"]:
                contour_image = np.zeros(contours["Reference Image"].shape)
                cv2.drawContours(contour_image, contour[0], -1,
                                 (255, 255, 255), 3)
                contour_images[jobid][name]["Reference Individual Contours"].append(contour_image)
    return contour_images


def generate_search_lines(bounding_box, mask):
    """
    Generates a set of horizontal and vertical lines that will be used to find
    the exact contour locations for comparison

    Parameters
    ----------
    bounding_box : tuple of four ints
        The bounding box of the masked region.
    mask : 2d numpy.ndarray
        The mask used to indentify the lens region.

    Returns
    -------
    vertical_search_lines : nx4 numpy.ndarray
        The vertical search lines.
    horizontal_search_lines : nx4 numpy.ndarray
        The horizontal search lines..

    """
    # default values from LensPrismMeasurmentArgs in code
    pixels_between_search_lines = 10
    bounding_box_margin_percent = 15
    lens_region_height, lens_region_width = mask.shape
    # reduce bounding box size
    percentage_of_bb_to_use = 1 - 0.02*bounding_box_margin_percent  # 70% of bb
    bounding_box_width = bounding_box[2]
    bounding_box_height = bounding_box[3]
    bounding_box_center = (bounding_box[0] + bounding_box_width/2,
                           bounding_box[1] + bounding_box_height/2)
    region_of_interest_width = \
        int(percentage_of_bb_to_use*bounding_box_width)
    region_of_interest_height = \
        int(percentage_of_bb_to_use*bounding_box_height)
    # termiantors of search lines
    search_line_start = (bounding_box_center[0] - region_of_interest_width/2,
                         bounding_box_center[1] - region_of_interest_height/2)
    search_line_end = (bounding_box_center[0] + region_of_interest_width/2,
                       bounding_box_center[1] + region_of_interest_height/2)
    # generate search lines 10 px apart
    x = np.arange(search_line_start[0], search_line_end[0],
                  pixels_between_search_lines, dtype=int)
    y = np.arange(search_line_start[1], search_line_end[1],
                  pixels_between_search_lines, dtype=int)
    vertical_search_lines = np.zeros((len(x), 4), dtype=int)
    vertical_search_lines[:, 0] = x
    vertical_search_lines[:, 1] = np.ones(len(x))*bounding_box[1]
    vertical_search_lines[:, 2] = x
    vertical_search_lines[:, 3] = np.ones(len(x))*lens_region_height
    horizontal_search_lines = np.zeros((len(y), 4), dtype=int)
    horizontal_search_lines[:, 0] = np.ones(len(y))*bounding_box[0]
    horizontal_search_lines[:, 1] = y
    horizontal_search_lines[:, 2] = np.ones(len(y))*lens_region_width
    horizontal_search_lines[:, 3] = y
    return vertical_search_lines, horizontal_search_lines


def find_contour_edges(contours, search_lines, search_line_direction):
    """
    Creates a collection of (x,y) points that taken from the edges of the image
    contours.

    Parameters
    ----------
    contours : nested dict
        Images of the contours whose edges we need to find.
    search_lines : nested dict
        A collection of vertical or horizontal lines along which scans of the
        contour image are taken to find the x, y locations.
    search_line_direction : SearchLineDirection
        A flag indicting which way the search lines are oriented.

    Returns
    -------
    edge_points : list of tuples
        A list of points represent as tuples of 2 ints.

    """
    edge_points = []
    # set parameters for edge search
    current_pixel_intensity = 0
    pervious_pixel_intensity = 0
    # perform seach
    if search_line_direction == SearchLineDirection.horizontal:
        for search_line in search_lines:
            row = search_line[1]
            col_start = search_line[0]
            col_end = search_line[2] \
                if search_line[2] <= contours.shape[1] else \
                contours.shape[1]
            for col in range(col_start, col_end):
                current_pixel_intensity = contours[row, col]
                if current_pixel_intensity != pervious_pixel_intensity:
                    x = row
                    y = col
                    edge_points.append((x, y))
                pervious_pixel_intensity = current_pixel_intensity
    else:
        for search_line in search_lines:
            col = search_line[0]
            row_start = search_line[1]
            row_end = search_line[3] \
                if search_line[3] <= contours.shape[1] else \
                contours.shape[1]
            for row in range(row_start, row_end):
                current_pixel_intensity = contours[row, col]
                if current_pixel_intensity != pervious_pixel_intensity:
                    x = row
                    y = col
                    edge_points.append((x, y))
                pervious_pixel_intensity = current_pixel_intensity
    return edge_points


def manage_find_contour_edges(contour_images, vertical_search_lines,
                              horizontal_search_lines):
    """
    Organises the images passed to find_contour_edges so that the both measured
    and reference contours are paired.

    Parameters
    ----------
    contour_images : nested dict
        Contains the images made of opencv contours.
    vertical_search_lines : nest dict
        A set of vertical search lines that iterated over and across to find
        the edge points.
    horizontal_search_lines : nest dict
        A set of horizontal search lines that iterated over and across to find
        the edge points.

    Returns
    -------
    edge_points : nested dict
        The edges points for each of the images in contour_images.
    """
    edge_points = {}
    for jobid, images in contour_images.items():
        edge_points[jobid] = {}
        for name, image_pairs in images.items():
            # get correct search lines
            search_lines = vertical_search_lines[jobid] if \
                "Horizontal" in name else horizontal_search_lines[jobid]
            cam_name = "Cam1Cam1" if "Cam1Cam1" in name else "Cam2Cam2"
            search_lines_key = \
                "SecondPos_DetectedTidyLensMask{0:s}".format(cam_name)
            search_lines = search_lines[search_lines_key]
            # set search line direction
            search_line_direction = SearchLineDirection.vertical if \
                "Horizontal" in name else SearchLineDirection.horizontal
            edge_points[jobid][name] = {}
            # get edge points for each seperate contour
            edge_points[jobid][name]["Measured Individual Contours"] = []
            for contour in image_pairs["Measured Individual Contours"]:
                points = find_contour_edges(contour, search_lines,
                                            search_line_direction)
                if len(points) > 0:
                    edge_points[jobid][name]["Measured Individual Contours"].append(points)
            edge_points[jobid][name]["Reference Individual Contours"] = []
            for contour in image_pairs["Reference Individual Contours"]:
                points = find_contour_edges(contour, search_lines,
                                            search_line_direction)
                if len(points) > 0:
                    edge_points[jobid][name]["Reference Individual Contours"].append(points)
            # get edge points for all contours
            edge_points[jobid][name]["Measured"] = \
                find_contour_edges(image_pairs["Measured"], search_lines,
                                   search_line_direction)
            edge_points[jobid][name]["Reference"] = \
                find_contour_edges(image_pairs["Reference"], search_lines,
                                   search_line_direction)
    return edge_points


def calculate_centre_line(name, contour):
    """
    Calculates the centre line for each image by taking the mean position of
    point in a contour on a given search line.

    Parameters
    ----------
    name : string
        The image name, used to indetify whether the search lines orientation.
    contour : nested dict
        The conouts of the lines in the image.

    Returns
    -------
    centre_line : nested dict
        A dictionary of centre lines.
    """
    contour_array = np.array(contour)
    search_line_direction = \
        SearchLineDirection.horizontal if "Vertical" in name\
        else SearchLineDirection.vertical
    unique_coordinates = np.unique(contour_array[:, 1]) \
        if search_line_direction == SearchLineDirection.vertical \
        else np.unique(contour_array[:, 0])
    centre_line = np.zeros((len(unique_coordinates), 2))
    if search_line_direction == SearchLineDirection.vertical:
        centre_line[:, 0] = \
            np.array([np.mean(contour_array[contour_array[:, 1] ==
                                            unique_coordinate][:, 0])
                      for unique_coordinate in unique_coordinates])
        centre_line[:, 1] = unique_coordinates
    else:
        centre_line[:, 0] = unique_coordinates
        centre_line[:, 1] = \
            np.array([np.mean(contour_array[contour_array[:, 0] ==
                                            unique_coordinate][:, 1])
                      for unique_coordinate in unique_coordinates])
    return centre_line


def manage_contour_centre_line_calculation(edge_points,
                                           horizontal_search_lines,
                                           vertical_search_lines):
    """
    A function to manage the calculate of the centre lines over all requried
    images.

    Parameters
    ----------
    edge_points : nested dict
        The edge points of each contour as an image.
    horizontal_search_lines : nest dict
        The horizontal search lines used to identify the position at which the
        centre line will be calculated.  These are used on images with vertical
        lines.
    vertical_search_lines : nest dict
        The vertical search lines used to identify the position at which the
        centre line will be calculated.  These are used on images with
        horizontal lines.

    Returns
    -------
    centre_points : nest_dict
        A dictionary of centre lines  for each image.
    """
    centre_points = {}
    for jobid, data in edge_points.items():
        centre_points[jobid] = {}
        for name, contours in data.items():
            search_lines = \
                horizontal_search_lines if "Vertical" in name \
                else vertical_search_lines
            cam_name = name[-8:]
            searchlines_name = \
                "SecondPos_DetectedTidyLensMask{0:s}".format(cam_name)
            number_of_search_lines = len(search_lines[jobid][searchlines_name])
            centre_points[jobid][name] = {"Measured Individual Contours": [],
                                          "Reference Individual Contours": []}
            for contour in contours["Measured Individual Contours"]:
                centre_line = calculate_centre_line(name, contour)
                if len(centre_line) == number_of_search_lines:
                    centre_points[jobid][name]["Measured Individual Contours"].append(centre_line)
            for contour in contours["Reference Individual Contours"]:
                centre_line = calculate_centre_line(name, contour)
                if len(centre_line) == number_of_search_lines:
                    centre_points[jobid][name]["Reference Individual Contours"].append(centre_line)
    return centre_points


def calculate_pixel_shift(name, contours):
    """
    Calculates the distance the pixels have shifted between reference and
    measured images.

    Parameters
    ----------
    name : string
        The name of the images used to calcualted the pixel shift.  This will
        indicate whether the horizontal or vertical component should be
        calculated.
    contours : dict
        The contours attached to the Measured Individual Contours are from
        images recored with the sample present.  The Reference Individual
        Contours are taken from images in which no sample was presnet and these
        images are taken as part of start-up.

    Returns
    -------
    pixel_shift_name : string
        A key for the pixel shift values.
    pixel_shift : nx3 numpy.ndarray of ints
        An array with x, y, pixel shift rows, where the x and y is the
        reference image coordinates.
    """
    pixel_shift = {}
    pixel_shift_name = "Horizontal" if "Vertical" in name else "Vertical"
    pixel_shift_index = 1 if "Vertical" in name else 0
    measured_centre_lines = contours["Measured Individual Contours"]
    reference_centre_lines = contours["Reference Individual Contours"]
    # iterate over the smaller array so unpaired centre lines are ignored
    if len(measured_centre_lines) <= len(reference_centre_lines):
        # allocate array for number of points
        number_of_points = \
            np.sum([len(centre_line)
                    for centre_line in measured_centre_lines])
        pixel_shift = np.zeros((number_of_points, 3))
        # calculate pixel shift
        start = 0
        for ii, meas_centre_line in enumerate(measured_centre_lines):
            # get variables and calculate pixel shift
            end = start + len(meas_centre_line)
            pixel_shift[start:end, 0] = \
                measured_centre_lines[ii][:, 1]
            pixel_shift[start:end, 1] = \
                measured_centre_lines[ii][:, 0]
            pixel_shift[start:end, 2] = \
                meas_centre_line[:, pixel_shift_index] - \
                reference_centre_lines[ii][:, pixel_shift_index]
            start = end
    else:
        number_of_points = \
            np.sum([len(centre_line)
                    for centre_line in reference_centre_lines])
        pixel_shift = np.zeros((number_of_points, 3))
        # calculate pixel shift
        start = 0
        for ii, ref_centre_line in enumerate(reference_centre_lines):
            # get variables and calculate pixel shift
            end = start + len(ref_centre_line)
            pixel_shift[start:end, 0] = \
                reference_centre_lines[ii][:, 1]
            pixel_shift[start:end, 1] = \
                reference_centre_lines[ii][:, 0]
            pixel_shift[start:end, 2] = \
                measured_centre_lines[ii][:, pixel_shift_index] - \
                ref_centre_line[:, pixel_shift_index]
            start = end
    return pixel_shift_name, pixel_shift


def convert_mm_to_camera_image_pixels(mm_value, camera_sensor_mm_to_pixels,
                                      lens_magnification):
    """
    Converts a distance in mm to pixels.

    Parameters
    ----------
    mm_value: float
        A distance in mm.
    camera_sensor_mm_to_pixels: float
        The reciprocal of the size of a pixel side in mm.  This assumes that
        the pixels are square.
    lens_magnification: float
        The magnification of the telecentric and NOT the sample.

    Return
    ------
    pixel_value: float
        The value of the distance converted to number of pixels.
    """
    return mm_value * lens_magnification/camera_sensor_mm_to_pixels


def appoximate_bounding_rects_from_contours(contours, epsilon):
    """
    A function to put bounding boxes around all the contours in a list of
    contours.  Based on
    EyeTech.OpenCv.ImageProcessing.Filtering.ParticleAnalysis.
    ApproximateBoundingRectsFromContours

    Parameters
    ----------
    contours : 3d numpy.ndarray
        The conoturs from cv2.GetContours.
    epsilon : float
        This function creates a finer approximation of the contours and this is
        the accuracy to which it should do so.

    Returns
    -------
    box_list : list of tuples
        A list of bounding boxes and contours.

    """
    box_list = []
    for contour in contours:
        finer_contour = cv2.approxPolyDP(contour, epsilon, True)
        bounding_rect = cv2.boundingRect(finer_contour)
        box_list.append((bounding_rect, contour))
    return box_list


def prentices_rule(pixel_shift, camera_magnification,
                   distance_between_sample_and_camera):
    """
    Prentices rule as defined in eMap Solution Guidebook -> Power Calculation
    Fixes -> Prism Problem Solution.

    Parameters
    ----------
    pixel_shift : int or array of ints
        The number of pixel that the reference to measured image has been
        translated by measured in mm.
    camera_magnification : float
        The magnification of the camera.
    distance_between_sample_and_camera : flaot
        The distance between the sameple and camera.

    Returns
    -------
    float
        The prism diopter equivalent of the pixel shift.
    """
    return 100*(pixel_shift /
                (camera_magnification *
                 distance_between_sample_and_camera))


# load images and masks
# path = "E:\\anaylsis_scripts\\prism_calculator\\data\\"
path = "C:\\Users\\RhysPoolman\\analysis_scripts\\prism_calculator\\data\\"
jobids = [jobid for jobid in os.listdir(path) if "old" not in jobid]
data = {jobid: {name: cv2.imread(path + jobid + "\\" + name)
                for name in os.listdir(path + jobid + "\\")
                if name.endswith(".png") and "prism" in name.lower()}
        for jobid in jobids}
mask_data = {jobid: {name: cv2.imread(path + jobid + "\\" + name)
                     for name in os.listdir(path + jobid + "\\")
                     if name.endswith(".png") and "mask" in name.lower()
                     and "secondpos" in name.lower()}
             for jobid in jobids}


# get prescription
jobids.append("13285446")
lms_path = path + "\\..\\lms\\"
prescriptions = {}
for jobid in jobids:
    oma_filename = jobid + ".oma"
    try:
        prescriptions[jobid] = pre.OmaPrescription(lms_path + oma_filename)
    except FileNotFoundError as error:
        print(str(error))
        print("No prescription for {0:s}".format(jobid))

# get rotate and stitch calibration data
cal_filename = "DualCameraAlignmentCalibration.dat"
serial_number = "test"
r_and_s = {jobid:
           utl.RotateStitchCalibration.from_file(path + jobid + "\\" +
                                                 cal_filename,
                                                 serial_number)
           for jobid in jobids}

# get distance between lens and screen
filename = "distanceBetweenSampleAndCamera.txt"
distance_between_sample_and_camera = {}
for jobid in jobids:
    full_filename = path + "\\" + jobid + "\\" + filename
    if os.path.isfile(full_filename):
        with open(full_filename) as file:
            lines = file.readlines()
            distance_between_sample_and_camera[jobid] = \
                {"Cam1": float(lines[0].split(" = ")[1]),
                 "Cam2": float(lines[1].split(" = ")[1])}

# get references
masked_references = \
    {jobid: {name.split(".")[0]: cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
             for name, image in images.items()
             if "CapturedPrismReferenceImage" in name}
     for jobid, images in data.items()}

# get measured images
masked_images_cam1 = \
    {jobid: {name.split(".")[0]:  cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
             for name, image in images.items()
             if name.startswith("SecondPos_Prism") and
             name.endswith("Cam1.png")}
     for jobid, images in data.items()}

# get mask
input_masks = \
    {jobid: {name.split(".")[0]:  cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
             for name, image in images.items()
             if name.startswith("SecondPos_DetectedTidyLensMask") and
             name.endswith("Cam1.png")}
     for jobid, images in mask_data.items()}

# plot masks
for jobid, image in input_masks.items():
    test_plot("Mask {0:s}".format(jobid),
              image["SecondPos_DetectedTidyLensMaskCam1Cam1"])

# get mask bounding box
input_mask_bb = \
    {jobid: {name.split(".")[0]: cv2.boundingRect(image)
             for name, image in masks.items()}
     for jobid, masks in input_masks.items()}

# find index line
masked_index_line_images_reference = \
    {jobid: {name: isolate_index_line(image) for name, image in images.items()}
     for jobid, images in masked_references.items()}
masked_index_line_images_cam1 = \
    {jobid: {name: isolate_index_line(image) for name, image in images.items()}
     for jobid, images in masked_images_cam1.items()}

# find lines on masked images
contours_reference = \
    {jobid: {ref_name: find_contours(reference)
             for ref_name, reference in references.items()}
     for jobid, references in masked_references.items()}
contours_images_cam1 = \
    {jobid: {image_name: find_contours(image)
             for image_name, image in images.items()}
     for jobid, images in masked_images_cam1.items()}

# find index lines on masked images
reference_index_line_contours = \
    {jobid: {ref_name: find_contours(reference)
             for ref_name, reference in references.items()}
     for jobid, references in masked_index_line_images_reference.items()}
images_index_line_cam1_contours = \
    {jobid: {image_name: find_contours(image)
             for image_name, image in images.items()}
     for jobid, images in masked_index_line_images_cam1.items()}

# calculate mean line positions
reference_means = calculate_contour_mean_positions(contours_reference)
images_cam1_means = calculate_contour_mean_positions(contours_images_cam1)
reference_index_line_means = \
    calculate_contour_mean_positions(reference_index_line_contours)
images_index_line_cam1_means = \
    calculate_contour_mean_positions(images_index_line_cam1_contours)

# get contours and bounding box for testing
cam1_contours = \
    manage_sort_contours(contours_images_cam1, images_cam1_means,
                         images_index_line_cam1_means, contours_reference,
                         reference_means, reference_index_line_means)

# compare image to index line contour
jobid = jobids[0]
name = "SecondPos_PrismHorizontalLinesImage_Cam1Cam1"
thickness = 3
# test plot with the contours
for jobid, index_line_contour in images_index_line_cam1_contours.items():
    if jobid != "PRISM1" and jobid != "PRISM4":
        continue
    # create blank image of the correct size
    measured_contour_set = \
        [contour[1] for contour in cam1_contours[jobid][name]["Contours"]]
    shape = cam1_contours[jobid][name]["Measured Image"].shape
    measured_plot_image = np.zeros((shape[0], shape[1], 3))
    reference_contour_set = \
        [contour[0] for contour in cam1_contours[jobid][name]["Contours"]]
    shape = cam1_contours[jobid][name]["Reference Image"].shape
    reference_plot_image = np.zeros((shape[0], shape[1], 3))
    # add contours to the image
    colour = (255, 255, 255)
    for contour in measured_contour_set:
        cv2.drawContours(measured_plot_image, contour, -1, colour, thickness)
    for contour in reference_contour_set:
        cv2.drawContours(reference_plot_image, contour, -1, colour, thickness)
    # get index line contours
    measured_index_contour = index_line_contour[name]
    orientation = "Horizontal" if "Horizontal" in name else "Vertical"
    cam_label = "Cam1" if "Cam1Cam1" in name else "Cam2"
    ref_index_name = \
        "CapturedPrismReferenceImage{0:s}Lines{1:s}".format(orientation,
                                                            cam_label)
    reference_index_contour_set = \
        reference_index_line_contours[jobid][ref_index_name]
    # create blank image of the correct size
    shape = np.shape(measured_index_contour[1])
    measured_index_contour_plot_image = np.zeros((shape[0], shape[1],
                                                  thickness))
    shape = np.shape(reference_index_contour_set[1])
    reference_index_contour_plot_image = np.zeros((shape[0], shape[1],
                                                   thickness))
    # add bb to image
    colour = (255, 0, 0)
    bb_name = "SecondPos_DetectedTidyLensMask" + name[-8:]
    bb = input_mask_bb[jobid][bb_name]
    cv2.rectangle(measured_plot_image, bb, colour, 3)
    cv2.rectangle(reference_plot_image, bb, colour, 3)
    # add contours to the image
    for contours in measured_index_contour[0]:
        cv2.drawContours(measured_plot_image, contours, -1, colour, thickness)
    for contours in reference_index_contour_set[0]:
        cv2.drawContours(reference_plot_image, contours, -1, colour, thickness)
    # plot index labels for measured contours
    mid_point = 1250 if orientation == "Horizontal" else 1000
    colour = (255, 255, 255)
    for ii, contour_pair in enumerate(cam1_contours[jobid][name]["Contours"]):
        # label for reference contours
        label_position = (mid_point, int(np.mean(contour_pair[0][:, 0, 1]))) \
            if orientation == "Horizontal" else \
            (int(np.mean(contour_pair[0][:, 0, 0])), mid_point)
        cv2.putText(reference_plot_image, str(ii),
                    label_position, cv2.FONT_HERSHEY_SIMPLEX, 1, colour, 1, 2)
        # label for measured contours
        label_position = (mid_point, int(np.mean(contour_pair[1][:, 0, 1]))) \
            if orientation == "Horizontal" else \
            (int(np.mean(contour_pair[1][:, 0, 0])), mid_point)
        cv2.putText(measured_plot_image, str(ii),
                    label_position, cv2.FONT_HERSHEY_SIMPLEX, 1, colour, 1, 2)
    # plot contours
    plt.figure(jobid)
    plt.clf()
    plt.subplot(121)
    plt.imshow(measured_plot_image.astype('uint8'))
    plt.subplot(122)
    plt.imshow(reference_plot_image.astype('uint8'))

# generate search lines
vertical_search_lines = {}
horizontal_search_lines = {}
for jobid, masks in input_masks.items():
    vertical_search_lines[jobid] = {}
    horizontal_search_lines[jobid] = {}
    for name, mask in masks.items():
        vertical_search_lines[jobid][name], \
            horizontal_search_lines[jobid][name] = \
            generate_search_lines(input_mask_bb[jobid][name], mask)

# create images of the contours
cam1_contour_images = create_contour_images(jobids, cam1_contours)

# find edge points
cam1_edge_points = manage_find_contour_edges(cam1_contour_images,
                                             vertical_search_lines,
                                             horizontal_search_lines)

# # plot contour edge lines
# jobid = jobids[0]
# name = "SecondPos_PrismVerticalLinesImage_Cam1Cam1"
# for ii, (measured_contour, reference_contour) in enumerate(zip(cam1_edge_points[jobid][name]["Measured Individual Contours"],
#                                                                 cam1_edge_points[jobid][name]["Reference Individual Contours"])):
#     plt.figure("Contour {0:d} Edges for {1:s} {2:s}".format(ii + 1, jobid,
#                                                             name))
#     plt.clf()
#     x = np.array([coordinates[0] for coordinates in measured_contour])
#     y = np.array([coordinates[1] for coordinates in measured_contour])
#     plt.scatter(x, y, label="Measured")
#     x = np.array([coordinates[0] for coordinates in reference_contour])
#     y = np.array([coordinates[1] for coordinates in reference_contour])
#     plt.scatter(x, y, label="Reference")
#     plt.legend()
#     plt.xlabel("X (pixels)")
#     plt.ylabel("Y (pixels)")

# calculate the mean of each set of points found by one search line
cam1_centre_points = \
    manage_contour_centre_line_calculation(cam1_edge_points,
                                           horizontal_search_lines,
                                           vertical_search_lines)

# plot vertical contour centre lines
jobid = jobids[0]
name = "SecondPos_PrismHorizontalLinesImage_Cam1Cam1"
for ii, (measured_contour, reference_contour) in \
        enumerate(zip(cam1_centre_points[jobid][name]["Measured Individual Contours"],
                      cam1_centre_points[jobid][name]["Reference Individual Contours"])):
    plt.figure("Contour {0:d} Center for {1:s} {2:s}".format(ii, jobid, name))
    plt.clf()
    plt.scatter(measured_contour[:, 0], measured_contour[:, 1],
                label="Measured")
    plt.scatter(reference_contour[:, 0], reference_contour[:, 1],
                label="Reference")
    plt.legend()
    plt.xlabel("X (pixels)")
    plt.ylabel("Y (pixels)")
    plt.xlim((600, 1700))
jobid = jobids[0]
name = "SecondPos_PrismHorizontalLinesImage_Cam1Cam1"
for ii, (measured_contour, reference_contour) in \
        enumerate(zip(cam1_centre_points[jobid][name]["Measured Individual Contours"],
                      cam1_centre_points[jobid][name]["Reference Individual Contours"])):
    plt.figure("Contour {0:d} Center for {1:s} {2:s}".format(ii, jobid, name))
    plt.clf()
    plt.scatter(measured_contour[:, 0], measured_contour[:, 1],
                label="Measured")
    plt.scatter(reference_contour[:, 0], reference_contour[:, 1],
                label="Reference")
    plt.legend()
    plt.xlabel("X (pixels)")
    plt.ylabel("Y (pixels)")
    plt.xlim((600, 1700))

# plot horizontal contour centre lines
jobid = jobids[0]
name = "SecondPos_PrismHorizontalLinesImage_Cam1Cam1"
for ii, (measured_contour, reference_contour) in \
        enumerate(zip(cam1_centre_points[jobid][name]["Measured Individual Contours"],
                      cam1_centre_points[jobid][name]["Reference Individual Contours"])):
    plt.figure("Contour {0:d} Center for {1:s} {2:s}".format(ii, jobid, name))
    plt.clf()
    plt.scatter(measured_contour[:, 0], measured_contour[:, 1],
                label="Measured")
    plt.scatter(reference_contour[:, 0], reference_contour[:, 1],
                label="Reference")
    plt.legend()
    plt.xlabel("X (pixels)")
    plt.ylabel("Y (pixels)")
    plt.xlim((600, 1700))
jobid = jobids[0]
name = "SecondPos_PrismHorizontalLinesImage_Cam1Cam1"
for ii, (measured_contour, reference_contour) in \
        enumerate(zip(cam1_centre_points[jobid][name]["Measured Individual Contours"],
                      cam1_centre_points[jobid][name]["Reference Individual Contours"])):
    plt.figure("Contour {0:d} Center for {1:s} {2:s}".format(ii, jobid, name))
    plt.clf()
    plt.scatter(measured_contour[:, 0], measured_contour[:, 1],
                label="Measured")
    plt.scatter(reference_contour[:, 0], reference_contour[:, 1],
                label="Reference")
    plt.legend()
    plt.xlabel("X (pixels)")
    plt.ylabel("Y (pixels)")
    plt.xlim((600, 1700))

# calculate the pixel shift of the measured data from the reference
cam1_pixel_shift = {}
for jobid, data in cam1_centre_points.items():
    cam1_pixel_shift[jobid] = {}
    for name, contours in data.items():
        direction, pixel_shift = calculate_pixel_shift(name, contours)
        cam1_pixel_shift[jobid][direction] = pixel_shift

# plot pixel shift histograms
for jobid, pixel_shifts in cam1_pixel_shift.items():
    if jobid != "PRISM1" and jobid != "PRISM4":
        continue
    plt.figure("Pixel Shifts for {0:s}".format(jobid))
    plt.clf()
    plt.hist(pixel_shifts["Horizontal"][:, 2], label="Horizontal")
    plt.hist(pixel_shifts["Vertical"][:, 2], label="Vertical")
    plt.legend()
    plt.ylim((0, 1000))
    plt.xlabel("Pixel Shift")

# calculate the interpolation spline for the pixel shift values
cam1_pixel_shift_interpolators = {}
for jobid, data in cam1_pixel_shift.items():
    cam1_pixel_shift_interpolators[jobid] = {}
    for orientation, pixel_shifts in data.items():
        cam1_pixel_shift_interpolators[jobid][orientation] = \
            spi.Rbf(pixel_shifts[:, 0], pixel_shifts[:, 1],
                    pixel_shifts[:, 2])
        cam1_pixel_shift_interpolators[jobid]["X Range"] = \
            (np.min(pixel_shifts[:, 0]), np.max(pixel_shifts[:, 0]))
        cam1_pixel_shift_interpolators[jobid]["Y Range"] = \
            (np.min(pixel_shifts[:, 1]), np.max(pixel_shifts[:, 1]))

# calculate prism diopter heat map
# from the #mmpx1 tag in DualCameraAlignmentCalibration.dat
camera_sensor_mm_to_pixels = 0.0024000001139938831
# from the lensx tag in DualCameraAlignmentCalibration.dat
camera_magnification = 0.083999998867511749

# calculate the prp by getting FCOCIN/UP from prescription
centre_of_lens_region_masks = {jobid:
                               {name[-4:]: (bb[0] + bb[2]/2,
                                            bb[1] + bb[3]/2)
                                for name, bb in lens.items()}
                               for jobid, lens in input_mask_bb.items()}
frame_centre_to_prp = {}
for jobid, rx in prescriptions.items():
    frame_centre_to_prp[jobid] = {}
    # left lens is cam1
    frame_centre_to_prp[jobid]["Cam1"] = \
        (convert_mm_to_camera_image_pixels(rx.left.frame_center_to_prp_in,
                                           camera_sensor_mm_to_pixels,
                                           camera_magnification),
         convert_mm_to_camera_image_pixels(rx.left.frame_center_to_prp_up,
                                           camera_sensor_mm_to_pixels,
                                           camera_magnification))
    # right lens is cam2
    frame_centre_to_prp[jobid]["Cam2"] = \
        (convert_mm_to_camera_image_pixels(rx.right.frame_center_to_prp_in,
                                           camera_sensor_mm_to_pixels,
                                           camera_magnification),
         convert_mm_to_camera_image_pixels(rx.right.frame_center_to_prp_up,
                                           camera_sensor_mm_to_pixels,
                                           camera_magnification))
lms_centre_offset_from_prp = \
    {jobid: {cam_name: (centre_of_lens_region_masks[jobid][cam_name][0] -
                        frame_centre_to_prp[jobid][cam_name][0],
                        centre_of_lens_region_masks[jobid][cam_name][1] +
                        frame_centre_to_prp[jobid][cam_name][1])
             for cam_name in ["Cam1", "Cam2"]}
     for jobid in jobids if jobid in frame_centre_to_prp.keys()}

# interpolate prism diopeters at PRP
prp_coordinates = {}
lens_type = pre.LensType.Single
for jobid, centre in centre_of_lens_region_masks.items():
    distance_reference_point = centre
    if lens_type == pre.LensType.Single:
        prp_coordinates[jobid] = distance_reference_point
    elif lens_type == pre.LensType.Progressive or \
            lens_type == pre.LensType.Degressive:
        near_interpupilary_distance = 1
        distance_interpupilary_distance = 2
        segment_centre_line = 1.5
        seg_line_offset = 0.5*(distance_interpupilary_distance -
                               near_interpupilary_distance)
        prp_coordinates[jobid] = (segment_centre_line + seg_line_offset,
                                  distance_reference_point[1])
    else:
        raise ValueError("Unrecognised lens type.")

# rotate and translate prp according to rotate and stitch
jobid = jobids[0]
for jobid in jobids:
    name = "SecondPos_PrismHorizontalLinesImage_Cam1Cam1"
    shape = cam1_contours[jobid][name]["Measured Image"].shape
    prp = (int(prp_coordinates[jobid]["Cam1"][0]),
           int(prp_coordinates[jobid]["Cam1"][1]))
    prp_image = np.zeros((shape[0], shape[1], 3))
    prp_image[:, :, 0] = cam1_contours[jobid][name]["Measured Image"]
    prp_image[:, :, 1] = cam1_contours[jobid][name]["Measured Image"]
    prp_image[:, :, 2] = cam1_contours[jobid][name]["Measured Image"]
    prp_image = cv2.circle(prp_image, prp, radius=5, color=(0, 255, 0),
                            thickness=-1)
    rotation_matrix = r_and_s[jobid].rotation_matrix_cam1
    width_difference = shape[0] - r_and_s[jobid].rotated_image_width_cam2
    height_difference = shape[1] - r_and_s[jobid].rotated_image_height_cam1
    translation_vector = (height_difference, width_difference)
    affine_transform = np.zeros((3, 3))
    affine_transform[2, 2] = 1.0
    affine_transform[:2, :2] = rotation_matrix[:2, :2]
    affine_transform[:2, 2] = translation_vector
    aligned_prp = np.matmul(affine_transform, np.array([prp[0], prp[1], 1]))
    aligned_prp = (int(aligned_prp[0]), int(aligned_prp[1]))
    prp_image = cv2.circle(prp_image, aligned_prp, radius=5, color=(255, 0, 0),
                           thickness=-1)
    test_plot("PRP Image for {0:s}".format(jobid), prp_image)

number_of_points = 100
cam1_prism_dioptres_new = {}
cam1_dioperts_at_prp_new = {}
for jobid, pixel_shift_interpolators in cam1_pixel_shift_interpolators.items():
    if jobid not in distance_between_sample_and_camera.keys():
        continue
    # calculate shift in mm from pixel shift
    x = np.linspace(pixel_shift_interpolators["X Range"][0],
                    pixel_shift_interpolators["X Range"][1],
                    number_of_points)
    y = np.linspace(pixel_shift_interpolators["Y Range"][0],
                    pixel_shift_interpolators["Y Range"][1],
                    number_of_points)
    pixel_shift_mm = {}
    for orientation, interpolator in pixel_shift_interpolators.items():
        if "X" in orientation or "Y" in orientation:
            continue
        xx, yy = np.meshgrid(x, y)
        pixel_shift_mm[orientation] = \
            camera_sensor_mm_to_pixels*interpolator(xx, yy)
    # heat map in, out, prism, base measurements using new calculation
    cam1_prism_dioptres_new[jobid] = {}
    cam1_prism_dioptres_new[jobid]["Horizontal"] = \
        prentices_rule(pixel_shift_mm["Horizontal"], camera_magnification,
                       distance_between_sample_and_camera[jobid]["Cam1"])
    cam1_prism_dioptres_new[jobid]["Vertical"] = \
        prentices_rule(pixel_shift_mm["Vertical"], camera_magnification,
                       distance_between_sample_and_camera[jobid]["Cam1"])
    cam1_prism_dioptres_new[jobid]["Prism"] = \
        np.sqrt(cam1_prism_dioptres_new[jobid]["Horizontal"]**2 +
                cam1_prism_dioptres_new[jobid]["Vertical"]**2)
    cam1_prism_dioptres_new[jobid]["Base"] = \
        np.arctan2(cam1_prism_dioptres_new[jobid]["Horizontal"]**2,
                   cam1_prism_dioptres_new[jobid]["Vertical"]**2)*180.0/np.pi
    # in, out, prism, base measurements at prp
    prp_x = prp_coordinates[jobid]["Cam1"][1]
    prp_y = prp_coordinates[jobid]["Cam1"][0]
    prp_horizontal_pixel_shift = camera_sensor_mm_to_pixels * \
        pixel_shift_interpolators["Horizontal"](prp_x,  prp_y)
    prp_vertical_pixel_shift = camera_sensor_mm_to_pixels * \
        pixel_shift_interpolators["Vertical"](prp_x,  prp_y)
    prp_pixel_shift_magnitude = \
        np.sqrt(prp_horizontal_pixel_shift**2 + prp_vertical_pixel_shift**2)
    prp_pixel_shift_magnitude = \
        np.sqrt(prp_horizontal_pixel_shift**2 + prp_vertical_pixel_shift**2)
    # calculations using new formula
    cam1_dioperts_at_prp_new[jobid] = {}
    cam1_dioperts_at_prp_new[jobid]["Horizontal"] = \
        prentices_rule(prp_horizontal_pixel_shift, camera_magnification,
                       distance_between_sample_and_camera[jobid]["Cam1"])
    cam1_dioperts_at_prp_new[jobid]["Vertical"] = \
        prentices_rule(prp_vertical_pixel_shift, camera_magnification,
                       distance_between_sample_and_camera[jobid]["Cam1"])
    cam1_dioperts_at_prp_new[jobid]["Prism"] = \
        np.sqrt(cam1_dioperts_at_prp_new[jobid]["Horizontal"]**2 +
                cam1_dioperts_at_prp_new[jobid]["Vertical"]**2)
    cam1_dioperts_at_prp_new[jobid]["Base"] = \
        np.arctan2(cam1_dioperts_at_prp_new[jobid]["Horizontal"]**2,
                   cam1_dioperts_at_prp_new[jobid]["Vertical"]**2)*180.0/np.pi
    # not orientation labels reversed because horizontal lines give vertical
    # prism and vice versa
    print()
    print(jobid + " Horizontal Prism @ PRP = " +
          "{0:.2f}".format(cam1_dioperts_at_prp_new[jobid]["Vertical"]))
    print(jobid + " Vertical Prism @ PRP = " +
          "{0:.2f}".format(cam1_dioperts_at_prp_new[jobid]["Horizontal"]))
    print(jobid + " Prism @ PRP = " +
          "{0:.2f}".format(cam1_dioperts_at_prp_new[jobid]["Prism"]))
    print(jobid + " Base @ PRP = " +
          "{0:.2f}".format(cam1_dioperts_at_prp_new[jobid]["Base"]))
    print(jobid + " Min Prism = " +
          "{0:.2f}".format(np.min(cam1_prism_dioptres_new[jobid]["Prism"])))
    print(jobid + " Max Prism = " +
          "{0:.2f}".format(np.max(cam1_prism_dioptres_new[jobid]["Prism"])))

# prism heat maps
for jobid, prism_dioptres in cam1_prism_dioptres_new.items():
    fig = plt.figure(jobid + " Prism Heatmap")
    fig.clf()
    c = fig.gca().pcolormesh(xx, yy, prism_dioptres["Prism"],
                             vmin=0.0,
                             vmax=prism_dioptres["Prism"].max())
    fig.colorbar(c, ax=fig.gca())
    fig.gca().scatter(prp_coordinates[jobid]["Cam1"][1],
                      prp_coordinates[jobid]["Cam1"][0],
                      marker="x", c="g")
    plt.show()
