# -*- coding: utf-8 -*-
"""
Created on Fri Aug 13 12:23:13 2021
Generates the different patterns that the eMap puts on the 4K monitor.  See
EyeTech.Recipes.Lensmeter.Imaging.Patterns.LensmeterPatternsGenerator.  Note
the class in C# is static, the python equivalent is simply a module.
@author: RhysPoolman
"""
from patterns import args
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os

_COLOR_WHITE = (255, 255, 255, 255, 255)
_COLOR_GREEN = (0, 255, 0, 255)
_COLOR_RED = (255, 0, 0, 255)
_COLOR_BLACK = (0, 0, 0, 255)


def _get_color(color_type):
    """
    Creates a tuple giving the RGB color values based on the passed ColorType
    enum.

    Parameters
    ----------
    color_type : args.ColorType
        An enum that is used to specify either black, white, green or red.

    Returns
    -------
    color : Four Integer Tuple
        The RGBalpha values of a color.

    """
    color = None

    if color_type == args.ColorTypes.black:
        color = _COLOR_BLACK
    elif color_type == args.ColorTypes.white:
        color = _COLOR_WHITE
    elif color_type == args.ColorTypes.green:
        color = _COLOR_GREEN
    elif color_type == args.ColorTypes.red:
        color = _COLOR_RED
    else:
        color = None

    return color


def create_horizontal_line_pattern(arg_set):
    """
     Generate a pattern with a full-intensity green background, horizontal
     black lines and a horizontal center line that is a different green from
     the background.  If defining an offset for the center line away from the
     image center, only args.OffsetY will be applied to this pattern.  A
     positive value for args.OffsetY will move the pattern's center line
     towards the bottom of the image.

    Parameters
    ----------
    arg_set : args.LensmeterPatternsGeneratorArgs
        The arguments for the pattern generation.

    Returns
    -------
    array of integers
        The buffer containing the pattern that the monitor will create.

    """
    image = None
    width = arg_set.image_width
    height = arg_set.image_height
    line_thickness = arg_set.line_thickness
    go = width > 0.0 and height > 0.0 and line_thickness > 0.0

    if go:
        image = np.zeros((height, width, 4), dtype=np.uint8)  # MatType.CV_8UC4
        color = _get_color(arg_set.color)
        red = color[0]
        green = color[1]
        blue = color[2]
        alpha = color[3]

        # create background
        image[:, :, 0].fill(blue)
        image[:, :, 1].fill(green)
        image[:, :, 2].fill(red)
        image[:, :, 3].fill(alpha)

        # flag for filled rectangles
        cv_filled = -1

        # calculate center point of the image
        image_center_point = np.array((np.int32(width/2), np.int32(height/2)))

        # apply any y offset to the image center point
        image_center_point[1] += np.int32(arg_set.offset_y)

        # calculate the top left point of the middle line if the line thickness
        # is odd ruound up
        mid_line_top_left_point = \
            (0, image_center_point[1] +
             np.int32(np.ceil(line_thickness/2.0)))
        mid_line_bottom_right_point = \
            (width, image_center_point[1] -
             np.int32(np.ceil(line_thickness/2.0)))
        center_line_color = None
        if arg_set.color == args.ColorTypes.green:
            center_line_color = (0, 150, 0, 255)
        else:
            center_line_color = (0, 0, 150, 255)

        # the center linse is a different color
        cv.rectangle(image,
                     mid_line_top_left_point,
                     mid_line_bottom_right_point,
                     center_line_color,
                     cv_filled)

        # draw black rectangels below the colored rectangle
        black_lines_start_y = mid_line_top_left_point[1] - 2*line_thickness
        y = black_lines_start_y
        while y > 0:
            cv.rectangle(image, (0, y), (width, y - line_thickness),
                         _COLOR_BLACK, cv_filled)
            y -= 2 * line_thickness

        #  draw the black rectangles above the coored rectrangle
        black_lines_start_y = mid_line_top_left_point[1] + 2*line_thickness
        y = black_lines_start_y
        while y < height:
            cv.rectangle(image, (0, y), (width, y - line_thickness),
                         _COLOR_BLACK, cv_filled)
            y += 2*line_thickness

    return image


def create_vertical_line_pattern(arg_set):
    """
    Create a vertical line pattern by transposing the output of
    create_horizontal_line_pattern.  Please note this is not how it's done in
    the eMap source code.

    Parameters
    ----------
    arg_set : args.LensmeterPatternsGeneratorArgs
        The arguments for the pattern generation.

    Returns
    -------
    array of integers
        The buffer containing the pattern that the monitor will create.
    """
    image = create_horizontal_line_pattern(arg_set)
    return np.stack((image[:, :, 0].T, image[:, :, 1].T, image[:, :, 2].T,
                    image[:, :, 3].T), axis=2)


def plot_image(image, filename=None):
    """
    Plots a png file of the pattern using matplotlib.  Saves tje pattern as a
    png using OpenCV.  The filename is image.png saved in the working directory
    by default

    Parameters
    ----------
    image : numpy.ndarray
        A 3 dimensional array representing the image.  The thrid dimension has
        four channels rgb and alpha.
    filename : string, optional
        The filename for the saved PNG file. The default is None.

    Returns
    -------
    None.
    """
    filename = os.getcwd() + "\\image.png" if filename is None else filename
    cv.imwrite(filename, image)
    plt.figure("Pattern")
    plt.clf()
    plt.imshow(mpimg.imread(filename))


def plot_image_opencv(image):
    """
    Plots the image using OpenCV routines.

    Parameters
    ----------
    image : 3d numpy.ndarray
        TAn image descibed with a 3 dimensional with the final fimension have
        four elemnts for RGB and alpha.

    Returns
    -------
    None.
    """
    cv.imshow("iamge", image)
    cv.waitKey(0)
