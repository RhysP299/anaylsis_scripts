# -*- coding: utf-8 -*-
"""
Created on Fri Aug 13 12:22:36 2021
The settings and arguments used to generate a monitor pattern.  The Enums are
taken from the eMap code.  See
.EyeTech.Recipies.Lensmeter.Imaging.Patterns.LensmeterPatternsGeneratorArgs
@author: RhysPoolman
"""
from enum import Enum


class ColorTypes(Enum):
    black = 0,
    white = 1,
    green = 2,
    red = 3


class PatternTypes(Enum):
    """
    The types of possible patterns. All patterns will have black background
    unless specified. Also dots are all green unless specified.  NOTE: The
    generator will only generate patterns on correct parameters. If
    inappropriate parameters are used then patterns may not be rendered or
    they are rendered incorrectly.  This class should be an immutable.
    """

    """
    An undefined pattern; this is an errorneous pattern.
    """
    undefined = 0,

    """
    Creates a regular rectangular grid of dots (in defined period/interval).
    Parameters: diameter of dots depends on the thickness parameter and period.
    """
    grid_rect = 1,

    """
    Creates a single hexagonal pattern. Each vertex is a dot and the seventh
    dot is in the middle.
    Parameters: diameter of dots depends on the period parameter. Also allow
    offsets X and Y.  The base image is generated with 0,0 offset. However, if
    offsets are used then the entire base image is shifted by the required
    offset.
    """
    single_hex = 2,

    """
    Creates a grid of dots in the hexagonal pattern.
    Parameters: diameter of dots depends on the period parameter. Also allow
    offsets X and Y.  The base image is generated with 0,0 offset. However, if
    offsets are used then the entire base image is shifted by the required
    offset.
    """
    grid_hex = 3,

    """
    Creates a single circular dot.
    Parameters: the shape depends on values of thickness and radius.
    If thickness and radius are different values and thickness is much smaller
    than radius then the shape is annular ring in which the outer ring depends
    on the thickness value and inner ring depends on the radius value. The
    outer ring is e.g. green and the inner ring follows the background color
    black (default).
    """
    single_circular_kind = 4,

    """
    Creates a dual circular pattern.
    Parameters: The shape depends on values of thickness and radius.
    Similar to SingleCircularKind where there is an annular ring (depending on
    thickness and radius). In addition, there is also a fixed dot inside the
    ring at its center, therefore, its name dual circular kind pattern.
    """
    dual_circular_kind = 5,

    """
    Displays the pattern for the dual camera alignment (dual lensmeter). This
    requires offsets X and Y in order to get 4 dots. If offsets are 0s then you
    see one dot in the middle.  Diameter of dots is internally fixed at
    designated pixels size.
    Parameters: The positions are dependent on Offsets X and Y.
    """
    dual_lensmeter_camera_alignment = 6,

    """
    Creates a bounding frame/rect then a cross that is located diagonal to
    diagonal on the image.  Maybe useful to show user the screen/display size.
    Parameters: None
    """
    bounding_area_test_box = 7,

    """
    Creates a background of image where all pixels colour is
    LensmeterPatternsGeneratorArgs.Color.
    Parameters: None.
    """
    background_color = 10,

    """
    Creates a background with checkerboard effect.
    """
    checker_board = 11,

    """
    Create a pattern with full-intensity ( RGB( 0, 255, 0 ) ) green background
    and horizontal black lines.  The centre line is a different green from the
    background.
    Parameters: Line thickness and optional YOffset.  If provided, a positive
    YOffset moves the centre line in the pattern towards the bottom.
    """
    horizontal_lines = 12,

    """
    Create a pattern with full-intensity ( RGB( 0, 255, 0 ) ) green background
    and vertical black lines.  The centre line is a different green from the
    background.
    Parameters: Line thickness and optional XOffset.  If provided, a positive
    XOffset moves the centre line in the pattern towards the right.
    """
    vertical_lines = 13,

    """
    Creates a black image with white vertical lines(bars).  The following can
    be specified:
    imageWidthPx: The width of the image.
    imageHeightPx: The height of the image.
    barWidthPx: The width of the vertical bar to be drawn.
    barHeightPx:The height of the vertical bar to be drawn.
    offsetType:
    offsetValue: Space inbetween bars.
    startShift: Column offset for first bar.
    """
    vertical_surface_inspection_v2 = 14


class LensmeterPatternsGeneratorArgs:

    DEFAULT_ENCODED_PERIOD_TYPE = 4

    def __init__(self, pattern_type=PatternTypes.undefined,
                 encoded_period_type=DEFAULT_ENCODED_PERIOD_TYPE,
                 line_thickness=1, radius=10, image_width=800,
                 image_height=800, offset_x=0.0, offset_y=0.0,
                 start_shift=0.0, color=ColorTypes.black):
        """
        Constructor for the class that contains the arguments for the
        pattern gernerator.

        Parameters
        ----------
        pattern_type: PatternTypes
             The type of patterns to create. Default PatternTypes.Undefined.
        encoded_period_type: integer
            The encoded period type. This is used for rapid development
            (LabView) in which the unencoded value is the actual value used as
            the period parameter to generate patterns for a sequence. Use the
            converter method to convert to actual values. Default is type 4.
            NOTE: the unencoded value is in pixels.
        line_thickness: float
            The line thickness of the drawing pen. A thickness of 10 creates a
            line thickness that is -5 and +5 from the incident stroke path.
            Default 1. Also applies to the thickness of the lines for the
            prism measurement pattern.
        radius: float
             The radius of a circle. Only applicable to certain patterns e.g.
             Circular and CircularDual. Default 10.  Also applies to
             checkerboard pattern. Checkerboard pattern creates a rectangle
             with a width and height equal to radius.
        image_width: float
            The width of the image. Default 800.
        image_height: float
            The height of the image. Default 800.
        offset_x: float
            The x-offset where pattern is located. A pattern is always drawn
            at the center. Default 0.  Depending on PatternTypes, a positive x
            offset moves the blob to the left, performs mirroring on its axis
            or not applicable.  NOTE: Depending on PatternTypes, a positive x
            offset moves the blob to the left, performs mirroring on its axis
            or not applicable. Having said above, if there are patterns that
            needs x-offset going to right of screen using the programmer's
            coordinate system origin at top left corner, then a positive
            offset goes right. This should be made clear on the pattern
            generator arg's enum.
        offset_y, float:
            The y-offset where pattern is located. A pattern is always drawn
            at the center.  NOTE: Depending on PatternTypes, a positive y
            offset moves the blob to the top, performs mirroring on its axis
            or not applicable.  Having said above, if there are patterns that
            needs y-offset going to bottom of screen using the programmer's
            coordinate system origin at top left corner, then a positive
            offset goes bottom. This should be made clear on the pattern
            generator arg's enum.
        start_shift: float
            Column offset for first bar. Applies to
            vertical_surface_inspection_v2.
        color: ColorTypes
            The type of colour to be used to do the measurement.
        """
        self._pattern_type = pattern_type
        self._encoded_period_type = encoded_period_type
        self._line_thickness = line_thickness
        self._radius = radius
        self._image_width = image_width
        self._image_height = image_height
        self._offset_x = offset_x
        self._offset_y = offset_y
        self._start_shift = start_shift
        self._color = color

    @property
    def pattern_type(self):
        """
        The pattern_tpye property.

        Returns
        -------
        PatternTypes
            The type of patterns to create. Default PatternTypes.Undefined.
        """
        return self._pattern_type

    @property
    def encoded_period_type(self):
        """
        The encoded_period_type property.

        Returns
        -------
        integer
            The encoded period type. This is used for rapid development
            (LabView) in which the unencoded value is the actual value used
            as the period parameter to generate patterns for a sequence.  Use
            the converter method to convert to actual values. Default is
            type 4.
            NOTE: the unencoded value is in pixels.
        """
        return self._encoded_period_type

    @property
    def line_thickness(self):
        """
        The property for line_thickness.

        Returns
        -------
        float
            The line thickness of the drawing pen. A thickness of 10 creates a
            line thickness that is -5 and +5 from the incident  stroke path.
            Default 1. Also applies to the thickness of the lines for the
            prism measurement pattern.
        """
        return self._line_thickness

    @property
    def radius(self):
        """
        The property for radius.

        Returns
        -------
        float
            The radius of a circle. Only applicable to certain patterns e.g.
            Circular and CircularDual. Default 10.  Also applies to
            checkerboard pattern. Checkerboard pattern creates a rectangle with
            a width and height equal to radius.
        """
        return self._radius

    @property
    def image_width(self):
        """
        The property for image_width.

        Returns
        -------
        float
            The width of the image. Default 800.
        """
        return self._image_width

    @property
    def image_height(self):
        """
        The property for image_height.

        Returns
        -------
        float
            The height of the image. Default 800.
        """
        return self._image_height

    @property
    def offset_x(self):
        """
        The property for offset_x

        Returns
        -------
        float
            The x-offset where pattern is located. A pattern is always drawn
            at the center. Default 0.  Depending on PatternTypes, a positive x
            offset moves the blob to the left, performs mirroring on its axis
            or not applicable.  NOTE: Depending on PatternTypes, a positive x
            offset moves the blob to the left, performs mirroring on its axis
            or not applicable. Having said above, if there are patterns that
            needs x-offset going to right of screen using the programmer's
            coordinate system origin at top left corner, then a positive offset
            goes right. This should be made clear on the pattern generator
            arg's enum.
        """
        return self._offset_x

    @property
    def offset_y(self):
        """
        The property for offset_y

        Returns
        -------
        float
            The y-offset where pattern is located. A pattern is always drawn at
            the center. Default 0.  Depending on PatternTypes, a positive y
            offset moves the blob to the bottom, performs mirroring on its axis
            or not applicable.  NOTE: Depending on PatternTypes, a positive y
            offset moves the blob to the bottom, performs mirroring on its axis
            or not applicable. Having said of screen using the programmer's
            coordinate system origin at top left corner, then a positive offset
            goes right. This should be made clear on the pattern generator
            arg's enum.
        """
        return self._offset_y

    @property
    def start_shift(self):
        """
        The property for start_shift.

        Returns
        -------
        float
            Column offset for first bar. Applies to
            vertical_surface_inspection_v2.

        """
        return self._start_shift

    @property
    def color(self):
        """
        The property for color.

        Returns
        -------
        ColorType
            The type of colour to be used to do the measurement.
        """
        return self._color
