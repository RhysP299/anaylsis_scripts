# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 13:18:42 2022
Test prism folder
@author: RhysPoolman
"""
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

# load files
path = "R:\\"
filenames = ["horizontal_search_lines_cam_1.csv",
             "horizontal_search_lines_cam_2.csv",
             "vertical_search_lines_cam_1.csv",
             "vertical_search_lines_cam_2.csv"]
data_names = [filename.split(".")[0] for filename in filenames]
data = {filename.split(".")[0]:
        np.loadtxt(path + filename, dtype="float", delimiter=",")
        for filename in filenames}

# calculate pixel shift magnitude cam1
camera_image_mm__to_pixels_conversion_factor = 0.02857143
x_cam1 = np.zeros(len(data[data_names[2]]))
y_cam1 = np.zeros(len(data[data_names[2]]))
horizontal_pixel_shift = data[data_names[2]]
vertical_pixel_shift = data[data_names[0]]
pixel_shift_magnitudes_cam_1 = np.zeros(len(horizontal_pixel_shift))
# for kk, row in enumerate(horizontal_pixel_shift):
#     x_cam1[kk] = row[0]
#     y_cam1[kk] = row[1]
#     # print(row[0], row[1])
#     index = np.logical_and(vertical_pixel_shift[:, 0] == row[0],
#                            vertical_pixel_shift[:, 1] == row[1])
#     pixel_shift_magnitudes_cam_1 = \
#         np.sqrt(row[2]*row[2] +
#                 vertical_pixel_shift[index, 2]*vertical_pixel_shift[index, 2])
#     pixel_shift_magnitudes_cam_1 *= \
#         camera_image_mm__to_pixels_conversion_factor

# calculate pixel shift magnitude cam1
x_cam2 = np.zeros(len(data[data_names[3]]))
y_cam2 = np.zeros(len(data[data_names[3]]))
horizontal_pixel_shift = data[data_names[3]]
vertical_pixel_shift = data[data_names[1]]
pixel_shift_magnitudes_cam_2 = np.zeros(len(horizontal_pixel_shift))
for kk, row in enumerate(horizontal_pixel_shift):
    x_cam2[kk] = row[0]
    y_cam2[kk] = row[1]
    index = vertical_pixel_shift[vertical_pixel_shift[:, 0] == row[0]].all() \
        and vertical_pixel_shift[vertical_pixel_shift[:, 1] == row[1]].all()
    pixel_shift_magnitudes_cam_2 = \
        np.sqrt(row[2]*row[2] +
                vertical_pixel_shift[index, 2]*vertical_pixel_shift[index, 2])
    pixel_shift_magnitudes_cam_1 *= \
        camera_image_mm__to_pixels_conversion_factor

# calculate prism diopters
magnification = 0.084
distance_between_sample_and_camera = 367.74
prentice_cam_1 = \
    100*pixel_shift_magnitudes_cam_1 /\
    (magnification*distance_between_sample_and_camera)
prentice_cam_2 = \
    100*pixel_shift_magnitudes_cam_2 /\
    (magnification*distance_between_sample_and_camera)
