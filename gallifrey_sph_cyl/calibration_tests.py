# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:09:31 2022

@author: FionaLoftus
"""

import pandas as pd
from src.statistical_tests import CalibrationData, CalibrationComparison

def run_individual_error_plots(cali_data,
                               data_label,
                               figname,
                               figpath,
                               tol_lim=0.06):
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            cali_data.plot_calibration_error(side, 
                                          prescriptive_component,
                                          data_label, 
                                          fig_path=figpath, 
                                          fig_name=figname,
                                          tolerance_lim=tol_lim)
            
def run_error_comparison_plots(cali_comp,
                         figname,
                         figpath,
                         tol_lim=0.06):
    
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            cali_comp.plot_error_comparison(prescriptive_component,
                                     side, 
                                     tolerance_lim=tol_lim, 
                                     fig_name=figname,
                                     fig_path=figpath)
            
def run_stats_comparison_plots(cali_comp,
                               stats_type,
                               figname,
                               figpath):
    
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            c1.plot_stats_comparison(prescriptive_component,
                                     side, 
                                     stats_type=stats_type, 
                                     fig_name=figname,
                                     fig_path=figpath)
            
# %%
# =============================================================================
# 19/12/22 NextGen Vs e2_Calibration_verification_noserial
# =============================================================================
path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey'
filename = 'e2_Calibration_verification_noserial.xlsx'
cali_dict = {'datasource_type': 'calibration',
             'cali_type': 'xlsx',
             'calibration_config': 'calibration_config_og.yml',
             'path': path,
             'filename': filename}
path = 'D:\\Gallifrey\\lenscal1912\\lenscal'
filename = 'data.ini'
cal_ini_dict = {'datasource_type': 'calibration',
             'cali_type': 'ini',
             'calibration_config': 'calibration_config_og.yml',
             'path': path,
             'filename': filename}
d_new = CalibrationData(**cal_ini_dict)
d_old = CalibrationData(**cali_dict)
c_dict = {'Old code': d_old,
      'NextGen': d_new}
c1 = CalibrationComparison(c_dict)

fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\Sph_cyl_axis_measurements\\calibration\\191222'
nextgenfig_name = '19-12-22'
oldfig_name = 'e2_Calibration_verification_noserial'
# %%
# 1. INDIVIDUAL ERROR PLOTS
# =============================================================================
run_individual_error_plots(d_new,
                           'NextGen',
                           nextgenfig_name,
                           fig_path)

run_individual_error_plots(d_old,
                           'Old code',
                           oldfig_name,
                           fig_path)

# 2. ERROR COMPARISON PLOTS
# =============================================================================
fig_comp_name = '19-12-22 vs e2_Calibration_verification_noserial'
run_error_comparison_plots(c1, fig_comp_name, fig_path)

# 3. MEAN ABSOLUTE ERROR COMPARISON
# =============================================================================
stats_type = 'Mean Absolute'
run_stats_comparison_plots(c1, stats_type, fig_comp_name, fig_path)

# 4. MEAN ERROR COMPARISON
# =============================================================================
stats_type = 'Mean'
run_stats_comparison_plots(c1, stats_type, fig_comp_name, fig_path)

# 4. ERROR STANDARD DEVIATION COMPARISON
# =============================================================================
stats_type = 'Standard Deviation'
run_stats_comparison_plots(c1, stats_type, fig_comp_name, fig_path)

# 5. ERROR RANGE COMPARISON
# =============================================================================
stats_type = 'Range (Max-Min)'
run_stats_comparison_plots(c1, stats_type, fig_comp_name, fig_path)

# =============================================================================
# 6. STATS
# =============================================================================
# %%
# GLOBAL MEAN
# NEXTGEN
d_new.global_mean_error(absolute=True)
# OLD
d_old.global_mean_error(absolute=True)
# %%
# MEAN OF MEAN
# NEXTGEN
d_new.get_class_stats_mean('mean absolute')
# %%
# OLD
d_old.get_class_stats_mean('mean absolute')
# %%
# MEAN OF STD
# NEXTGEN
d_new.get_class_stats_mean('std')
# %%
# OLD
d_old.get_class_stats_mean('std')
# %%
# MEAN OF RANGE
# NEXTGEN
d_new.get_class_stats_mean('range')
# %%
# OLD
d_old.get_class_stats_mean('range')
# %%
d_old.get_within_power_std()
# %%
d_old.get_within_power_range()
