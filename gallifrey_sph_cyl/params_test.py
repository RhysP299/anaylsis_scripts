# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 14:08:49 2022

@author: FionaLoftus
"""
import numpy as np
import os
import matplotlib.pyplot as plt
# %%
# parameters test

power_range = np.arange(-10,11,1)

d1 = 19.95
april_tag_base = 4
april_tag_rp = 8
baseline_d = 142.234

baseline_d_pixels = (baseline_d/april_tag_base)*april_tag_rp

mag_1 = []
for p in power_range:
   m = 1 / (d1*p+1)
   mag_1.append(m)
   
plt.plot(power_range, mag_1)

r_1 = []

for m in mag_1:
    r = m * baseline_d_pixels
    r_1.append(r)

fig, ax = plt.subplots()
plt.plot(power_range[:-11], r_1[:-11])

fig, ax = plt.subplots()
plt.plot(power_range[11:], r_1[11:])

# %%
# Current system
d1 = 15
d1_m = d1/1000
baseline_r = 73

power_range = np.arange(-10,11,1)

mag_1 = []
for p in power_range:
   m = 1 / (d1_m*p+1)
   mag_1.append(m)
   
plt.plot(power_range, mag_1)

