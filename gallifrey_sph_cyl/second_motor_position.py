# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 12:38:22 2023

@author: FionaLoftus
"""

# =============================================================================
# Calibration lenses quick motor position calculations
# =============================================================================
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm

def calculate_m1(p, d1):
    
    m1 = 1 / (1-d1*p)
    
    return m1

def calculate_m2_req(p, m1):

    if p > 0:
        m2_req = 1.25*m1
    elif p <= 0:
        m2_req = 0.75*m1
        
    return m2_req

def calculate_d2_req(p, m2_req):
    
    d2_req = ((m2_req - 1) / (p*m2_req)) * 1000
    
    return d2_req

def calculate_d2_from_home(d2, d_home):
    
    d2_from_home = d_home - d2
    
    return d2_from_home

df = pd.DataFrame(columns=['EPSph', 'EstMag1', 'd1MM', 'Mag2Req' , 'd2ReqMM', 'd2FromHome'])
max_distance_from_displayMM = 53
position0_from_homeMM = 15
motor_range = 19.36
d1MM = max_distance_from_displayMM - position0_from_homeMM
d1M = d1MM / 1000
# df = pd.DataFrame(columns=['EPSph', 'EstMag1', 'd1', 'Mag2Req' , 'd2Req', 'd2FromHome'])
EP_Sph = [-14.991, -9.997, -5.036, 5.024, 9.957, 14.963]
df['EPSph'] = EP_Sph
df['EstMag1'] = df['EPSph'].apply(calculate_m1, d1=d1M)
df['d1MM'] = d1MM
df['Mag2Req'] = df.apply(lambda row: calculate_m2_req(row.EPSph, row.EstMag1), axis=1)
df['d2ReqMM'] = df.apply(lambda row: calculate_d2_req(row.EPSph, row.Mag2Req), axis=1)
df['d2FromHome'] = df['d2ReqMM'].apply(calculate_d2_from_home, d_home=max_distance_from_displayMM)

# %%
fig, ax = plt.subplots()
cmap = cm.get_cmap('tab10')

# test = [i for i in enumerate(df['d2FromHome'])]

ax.hlines([0, motor_range, max_distance_from_displayMM], xmin=0, xmax=10, colors=['r', 'r', 'b'])
ax.hlines([position0_from_homeMM], xmin=0, xmax=10, colors=['k'])
ax.hlines(df['d2FromHome'], xmin=0, xmax=10, colors=[cmap(i[0]) for i in enumerate(df['d2FromHome'])], linestyles='dashed')


    
    
