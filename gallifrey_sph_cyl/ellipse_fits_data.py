# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 16:23:37 2022

@author: FionaLoftus
"""

import configparser
import os
import pandas as pd
import csv
from src.statistical_tests import DataSample, GoldenData, DataPlots
from tools.data_elt import load_yaml
from tools.utilities import get_project_root

# %%
# path = 'D:\\Gallifrey\\rerun_results_25_11'
# fig_path = \
    # 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\Sph_cyl_axis_measurements\\25_11_22\\rerun'
# %%
path = 'D:\\Gallifrey\\jobs_30_11'
fig_path = \
    'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\Sph_cyl_axis_measurements\\30_11_22_GS4'

# %%
file = 'data.ini'
ini_dict = {'datasource_type': 'ini',
            'path': path,
            'filename': file,
            'lens_type': 'PAL'}
file_path = os.path.join(get_project_root())
filename = 'local_config.yml'
local_configs = load_yaml(file_path, filename)
golden_dict = {'datasource_type': 'xlsx',
               'path': local_configs['golden_path'],
               'filename': local_configs['golden_filename']}
csv_dict = {'datasource_type':'csv',
            'path': 'D:\Measure_{0}\Alex210722',
            'filename': 'All Devices.csv'}
# %%
gs = GoldenData(**golden_dict)
d1 = DataSample(golden_data=gs, **ini_dict)
dp = DataPlots(d1)

# %%
# Alex data
gs = GoldenData(**golden_dict)
d1 = DataSample(golden_data=gs, **csv_dict)
dp = DataPlots(d1)
fig_path = \
    'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\OLD_device'

# %%
d1.data['OculusDexter']['r1diff'] = d1.data['OculusDexter']['r1Pos0'].apply(pd.to_numeric) -\
    d1.data['OculusDexter']['r1Pos1'].apply(pd.to_numeric)
d1.data['OculusSinister']['r1diff'] = d1.data['OculusSinister']['r1Pos0'].apply(pd.to_numeric) -\
    d1.data['OculusSinister']['r1Pos1'].apply(pd.to_numeric)
d1.data['OculusDexter']['r2diff'] = d1.data['OculusDexter']['r2Pos0'].apply(pd.to_numeric) -\
    d1.data['OculusDexter']['r2Pos1'].apply(pd.to_numeric)
d1.data['OculusSinister']['r2diff'] = d1.data['OculusSinister']['r2Pos0'].apply(pd.to_numeric) -\
    d1.data['OculusSinister']['r2Pos1'].apply(pd.to_numeric)
d1.data['OculusDexter']['r1_r2diff0'] = d1.data['OculusDexter']['r1Pos0'].apply(pd.to_numeric) -\
    d1.data['OculusDexter']['r2Pos0'].apply(pd.to_numeric)
d1.data['OculusSinister']['r1_r2diff0'] = d1.data['OculusDexter']['r1Pos0'].apply(pd.to_numeric) -\
    d1.data['OculusSinister']['r2Pos0'].apply(pd.to_numeric)
d1.data['OculusDexter']['r1_r2diff1'] = d1.data['OculusDexter']['r1Pos1'].apply(pd.to_numeric) -\
    d1.data['OculusDexter']['r2Pos1'].apply(pd.to_numeric)
d1.data['OculusSinister']['r1_r2diff1'] = d1.data['OculusDexter']['r1Pos1'].apply(pd.to_numeric) -\
    d1.data['OculusSinister']['r2Pos1'].apply(pd.to_numeric)
# %%

# %%
# 1. Mean error
sphere_diff = [i[0] for i in d1.datadiff['Sphere']]
d1.datadiff_means
d1.datadiff_std
d1.proportion_lenses_within_tolerance()
# 2. Proportion of lenses within tolerance
# %%
# SPHERE
fig_name = 'Gallifrey'
sphere_fig_path = os.path.join(fig_path, 'sphere')
df = dp.correlated_power_plot_gs('Sphere', 
                                 'Sphere', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=sphere_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Cylinder', 
                                 'Sphere', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=sphere_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Axis', 
                                 'Sphere', 
                                 'Degrees', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=sphere_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sph Equiv', 
                                 'Sphere', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=sphere_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Add', 
                                 'Sphere', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=sphere_fig_path,
                                 fig_name='Gallifrey')
# %%
df = dp.correlated_power_plot_extra_data('r1Pos0', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1Pos1', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r2Pos0', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r2Pos1', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1diff', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r2diff', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1_r2diff0', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1_r2diff1', 
                                 'Sphere', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
# %%
df['r1Pos0']
# %%
# CYLINDER
cylinder_fig_path = os.path.join(fig_path, 'cylinder')
df = dp.correlated_power_plot_gs('Cylinder', 
                                 'Cylinder', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=cylinder_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sphere', 
                                 'Cylinder', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=cylinder_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Axis', 
                                 'Cylinder', 
                                 'Degrees', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=cylinder_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sph Equiv', 
                                 'Cylinder', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=cylinder_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Add', 
                                 'Cylinder', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=cylinder_fig_path,
                                 fig_name='Gallifrey')
# %%
df = dp.correlated_power_plot_extra_data('r1diff', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r2diff', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1_r2diff0', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1_r2diff1', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
# %%
df = dp.correlated_power_plot_extra_data('r1Pos0', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r1Pos1', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r2Pos0', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_extra_data('r2Pos1', 
                                 'Cylinder', 
                                 '', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
# %%
# AXIS
axis_fig_path = os.path.join(fig_path, 'axis')
df = dp.correlated_power_plot_gs('Axis', 
                                 'Axis', 
                                 'Degrees', 
                                 'Degrees', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=2,
                                 fig_path=axis_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sphere', 
                                 'Axis', 
                                 'D', 
                                 'Degrees', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=2,
                                 fig_path=axis_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Cylinder', 
                                 'Axis', 
                                 'D', 
                                 'Degrees', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=2,
                                 fig_path=axis_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sph Equiv', 
                                 'Axis', 
                                 'D', 
                                 'Degrees', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=2,
                                 fig_path=axis_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Add', 
                                 'Axis', 
                                 'D', 
                                 'Degrees', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=2,
                                 fig_path=axis_fig_path,
                                 fig_name='Gallifrey')
# %%
# SPH EQUIV
df = dp.correlated_power_plot_gs('Sphere', 
                                 'Sph Equiv', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.13,
                                 fig_path=fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Cylinder', 
                                  'Sph Equiv', 
                                  'D', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.13,
                                  fig_path=fig_path,
                                  fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Axis', 
                                  'Sph Equiv', 
                                  'Degrees', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.13,
                                  fig_path=fig_path,
                                  fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sph Equiv', 
                                  'Sph Equiv', 
                                  'D', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.13,
                                  fig_path=fig_path,
                                  fig_name='Gallifrey')
# %%
# ADD
add_fig_path = os.path.join(fig_path, 'add')
df = dp.correlated_power_plot_gs('Sphere', 
                                 'Add', 
                                 'D', 
                                 'D', 
                                 singleline_plot=True, 
                                 prescriptive_component2_tol=0.12,
                                 fig_path=add_fig_path,
                                 fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Cylinder', 
                                  'Add', 
                                  'D', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.12,
                                  fig_path=add_fig_path,
                                  fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Axis', 
                                  'Add', 
                                  'Degrees', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.12,
                                  fig_path=add_fig_path,
                                  fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Sph Equiv', 
                                  'Add', 
                                  'D', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.12,
                                  fig_path=add_fig_path,
                                  fig_name='Gallifrey')
df = dp.correlated_power_plot_gs('Add', 
                                  'Add', 
                                  'D', 
                                  'D', 
                                  singleline_plot=True, 
                                  prescriptive_component2_tol=0.12,
                                  fig_path=add_fig_path,
                                  fig_name='Gallifrey')
# %%
color_test_bools = [False, True, False]
def color_test(x):
    if x:
        return 'b' 
    else:
        return 'r'
color_test = [color_test(i) for i in color_test_bools]
# %%
import math
math.sqrt(55*55*2)
# %%
c0=-0.015469
c1=0.984534
c2=-0.0001511
c3=-0.000018725

x=117.549220

import numpy as np
y = np.polyval([c0,c1,c2,c3], x)