# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 11:28:15 2022

@author: FionaLoftus
"""
# %%
import numpy as np
from matplotlib import pyplot as plt
from math import pi

u=1.     #x-position of the center
v=0.5    #y-position of the center
a=2.     #radius on the x-axis
b=1.5    #radius on the y-axis

t = np.linspace(0, 2*pi, 100)
plt.plot( u+a*np.cos(t) , v+b*np.sin(t) )
plt.grid(color='lightgray',linestyle='--')
plt.show()

# %%
import numpy as np
from matplotlib import pyplot as plt
from math import pi, cos, sin

u=1.       #x-position of the center
v=0.5      #y-position of the center
a=2.       #radius on the x-axis
b=1.5      #radius on the y-axis
t_rot=pi/4 #rotation angle

t = np.linspace(0, 2*pi, 100)
Ell = np.array([a*np.cos(t) , b*np.sin(t)])  
     #u,v removed to keep the same center location
R_rot = np.array([[cos(t_rot) , -sin(t_rot)],[sin(t_rot) , cos(t_rot)]])  
     #2-D rotation matrix

Ell_rot = np.zeros((2,Ell.shape[1]))
for i in range(Ell.shape[1]):
    Ell_rot[:,i] = np.dot(R_rot,Ell[:,i])

plt.plot( u+Ell[0,:] , v+Ell[1,:] )     #initial ellipse
plt.plot( u+Ell_rot[0,:] , v+Ell_rot[1,:],'darkorange' )    #rotated ellipse
plt.grid(color='lightgray',linestyle='--')

plt.show()

# %%
w1 = 108.60
w2 = 116.96
h1 = 114.89
h2 = 121.51
b_r = 141.50
d0 = -15.00

sp_m1 = h1/b_r
sp_m2 = h2/b_r

fact_1 = d0/1000.00*sp_m2
fact_2 = sp_m2-1
fact_3 = sp_m2*(sp_m1-1)/sp_m1

f = (d0/1000.00*sp_m2)/((sp_m2-1)-(sp_m2*(sp_m1-1))/sp_m1)

P = 1/f

# %%
# TEST MAG
import numpy as np
import matplotlib.pyplot as plt
d1 = 10 /1000
d1 = 20 /1000
d2 = 100 / 1000
P = 10
f = 1/P

m1_pos = np.linspace(1.01,10.0,100)
m2_pos = np.linspace(1.01,10.0,100)
# m2 = np.linspace(0.01,1.0,100)

# m1 = np.linspace(1.01,10.0,100)
# m2 = np.linspace(1.01,10.0,100)

y1 = d1*m1_pos/(m1_pos-1)
y2 = d2*m2_pos/(m2_pos-1)

plt.plot(m1_pos,y1)
plt.plot(m2_pos,y2,'r')
# plt.ylim([-1,0])
plt.ylim([0,1.0])

# %%
d1 = 100/1000
y1 = d1*m1/(m1-1)
plt.plot(m1,y1)
plt.ylim([0,1.0])

