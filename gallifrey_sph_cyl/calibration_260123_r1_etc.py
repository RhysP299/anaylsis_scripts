# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 14:07:00 2023

@author: FionaLoftus
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 14:09:31 2022

@author: FionaLoftus
"""

import pandas as pd
from src.statistical_tests import CalibrationData, CalibrationComparison
import matplotlib.pyplot as plt

def run_individual_error_plots(cali_data,
                               data_label,
                               figname,
                               figpath,
                               tol_lim=0.06):
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            cali_data.plot_calibration_error(side, 
                                          prescriptive_component,
                                          data_label, 
                                          fig_path=figpath, 
                                          fig_name=figname,
                                          tolerance_lim=tol_lim)
            
def run_error_comparison_plots(cali_comp,
                         figname,
                         figpath,
                         tol_lim=0.06):
    
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            cali_comp.plot_error_comparison(prescriptive_component,
                                     side, 
                                     tolerance_lim=tol_lim, 
                                     fig_name=figname,
                                     fig_path=figpath)
            
def run_stats_comparison_plots(cali_comp,
                               stats_type,
                               figname,
                               figpath):
    
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            cali_comp.plot_stats_comparison(prescriptive_component,
                                     side, 
                                     stats_type=stats_type, 
                                     fig_name=figname,
                                     fig_path=figpath)
            
# %%
# =============================================================================
# 26/01/23 NextGen Vs e2_Calibration_verification_noserial
# =============================================================================
path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey'
filename = 'e2_Calibration_verification_noserial.xlsx'
cali_dict = {'datasource_type': 'calibration',
             'cali_type': 'xlsx',
             'path': path,
             'filename': filename}
path = 'D:\\Gallifrey\\lenscal2601\\lenscal'
filename = 'data.ini'
cal_ini_dict = {'datasource_type': 'calibration',
             'cali_type': 'ini',
             'path': path,
             'filename': filename}
d_new = CalibrationData(**cal_ini_dict)

# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\Sph_cyl_axis_measurements\\calibration\\260123'
nextgenfig_name = '26-01-23'
oldfig_name = 'e2_Calibration_verification_noserial'

# 1. INDIVIDUAL ERROR PLOTS
# =============================================================================
run_individual_error_plots(d_new,
                           'NextGen',
                           nextgenfig_name,
                           fig_path)
# %%
data = d_new.data
comp = d_new.comparison_data
datadiff = d_new.datadiff
# %%
within_power_mean = d_new.get_within_power_mean(False)
within_power_mean_abs = d_new.get_within_power_mean(True)
within_power_range = d_new.get_within_power_range()
within_power_std = d_new.get_within_power_std()
# %%
fig_path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\Sph_cyl_axis_measurements\\calibration\\260123'
nextgenfig_name = '26-01-23'
for side in ['OculusDexter', 'OculusSinister']:
    d_new.r1_r2_error_plots(side, 'NextGen', 'r2', fig_name=nextgenfig_name, fig_path=fig_path)


# =============================================================================
# 6. STATS
# =============================================================================
# %%
# GLOBAL MEAN
# NEXTGEN
d_new.global_mean_error(absolute=True)
# OLD
d_old.global_mean_error(absolute=True)
# %%
# MEAN OF MEAN
# NEXTGEN
d_new.get_class_stats_mean('mean absolute')
# %%
# OLD
d_old.get_class_stats_mean('mean absolute')
# %%
# MEAN OF STD
# NEXTGEN
d_new.get_class_stats_mean('std')
# %%
# OLD
d_old.get_class_stats_mean('std')
# %%
# MEAN OF RANGE
# NEXTGEN
d_new.get_class_stats_mean('range')
# %%
# OLD
d_old.get_class_stats_mean('range')
# %%
d_old.get_within_power_std()
# %%
d_old.get_within_power_range()
