# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 12:26:32 2023

@author: FionaLoftus
"""
import pandas as pd
# =============================================================================
# Calibration lenses quick precision checks
# =============================================================================

def calculate_m1(p, d1):
    
    m1 = 1 / (1-d1*p)
    
    return m1

def calculate_r1(m1, br):
    
    r1 = m1*br
    
    return r1

def calculate_p(m1, m2, d1, d2):
    
    d0 = d2-d1
    
    f = (d0*m2)/((m2-1)-(m2*(m1-1))/m1)
    
    P = 1/f
    
    return P

def calculate_p_single(m1, d1):
    
    f = m1*d1/(1-m1)
    
    p = 1/f
    
    return p
    
    

EP_Sph = [-14.92191522, -9.91168489, -4.968529642, 4.813124483, 9.470230084]
EP_Cyl = 0;

baselineGridSize = 10
gridsizeOC= 4
baselinePixels = 176.798

baselineRadius = (baselinePixels/baselineGridSize)*gridsizeOC

maxPositionMMFromDisplay = 53
position0MMFromHome = 15
position0MMFromDisplay = maxPositionMMFromDisplay-position0MMFromHome
position0MFromDisplay = position0MMFromDisplay / 1000
position1MMFromHome = 0
position1MMFromDisplay = maxPositionMMFromDisplay-position1MMFromHome
position1MFromDisplay = position1MMFromDisplay / 1000

# %%
pixel = 1
for gridsizeOC in [4,5,6,7,8,9,10,11,12]:
    baselineRadius = (baselinePixels/baselineGridSize)*gridsizeOC
    m = pixel / baselineRadius
    p = calculate_p_single(m, position0MFromDisplay)
    print(p)
# %%
err = -0.12
m1 = calculate_m1(err, position0MFromDisplay)
for gridsizeOC in [4,5,6,7,8,9,10,11,12]:
    baselineRadius = (baselinePixels/baselineGridSize)*gridsizeOC
    r1 = calculate_r1(m1, baselineRadius)
    r = r1-baselineRadius
    print(r)

# %%
err = 0.12
m1 = calculate_m1(err, position0MFromDisplay)
r1 = calculate_r1(m1, baselineRadius)
r = r1-baselineRadius
# expected magnification at position0
# %%
df = pd.DataFrame(columns=['EPSph', 'EstMag1'])
df['EPSph'] = EP_Sph
df['EstMag1'] = df['EPSph'].apply(calculate_m1, d1=position0MFromDisplay)
df['EstMag2'] = df['EPSph'].apply(calculate_m1, d1=position1MFromDisplay)
df['EstR1POS0'] = df['EstMag1'].apply(calculate_r1, br=baselineRadius)
df['EstR1POS1'] = df['EstMag2'].apply(calculate_r1, br=baselineRadius)
df['EstR1Diff'] = df['EstR1POS0']- df['EstR1POS1']
df['EPSphCalc'] = df.apply(lambda row: \
        calculate_p(row.EstMag1, row.EstMag2, position0MFromDisplay, position1MFromDisplay), axis=1)
# %%
