# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 08:21:02 2023

@author: FionaLoftus
"""
import pandas as pd
from src.statistical_tests import CalibrationData, CalibrationComparison
from tools.data_elt import calculate_baseline_radius, calculate_r1
import matplotlib.pyplot as plt

def run_individual_error_plots(cali_data,
                               data_label,
                               figname,
                               figpath,
                               tol_lim=0.06):
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Sphere', 'Cylinder', 'Sph Equiv']:
            cali_data.plot_calibration_error(side, 
                                          prescriptive_component,
                                          data_label, 
                                          fig_path=figpath, 
                                          fig_name=figname,
                                          tolerance_lim=tol_lim)
            
def run_r1_r2_error_plots(cali_data,
                          data_label,
                          figname,
                          figpath
                          ):
    
    for side in ['OculusDexter', 'OculusSinister']:
        for prescriptive_component in ['Pos0r1', 'Pos1r1']:
            cali_data.plot_calibration_error(side, 
                                          prescriptive_component,
                                          data_label, 
                                          units='pixels',
                                          fig_path=figpath, 
                                          fig_name=figname,
                                          tolerance_lim=None)
# %%
# =============================================================================
#  19/12/22 NextGen26/01/23 Vs NextGen Vs e2_Calibration_verification_noserial
# =============================================================================
path = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey'
filename = 'e2_Calibration_verification_noserial.xlsx'
cali_dict = {'datasource_type': 'calibration',
             'cali_type': 'xlsx',
             'path': path,
             'filename': filename}
path = 'D:\\Gallifrey\\lenscalrepeats\\lenscal'
filename = 'data.ini'
cal_ini_dict = {'datasource_type': 'calibration',
             'cali_type': 'ini',
             'path': path,
             'filename': filename}
d_new = CalibrationData(**cal_ini_dict)

# %%
data = d_new.data
comp = d_new.comparison_data
datadiff = d_new.datadiff
# %%
d_new.calculate_r1_r2()
data = d_new.data
# %%
figpath = 'C:\\Users\\FionaLoftus\\OneDrive - EYOTO GROUP LTD\\emap\\Gallifrey\\Sph_cyl_axis_measurements\\calibration\\repeats'
figname = '26-01-2023'
run_individual_error_plots(d_new, 'NextGen', figname, figpath)
# %%
run_r1_r2_error_plots(d_new, 'NextGen', figname, figpath)
# %%
within_power_mean = d_new.get_within_power_mean(False)
within_power_mean_abs = d_new.get_within_power_mean(True)
within_power_range = d_new.get_within_power_range()
within_power_std = d_new.get_within_power_std()

# %%
data['OculusDexter']['baselineRadius'] = data['OculusDexter'].apply(lambda row: \
            calculate_baseline_radius(row.MPBaselineDiameter, row.MPBaseGridsize, row.MPOCGridsize), axis=1)
data['OculusSinister']['baselineRadius'] = data['OculusSinister'].apply(lambda row: \
            calculate_baseline_radius(row.MPBaselineDiameter, row.MPBaseGridsize, row.MPOCGridsize), axis=1)
# %%
data['OculusDexter']['MPPos0r1'] = data['OculusDexter'].apply(lambda row: \
                                                calculate_r1(row.MPPos0m1, row.baselineRadius), axis=1)
data['OculusSinister']['MPPos0r1'] = data['OculusSinister'].apply(lambda row: \
                                                calculate_r1(row.MPPos0m1, row.baselineRadius), axis=1)
data['OculusSinister']['MPPos0r2'] = data['OculusSinister'].apply(lambda row: \
                                                    calculate_r1(row.MPPos0m2, row.baselineRadius), axis=1)
data['OculusSinister']['MPPos0r2'] = data['OculusSinister'].apply(lambda row: \
                                                    calculate_r1(row.MPPos0m2, row.baselineRadius), axis=1)
# %%

error_power_df = d_new.get_error_power_df('OculusDexter', 'Pos0r1')
fig, ax = plt.subplots()
data = []
powers = []
for power in error_power_df['power'].unique():
    data.append(error_power_df[error_power_df['power']==power]['error'])
    powers.append(power)
    
bp = ax.boxplot(data[2])
# %%
fig, ax = plt.subplots()
error_power_df_pos0 = d_new.get_error_power_df('OculusDexter', 'Pos0r1')
error_power_df_pos1 = d_new.get_error_power_df('OculusDexter', 'Pos1r1')
ax.plot(error_power_df_pos0['power'], error_power_df_pos0['error'], '*', label='Pos0r1')
ax.plot(error_power_df_pos1['power'], error_power_df_pos1['error'], '*', label='Pos1r1')
ax.set_ylabel('R1 error (pixels)')
ax.set_xlabel('Lens Power (Sphere) (D)')
ax.legend()
# %%
fig, ax = plt.subplots()
error_power_df_pos0 = d_new.get_error_power_df('OculusSinister', 'Pos0r1')
error_power_df_pos1 = d_new.get_error_power_df('OculusSinister', 'Pos1r1')
ax.plot(error_power_df_pos0['power'], error_power_df_pos0['error'], '*', label='Pos0r1')
ax.plot(error_power_df_pos1['power'], error_power_df_pos1['error'], '*', label='Pos1r1')
ax.set_ylabel('R1 error (pixels)')
ax.set_xlabel('Lens Power (Sphere) (D)')
ax.legend()