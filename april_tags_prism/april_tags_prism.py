# -*- coding: utf-8 -*-
"""
Created on Sat Oct  1 12:57:20 2022
A test script to detect april tags and calculate the displacement from them.
@author: RhysPoolman
"""
import cv2
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# load images
path = "C:\\Users\\RhysPoolman\\analysis_scripts\\april_tags_prism\\data\\" + \
    "GoldStandard\\13285100\\GridSize4_StepperMovement5\\images\\"
image_name = "Camera1_AprilTag_0deg_x0_y0_p0.tiff"
image = cv2.imread(path + image_name)

# load tag locations
results_name = path + "results\\results.csv"
data = pd.read_csv(results_name)
