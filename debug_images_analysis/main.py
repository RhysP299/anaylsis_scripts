# -*- coding: utf-8 -*-
"""
Created on Thu May 19 10:46:00 2022

@author: RhysPoolman
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import imageio as iio
import pandas as pd
import tools.prescription as tlp

# load data
data_path = "E:\\anaylsis_scripts\\debug_images_analysis\\data\\"
data_folders = [data_path + data_folder
                for data_folder in os.listdir(data_path)]
data = {}
for folder in data_folders:
    if len(os.listdir(folder)) < 5:
        continue
    folder_name = folder.split("\\")[-1]
    data[folder_name] = {}
    data[folder_name]["csv"] = {}
    data[folder_name]["png"] = {}
    data[folder_name]["oma"] = {}
    for file in os.listdir(folder):
        ext = file.split(".")[1]
        if ext == "csv":
            data[folder_name]["csv"][file.split(".")[0]] = {}
            if "Final" in file and "Prism" not in file:
                with open(folder + "\\" + file) as f:
                    for ii in range(17):
                        header = f.readline()
                data[folder_name]["csv"][file.split(".")[0]]["header"] = header
                data[folder_name]["csv"][file.split(".")[0]]["data"] = \
                    pd.read_csv(folder + "\\" + file, skiprows=17)
            elif "Final" in file and "Prism" in file:
                with open(folder + "\\" + file) as f:
                    for ii in range(1):
                        header = f.readline()
                data[folder_name]["csv"][file.split(".")[0]]["header"] = header
                data[folder_name]["csv"][file.split(".")[0]]["data"] = \
                    pd.read_csv(folder + "\\" + file, skiprows=1,
                                delimiter=",")
            elif "Prescriptions.csv" == file:
                data[folder_name]["csv"][file.split(".")[0]] = \
                    tlp.Prescription(folder + "\\")
            else:
                with open(folder + "\\" + file) as f:
                    header = f.readline()
                data[folder_name]["csv"][file.split(".")[0]] = {}
                data[folder_name]["csv"][file.split(".")[0]]["header"] = header
                data[folder_name]["csv"][file.split(".")[0]]["data"] = \
                    pd.read_csv(folder + "\\" + file)
        elif ext == "png":
            data[folder_name]["png"][file.split(".")[0]] = \
                iio.imread(folder + "\\" + file)
        elif ext == "oma":
            continue

# plot csvs
for key, entry in data.items():
    for file, values in entry["csv"].items():
        if "Prescription" in file:
            continue
        print(values["data"].shape)
