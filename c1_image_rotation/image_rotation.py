# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 09:33:53 2022
Get meta data out of image files.
@author: RhysPoolman
"""
import cv2
import numpy as np
import matplotlib.pyplot as plt


def show_image(image, title):
    """
    A simple image plotting function.

    Parameters
    ----------
    image : 2d numpy.ndarray
        An 8-bit array to plotted as an image.
    title : string
        The string used in the window name.

    Returns
    -------
    None.

    """
    plt.figure(title)
    plt.clf()
    plt.imshow(image)


def rotate_image(image, angle):
    image_center = tuple(np.array(image.shape) / 2)
    rotation_matrix = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    rotated_image = cv2.warpAffine(image, rotation_matrix, image.shape[1::-1],
                                   flags=cv2.INTER_LINEAR)
    return rotated_image


path = "R:\\input_images\\cr1_inputs\\"
filename = "cr1_lens_markings_non_circ.png"
output_filename = "cr1_lens_markings_output_non_circ.png"

# load image
image = cv2.imread(path + filename, cv2.IMREAD_UNCHANGED)
# image = cv2.medianBlur(image, 1)
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# get alignment mark image
alignment_mark_filename = "test_alignment_mark.png"
alignment_mark = cv2.imread(path + alignment_mark_filename)

# create detector
min_hessian = 400
detector = cv2.xfeatures2d_SURF().create(hessianThreshold=min_hessian)

# detect features in alignment mark and measured image
key_points_mark, descriptors_mark = detector.detectAndCompute(alignment_mark,
                                                              None)
key_points_image, descriptors_image = detector.detectAndComputedetect(image,
                                                                      None)

# match alignment image descriptors with a brute force matcher
matcher = cv2.DescriptorMatcher_create(cv2.DescriptorMatcher_BRUTEFORCE)
matches = matcher.match(descriptors_mark, descriptors_image)

# detect alignment marks
camera_sensor_mm_to_pixels = 1.0/0.055
distance_between_circles = 25*camera_sensor_mm_to_pixels
upper_threshold_canny_edge = 100
center_detection_threshold = 20
min_radius = 0  # int(2.0*camera_sensor_mm_to_pixels)
max_radius = int(1.0*camera_sensor_mm_to_pixels)
circles = cv2.HoughCircles(image, cv2.HOUGH_GRADIENT, 1,
                           distance_between_circles,
                           param1=upper_threshold_canny_edge,
                           param2=center_detection_threshold,
                           minRadius=min_radius, maxRadius=max_radius)

if circles is not None:
    colour = (255, 0, 0)
    for circle in circles[0]:
        # draw circumference
        cv2.circle(image, (int(circle[0]), int(circle[1])), int(circle[2]),
                   colour, 1)
        # draw centre
        cv2.circle(image, (int(circle[0]), int(circle[1])), 2, colour, 2)

# plot image
show_image(image, "Scratch Image")

# calculate image rotation angle
alignment_point_1 = circles[0, 0, :2]
alignment_point_2 = circles[0, 1, :2]
horizontal_side_triangle = np.abs(alignment_point_1[0] - alignment_point_2[0])
vertical_side_triangle = np.abs(alignment_point_1[1] - alignment_point_2[1])
image_rotaion_angle = np.arctan2(vertical_side_triangle,
                                 horizontal_side_triangle)  # radians

# rotate image
rotated_image = rotate_image(image, np.rad2deg(image_rotaion_angle))

# plot image
show_image(rotated_image, "Rotated Image")

# save output image
cv2.imwrite(path + output_filename, rotated_image)
