# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 09:33:53 2022
Get meta data out of image files.
@author: RhysPoolman
"""
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os


def show_image(image, title, centroid=None, keypoints=None):
    """
    A simple image plotting function.

    Parameters
    ----------
    image : 2d numpy.ndarray
        An 8-bit array to plotted as an image.
    title : string
        The string used in the window name.
    centroid: 2 elment array-like of integers
        The centroid of the image plotted as a red cross, defaults to None.
    keypoints: list of cv2.KeyPoints
        Points of interest to be marked on the plot. Defaualts to none

    Returns
    -------
    None.

    """
    plt.figure(title)
    plt.clf()
    plt.imshow(image)
    if centroid is not None:
        plt.scatter([centroid[0]], [centroid[1]], [10], c="r")
    if keypoints is not None:
        for keypoint in keypoints:
            plt.scatter([keypoint.pt[0]], [keypoint.pt[1]], [10], c="k")


def show_image_with_lines(image, lines, title):
    """
    A simple image plotting function.

    Parameters
    ----------
    image : 2d numpy.ndarray
        An 8-bit array to plotted as an image.
    lines : Draw
    title : string
        The string used in the window name.

    Returns
    -------
    None.

    """
    plt.figure(title)
    plt.clf()
    plt.imshow(image)
    plt.imshow(lines)


def rotate_image(image, angle, centroid):
    """
    Rotates the  image using an affine transformation through an angle about a
    centroid

    Parameters
    ----------
    image : numpy.ndarray of ints
        The image to be rotated.
    angle : float
        The angle to rotae the image.
    centroid : two floats
        The point about which the rotation occurs.

    Returns
    -------
    rotated_image : numpy.ndarray of ints
        The rotated image.
    """
    image_center = centroid
    rotation_matrix = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    rotated_image = cv2.warpAffine(image, rotation_matrix, image.shape[1::-1],
                                   flags=cv2.INTER_LINEAR)
    return rotated_image


def _convert_lms_values(string_value):
    """
    Tests if string is float, int or alpha-numerical and converts to
    appropriate type.  Does nothing is string_value is alpha-numeric.

    Parameters
    ----------
    string_value : string
        String of data to be converted.

    Returns
    -------
    value : float, int or string
        Convert value.
    """
    convertable_string_value = string_value.strip()  # removes whitespace
    value = None
    if convertable_string_value.isdigit():
        value = int(convertable_string_value)
    elif convertable_string_value.count(".") < 2 and \
            convertable_string_value.replace(".", "", 1).isdigit():
        value = float(convertable_string_value)
    return value


def read_lms(filename):
    """
    Reads the contenst of an lms file and converts it into a dictionary.

    Parameters
    ----------
    filename : string
        The lms filename.

    Returns
    -------
    lms : dict
        Dictionary containing data.
    """
    lms = {}
    with open(filename) as lms_file:
        lines = lms_file.readlines()
        for line in lines:
            key, values = line.split("=")
            value_array = \
                np.array([_convert_lms_values(value)
                          for value in values.split(";")]) \
                if ";" in values else _convert_lms_values(values)
            lms[key] = value_array
    return lms


def convert_camera_image_pixels_to_mm(pixel_length, camera_sensor_mm_to_pixels,
                                      camera_lens_magnification):
    """
    Converts lengths in image pixels to mm.  NOTE:  The
    camera_sensor_mm_to_pixels is a divisor, which is odd.  However, this is
    how its done in the C# code and I want to maintain similar logic.  See
    EyeTech.Core.Primitives.Maths.RealWorldMetrics.ConvertCameraImagePixelsToMM
    for function and hard coded values CameraSensorMMToPixels and
    CameraLensMagnification in
    EyeTech.IndustrialLensmeter.Core.Systems.Single.SingleLensmeterSystemSettings
    and Eyetech.recipes.lensmeter.unittests.LMSUtilities.Parsers.LensWarePrescriptionParserTest.
    Hard coded values are bloody stupid so we need to sort that out at some
    point.

    Parameters
    ----------
    pixel_length : float
        The length in pixels to be converted to mm.
    camera_sensor_mm_to_pixels : float
        The divisor that is used to convert mm to pixels, it is used as a
        factor here.
    lens_magnification : float
        The camera lens magnification..

    Returns
    -------
    float
        The pixel length value convert ot a mm length value.

    """
    return pixel_length*camera_sensor_mm_to_pixels/camera_lens_magnification


camera_sensor_mm_to_pixels = 0.055  # C1 value
camera_lens_magnification = 0.133  # C1 value

jobids = ["137333" + jobid for jobid in ["02", "04", "10", "82", "99"]]
lms_path = "E:\\eMap\\C1 Rotation\\lms_files\\"
lms_filenames = os.listdir(lms_path)

for jobid in jobids:
    # rotate image
    for side in ["left", "right"]:
        job = jobid + "_" + side
        path = "R:\\input_images\\stripes_" + job + "\\"
        output_path = "E:\\eMap\\C1 Rotation\\"
        filename_1 = "stripes_1.png"
        filename_2 = "stripes_2.png"
        filename_3 = "stripes_3.png"
        filename_4 = "stripes_4.png"
        mask_filename = "mask.png"
        output_filename = "stripes_output" + job + ".png"
        input_filename = "stripes_input" + job + ".png"

        # load image
        image_1 = cv2.imread(path + filename_1, cv2.IMREAD_UNCHANGED)
        image_2 = cv2.imread(path + filename_2, cv2.IMREAD_UNCHANGED)
        image_3 = cv2.imread(path + filename_3, cv2.IMREAD_UNCHANGED)
        image_4 = cv2.imread(path + filename_4, cv2.IMREAD_UNCHANGED)
        mask = cv2.imread(path + mask_filename, cv2.IMREAD_UNCHANGED)
        ret, mask = cv2.threshold(mask, np.max(mask)/2, 1, cv2.THRESH_BINARY)

        # input pattern
        cv2.imwrite(path + input_filename, image_1)

        # mask images
        masked_image_1 = mask*image_1
        edges_1 = cv2.Canny(masked_image_1, 100, 200)
        # show_image(edges_1, "Edges 1")
        masked_image_2 = mask*image_2
        edges_2 = cv2.Canny(masked_image_2, 100, 200)
        # show_image(edges_2, "Edges 2")
        masked_image_3 = mask*image_3
        edges_3 = cv2.Canny(masked_image_3, 100, 200)
        # show_image(edges_3, "Edges 3")
        masked_image_4 = mask*image_4
        edges_4 = cv2.Canny(masked_image_4, 100, 200)
        # show_image(edges_4, "Edges 4")
        edges = edges_1 + edges_2 + edges_3 + edges_4
        # show_image(edges, "Edges")
        # show_image(mask, "Mask Image")

        # find high gradient areas
        sobely_unnormed = cv2.Sobel(edges, cv2.CV_64F, 0, 1, ksize=5, scale=1,
                                    delta=0, borderType=cv2.BORDER_DEFAULT)
        # show_image(sobely_unnormed, "Sobel Y Unnormed")
        sobely = np.abs(sobely_unnormed)
        sobely = np.uint8(sobely/np.max(sobely)*np.iinfo(np.uint8).max)
        # show_image(sobely, "Sobel Y")

        # mask sobel output
        scale = 0.9  # % of orignal size
        width = int(mask.shape[1]*scale)
        height = int(mask.shape[0]*scale)
        new_size = (width, height)
        scaled_mask = cv2.resize(mask, new_size, interpolation=cv2.INTER_AREA)
        diff = image_1.shape[1] - width
        top = int(diff/2)
        diff += 0 if diff % 2 == 0 else 1
        bottom = int(diff/2)
        diff = image_1.shape[0] - width
        left = int(diff/2)
        diff += 0 if diff % 2 == 0 else 1
        right = int(diff/2)
        scaled_mask = cv2.copyMakeBorder(scaled_mask, top, bottom, left, right,
                                         cv2.BORDER_CONSTANT, value=0)
        # show_image(scaled_mask, "Scaled Mask")
        masked_sobely = sobely*scaled_mask
        # show_image(masked_sobely, "Masked Sobel Y")

        # median bluring
        kernal_size = 15
        sobely_cleaned_up = cv2.medianBlur(masked_sobely, kernal_size)
        # show_image(sobely_cleaned_up, "Sobel Y Cleaned Up")

        # estimate center point
        # mask_inverted = np.abs(np.max(mask) - mask)
        # lens_edge = edges*mask_inverted
        moments = cv2.moments(mask)
        centroid = (moments["m10"]/moments["m00"],
                    moments["m01"]/moments["m00"])

        # detect blobs
        params = cv2.SimpleBlobDetector_Params()
        # threshold
        params.minThreshold = 0
        params.maxThreshold = np.max(sobely_cleaned_up)
        # filter by area
        params.filterByArea = True
        params.minArea = 0
        params.maxArea = 200
        # filter by circularity
        params.filterByCircularity = False
        # filter by convexity
        params.filterByConvexity = False
        # filter by inertia
        params.filterByInertia = False
        # filter by colour
        params.filterByColor = False
        detector = cv2.SimpleBlobDetector_create(params)
        keypoints = detector.detect(sobely_cleaned_up)
        # show_image(sobely_cleaned_up, "Lens Center {0:s}".format(job),
        #            centroid, keypoints)

        # select alignment marks by calculating distance between lines
        alignment_keypoints = []
        number_of_keypoints = len(keypoints)
        if number_of_keypoints < 3:
            continue
        distances = []
        lower_limit_pixels = 30/camera_sensor_mm_to_pixels
        upper_limit_pixels = 50/camera_sensor_mm_to_pixels
        for ii in range(number_of_keypoints):
            for jj in range(number_of_keypoints):
                if ii == jj:
                    continue
                # https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
                x0 = centroid[0]
                y0 = centroid[1]
                x1 = keypoints[ii].pt[0]
                y1 = keypoints[ii].pt[1]
                x2 = keypoints[jj].pt[0]
                y2 = keypoints[jj].pt[1]
                # length of line
                pixel_length = np.sqrt((x2 - x1)**2 + (y2 - y1)**2)
                mm_length = \
                    convert_camera_image_pixels_to_mm(pixel_length,
                                                      camera_sensor_mm_to_pixels,
                                                      camera_lens_magnification)
                if pixel_length < lower_limit_pixels or \
                        pixel_length > upper_limit_pixels:
                    continue
                alignment_keypoints.append(keypoints[ii])
                distance = np.zeros(6)
                distance[0] = x1
                distance[1] = y1
                distance[2] = x2
                distance[3] = y2
                # distance between centroid and line
                distance[4] = np.abs((x2 - x1)*(y1 - y0) - (x2 - x0)*(y2 - y1)) /\
                    np.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1))
                distance[5] = mm_length
                distances.append(distance)
        distances = np.array(distances)
        if len(distances) < 1:
            continue
        alignment_index = np.argmin(distances[:, 4])
        alignment_point_1 = distances[alignment_index, 0:2]
        alignment_point_2 = distances[alignment_index, 2:4]

        # calculate image rotation angle
        horizontal_side_triangle = \
            np.abs(alignment_point_1[0] - alignment_point_2[0])
        vertical_side_triangle = np.abs(alignment_point_1[1] -
                                        alignment_point_2[1])
        image_rotaion_angle = np.arctan2(vertical_side_triangle,
                                         horizontal_side_triangle)  # radians

        # rotate image
        rotated_image = rotate_image(sobely_cleaned_up,
                                     np.rad2deg(image_rotaion_angle),
                                     centroid)

        # plot image
        alignment_points = \
            [cv2.KeyPoint(alignment_point_1[0], alignment_point_1[1], 1),
             cv2.KeyPoint(alignment_point_2[0], alignment_point_2[1], 1)]
        show_image(image_1, "Unrotated Image {0:s}".format(job), centroid,
                   alignment_points)
        #show_image(image_1, "Rotated Image {0:s}".format(job))

        # print rotation angle
        print(job)
        print(len(keypoints))
        print(np.rad2deg(image_rotaion_angle))

        # save output image
        cv2.imwrite(output_path + output_filename, rotated_image)
        print()
