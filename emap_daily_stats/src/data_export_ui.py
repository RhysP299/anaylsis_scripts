from importlib.resources import path
import sys

from PyQt5.QtCore import Qt, QDate
from PyQt5.QtWidgets import (
    QApplication,
    QCheckBox,
    QComboBox,
    QDateEdit,
    QDateTimeEdit,
    QDial,
    QDialog,
    QDialogButtonBox,
    QDoubleSpinBox,
    QFileDialog,
    QFormLayout,
    QFontComboBox,
    QLabel,
    QLCDNumber,
    QLineEdit,
    QMainWindow,
    QMessageBox,
    QProgressBar,
    QPushButton,
    QRadioButton,
    QSlider,
    QSpinBox,
    QTimeEdit,
    QVBoxLayout,
    QWidget,
)

from raw_export import get_raw_export

class DataExportUi(QMainWindow):
    """Dialog."""
    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)

        self.setWindowTitle('Data Export')
        self.setFixedSize(235, 235)
        
        self.generalLayout = QVBoxLayout()
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)

        todays_date = QDate().currentDate()
        one_week_ago = todays_date.addDays(-7)
        formLayout = QFormLayout()
        self.client_dropdown = QComboBox()
        self.client_dropdown.addItems(["Visionworks", "Robertsons", "Eye Kraft"])
        self.start_date_edit = QDateEdit()
        self.start_date_edit.setDate(one_week_ago)

        self.end_date_edit = QDateEdit()
        self.end_date_edit.setDate(todays_date)

        formLayout.addRow('Client:', self.client_dropdown)
        formLayout.addRow('Start Date:', self.start_date_edit)
        formLayout.addRow('End Date:', self.end_date_edit)
        self.generalLayout.addLayout(formLayout)
        self.btns = QDialogButtonBox()
        self.btns.setStandardButtons(
            QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        self.generalLayout.addWidget(self.btns)

        self.btns.accepted.connect(self.on_submit)
        self.btns.rejected.connect(self.on_reject)

    def on_submit(self):

        num_days = self.start_date_edit.date().daysTo(self.end_date_edit.date())

        if num_days < 0:
            self.invalid_date_popup()
        elif num_days > 7:
            self.large_date_file_warning(num_days)
        else:
            self.select_file_path('OK')

    def on_reject(self):

        self.close()

    def invalid_date_popup(self):

        msg = QMessageBox()
        msg.setText('Start date must be before end date')
        x = msg.exec_()

    def large_date_file_warning(self, num_days):

        txt = f'Exporting {num_days} days of data.  Do you want to proceed?'
        msg = QMessageBox()
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setText(txt)

        msg.buttonClicked.connect(self.select_file_path)

        retval = msg.exec_()

    def data_download_complete(self):

        txt = 'Data Exported'
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle(" ")
        msg.setText(txt)
        x = msg.exec_()

    def submit_db_request(self):

        client = self.client_dropdown.currentText()
        start_date = self.start_date_edit.date().toString("yyyy.MM.dd")
        end_date = self.end_date_edit.date().toString("yyyy.MM.dd")
        path = self.dir.selectedFiles()[0]

        db_request(client, start_date, end_date, path)

        self.data_download_complete()

    def select_file_path(self, i):

        if isinstance(i, str) or i.text() == 'OK':
            self.dir = QFileDialog()
            self.dir.setFileMode(QFileDialog.DirectoryOnly)
            self.dir.setAcceptMode(QFileDialog.AcceptOpen)
            if self.dir.exec_() == QDialog.Accepted:
                path = self.dir.selectedFiles()[0]  # returns a list
                self.submit_db_request()

class DataExportCtrl:
    """DataExport Controller class."""
    def __init__(self, view):
        """Controller initializer."""
        self._view = view
        # Connect signals and slots
        self._connectSignals()

def db_request(client, start_date, end_date, path):

    get_raw_export(client, start_date, end_date, path)


def main():
    app = QApplication(sys.argv)
    dlg = DataExportUi()
    dlg.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()