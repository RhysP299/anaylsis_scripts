"""
Scripts to run daily stats
"""

import openpyxl
import pandas as pd 
import Orange
import numpy as np
import pymssql
import pyodbc
import os
import yaml
import math
from datetime import date, timedelta
from typing import Iterable
import csv
import click
from utils import get_project_root
from openpyxl import load_workbook
from openpyxl.styles.borders import Border, Side
from copy import copy
import time
from data_elt import flatten, sql_query_to_df, db_to_portal_labels, lens_pairs_df, get_average_measurements_per_hour

path = get_project_root()
filename_stem = f'daily_numbers'
lens_type_dict = {
    'PAL': ['Progressive'],
    'SV': ['Single'],
    'Both': ['Progressive', 'Single']
}
path_to_save = r'C:\Users\FionaLoftus\OneDrive - EYOTO GROUP LTD\emap\emap_daily_numbers\data'
config_name = 'config.yml'
db_portal_dict_yml = 'db_to_portal_dict.yml'
path_to_times_csv = r'C:\Users\FionaLoftus\OneDrive - EYOTO GROUP LTD\emap\emap_db_times'
times_csv = 'data_pull_times.csv'

@click.command()
@click.argument('client', nargs=1)
@click.argument('todays_date', nargs=1)
@click.option('--account', default=None, help='Client Account Name')
@click.option('--pairs', default=True, help='Lens pairs (True), individual lenses (False)')
@click.option('--write_to_file', default=True, help='Save file')
@click.option('--record_time', default=True, help='Record query duration')
def run_daily_update(client, todays_date, account=None, pairs=True, write_to_file=False, record_time=False):

    with open(os.path.join(path, config_name)) as stream:
        config = yaml.safe_load(stream)

    with open(os.path.join(path, db_portal_dict_yml), 'r') as stream:
        db_portal_dict = yaml.safe_load(stream)

    if account is None:
        accountids = [v for v in db_portal_dict[client].values()]
    else:
        accountids = [db_portal_dict[client][account]]

    measurement_date = pd.to_datetime(todays_date) - timedelta(days=1)
    end_date = pd.to_datetime(todays_date)
    record_time_to = None
    if record_time:
        record_time_to = {'path': path_to_times_csv, 'filename': times_csv}
    df = sql_query_to_df(config, measurement_date, end_date, accountids, record_time_to)

    if len(df) == 0:
        if account is not None:
            print(f'No {client} {account} data for {measurement_date}')
        else:
            print(f'No {client} data for {measurement_date}')
        return

    df = db_to_portal_labels(df, db_portal_dict_yml, path)

    if write_to_file:

        filename = f'{client}_{filename_stem}.xlsx'
        file_path = os.path.join(path_to_save, filename)

        wb = load_workbook(os.path.join(filename, file_path))

        if account is None:
            ws = wb['All Accounts']
        else:
            ws = wb[account]

        for k, v in lens_type_dict.items():
            new_data = daily_update(df, measurement_date, {k:v}, pairs)

            ws.append(list(new_data.values()))
            
            # excel row with new data reference for formatting
            new_row = ws.max_row
    
            # for each cell in row, copy style from previous date's entry
            row_for_style = ws.iter_rows(min_row=new_row-(len(lens_type_dict)), max_row=new_row-(len(lens_type_dict)))
            
            for row in row_for_style:
                for cell in row:
                    new_cell = ws.cell(row=new_row, column=cell.col_idx)
                    if cell.col_idx in config['excel_configs']['right_border_cols']:
                        if k == 'Both':
                            new_cell.border = Border(top=Side(style='medium'),
                                                    bottom=Side(style='medium'),
                                                    right=Side(style='medium'))
                        elif k == 'PAL':
                            new_cell.border = Border( 
                                            top=Side(style='medium'),
                                            right=Side(style='medium'))
                        elif k == 'SV':
                            new_cell.border = Border( 
                                            bottom=Side(style='medium'),
                                            right=Side(style='medium'))
                    else:
                        if k == 'Both':
                            new_cell.border = Border(top=Side(style='medium'),
                                                    bottom=Side(style='medium'))
                        elif k == 'PAL':
                            new_cell.border = Border(top=Side(style='medium'))

                    if cell.has_style:
                        new_cell.font = copy(cell.font)
                        new_cell.fill = copy(cell.fill)
                        new_cell.number_format = copy(cell.number_format)
                        new_cell.protection = copy(cell.protection)
                        new_cell.alignment = copy(cell.alignment)
                    
        wb.save(os.path.join(filename, file_path))
    return df

def daily_update(df, date, lens_type, pairs=True, filename=None):
    """ 
    
    Inputs:
    df: (Pandas DataFrame) 
    date: (str) 
    lens_type: list(str) 
    pairs: (bool)
    filename: (str)

    Returns: 
    result_dict: (dict)
    """

    status_list = ['OverallStatus', 'PrescriptionStatus', 'SurfaceInspectionStatus', 'SphereStatus', 'CylinderStatus', 'AxisStatus', 'AddStatus', 'CombinedPrismResult']

    manual_autoList = ['OverallStatus', 'PrescriptionStatus', 'SurfaceInspectionStatus']

    status_type_list = ['OverallStatusType', 'PrescriptionStatusType', 'SurfaceInspectionStatusType']

    cols = ['LensType']
    if pairs:
        df = lens_pairs_df(df, False, cols, True, status_list, status_type_list)

    df = df[df['LensType'].isin(list(lens_type.values())[0])]

    df_day = df[(pd.to_datetime(df['MeasurementDate']) >= pd.to_datetime(date)) & (pd.to_datetime(df['MeasurementDate']) < pd.to_datetime(date) + timedelta(days=1))]

    results_dict = {}
    results_dict['Date'] = date
    results_dict['Lens Type'] = list(lens_type.keys())[0]
    if pairs:
        results_dict['Ind Pairs'] = 'Pair'
    else:
        results_dict['Ind Pairs'] = 'Ind'

    for s in status_list:

        if len(df_day) == 0:
            results_dict[s] = np.NaN
        else:
            if (s == 'AddStatus') and (list(lens_type.keys())[0] == 'SV' or list(lens_type.keys())[0] == 'Both'):
                results_dict[s] = None
            else:
                results_dict[s] = len(df_day[df_day[s]=='Pass']) / len(df_day[s]) * 100
        if s in manual_autoList:
            if len(df_day) == 0:
                results_dict[f'{s} Automatic'] = np.NaN
                results_dict[f'{s} Manual'] = np.NaN
            else:
                results_dict[f'{s} Automatic'] = len(df_day[(df_day[s]=='Pass') & (df_day[f'{s}Type']=='Automatic')]) / len(df_day[s]) * 100
                results_dict[f'{s} Manual'] = len(df_day[(df_day[s]=='Pass') & (df_day[f'{s}Type']=='Manual')]) / len(df_day[s]) * 100
    
    if len(df_day) == 0:
        results_dict['Prism Failed to Measure'] = np.NaN
    else:
        prism_nan = len(df_day[df_day['CombinedPrismResult'].isnull()]) + len(df_day[df_day['CombinedPrismResult']=='NaN'])
        results_dict['Prism Failed to Measure'] = prism_nan / len(df_day) * 100

    results_dict['Studies Count'] = len(df_day)

    results_dict['Average Jobs per Hour'] = get_average_measurements_per_hour(df_day)

    return results_dict

if __name__ == '__main__':
    run_daily_update()