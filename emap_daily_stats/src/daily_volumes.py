"""
Scripts to run daily jobs volume and plots
"""

from numpy.lib.arraysetops import isin
import openpyxl
import pandas as pd
import numpy as np
import os
import sys
import yaml
import math
from datetime import date, timedelta
from typing import Iterable
import csv
import click
from src.utils import get_project_root
from openpyxl import load_workbook
from openpyxl.styles.borders import Border, Side
from copy import copy
import time
from src.data_elt import flatten, sql_query_to_df, db_to_portal_labels
from src.data_elt import lens_pairs_df, get_average_measurements_per_hour
from src.data_elt import sql_query_to_df_ma_all_accounts, sql_query_to_df_all_accounts
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, MO
import matplotlib.dates as mdates

path = get_project_root()
filename = 'Daily_emap_numbers_US_UK.xlsx'
# filename = 'Daily_emap_numbers_US.xlsx'
lens_type_dict = {
    'PAL': ['Progressive'],
    'SV': ['Single'],
    'Both': ['Progressive', 'Single']
}

config_name = 'config_daily_volume.yml'
config_uk_name = 'config_daily_volume_uk.yml'
db_portal_dict_yml = 'db_to_portal_dict.yml'
path_to_times_csv = r'C:\Users\FionaLoftus\OneDrive - EYOTO GROUP LTD\emap\emap_db_times'
times_csv = 'data_pull_times.csv'

client_color_dict = {
    'Combined': '#E7D6BC',
    'Visionworks Total': '#E69BA3',
    'Robertsons Total': '#FBCA99',
    'Eye Kraft Total': '#88C2E6',
    'Optica Lopez SA Total': '#BFAFCF',
    'R&D Optical Total': '#B3D6AB',
    'Pacific Artisan Labs Total': '#CBCBCB'
}
client_plot_start_date_dict = {
    'Combined': '2021-09-01',
    'Visionworks Total': '2021-09-01',
    'Robertsons Total': '2021-09-01',
    'Eye Kraft Total': '2021-09-01',
    'Optica Lopez SA Total': '2022-03-14',
    'R&D Optical Total': '2022-05-16',
    'Pacific Artisan Labs Total': '2022-05-16'
}


days_in_week = 7

# @click.command()
# @click.argument('todays_date', nargs=1)
# @click.option('--record_time', default=True, help='Record query duration')
def run_script_daily(todays_date, days=1, record_time=True, add_uk=True):
    """ 
    Pulls previous day's data from database for all clients, and updates job volumes xlsx file

    Inputs:
    todays_date: (str) 
    record_time: (bool) Record query duration

    Returns: void
    """
    start_date = pd.to_datetime(todays_date) - timedelta(days=int(days))
    end_date = pd.to_datetime(todays_date)

    df, config = sql_to_df(start_date, end_date, record_time=record_time, pairs=True)
    config_uk=None
    if add_uk:
        df_uk, config_uk = sql_to_df_uk(start_date, end_date, record_time=record_time, pairs=True)
        df = pd.concat([df,df_uk],ignore_index=True,sort=False)

    append_to_xlsx(df, config, start_date, end_date, formatting=True, config_uk=config_uk)

def run_script_historical(start_date, 
                          end_date, 
                          record_time=False, 
                          formatting=True, 
                          add_uk=True):
    """ 
    Pulls historical data from database for all clients, and updates job volumes xlsx file

    Inputs:
    start_date: (str) First date in period (inclusive)
    end_date: (str) Last date in period (exclusive)
    record_time: (bool) Record query duration
    formatting: (bool) Format xlsx file using existing formatting

    Returns: void

    """
    start_date = pd.to_datetime(start_date)
    end_date = pd.to_datetime(end_date)

    df, config = sql_to_df(start_date, end_date, record_time=record_time, pairs=True)
    config_uk = None
    
    if add_uk:
        df_uk, config_uk = sql_to_df_uk(start_date, end_date, record_time=record_time, pairs=True)
        df = pd.concat([df,df_uk],ignore_index=True,sort=False)

    append_to_xlsx(df, config, start_date, end_date, formatting=True, config_uk=config_uk)

def run_plots(end_date):
    """ 
    Runs line plots for jobs volume for all clients from specified date period (uses saved xlsx file for data)

    Inputs:
    start_date: (str) First date in period (inclusive)
    end_date: (str) Last date in period (exclusive)

    Returns: void

    """
    for k,v in client_color_dict.items():
        start_date = client_plot_start_date_dict[k]

        plot_daily_job_count(k, v, start_date, end_date)

def sql_to_df(start_date, end_date, record_time=False, pairs=True):
    """ 
    Pulls data from database for given period and puts in pandas dataframe

    Inputs:
    start_date: (str) First date in period (inclusive)
    end_date: (str) Last date in period (exclusive)
    record_time: (bool) Record query duration
    pairs: (bool) Combine jobs to lens pairs

    Returns:
    df: (DataFrame) 1 row per job pair
    config: (dict) config dictionary

    """

    with open(os.path.join(path, config_name)) as stream:
        config = yaml.safe_load(stream)

    with open(os.path.join(path, db_portal_dict_yml), 'r') as stream:
        db_portal_dict = yaml.safe_load(stream)

    record_time_to = None
    if record_time:
        record_time_to = {'path': path_to_times_csv, 'filename': times_csv}
    df = sql_query_to_df_all_accounts(config, start_date, end_date, record_time_to)

    df = db_to_portal_labels(df, db_portal_dict_yml, path)

    if pairs:
        cols = ['LensType', 'AccountId']
        df = lens_pairs_df(df, False, cols=cols)

    return df, config

def sql_to_df_uk(start_date, end_date, record_time=False, pairs=True):
    """ 
    Pulls data from database for given period and puts in pandas dataframe

    Inputs:
    start_date: (str) First date in period (inclusive)
    end_date: (str) Last date in period (exclusive)
    record_time: (bool) Record query duration
    pairs: (bool) Combine jobs to lens pairs

    Returns:
    df: (DataFrame) 1 row per job pair
    config: (dict) config dictionary

    """

    with open(os.path.join(path, config_uk_name)) as stream:
        config = yaml.safe_load(stream)

    with open(os.path.join(path, db_portal_dict_yml), 'r') as stream:
        db_portal_dict = yaml.safe_load(stream)

    record_time_to = None
    if record_time:
        record_time_to = {'path': path_to_times_csv, 'filename': times_csv}
    df = sql_query_to_df_ma_all_accounts(config, start_date, end_date)

    df = db_to_portal_labels(df, db_portal_dict_yml, path)

    if pairs:
        cols = ['LensType', 'AccountId']
        df = lens_pairs_df(df, False, cols=cols)

    return df, config


def job_volume_by_day(df, config, measurement_date, lens_type, config_uk=None):
    """ 
    Collates measurements into total jobs performed on specified measurement day, for a given lens type 

    Inputs:
    df: (DataFrame) Data exported from portal to pandas df 
    config: (dict) config dict used for list of clients and accounts
    measurement_date: (Timestamp) date of jobs to collate
    lens_type: (dict) lens type name: list(lens types under lens name)

    Returns:
    results_dict: (dict)
    {
        Date: date of measurements
        Lens Type: name of lens types included
        {
            Client: {
                Account: Total jobs per client and account
                Total: Total jobs per client
            }
    }
    """
    df_day = df[(pd.to_datetime(df['MeasurementDate']) >= pd.to_datetime(measurement_date)) & (pd.to_datetime(df['MeasurementDate']) < pd.to_datetime(measurement_date) + timedelta(days=1))]

    df_day = df_day[df_day['LensType'].isin(list(lens_type.values())[0])]

    results_dict = {}
    results_dict['Date'] = measurement_date.date()
    results_dict['Lens Type'] = list(lens_type.keys())[0]
    clients = config['clients']
    accounts = config['accounts']
    if config_uk is not None:
        clients = clients + config_uk['clients']
        accounts = {**accounts, **config_uk['accounts']}
    all_accounts = []
    for client in clients:
        client_accountids = []
        if isinstance(accounts[client],dict):
            for k, v in accounts[client].items():
                if isinstance(v, list):
                    accounts_ids = v
                else:
                    accounts_ids = [v]
                client_accountids.append(accounts_ids)
                results_dict[f'{client}_{k}'] = len(df_day[df_day['AccountId'].isin(accounts_ids)])
            results_dict[f'{client}_Total'] = len(df_day[df_day['AccountId'].isin([x for x in flatten(client_accountids)])])
            all_accounts.append(client_accountids)
        else:
            accounts_ids = accounts[client]
            results_dict[f'{client}_Total'] = len(df_day[df_day['AccountId'].isin(accounts_ids)])
            all_accounts.append(accounts_ids)
    results_dict['Total'] = len(df_day[df_day['AccountId'].isin([x for x in flatten(all_accounts)])])

    return results_dict

def concat_dfs(df1, df2):
    """ 
    Concatenates 2 dataframes based on 

    Inputs:
    df1: (Dataframe) Data exported from portal to pandas df 
    df2: (Dataframe) Data exported from portal to pandas df 
    start_date: (Timestamp) First date in period to append (inclusive)
    end_date: (Timestamp) End date in period to append (exclusive)
    formatting: (bool) Format new xlsx rows using existing formatting

    Returns:
    results_dict: (dict)
    {
        Date: date of measurements
        Lens Type: name of lens types included
        {
            Client: {
                Account: Total jobs per client and account
                Total: Total jobs per client
            }
    }
    """

def append_to_xlsx(df, config, start_date, end_date, formatting=True, config_uk=None):
    """ 
    Appends jobs volume to xlsx file for given date period

    Inputs:
    df: (Dataframe) Data exported from portal to pandas df 
    config: (dict) config dict used for list of clients and accounts
    start_date: (Timestamp) First date in period to append (inclusive)
    end_date: (Timestamp) End date in period to append (exclusive)
    formatting: (bool) Format new xlsx rows using existing formatting

    Returns:
    results_dict: (dict)
    {
        Date: date of measurements
        Lens Type: name of lens types included
        {
            Client: {
                Account: Total jobs per client and account
                Total: Total jobs per client
            }
    }
    """
    num_days = (pd.to_datetime(end_date) - pd.to_datetime(start_date)).days
    path_to_save = config['path_to_save']

    file_path = os.path.join(f'{path_to_save}', filename)

    wb = load_workbook(file_path)

    ws = wb.active

    for day in np.arange(0, num_days):
        measurement_date = pd.to_datetime(start_date) + timedelta(days=int(day))

        for k, v in lens_type_dict.items():
            results_dict = job_volume_by_day(df, config, measurement_date, {k:v}, config_uk)

            ws.append(list(results_dict.values()))

            if formatting:

                # excel row with new data reference for formatting
                new_row = ws.max_row
    
                # for each cell in row, copy style from previous week's entry
                row_for_style = ws.iter_rows(min_row=new_row-(len(lens_type_dict)*days_in_week), max_row=new_row-(len(lens_type_dict)*days_in_week))

                for row in row_for_style:
                    for cell in row:
                        new_cell = ws.cell(row=new_row, column=cell.col_idx)
                        if k == 'Both':
                            new_cell.border = Border(top=Side(style='thin'),
                                                    bottom=Side(style='thin'),
                                                    right=Side(style='thin'))
                        elif k == 'PAL':
                            new_cell.border = Border( 
                                            top=Side(style='thin'),
                                            right=Side(style='thin'))
                        elif k == 'SV':
                            new_cell.border = Border( 
                                            bottom=Side(style='thin'),
                                            right=Side(style='thin'))

                        if cell.has_style:
                            new_cell.font = copy(cell.font)
                            new_cell.fill = copy(cell.fill)
                            new_cell.number_format = copy(cell.number_format)
                            new_cell.protection = copy(cell.protection)
                            new_cell.alignment = copy(cell.alignment)

    wb.save(os.path.join(filename, file_path))

    return results_dict

def plot_daily_job_count(client, color, start_date, end_date):

    """ 
    Plots daily job count for given client over given period

    Inputs:
    client: (str) Name of client/account totals to plot (from names list below)
    color: (str) line color to plot
    start_date: (Timestamp) First date in period to append (inclusive)
    end_date: (Timestamp) End date in period to append (exclusive)

    Returns: void

    """

    names = ['Date', 
             'Job type', 
             'Visionworks Schertz', 
             'Visionworks Richland Hills', 
             'Visionworks Total', 
             'Robertsons Optical Laboratories', 
             'Robertsons Optical Columbia', 
             'Robertsons Total',
             'Eye Kraft Total',
             'R&D Optical Total',
             'Pacific Artisan Labs Total',
             'Optica Lopez SA Total',
             'Combined']

    with open(os.path.join(path, config_name)) as stream:
        config = yaml.safe_load(stream)

    file_path = config['path_to_save']
    df = pd.read_excel(os.path.join(file_path, filename), names=names, skiprows=4)
    df = df[(df['Date'] >= pd.to_datetime(start_date)) & (df['Date'] < pd.to_datetime(end_date))]

    df_both = df[df['Job type']=='Both']
    df_sv = df[df['Job type']=='SV']
    df_pal = df[df['Job type']=='PAL']

    fig, ax = plt.subplots(figsize=(10,8))

    x = np.arange(0, len(df_both))

    ax.set_ylabel('Jobs (pairs)')
    ax.plot(df_both['Date'], df_both[client], color=color, label='Both')
    ax.plot(df_sv['Date'], df_sv[client], color=color, linestyle='--', label='SV')
    ax.plot(df_pal['Date'], df_pal[client], color=color, linestyle=':', label='PAL')

    xl = ax.set_xlabel('Date')
    ax.legend()
    ax.set_xticks(df_both['Date'])
    ax.set_xticklabels(df_both['Date'], rotation=90)
    date_form = DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
    ax.set_title(f'{client} Jobs')
    figname = os.path.join(file_path, f'{client} Jobs {start_date}-{end_date}.jpg')
    fig.savefig(figname, box_extra_artists=(xl,), bbox_inches='tight')
    
def main():
    print('Enter type of script to run')
    print('For daily script, enter "daily" or "d", for weekend script enter "weekend" or "w"')
    print('Otherwise enter "bespoke" or "b" to set start and end date ')
    script_type = input()
    
    if (script_type == 'daily') or (script_type == 'd'):
        today = date.today()
        days = 1
        run_script_daily(today, days, record_time=False)
    elif (script_type == 'weekend') or (script_type == 'w'):
        today = date.today()
        days = 3
        run_script_daily(today, days, record_time=False)
        run_plots(today)
    elif (script_type == 'bespoke') or (script_type == 'b'):
        print('Enter start date (inclusive) (YYYY/MM/DD):')
        start_date = input()
        print('Enter end date (exclusive) (YYYY/MM/DD):')
        end_date = input()
        run_script_historical(start_date, end_date)

if __name__ == '__main__':

    args = sys.argv
    globals()[args[1]](*args[2:])