"""
Scripts to run data export in format produced by portal
Produces zipped csv file
"""
import pandas as pd 
import numpy as np
import os
import yaml
import math
import csv
import click
from utils import get_project_root
import time
from data_elt import sql_query_to_df, db_to_portal_labels
import zipfile

config_name = 'config_data_export.yml'
db_portal_dict_yml = 'db_to_portal_dict_export.yml'
path = get_project_root()

def get_raw_export(client, start_date, end_date, path_to_save, account=None, record_time=False):

    with open(os.path.join(path, config_name)) as stream:
        config = yaml.safe_load(stream)

    with open(os.path.join(path, db_portal_dict_yml), 'r') as stream:
        db_portal_dict = yaml.safe_load(stream)

    if account is None:
        accountids = [v for v in db_portal_dict[client].values()]
    else:
        accountids = [db_portal_dict[client][account]]

    accountids = [v for v in db_portal_dict[client].values()]

    record_times = None
    if record_time:
        record_times = {'path': f'{db_portal_dict["record_times"]["path"]}', 
        'filename': db_portal_dict['record_times']['filename']}

    start_date_ts = pd.to_datetime(start_date)
    end_date_ts = pd.to_datetime(end_date)

    df = sql_query_to_df(config, start_date_ts, end_date_ts, accountids, record_times)
    df = db_to_portal_labels(df, db_portal_dict_yml, path)
    df['MeasurementDate'] = pd.to_datetime(df['MeasurementDate'])

    file_to_save = f'{client}_{start_date}_{end_date}'
    full_file = os.path.join(path_to_save, f'{file_to_save}')

    df.to_csv(f'{full_file}.csv', index=False, date_format='%Y-%m-%d %H:%M')

    with zipfile.ZipFile(f'{full_file}.zip', 'w') as file:
        file.write(f'{full_file}.csv', arcname=f'{file_to_save}.csv')
    
    os.remove(f'{full_file}.csv')
    return df

@click.command()
@click.argument('client', nargs=1)
@click.argument('start_date', nargs=1)
@click.argument('end_date', nargs=1)
@click.argument('path_to_save', nargs=1)
@click.option('--account', default=None, help='Client Account Name')
@click.option('--record_time', default=False, help='Record query duration')
def raw_export_cmd_line(client, start_date, end_date, path_to_save, account=None, record_time=False):
    
    get_raw_export(client, start_date, end_date, path_to_save, account=None, record_time=False)
    
if __name__ == '__main__':

    raw_export_cmd_line()