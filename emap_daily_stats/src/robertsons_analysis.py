# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 11:31:15 2022
Robertsons analysis scripts
@author: FionaLoftus
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.dates import DateFormatter, MO
import matplotlib.dates as mdates
# %%
# 1. load xlsx file
dir_path = 'C:\\Users\\FionaLoftus\\Box\\Technical File eMap\\eMap Daily Stats'
filename = 'Robertsons_daily_numbers.xlsx'

col_names = ['Date', 'LensType', 'IndiPair', 'OverallPass', 'AutomaticPass', 'ManualPass', \
             'PrescriptionPass', 'PrescriptionAuto', 'PrescriptionManual', 'SurfacePass', \
                 'SurfaceAuto', 'SurfaceManual', 'Sph', 'Cyl', 'Axis', 'Add', 'Prism', 'PrismFTM', \
                     'JobsCount', 'JPH']
df = pd.read_excel(os.path.join(dir_path, filename), header=None, skiprows=[0,1], \
                   names=col_names)

# %%
columns_colors = {}
lenses_lines = {'Both': '-',
                'SV': '--',
                'PAL': ':'}
# %%
def plot_by_date_2_cols(df, lenstype, columns, save=None):
    
    cmap = cm.get_cmap('tab10')
    df = df[df['LensType']==lenstype]
    fig, ax = plt.subplots()
    ax2 = ax.twinx()
    ax.plot(df['Date'], df[list(columns.keys())[0]], \
            color=cmap(list(columns.values())[0]), label=list(columns.keys())[0])
    ax2.plot(df['Date'], df[list(columns.keys())[1]], \
             color=cmap(list(columns.values())[1]), label=list(columns.keys())[1])
    ax.set_xticks(df['Date'])
    ax.set_ylabel('Pass Rate')
    ax2.set_ylabel('Jobs Count')
    ax.legend()
    ax2.legend()
    ax.set_xticklabels(df['Date'], rotation=90)
    ax.set_title()
    date_form = DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
    xl = ax.set_xlabel('Date')
    if save is not None:
        figname = os.path.join(save, f'{lenstype} {list(columns.keys())[0]} {list(columns.keys())[1]} .jpg')
        fig.savefig(figname, box_extra_artists=(xl,), bbox_inches='tight')
    
def plot_by_date(df, lenstype, column, save=None):
    
    cmap = cm.get_cmap('tab10')
    df = df[df['LensType']==lenstype]
    fig, ax = plt.subplots()
    ax.plot(df['Date'], df[column], linestyle=lenses_lines[lenstype],
            color=cmap(0), label=column)
    ax.set_xticks(df['Date'])
    ax.set_ylabel('Pass Rate')
    ax.legend()
    ax.set_xticklabels(df['Date'], rotation=90)
    ax.set_title(f'{lenstype} Lenses')
    date_form = DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
    xl = ax.set_xlabel('Date')
    if save is not None:
        figname = os.path.join(save, f'{lenstype} {column}.jpg')
        fig.savefig(figname, box_extra_artists=(xl,), bbox_inches='tight')
        
        
def moving_average(a, n=3) :
    ret = np.nancumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
        
def plot_auto_man_breakdown(df, 
                            lenstype, 
                            overallcol, 
                            autocol, 
                            mancol,
                            start_date=None,
                            end_date=None,
                            ma=None,
                            save=None):
    
    cmap = cm.get_cmap('tab10')
    df = df[df['LensType']==lenstype]
    figname = f'{lenstype} {overallcol}'
    if start_date is not None:
        df = df[(df['Date']>=pd.to_datetime(start_date))&(df['Date']<=pd.to_datetime(end_date))]
        figname = f'{figname}_{start_date}_{end_date}'
    overall = df[overallcol].to_numpy()
    auto = df[autocol].to_numpy()
    manual = df[mancol].to_numpy()
    if ma is not None:
        overall = moving_average(overall, ma)
        overall = np.pad(overall, int(ma/2), mode='edge')
        auto = moving_average(auto, ma)
        auto = np.pad(auto, int(ma/2), mode='edge')
        manual = moving_average(manual, ma)
        manual = np.pad(manual, int(ma/2), mode='edge')
        figname = f'{figname}_MA{ma}'
    fig, ax = plt.subplots()
    ax.plot(df['Date'], overall, linestyle=lenses_lines[lenstype],
            color=cmap(0), label='Pass Rate')
    ax.plot(df['Date'], auto, label='Automatic Pass', color=cmap(1), \
             linestyle=lenses_lines[lenstype])
    ax.plot(df['Date'], manual, label='Manual Pass', color=cmap(2), \
             linestyle=lenses_lines[lenstype])
    ax.set_xticks(df['Date'])
    ax.set_ylabel('Percentage')
    ax.legend()
    ax.set_xticklabels(df['Date'], rotation=90)
    ax.set_title(f'{overallcol} {lenstype} Lenses')
    date_form = DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
    xl = ax.set_xlabel('Date')
    if save is not None:
        figname = f'{figname}.jpg'
        figpath = os.path.join(save, figname)
        fig.savefig(figpath, box_extra_artists=(xl,), bbox_inches='tight')
        
def plot_all_prescriptions(df,
                           lenstype,
                           start_date=None,
                           end_date=None,
                           ma=None,
                           save=None):

    cmap = cm.get_cmap('tab10')
    df_to_use = df[df['LensType']==lenstype]
    start_date=None
    end_date=None
    prescription_types = {'Sph': 3, 'Cyl': 4, 'Axis': 5, 'Add': 6, 'Prism': 7}
    figname = f'{lenstype} Prescription Breakdown'
    if start_date is not None:
        df_to_use = df_to_use[(df_to_use['Date']>=pd.to_datetime(start_date))&(df_to_use['Date']<=pd.to_datetime(end_date))]
        figname = f'{figname}_{start_date}_{end_date}'
    prescription_values = {}
    if ma is not None:
        figname = f'{figname}_MA{ma}'
    for key in prescription_types.keys():
        prescription_values[key] = df_to_use[key].to_numpy()  
        if ma is not None:
            prescription_values[key] = moving_average( prescription_values[key], ma)
            prescription_values[key] = np.pad( prescription_values[key], int(ma/2), mode='edge')
    fig, ax = plt.subplots()
    for key, item in prescription_values.items():
        ax.plot(df_to_use['Date'], item, linestyle=lenses_lines[lenstype],
            color=cmap(prescription_types[key]), label=key)
    ax.set_xticks(df['Date'])
    ax.set_ylabel('Percentage')
    ax.legend()
    ax.set_xticklabels(df['Date'], rotation=90)
    ax.set_title(f'{lenstype} Prescription Breakdown')
    date_form = DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
    xl = ax.set_xlabel('Date')
    if save is not None:
        figname = f'{figname}.jpg'
        figpath = os.path.join(save, figname)
        fig.savefig(figpath, box_extra_artists=(xl,), bbox_inches='tight')

def plot_single_prescriptive_type(df,
                                  prescription_type,
                                  start_date=None,
                                  end_date=None,
                                  ma=None,
                                  save=None):
    cmap = cm.get_cmap('tab10')
    prescription_types = {'Sph': 3, 'Cyl': 4, 'Axis': 5, 'Add': 6, 'Prism': 7}
    figname = f'{prescription_type} Pass Rates'
    if start_date is not None:
        df = df[(df['Date']>=pd.to_datetime(start_date))&(df['Date']<=pd.to_datetime(end_date))]
        figname = f'{figname}_{start_date}_{end_date}'
    lens_values = {}
    if ma is not None:
        figname = f'{figname}_MA{ma}'
    for key in lenses_lines.keys():
        df_lens = df[df['LensType']==key]
        lens_values[key] = df_lens[prescription_type].to_numpy()
        if ma is not None:
            lens_values[key] = moving_average(lens_values[key], ma)
            lens_values[key] = np.pad(lens_values[key], int(ma/2), mode='edge')   
    fig, ax = plt.subplots()
    for key, item in lens_values.items():
        ax.plot(df_lens['Date'], item, linestyle=lenses_lines[key],
            color=cmap(prescription_types[prescription_type]), label=key)
    ax.set_xticks(df['Date'])
    ax.set_ylabel('Percentage')
    ax.set_ylim([0,102])
    ax.legend()
    ax.set_xticklabels(df['Date'], rotation=90)
    ax.set_title(f'{prescription_type} Pass Rates')
    date_form = DateFormatter("%Y-%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=MO))
    xl = ax.set_xlabel('Date')
    if save is not None:
        figname = f'{figname}.jpg'
        figpath = os.path.join(save, figname)
        fig.savefig(figpath, box_extra_artists=(xl,), bbox_inches='tight')
# %%
save = 'D:\\Atlas\\RobertsonsAnalysis'
plot_single_prescriptive_type(df, 'Prism', ma=7, save=save)
# %%
start_date = None
end_date = None
ma=None
overall='PrescriptionPass'
auto='PrescriptionAuto'
manual='PrescriptionManual'
save = 'D:\\Atlas\\RobertsonsAnalysis'
plot_auto_man_breakdown(df, 'Both', overall, auto, manual, start_date, end_date, ma, save)
# %%
plot_auto_man_breakdown(df, 'SV', overall, auto, manual, start_date, end_date, ma, save)
# %%
plot_auto_man_breakdown(df, 'PAL', overall, auto, manual, start_date, end_date, ma, save)
