@echo off

for /f %%x in ('wmic path win32_utctime get /format:list ^| findstr "="') do set %%x
set today=%Year%-%Month%-%Day%

set client=Visionworks

set CONDAPATH=C:\Users\FionaLoftus\anaconda3

set ENVNAME=py_general

if %ENVNAME%==base (set ENVPATH=%CONDAPATH%) else (set ENVPATH=%CONDAPATH%\envs\%ENVNAME%)

@echo on
call %CONDAPATH%\Scripts\activate.bat %ENVPATH%

python "D:\python_code\anaylsis_scripts\emap_daily_stats\src\daily_numbers_xlsx.py" %client% %today% --account=RichlandHills

call conda deactivate