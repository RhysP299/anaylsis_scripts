from setuptools import find_packages, setup

setup(
    name='emap_daily_numbers',
    packages=find_packages(),
    version='1.5.0',
    description='''Automated production of daily stats for eMap clients''',
    author='fiona loftus',
    license='MIT',
)