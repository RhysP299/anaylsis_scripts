# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 13:33:28 2021
The simulation tool for the eMap C2.
@author: RhysPoolman
"""
import clr
import os
import winreg
from itertools import islice
import patterns.args as args
import patterns.generator as gen
import eMap_interface.options as opt
import cv2 as cv

# This boilerplate requires the 'pythonnet' module.
# The following instructions are for installing the 'pythonnet' module via
# pip:
#    1. Ensure you are running Python 3.4, 3.5, 3.6, or 3.7. PythonNET does
#       not work with Python 3.8 yet.
#    2. Install 'pythonnet' from pip via a command prompt (type 'cmd' from the
#       start menu or press Windows + R and type 'cmd' then enter)
#
#        python -m pip install pythonnet

# determine the Zemax working directory
aKey = winreg.OpenKey(winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER),
                      r"Software\Zemax", 0, winreg.KEY_READ)
zemaxData = winreg.QueryValueEx(aKey, 'ZemaxRoot')
NetHelper = os.path.join(os.sep, zemaxData[0],
                         r'ZOS-API\Libraries\ZOSAPI_NetHelper.dll')
winreg.CloseKey(aKey)

# add the NetHelper DLL for locating the OpticStudio install folder
clr.AddReference(NetHelper)
import ZOSAPI_NetHelper

pathToInstall = ''
# uncomment the following line to use a specific instance of the ZOS-API
# assemblies
# pathToInstall = r'C:\C:\Program Files\Zemax OpticStudio'

# connect to OpticStudio
success = ZOSAPI_NetHelper.ZOSAPI_Initializer.Initialize(pathToInstall)

zemaxDir = ''
if success:
    zemaxDir = ZOSAPI_NetHelper.ZOSAPI_Initializer.GetZemaxDirectory()
    print('Found OpticStudio at:   %s' + zemaxDir)
else:
    raise Exception('Cannot find OpticStudio')

# load the ZOS-API assemblies
clr.AddReference(os.path.join(os.sep, zemaxDir, r'ZOSAPI.dll'))
clr.AddReference(os.path.join(os.sep, zemaxDir, r'ZOSAPI_Interfaces.dll'))
import ZOSAPI

TheConnection = ZOSAPI.ZOSAPI_Connection()
if TheConnection is None:
    raise Exception("Unable to intialize NET connection to ZOSAPI")

TheApplication = TheConnection.ConnectAsExtension(0)
if TheApplication is None:
    raise Exception("Unable to acquire ZOSAPI application")

if not TheApplication.IsValidLicenseForAPI:
    msg = "License is not valid for ZOSAPI use.  Make sure you have enabled "
    msg += "'Programming > Interactive Extension' from the OpticStudio GUI."
    raise Exception(msg)

TheSystem = TheApplication.PrimarySystem
if TheSystem is None:
    raise Exception("Unable to acquire Primary system")


def transpose(data):
    """Transposes a 2D list (Python3.x or greater).

    Useful for converting mutli-dimensional line series (i.e. FFT PSF)

    Parameters
    ----------
    data      : Python native list (if using System.Data[,] object reshape
                first)

    Returns
    -------
    res       : transposed 2D list
    """
    if type(data) is not list:
        data = list(data)
    return list(map(list, zip(*data)))


def reshape(data, x, y, transpose=False):
    """Converts a System.Double[,] to a 2D list for plotting or post
    processing

    Parameters
    ----------
    data      : System.Double[,] data directly from ZOS-API
    x         : x width of new 2D list [use var.GetLength(0) for dimension]
    y         : y width of new 2D list [use var.GetLength(1) for dimension]
    transpose : transposes data; needed for some multi-dimensional line series
                data

    Returns
    -------
    res       : 2D list; can be directly used with Matplotlib or converted to
                a numpy array using numpy.asarray(res)
    """
    if type(data) is not list:
        data = list(data)
    var_lst = [y] * x
    it = iter(data)
    res = [list(islice(it, i)) for i in var_lst]
    if transpose:
        return transpose(res)
    return res


def run_analysis(analysis):
    """
    Checks if analysis is running, if it is waits for completion before
    running the analysis again and waiting for completion.  If the analysis is
    not running then runs it and waits for completeion.  Will not return until
    analysis has finished.

    Parameters
    ----------
    analysis : ZOSAPI I_Analysis object
        An anaylsis object from the ZOSAPI of Zemax OpticStudio.  See
        Classes -> Class List -> ZOSAPI -> Analysis -> I_Analysis section of
        the ZOSAPI help and the The Programming Tab -> About ZOSAPI -> Anaylsis
        section of the OpticStudio manual for details.

    Returns
    -------
    None.
    """
    if analysis.IsRunning():
        analysis.WaitForCompletion()
    analysis.ApplyAndWaitForCompletion()


print('Connected to OpticStudio')

# The connection should now be ready to use.  For example:
print('Serial #: ', TheApplication.SerialCode)

# set pattern arguments for vertical lines
pattern = args.PatternTypes.horizontal_lines
encoded_period_type = 4
line_thickness = 10
radius = 10
image_width = 800
image_height = 800
offset_x = 0.0
offset_y = 0.0
start_shift = 0.0
color = args.ColorTypes.green
arg_set = \
    args.LensmeterPatternsGeneratorArgs(pattern, encoded_period_type,
                                        line_thickness, radius, image_width,
                                        image_height, offset_x, offset_y,
                                        start_shift, color)

# generate vertical line pattern
image = gen.create_vertical_line_pattern(arg_set)

# save image to file where opticstudio can access it
path = "C:\\Users\\RhysPoolman\\OneDrive - EYOTO GROUP LTD\\Documents" + \
    "\\Zemax\\IMAFiles\\"
filename = "monitor_pattern_800_x_800.png"
cv.imwrite(path + filename, image)

# create image simulation analysis
TheAnalysis = TheSystem.Analyses
image_simulation = TheAnalysis.New_ImageSimulation()

# change settings of image simulation
image_simulation_settings = image_simulation.GetSettings()

# change settings of image simulation
image_simulation_settings = image_simulation.GetSettings()
image_simulation_settings.InputFile = "monitor_pattern_800_x_800.png"

# get source bitmap output
source_bitmap = \
    ZOSAPI.Analysis.Settings.ExtendedScene.ISShowAsTypes.SourceBitmap
image_simulation_settings.ShowAs = source_bitmap

# generate source bitmap results
run_analysis(image_simulation)

# get source bitmap results
image_simulation_source_bitmap_results = image_simulation.GetResults()
source_bitmap_metadata = image_simulation_source_bitmap_results.MetaData
source_bitmap_header_data = image_simulation_source_bitmap_results.HeaderData
for line in source_bitmap_header_data.Lines:
    print(line)
source_bitmap_data_grid = image_simulation_source_bitmap_results.DataGrids
print(source_bitmap_data_grid)

# close image simulation
image_simulation.Close()
