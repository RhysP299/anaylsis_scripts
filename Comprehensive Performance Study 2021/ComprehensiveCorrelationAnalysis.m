function ComprehensiveCorrelationAnalysis(ReferenceData, TargetData, NumberOfBins)
    %% Overall Correlation Analysis:
%     MultipleCorrelationAnalysis( ReferenceData, TargetData, ...
%                                 {'Sphere_Lens', 'Cylinder_Lens', 'Axis_Lens', 'Add_Lens', ...
%                                 'HPrism_Lens', 'VPrism_Lens', 'HPrism_Pair', 'VPrism_Pair'}, NumberOfBins);
%     CorrelationAnalysis( ANOVADataExtract(ExtrcatDataByDeviceRecorder(ReferenceData, '', 'Any', 'Combined', 'Combined'), {'Recorder', 'Device'}, 'Cylinder'), ...
%                          ANOVADataExtract(TargetData, {'Recorder', 'Device'}, 'Axis'), ...
%                          NumberOfBins);
                 
    %% Equivalent Power Calculation
    ReferenceData.OD_RightLens{1}=AddIndividualLensesEquivalentPower(ReferenceData.OD_RightLens{1});
    ReferenceData.OS_LeftLens{1}=AddIndividualLensesEquivalentPower(ReferenceData.OS_LeftLens{1});
    ReferenceData.Pair{1}=AddPairsPrismEquivalentPower(ReferenceData);
    
    %% Equivalent Power Correlation Analysis
    TargetMeasures={'Sphere', 'Cylinder', 'Axis', 'Add', 'HPrism', 'VPrism'};
    for i=1:length(TargetMeasures)
        CorrelationAnalysis( ANOVADataExtract(ExtrcatDataByDeviceRecorder(ReferenceData, '', 'Any', 'Combined', 'Combined'), {'Recorder', 'Device'}, 'EquivalentPower'), ...
                             ANOVADataExtract(TargetData, {'Recorder', 'Device'}, TargetMeasures{i}), NumberOfBins);
    end
    CorrelationAnalysis( ANOVADataExtract(ExtrcatDataByDeviceRecorder(ReferenceData, '', 'Any', 'Combined', 'Combined'), {'Recorder', 'Device'}, 'VPrismEquivalentPower', true, true), ...
                         ANOVADataExtract(TargetData, {'Recorder', 'Device'}, 'VPrism', true, true), NumberOfBins);
    CorrelationAnalysis( ANOVADataExtract(ExtrcatDataByDeviceRecorder(ReferenceData, '', 'Any', 'Combined', 'Combined'), {'Recorder', 'Device'}, 'HPrismEquivalentPower', true, true), ...
                         ANOVADataExtract(TargetData, {'Recorder', 'Device'}, 'HPrism', true, true), NumberOfBins);
end

function Data=AddIndividualLensesEquivalentPower(Data)
    Data.EquivalentPower=Data.Sphere+(Data.Cylinder/2);
end

function Data=AddPairsPrismEquivalentPower(ReferenceData)
    Data=ReferenceData.Pair{1};
    [RightLens.VPrismEquivalentPower, RightLens.HPrismEquivalentPower]=LensMeridianPowerCalculator( ReferenceData.OD_RightLens{1}.Sphere, ...
                                                                                                    ReferenceData.OD_RightLens{1}.Cylinder, ...
                                                                                                    ReferenceData.OD_RightLens{1}.Axis);
    [LeftLens.VPrismEquivalentPower, LeftLens.HPrismEquivalentPower]=LensMeridianPowerCalculator( ReferenceData.OS_LeftLens{1}.Sphere, ...
                                                                                                  ReferenceData.OS_LeftLens{1}.Cylinder, ...
                                                                                                  ReferenceData.OS_LeftLens{1}.Axis);
	Data.VPrismEquivalentPower=HighestPrismEquivalentPowerCalculator(RightLens.VPrismEquivalentPower, LeftLens.VPrismEquivalentPower);
    Data.HPrismEquivalentPower=HighestPrismEquivalentPowerCalculator(RightLens.HPrismEquivalentPower, LeftLens.HPrismEquivalentPower);
end

function [VPrismEquivalentPower, HPrismEquivalentPower]=LensMeridianPowerCalculator(SphericalPower, CylindericalPower, Angle)
    Angle(Angle>90)=Angle(Angle>90)-90;
    VPrismEquivalentPower=SphericalPower.*sin(pi/180.*Angle);
    HPrismEquivalentPower=SphericalPower.*cos(pi/180.*(Angle));
%     VPrismEquivalentPower=(SphericalPower+CylindericalPower).*(sin(pi/180.*Angle).^2);
%     HPrismEquivalentPower=(SphericalPower+CylindericalPower).*(sin(pi/180.*(Angle+90)).^2);
end

function HighestPrismEquivalentPower=HighestPrismEquivalentPowerCalculator(RightLensPrismEquivalentPower, LeftLensPrismEquivalentPower)
    HighestPrismEquivalentPower=[RightLensPrismEquivalentPower, LeftLensPrismEquivalentPower];
    HighestPrismEquivalentPower=max(HighestPrismEquivalentPower, [], 2);
end




