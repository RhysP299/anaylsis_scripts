function Direction=PrismVectorDirection(Angle, Axis, Lens)
    Direction={'UNKNOWN'};
    if(strcmp(Axis,'Horizontal'))
        if(strcmp(Lens,'Left'))
            if((Angle>90) && (Angle<270))
                Direction={'IN'};
            else
                Direction={'OUT'};
            end
        else
            if((Angle>90) && (Angle<270))
                Direction={'OUT'};
            else
                Direction={'IN'};
            end
        end
    elseif(strcmp(Axis,'Vertical'))
        if((Angle>=0) && (Angle<=180))
            Direction={'UP'};
        else
            Direction={'DN'};
        end
    end
end