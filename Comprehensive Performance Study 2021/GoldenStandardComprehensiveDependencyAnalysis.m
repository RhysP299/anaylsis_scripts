function [BetweenIndependentParameterDifferenceAnalysisResult, AVOVAResults]=GoldenStandardComprehensiveDependencyAnalysis(Data, GoldStandard, SignificanceLevel, DrawMarginalMeans, DrawCrossTabulationPlots)
    %% Initialization
    DependantVariablesPreferableOrder={'Sphere', 'Cylinder' ,'Axis', 'Add', ...
                                       'HPrismMagnitude',	'HPrismDirection',	...
                                       'VPrismMagnitude', 'VPrismDirection', ...
                                       'HPrism', 'VPrism', 'PassFail'};
    if(~exist('DrawMarginalMeans', 'var'))
        DrawMarginalMeans=false;
    end
    if(~exist('DrawCrossTabulationPlots', 'var'))
        DrawCrossTabulationPlots=false;
    end
    AllDependantVariables={};
    for i=1:length(Data.Properties.VariableNames)
        if(~strcmp(Data.Properties.VariableNames{i}, 'Recorder') && ~strcmp(Data.Properties.VariableNames{i}, 'Device'))
            for j=1:length(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames)
                AllDependantVariables{end+1}=Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j};
            end
        end
    end
    AllDependantVariables=intersect(DependantVariablesPreferableOrder, unique(AllDependantVariables), 'stable');
    AllDependantVariables(strcmp(AllDependantVariables,'JobID'))=[];
    BetweenIndependentParameterDifferenceAnalysisResult={};
    
%     AllIndependantVariables={'Recorder', 'Device'};
    VariableTypes=cell(1, length(AllDependantVariables)+3);
    VariableTypes(1:2)={'cell'};
    VariableTypes(3:end)={'double'};
    AVOVAResults=table('Size', [6, length(VariableTypes)], 'VariableTypes', VariableTypes, 'VariableNames', [{'Type', 'Source', 'df'}, AllDependantVariables]);
    
    disp(' ')
    disp(' ')
    disp('############################# Golden Standard Comprehensive Dependency Analysis #############################')
    
    
    %% Individual Lenses Analysis
    AVOVAResultsStartIndex=1;
    AVOVAResults.Type(AVOVAResultsStartIndex:3)={'Individual Lenses'};
    disp('================ Individual Lenses =================')
    for i=1:length(AllDependantVariables)
        if(isa(Data.OD_RightLens{1}.(AllDependantVariables{i}), 'double'))
            TempResult=ANOVA(ANOVADataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}), SignificanceLevel);
            for j=1:size(TempResult.Table, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult.Table{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 3};
            end
            Mode=0; %% 0=>Device & Recorder , Negative=>Device ONLY , Positive=>Recorder ONLY
            NumberofSignificants=0;
            for j=1:size(TempResult.Table,1)
                if(strcmp(TempResult.Table{j,1}, 'Recorder') && (TempResult.Table{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode+1;
                end
                if(strcmp(TempResult.Table{j,1}, 'Device') && (TempResult.Table{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode-1;
                end
            end
            if(NumberofSignificants>0)
                if(Mode==0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}), 'Between_Recorder_&_Device_for_Lenses');
                elseif(Mode>0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Not Combined', 'Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}), 'Between_Recorders_for_Lenses');
                else
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Combined', 'Not Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}), 'Between_Devices_for_Lenses');
                end
                BetweenIndependentParameterDifferenceAnalysisResult{end}.Statistics=TempResult.Statistics;
            end
        else
            TempResult=MultinomialLogisticRegression(MultinomialLogisticRegressionDataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}), SignificanceLevel);
            for j=1:size(TempResult, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult{j, 3};
            end
            Mode=0; %% 0=>Device & Recorder , Negative=>Device ONLY , Positive=>Recorder ONLY
            NumberofSignificants=0;
            for j=1:size(TempResult,1)
                if(strcmp(TempResult{j,1}, 'Recorder') && (TempResult{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode+1;
                end
                if(strcmp(TempResult{j,1}, 'Device') && (TempResult{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode-1;
                end
            end
            if(NumberofSignificants>0)
                if(Mode==0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}), 'Between_Recorder_&_Device_for_Lenses');
                elseif(Mode>0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Not Combined', 'Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}), 'Between_Recorders_for_Lenses');
                else
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Combined', 'Not Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}), 'Between_Devices_for_Lenses');
                end
            end 
        end
    end
    
    %% Pairs Analysis
    AVOVAResultsStartIndex=4;
    AVOVAResults.Type(AVOVAResultsStartIndex:end)={'Pairs'};
    disp('====================== Pairs =======================')
    for i=1:length(AllDependantVariables)
        if(isa(Data.OD_RightLens{1}.(AllDependantVariables{i}), 'double'))
            TempResult=ANOVA(ANOVADataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), SignificanceLevel);
            for j=1:size(TempResult.Table, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult.Table{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 3};
            end
            Mode=0; %% 0=>Device & Recorder , Negative=>Device ONLY , Positive=>Recorder ONLY
            NumberofSignificants=0;
            for j=1:size(TempResult.Table,1)
                if(strcmp(TempResult.Table{j,1}, 'Recorder') && (TempResult.Table{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode+1;
                end
                if(strcmp(TempResult.Table{j,1}, 'Device') && (TempResult.Table{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode-1;
                end
            end
            if(NumberofSignificants>0)
                if(Mode==0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), 'Between_Recorder_&_Device_for_Pairs');
                elseif(Mode>0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Not Combined', 'Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), 'Between_Recorders_for_Pairs');
                else
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Combined', 'Not Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), 'Between_Devices_for_Pairs');
                end
                BetweenIndependentParameterDifferenceAnalysisResult{end}.Statistics=TempResult.Statistics;
            end
        else
            TempResult=MultinomialLogisticRegression(MultinomialLogisticRegressionDataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), SignificanceLevel);
            for j=1:size(TempResult, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult{j, 3};
            end
            Mode=0; %% 0=>Device & Recorder , Negative=>Device ONLY , Positive=>Recorder ONLY
            NumberofSignificants=0;
            for j=1:size(TempResult,1)
                if(strcmp(TempResult{j,1}, 'Recorder') && (TempResult{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode+1;
                end
                if(strcmp(TempResult{j,1}, 'Device') && (TempResult{j,3}<SignificanceLevel))
                    NumberofSignificants=NumberofSignificants+1;
                    Mode=Mode-1;
                end
            end
            if(NumberofSignificants>0)
                if(Mode==0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(Data, {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), 'Between_Recorder_&_Device_for_Pairs');
                elseif(Mode>0)
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Not Combined', 'Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), 'Between_Recorders_for_Pairs');
                else
                    BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Combined', 'Not Combined'), {'Recorder', 'Device'}, AllDependantVariables{i}, true, true), 'Between_Devices_for_Pairs');
                end
            end 
        end 
    end
    
    %% Between Independent Parameter Difference Analysis
    BetweenIndependentParameterDifferenceAnalysisPlot(BetweenIndependentParameterDifferenceAnalysisResult, DrawMarginalMeans, DrawCrossTabulationPlots);
    disp('############################# Golden Standard Comprehensive Dependency Analysis #############################')
    disp(' ')
    disp(' ')
end