function [LMSData, ExcelFileFormat, FrameData]=ImportLMSData(FolderPath, ImportFrameData)
    if(~exist('ImportFrameData', 'var'))
        ImportFrameData=false;
    end
    
    if(exist('LMSData.mat', 'file'))
        load('LMSData.mat');
        ExcelFileFormat={};
        FrameData=[];
    else
        if(~exist('MakeExcelFileFormat', 'var'))
            MakeExcelFileFormat=false;
        end
        Files=dir(FolderPath);
        if(ImportFrameData)
            FrameData=zeros(size(Files,1)-2, 28);
        else
            FrameData=[];
        end
        LMSData.Recorder='LMS Data';
        LMSData.Device='LMS Data';
        Direction_Label=cell(size(Files,1)-2, 1);
        Direction_Label(:)={'UNKNOW'};
        LMSData.OD_RightLens = table( zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), ...
                                      zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), ...
                                      zeros(size(Files,1)-2, 1), Direction_Label, ...
                                      zeros(size(Files,1)-2, 1), Direction_Label, ...
                                      zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), ...
                                      Direction_Label, ...
                                      'VariableNames', { 'JobID', 'Sphere', 'Cylinder', ...
                                                         'Axis', 'Add', ...
                                                         'HPrismMagnitude', 'HPrismDirection', ...
                                                         'VPrismMagnitude', 'VPrismDirection', ...
                                                         'HPrism', 'VPrism', 'PassFail' } );

        LMSData.OS_LeftLens =  table( zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), ...
                                      zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), ...
                                      zeros(size(Files,1)-2, 1), Direction_Label, ...
                                      zeros(size(Files,1)-2, 1), Direction_Label, ...
                                      zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), ...
                                      Direction_Label, ...
                                      'VariableNames', { 'JobID', 'Sphere', 'Cylinder', ...
                                                         'Axis', 'Add', ...
                                                         'HPrismMagnitude', 'HPrismDirection', ...
                                                         'VPrismMagnitude', 'VPrismDirection', ...
                                                         'HPrism', 'VPrism', 'PassFail'} );  
        LMSData.Pair =          table( zeros(size(Files,1)-2, 1), ...
                                       zeros(size(Files,1)-2, 1), Direction_Label, ...
                                       zeros(size(Files,1)-2, 1), Direction_Label, ...
                                       zeros(size(Files,1)-2, 1), zeros(size(Files,1)-2, 1), Direction_Label, ...
                                       'VariableNames', { 'JobID', ...
                                                          'HPrismMagnitude', 'HPrismDirection', ...
                                                          'VPrismMagnitude', 'VPrismDirection', ...
                                                          'HPrism', 'VPrism', 'PassFail' } );

        Index=1;
        for i=1:length(Files)
            if(strcmp(Files(i).name,'.') || strcmp(Files(i).name,'..'))
                continue;
            end
            [SubData, SubFrameData]=ImportLMSFile(horzcat(Files(i).folder, '\', Files(i).name), str2double(strrep(strrep(Files(i).name,'0000',''),'.lms','')), ImportFrameData);
            if(ImportFrameData)
                FrameData(Index,:)=SubFrameData;
            end
            LMSData.OD_RightLens(Index, :)=SubData.OD_RightLens;
            LMSData.OS_LeftLens(Index, :)=SubData.OS_LeftLens;
            LMSData.Pair(Index, :)=SubData.Pair;
            Index=Index+1;
        end

        save('LMSData.mat', 'LMSData');

        ExcelFileFormat=cell(size(LMSData.OD_RightLens, 1)*2, 16);
        for i=1:size(LMSData.OD_RightLens, 1)
            ExcelFileFormat{(2*i)-1, 1}=LMSData.OD_RightLens.JobID(i);
            ExcelFileFormat{(2*i)-1, 2}='OD';
            ExcelFileFormat{(2*i)-1, 3}=LMSData.OD_RightLens.Sphere(i);
            ExcelFileFormat{(2*i)-1, 4}=LMSData.OD_RightLens.Cylinder(i);
            ExcelFileFormat{(2*i)-1, 5}=LMSData.OD_RightLens.Axis(i);
            ExcelFileFormat{(2*i)-1, 6}=LMSData.OD_RightLens.Add(i);
            ExcelFileFormat{(2*i)-1, 7}=LMSData.OD_RightLens.HPrismMagnitude(i);
            ExcelFileFormat{(2*i)-1, 8}=LMSData.OD_RightLens.HPrismDirection(i);
            ExcelFileFormat{(2*i)-1, 9}=LMSData.OD_RightLens.VPrismMagnitude(i);
            ExcelFileFormat{(2*i)-1, 10}=LMSData.OD_RightLens.VPrismDirection(i);
            ExcelFileFormat{(2*i)-1, 15}=LMSData.OD_RightLens.PassFail(i);

            ExcelFileFormat{(2*i), 1}=LMSData.OS_LeftLens.JobID(i);
            ExcelFileFormat{(2*i), 2}='OS';
            ExcelFileFormat{(2*i), 3}=LMSData.OS_LeftLens.Sphere(i);
            ExcelFileFormat{(2*i), 4}=LMSData.OS_LeftLens.Cylinder(i);
            ExcelFileFormat{(2*i), 5}=LMSData.OS_LeftLens.Axis(i);
            ExcelFileFormat{(2*i), 6}=LMSData.OS_LeftLens.Add(i);
            ExcelFileFormat{(2*i), 7}=LMSData.OS_LeftLens.HPrismMagnitude(i);
            ExcelFileFormat{(2*i), 8}=LMSData.OS_LeftLens.HPrismDirection(i);
            ExcelFileFormat{(2*i), 9}=LMSData.OS_LeftLens.VPrismMagnitude(i);
            ExcelFileFormat{(2*i), 10}=LMSData.OS_LeftLens.VPrismDirection(i);
            ExcelFileFormat{(2*i), 15}=LMSData.OS_LeftLens.PassFail(i);

            ExcelFileFormat{(2*i)-1, 11}=LMSData.Pair.HPrismMagnitude(i);
            ExcelFileFormat{(2*i)-1, 12}=LMSData.Pair.HPrismDirection(i);
            ExcelFileFormat{(2*i)-1, 13}=LMSData.Pair.VPrismMagnitude(i);
            ExcelFileFormat{(2*i)-1, 14}=LMSData.Pair.VPrismDirection(i);
            ExcelFileFormat{(2*i)-1, 16}=LMSData.Pair.PassFail(i);
        end
    end
end
