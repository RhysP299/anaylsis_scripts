function PlotCombinedData(Data)
    NumberofBins=10;
    IndependantVariableNames=Data.Properties.VariableNames(1:end-1);
    IndependantVariableNames(strcmp(IndependantVariableNames, 'JobID'))=[];
    if(~isempty(IndependantVariableNames))
        return;
    end

    Data(isnan(Data{:, end}), :)=[];
    Result.RawData=Data;
    Result.Mean=mean(Data{:, end});
    Result.STD=std(Data{:, end});
    Result.Median=median(Data{:, end});
    Result.Percentile25th=prctile(Data{:, end}, 25);
    Result.Percentile75th=prctile(Data{:, end}, 75);
    Result.RMSE=sqrt(sum(Data{:, end}.^2)/size(Data{:, end}, 1));
    [~, Result.DifferentFromZeroPValue]=ttest(Data{:, end});
      
    if(contains(Data.JobID{1}, 'Lens'))
        FigureName=horzcat(Data.Properties.VariableNames{end}, ' Individual Lenses - Combined Comparison');
    else
        FigureName=horzcat(Data.Properties.VariableNames{end}, ' Pairs - Combined Comparison');
    end
    fig=figure('Position', [2049 41 2048 1.0368e+03], 'name', FigureName);
    hold on
    grid on
    boxplot(Data{:, end}, 'labels', 'Combined Comparison');
    up_adj=get(findobj(gcf, 'tag', 'Upper Adjacent Value'),'YData');
    lower_adj=get(findobj(gcf, 'tag', 'Lower Adjacent Value'),'YData');
    if(iscell(up_adj))
        up_adj=cell2mat(up_adj);
    end
    if(iscell(lower_adj))
        lower_adj=cell2mat(lower_adj);
    end
    VertecesWidth=1/15;
    
    Percentile_25=prctile(Data{:, end}, 25);
    Percentile_75=prctile(Data{:, end}, 75);
    tempBox=Data{:, end}( (Data{:, end}>=Percentile_25) & (Data{:, end}<=Percentile_75) );
    [tempHistogram, tempHistogramEdges]=histcounts(tempBox, NumberofBins);
    Verteces=zeros(length(tempHistogram)*4, 2);
    Faces=zeros(length(tempHistogram), 4);
    StartIndex=1;
    FaceCounter=1;
    for i=1:length(tempHistogram)
        Verteces(StartIndex:StartIndex+3, :)=[ 1-VertecesWidth, tempHistogramEdges(i); ...
                                               1-VertecesWidth, tempHistogramEdges(i+1); ...
                                               1+VertecesWidth, tempHistogramEdges(i+1); ...
                                               1+VertecesWidth, tempHistogramEdges(i)];
        Faces(i,:)=FaceCounter:FaceCounter+3;
        StartIndex=StartIndex+4;
        FaceCounter=FaceCounter+4;
    end
    tempHistogramNormalised=tempHistogram'/sum(tempHistogram);
    tempHistogramNormalised=tempHistogramNormalised/max(max(tempHistogramNormalised))*100;
    patch('Faces', Faces, 'Vertices', Verteces, 'FaceVertexCData', tempHistogramNormalised, 'FaceColor','flat', 'EdgeColor', 'none');
        
    Title=horzcat('Comparison Between ', Data.Properties.VariableNames{end});
    if(contains(Data.JobID{1}, 'Lens'))
        Title=horzcat(Title, ' (Individual Lenses) measured');
    else
        Title=horzcat(Title, ' (Pairs) measured');
    end
    title(Title);
    set(gca,'fontsize', 15);
    ylabel(Data.Properties.VariableNames{end});

    rectangle('Position',[min(xlim), 0, max(xlim)-min(xlim), max(ylim)], 'FaceColor', [0, 1, 0, 0.05]);
    rectangle('Position',[min(xlim), min(ylim), max(xlim)-min(xlim), abs(min(ylim))], 'FaceColor', [1, 0, 0, 0.05]);
    h=boxplot(Data{:, end}, 'labels', 'Combined Comparison');
    set(findobj(gca,'type','line'), 'linew', 2);
    ylim([min(min(lower_adj)) - (abs(min(min(lower_adj))) * 0.1), max(max(up_adj))*1.1]);
    text(mean(xlim)*0.87, max(ylim)*0.9, 'Over Estimation Area', 'FontSize', 30, 'Color', 'g', 'FontWeight', 'bold');
    text(mean(xlim)*0.87, min(ylim)*0.9, 'Under Estimation Area', 'FontSize', 30, 'Color', 'r', 'FontWeight', 'bold');

    Result.UpperAdjacent(i)=up_adj(1, 1);
    Result.LowerAdjacent(i)=lower_adj(1, 1);
    
    dcm_obj = datacursormode(fig);
    set(dcm_obj,'UpdateFcn',{@BoxPlotDataCursorFunction, Result, {'Combined Comparison'}})
end





