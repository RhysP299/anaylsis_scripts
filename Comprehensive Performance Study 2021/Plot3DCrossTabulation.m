function Plot3DCrossTabulation(Data)
    NumberofBins=10;
    IndependantVariableNames=Data.Properties.VariableNames(1:end-1);
    IndependantVariableNames(strcmp(IndependantVariableNames, 'JobID'))=[];
    if(length(IndependantVariableNames)~=3)
        return;
    end
    UniqueGroups{1}=unique(Data.(IndependantVariableNames{1}));
    UniqueGroups{2}=unique(Data.(IndependantVariableNames{2}));
    UniqueGroups{3}=unique(Data.(IndependantVariableNames{3}));
    BoxPlotData=zeros(2, size(Data, 1));
    ResultLength=1;
    for i=1:length(UniqueGroups)
        ResultLength=ResultLength*length(UniqueGroups{i});
    end
    Result=table( zeros(ResultLength, 1), zeros(ResultLength, 1), ...
                  zeros(ResultLength, 1), zeros(ResultLength, 1), zeros(ResultLength, 1), ...
                  zeros(ResultLength, 1), zeros(ResultLength, 1), zeros(ResultLength, 1), ...
                  zeros(ResultLength, 1), cell(ResultLength, 1), ...
                  'VariableNames', {'Mean', 'STD', ...
                                    'Median', 'Percentile25th', 'Percentile75th', ...
                                    'LowerAdjacent', 'UpperAdjacent', 'RMSE', ...
                                    'DifferentFromZeroPValue', 'RawData'});
    StartIndex=1;
    GroupCounter=1;
    BoxPlotCategoryNames={};
    for i=1:length(UniqueGroups{1})
        for j=1:length(UniqueGroups{2})
            for k=1:length(UniqueGroups{3})
                Indeces=true(size(Data, 1), 1);
                if(isa(UniqueGroups{1},'double'))
                    Indeces=Indeces & (Data.(IndependantVariableNames{1}) == UniqueGroups{1}(i));
                    BoxPlotCategoryNames{GroupCounter}=num2str(UniqueGroups{1}(i));
                else
                    Indeces=Indeces & strcmp(Data.(IndependantVariableNames{1}), UniqueGroups{1}{i});
                    BoxPlotCategoryNames{GroupCounter}=UniqueGroups{1}{i};
                end
                if(isa(UniqueGroups{2},'double'))
                    Indeces=Indeces & (Data.(IndependantVariableNames{2}) == UniqueGroups{2}(j));
                    BoxPlotCategoryNames{GroupCounter}=horzcat(BoxPlotCategoryNames{GroupCounter}, ' & ', IndependantVariableNames{2}, ' ', num2str(UniqueGroups{2}(j)));
                else
                    Indeces=Indeces & strcmp(Data.(IndependantVariableNames{2}), UniqueGroups{2}{j});
                    BoxPlotCategoryNames{GroupCounter}=horzcat(BoxPlotCategoryNames{GroupCounter}, ' & ', UniqueGroups{2}{j});
                end
                if(isa(UniqueGroups{3},'double'))
                    Indeces=Indeces & (Data.(IndependantVariableNames{3}) == UniqueGroups{3}(k));
                    BoxPlotCategoryNames{GroupCounter}=horzcat(BoxPlotCategoryNames{GroupCounter}, ' & ', IndependantVariableNames{3}, ' ', num2str(UniqueGroups{3}(k)));
                else
                    Indeces=Indeces & strcmp(Data.(IndependantVariableNames{3}), UniqueGroups{3}{k});
                    BoxPlotCategoryNames{GroupCounter}=horzcat(BoxPlotCategoryNames{GroupCounter}, ' & ', UniqueGroups{3}{k});
                end
                temp=Data(Indeces, :);
                temp(isnan(temp{:, end}), :)=[];

                if(~isempty(temp))
                    Result.RawData{GroupCounter}=temp;
                    Result.Mean(GroupCounter)=mean(temp{:, end});
                    Result.STD(GroupCounter)=std(temp{:, end});
                    Result.Median(GroupCounter)=median(temp{:, end});
                    Result.Percentile25th(GroupCounter)=prctile(temp{:, end}, 25);
                    Result.Percentile75th(GroupCounter)=prctile(temp{:, end}, 75);
                    Result.RMSE(GroupCounter)=sqrt(sum(temp{:, end}.^2)/size(temp, 1));
                    [~, Result.DifferentFromZeroPValue(GroupCounter)]=ttest(temp{:, end});

                    BoxPlotData(1, StartIndex:(StartIndex+size(temp, 1)-1))=temp{:, end};
                    BoxPlotData(2, StartIndex:(StartIndex+size(temp, 1)-1))=GroupCounter;
                    StartIndex=StartIndex+size(temp, 1)+1;
                    GroupCounter=GroupCounter+1;
                else
                    BoxPlotCategoryNames(end)=[];
                end
            end
        end
    end
    BoxPlotData(:, BoxPlotData(2, :)==0)=[];
    Result(cellfun(@isempty,Result.RawData),:)=[];
    Result.DifferentFromZeroPValue(isnan(Result.DifferentFromZeroPValue))=0;
    
    if(contains(Data.JobID{1}, 'Lens'))
        FigureName=horzcat(Data.Properties.VariableNames{end}, ' Individual Lenses - Gorups Comparison');
    else
        FigureName=horzcat(Data.Properties.VariableNames{end}, ' Pairs - Gorups Comparison');
    end
    fig=figure('Position', [2049 41 2048 1.0368e+03], 'name', FigureName);
    hold on
    grid on    
    boxplot(BoxPlotData(1, :), BoxPlotData(2, :), 'labels', BoxPlotCategoryNames);
    up_adj=get(findobj(gcf, 'tag', 'Upper Adjacent Value'),'YData');
    lower_adj=get(findobj(gcf, 'tag', 'Lower Adjacent Value'),'YData');
    if(iscell(up_adj))
        up_adj=cell2mat(up_adj);
    end
    if(iscell(lower_adj))
        lower_adj=cell2mat(lower_adj);
    end
    VertecesWidth=max(xlim)/(6*(GroupCounter-1));
    for Group=1:GroupCounter-1
        temp=BoxPlotData(1, BoxPlotData(2, :)==Group);
        Percentile_25=prctile(temp, 25);
        Percentile_75=prctile(temp, 75);
        tempBox=temp( (temp>Percentile_25) & (temp<Percentile_75) );
        [tempHistogram, tempHistogramEdges]=histcounts(tempBox, NumberofBins);
        Verteces=zeros(length(tempHistogram)*4, 2);
        Faces=zeros(length(tempHistogram), 4);
        StartIndex=1;
        FaceCounter=1;
        for i=1:length(tempHistogram)
            Verteces(StartIndex:StartIndex+3, :)=[ Group-VertecesWidth, tempHistogramEdges(i); ...
                                                   Group-VertecesWidth, tempHistogramEdges(i+1); ...
                                                   Group+VertecesWidth, tempHistogramEdges(i+1); ...
                                                   Group+VertecesWidth, tempHistogramEdges(i)];
            Faces(i,:)=FaceCounter:FaceCounter+3;
            StartIndex=StartIndex+4;
            FaceCounter=FaceCounter+4;
        end
        tempHistogramNormalised=tempHistogram'/sum(tempHistogram);
        tempHistogramNormalised=tempHistogramNormalised/max(max(tempHistogramNormalised))*100;
        patch('Faces', Faces, 'Vertices', Verteces, 'FaceVertexCData', tempHistogramNormalised, 'FaceColor','flat', 'EdgeColor', 'none');
    end
    
    Title=horzcat('Comparison Between ', Data.Properties.VariableNames{end});
    if(contains(Data.JobID{1}, 'Lens'))
        Title=horzcat(Title, ' (Individual Lenses) measured in different ');
    else
        Title=horzcat(Title, ' (Pairs) measured in different ');
    end
    for i=1:length(IndependantVariableNames)
        if(i>1)
            Title=horzcat(Title, ' & ', IndependantVariableNames{i}, 's');
        else
            Title=horzcat(Title, IndependantVariableNames{i}, 's');
        end
    end
    title(Title);
    set(gca,'fontsize', 14);
    if(length(BoxPlotCategoryNames)>3)
        xtickangle(25);
    end
    ylabel(Data.Properties.VariableNames{end});

    if(min(BoxPlotData(1, :))<0)
        rectangle('Position',[min(xlim), 0, max(xlim)-min(xlim), max(ylim)], 'FaceColor', [0, 1, 0, 0.05]);
        rectangle('Position',[min(xlim), min(ylim), max(xlim)-min(xlim), abs(min(ylim))], 'FaceColor', [1, 0, 0, 0.05]);
    end
    h=boxplot(BoxPlotData(1, :), BoxPlotData(2, :), 'labels', BoxPlotCategoryNames);
    set(findobj(gca,'type','line'), 'linew', 2);
    ylim([min(min(lower_adj)) - (abs(min(min(lower_adj))) * 0.1), max(max(up_adj))*1.1]);
    if(min(BoxPlotData(1, :))<0)
        text(mean(xlim)*0.78, max(ylim)*0.9, 'Over Estimation Area', 'FontSize', 30, 'Color', 'g', 'FontWeight', 'bold');
        text(mean(xlim)*0.78, min(ylim)*0.9, 'Under Estimation Area', 'FontSize', 30, 'Color', 'r', 'FontWeight', 'bold');
    end
    
    for i=1:size(Result, 1)
        Result.UpperAdjacent(i)=up_adj(end-i+1, 1);
        Result.LowerAdjacent(i)=lower_adj(end-i+1, 1);
    end
    
    dcm_obj = datacursormode(fig);
    set(dcm_obj,'UpdateFcn',{@BoxPlotDataCursorFunction, Result, BoxPlotCategoryNames});
end




