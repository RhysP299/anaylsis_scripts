function [Data, PrunedPresence, PrunedPresenceReport, OriginalPresenceReport]=RejectIsufficientData(Data, Presence, Print)
    if(~exist('Print', 'var'))
        Print=true;
    end
    if(Print)
        disp(' ')
        disp(' ')
        disp('========================================== Original Presence Report ==========================================')
    end
    OriginalPresenceReport=PresenceReport(Presence, Print);
    

    for i=1:length(OriginalPresenceReport.IndependentParmeters)
        if(~strcmp(OriginalPresenceReport.IndependentParmeters{i}, 'JobID'))
            Indeces = ( sum(OriginalPresenceReport.Count.(OriginalPresenceReport.IndependentParmeters{i})) / ...
                        max(sum(OriginalPresenceReport.Count.(OriginalPresenceReport.IndependentParmeters{i})))*100) <50; % Reject any device that has less than half of others
            Data=DeleteAParticularRecord(Data, OriginalPresenceReport.IndependentParmeters{i}, OriginalPresenceReport.Uniques.(OriginalPresenceReport.IndependentParmeters{i}){1}(Indeces));
        else
            Indeces=sum(diff(OriginalPresenceReport.Count.(OriginalPresenceReport.IndependentParmeters{i})))==1;
            Data=DeleteAParticularRecord(Data, OriginalPresenceReport.IndependentParmeters{i}, OriginalPresenceReport.Uniques.(OriginalPresenceReport.IndependentParmeters{i}){1}(Indeces)); % reject any lens that has one or the pair missing.
        end
    end
    
    if(Print)
        disp(' ')
        disp(' ')
        disp('========================================== Pruned Presence Report ==========================================')
    end
    PrunedPresence=CheckDataPresence(Data, { 'JobID', 'EMapSN', 'Insert', 'Measurment'});
    PrunedPresenceReport=PresenceReport(PrunedPresence, Print);
end

function Data=DeleteAParticularRecord(Data, TargetFiled, TargetValues)
    if(isempty(TargetValues))
        return;
    end
    if(isa(TargetValues, 'char'))
        TargetValues={TargetValues};
    end
    for i=1:length(Data.Properties.VariableNames)
        if(~strcmp(Data.Properties.VariableNames{i}, 'Recorder') && ~strcmp(Data.Properties.VariableNames{i}, 'Device'))
            for j=1:length(TargetValues)
                if(isa(TargetValues, 'double'))
                    Data.(Data.Properties.VariableNames{i}){1}=Data.(Data.Properties.VariableNames{i}){1}(Data.(Data.Properties.VariableNames{i}){1}.(TargetFiled)~=TargetValues(j), :);
                else
                    Data.(Data.Properties.VariableNames{i}){1}=Data.(Data.Properties.VariableNames{i}){1}(~strcmp(Data.(Data.Properties.VariableNames{i}){1}.(TargetFiled), TargetValues{j}), :);
                end
            end
        end
    end
end