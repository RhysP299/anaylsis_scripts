function ComprehensiveBlandAltmanAnalysis(Data, Reference, ANOVAAnalysisResult, CriticalP_Value)
    IndependentVariables=ANOVAAnalysisResult.Properties.VariableNames;
    IndependentVariables(strcmp(IndependentVariables,{'Type'}))=[];
    IndependentVariables(strcmp(IndependentVariables,{'Source'}))=[];
    IndependentVariables(strcmp(IndependentVariables,{'df'}))=[];
    for i=1:length(IndependentVariables)
        if(iscell(Data.OD_RightLens{1}.(IndependentVariables{i})))
            continue;
        end
        
        %% Individual Lenses
        IndIndeces=strcmp(ANOVAAnalysisResult.Type, 'Individual Lenses') & ~strcmp(ANOVAAnalysisResult.Source, 'JobID');
        DependentVariables=ANOVAAnalysisResult.Source(IndIndeces);
        SubData=IndividualLensesCombineData(Data, Reference, IndependentVariables{i}, DependentVariables);
        
        VariationDependentVariables=DependentVariables(ANOVAAnalysisResult.(IndependentVariables{i})(IndIndeces)<=CriticalP_Value);
        figure('Position', [2049 41 2048 1.0368e+03], 'name', horzcat('Bland Altman Analysis for ', IndependentVariables{i}, ' in Individual Lenses'));
        if(isempty(VariationDependentVariables))
            BlandAltmanPlot(SubData.(IndependentVariables{i})(:, 2), SubData.(IndependentVariables{i})(:, 1), horzcat(IndependentVariables{i}, ' in Lenses'));
        else
            for j=1:length(VariationDependentVariables)
                Uniques{j}=unique(SubData.(VariationDependentVariables{j}));
            end
            if(length(Uniques)==1)
                SubPlotDime=SubPlotDimension(length(Uniques{1}));
                for j=1:length(Uniques{1})
                    subplot(SubPlotDime(1), SubPlotDime(2), j);
                    if(iscell(SubData.(VariationDependentVariables{1})))
                        indeces=strcmp(SubData.(VariationDependentVariables{1}), Uniques{1}{j});
                    else
                        indeces=SubData.(VariationDependentVariables{1}) == Uniques{1}(j);
                    end
                    BlandAltmanPlot(SubData.(IndependentVariables{i})(indeces, 2), SubData.(IndependentVariables{i})(indeces, 1), horzcat(IndependentVariables{i}, ' in Lenses - ', VariationDependentVariables{1}, ': ', Uniques{1}{j}));
                end
            else
                % TO DO........... 
            end
        end
        
        %% Pairs:
        if(~isnan(sum(ANOVAAnalysisResult.(IndependentVariables{i})(strcmp(ANOVAAnalysisResult.Type, 'Pairs')))))
            IndIndeces=strcmp(ANOVAAnalysisResult.Type, 'Pairs') & ~strcmp(ANOVAAnalysisResult.Source, 'JobID');
            DependentVariables=ANOVAAnalysisResult.Source(IndIndeces);
            SubData=PairsCombineData(Data, Reference, IndependentVariables{i}, DependentVariables);

            VariationDependentVariables=DependentVariables(ANOVAAnalysisResult.(IndependentVariables{i})(IndIndeces)<=CriticalP_Value);
            figure('Position', [2049 41 2048 1.0368e+03], 'name', horzcat('Bland Altman Analysis for ', IndependentVariables{i}, ' in Pairs'));
            if(isempty(VariationDependentVariables))
                BlandAltmanPlot(SubData.(IndependentVariables{i})(:, 2), SubData.(IndependentVariables{i})(:, 1), horzcat(IndependentVariables{i}, ' in Pairs'));
            else
                for j=1:length(VariationDependentVariables)
                    Uniques{j}=unique(SubData.(VariationDependentVariables{j}));
                end
                if(length(Uniques)==1)
                    SubPlotDime=SubPlotDimension(length(Uniques{1}));
                    for j=1:length(Uniques{1})
                        subplot(SubPlotDime(1), SubPlotDime(2), j);
                        if(iscell(SubData.(VariationDependentVariables{1})))
                            indeces=strcmp(SubData.(VariationDependentVariables{1}), Uniques{1}{j});
                        else
                            indeces=SubData.(VariationDependentVariables{1}) == Uniques{1}(j);
                        end
                        BlandAltmanPlot(SubData.(IndependentVariables{i})(indeces, 2), SubData.(IndependentVariables{i})(indeces, 1), horzcat(IndependentVariables{i}, ' in Pairs - ', VariationDependentVariables{1}, ': ', Uniques{1}{j}));
                    end
                else
                    % TO DO........... 
                end
            end
        end
    end
end

function SubData=IndividualLensesCombineData(Data, Reference, IndependentVariables, DependentVariables)
    JobIDs=unique(Reference.OD_RightLens{1}.JobID);
    SubData=table( zeros(size(Data.OD_RightLens{1}, 1)*2, 1), ...
                   cell(size(Data.OD_RightLens{1}, 1)*2, 1), ...
                   zeros(size(Data.OD_RightLens{1}, 1)*2, 1), ...
                   zeros(size(Data.OD_RightLens{1}, 1)*2, 1), ...
                   zeros(size(Data.OD_RightLens{1}, 1)*2, 2), ...
                   'VariableNames', ...
                   ['JobID'; DependentVariables; IndependentVariables]);
               
    Index=1;
    for i=1:length(JobIDs)
        %% Right Lens:
        SubD=Data.OD_RightLens{1}(Data.OD_RightLens{1}.JobID==JobIDs(i), :);
        SubData(Index:Index+size(SubD, 1)-1, 1:end-1)=SubD(:, ['JobID'; DependentVariables]);
        SubData{Index:Index+size(SubD, 1)-1, end}(:, 1)=zeros(size(SubD, 1), 1) + Reference.OD_RightLens{1}(Reference.OD_RightLens{1}.JobID==JobIDs(i), :).(IndependentVariables);
        SubData{Index:Index+size(SubD, 1)-1, end}(:, 2)=SubD{:, IndependentVariables};
        Index=Index+size(SubD, 1);
        
        %% Left Lens:
        SubD=Data.OS_LeftLens{1}(Data.OS_LeftLens{1}.JobID==JobIDs(i), :);
        SubData(Index:Index+size(SubD, 1)-1, 1:end-1)=SubD(:, ['JobID'; DependentVariables]);
        SubData{Index:Index+size(SubD, 1)-1, end}(:, 1)=zeros(size(SubD, 1), 1) + Reference.OS_LeftLens{1}(Reference.OS_LeftLens{1}.JobID==JobIDs(i), :).(IndependentVariables);
        SubData{Index:Index+size(SubD, 1)-1, end}(:, 2)=SubD{:, IndependentVariables};
        Index=Index+size(SubD, 1);
    end
end


function SubData=PairsCombineData(Data, Reference, IndependentVariables, DependentVariables)
    JobIDs=unique(Reference.Pair{1}.JobID);
    SubData=table( zeros(size(Data.Pair{1}, 1), 1), ...
                   cell(size(Data.Pair{1}, 1), 1), ...
                   zeros(size(Data.Pair{1}, 1), 1), ...
                   zeros(size(Data.Pair{1}, 1), 1), ...
                   zeros(size(Data.Pair{1}, 1), 2), ...
                   'VariableNames', ...
                   ['JobID'; DependentVariables; IndependentVariables]);
               
    Index=1;
    for i=1:length(JobIDs)
        SubD=Data.Pair{1}(Data.Pair{1}.JobID==JobIDs(i), :);
        SubData(Index:Index+size(SubD, 1)-1, 1:end-1)=SubD(:, ['JobID'; DependentVariables]);
        SubData{Index:Index+size(SubD, 1)-1, end}(:, 1)=zeros(size(SubD, 1), 1) + Reference.Pair{1}(Reference.Pair{1}.JobID==JobIDs(i), :).(IndependentVariables);
        SubData{Index:Index+size(SubD, 1)-1, end}(:, 2)=SubD{:, IndependentVariables};
        Index=Index+size(SubD, 1);
    end
end

function SubPlotDime=SubPlotDimension(NumberOfSubplots)
    switch(NumberOfSubplots)
        case 1
            SubPlotDime=[1 1];
        case 2
            SubPlotDime=[2 1];
        case {3, 4}
            SubPlotDime=[2 2];
        case {5, 6}
            SubPlotDime=[3 2];
    end
end


