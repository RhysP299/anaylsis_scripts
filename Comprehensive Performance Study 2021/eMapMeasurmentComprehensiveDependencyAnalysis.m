function [BetweenIndependentParameterDifferenceAnalysisResult, AVOVAResults]=eMapMeasurmentComprehensiveDependencyAnalysis(Data, SignificanceLevel, IndependentVariablesToIgnore, DrawMarginalMeans, DrawCrossTabulationPlots)
    %% Initialization
    DependantVariablesPreferableOrder={'Sphere', 'Cylinder' ,'Axis', 'Add', ...
                                       'HPrismMagnitude',	'HPrismDirection',	...
                                       'VPrismMagnitude', 'VPrismDirection', ...
                                       'HPrism', 'VPrism', 'PassFail'};
    if(~exist('DrawMarginalMeans', 'var'))
        DrawMarginalMeans=false;
    end
    if(~exist('DrawCrossTabulationPlots', 'var'))
        DrawCrossTabulationPlots=false;
    end
    AllDependantVariables={};
    AllIndependantVariables={};
    for i=1:length(Data.Properties.VariableNames)
        if(~strcmp(Data.Properties.VariableNames{i}, 'Recorder') && ~strcmp(Data.Properties.VariableNames{i}, 'Device'))
            for j=1:length(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames)
                if( ~strcmp(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j}, 'JobID') && ...
                    ~strcmp(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j}, 'EMapSN') && ...
                    ~strcmp(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j}, 'Insert') && ...
                    ~strcmp(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j}, 'Measurment') && ...
                    ~strcmp(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j}, 'Recorder') && ...
                    ~strcmp(Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j}, 'Device') )
                    AllDependantVariables{end+1}=Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j};
                else
                    AllIndependantVariables{end+1}=Data.(Data.Properties.VariableNames{i}){1}.Properties.VariableNames{j};
                end
            end
        end
    end
    AllDependantVariables=intersect(DependantVariablesPreferableOrder, unique(AllDependantVariables), 'stable');
    AllIndependantVariables=unique(AllIndependantVariables);
    IndependentVariables=AllIndependantVariables;
    if(exist('IndependentVariablesToIgnore', 'var'))
        for i=1:length(IndependentVariablesToIgnore)
            IndependentVariables(strcmp(IndependentVariables, IndependentVariablesToIgnore{i}))=[];
        end
    end
    IndependentVariables(strcmp(IndependentVariables, 'JobID'))=[];
    BetweenIndependentParameterDifferenceAnalysisResult={};

    VariableTypes=cell(1, length(AllDependantVariables)+3);
    VariableTypes(1:2)={'cell'};
    VariableTypes(3:end)={'double'};
    AVOVAResults=table('Size', [(length(IndependentVariables)+1)*2, length(VariableTypes)], 'VariableTypes', VariableTypes, 'VariableNames', [{'Type', 'Source', 'df'}, AllDependantVariables]);
    
    AVOVAResults{:, strcmp(VariableTypes, 'double')}=nan;
    
    disp(' ')
    disp(' ')
    disp('############################# eMap Measurment Comprehensive Dependency Analysis #############################')
    
    %% Individual Lenses Analysis
    AVOVAResultsStartIndex=1;
    AVOVAResults.Type(AVOVAResultsStartIndex:length(IndependentVariables)+1)={'Individual Lenses'};
    disp('================ Individual Lenses =================')
    for i=1:length(AllDependantVariables)
        if(sum(strcmp(Data.OD_RightLens{1}.Properties.VariableNames, AllDependantVariables{i}))==0)
            continue;
        end
        if(isa(Data.OD_RightLens{1}.(AllDependantVariables{i}), 'double'))
            TempResult=ANOVA(ANOVADataExtract(Data, IndependentVariables, AllDependantVariables{i}), SignificanceLevel);
            for j=1:size(TempResult.Table, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult.Table{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 3};
            end
            SignificantlyDifferentIndependantVariables={};
            SignificantGroups='';
            TargeIndependantVariablesToAverageAcross=AllIndependantVariables;
            for j=1:size(TempResult.Table, 1)
                if(TempResult.Table{j, 3}<=SignificanceLevel)
                    TargeIndependantVariablesToAverageAcross(strcmp(TargeIndependantVariablesToAverageAcross, TempResult.Table{j, 1}))=[];
                    if(~strcmp(TempResult.Table{j, 1}, 'JobID'))
                        SignificantlyDifferentIndependantVariables{end+1}=TempResult.Table{j, 1};
                        if(isempty(SignificantGroups))
                            SignificantGroups=horzcat('Between_', TempResult.Table{j, 1});
                        else
                            SignificantGroups=horzcat(SignificantGroups, '_&_', TempResult.Table{j, 1});
                        end
                    end
                end
            end
            if(~isempty(SignificantlyDifferentIndependantVariables))
                SignificantGroups=horzcat(SignificantGroups, '_for_Lenses');
                SubData=Data;
                SubData.OD_RightLens{1}=ExtrcatDataByGroupAveraging(Data.OD_RightLens{1}, TargeIndependantVariablesToAverageAcross);
                SubData.OS_LeftLens{1}=ExtrcatDataByGroupAveraging(Data.OS_LeftLens{1}, TargeIndependantVariablesToAverageAcross);
                BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(SubData, SignificantlyDifferentIndependantVariables, AllDependantVariables{i}), SignificantGroups);
                BetweenIndependentParameterDifferenceAnalysisResult{end}.Statistics=TempResult.Statistics;
            else
                TempVariables=AllIndependantVariables(~strcmp(AllIndependantVariables, 'JobID'));
                SubDataExtracted=ANOVADataExtract(Data, TempVariables, AllDependantVariables{i});
                for l=1:length(TempVariables)-1
                    SubDataExtracted=removevars(SubDataExtracted,TempVariables{l});
                end
                SubDataExtracted.Properties.VariableNames{1}='Attempt';
                SubDataExtracted{:,1}=1;
                BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(SubDataExtracted, 'All_eMaps_Combined_for_Lenses');
                BetweenIndependentParameterDifferenceAnalysisResult{end}.Statistics=[];
            end
        else
            TempResult=MultinomialLogisticRegression(MultinomialLogisticRegressionDataExtract(Data, IndependentVariables, AllDependantVariables{i}), SignificanceLevel);
            for j=1:size(TempResult, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult{j, 3};
            end
            SignificantlyDifferentIndependantVariables={};
            SignificantGroups='';
            TargeIndependantVariablesToAverageAcross=AllIndependantVariables;
            for j=1:size(TempResult, 1)
                if(TempResult{j, 3}<=SignificanceLevel)
                    TargeIndependantVariablesToAverageAcross(strcmp(TargeIndependantVariablesToAverageAcross, TempResult{j, 1}))=[];
                    if(~strcmp(TempResult{j, 1}, 'JobID'))
                        SignificantlyDifferentIndependantVariables{end+1}=TempResult{j, 1};
                        if(isempty(SignificantGroups))
                            SignificantGroups=horzcat('Between_', TempResult{j, 1});
                        else
                            SignificantGroups=horzcat(SignificantGroups, '_&_', TempResult{j, 1});
                        end
                    end
                end
            end
            if(~isempty(SignificantlyDifferentIndependantVariables))
                SignificantGroups=horzcat(SignificantGroups, '_for_Lenses');
                SubData=Data;
                SubData.OD_RightLens{1}=ExtrcatDataByGroupAveraging(Data.OD_RightLens{1}, TargeIndependantVariablesToAverageAcross);
                SubData.OS_LeftLens{1}=ExtrcatDataByGroupAveraging(Data.OS_LeftLens{1}, TargeIndependantVariablesToAverageAcross);
                BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(SubData, SignificantlyDifferentIndependantVariables, AllDependantVariables{i}), SignificantGroups);
            end 
        end
    end
    
    %% Pairs Analysis
    AVOVAResultsStartIndex=length(IndependentVariables)+2;
    AVOVAResults.Type(AVOVAResultsStartIndex:end)={'Pairs'};
    disp('================ Pairs =================')
    for i=1:length(AllDependantVariables)
        if(sum(strcmp(Data.Pair{1}.Properties.VariableNames, AllDependantVariables{i}))==0)
            continue;
        end
        if(isa(Data.Pair{1}.(AllDependantVariables{i}), 'double'))
            TempResult=ANOVA(ANOVADataExtract(Data, IndependentVariables, AllDependantVariables{i}, true, true), SignificanceLevel);
            for j=1:size(TempResult.Table, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult.Table{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult.Table{j, 3};
            end
            SignificantlyDifferentIndependantVariables={};
            SignificantGroups='';
            TargeIndependantVariablesToAverageAcross=AllIndependantVariables;
            for j=1:size(TempResult.Table, 1)
                if(TempResult.Table{j, 3}<=SignificanceLevel)
                    TargeIndependantVariablesToAverageAcross(strcmp(TargeIndependantVariablesToAverageAcross, TempResult.Table{j, 1}))=[];
                    if(~strcmp(TempResult.Table{j, 1}, 'JobID'))
                        SignificantlyDifferentIndependantVariables{end+1}=TempResult.Table{j, 1};
                        if(isempty(SignificantGroups))
                            SignificantGroups=horzcat('Between_', TempResult.Table{j, 1});
                        else
                            SignificantGroups=horzcat(SignificantGroups, '_&_', TempResult.Table{j, 1});
                        end
                    end
                end
            end
            if(~isempty(SignificantlyDifferentIndependantVariables))
                SignificantGroups=horzcat(SignificantGroups, '_for_Pairs');
                SubData=Data;
                SubData.Pair{1}=ExtrcatDataByGroupAveraging(Data.Pair{1}, TargeIndependantVariablesToAverageAcross);
                BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(ANOVADataExtract(SubData, SignificantlyDifferentIndependantVariables, AllDependantVariables{i}, true, true), SignificantGroups);
                BetweenIndependentParameterDifferenceAnalysisResult{end}.Statistics=TempResult.Statistics;
            else
                TempVariables=AllIndependantVariables(~strcmp(AllIndependantVariables, 'JobID'));
                SubDataExtracted=ANOVADataExtract(Data, TempVariables, AllDependantVariables{i}, true, true);
                for l=1:length(TempVariables)-1
                    SubDataExtracted=removevars(SubDataExtracted,TempVariables{l});
                end
                SubDataExtracted.Properties.VariableNames{1}='Attempt';
                SubDataExtracted{:,1}=1;
                BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Scaler(SubDataExtracted, 'All_eMaps_Combined_for_Pairs');
                BetweenIndependentParameterDifferenceAnalysisResult{end}.Statistics=[];
            end
        else
            TempResult=MultinomialLogisticRegression(MultinomialLogisticRegressionDataExtract(Data, IndependentVariables, AllDependantVariables{i}, true, true), SignificanceLevel);
            for j=1:size(TempResult, 1)
                AVOVAResults.Source{AVOVAResultsStartIndex+j-1}=TempResult{j, 1};
                AVOVAResults.df(AVOVAResultsStartIndex+j-1)=TempResult{j, 2};
                AVOVAResults.(AllDependantVariables{i})(AVOVAResultsStartIndex+j-1)=TempResult{j, 3};
            end
            SignificantlyDifferentIndependantVariables={};
            SignificantGroups='';
            TargeIndependantVariablesToAverageAcross=AllIndependantVariables;
            for j=1:size(TempResult, 1)
                if(TempResult{j, 3}<=SignificanceLevel)
                    TargeIndependantVariablesToAverageAcross(strcmp(TargeIndependantVariablesToAverageAcross, TempResult{j, 1}))=[];
                    if(~strcmp(TempResult{j, 1}, 'JobID'))
                        SignificantlyDifferentIndependantVariables{end+1}=TempResult{j, 1};
                        if(isempty(SignificantGroups))
                            SignificantGroups=horzcat('Between_', TempResult{j, 1});
                        else
                            SignificantGroups=horzcat(SignificantGroups, '_&_', TempResult{j, 1});
                        end
                    end
                end
            end
            if(~isempty(SignificantlyDifferentIndependantVariables))
                SignificantGroups=horzcat(SignificantGroups, '_for_Pairs');
                SubData=Data;
                SubData.Pair{1}=ExtrcatDataByGroupAveraging(Data.Pair{1}, TargeIndependantVariablesToAverageAcross);
                BetweenIndependentParameterDifferenceAnalysisResult{end+1}=BetweenIndependentParameterDifferenceAnalysis_Categorical(MultinomialLogisticRegressionDataExtract(SubData, SignificantlyDifferentIndependantVariables, AllDependantVariables{i}, true, true), SignificantGroups);
            end 
        end
    end
    
    %% Between Independent Parameter Difference Analysis
    BetweenIndependentParameterDifferenceAnalysisPlot(BetweenIndependentParameterDifferenceAnalysisResult, DrawMarginalMeans, DrawCrossTabulationPlots);
    disp('############################# eMap Measurment Comprehensive Dependency Analysis #############################')
    disp(' ')
    disp(' ')
end