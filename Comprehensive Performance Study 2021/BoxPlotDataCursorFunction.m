function txt = BoxPlotDataCursorFunction(~,event_obj, Result, BoxPlotCategoryNames)
    % Customizes text of data tips
    pos = round(get(event_obj,'Position'));
    DataCursors=findall(gcf, 'Type', 'hggroup');
    Delete=[];
    for i=1:length(DataCursors)
        if(~isempty(DataCursors(i).Tag))
            Delete(end+1)=i;
        end
    end
    DataCursors(Delete)=[];
    PValue=nan;
    if(GetDataCursorTTest())
        while(length(DataCursors)>2)
            delete(DataCursors(2));
            DataCursors(2)=[];
        end
        if(length(DataCursors)==2)
            Temp1=Result.RawData{round(DataCursors(1).Position(1))};
            Temp2=Result.RawData{round(DataCursors(2).Position(1))};
            [~, PValue]=ttest2(Temp1{:, end}, Temp2{:, end});
        end
    end
    
   
    if(isstruct(Result))
        RawData=Result.RawData;
    else
        RawData=Result.RawData{pos(1)};
    end
    MainRangeCoverage=RawData{:,end};
    MainRangeCoverage=MainRangeCoverage((MainRangeCoverage>=Result.Percentile25th(pos(1))) & (MainRangeCoverage<=Result.Percentile75th(pos(1))));
    MainRangeCoverage=length(MainRangeCoverage)/length(RawData{:,end})*100;
    
    txt={['Mean: ', num2str(Result.Mean(pos(1)), '%2.2f')], ...
         ['STD: ', num2str(Result.STD(pos(1)), '%2.2f')], ...
         ['Median: ', num2str(Result.Median(pos(1)), '%2.2f')], ...
         ['Lower Adjacent: ', num2str(Result.LowerAdjacent(pos(1)), '%2.2f')], ...
         ['25th Percentile: ', num2str(Result.Percentile25th(pos(1)), '%2.2f')], ...
         ['75th Percentile: ', num2str(Result.Percentile75th(pos(1)), '%2.2f')], ...
         ['Upper Adjacent: ', num2str(Result.UpperAdjacent(pos(1)), '%2.2f')], ...
         ['Number of Samples: ', num2str(size(RawData, 1), '%2.0f')], ...
         ['Main Range Coverage: ', num2str(round(MainRangeCoverage), '%2.0f'), '%'], ...
         ['Root Mean Square Error: ', num2str(Result.RMSE(pos(1)), '%2.3f')], ...
         ['Comopare to Zero TTest P-Value: ', num2str(Result.DifferentFromZeroPValue(pos(1)), '%2.3f')]};
     if(~isnan(PValue))
         txt{end+1}='------------------------------------------------------------------------';
         txt{end+1}=['"', BoxPlotCategoryNames{round(DataCursors(1).Position(1))}, '"   VS.   "', BoxPlotCategoryNames{round(DataCursors(2).Position(1))}, '"'];
         txt{end+1}=['TTest PValue: ', num2str(PValue, '%2.3f')];
         txt{end+1}=['Absolute Difference Mean: ', num2str(abs(Result.Mean(round(DataCursors(1).Position(1)))- Result.Mean(round(DataCursors(2).Position(1)))), '%2.2f')];
         txt{end+1}=['Absolute Difference Median: ', num2str(abs(Result.Median(round(DataCursors(1).Position(1)))- Result.Median(round(DataCursors(2).Position(1)))), '%2.2f')];
         txt{end+1}=['Absolute Difference 25th Percentile: ', num2str(abs(Result.Percentile25th(round(DataCursors(1).Position(1)))- Result.Percentile25th(round(DataCursors(2).Position(1)))), '%2.2f')];
         txt{end+1}=['Absolute Difference 75th Percentile: ', num2str(abs(Result.Percentile75th(round(DataCursors(1).Position(1)))- Result.Percentile75th(round(DataCursors(2).Position(1)))), '%2.2f')];
     end
end