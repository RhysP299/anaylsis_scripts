function Value=GetDataCursorTTest()
    global ConductDataCursorTTest
    if(isempty(ConductDataCursorTTest))
        Value=false;
    else
        Value=ConductDataCursorTTest;
    end
end