function Mean=MeanJobAccrossTables(SubData, Lens, JobID)
    TargetSubData=SubData.(Lens);
    Mean=TargetSubData{1}(TargetSubData{1}.JobID == JobID, :);
    for i=2:length(TargetSubData)
        temp=TargetSubData{i}(TargetSubData{i}.JobID == JobID, :);
        for j=1:length(Mean.Properties.VariableNames)
            if(~strcmp(Mean.Properties.VariableNames{j}, 'JobID'))
                if(isa(Mean.(Mean.Properties.VariableNames{j}), 'double'))
                    Mean.(Mean.Properties.VariableNames{j}) = Mean.(Mean.Properties.VariableNames{j}) + temp.(Mean.Properties.VariableNames{j});
                else
                    if(~strcmp(temp.(Mean.Properties.VariableNames{j}), ''))
                        Mean.(Mean.Properties.VariableNames{j}){1,size(Mean.(Mean.Properties.VariableNames{j}),2)+1}=temp.(Mean.Properties.VariableNames{j}){1};
                    end
                end
            end
        end
    end
    for j=1:length(Mean.Properties.VariableNames)
        if(~strcmp(Mean.Properties.VariableNames{j}, 'JobID'))
            if(isa(Mean.(Mean.Properties.VariableNames{j}), 'double'))
                Mean.(Mean.Properties.VariableNames{j}) = Mean.(Mean.Properties.VariableNames{j}) / length(TargetSubData);
            else
                if(length(Mean.(Mean.Properties.VariableNames{j}))>1)
                    UniqueSelections=unique(Mean.(Mean.Properties.VariableNames{j}));
                    UniqueSelectionsCount=zeros(size(UniqueSelections));
                    for i=1:length(UniqueSelections)
                        UniqueSelectionsCount(i)=sum(strcmp(Mean.(Mean.Properties.VariableNames{j}), UniqueSelections{i}));
                    end
                    if( (length(UniqueSelectionsCount)>1) && (range(UniqueSelectionsCount)==0) )
                        Mean.(Mean.Properties.VariableNames{j})={'TIE'};
                    else
                        [~, ind]=max(UniqueSelectionsCount);
                        Mean.(Mean.Properties.VariableNames{j})={UniqueSelections{ind}};
                    end
                end
            end
        end
    end
end