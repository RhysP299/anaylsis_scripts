function SetDataCursorTTest(Conduct)
    if(~exist('Conduct', 'var'))
        Conduct=true;
    end
    global ConductDataCursorTTest
    ConductDataCursorTTest = Conduct;
end