function Data=PrismVectorConstruction(Data, Lens)
    for i=1:size(Data)
        Data.HPrism(i)=PrismVectorDirectionSign(Lens, 'Horizontal', Data.HPrismDirection{i}) * Data.HPrismMagnitude(i);
        Data.VPrism(i)=PrismVectorDirectionSign(Lens, 'Vertical', Data.VPrismDirection{i}) * Data.VPrismMagnitude(i);
    end
end