function SubData=ExtrcatDataByGroupAveraging(Data, TargeIndependantVariablesToAverageAcross, TargetDependantVariablesToAverage)
    %% Pre-Cehcks
    AllDependantVariables={};
    AllIndependantVariables={};
    AllDependantVariablesType={};
    AllIndependantVariablesType={};
    for i=1:length(Data.Properties.VariableNames)
        if( ~strcmp(Data.Properties.VariableNames{i}, 'JobID') && ...
            ~strcmp(Data.Properties.VariableNames{i}, 'EMapSN') && ...
            ~strcmp(Data.Properties.VariableNames{i}, 'Insert') && ...
            ~strcmp(Data.Properties.VariableNames{i}, 'Measurment') && ...
            ~strcmp(Data.Properties.VariableNames{i}, 'Recorder') && ...
            ~strcmp(Data.Properties.VariableNames{i}, 'Device') )
            AllDependantVariables{end+1}=Data.Properties.VariableNames{i};
            AllDependantVariablesType{end+1}=class(Data.(AllDependantVariables{end}));
        else
            AllIndependantVariables{end+1}=Data.Properties.VariableNames{i};
            AllIndependantVariablesType{end+1}=class(Data.(AllIndependantVariables{end}));
        end
    end
    if(~exist('TargeIndependantVariablesToAverageAcross', 'var'))
        TargeIndependantVariablesToAverageAcross=AllIndependantVariables;
        TargeIndependantVariablesToAverageAcross(strcmp(TargeIndependantVariablesToAverageAcross, 'JobID'))=[];
    end
    if(~exist('TargetDependantVariablesToAverage', 'var'))
        TargetDependantVariablesToAverage=AllDependantVariables;
    end
    
    AveragingInfo.VariablesNames=setdiff(AllIndependantVariables, TargeIndependantVariablesToAverageAcross);
    AveragingInfo.VariablesNames(strcmp(AveragingInfo.VariablesNames, 'JobID'))=[];
    AveragingInfo.VariablesNames{end+1}='JobID';
    AveragingInfo.Uniques=cell(length(AveragingInfo.VariablesNames), 1);
    AveragingInfo.VariableTypes=cell(length(AveragingInfo.VariablesNames), 1);
    SubDataSize=1;
    SubDataVariableNames={AveragingInfo.VariablesNames, 'NumberOfRecords', AllDependantVariables};
    SubDataVariableNames=cat(2, SubDataVariableNames{:});
    SubDataVariableTypes=cell(1, length(AveragingInfo.VariablesNames));
    for i=1:length(AveragingInfo.VariablesNames)
        SubDataVariableTypes{i}=AllIndependantVariablesType{strcmp(AllIndependantVariables, AveragingInfo.VariablesNames{i})};
        AveragingInfo.VariableTypes{i}=SubDataVariableTypes{i};
        AveragingInfo.Uniques{i}=unique(Data.(AveragingInfo.VariablesNames{i}));
        SubDataSize=SubDataSize*length(AveragingInfo.Uniques{i});
    end
    SubDataVariableTypes={SubDataVariableTypes, 'double', AllDependantVariablesType};
    SubDataVariableTypes=cat(2, SubDataVariableTypes{:});
    SubData=table('Size', [SubDataSize, length(SubDataVariableNames)], 'VariableTypes', SubDataVariableTypes, 'VariableNames', SubDataVariableNames);
    switch length(AveragingInfo.VariablesNames)
        case 1
            SubData=AverageDataBy_1(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage);
        case 2
            SubData=AverageDataBy_2(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage);
        case 3 
            SubData=AverageDataBy_3(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage);
        case 4
            SubData=AverageDataBy_4(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage);
    end
end


function SubData=AverageDataBy_1(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage)
    if(length(AveragingInfo.VariablesNames)~=1)
        error('Wrong Number of Independant Variables!');
    end
    SubDataIndex=1;
    for i=1:length(AveragingInfo.Uniques{1})
        Indeces=true(size(Data, 1), 1);
        if(strcmp(AveragingInfo.VariableTypes{1}, 'double'))
            Indeces=Indeces & Data.(AveragingInfo.VariablesNames{1})==AveragingInfo.Uniques{1}(i);
            SubData.(AveragingInfo.VariablesNames{1})(SubDataIndex)=AveragingInfo.Uniques{1}(i);
        else
            Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{1}), AveragingInfo.Uniques{1}{i});
            SubData.(AveragingInfo.VariablesNames{1}){SubDataIndex}=AveragingInfo.Uniques{1}{i};
        end
        
        Temp=Data(Indeces, :);
        if(~isempty(Temp))
            SubData.NumberOfRecords(SubDataIndex)=size(Temp, 1);
            for TIndex=1:length(TargetDependantVariablesToAverage)
                SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=Mean(Temp.(TargetDependantVariablesToAverage{TIndex}));
            end
        else
            for TIndex=1:length(TargetDependantVariablesToAverage)
                if(isa(SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex), 'double'))
                    SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=nan;
                else
                    SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)={'NOT_MEASURED'};
                end
            end
        end
        SubDataIndex=SubDataIndex+1;
    end
end

function SubData=AverageDataBy_2(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage)
    if(length(AveragingInfo.VariablesNames)~=2)
        error('Wrong Number of Independant Variables!');
    end
    SubDataIndex=1;
    for i=1:length(AveragingInfo.Uniques{1})
        for j=1:length(AveragingInfo.Uniques{2})
            Indeces=true(size(Data, 1), 1);
            if(strcmp(AveragingInfo.VariableTypes{1}, 'double'))
                Indeces=Indeces & Data.(AveragingInfo.VariablesNames{1})==AveragingInfo.Uniques{1}(i);
                SubData.(AveragingInfo.VariablesNames{1})(SubDataIndex)=AveragingInfo.Uniques{1}(i);
            else
                Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{1}), AveragingInfo.Uniques{1}{i});
                SubData.(AveragingInfo.VariablesNames{1}){SubDataIndex}=AveragingInfo.Uniques{1}{i};
            end
            if(strcmp(AveragingInfo.VariableTypes{2}, 'double'))
                Indeces=Indeces & Data.(AveragingInfo.VariablesNames{2})==AveragingInfo.Uniques{2}(j);
                SubData.(AveragingInfo.VariablesNames{2})(SubDataIndex)=AveragingInfo.Uniques{2}(j);
            else
                Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{2}), AveragingInfo.Uniques{2}{j});
                SubData.(AveragingInfo.VariablesNames{2}){SubDataIndex}=AveragingInfo.Uniques{2}{j};
            end

            Temp=Data(Indeces, :);
            if(~isempty(Temp))
                SubData.NumberOfRecords(SubDataIndex)=size(Temp, 1);
                for TIndex=1:length(TargetDependantVariablesToAverage)
                    SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=Mean(Temp.(TargetDependantVariablesToAverage{TIndex}));
                end
            else
                for TIndex=1:length(TargetDependantVariablesToAverage)
                    if(isa(SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex), 'double'))
                        SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=nan;
                    else
                        SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)={'NOT_MEASURED'};
                    end
                end
            end
            SubDataIndex=SubDataIndex+1;
        end
    end
end

function SubData=AverageDataBy_3(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage)
    if(length(AveragingInfo.VariablesNames)~=3)
        error('Wrong Number of Independant Variables!');
    end
    SubDataIndex=1;
    for i=1:length(AveragingInfo.Uniques{1})
        for j=1:length(AveragingInfo.Uniques{2})
            for k=1:length(AveragingInfo.Uniques{3})
                Indeces=true(size(Data, 1), 1);
                if(strcmp(AveragingInfo.VariableTypes{1}, 'double'))
                    Indeces=Indeces & Data.(AveragingInfo.VariablesNames{1})==AveragingInfo.Uniques{1}(i);
                    SubData.(AveragingInfo.VariablesNames{1})(SubDataIndex)=AveragingInfo.Uniques{1}(i);
                else
                    Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{1}), AveragingInfo.Uniques{1}{i});
                    SubData.(AveragingInfo.VariablesNames{1}){SubDataIndex}=AveragingInfo.Uniques{1}{i};
                end
                if(strcmp(AveragingInfo.VariableTypes{2}, 'double'))
                    Indeces=Indeces & Data.(AveragingInfo.VariablesNames{2})==AveragingInfo.Uniques{2}(j);
                    SubData.(AveragingInfo.VariablesNames{2})(SubDataIndex)=AveragingInfo.Uniques{2}(j);
                else
                    Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{2}), AveragingInfo.Uniques{2}{j});
                    SubData.(AveragingInfo.VariablesNames{2}){SubDataIndex}=AveragingInfo.Uniques{2}{j};
                end
                if(strcmp(AveragingInfo.VariableTypes{3}, 'double'))
                    Indeces=Indeces & Data.(AveragingInfo.VariablesNames{3})==AveragingInfo.Uniques{3}(k);
                    SubData.(AveragingInfo.VariablesNames{3})(SubDataIndex)=AveragingInfo.Uniques{3}(k);
                else
                    Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{3}), AveragingInfo.Uniques{3}{k});
                    SubData.(AveragingInfo.VariablesNames{3}){SubDataIndex}=AveragingInfo.Uniques{3}{k};
                end

                Temp=Data(Indeces, :);
                if(~isempty(Temp))
                    SubData.NumberOfRecords(SubDataIndex)=size(Temp, 1);
                    for TIndex=1:length(TargetDependantVariablesToAverage)
                        SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=Mean(Temp.(TargetDependantVariablesToAverage{TIndex}));
                    end
                else
                    for TIndex=1:length(TargetDependantVariablesToAverage)
                        if(isa(SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex), 'double'))
                            SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=nan;
                        else
                            SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)={'NOT_MEASURED'};
                        end
                    end
                end
                SubDataIndex=SubDataIndex+1;
            end
        end
    end
end

function SubData=AverageDataBy_4(Data, AveragingInfo, SubData, TargetDependantVariablesToAverage)
    if(length(AveragingInfo.VariablesNames)~=4)
        error('Wrong Number of Independant Variables!');
    end
    SubDataIndex=1;
    for i=1:length(AveragingInfo.Uniques{1})
        for j=1:length(AveragingInfo.Uniques{2})
            for k=1:length(AveragingInfo.Uniques{3})
                for l=1:length(AveragingInfo.Uniques{4})
                    Indeces=true(size(Data, 1), 1);
                    if(strcmp(AveragingInfo.VariableTypes{1}, 'double'))
                        Indeces=Indeces & Data.(AveragingInfo.VariablesNames{1})==AveragingInfo.Uniques{1}(i);
                        SubData.(AveragingInfo.VariablesNames{1})(SubDataIndex)=AveragingInfo.Uniques{1}(i);
                    else
                        Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{1}), AveragingInfo.Uniques{1}{i});
                        SubData.(AveragingInfo.VariablesNames{1}){SubDataIndex}=AveragingInfo.Uniques{1}{i};
                    end
                    if(strcmp(AveragingInfo.VariableTypes{2}, 'double'))
                        Indeces=Indeces & Data.(AveragingInfo.VariablesNames{2})==AveragingInfo.Uniques{2}(j);
                        SubData.(AveragingInfo.VariablesNames{2})(SubDataIndex)=AveragingInfo.Uniques{2}(j);
                    else
                        Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{2}), AveragingInfo.Uniques{2}{j});
                        SubData.(AveragingInfo.VariablesNames{2}){SubDataIndex}=AveragingInfo.Uniques{2}{j};
                    end
                    if(strcmp(AveragingInfo.VariableTypes{3}, 'double'))
                        Indeces=Indeces & Data.(AveragingInfo.VariablesNames{3})==AveragingInfo.Uniques{3}(k);
                        SubData.(AveragingInfo.VariablesNames{3})(SubDataIndex)=AveragingInfo.Uniques{3}(k);
                    else
                        Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{3}), AveragingInfo.Uniques{3}{k});
                        SubData.(AveragingInfo.VariablesNames{3}){SubDataIndex}=AveragingInfo.Uniques{3}{k};
                    end
                    if(strcmp(AveragingInfo.VariableTypes{4}, 'double'))
                        Indeces=Indeces & Data.(AveragingInfo.VariablesNames{4})==AveragingInfo.Uniques{4}(l);
                        SubData.(AveragingInfo.VariablesNames{4})(SubDataIndex)=AveragingInfo.Uniques{4}(l);
                    else
                        Indeces=Indeces & strcmp(Data.(AveragingInfo.VariablesNames{4}), AveragingInfo.Uniques{4}{l});
                        SubData.(AveragingInfo.VariablesNames{4}){SubDataIndex}=AveragingInfo.Uniques{4}{l};
                    end

                    Temp=Data(Indeces, :);
                    if(~isempty(Temp))
                        SubData.NumberOfRecords(SubDataIndex)=size(Temp, 1);
                        for TIndex=1:length(TargetDependantVariablesToAverage)
                            SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=Mean(Temp.(TargetDependantVariablesToAverage{TIndex}));
                        end
                    else
                        for TIndex=1:length(TargetDependantVariablesToAverage)
                            if(isa(SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex), 'double'))
                                SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)=nan;
                            else
                                SubData.(TargetDependantVariablesToAverage{TIndex})(SubDataIndex)={'NOT_MEASURED'};
                            end
                        end
                    end
                    SubDataIndex=SubDataIndex+1;
                end
            end
        end
    end
end


function Mean=Mean(Data)
    if(isa(Data, 'double'))
        Mean=mean(Data);
    else
        Unique=unique(Data);
        UniqueCount=zeros(length(Unique), 1);
        for i=1:length(Unique)
            UniqueCount(i)=sum(strcmp(Data, Unique{i}));
        end
        
        if( (length(UniqueCount)>1) && (range(UniqueCount)==0) )
            Mean={'TIE'};
        else
            [~, ind]=max(UniqueCount);
            Mean={Unique{ind}};
        end
    end
end




