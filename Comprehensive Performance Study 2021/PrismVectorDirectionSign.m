function Sign=PrismVectorDirectionSign(Lens, Axes, Direction)
    if(strcmp(Axes, 'Vertical'))
        if(strcmp(Direction, 'UP'))
            Sign=1;
        else
            Sign=-1;
        end
    else
        if(strcmp(Lens, 'Left')) %% Left Lens
            if(strcmp(Direction, 'OUT'))
                Sign=1;
            else
                Sign=-1;
            end
        else                     %% Right Lens or Pair
            if(strcmp(Direction, 'IN'))
                Sign=1;
            else
                Sign=-1;
            end
        end
    end
end