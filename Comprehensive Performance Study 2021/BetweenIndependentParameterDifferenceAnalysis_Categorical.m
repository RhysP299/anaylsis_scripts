function Result=BetweenIndependentParameterDifferenceAnalysis_Categorical(Data, Label)
    DeleteColumns=[];
    for i=1:size(Data,2)
        if(size(unique(Data(:,i)), 1)==1)
            DeleteColumns=[DeleteColumns,i];
        end
    end
    Data(:,DeleteColumns)=[];
    Groups=Data.Properties.VariableNames(1:end-1);
    Groups(strcmp(Groups,'JobID'))=[];
    for j=1:size(Groups, 2)
        UniqueCategories=unique(Data.(Groups{1, j}));
        for i=1:length(UniqueCategories)
            if(isa(UniqueCategories,'double'))
                Groups{i+1, j}=UniqueCategories(i);
            else
                Groups{i+1, j}=UniqueCategories{i};
            end        
        end
    end
    Result.RawTable=Data;
    CrossTabulationTableDimension=zeros(1, size(Groups, 2));
    for j=1:length(CrossTabulationTableDimension)
        CrossTabulationTableDimension(j)=length(Groups(2:end, j));
    end
    switch(length(CrossTabulationTableDimension))
        case 1
            Result.CrossTabulationTable=cell(CrossTabulationTableDimension+1, 2);
        case 2
            Result.CrossTabulationTable=cell(CrossTabulationTableDimension(1)+1, CrossTabulationTableDimension(2)+1);
    end
    
    Result.GroupsComparisonTable=cell(CrossTabulationTableDimension+1, CrossTabulationTableDimension+1);
    for i=2:CrossTabulationTableDimension+1
        for j=2:CrossTabulationTableDimension+1
            Result.GroupsComparisonTable{i, j}=1;
        end
    end

    Result.CrossTabulationTable{1, 1}=horzcat(strrep(Label,'_',' '), ' in ', Data.Properties.VariableNames{end});
    Result.GroupsComparisonTable{1, 1}=horzcat('Similarity Level ', Result.CrossTabulationTable{1, 1});
    for i=2:size(Result.CrossTabulationTable, 1)
        if(isa(Groups{i, 1}, 'double'))
            Result.CrossTabulationTable{i, 1}=num2str(Groups{i, 1});
        else
            Result.CrossTabulationTable{i, 1}=Groups{i, 1};
        end
        Result.GroupsComparisonTable{i, 1}=Result.CrossTabulationTable{i, 1};
        Result.GroupsComparisonTable{1, i}=Result.CrossTabulationTable{i, 1};
        for j=2:size(Result.CrossTabulationTable, 2)
            Indeces=true(size(Data, 1), 1);
            for k=1:size(Groups, 2)
                if(isa(Data.(Groups{1, 1})(end), 'double'))
                    Indeces=Indeces & (Data.(Groups{1, k})==Groups{i, k});
                else
                    Indeces=Indeces & (strcmp(Data.(Groups{1, k}), Groups{i, k}));
                end
                if(size(Groups, 2)>1)
                    Result.CrossTabulationTable{1, j}=Groups{j, k};
                end
            end
            temp=cellstr(Data{Indeces, end});
            if((sum(strcmp(temp,'Pass'))>0) || (sum(strcmp(temp,'Failed'))>0))
                if(sum(strcmp(temp,'Pass'))>0)
                    Result.CrossTabulationTable{i, j}=horzcat('Passed ', num2str(sum(strcmp(temp,'Pass'))/length(temp)*100,'%2.2f'), '% for ',num2str(size(temp, 1)), ' jobs,');
                else
                    Result.CrossTabulationTable{i, j}=horzcat('Passed NONE for ',num2str(size(temp, 1)),' jobs,');
                end
            elseif((sum(strcmp(temp,'IN'))>0) || (sum(strcmp(temp,'OUT'))>0))
                if(sum(strcmp(temp,'IN'))>0)
                    Result.CrossTabulationTable{i, j}=horzcat(num2str(sum(strcmp(temp,'IN'))/length(temp)*100,'%2.2f'), '% labeled IN for ',num2str(size(temp, 1)), ' jobs,');
                else
                    Result.CrossTabulationTable{i, j}=horzcat('NONE labeled IN for ',num2str(size(temp, 1)),' jobs,');
                end
            elseif((sum(strcmp(temp,'UP'))>0) || (sum(strcmp(temp,'DN'))>0))
                if(sum(strcmp(temp,'UP'))>0)
                    Result.CrossTabulationTable{i, j}=horzcat(num2str(sum(strcmp(temp,'UP'))/length(temp)*100,'%2.2f'), '% labeled UP for ',num2str(size(temp, 1)), ' jobs,');
                else
                    Result.CrossTabulationTable{i, j}=horzcat('NONE labeled UP for ',num2str(size(temp, 1)),' jobs,');
                end
            end
            if(sum(strcmp(temp,'NOT_MEASURED')))
                Result.CrossTabulationTable{i, j}=horzcat(Result.CrossTabulationTable{i, j}, ' ', num2str(sum(~strcmp(temp,'NOT_MEASURED'))/length(temp)*100,'%2.2f'), '% were measured in this group.');
            else
                if(isempty(Result.CrossTabulationTable{i, j}))
                    Result.CrossTabulationTable{i, j}='ALL were measured in this group.';
                else
                    Result.CrossTabulationTable{i, j}=horzcat(Result.CrossTabulationTable{i, j}, ' while ALL were measured in this group.');
                end
            end
        end
    end

    Comparisons=[];
    for MR1=2:size(Groups, 1) %% Main Row 1
        for TR1=2:size(Groups, 1) %% Target Row 1
            if(size(Groups, 2)==1)
                if(isa(Groups{MR1, 1}, 'double'))
                    Info1=num2str(Groups{MR1, 1});
                else
                    Info1=Groups{MR1, 1};
                end
            elseif(size(Groups, 2)==2)
                if(isa(Groups{MR1, 1}, 'double'))
                    Info1=num2str(Groups{MR1, 1});
                else
                    Info1=Groups{MR1, 1};
                end
                if(isa(Groups{TR1, 2}, 'double'))
                    Info1=horzcat(Info1, ' With ', num2str(Groups{TR1, 2}));
                else
                    Info1=horzcat(Info1, ' With ', Groups{TR1, 2});
                end
            end
            for MR2=2:size(Groups, 1) %% Main Row 2
                for TR2=2:size(Groups, 1) %% Target Row 2
                    if(size(Groups, 2)==1)
                        if(isa(Groups{MR2, 1}, 'double'))
                            Info2=num2str(Groups{MR2, 1});
                        else
                            Info2=Groups{MR2, 1};
                        end
                    elseif(size(Groups, 2)==2)
                        if(isa(Groups{MR2, 1}, 'double'))
                            Info2=num2str(Groups{MR2, 1});
                        else
                            Info2=Groups{MR2, 1};
                        end
                        if(isa(Groups{TR2, 2}, 'double'))
                            Info2=horzcat(Info2, ' With ', num2str(Groups{TR2, 2}));
                        else
                            Info2=horzcat(Info2, ' With ', Groups{TR2, 2});
                        end
                    end
                    
                    if(~strcmp(Info1, Info2))
                        Add=true;
                        for i=1:length(Comparisons)
                            if(contains(Comparisons(i).Info, Info1) && contains(Comparisons(i).Info, Info2))
                                Add=false;
                                break;
                            end
                        end
                        if(Add)
                            Index=length(Comparisons)+1;
                            Comparisons(Index).Info=horzcat(Info1, ' vs. ', Info2);
                            if(size(Groups, 2)==1)
                                Comparisons(Index).ComparisonIndeces=[MR1; MR2];
                            elseif(size(Groups, 2)==2)
                                Comparisons(Index).ComparisonIndeces=[MR1, TR1; MR2, TR2];
                            end
                        end
                    end
                end
            end
        end
    end
    
    
    Result.OneByOneComparisonTable=cell(length(Comparisons)+1, 2);
    Result.OneByOneComparisonTable{1,1}='Group Comparison';
    Result.OneByOneComparisonTable{1,2}='Agreement Level (%)';
    for i=1:length(Comparisons)
        if(size(Groups, 2)==1)
            if(isa(Groups{Comparisons(i).ComparisonIndeces(1), 1}, 'double'))
                Temp1=Data(Data.(Groups{1,1}) == Groups{Comparisons(i).ComparisonIndeces(1), 1}, :);
                Temp2=Data(Data.(Groups{1,1}) == Groups{Comparisons(i).ComparisonIndeces(2), 1}, :);
            else
                Temp1=Data(strcmp(Data.(Groups{1,1}), Groups{Comparisons(i).ComparisonIndeces(1), 1}), :);
                Temp2=Data(strcmp(Data.(Groups{1,1}), Groups{Comparisons(i).ComparisonIndeces(2), 1}), :);
            end
        elseif(size(Groups, 2)==2)
            if(isa(Groups{Comparisons(i).ComparisonIndeces(1,1), 1}, 'double'))
                Temp1=Data((Data.(Groups{1,1}) == Groups{Comparisons(i).ComparisonIndeces(1,1), 1}) & (Data.(Groups{1,2}) == Groups{Comparisons(i).ComparisonIndeces(1,2), 2}), :);
                Temp2=Data((Data.(Groups{1,1}) == Groups{Comparisons(i).ComparisonIndeces(2,1), 1}) & (Data.(Groups{1,2}) == Groups{Comparisons(i).ComparisonIndeces(2,2), 2}), :);
            else
                Temp1=Data(strcmp(Data.(Groups{1,1}), Groups{Comparisons(i).ComparisonIndeces(1,1), 1}) & strcmp(Data.(Groups{1,2}), Groups{Comparisons(i).ComparisonIndeces(1,2), 2}), :);
                Temp2=Data(strcmp(Data.(Groups{1,1}), Groups{Comparisons(i).ComparisonIndeces(2,1), 1}) & strcmp(Data.(Groups{1,2}), Groups{Comparisons(i).ComparisonIndeces(2,2), 2}), :);
            end
        end
        UniqueJobIDs=unique(Temp1.JobID);
        Temp=table(UniqueJobIDs, false(length(UniqueJobIDs), 1), false(length(UniqueJobIDs), 1), 'VariableNames', {'JobID', 'SameConclusion', 'MeasuredOnBothGroups'});
        for j=1:length(UniqueJobIDs)
            temp1=Temp1(strcmp(Temp1.JobID, UniqueJobIDs{j}), :);
            temp2=Temp2(strcmp(Temp2.JobID, UniqueJobIDs{j}), :);
            if((size(temp1,1)==1) && (size(temp2,1)==1))
                Temp.MeasuredOnBothGroups(strcmp(Temp.JobID, UniqueJobIDs{j}))=(temp1{1,end}~=categorical({'NOT_MEASURED'})) & (temp2{1,end}~=categorical({'NOT_MEASURED'}));
                if(temp1{1,end}==temp2{1,end})
                    Temp.SameConclusion(strcmp(Temp.JobID, UniqueJobIDs{j}))=true; 
                end
            end
        end
        Result.OneByOneComparison(i).Info=Comparisons(i).Info;
        Result.OneByOneComparison(i).CompareTable=Temp;
        Result.OneByOneComparison(i).DifferenceTable=Temp(~Temp.SameConclusion & Temp.MeasuredOnBothGroups, :);
        
        Result.OneByOneComparisonTable{i+1, 1}=Comparisons(i).Info;
        Result.OneByOneComparisonTable{i+1, 2}=horzcat(num2str(sum(Temp.SameConclusion & Temp.MeasuredOnBothGroups)/size(Temp(Temp.MeasuredOnBothGroups, :), 1)*100, '%2.2f'), '% Similar Classifications for ', num2str(size(Temp(Temp.MeasuredOnBothGroups, :), 1)), ' repetitive measurements within both groups.');
        Result.GroupsComparisonTable{Comparisons(i).ComparisonIndeces(1,1), Comparisons(i).ComparisonIndeces(2,1)}=sum(Temp.SameConclusion & Temp.MeasuredOnBothGroups)/size(Temp(Temp.MeasuredOnBothGroups, :), 1);
        Result.GroupsComparisonTable{Comparisons(i).ComparisonIndeces(2,1), Comparisons(i).ComparisonIndeces(1,1)}=sum(Temp.SameConclusion & Temp.MeasuredOnBothGroups)/size(Temp(Temp.MeasuredOnBothGroups, :), 1);
    end
end

