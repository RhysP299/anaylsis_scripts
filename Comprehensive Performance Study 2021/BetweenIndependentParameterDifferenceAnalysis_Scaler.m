function Result=BetweenIndependentParameterDifferenceAnalysis_Scaler(Data, Label)
    DeleteColumns=[];
    for i=1:size(Data,2)
        if(size(unique(Data(:,i)), 1)==1)
            DeleteColumns=[DeleteColumns,i];
        end
    end
    Data(:,DeleteColumns)=[];
    UniqueJobIDs=unique(Data.JobID);
    NumberOfRecords=1;
    for i=1:length(Data.Properties.VariableNames)
        if(strcmp(Data.Properties.VariableNames{i}, 'JobID'))
            break;
        end
        NumberOfRecords=NumberOfRecords*length(unique(Data.(Data.Properties.VariableNames{i})));
    end
    
    Result.RawTable=Data;
    
    TotalNumberOfErrorComparisons=NumberOfRecords*(NumberOfRecords-1)/2;
    if(TotalNumberOfErrorComparisons>=1)
        Result.DifferenceTable=table(UniqueJobIDs, zeros(size(UniqueJobIDs, 1), TotalNumberOfErrorComparisons), 'VariableNames', {'JobID', horzcat(Label, '_', Data.Properties.VariableNames{end}, '_Error')});
        for i=1:length(UniqueJobIDs)
            temp=Data(strcmp(Data.JobID, UniqueJobIDs{i}), :);
            if(size(temp,1)>=2)
                Ind=1;
                for j=1:size(temp,1)
                    for k=j+1:size(temp,1)
                        Result.DifferenceTable{i,end}(Ind)=abs(temp{j,end}-temp{k,end});
                        Ind=Ind+1;
                    end
                end
            end
        end
    else
        Result.DifferenceTable=[];
    end
end
