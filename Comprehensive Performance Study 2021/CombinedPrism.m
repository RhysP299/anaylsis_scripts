function [Value, Direction]=CombinedPrism(MagnitudeRight, DirectionRight, MagnitudeLeft, DirectionLeft, Axis)
    if(strcmp(Axis,'Horizontal'))
        if(strcmp(DirectionRight, DirectionLeft))
            Value=MagnitudeRight+MagnitudeLeft;
            Direction=DirectionRight;
        else
            if(MagnitudeRight>=MagnitudeLeft)
                Value=MagnitudeRight-MagnitudeLeft;
                Direction=DirectionRight;
            else
                Value=MagnitudeLeft-MagnitudeRight;
                Direction=DirectionLeft;
            end
        end
    elseif(strcmp(Axis,'Vertical'))
        if(strcmp(DirectionRight, DirectionLeft))
            if(MagnitudeRight>=MagnitudeLeft)
                Value=MagnitudeRight-MagnitudeLeft;
            else
                Value=MagnitudeLeft-MagnitudeRight;
            end
            Direction=DirectionRight;
        else
            Value=MagnitudeRight+MagnitudeLeft;
            if(MagnitudeRight>=MagnitudeLeft)
                Direction=DirectionRight;
            else
                Direction=DirectionLeft;
            end
        end
    end
    Value=round(Value, 2);
end