function Result=PrecisionRecallF1Score(Data, PrintTheResults)
    %% Pre-Analysis
    if(~exist('PrintTheResults', 'var'))
        PrintTheResults=false;
    end
    Result=[];
    TargetVariables={'HPrismDirection', 'VPrismDirection', 'PassFail'};
    if(~contains(Data.Recorder{1}, 'vs Golden Standard'))
        return;
    end
    Result=table( cell(size(Data, 1)*2, 1), cell(size(Data, 1)*2, 1), cell(size(Data, 1)*2, 1), ...
                  cell(size(Data, 1)*2, 1), cell(size(Data, 1)*2, 1), cell(size(Data, 1)*2, 1), ...
                  zeros(size(Data, 1)*2, 1), zeros(size(Data, 1)*2, 1), zeros(size(Data, 1)*2, 1), ...
                  zeros(size(Data, 1)*2, 1), zeros(size(Data, 1)*2, 1), zeros(size(Data, 1)*2, 1), ...
                  cell(size(Data, 1)*2, 1), cell(size(Data, 1)*2, 1), cell(size(Data, 1)*2, 1), ...
                  'VariableNames', [{ 'Recorder', 'Device', 'LensOrPair'}, strcat(TargetVariables,'TruthTable'), ...
                                    strcat(TargetVariables,'Accuracy'), strcat(TargetVariables,'UncertainPortion'), ... 
                                    strcat(TargetVariables,'Analysis')] );
    
    %% Analysis
    for i=1:size(Data, 1)
        Result.Recorder{(i*2)-1}=Data.Recorder{i};
        Result.Device{(i*2)-1}=Data.Device{i};
        Result.LensOrPair{(i*2)-1}='Individual Lenses';
        Result.Recorder{(i*2)}=Data.Recorder{i};
        Result.Device{(i*2)}=Data.Device{i};
        Result.LensOrPair{(i*2)}='Pairs';
        LensesSubData=vertcat(Data.OD_RightLens{i}, Data.OS_LeftLens{i});
        PairsSubData=Data.Pair{i};
        for j=1:length(TargetVariables)
            Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)-1}=CalculateTruthTable(LensesSubData.(TargetVariables{j})(:));
            Result.(horzcat(TargetVariables{j}, 'Accuracy'))((i*2)-1)=Accuracy(Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)-1});
            Result.(horzcat(TargetVariables{j}, 'UncertainPortion'))((i*2)-1)=UncertainPortion(Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)-1});
            Result.(horzcat(TargetVariables{j}, 'Analysis')){(i*2)-1}=TruthTableAnalysis(Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)-1});
            
            Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)}=CalculateTruthTable(PairsSubData.(TargetVariables{j})(:));
            Result.(horzcat(TargetVariables{j}, 'Accuracy'))((i*2))=Accuracy(Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)});
            Result.(horzcat(TargetVariables{j}, 'UncertainPortion'))((i*2))=UncertainPortion(Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)});
            Result.(horzcat(TargetVariables{j}, 'Analysis')){(i*2)}=TruthTableAnalysis(Result.(horzcat(TargetVariables{j}, 'TruthTable')){(i*2)});
            
            if(PrintTheResults)
                PrintTheResultTables( Result((i*2)-1, :), TargetVariables{j} );
                PrintTheResultTables( Result((i*2), :), TargetVariables{j} );
            end
        end
    end
end


function TruthTable=CalculateTruthTable(Data)
    OutComes={'True', 'False', 'Uncertain'};
    TruthTable=table( 'Size', [2, 4], 'VariableTypes', {'cell', 'double', 'double', 'double'}, ...
                      'VariableNames', [{'Class'}, OutComes] );
    Classes=unique(strrep(strrep(strrep(unique(Data), 'False ', ''), 'True ', ''), 'Uncertain ', ''));
    for i=1:length(Classes)
        TruthTable.Class{i}=Classes{i};
        for j=1:length(OutComes)
            TruthTable.(OutComes{j})(i)=sum(strcmp(Data, horzcat(OutComes{j}, ' ', Classes{i})));
        end
    end
end

function Result=Accuracy(TruthTable)
    Result=sum(TruthTable.True) / sum([sum(TruthTable.True), sum(TruthTable.False)]) * 100;
end

function Result=TruthTableAnalysis(TruthTable)
    Result=table( 'Size', [2, 4], 'VariableTypes', {'cell', 'double', 'double', 'double'}, ...
                  'VariableNames', {'Class', 'Precision', 'Recall', 'F1Score'} );
    for i=1:size(Result, 1)
        Result.Class{i}=TruthTable.Class{i};
        Result.Precision(i)=Precision(TruthTable, i);
        Result.Recall(i)=Recall(TruthTable, i);
        Result.F1Score(i)=F1Score(Result.Precision(i), Result.Recall(i));
    end
end

function Result=Precision(TruthTable, Index)
    Result=TruthTable.True(Index) / sum(TruthTable{Index, [2, 3]}) * 100;
end

function Result=Recall(TruthTable, Index)
    OtherIndex=true(size(TruthTable, 1), 1);
    OtherIndex(Index)=false;
    Result=TruthTable.True(Index) / sum([TruthTable.True(Index), TruthTable.False(OtherIndex)]) * 100;
end

function Result=F1Score(Precision, Recall)
    if((Precision==0) || (Recall==0))
        Result=0;
    else
        Result=(2*Precision*Recall)/(Precision+Recall);
    end
end

function Result=UncertainPortion(TruthTable)
    Result=sum(TruthTable{:, end}) / sum(sum(TruthTable{:, 2:end})) * 100;
end


function PrintTheResultTables(Result, TargetVariable)
    TruthTable=Result.(horzcat(TargetVariable, 'TruthTable')){1};
    Accuracy=Result.(horzcat(TargetVariable, 'Accuracy'));
    UncertainPortion=Result.(horzcat(TargetVariable, 'UncertainPortion'));
    Analysis=Result.(horzcat(TargetVariable, 'Analysis')){1};
    
    disp(' ')
    disp(' ')
    disp('-------------------------------------------------------------------------------------------------------------------------')
    disp(horzcat(Result.Recorder{1}, ' for ', Result.LensOrPair{1}, ' ==> Performance Analysis for ', TargetVariable))
    disp(horzcat('Overall there were ', num2str(sum(TruthTable.Uncertain)), ' (', num2str(sum(TruthTable.Uncertain)/sum(sum(TruthTable{:, [2,3,4]}))*100, '%2.2f'), '% of all data) were UNCERTAIN.')) 
    disp(horzcat('Accuracy: ', num2str(Accuracy, '%2.2f'), '%'))
    disp(horzcat(num2str(UncertainPortion, '%2.2f'), '% of classifications have been Uncertain!'))
    disp('Class          Precision      Recall         F1-Score')
    for i=1:size(Analysis, 1)
        Text=Analysis.Class{i};
        while(rem(length(Text), 15)~=0)
            Text=horzcat(Text, ' ');
        end
        Text=horzcat(Text, num2str(Analysis.Precision(i), '%2.2f'), '%');
        while(rem(length(Text), 15)~=0)
            Text=horzcat(Text, ' ');
        end
        Text=horzcat(Text, num2str(Analysis.Recall(i), '%2.2f'), '%');
        while(rem(length(Text), 15)~=0)
            Text=horzcat(Text, ' ');
        end
        Text=horzcat(Text, num2str(Analysis.F1Score(i), '%2.2f'), '%');
        disp(Text)
    end
end























