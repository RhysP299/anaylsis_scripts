function CorrelationAnalysis(ReferenceData, TargetData, NumberOfBins)
    TargetData.(ReferenceData.Properties.VariableNames{end})=zeros(size(TargetData, 1), 1);
    for i=1:size(TargetData, 1)
        temp=ReferenceData(strcmp(ReferenceData.JobID, TargetData.JobID{i}), :);
        TargetData.(ReferenceData.Properties.VariableNames{end})(i)=mean(temp.(ReferenceData.Properties.VariableNames{end}));
    end
    Ranges=unique(TargetData.(ReferenceData.Properties.VariableNames{end}));
    Ranges=min(Ranges):((max(Ranges)-min(Ranges))/NumberOfBins):max(Ranges);
    BoxplotData=zeros(size(TargetData, 1)*size(TargetData{:, end-1}, 2), 2);
    Index=1;
    for i=1:size(TargetData, 1)
        BoxplotData(Index:Index+size(TargetData{:, end-1}, 2)-1, 1)=TargetData{i, end-1};
        Class=find((Ranges-TargetData.(ReferenceData.Properties.VariableNames{end})(i))>=0, 1)-1;
        if(Class<1)
            Class=1;
        end
        BoxplotData(Index:Index+size(TargetData{:, end-1}, 2)-1, 2)=Class;
        BoxplotData(Index:Index+size(TargetData{:, end-1}, 2)-1, 3)=TargetData.(ReferenceData.Properties.VariableNames{end})(i);
        Index=Index+size(TargetData{:, end-1}, 2);
    end
    Groups=(Ranges(2:end)+Ranges(1:end-1))/2;
    GroupsString=sprintfc('%2.2f', Groups);
    
    fig=figure('Position', [2049 41 2048 1.0368e+03], 'Name', horzcat('Correlation Analysis for ', strrep(strrep(TargetData.Properties.VariableNames{end-1}, '_', ' '), 'Error', '')));
    hold on
    grid on
    ToBeDeleted=false(size(BoxplotData, 1), 1);
    for i=1:size(BoxplotData, 2)
        ToBeDeleted=or(ToBeDeleted, isnan(BoxplotData(:, i)));
    end
    BoxplotData(ToBeDeleted, :)=[];
    [R, P]=corrcoef(BoxplotData(:, 3), BoxplotData(:, 1));
    [fitresult, gof]=fit( BoxplotData(:, 3), BoxplotData(:, 1), fittype( 'poly1' ) );
    title(horzcat('Corrolation Coefficient: ', num2str(R(1,2), '%2.3f'), ' - P-Value: ', num2str(P(1,2), '%2.3f'), ' - R-Squred: ', num2str(abs(gof.adjrsquare)*100, '%2.2f'), '%'));
    xlabel(ReferenceData.Properties.VariableNames{end});
    ylabel(strrep(TargetData.Properties.VariableNames{end-1}, '_', ' '));
    
    boxplot(BoxplotData(:, 1), BoxplotData(:, 2), 'labels', GroupsString(unique(BoxplotData(:, 2))), 'positions', Groups(unique(BoxplotData(:, 2))));
    set(findobj(gca,'type','line'), 'linew', 2);
    x=min(Ranges):0.05:max(Ranges);
    plot( x, fitresult(x), 'g:', 'LineWidth', 2);
    
    set(gca,'fontsize', 15);
    up_adj=get(findobj(gcf, 'tag', 'Upper Adjacent Value'),'YData');
    lower_adj=get(findobj(gcf, 'tag', 'Lower Adjacent Value'),'YData');
    if(iscell(up_adj))
        up_adj=cell2mat(up_adj);
    end
    if(iscell(lower_adj))
        lower_adj=cell2mat(lower_adj);
    end
    ylim([min(min(lower_adj)) - (abs(min(min(lower_adj))) * 0.1), max(max(up_adj))*1.1]);
end