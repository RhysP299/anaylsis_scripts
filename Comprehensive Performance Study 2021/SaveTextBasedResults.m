function SaveTextBasedResults(FilePath)
    FolderPath=FilePath(1:max(strfind(FilePath, '\'))-1);
    if (~exist(FolderPath, 'dir'))
       mkdir(FolderPath);
    end
    if (exist(FilePath, 'file'))
       delete(FilePath);
    end
    diary(FilePath);
end