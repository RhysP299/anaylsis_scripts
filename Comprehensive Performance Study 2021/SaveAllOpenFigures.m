function SaveAllOpenFigures(FolderPath)
    if (~exist(FolderPath, 'dir'))
       mkdir(FolderPath);
    end
    if (exist(fullfile(FolderPath, 'Matlab Figures'), 'dir'))
       delete(horzcat(fullfile(FolderPath, 'Matlab Figures'), '\*'));
    else
        mkdir(fullfile(FolderPath, 'Matlab Figures'));
    end
    
    if (exist(fullfile(FolderPath, 'Image Figures'), 'dir'))
       delete(horzcat(fullfile(FolderPath, 'Image Figures'), '\*'));
    else
        mkdir(fullfile(FolderPath, 'Image Figures'));
    end
    
    FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
    for iFig = 1:length(FigList)
        FigHandle = FigList(iFig);
        FigName   = horzcat('Figure ', num2str(get(FigHandle, 'Number')), ' - ', get(FigHandle, 'Name'));
        savefig(FigHandle, fullfile(FolderPath, 'Matlab Figures', horzcat(FigName, '.fig')));
        saveas(FigHandle, fullfile(FolderPath, 'Image Figures', horzcat(FigName, '.png')));
    end
end