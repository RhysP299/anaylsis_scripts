function Result=ANOVA(ANOVAData, SignificanceLevel)
    if(isempty(ANOVAData))
        Result.Table=[];
        return
    end
    DependantVariable=ANOVAData{:,end}';
    IndependentVariables=cell(1, size(ANOVAData,2)-1);
    for i=1:size(ANOVAData,2)-1
        IndependentVariables{i}=ANOVAData{:, i}';
    end
    VariableNames=ANOVAData.Properties.VariableNames;
    VariableNames(end)=[];
    [~, ResultTemp, Result.Statistics]=anovan(DependantVariable, IndependentVariables, 'varnames', VariableNames, 'display', 'off');
       
    disp(horzcat('ANOVA Analysis for ',ANOVAData.Properties.VariableNames{end}))
    disp('-----------------------------------------------')
    disp('Source                   d.f.           P-Value')
    for i=2:(size(ResultTemp,1)-2)
        Result.Table{i-1,1}=ResultTemp{i,1};
        Text=ResultTemp{i,1};
        while(length(Text)<25)
            Text=horzcat(Text,' ');
        end
        Result.Table{i-1,2}=ResultTemp{i,3};
        Text=horzcat(Text, num2str(ResultTemp{i,3}));
        while(length(Text)<40)
            Text=horzcat(Text,' ');
        end
        Result.Table{i-1,3}=ResultTemp{i,7};
        if((ResultTemp{i,7}<SignificanceLevel) && ~contains(Text,'JobID'))
            Text=horzcat(Text, num2str(ResultTemp{i,7},'%2.3f'), '***');
        else
            Text=horzcat(Text, num2str(ResultTemp{i,7},'%2.3f'));
        end
        disp(Text)
    end
    disp(' ')
    disp(' ')
end