function [Data, FrameData]=ImportLMSFile(FilePath, JobID, ImportFrameData)
    if(~exist('ImportFrameData', 'var'))
        ImportFrameData=false;
    end
    
    FrameData=[];
    opts = delimitedTextImportOptions("NumVariables", 2);
    opts.DataLines = [1, Inf];
    opts.Delimiter = "=";
    opts.VariableNames = ["Label", "Value"];
    opts.VariableTypes = ["string", "string"];
    opts.ExtraColumnsRule = "ignore";
    opts.EmptyLineRule = "read";
    opts = setvaropts(opts, "Value", "WhitespaceRule", "preserve");
    opts = setvaropts(opts, ["Label", "Value"], "EmptyFieldRule", "auto");
    Raw = readtable(FilePath, opts);
    
    Data.OD_RightLens = table( JobID, zeros(1, 1), zeros(1, 1), ...
                               zeros(1, 1), zeros(1, 1), ...
                               zeros(1, 1), cell(1, 1), ...
                               zeros(1, 1), cell(1, 1), ...
                               zeros(1, 1), zeros(1, 1), ...
                               cell(1, 1), ...
                               'VariableNames', { 'JobID', 'Sphere', 'Cylinder', ...
                                                  'Axis', 'Add', ...
                                                  'HPrismMagnitude', 'HPrismDirection', ...
                                                  'VPrismMagnitude', 'VPrismDirection', ...
                                                  'HPrism', 'VPrism', 'PassFail' } );
                                                             
    Data.OS_LeftLens = table( JobID, zeros(1, 1), zeros(1, 1), ...
                              zeros(1, 1), zeros(1, 1), ...
                              zeros(1, 1), cell(1, 1), ...
                              zeros(1, 1), cell(1, 1), ...
                              zeros(1, 1), zeros(1, 1), ...
                              cell(1, 1), ...
                              'VariableNames', { 'JobID', 'Sphere', 'Cylinder', ...
                                                  'Axis', 'Add', ...
                                                  'HPrismMagnitude', 'HPrismDirection', ...
                                                  'VPrismMagnitude', 'VPrismDirection', ...
                                                  'HPrism', 'VPrism', 'PassFail'} );  
    Data.Pair =        table( JobID, ...
                              zeros(1, 1), cell(1, 1), ...
                              zeros(1, 1), cell(1, 1), ...
                              zeros(1, 1), zeros(1, 1), cell(1, 1), ...
                              'VariableNames', { 'JobID', ...
                                                 'HPrismMagnitude', 'HPrismDirection', ...
                                                 'VPrismMagnitude', 'VPrismDirection', ...
                                                 'HPrism', 'VPrism', 'PassFail' } );
    
	if(sum(strcmp(Raw.Label, 'LDDRSPH'))>0)
        SPH=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDDRSPH')), ';'));
    elseif(sum(strcmp(Raw.Label, 'LDSPH'))>0)
        SPH=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDSPH')), ';'));
    elseif(sum(strcmp(Raw.Label, 'SPH'))>0)
        SPH=str2double(split(Raw.Value(strcmp(Raw.Label, 'SPH')), ';'));
    else
        SPH=[0, 0];
    end
    
    if(sum(strcmp(Raw.Label, 'LDDRCYL'))>0)
        CYL=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDDRCYL')), ';'));
    elseif(sum(strcmp(Raw.Label, 'LDCYL'))>0)
        CYL=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDCYL')), ';'));
    elseif(sum(strcmp(Raw.Label, 'CYL'))>0)
        CYL=str2double(split(Raw.Value(strcmp(Raw.Label, 'CYL')), ';'));
    else
        CYL=[0, 0];
    end
    
    if(sum(strcmp(Raw.Label, 'LDDRAX'))>0)
        AX=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDDRAX')), ';'));
    elseif(sum(strcmp(Raw.Label, 'LDAX'))>0)
        AX=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDAX')), ';'));
    elseif(sum(strcmp(Raw.Label, 'AX'))>0)
        AX=str2double(split(Raw.Value(strcmp(Raw.Label, 'AX')), ';'));
    else
        AX=[0, 0];
    end
    
    if(sum(strcmp(Raw.Label, 'LDDRADD'))>0)
        ADD=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDDRADD')), ';'));
    elseif(sum(strcmp(Raw.Label, 'LDADD'))>0)
        ADD=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDADD')), ';'));
    elseif(sum(strcmp(Raw.Label, 'ADD'))>0)
        ADD=str2double(split(Raw.Value(strcmp(Raw.Label, 'ADD')), ';'));
    else
        ADD=[0, 0];
    end
    
%     disp(horzcat( 'JobID: ', num2str(JobID), ...
%                   ' ==> LDPRVA: ',num2str(sum(strcmp(Raw.Label, 'LDPRVA'))), ' - LDPRVM: ',num2str(sum(strcmp(Raw.Label, 'LDPRVM'))), ...
%                   ' - PRVA: ',num2str(sum(strcmp(Raw.Label, 'PRVA'))), ' - PRVM: ',num2str(sum(strcmp(Raw.Label, 'PRVM'))) ))
              
              
    if((sum(strcmp(Raw.Label, 'LDPRVA'))>0) && (sum(strcmp(Raw.Label, 'LDPRVM'))>0))
        PRVA=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDPRVA')), ';'));
        PRVM=str2double(split(Raw.Value(strcmp(Raw.Label, 'LDPRVM')), ';'));
    elseif((sum(strcmp(Raw.Label, 'PRVA'))>0) && (sum(strcmp(Raw.Label, 'PRVM'))>0))
        PRVA=str2double(split(Raw.Value(strcmp(Raw.Label, 'PRVA')), ';'));
        PRVM=str2double(split(Raw.Value(strcmp(Raw.Label, 'PRVM')), ';'));
    else
        PRVA=[0, 0];
        PRVM=[0, 0];
    end
    
    Data.OD_RightLens.Sphere=round(SPH(1), 2);
    Data.OS_LeftLens.Sphere=round(SPH(2), 2);
    
    Data.OD_RightLens.Cylinder=round(CYL(1), 2);
    Data.OS_LeftLens.Cylinder=round(CYL(2), 2);
    
    Data.OD_RightLens.Axis=round(AX(1), 2);
    Data.OS_LeftLens.Axis=round(AX(2), 2);
    
    Data.OD_RightLens.Add=round(ADD(1), 2);
    Data.OS_LeftLens.Add=round(ADD(2), 2);
    
    Data.OD_RightLens.HPrismMagnitude=round(abs(PRVM(1)*cos(PRVA(1)*pi/180)), 2);
    Data.OD_RightLens.HPrismDirection=PrismVectorDirection(PRVA(1), 'Horizontal', 'Right');
    Data.OS_LeftLens.HPrismMagnitude=round(abs(PRVM(2)*cos(PRVA(2)*pi/180)), 2);
    Data.OS_LeftLens.HPrismDirection=PrismVectorDirection(PRVA(2), 'Horizontal', 'Left');
    
    Data.OD_RightLens.VPrismMagnitude=round(abs(PRVM(1)*sin(PRVA(1)*pi/180)), 2);
    Data.OD_RightLens.VPrismDirection=PrismVectorDirection(PRVA(1), 'Vertical', 'Right');
    Data.OS_LeftLens.VPrismMagnitude=round(abs(PRVM(2)*sin(PRVA(2)*pi/180)), 2);
    Data.OS_LeftLens.VPrismDirection=PrismVectorDirection(PRVA(2), 'Vertical', 'Left');
    
    Data.OD_RightLens.HPrism=round(PrismVectorDirectionSign('Right', 'Horizontal', Data.OD_RightLens.HPrismDirection) * Data.OD_RightLens.HPrismMagnitude, 2);
    Data.OD_RightLens.VPrism=round(PrismVectorDirectionSign('Right', 'Vertical', Data.OD_RightLens.VPrismDirection) * Data.OD_RightLens.VPrismMagnitude, 2);
    
    Data.OS_LeftLens.HPrism=round(PrismVectorDirectionSign('Left', 'Horizontal', Data.OS_LeftLens.HPrismDirection) * Data.OS_LeftLens.HPrismMagnitude, 2);
    Data.OS_LeftLens.VPrism=round(PrismVectorDirectionSign('Left', 'Vertical', Data.OS_LeftLens.VPrismDirection) * Data.OS_LeftLens.VPrismMagnitude, 2);
    
    [Data.Pair.HPrismMagnitude, Data.Pair.HPrismDirection]=CombinedPrism( Data.OD_RightLens.HPrismMagnitude, Data.OD_RightLens.HPrismDirection, ...
                                                                          Data.OS_LeftLens.HPrismMagnitude, Data.OS_LeftLens.HPrismDirection, 'Horizontal' );
    [Data.Pair.VPrismMagnitude, Data.Pair.VPrismDirection]=CombinedPrism( Data.OD_RightLens.VPrismMagnitude, Data.OD_RightLens.VPrismDirection, ...
                                                                          Data.OS_LeftLens.VPrismMagnitude, Data.OS_LeftLens.VPrismDirection, 'Vertical' );
                                                                      
    Data.Pair.HPrism=round(PrismVectorDirectionSign('Pair', 'Horizontal', Data.Pair.HPrismDirection) * Data.Pair.HPrismMagnitude, 2);
    Data.Pair.VPrism=round(PrismVectorDirectionSign('Pair', 'Vertical', Data.Pair.VPrismDirection) * Data.Pair.VPrismMagnitude, 2);
    
    Data.OD_RightLens.PassFail={'Pass'};
    Data.OS_LeftLens.PassFail={'Pass'};
    Data.Pair.PassFail={'Pass'};
    
    if(ImportFrameData)
        if(sum(strcmp(Raw.Label, 'HBOX'))>0)
            HBOX=str2double(split(Raw.Value(strcmp(Raw.Label, 'HBOX')), ';'));
        else
            HBOX=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'VBOX'))>0)
            VBOX=str2double(split(Raw.Value(strcmp(Raw.Label, 'VBOX')), ';'));
        else
            VBOX=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'IPD'))>0)
            IPD=str2double(split(Raw.Value(strcmp(Raw.Label, 'IPD')), ';'));
        else
            IPD=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'OCHT'))>0)
            OCHT=str2double(split(Raw.Value(strcmp(Raw.Label, 'OCHT')), ';'));
        else
            OCHT=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'FCOCIN'))>0)
            FCOCIN=str2double(split(Raw.Value(strcmp(Raw.Label, 'FCOCIN')), ';'));
        else
            FCOCIN=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'FCOCUP'))>0)
            FCOCUP=str2double(split(Raw.Value(strcmp(Raw.Label, 'FCOCUP')), ';'));
        else
            FCOCUP=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'ERDRIN'))>0)
            ERDRIN=str2double(split(Raw.Value(strcmp(Raw.Label, 'ERDRIN')), ';'));
        else
            ERDRIN=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'ERDRUP'))>0)
            ERDRUP=str2double(split(Raw.Value(strcmp(Raw.Label, 'ERDRUP')), ';'));
        else
            ERDRUP=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'ERNRIN'))>0)
            ERNRIN=str2double(split(Raw.Value(strcmp(Raw.Label, 'ERNRIN')), ';'));
        else
            ERNRIN=[0, 0];
        end

        if(sum(strcmp(Raw.Label, 'ERNRUP'))>0)
            ERNRUP=str2double(split(Raw.Value(strcmp(Raw.Label, 'ERNRUP')), ';'));
        else
            ERNRUP=[0, 0];
        end
        
        LTYPE=5000000;
        if(sum(strcmp(Raw.Label, 'LTYPE'))>0)
            LTYPE_Cat=split(Raw.Value(strcmp(Raw.Label, 'LTYPE')), ';');
            if(sum(contains(LTYPE_Cat, 'SV'))>0)
                LTYPE=5000001;
            elseif(sum(contains(LTYPE_Cat, 'PR'))>0)
                LTYPE=5000002;
            end
        end
        
        if(LTYPE==5000000)
            khar=1;
        end
        
        FrameData=[ JobID, LTYPE, ...
                           HBOX(1), VBOX(1), IPD(1), OCHT(1), ...
                           FCOCIN(1), FCOCUP(1), ERDRIN(1), ERDRUP(1), ...
                           ERNRIN(1), ERNRUP(1), ...
                           HBOX(2), VBOX(2), IPD(2), OCHT(2), ...
                           FCOCIN(2), FCOCUP(2), ERDRIN(2), ERDRUP(2), ...
                           ERNRIN(2), ERNRUP(2), ...
                           IPD(2)-IPD(1), OCHT(2)-OCHT(1), FCOCIN(2)-FCOCIN(1), ...
                           FCOCUP(2)-FCOCUP(1), 0, 0];
        FrameData(27) = FrameData(25)- FrameData(23);
        FrameData(28) = FrameData(26)- FrameData(24);
    end
end






