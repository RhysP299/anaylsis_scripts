clear all
clear global
close all
clc

CriticalP_Value=0.05;
CorrelationAnalysisNumberOfBins=20;


% ExperimentFilePath='eMap Experiments\Paul Clapton - 03-11-21.csv';
% DatesToConsider={};

ExperimentFilePath='eMap Experiments\Jim Cox - 11-12-01-22.csv';
DatesToConsider={'12/01/2022', '13/01/2022'};


SaveTextBasedResults(horzcat('Results\', strrep(ExperimentFilePath(max(strfind(ExperimentFilePath, '\'))+1:end), '.csv', ''), '\Analysis Results.txt'));
%% Measurment & Insert Assignment Protocol:
% DataTimeBasedMeasurmentInsertAssignment.InserDetectionMinuteDifferenceLimit=5; %% Date and Time Base

DataTimeBasedMeasurmentInsertAssignment.ExpectedNumberOfMeasurments=2; %% Sequential Based
DataTimeBasedMeasurmentInsertAssignment.ExpectedNumberOfInserts=2; %% Sequential Based


%% Golden Standard
GoldStandard=ImportGoldStandard('Golden Standard - Manual Measurment.xlsx');
% [GoldenStandardComprehensiveDependencyAnalysisResult, GoldenStandardComprehensiveDependencyAnalysisANOVAResult]=GoldenStandardComprehensiveDependencyAnalysis(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Not Combined', 'Not Combined'), GoldStandard, CriticalP_Value, true, true);
% CorrelationAnalysis(GoldenStandardComprehensiveDependencyAnalysisResult{2}.RawTable, GoldenStandardComprehensiveDependencyAnalysisResult{2}.DifferenceTable, CorrelationAnalysisNumberOfBins);
% CorrelationAnalysis(ANOVADataExtract(ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Combined', 'Combined'), {'Recorder', 'Device'}, 'Cylinder'), GoldenStandardComprehensiveDependencyAnalysisResult{1}.DifferenceTable, CorrelationAnalysisNumberOfBins);


% AllTIEs=ExtrcatDataByDeviceRecorder(GoldStandard, 'TIE');
% HPrismDirectionTIEs=ExtrcatDataByDeviceRecorder(GoldStandard, 'TIE', 'HPrismDirection');
% VPrismDirectionTIEs=ExtrcatDataByDeviceRecorder(GoldStandard, 'TIE', 'VPrismDirection');
% PassFailTIEs=ExtrcatDataByDeviceRecorder(GoldStandard, 'TIE', 'PassFail');

%% LMS Data
% [LMSData, ExcelFileFormat, FrameData]=ImportLMSData('LMS Data', true);

%% eMap Data
[eMapData, eMapDataPresence]=ImportEmapData(ExperimentFilePath, DataTimeBasedMeasurmentInsertAssignment, DatesToConsider, true);
[PrunedeMapData, PrunedeMapPresence, PrunedeMapPresenceReport]=RejectIsufficientData(eMapData, eMapDataPresence);
IndependentVariablesToIgnore={};
for i=1:length(PrunedeMapPresenceReport.Uniques.Properties.VariableNames)
    if(length(PrunedeMapPresenceReport.Uniques.(PrunedeMapPresenceReport.Uniques.Properties.VariableNames{i}){1})==1)
        IndependentVariablesToIgnore{end+1}=PrunedeMapPresenceReport.IndependentParmeters{i};
    end
end
% eMapMeasurmentComprehensiveDependencyAnalysisResult=eMapMeasurmentComprehensiveDependencyAnalysis(PrunedeMapData, CriticalP_Value, IndependentVariablesToIgnore);

PrunedeMapErrorData=MeasureError(PrunedeMapData, ExtrcatDataByDeviceRecorder(GoldStandard));
[eMapMeasurmentErrorComprehensiveDependencyAnalysisResult, eMapMeasurmentErrorComprehensiveDependencyAnalysisANOVAResult]=eMapMeasurmentComprehensiveDependencyAnalysis(PrunedeMapErrorData, CriticalP_Value, IndependentVariablesToIgnore, true, true);
ComprehensiveBlandAltmanAnalysis(PrunedeMapData, ExtrcatDataByDeviceRecorder(GoldStandard), eMapMeasurmentErrorComprehensiveDependencyAnalysisANOVAResult, CriticalP_Value);
ComprehensiveCorrelationAnalysis( ExtrcatDataByDeviceRecorder(GoldStandard, '', 'Any', 'Combined', 'Combined'), PrunedeMapErrorData, CorrelationAnalysisNumberOfBins);
PrecisionRecallF1ScoreResult=PrecisionRecallF1Score(PrunedeMapErrorData, true);


%% Save Results:
SaveAllOpenFigures(horzcat('Results\', ExperimentFilePath(max(strfind(ExperimentFilePath, '\'))+1:strfind(ExperimentFilePath,'.csv')-1), '\'));
diary off
fclose('all');

% SetDataCursorTTest(1) % To turn ON Box Plot Groups TTest Comparison.


