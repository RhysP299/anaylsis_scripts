function Presence=CheckDataPresence(Data, AllIndependantVariables)
    TotalNumberOfCombinations=1;
    
    DataIndecesToDeleted=cell(1, length(Data.Properties.VariableNames));
    Records={};
    for i=1:length(Data.Properties.VariableNames)
        if(~strcmp(Data.Properties.VariableNames{i}, 'Recorder') && ~strcmp(Data.Properties.VariableNames{i}, 'Device'))
            DataIndecesToDeleted{i}=false(size(Data.(Data.Properties.VariableNames{i}){1}, 1), 1);
            Records{end+1}=horzcat('NumberOfRecordsIn_', Data.Properties.VariableNames{i});
        end
    end
    VariableTypes=cell(length(AllIndependantVariables)+length(Records), 1);
    for i=1:length(AllIndependantVariables)
        VariableTypes{i}=class(Data.OD_RightLens{1}.(AllIndependantVariables{i}));
        IndependantVariables(i).Name=AllIndependantVariables{i};
        IndependantVariables(i).Unique=unique([ Data.OD_RightLens{1}.(AllIndependantVariables{i}); ...
                                                Data.OS_LeftLens{1}.(AllIndependantVariables{i});...
                                                Data.Pair{1}.(AllIndependantVariables{i}) ]);
        TotalNumberOfCombinations=TotalNumberOfCombinations*length(IndependantVariables(i).Unique);
    end
    for i=1:length(Records)
        VariableTypes{length(AllIndependantVariables)+i}='double';
    end
    
    
    Presence=table('Size', [TotalNumberOfCombinations, length(VariableTypes)], 'VariableTypes', VariableTypes, 'VariableNames', [AllIndependantVariables, Records]);
    Indeces=ones(1, length(IndependantVariables));
    CombinationsCovered=0;
    
    while(CombinationsCovered<TotalNumberOfCombinations)
        for i=1:length(IndependantVariables)
            if(strcmp(VariableTypes{i}, 'double'))
                Presence.(IndependantVariables(i).Name)(CombinationsCovered+1)=IndependantVariables(i).Unique(Indeces(i));
            else
                Presence.(IndependantVariables(i).Name){CombinationsCovered+1}=IndependantVariables(i).Unique{Indeces(i)};
            end
        end
        DataIndeces=cell(1, length(Data.Properties.VariableNames));
        for i=1:length(Data.Properties.VariableNames)
            if(~strcmp(Data.Properties.VariableNames{i}, 'Recorder') && ~strcmp(Data.Properties.VariableNames{i}, 'Device'))
                DataIndeces{i}=true(size(Data.(Data.Properties.VariableNames{i}){1}, 1), 1);
                for j=1:length(IndependantVariables)
                    if(strcmp(VariableTypes{j}, 'double'))
                        DataIndeces{i}=DataIndeces{i} & (Data.(Data.Properties.VariableNames{i}){1}.(IndependantVariables(j).Name)==Presence.(IndependantVariables(j).Name)(CombinationsCovered+1));
                    else
                        DataIndeces{i}=DataIndeces{i} & strcmp(Data.(Data.Properties.VariableNames{i}){1}.(IndependantVariables(j).Name), Presence.(IndependantVariables(j).Name){CombinationsCovered+1});
                    end
                end
                Index=contains(Presence.Properties.VariableNames, Data.Properties.VariableNames{i});
                if(sum(contains(Presence.Properties.VariableNames, Data.Properties.VariableNames{i}))==1)
                    Presence.(Presence.Properties.VariableNames{Index})(CombinationsCovered+1)=sum(DataIndeces{i});
                end
            end
        end
        
        
        Indeces=Iterate(Indeces, IndependantVariables);
        CombinationsCovered=CombinationsCovered+1;
    end
end

function Indeces=Iterate(Indeces, IndependantVariables)
    Indeces(end)=Indeces(end)+1;
    for i=length(Indeces):-1:2
        if(Indeces(i)>length(IndependantVariables(i).Unique))
            Indeces(i)=1;
            Indeces(i-1)=Indeces(i-1)+1;
        end
    end
    if(Indeces(1)>length(IndependantVariables(1).Unique))
        Indeces=nan(1, length(Indeces));
    end
end