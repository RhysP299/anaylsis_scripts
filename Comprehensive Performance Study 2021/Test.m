clear all
close all
clc

NBin=20;

load('temp.mat');
figure
hold on
grid on

boxplot([a; b; c], [ones(length(a), 1); ones(length(b), 1)+1; ones(length(c), 1)+2]);
up_adj=get(findobj(gcf, 'tag', 'Upper Adjacent Value'),'YData');
lower_adj=get(findobj(gcf, 'tag', 'Lower Adjacent Value'),'YData');
if(iscell(up_adj))
    up_adj=cell2mat(up_adj);
end
if(iscell(lower_adj))
    lower_adj=cell2mat(lower_adj);
end


Percentile_25=prctile(c, 25);
Percentile_75=prctile(c, 75);
CBox=c( (c>Percentile_25) & (c<Percentile_75) );
[h, e]=histcounts(CBox, NBin);
Verteces=zeros(length(h)*4, 2);
Faces=zeros(length(h), 4);
StartIndex=1;
FaceCounter=1;
for i=1:length(h)
    Verteces(StartIndex:StartIndex+3, :)=[ 2.9, e(i); ...
                                           2.9, e(i+1); ...
                                           3.1, e(i+1); ...
                                           3.1, e(i)];
    Faces(i,:)=FaceCounter:FaceCounter+3;
    StartIndex=StartIndex+4;
    FaceCounter=FaceCounter+4;
end
hNor=h'/sum(h);
hNor=hNor/max(max(hNor))*100;
patch('Faces', Faces, 'Vertices', Verteces, 'FaceVertexCData', hNor, 'FaceColor','flat', 'EdgeColor', 'none');
h=boxplot([a; b; c], [ones(length(a), 1); ones(length(b), 1)+1; ones(length(c), 1)+2]);
set(findobj(gca,'type','line'),'linew',3)
ylim([min(min(lower_adj)) - (abs(min(min(lower_adj))) * 0.1), max(max(up_adj))*1.1]);
colorbar;




% Verteces=zeros(NumberofBins*4*(GroupCounter-1), 2);
% Faces=zeros(NumberofBins*(GroupCounter-1), 4);
% FaceColors=zeros(NumberofBins*(GroupCounter-1), 1);
% StartIndex=1;
% FaceCounter=1;
% FaceColorCounter=1;
% for Group=1:GroupCounter-1
%     temp=BoxPlotData(1, BoxPlotData(2, :)==Group);
%     Percentile_25=prctile(temp, 25);
%     Percentile_75=prctile(temp, 75);
%     tempBox=temp( (temp>Percentile_25) & (temp<Percentile_75) );
%     [tempHistogram, tempHistogramEdges]=histcounts(tempBox, NumberofBins);
%     for i=1:length(tempHistogram)
%         Verteces(StartIndex:StartIndex+3, :)=[ Group-0.2, tempHistogramEdges(i); ...
%                                                Group-0.2, tempHistogramEdges(i+1); ...
%                                                Group+0.2, tempHistogramEdges(i+1); ...
%                                                Group+0.2, tempHistogramEdges(i)];
%         Faces(floor(FaceCounter/4)+1,:)=FaceCounter:FaceCounter+3;
%         StartIndex=StartIndex+4;
%         FaceCounter=FaceCounter+4;
%     end
%     FaceColors(FaceColorCounter:FaceColorCounter+NumberofBins-1)=tempHistogram'/sum(tempHistogram)*100;
%     FaceColorCounter=FaceColorCounter+NumberofBins;
% %         tempHistogramNormalised=tempHistogramNormalised/max(max(tempHistogramNormalised))*100;
% %         patch('Faces', Faces, 'Vertices', Verteces, 'FaceVertexCData', tempHistogramNormalised, 'FaceColor','flat', 'EdgeColor', 'none');
% end
% 
% patch('Faces', Faces, 'Vertices', Verteces, 'FaceVertexCData', NumberofBins, 'FaceColor','flat', 'EdgeColor', 'none');
