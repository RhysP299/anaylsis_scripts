function GoldStandard=ImportGoldStandard(FilePath)
    if(~exist('GoldStandard.mat', 'file')>0)
        warning ('off','all');
        [~,SheetNames]=xlsfinfo(FilePath);
        GoldStandard(length(SheetNames)+1).Recorder='';
        GoldStandard(length(SheetNames)+1).Device='';  
        JobIDs=[];
        CombinedJobIDs=[];
        for i=1:length(SheetNames)
            SheetNamesSplit=split(strtrim(SheetNames{i}));
            if(length(SheetNamesSplit)~=2)
                continue;
            end
            GoldStandard(i).Recorder=SheetNamesSplit{1};
            GoldStandard(i).Device=SheetNamesSplit{2};
            Raw=readtable(FilePath, 'Sheet', SheetNames{i});
            JobIDs=unique([JobIDs;unique(Raw.JobID)]);
            CombinedJobIDs=[CombinedJobIDs;Raw.JobID];
            JobIDs(isnan(JobIDs))=[];
            OD_Ind=strcmp(Raw.Side, 'OD');
            OS_Ind=strcmp(Raw.Side, 'OS');
            Pair_Ind=OD_Ind;
            GoldStandard(i).OD_RightLens = table( Raw.JobID(OD_Ind), Raw.Sphere(OD_Ind), ...
                                                  Raw.Cylinder(OD_Ind), Raw.Axis(OD_Ind), ...
                                                  Raw.Add(OD_Ind), Raw.HPrismMag(OD_Ind), ...
                                                  Raw.HPrismDirection(OD_Ind), Raw.VPrismMag(OD_Ind), ...
                                                  Raw.VPrismDirection(OD_Ind), ...
                                                  zeros(sum(OD_Ind), 1), zeros(sum(OD_Ind), 1), ....
                                                  Raw.Pass_FailLens(OD_Ind), ...
                                                  'VariableNames', { 'JobID', 'Sphere', 'Cylinder', ...
                                                                     'Axis', 'Add', ...
                                                                     'HPrismMagnitude', 'HPrismDirection', ...
                                                                     'VPrismMagnitude', 'VPrismDirection', ...
                                                                     'HPrism', 'VPrism', 'PassFail'} );
            GoldStandard(i).OD_RightLens=PrismVectorConstruction(GoldStandard(i).OD_RightLens, 'Right');

            GoldStandard(i).OS_LeftLens =  table( Raw.JobID(OS_Ind), Raw.Sphere(OS_Ind), ...
                                                  Raw.Cylinder(OS_Ind), Raw.Axis(OS_Ind), ...
                                                  Raw.Add(OS_Ind), Raw.HPrismMag(OS_Ind), ...
                                                  Raw.HPrismDirection(OS_Ind), Raw.VPrismMag(OS_Ind), ...
                                                  Raw.VPrismDirection(OS_Ind), ...
                                                  zeros(sum(OS_Ind), 1), zeros(sum(OS_Ind), 1), ....
                                                  Raw.Pass_FailLens(OS_Ind), ...
                                                  'VariableNames', { 'JobID', 'Sphere', 'Cylinder', ...
                                                                     'Axis', 'Add', ...
                                                                     'HPrismMagnitude', 'HPrismDirection', ...
                                                                     'VPrismMagnitude', 'VPrismDirection', ...
                                                                     'HPrism', 'VPrism', 'PassFail' } );                                    
            GoldStandard(i).OS_LeftLens=PrismVectorConstruction(GoldStandard(i).OS_LeftLens, 'Left');

            GoldStandard(i).Pair =         table( Raw.JobID(Pair_Ind), ...
                                                  Raw.HCombinedPrismMagnitude(Pair_Ind), Raw.HCombinedPrismDirection(Pair_Ind), ...
                                                  Raw.VCombinedPrismMagnitude(Pair_Ind), Raw.VCombinedPrismDirection(Pair_Ind), ...
                                                  zeros(sum(Pair_Ind), 1), zeros(sum(Pair_Ind), 1), ....
                                                  Raw.PassFailJob(Pair_Ind), ...
                                                  'VariableNames', { 'JobID', ...
                                                                     'HPrismMagnitude', 'HPrismDirection', ...
                                                                     'VPrismMagnitude', 'VPrismDirection', ...
                                                                     'HPrism', 'VPrism', 'PassFail' } );
            GoldStandard(i).Pair=PrismVectorConstruction(GoldStandard(i).Pair, 'Pair');
        end
        warning ('on','all');

        for i=1:length(JobIDs)
            temp=sum(CombinedJobIDs==JobIDs(i));
            if(temp~=8)
               disp(horzcat(num2str(JobIDs(i)), ' had ', num2str(temp), ' occurrence.'))
            end
        end
        
        GoldStandard=struct2table(GoldStandard);
        for i=1:(size(GoldStandard, 1)-1)
            for j=3:size(GoldStandard, 2)
                for k=1:length(GoldStandard{i,j}{1}.Properties.VariableNames)
                    if(strcmp(GoldStandard{i,j}{1}.Properties.VariableNames{k}, 'JobID'))
                        continue;
                    end
                    if(isa(GoldStandard{i,j}{1}.(GoldStandard{i,j}{1}.Properties.VariableNames{k}), 'double'))
                        GoldStandard{i,j}{1}.(GoldStandard{i,j}{1}.Properties.VariableNames{k})(isnan(GoldStandard{i,j}{1}.(GoldStandard{i,j}{1}.Properties.VariableNames{k})))=0;
                    else
                        Indeces=cellfun('isempty', GoldStandard{i,j}{1}.(GoldStandard{i,j}{1}.Properties.VariableNames{k}));
                        if(sum(Indeces)>0)
                            Indeces=find(Indeces);
                            for ind=1:length(Indeces)
                                Others={};
                                for l=1:(size(GoldStandard, 1)-1)
                                    if(l==i)
                                        continue;
                                    end
                                    Others{end+1}=GoldStandard{l,j}{1}.(GoldStandard{l,j}{1}.Properties.VariableNames{k}){GoldStandard{l,j}{1}.JobID==GoldStandard{i,j}{1}.JobID(Indeces(ind))};
                                end
                                UniqueSelections=unique(Others);
                                UniqueSelectionsCount=zeros(size(UniqueSelections));
                                for l=1:length(UniqueSelections)
                                    UniqueSelectionsCount(l)=sum(strcmp(Others, UniqueSelections{l}));
                                end
                                [~, MaxIndex]=max(UniqueSelectionsCount);
                                GoldStandard{i,j}{1}.(GoldStandard{i,j}{1}.Properties.VariableNames{k}){Indeces(ind)}=UniqueSelections{MaxIndex};
                            end
                        end
                    end
                end
            end
        end
        
        Recorders=unique(GoldStandard.Recorder);
        Recorders=Recorders(~cellfun('isempty', Recorders'));
        Devices=unique(GoldStandard.Device);
        Devices=Devices(~cellfun('isempty', Devices'));
        if( (length(Recorders)>1) && (length(Devices)>1) )
            index=size(GoldStandard,1);
            GoldStandard(end+(length(Recorders) + length(Devices)), :)=GoldStandard(end, :);
            for r=1:length(Recorders)
                temp=AverageSubData(GoldStandard(strcmp(GoldStandard.Recorder, Recorders{r}) & ~strcmp(GoldStandard.Device, 'Combined') & ...
                                                 ~strcmp(GoldStandard.Recorder, '') & ~strcmp(GoldStandard.Device, ''), :), JobIDs);
                GoldStandard.Recorder{index}=Recorders{r};
                GoldStandard.Device{index}='Combined';
                GoldStandard.OD_RightLens{index}=temp.OD_RightLens;
                GoldStandard.OS_LeftLens{index}=temp.OS_LeftLens;
                GoldStandard.Pair{index}=temp.Pair;
                index=index+1;
            end
            for d=1:length(Devices)
                temp=AverageSubData(GoldStandard(~strcmp(GoldStandard.Recorder, 'Combined') & strcmp(GoldStandard.Device, Devices{d}) & ...
                                                 ~strcmp(GoldStandard.Recorder, '') & ~strcmp(GoldStandard.Device, ''), :), JobIDs);
                GoldStandard.Recorder{index}='Combined';
                GoldStandard.Device{index}=Devices{d};
                GoldStandard.OD_RightLens{index}=temp.OD_RightLens;
                GoldStandard.OS_LeftLens{index}=temp.OS_LeftLens;
                GoldStandard.Pair{index}=temp.Pair;
                index=index+1;
            end
        else
            GoldStandard(end+1, :)=GoldStandard(end, :);
        end

        temp=AverageSubData(GoldStandard(~strcmp(GoldStandard.Recorder, 'Combined') & ~strcmp(GoldStandard.Device, 'Combined') & ...
                                         ~strcmp(GoldStandard.Recorder, '') & ~strcmp(GoldStandard.Device, ''), :), JobIDs);
        GoldStandard.Recorder{end}='Combined';
        GoldStandard.Device{end}='Combined';
        GoldStandard.OD_RightLens{end}=temp.OD_RightLens;
        GoldStandard.OS_LeftLens{end}=temp.OS_LeftLens;
        GoldStandard.Pair{end}=temp.Pair;

        save('GoldStandard.mat', 'GoldStandard');
    else
        load('GoldStandard.mat')
    end
end



