function MultipleCorrelationAnalysis(ReferenceData, TargetData, DependantVariables, CorrelationAnalysisNumberOfBins)
    for i=1:length(DependantVariables)
        RefData=[];
        TarData=[];
        if(contains(DependantVariables{i}, '_Lens'))
            DependantVariables{i}=strrep(DependantVariables{i}, '_Lens', '');
            RefData=ANOVADataExtract(ReferenceData, {'Recorder', 'Device'}, DependantVariables{i});
            TarData=ANOVADataExtract(TargetData, {'Recorder', 'Device'}, DependantVariables{i});
            TarData.Properties.VariableNames{end}=horzcat('Lenses_', TarData.Properties.VariableNames{end}, '_Error');
        elseif(contains(DependantVariables{i}, '_Pair'))
            DependantVariables{i}=strrep(DependantVariables{i}, '_Pair', '');
            RefData=ANOVADataExtract(ReferenceData, {'Recorder', 'Device'}, DependantVariables{i}, true, true);
            TarData=ANOVADataExtract(TargetData, {'Recorder', 'Device'}, DependantVariables{i}, true, true);
            TarData.Properties.VariableNames{end}=horzcat('Pairs_', TarData.Properties.VariableNames{end}, '_Error');
        end
        if(~isempty(RefData) && ~isempty(TarData))
            CorrelationAnalysis(RefData, TarData, CorrelationAnalysisNumberOfBins);
        end
    end
end