function Result=PresenceReport(Presence, Print)
    if(~exist('Print', 'var'))
        Print=false;
    end
    Result.IndependentParmeters=Presence.Properties.VariableNames(~contains(Presence.Properties.VariableNames, 'NumberOfRecordsIn_'));
    Result.NumberOfRecordsIn=Presence.Properties.VariableNames(contains(Presence.Properties.VariableNames, 'NumberOfRecordsIn_'));
    for i=1:length(Result.IndependentParmeters)
        if(Print)
            disp(horzcat('There are ', num2str(length(unique(Presence.(Result.IndependentParmeters{i})))), ' ', Result.IndependentParmeters{i}, 's.'));
        end
    end
    Result.Count=table();
    Result.Uniques=table();
    for i=1:length(Result.IndependentParmeters)
        Uniques=unique(Presence.(Result.IndependentParmeters{i}));
        Result.Uniques.(Result.IndependentParmeters{i}){1}=Uniques';
        Result.Count.(Result.IndependentParmeters{i})=zeros(length(Result.NumberOfRecordsIn), length(Uniques));
        if(Print)
            disp(' ')
            disp(horzcat('---------- ', Result.IndependentParmeters{i}, ' ----------'))
        end
        for j=1:length(Uniques)
            if(Print)
                if(isa(Presence.(Result.IndependentParmeters{i}), 'double'))
                    Text=horzcat(Result.IndependentParmeters{i}, ' ', num2str(Uniques(j)), ' has');
                else
                    Text=horzcat(Uniques{j}, ' has');
                end
            end
            for k=1:length(Result.NumberOfRecordsIn)
                if(Print)
                    if(k==1)
                        Text=horzcat(Text, ' ');
                    else
                        Text=horzcat(Text, ' and ');
                    end
                end
                if(isa(Presence.(Result.IndependentParmeters{i}), 'double'))
                    Result.Count.(Result.IndependentParmeters{i})(k, j)=sum(Presence.(Result.NumberOfRecordsIn{k})(Presence.(Result.IndependentParmeters{i})==Uniques(j)));
                else
                    Result.Count.(Result.IndependentParmeters{i})(k, j)=sum(Presence.(Result.NumberOfRecordsIn{k})(strcmp(Presence.(Result.IndependentParmeters{i}),Uniques{j})));
                end
                if(Print)
                    Text=horzcat(Text, num2str(Result.Count.(Result.IndependentParmeters{i})(k, j)), ' ', strrep(Result.NumberOfRecordsIn{k}, 'NumberOfRecordsIn_', ''));
                end
            end
            if(Print)
                disp(horzcat(Text, ' Records.'))
            end
        end
    end
end