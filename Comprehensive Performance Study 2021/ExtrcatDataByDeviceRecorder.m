function SubData=ExtrcatDataByDeviceRecorder(Data, TargetIndicator, TargetField, Recorder, Device, Remove)
    %% Pre-Cehcks
    if(~exist('TargetIndicator', 'var'))
        TargetIndicator={};
    end
    if(~exist('TargetField', 'var'))
        TargetField='Any';
    end
    if(~exist('Recorder', 'var'))
        Recorder='Combined';
    end
    if(~exist('Device', 'var'))
        Device='Combined';
    end
    if(~exist('Remove', 'var'))
        Remove={};
    else
        if(~iscell(Remove))
            Remove={Remove};
        end
    end
    
    %% Overall SubData Extract
    Indeces=true(size(Data,1), 1);
    if(strcmp(Recorder, 'Not Combined'))
        Indeces = Indeces & ~strcmp(Data.Recorder, 'Combined');
    else
        Indeces = Indeces & strcmp(Data.Recorder, Recorder);
    end
    if(strcmp(Device, 'Not Combined'))
        Indeces = Indeces & ~strcmp(Data.Device, 'Combined');
    else
        Indeces = Indeces & strcmp(Data.Device, Device);
    end
    SubData=Data(Indeces, :);
    
    %% Required SubData Extract
    for i=1:length(SubData.Properties.VariableNames)
        if(~strcmp(SubData.Properties.VariableNames{i}, 'Recorder') && ~strcmp(SubData.Properties.VariableNames{i}, 'Device'))
            for k=1:size(SubData, 1)
                Temp=SubData.(SubData.Properties.VariableNames{i}){k};
                if(isempty(TargetIndicator))
                    Indeces=true(size(Temp,1),1);
                else
                    Indeces=false(size(Temp,1),1);
                end
                for j=1:length(Temp.Properties.VariableNames)
                    if(~isempty(TargetField) && ~strcmp(TargetField, 'Any') && ~strcmp(Temp.Properties.VariableNames{j}, TargetField))
                        continue;
                    end
                    if(~isempty(TargetIndicator))
                        if(isa(Temp.(Temp.Properties.VariableNames{j}), 'double') && isa(TargetIndicator, 'double'))
                            Indeces = Indeces | (Temp.(Temp.Properties.VariableNames{j})==TargetIndicator);
                        elseif(isa(Temp.(Temp.Properties.VariableNames{j}), 'cell') && isa(TargetIndicator, 'char'))
                            Indeces = Indeces | strcmp(Temp.(Temp.Properties.VariableNames{j}), TargetIndicator);
                        end
                    end
                    if(~isempty(Remove))
                        for l=1:length(Remove)
                            if(isa(Temp.(Temp.Properties.VariableNames{j}), 'double') && isa(Remove{l}, 'double'))
                                Indeces = Indeces & (Temp.(Temp.Properties.VariableNames{j})~=Remove{l});
                            elseif(isa(Temp.(Temp.Properties.VariableNames{j}), 'cell') && isa(Remove{l}, 'char'))
                                Indeces = Indeces & ~strcmp(Temp.(Temp.Properties.VariableNames{j}), Remove{l});
                            end
                        end
                    end
                end
                SubData.(SubData.Properties.VariableNames{i}){k}=Temp(Indeces,:);
            end
        end
    end
end