function Result=MultinomialLogisticRegression(MultinomialLogisticRegressionData, SignificanceLevel)
    if(isempty(MultinomialLogisticRegressionData))
        Result=[];
        return
    end
    X=zeros(size(MultinomialLogisticRegressionData,1), size(MultinomialLogisticRegressionData,2)-1); 
    for i=1:size(X,2)
        X(:,i)=grp2idx(categorical(MultinomialLogisticRegressionData{:,i}));
    end
    Y=MultinomialLogisticRegressionData{:,end};
    warning ('off','all');
    [~,~,Stats]=mnrfit(X,Y);
    warning ('on','all');
    
    disp(horzcat('Multinomial Logistic Regression Analysis for ',MultinomialLogisticRegressionData.Properties.VariableNames{end}))
    disp('------------------------------------------------------------')
    disp('Source                   d.f.           P-Value')
    for i=2:size(Stats.p,1)
        Result{i-1,1}=MultinomialLogisticRegressionData.Properties.VariableNames{i-1};
        Text=MultinomialLogisticRegressionData.Properties.VariableNames{i-1};
        while(length(Text)<25)
            Text=horzcat(Text,' ');
        end
        Result{i-1,2}=length(unique(MultinomialLogisticRegressionData{:,i-1}))-1;
        Text=horzcat(Text, num2str(Result{i-1,2}));
        while(length(Text)<40)
            Text=horzcat(Text,' ');
        end
        Result{i-1,3}=Stats.p(i,2);
        if((Stats.p(i,2)<SignificanceLevel) && ~contains(Text,'JobID'))
            Text=horzcat(Text, num2str(Stats.p(i,2),'%2.3f'), '***');
        else
            Text=horzcat(Text, num2str(Stats.p(i,2),'%2.3f'));
        end
        disp(Text)
    end
    disp(' ')
    disp(' ')
end