function BetweenIndependentParameterDifferenceAnalysisPlot(Data, DrawMarginalMeans, DrawCrossTabulationPlots)
    DataScalers={};
    DataCategorical={};
    for i=1:length(Data)
        if(sum(strcmp(fieldnames(Data{i}), 'CrossTabulationTable')))
            DataCategorical{end+1}=Data{i};
        else
            DataScalers{end+1}=Data{i};
        end
    end
    PlotCombinedScalerDifferences(DataScalers);
    if(DrawCrossTabulationPlots)
        PlotSeperatedScalerDifferences(DataScalers);
    end
    if(DrawMarginalMeans)
        PlotMarginalMeans(DataScalers);
    end
    DisplayCategoricalComparisons(DataCategorical);
end

function PlotMarginalMeans(Data)
    if(isempty(Data))
        return;
    end
    for i=1:length(Data)
        if(isempty(Data{i}.Statistics))
            continue;
        end
        fig=figure('Position', [2049 41 2048 1.0368e+03]);
        multcompare(Data{i}.Statistics,'Dimension',[1 2]);
        Title=horzcat('Comparison Between ', Data{i}.RawTable.Properties.VariableNames{end});
        if(contains(Data{i}.RawTable.JobID{1}, 'Lens'))
            Title=horzcat(Title, ' (Individual Lenses) measured in different ');
        else
            Title=horzcat(Title, ' (Pairs) measured in different ');
        end
        IndependantVariableNames=Data{i}.RawTable.Properties.VariableNames(1:end-1);
        IndependantVariableNames(strcmp(IndependantVariableNames, 'JobID'))=[];
        for j=1:length(IndependantVariableNames)
            if(i>1)
                Title=horzcat(Title, '& ', IndependantVariableNames{j});
            else
                Title=horzcat(Title, IndependantVariableNames{j});
            end
        end
        title(Title);
        if(contains(Data{i}.RawTable.JobID{1}, 'Lens'))
            fig.Name=horzcat(Data{i}.RawTable.Properties.VariableNames{end}, ' Individual Lenses - Marginal Mean Comparison');
        else
            fig.Name=horzcat(Data{i}.RawTable.Properties.VariableNames{end}, ' Pairs - Marginal Mean Comparison');
        end
    end
end

function PlotSeperatedScalerDifferences(Data)
    if(isempty(Data))
        return;
    end
    for i=1:length(Data)
        switch(size(Data{i}.RawTable, 2)-2) % the JOB ID and the measure aside, the rest are significant independant variables:
            case 0
                PlotCombinedData(Data{i}.RawTable);
            case 1
                Plot1DCrossTabulation(Data{i}.RawTable);
            case 2
                Plot2DCrossTabulation(Data{i}.RawTable);
            case 3
                Plot3DCrossTabulation(Data{i}.RawTable);
        end
    end
end

function PlotCombinedScalerDifferences(Data)
    if(isempty(Data))
        return;
    end
    BoxPlotDataLength=0;
    NumberOfBoxPlots=0;
    for i=1:length(Data)
        if(~isempty(Data{i}.DifferenceTable))
            ErrorData=Data{i}.DifferenceTable{:,2}(:);
            ErrorData(isnan(ErrorData))=[];
            BoxPlotDataLength=BoxPlotDataLength+size(ErrorData, 1);
            NumberOfBoxPlots=NumberOfBoxPlots+1;
        end
    end
    Result=table( zeros(NumberOfBoxPlots, 1), zeros(NumberOfBoxPlots, 1), ...
                  zeros(NumberOfBoxPlots, 1), zeros(NumberOfBoxPlots, 1), zeros(NumberOfBoxPlots, 1), ...
                  zeros(NumberOfBoxPlots, 1), zeros(NumberOfBoxPlots, 1), ...
                  zeros(NumberOfBoxPlots, 1), cell(NumberOfBoxPlots, 1), ...
                  'VariableNames', {'Mean', 'STD', ...
                                    'Median', 'Percentile25th', 'Percentile75th', ...
                                    'LowerAdjacent', 'UpperAdjacent', ...
                                    'DifferentFromZeroPValue', 'RawData'});
    if(BoxPlotDataLength>0)
        BoxPlotData=zeros(BoxPlotDataLength, 2);
    end
    XTicksLabels=cell(NumberOfBoxPlots,1);
    StartIndex=1;
    BoxPlotCounter=1;
    for i=1:length(Data)
        if(isempty(Data{i}.DifferenceTable))
            continue;
        end
        XTicksLabels{BoxPlotCounter}=strrep(strrep(Data{i}.DifferenceTable.Properties.VariableNames{end},'_',' '),'Error','');
        ErrorData=Data{i}.DifferenceTable{:,2}(:);
        ErrorData(isnan(ErrorData))=[];
        EndIndex=StartIndex+size(ErrorData, 1)-1;
        BoxPlotData(StartIndex:EndIndex, 1)=ErrorData;
        BoxPlotData(StartIndex:EndIndex, 2)=BoxPlotCounter;
        StartIndex=EndIndex+1;
        
        Result.RawData{BoxPlotCounter}=ErrorData;
        Result.Mean(BoxPlotCounter)=mean(ErrorData);
        Result.STD(BoxPlotCounter)=std(ErrorData);
        Result.Median(BoxPlotCounter)=median(ErrorData);
        Result.Percentile25th(BoxPlotCounter)=prctile(ErrorData, 25);
        Result.Percentile75th(BoxPlotCounter)=prctile(ErrorData, 75);
        [~, Result.DifferentFromZeroPValue(BoxPlotCounter)]=ttest(ErrorData);
        BoxPlotCounter=BoxPlotCounter+1;
    end
    
    fig=figure('Position', [2049 41 2048 1.0368e+03], 'name', 'Combined Scaler Difference Variations');
    hold on
    grid on
    boxplot(BoxPlotData(:,1), BoxPlotData(:,2), 'labels', XTicksLabels);
    set(findobj(gca,'type','line'), 'linew', 2);
    ylabel('Absolute Difference Across All Possible Comparisons');
    title('Error Analysis of Significantly Different Groups');
    set(gca,'fontsize', 15);
    if(length(XTicksLabels)>3)
        xtickangle(25);
    end
    up_adj=get(findobj(gcf, 'tag', 'Upper Adjacent Value'),'YData');
    lower_adj=get(findobj(gcf, 'tag', 'Lower Adjacent Value'),'YData');
    if(iscell(up_adj))
        up_adj=cell2mat(up_adj);
    end
    if(iscell(lower_adj))
        lower_adj=cell2mat(lower_adj);
    end
    ylim([min(min(lower_adj)) - (abs(min(min(lower_adj))) * 0.1), max(max(up_adj))*1.1]);
    
    for i=1:size(Result, 1)
        Result.UpperAdjacent(i)=up_adj(end-i+1, 1);
        Result.LowerAdjacent(i)=lower_adj(end-i+1, 1);
    end
    
    dcm_obj = datacursormode(fig);
    set(dcm_obj,'UpdateFcn',{@DataCursorFunction, Result})
end

function DisplayCategoricalComparisons(Data)
    if(isempty(Data))
        return;
    end
    disp('==================== Cross Tabulation Tables ====================')
    for i=1:length(Data)
        disp(Data{i}.CrossTabulationTable{1,1})
        disp('-----------------------------------------------------------------')
        for r=1:size(Data{i}.CrossTabulationTable, 1)
            Text='';
            for c=1:size(Data{i}.CrossTabulationTable, 2)
                if((r==1) && (c==1))
                    if(size(Data{i}.CrossTabulationTable, 2)>2)
                        Text='                    ';
                    end
                else
                    Text=horzcat(Text,Data{i}.CrossTabulationTable{r,c});
                    while(rem(length(Text), 40)~=0)
                        Text=horzcat(Text,' ');
                    end
                end
            end
            if(~isempty(Text))
                disp(Text)
            end
        end
        disp(' ')
        disp(' ')
    end
    
    disp('======================== One By One Comparison ========================')
    for i=1:length(Data)
        disp(Data{i}.CrossTabulationTable{1,1})
        disp('-----------------------------------------------------------------------')
        for r=1:size(Data{i}.OneByOneComparisonTable, 1)
            Text='';
            for c=1:size(Data{i}.OneByOneComparisonTable, 2)
                Text=horzcat(Text,Data{i}.OneByOneComparisonTable{r,c});
                while(rem(length(Text),40)~=0)
                    Text=horzcat(Text,' ');
                end
            end
            disp(Text)
        end
        disp(' ')
        disp(' ')
    end
end


function txt = DataCursorFunction(~,event_obj, Result)
    % Customizes text of data tips
    pos = get(event_obj,'Position');
    
    MainRangeCoverage=Result.RawData{pos(1)};
    MainRangeCoverage=MainRangeCoverage((MainRangeCoverage>=Result.Percentile25th(pos(1))) & (MainRangeCoverage<=Result.Percentile75th(pos(1))));
    MainRangeCoverage=length(MainRangeCoverage)/length(Result.RawData{pos(1)})*100;
    
    txt={['Mean: ', num2str(Result.Mean(pos(1)), '%2.2f')], ...
         ['STD: ', num2str(Result.STD(pos(1)), '%2.2f')], ...
         ['Median: ', num2str(Result.Median(pos(1)), '%2.2f')], ...
         ['Lower Adjacent: ', num2str(Result.LowerAdjacent(pos(1)), '%2.2f')], ...
         ['25th Percentile: ', num2str(Result.Percentile25th(pos(1)), '%2.2f')], ...
         ['75th Percentile: ', num2str(Result.Percentile75th(pos(1)), '%2.2f')], ...
         ['Upper Adjacent: ', num2str(Result.UpperAdjacent(pos(1)), '%2.2f')], ...
         ['Number of Samples: ', num2str(size(Result.RawData{pos(1)}, 1), '%2.0f')], ...
         ['Main Range Coverage: ', num2str(round(MainRangeCoverage), '%2.0f'), '%']};
%          ['Comopare to Zero TTest P-Value: ', num2str(Result.DifferentFromZeroPValue(pos(1)), '%2.3f')]};
     
    DataCursors=findall(gcf, 'Type', 'hggroup');
    for i=1:length(DataCursors)
        if(isempty(DataCursors(i).Tag))
            set(DataCursors(i),'FontSize',15);
        end
    end
end

