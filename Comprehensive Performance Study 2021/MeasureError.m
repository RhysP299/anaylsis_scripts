function ErrorData=MeasureError(MeasuredData, GoldenStandardData)
    ClassificationResult={'False'; 'True'};
    ErrorData=GoldenStandardData;
    for i=1:size(GoldenStandardData, 1)
        ErrorData.Recorder{i}=horzcat('Measured by ', MeasuredData.Recorder{1}, ' vs Golden Standard by ', GoldenStandardData.Recorder{i});
        if(strcmp(GoldenStandardData.Recorder{i}, 'Combined'))
            ErrorData.Recorder{i}=horzcat(ErrorData.Recorder{i}, ' Recorders');
        end
        ErrorData.Device{i}=horzcat('Measured on ', MeasuredData.Device{1}, ' vs Golden Standard on ', GoldenStandardData.Device{i});
        if(strcmp(GoldenStandardData.Device{i}, 'Combined'))
            ErrorData.Device{i}=horzcat(ErrorData.Device{i}, ' Devices');
        end
        for j=1:length(ErrorData.Properties.VariableNames)
            if(~strcmp(ErrorData.Properties.VariableNames{j}, 'Recorder') && ~strcmp(ErrorData.Properties.VariableNames{j}, 'Device'))
                ErrorData.(ErrorData.Properties.VariableNames{j}){i}=MeasuredData.(ErrorData.Properties.VariableNames{j}){1};
                UniqueJobIDs=unique(ErrorData.(ErrorData.Properties.VariableNames{j}){i}.JobID);
                for k=1:length(UniqueJobIDs)
                    MeasuredIndeces=(ErrorData.(ErrorData.Properties.VariableNames{j}){i}.JobID==UniqueJobIDs(k));
                    GoldenStandardIndeces=(GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}.JobID==UniqueJobIDs(k));
                    if(sum(GoldenStandardIndeces)==1) % Extra check - IT MUST BE ONLY ONE MEASURMENT
                        DependantParameters=GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}.Properties.VariableNames;
                        DependantParameters(strcmp(DependantParameters, 'JobID'))=[];
                        for l=1:length(DependantParameters)
                            if(isa(GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}.(DependantParameters{l}), 'double'))
                                % Measured - Golden Standard
                                % If POSITIVE that means that we have OVER-ESTIMATED (considering absolute value)
                                % If NEGATIVE that means that we have UNDE-ESTIMATED (considering absolute value)
                                ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l})(MeasuredIndeces) = ...
                                           ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l})(MeasuredIndeces) - ...
                                           GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}.(DependantParameters{l})(GoldenStandardIndeces);
                            else
%                                 tempmeasured=ErrorData.(ErrorData.Properties.VariableNames{j}){i}(MeasuredIndeces, :);
%                                 tempgolden=GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}(GoldenStandardIndeces, :);
                                Indeces=find(MeasuredIndeces);
                                for ind=1:length(Indeces)
                                    if(~strcmp(GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}.(DependantParameters{l}){GoldenStandardIndeces}, 'TIE'))
                                        ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l}){Indeces(ind)} = horzcat( ...
                                                   ClassificationResult{ strcmp(ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l}){Indeces(ind)}, ...
                                                                         GoldenStandardData.(ErrorData.Properties.VariableNames{j}){1}.(DependantParameters{l}){GoldenStandardIndeces}) + 1 }, ' ', ...
                                                                         ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l}){Indeces(ind)} );
                                    else
                                        ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l}){Indeces(ind)}=horzcat( 'Uncertain ', ...
                                                                         ErrorData.(ErrorData.Properties.VariableNames{j}){i}.(DependantParameters{l}){Indeces(ind)} );
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end