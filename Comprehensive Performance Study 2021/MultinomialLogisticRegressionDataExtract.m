function MultinomialLogisticRegressionData=MultinomialLogisticRegressionDataExtract(Data, IndependentVariables, DependantVariable, IncludePair, ExcludeIndividualLenses)
    %% Pre-Checks
    if(~iscell(IndependentVariables))
        IndependentVariables={IndependentVariables};
    end
    if(length(IndependentVariables)<1)
        error('There should be at least 1 independent variables.')
    end
    if(iscell(DependantVariable) || ~ischar(DependantVariable))
        error('There must be 1 dependant variable.')
    end
    if(~exist('IncludePair', 'var'))
        IncludePair=false;
    else
        if(IncludePair~=0)
            IncludePair=true;
        end
    end
    if(~exist('ExcludeIndividualLenses', 'var'))
        ExcludeIndividualLenses=false;
    else
        if(ExcludeIndividualLenses~=0)
            ExcludeIndividualLenses=true;
        end
    end
    
    %% Calculating the Length
    MultinomialLogisticRegressionDataLength=0;
    if(~ExcludeIndividualLenses &&(sum(strcmp(Data.OD_RightLens{1}.Properties.VariableNames, DependantVariable))==1))
        MultinomialLogisticRegressionDataLength=MultinomialLogisticRegressionDataLength+size(Data.OD_RightLens{1}, 1);
    end
    if(~ExcludeIndividualLenses && (sum(strcmp(Data.OS_LeftLens{1}.Properties.VariableNames, DependantVariable))==1))
        MultinomialLogisticRegressionDataLength=MultinomialLogisticRegressionDataLength+size(Data.OS_LeftLens{1}, 1);
    end
    if(IncludePair && (sum(strcmp(Data.Pair{1}.Properties.VariableNames, DependantVariable))==1))
        MultinomialLogisticRegressionDataLength=MultinomialLogisticRegressionDataLength+size(Data.Pair{1}, 1);
    end
    MultinomialLogisticRegressionDataLength=MultinomialLogisticRegressionDataLength*size(Data, 1);
    
    %% Copying the corresponding Data
    if(MultinomialLogisticRegressionDataLength>0)
        MultinomialLogisticRegressionData=table();
        for i=1:length(IndependentVariables)
            ISCell=true;
            if(sum(strcmp(Data.Properties.VariableNames, IndependentVariables{i}))==0)
                if(sum(strcmp(Data.OD_RightLens{1}.Properties.VariableNames, IndependentVariables{i}))>0)
                    if(isa(Data.OD_RightLens{1}.(IndependentVariables{i})(end),'double'))
                        ISCell=false;
                    end
                end
            end
            if(ISCell)
                MultinomialLogisticRegressionData.(IndependentVariables{i})=cell(MultinomialLogisticRegressionDataLength, 1);
            else
                MultinomialLogisticRegressionData.(IndependentVariables{i})=zeros(MultinomialLogisticRegressionDataLength, 1);
            end
        end
        MultinomialLogisticRegressionData.JobID=cell(MultinomialLogisticRegressionDataLength, 1);
        MultinomialLogisticRegressionData.(DependantVariable)=categorical(zeros(MultinomialLogisticRegressionDataLength, 1));
        for i=1:size(Data, 1)
            TempIndependentVariables=IndependentVariables;
            StartIndex=(((MultinomialLogisticRegressionDataLength/size(Data, 1)) * (i-1)) + 1);
            EndIndex=(MultinomialLogisticRegressionDataLength/size(Data, 1)*i);
            for j=1:length(IndependentVariables)
                if(sum(strcmp(Data.Properties.VariableNames, TempIndependentVariables{j}))>0)
                    MultinomialLogisticRegressionData(StartIndex:EndIndex, j)=Data{i, strcmp(Data.Properties.VariableNames, IndependentVariables{j})};
                    TempIndependentVariables{j}=[];
                end
            end
            TempIndependentVariables=TempIndependentVariables(~cellfun('isempty', TempIndependentVariables));
            if(~ExcludeIndividualLenses && (sum(strcmp(Data.OD_RightLens{1}.Properties.VariableNames, DependantVariable))==1))
                EndIndex=StartIndex+size(Data.OD_RightLens{i}, 1)-1;
                MultinomialLogisticRegressionData.JobID(StartIndex:EndIndex)=cellstr(strcat(num2str(Data.OD_RightLens{i}.JobID), '_OD_RightLens'));
                MultinomialLogisticRegressionData.(DependantVariable)(StartIndex:EndIndex)=categorical(Data.OD_RightLens{i}.(DependantVariable));
                if(~isempty(TempIndependentVariables))
                    for j=1:length(TempIndependentVariables)
                        MultinomialLogisticRegressionData.(TempIndependentVariables{j})(StartIndex:EndIndex)=Data.OD_RightLens{i}.(TempIndependentVariables{j});
                    end
                end
                StartIndex=EndIndex+1;
            end
            if(~ExcludeIndividualLenses && (sum(strcmp(Data.OS_LeftLens{1}.Properties.VariableNames, DependantVariable))==1))
                EndIndex=StartIndex+size(Data.OS_LeftLens{i}, 1)-1;
                MultinomialLogisticRegressionData.JobID(StartIndex:EndIndex)=cellstr(strcat(num2str(Data.OS_LeftLens{i}.JobID), '_OS_LeftLens'));
                MultinomialLogisticRegressionData.(DependantVariable)(StartIndex:EndIndex)=categorical(Data.OS_LeftLens{i}.(DependantVariable));
                if(~isempty(TempIndependentVariables))
                    for j=1:length(TempIndependentVariables)
                        MultinomialLogisticRegressionData.(TempIndependentVariables{j})(StartIndex:EndIndex)=Data.OS_LeftLens{i}.(TempIndependentVariables{j});
                    end
                end
                StartIndex=EndIndex+1;
            end
            if(IncludePair && (sum(strcmp(Data.Pair{1}.Properties.VariableNames, DependantVariable))==1))
                EndIndex=StartIndex+size(Data.Pair{i}, 1)-1;
                MultinomialLogisticRegressionData.JobID(StartIndex:EndIndex)=cellstr(strcat(num2str(Data.Pair{i}.JobID), '_Pair'));
                MultinomialLogisticRegressionData.(DependantVariable)(StartIndex:EndIndex)=categorical(Data.Pair{i}.(DependantVariable));
                if(~isempty(TempIndependentVariables))
                    for j=1:length(TempIndependentVariables)
                        MultinomialLogisticRegressionData.(TempIndependentVariables{j})(StartIndex:EndIndex)=Data.Pair{i}.(TempIndependentVariables{j});
                    end
                end
                StartIndex=EndIndex+1;
            end
        end
    else
        MultinomialLogisticRegressionData=[];
    end
end