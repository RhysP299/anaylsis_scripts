# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 10:56:06 2021
A selection of classess and functions for prescription outputs from the eMap.
@author: RhysPoolman
"""
import numpy as np
import time
from enum import Enum


class TintedType(Enum):
    """
    The tinted lens type identifiers.  See
    EyeTech.Recipes.Lensmeter.Portable.Enums.TintedType
    """
    # It is not set by caller.
    Undefined = "Undefined"

    # Defines an unknown tintedType.
    # Reserved Value
    # Unknown = "Unknown",

    # Clear lens, no tint
    Clear = "Clear"

    # Tinted lens
    Tinted = "Tinted"


class LensType(Enum):
    """
    The lens type identifiers.  See
    EyeTech.Recipes.Lensmeter.Portable.Enums.LensType
    """
    # It is not set by caller. This can also be indicated as a failure for a
    # result e.g. unable to find reference points or detect correct lens type.
    Undefined = "Undefined"  # do NOT change

    # Defines an unknown lenstype.
    # This is usually used in a LensTypeHint to indicate we intend to perform
    # an automatic detection (meaning we treat this value as an input to a
    # method).
    Unknown = "Unknown"  # do NOT change

    # Single vision lens
    Single = "Single"  # do NOT change

    # Progressive lens
    Progressive = "Progressive"  # do NOT change

    # Bifocal lens.
    Bifocal = "Bifocal"  # do NOT change

    # Degressive lens
    Degressive = "Degressive"  # do NOT change


class DCSLensType(Enum):
    """
    The lens type (LTYPE) defined in DCS-v3_12_FINAL.pdf on page 129.  This
    is not an exhaustive list.
    """
    single_vision = "SV"
    near_variable_focus = "NV"
    bifocal = "BF"
    trifocal = "TF"
    quadrafocal = "QF"
    double_segment = "DS"
    progressive_addition = "PR"


class PrescriptionType(Enum):
    """
    Indicates if the prescription type is EffectivePower, BackVertexPower
    (BackPower) or undefined.
    See EyeTech.Recipes.Lensmeter.Portable.Enums.PrescriptionType
    """
    # Not defined (initial value)
    Undefined = "Undefined"  # do NOT change; webpages use actual int value

    # Effective Power Prescription type
    EffectivePower = "EffectivePower"  # do NOT change; webpages use actual int
    # value

    # Back Power Prescription type
    BackVertexPower = "BackVertexPower"  # do NOT change; webpages use actual
    # int value

    # FrontVertexPower = 7 or next value?,	... should have been 4 but if we
    # ever walked this path, see if we can reassign values in the db
    # ApproximatePower = 8 ... should have been 4 but if we ever walked this
    # path, see if we can reassign values in the db

    # User Entered Prescription type
    UserEntered = "UserEntered"  # do NOT change; webpages use actual int value

    # Lms Entered (Populated using Lms system) Prescription type
    LmsEntered = "LmsEntered"  # do NOT change; webpages use actual int value

    # Compensated Prescription type
    Compensated = "Compensated"  # do NOT change; webpages use actual int value


class PrescriptionSection(Enum):
    ep = "EP"
    bp = "BP"
    none = ""


class LensDataContents(Enum):
    """
    Used to indicate whether the left, right, both None lenses are present in
    an OMA file.
    """
    both = "B"
    left = "L"
    right = "R"
    none = "N"


class FrameType(Enum):
    """
    Used to indicate the type of frame in glazed optics. Values are defined in
    DCS-v3_12_FINAL.pdf found here:
    C:\\Users\\RhysPoolman\\OneDrive - EYOTO GROUP LTD\\Documents\\Standards
    """
    undefined = 0
    plastic = 1
    metal = 2
    rimless = 3
    optyl = 4


class Prescription:
    """
    A class to manage the data in prescription.csv in the debug measurement
    folders from the eMap
    """

    def __init__(self, path):
        """
        Loads the data in prescription.csv found in the specified path.

        Parameters
        ----------
        path : string
            The path in which the prescripition.csv file to be read is found.

        Returns
        -------
        None.
        """
        # create empty dict variables
        self._prescription_section = False
        self._path = path
        self._is_bp_approximated = {}
        self._is_lens_tinted = {}
        self._single_vision_lens_measurement_location = {}
        self._monocular_pupillary_distance = {}
        self._sphere_effective_power = {}
        self._secondary_principle_plane_offset_from_back_vertex = {}
        self._base_curve = {}
        self._lens_center_thickness = {}
        self._refractive_index_lens = {}
        self._refractive_index_air = {}
        self._lms_base_curve_lens = {}
        self._lms_base_curve_lens = {}
        self._lms_center_thickness_lens = {}
        self._lms_refractive_index_lens = {}
        self._lens_prescription_type_hint = {}
        self._lens_prescription_type_detected = {}
        self._distance_reference_point_height = {}
        self._near_pupillary_distance = {}
        self._optical_center_reference_point = {}
        self._ep_prescription_result_success = {}
        self._measurement_power_specification_for_prescription_type = {}
        self._axis = {}
        self._axis_at_near_reference_point = {}
        self._axis_at_distance_reference_point = {}
        self._sphere_at_near_reference_point = {}
        self._sphere_at_distance_reference_point = {}
        self._cylinder_at_near_reference_point = {}
        self._cylinder_at_distance_reference_point = {}
        self._spherical_equivalent_at_near_reference_point = {}
        self._spherical_equivalent_at_distance_reference_point = {}
        self._corridor_length = {}
        self._corridor_width = {}
        self._distance_corridor_points = {}
        self._near_corridor_points = {}
        self._prism_diopters = {}
        self._prism_base_angle = {}
        self._prism_reference_point_offset_from_distance_reference_point_mm = \
            {}
        self._near_reference_point_inset_mm = {}
        self._distance_between_measuring_lens_and_screen_mm = {}
        self._lens_center_point = {}
        self._lens_distance_reference_point = {}
        self._lens_distance_region_radius = {}
        self._lens_near_reference_point = {}
        self._lens_near_region_radius = {}
        self._lens_prism_reference_point = {}
        self._point_of_least_prism_change = {}
        self._monocular_pupillary_distance_prescription = {}
        self._monocular_distance_between_lenses_mm = {}
        self._lens_distance_reference_point_height_mm = {}
        self._refractive_index = {}
        self._central_thickness = {}
        self._base_curve_prescription = {}
        self._engraving_point_left = {}
        self._engraving_point_right = {}
        self._engraving_offset_angle = {}
        self._degression = {}
        self._spherical_values_along_corridor_line = {}
        # fill dicts
        prescription_type_key = PrescriptionSection.none
        with open(path + "Prescriptions.csv") as file:
            lines = file.readlines()
            for ii, line in enumerate(lines):
                if "EP" in line:
                    prescription_type_key = PrescriptionSection.ep
                elif "BP" in line:
                    prescription_type_key = PrescriptionSection.bp
                if "SphericalValuesAlongCorridorLine" in line:
                    line = line + " " + lines[ii + 2]
                    prescription_type_key = PrescriptionSection.none
                self._extract_result_from_string(line, prescription_type_key)

    def _extract_result_from_string(self, line, prescription_type_key):
        """
        Extract data for the attributes from a line in a prescriptions.csv file

        Parameters
        ----------
        line: string
            The line in the Prescriptions.csv that contains data for an
            attribute and a string to define which attribute the data is
            intended for.  These are seperated by a colon.
        prescription_type_key: PrescriptionSection
            A PrescriptionSection enum that lets the method know if the data is
            from the EP, BP or unspecified section of Prescritions.csv file.
        """
        # split line into variable name and value and extract key
        result = line.split(": ")
        split_result = result[0].split()
        # get key for value type
        key = None
        if len(split_result) < 1:
            pass
        elif len(result) < 3 and ("Cam" in split_result[0] or
                                  "LMS" in split_result[0]):
            key = split_result[0] if len(split_result) > 0 else None
            self._prescription_section = False
        elif "->" in result[0]:
            key = result[0].split()[1]
            self._prescription_section = True
            self._key = key
        elif self._prescription_section:
            key = self._key
        # get assign value to variable
        if "The following " in result[0] or \
                "Cam1 is left and " in result[0]:
            pass
        elif "Measurement time" in result[0]:
            self._measurement_time = time.strptime(result[1].split(".")[0],
                                                   "%Y-%m-%dT%H:%M:%S")
        elif "Binocular pupillary distance (mm)" in result[0]:
            self._binocular_pupillary_distance = float(result[1])
        elif " Is BP non-approximated" in result[0]:
            self._is_bp_approximated[key] = not bool(result[1])
        elif " Is lens tinted" in result[0]:
            self._is_lens_tinted[key] = TintedType(result[1].strip())
        elif "single vision lens measurement location" in result[0]:
            self._single_vision_lens_measurement_location[key] = result[1]
        elif "monocular pupillary distance" in result[0]:
            self._monocular_pupillary_distance[key] = float(result[1])
        elif "Sphere effective power DRP" in result[0]:
            self._sphere_effective_power[key] = float(result[1])
        elif "Secondary principle plane offset from back vertex" in result[0]:
            self._secondary_principle_plane_offset_from_back_vertex[key] = \
                float(result[1])
        elif "Base curve" in result[0]:
            self._base_curve[key] = float(result[1])
        elif "Lens center thickness" in result[0]:
            self._lens_center_thickness[key] = float(result[1])
        elif " Refractive index lens" in result[0]:
            self._refractive_index_lens[key] = float(result[1])
        elif " Refractive index air" in result[0]:
            self._refractive_index_air[key] = float(result[1])
        elif "LMS Base curve lens" in result[0]:
            self._lms_base_curve_lens[key] = float(result[1])
        elif "LMS Center thickness lens" in result[0]:
            self._lms_center_thickness_lens[key] = float(result[1])
        elif "LMS Refractive index lens" in result[0]:
            self._lms_refractive_index_lens[key] = float(result[1])
        elif "Lens prescription type (hinted)" in result[0]:
            self._lens_prescription_type_hint = LensType(result[1].strip())
        elif "Lens prescription type (detected)" in result[0]:
            self._lens_prescription_type_detected = result[1]
        elif "Distance reference point height" in result[0]:
            self._distance_reference_point_height = float(result[1])
        elif "Near pupillary distance" in result[0]:
            self._near_pupillary_distance = float(result[1])
        elif "Optical center reference point" in result[0]:
            point_string = result[1].strip().split(",")
            self._optical_center_reference_point[key] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "->" in result[0] and "EP prescription" in result[0]:
            self._ep_prescription_result_success[key] = "Success" in result[2]
        elif "MeasurementPowerSpecificationForPrescriptionType" in result[0]:
            if key not in self._measurement_power_specification_for_prescription_type.keys():
                self._measurement_power_specification_for_prescription_type[key] = {}
            self._measurement_power_specification_for_prescription_type[key][prescription_type_key.value] = \
                PrescriptionType(result[1].strip())
        elif "Axis" in result[0]:
            if key not in self._axis.keys():
                self._axis[key] = {}
            self._axis[key][prescription_type_key.value] = float(result[1])
        elif "AxisAtNearReferencePoint" in result[0]:
            if key not in self._axis_at_near_reference_point.keys():
                self._axis_at_near_reference_point[key] = {}
            self._axis_at_near_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "AxisAtDistanceReferencePoint" in result[0]:
            if key not in self._axis_at_distance_reference_point.keys():
                self._axis_at_distance_reference_point[key] = {}
            self._axis_at_distance_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "SphereAtNearReferencePoint" in result[0]:
            if key not in self._sphere_at_near_reference_point.keys():
                self._sphere_at_near_reference_point[key] = {}
            self._sphere_at_near_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "SphereAtDistanceReferencePoint" in result[0]:
            if key not in self._sphere_at_distance_reference_point.keys():
                self._sphere_at_distance_reference_point[key] = {}
            self._sphere_at_distance_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "CylinderAtNearReferencePoint" in result[0]:
            if key not in self._cylinder_at_near_reference_point.keys():
                self._cylinder_at_near_reference_point[key] = {}
            self._cylinder_at_near_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "CylinderAtDistanceReferencePoint" in result[0]:
            if key not in self._cylinder_at_distance_reference_point.keys():
                self._cylinder_at_distance_reference_point[key] = {}
            self._cylinder_at_distance_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "SphericalEquivalentAtNearReferencePoint" in result[0]:
            if key not in \
                    self._spherical_equivalent_at_near_reference_point.keys():
                self._spherical_equivalent_at_near_reference_point[key] = {}
            self._spherical_equivalent_at_near_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "SphericalEquivalentAtDistanceReferencePoint" in result[0]:
            if key not in self._spherical_equivalent_at_distance_reference_point.keys():
                self._spherical_equivalent_at_distance_reference_point[key] = \
                    {}
            self._spherical_equivalent_at_distance_reference_point[key][prescription_type_key.value] = \
                float(result[1])
        elif "CorridorLength" in result[0]:
            if key not in self._corridor_length.keys():
                self._corridor_length[key] = {}
            self._corridor_length[key][prescription_type_key.value] = \
                float(result[1])
        elif "CorridorWidth" in result[0]:
            if key not in self._corridor_width.keys():
                self._corridor_width[key] = {}
            self._corridor_width[key][prescription_type_key.value] = \
                float(result[1])
        elif "DistanceCorridorPoints" in result[0]:
            if key not in self._distance_corridor_points.keys():
                self._distance_corridor_points[key] = {}
            points_string = result[1].strip().split(",")
            point1 = np.array([float(points_string[0]),
                               float(points_string[1])])
            point2 = np.array([float(points_string[2]),
                               float(points_string[3])])
            self._distance_corridor_points[key][prescription_type_key.value] = \
                {"point1": point1, "point2": point2}
        elif "NearCorridorPoints" in result[0]:
            if key not in self._near_corridor_points.keys():
                self._near_corridor_points[key] = {}
            points_string = result[1].strip().split(",")
            point1 = np.array([float(points_string[0]),
                               float(points_string[1])])
            point2 = np.array([float(points_string[2]),
                               float(points_string[3])])
            self._near_corridor_points[key][prescription_type_key.value] = \
                {"point1": point1, "point2": point2}
        elif "PrismDiopters" in result[0]:
            if key not in self._prism_diopters.keys():
                self._prism_diopters[key] = {}
            self._prism_diopters[key][prescription_type_key.value] = \
                float(result[1])
        elif "PrismBaseAngleDegrees" in result[0]:
            if key not in self._prism_base_angle.keys():
                self._prism_base_angle[key] = {}
            self._prism_base_angle[key][prescription_type_key.value] = \
                float(result[1])
        elif "PrismReferencePointOffsetFromDistanceReferencePointMM" in \
                result[0]:
            if key not in self._prism_reference_point_offset_from_distance_reference_point_mm.keys():
                self._prism_reference_point_offset_from_distance_reference_point_mm[key] = {}
            self._prism_reference_point_offset_from_distance_reference_point_mm[key] = \
                float(result[1])
        elif "NearReferencePointInsetMM" in result[0]:
            if key not in self._near_reference_point_inset_mm.keys():
                self._near_reference_point_inset_mm[key] = {}
            self._near_reference_point_inset_mm[key][prescription_type_key.value] = \
                float(result[1])
        elif "DistanceBetweenMeasuringLensAndScreenMM" in result[0]:
            if key not in \
                    self._distance_between_measuring_lens_and_screen_mm.keys():
                self._distance_between_measuring_lens_and_screen_mm[key] = {}
            self._distance_between_measuring_lens_and_screen_mm[key][prescription_type_key.value] = \
                float(result[1])
        elif "LensCenterPoint" in result[0]:
            if key not in self._lens_center_point.keys():
                self._lens_center_point[key] = {}
            point_string = result[1].strip().split(",")
            self._lens_center_point[key][prescription_type_key.value] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "LensDistanceReferencePoint (x" in result[0]:
            if key not in self._lens_distance_reference_point.keys():
                self._lens_distance_reference_point[key] = {}
            point_string = result[1].strip().split(",")
            self._lens_distance_reference_point[key][prescription_type_key.value] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "LensDistanceRegionRadius" in result[0]:
            if key not in self._lens_distance_region_radius.keys():
                self._lens_distance_region_radius[key] = {}
            self._lens_distance_region_radius[key][prescription_type_key.value] = \
                float(result[1])
        elif "LensNearReferencePoint" in result[0]:
            if key not in self._lens_near_reference_point.keys():
                self._lens_near_reference_point[key] = {}
            point_string = result[1].strip().split(",")
            self._lens_near_reference_point[key] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "LensNearRegionRadius" in result[0]:
            if key not in self._lens_near_region_radius.keys():
                self._lens_near_region_radius[key] = {}
            self._lens_near_region_radius[key][prescription_type_key.value] = \
                float(result[1])
        elif "LensPrismReferencePoint" in result[0]:
            if key not in self._lens_prism_reference_point.keys():
                self._lens_prism_reference_point[key] = {}
            point_string = result[1].strip().split(",")
            self._lens_prism_reference_point[key][prescription_type_key.value] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "PointOfLeastPrismChange" in result[0]:
            if key not in self._point_of_least_prism_change.keys():
                self._point_of_least_prism_change[key] = {}
            point_string = result[1].strip().split(",")
            self._point_of_least_prism_change[key][prescription_type_key.value] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "MonocularPupillaryDistance" in result[0]:
            if key not in \
                    self._monocular_pupillary_distance_prescription.keys():
                self._monocular_pupillary_distance_prescription[key] = {}
            self._monocular_pupillary_distance_prescription[key][prescription_type_key.value] = \
                float(result[1])
        elif "MonocularDistanceBetweenLensesMM" in result[0]:
            if key not in self._monocular_distance_between_lenses_mm.keys():
                self._monocular_distance_between_lenses_mm[key] = {}
            self._monocular_distance_between_lenses_mm[key][prescription_type_key.value] = \
                float(result[1])
        elif "LensDistanceReferencePointHeightMM" in result[0]:
            if key not in self._lens_distance_reference_point_height_mm.keys():
                self._lens_distance_reference_point_height_mm[key] = {}
            self._lens_distance_reference_point_height_mm[key][prescription_type_key.value] = \
                float(result[1])
        elif "RefractiveIndex" in result[0]:
            if key not in self._refractive_index.keys():
                self._refractive_index[key] = {}
            self._refractive_index[key][prescription_type_key.value] = \
                float(result[1])
        elif "CentralThickness" in result[0]:
            if key not in self._central_thickness.keys():
                self._central_thickness[key] = {}
            self._central_thickness[key][prescription_type_key.value] = \
                float(result[1])
        elif "BaseCurve" in result[0]:
            if key not in self._base_curve_prescription.keys():
                self._base_curve_prescription[key] = {}
            self._base_curve_prescription[key][prescription_type_key.value] = \
                float(result[1])
        elif "EngravingPointLeft" in result[0]:
            if key not in self._engraving_point_left.keys():
                self._engraving_point_left[key] = {}
            point_string = result[1].strip().split(",")
            self._engraving_point_left[key][prescription_type_key.value] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "EngravingPointRight" in result[0]:
            if key not in self._engraving_point_right.keys():
                self._engraving_point_right[key] = {}
            point_string = result[1].strip().split(",")
            self._engraving_point_right[key][prescription_type_key.value] = \
                np.array([float(point_string[0]), float(point_string[1])])
        elif "EngravingOffsetAngle" in result[0]:
            if key not in self._engraving_offset_angle.keys():
                self._engraving_offset_angle[key] = {}
            self._engraving_offset_angle[key][prescription_type_key.value] = \
                float(result[1])
        elif "Degression" in result[0]:
            if key not in self._degression.keys():
                self._degression[key] = {}
            self._degression[key][prescription_type_key.value] = \
                float(result[1])
        elif "SphericalValuesAlongCorridorLine" in result[0]:
            if key not in self._spherical_values_along_corridor_line.keys():
                self._spherical_values_along_corridor_line[key] = {}
            value_string = result[1].strip().split(",")
            self._spherical_values_along_corridor_line[key][prescription_type_key.value] = \
                np.array([float(value) for value in value_string[:-1]])

    def diff(self, other):
        """
        Assess the difference between two instances of Prescription.

        Parameters
        ----------
        other: Prescription
            The Prescription instance to which this instance will be compared.

        Return
        ------
        diff: dict
            A dictionary containing all differences between the two instances.
        """
        diff = {}
        for key, value in self.__dict__.items():
            if isinstance(value, dict):
                diff[key] = {}
                for cam, cam_value in value.items():
                    if isinstance(cam_value, dict):
                        for section, section_value in cam_value.items():
                            if cam not in diff[key].keys():
                                diff[key][cam] = {}
                            diff_value = \
                                self._diff_attribute(section_value,
                                                     other.__dict__[key][cam][section])
                            if diff_value is None:
                                continue
                            diff[key][cam][section] = diff_value
                    else:
                        diff_value = self._diff_attribute(cam_value,
                                                          other.__dict__[key][cam])
                        if diff_value is None:
                            continue
                        diff[key][cam] = diff_value
            else:
                diff_value = self._diff_attribute(value, other.__dict__[key])
                if diff_value is None:
                    continue
                diff[key] = diff_value
        return diff

    def _diff_attribute(self, this_value, other_value):
        """
        Performs the diff for a specific attribute.  Parameters must be of the
        same type.

        Parameters
        ----------
        this_value: attribute
            An attribute from this object is either a float, a bool, a string,
            a TintedType, a LensType or a PrescriptionType.
        other_value: attribute
            An attribute from this object is either a float, a bool, a string,
            a TintedType, a LensType or a PrescriptionType.

        Return
        ------
        diff_value: object
            An object that represents the difference between the two inputs.
            Returns None if the inputs are not recognised or there is no
            differnece.
        """
        diff_value = None
        if isinstance(this_value, float) or \
                isinstance(this_value, bool):  # difference for bool or float
            diff_value = this_value - other_value
            if diff_value == 0 or np.isnan(diff_value):
                diff_value = None
                pass
        elif isinstance(this_value, np.ndarray):
            diff_value = np.array([value_element - other_value_element
                                   for value_element, other_value_element
                                   in zip(this_value, other_value)])
            if np.all(diff_value == 0):
                diff_value = None
                pass
        elif isinstance(this_value, str):  # string diff for string
            diff_value = self._generate_diff_string(this_value, other_value)
            if diff_value == "":
                diff_value = None
                pass
        # string diff for enum value
        elif isinstance(this_value, TintedType) or isinstance(this_value, LensType) or \
                isinstance(this_value, PrescriptionType):
            diff_value = self._generate_diff_string(this_value, other_value)
            if diff_value == "":
                diff_value = None
                pass
        return diff_value

    def _generate_diff_string(self, self_string, other_string):
        """
        Creates a string describing the difference between two strings.

        Parameters
        ----------
        self_string: string
            The string from this instance of Prescription.
        self_string: string
            The string from the other instance of Prescription.

        Return
        ------
        string
            A string describing the differences.
        """
        return "" if self_string == other_string else self_string + " : " + \
            other_string

    @property
    def measurement_time(self):
        """
        A property containing the measurement time.

        Returns
        -------
        time.datetime
            The time at which the measurement was taken.
        """
        return self._measurement_time

    @property
    def binocular_pupillary_distance(self):
        """
        A property containing the pupillary distance.

        Returns
        -------
        float
            The binocular pupillary distance in mm.
        """
        return self._binocular_pupillary_distance

    @property
    def is_bp_approximated(self):
        """
        A property containing flags indicating if the bp is approximated or not
        for each camera.

        Returns
        -------
        dict of bool
            A dictionary with Cam1 and Cam2 as keys to flags indicating if the
            bp is approximated.
        """
        return self._is_bp_approximated

    @property
    def is_lens_tinted(self):
        """
        A property containing flags indicating if the lens is tinted.

        Returns
        -------
        dict of TintedType
            A dictionary with Cam1 and Cam2 as keys to flags indicate if the
            lens is tinted.
        """
        return self._is_lens_tinted

    @property
    def single_vision_lens_measurement_location(self):
        """
        A property containing strings indicating the single vision lens
        measurement location.

        Returns
        -------
        dict of strings
            A dictionary with Cam1 and Cam2 as keys to strings indicating the
            single vision lens measurement location.
        """
        return self._single_vision_lens_measurement_location

    @property
    def monocular_pupillary_distance(self):
        """
        A property containing floats indicating the monocular pupillary
        distance in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            monocular pupillary distance in
            mm.
        """
        return self._monocular_pupillary_distance

    @property
    def sphere_effective_power(self):
        """
        A property containing floats indicating the spherical power in
        diopters.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            spherical power in diopters.
        """
        return self._sphere_effective_power

    @property
    def secondary_principle_plane_offset_from_back_vertex(self):
        """
        A property containing floats indicating distance of the principle plane
        from the back vertex in metres.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating
            distance of the principle plane from the back vertex in metres.
        """
        return self._secondary_principle_plane_offset_from_back_vertex

    @property
    def base_curve(self):
        """
        A property containing floats indicating base curve in diopters.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating base
            curve in diopters.
        """
        return self._base_curve

    @property
    def lens_center_thickness(self):
        """
        A property containing floats indicating lens center thickness in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating lens
            center thickness in mm.
        """
        return self._lens_center_thickness

    @property
    def refractive_index_lens(self):
        """
        A property containing floats indicating the refractive index of each
        lens.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            refractive index of each lens.
        """
        return self._refractive_index_lens

    @property
    def refractive_index_air(self):
        """
        A property containing floats indicating the refractive index of the
        air.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            refractive index of the air.
        """
        return self._refractive_index_air

    @property
    def lms_base_curve_lens(self):
        """
        A property containing floats indicating base curve in diopters from the
        lms file.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating base
            curve in diopters from the lms file.
        """
        return self._lms_base_curve_lens

    @property
    def lms_center_thickness_lens(self):
        """
        A property containing floats indicating lens center thickness in mm
        from the lms file.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating lens
            center thickness in mm from
            the lms file.
        """
        return self._lms_center_thickness_lens

    @property
    def lms_refractive_index_lens(self):
        """
        A property containing floats indicating the refractive index from the
        lms file.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            refractive index from the lms
            file.
        """
        return self._lms_refractive_index_lens

    @property
    def lens_prescription_type_hint(self):
        """
        A property containing floats indicating the types of lens in the frame.

        Returns
        -------
        dict of LensType
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            types of lens in the frame.
        """
        return self._lens_prescription_type_hint

    @property
    def distance_reference_point_height(self):
        """
        A property containing floats indicating the height of the distance
        reference point in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            height of the distance reference point in mm.
        """
        return self._distance_reference_point_height

    @property
    def near_pupillary_distance(self):
        """
        A property containing floats indicating the near pupillary distance in
        mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            near pupillary distance in mm.
        """
        return self._near_pupillary_distance

    @property
    def optical_center_reference_point(self):
        """
        A property containing two element numpy arrays of floats indicating the
        optical center of each lens
        in pixels.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the optical
            center of each lens in pixels.
        """
        return self._optical_center_reference_point

    @property
    def ep_prescription_result_success(self):
        """
        A property containing flags indicating if the ep prescription was
        successfully generated.

        Returns
        -------
        dict of bool
            A dictionary with Cam1 and Cam2 as keys to flags indicating if the
            ep prescription was successfully generated.
        """
        return self._ep_prescription_result_success

    @property
    def measurement_power_specification_for_prescription_type(self):
        """
        A property containing PrescriptionTypes to indicate the type of power
        measurements in the prescription.

        Returns
        -------
        dict of PrescriptionType
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            type of power measurements in the prescription.
        """
        return self._measurement_power_specification_for_prescription_type

    @property
    def axis(self):
        """
        A property containing floats indicating the axis value in degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            axis value in degrees.
        """
        return self._axis

    @property
    def axis_at_near_reference_point(self):
        """
        A property containing floats indicating the axis value at the near
        reference point in degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            axis value at the near
            reference point in degrees.
        """
        return self._axis_at_near_reference_point

    @property
    def axis_at_distance_reference_point(self):
        """
        A property containing floats indicating the axis value at the distance
        reference point in degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            axis value at the distance
            reference point in degrees.
        """
        return self._axis_at_distance_reference_point

    @property
    def sphere_at_near_reference_point(self):
        """
        A property containing floats indicating the sphere value at the near
        reference point in degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            sphere value at the near
            reference point in degrees.
        """
        return self._sphere_at_near_reference_point

    @property
    def sphere_at_distance_reference_point(self):
        """
        A property containing floats indicating the sphere value at the
        distance reference point in degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            sphere value at the distance
            reference point in degrees.
        """
        return self._sphere_at_distance_reference_point

    @property
    def cylinder_at_near_reference_point(self):
        """
        A property containing floats indicating the cylinder value at the near
        reference point in degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            cylinder value at the near
            reference point in degrees.
        """
        return self._cylinder_at_near_reference_point

    @property
    def cylinder_at_distance_reference_point(self):
        """
        A property containing floats indicating the cylinder value at the
        distance reference point in diopters.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            cylinder value at the distance reference point in diopters.
        """
        return self._cylinder_at_distance_reference_point

    @property
    def spherical_equivalent_at_near_reference_point(self):
        """
        A property containing floats indicating the spherical equivalent value
        at the distance reference point in diopters.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            spherical equivalent value at the near reference point in diopters.
        """
        return self._spherical_equivalent_at_near_reference_point

    @property
    def spherical_equivalent_at_distance_reference_point(self):
        """
        A property containing floats indicating the spherical equivalent value
        at the distance reference point in diopters.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            spherical equivalent value at the distance reference point in
            diopters.
        """
        return self._spherical_equivalent_at_distance_reference_point

    @property
    def corridor_length(self):
        """
        A property containing floats indicating the corridor length value.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            corridor length value.
        """
        return self._corridor_length

    @property
    def corridor_width(self):
        """
        A property containing floats indicating the corridor width value.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            corridor width value.
        """
        return self._corridor_width

    @property
    def distance_corridor_points(self):
        """
        A property containing two element numpy arrays of floats indicating the
        distance corridor points.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the distance corridor points.
        """
        return self._distance_corridor_points

    @property
    def near_corridor_points(self):
        """
        A property containing two element numpy arrays of floats indicating the
        near corridor points.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the
            near corridor points.
        """
        return self._near_corridor_points

    @property
    def prism_diopters(self):
        """
        A property containing floats indicating the prism diopter value.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            prism diopter value.
        """
        return self._prism_diopters

    @property
    def prism_base_angle(self):
        """
        A property containing floats indicating the prism base angle value in
        degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            prism base angle value in degrees.
        """
        return self._prism_base_angle

    @property
    def prism_reference_point_offset_from_distance_reference_point_mm(self):
        """
        A property containing floats indicating the distance between the prism
        reference point and the distance reference point value in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            distance between the prism reference point and the distance
            reference point value in mm.
        """
        return \
            self._prism_reference_point_offset_from_distance_reference_point_mm

    @property
    def near_reference_point_inset_mm(self):
        """
        A property containing floats indicating the near reference point inset
        value in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            near reference point inset value in mm.
        """
        return self._near_reference_point_inset_mm

    @property
    def distance_between_measuring_lens_and_screen_mm(self):
        """
        A property containing floats indicating the separation between the
        sample and the screen in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            separation between the
            sample and the screen in mm.
        """
        return self._distance_between_measuring_lens_and_screen_mm

    @property
    def lens_center_point(self):
        """
        A property containing two element numpy arrays of floats indicating the
        center of each lens in mm.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the center of each lens in mm.
        """
        return self._lens_center_point

    @property
    def lens_distance_reference_point(self):
        """
        A property containing two element numpy arrays of floats indicating the
        distance reference point in mm.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the distance reference point in mm.
        """
        return self._lens_distance_reference_point

    @property
    def lens_distance_region_radius(self):
        """
        A property containing floats indicating the radius of the distance
        region in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            radius of the distance region in mm.
        """
        return self._lens_distance_region_radius

    @property
    def lens_near_reference_point(self):
        """
        A property containing two element numpy arrays of floats indicating the
        near reference point in mm.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the near reference point in mm.
        """
        return self._lens_near_reference_point

    @property
    def lens_near_region_radius(self):
        """
        A property containing floats indicating the radius of the near region
        in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            radius of the near region in mm.
        """
        return self._lens_near_region_radius

    @property
    def lens_prism_reference_point(self):
        """
        A property containing two element numpy arrays of floats indicating
        the prism reference point in mm.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy
            arrays of floats indicating the prism reference point in mm.
        """
        return self._lens_prism_reference_point

    @property
    def monocular_pupillary_distance_prescription(self):
        """
        A property containing floats indicating the monocular pupillary
        distance calculated by the eMap in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            monocular pupillary distance calculated by the eMap in mm.
        """
        return self._monocular_pupillary_distance_prescription

    @property
    def monocular_distance_between_lenses_mm(self):
        """
        A property containing floats indicating the monocular distance between
        lenses in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            monocular distance between
            lenses in mm.
        """
        return self._monocular_distance_between_lenses_mm

    @property
    def lens_distance_reference_point_height_mm(self):
        """
        A property containing floats indicating the distance reference point
        height in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            distance reference point
            height in mm.
        """
        return self._lens_distance_reference_point_height_mm

    @property
    def refractive_index(self):
        """
        A property containing floats indicating the refractive index.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            refractive index.
        """
        return self._refractive_index

    @property
    def central_thickness(self):
        """
        A property containing floats indicating the central thickness in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            central thickness in mm.
        """
        return self._central_thickness

    @property
    def base_curve_prescription(self):
        """
        A property containing floats indicating the central thickness in mm.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            central thickness in mm.
        """
        return self._base_curve_prescription

    @property
    def engraving_point_left(self):
        """
        A property containing two element numpy arrays of floats indicating the
        left engraving point in mm.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the left engraving point in mm.
        """
        return self._engraving_point_left

    @property
    def engraving_point_right(self):
        """
        A property containing two element numpy arrays of floats indicating the
        right engraving point in mm.

        Returns
        -------
        dict of 2 element numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the
             right engraving point in mm.
        """
        return self._engraving_point_right

    @property
    def engraving_offset_angle(self):
        """
        A property containing floats indicating the engraving offset angle in
        degrees.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            engraving offset angle in degrees.
        """
        return self._engraving_offset_angle

    @property
    def degression(self):
        """
        A property containing floats indicating the degression in diopters.

        Returns
        -------
        dict of floats
            A dictionary with Cam1 and Cam2 as keys to floats indicating the
            degression in diopters.
        """
        return self._degression

    @property
    def spherical_values_along_corridor_line(self):
        """
        A property containing two element numpy arrays of floats indicating the
        sphere values along the corridor in diopters.

        Returns
        -------
        dict of numpy.ndarrays of floats
            A dictionary with Cam1 and Cam2 as keys to two element numpy arrays
            of floats indicating the sphere values along the corridor in
            diopters.
        """
        return self._spherical_values_along_corridor_line


class OmaPrescriptionSingleLens:
    """
    The prescription information for a single lens.
    """

    def __init__(self):
        pass

    def initalise(self, data):
        """
        Takes a dictionary of data load from an oma file and generates an
        object from it.

        Parameters
        ----------
        data: dict
            A dictionary with the data in.
        """
        try:
            self._lens_style_name = data["LNAM"]
            self._prescription_sphere = float(data["SPH"])
            self._prescription_cylinder = float(data["CYL"])
            self._prescription_axis = float(data["AX"])
            self._lens_type = DCSLensType(data["LTYPE"].split(" ")[0])
            self._prescription_prism = float(data["PRVM"])
            self._prescription_prism_base = float(data["PRVA"])
            self._vertical_prism_reference_point = float(data["OCHT"])
            self._monocular_centration_distance = float(data["IPD"])
            self._material_refractive_index = float(data["LIND"])
            self._designed_prism_magnitude = float(data["LDPRVM"])
            self._designed_prism_base = float(data["LDPRVA"])
            self._blank_true_fron_curve = float(data["FRNT"])
            self._finished_centre_thickness = float(data["CTHICK"])
            self._generator_cross_curve = float(data["GCROS"])
            self._measured_prism_magnitude_at_prp =float( data["INSPRPRVM"])
            self._measured_prism_base_at_prp = float(data["INSPRPRVA"])
            # alternate 1.53 value from DCS-v3_12_FINAL see p.143
            self._diopter_curve_refractive_index = \
                float(data["TIND"]) if "TIND" in data else 1.53
            self._thickest_edge_thickness = float(data["THKP"])
            self._thick_point_angle = float(data["THKA"])
            self._thinnest_edge_point = float(data["THNP"])
            self._horizontal_size_lens_box = float(data["HBOX"])
            self._vertical_size_lens_box = float(data["VBOX"])
            self._blank_diameter = float(data["DIA"])
            self._horizontal_frame_tilt = float(data["ZTILT"])
            # prp is prism reference point
            self._blank_geometric_centre_to_prp_down = float(data["BCOCUP"])
            self._blank_geometric_centre_to_prp_in = float(data["BCOCIN"])
            # lrp is layout reference point
            self._blank_geometric_centre_to_lrp_up = float(data["BCSGUP"])
            self._blank_geometric_centre_to_lrp_in = float(data["BCSGIN"])
            self._lrp_to_prp_up = float(data["SGOCUP"])
            self._lrp_to_prp_in = float(data["SGOCIN"])
            self._surface_block_to_prp_up = float(data["SBOCUP"])
            self._surface_block_to_prp_in = float(data["SBOCIN"])
            self._frame_center_to_prp_up = float(data["FCOCUP"])
            self._frame_center_to_prp_in = float(data["FCOCIN"])
            self._frame_center_to_lrp_up = float(data["FCSGUP"])
            self._frame_center_to_lrp_in = float(data["FCSGIN"])
            self._engraving_point_to_lrp_up = float(data["ERSGUP"])
            self._engraving_point_to_lrp_in = float(data["ERSGIN"])
            self._engraving_point_to_nrp_up = float(data["ERNRUP"])
            self._engraving_point_to_nrp_in = float(data["ERNRUP"])
        except KeyError:
            pass

    @property
    def lens_style_name(self):
        """
        Name of lens style (LNAM).
        """
        return self._lens_style_name

    @property
    def prescription_sphere(self):
        """
        Rx Sphere power (diopters) (SPH).
        """
        return self._prescription_sphere

    @property
    def prescription_cylinder(self):
        """
        Rx cylinder power (diopters).
        """
        return self._prescription_cylinder

    @property
    def prescription_axis(self):
        """
        Prescribed cylinder axis. May be 0 - 180. (degrees) Notice
        that this may be different from GAX which is generator axis (AX).
        """
        return self._prescription_axis

    @property
    def lens_type(self):
        """
        Lens type, constructed using the following 2 letter codes
        separated by spaces. In the case of lenses with differing
        front and back surface types, as described byLTYPEF and
        LTYPEB, LTYPE describes the effective combination of both
        (LTYPE).
        """
        return self._lens_type

    @property
    def prescription_prism(self):
        """
        Rx Prism in diopters, including equithinning prism to inspect at
        Prism Reference Point. (LIND or “natural” diopters) (PRVM).
        """
        return self._prescription_prism

    @property
    def prescription_prism_base(self):
        """
        Rx Prism base setting, 0 - 360 (degrees) (PRVA).
        """
        return self._prescription_prism_base

    @property
    def vertical_prism_reference_point(self):
        """
        Vertical Prism Reference Point height measured from the (frame)
        lower boxed tangent (OCHT).
        """
        return self._vertical_prism_reference_point

    @property
    def monocular_centration_distance(self):
        """
        Monocular centration distance (mm) (IPD).
        """
        return self._monocular_centration_distance

    @property
    def material_refractive_index(self):
        """
        Index of lens material (LIND).
        """
        return self._material_refractive_index

    @property
    def designed_prism_magnitude(self):
        """
        Designed magnitude of prism (LIND or “natural” diopters) (LDPRVM).
        """
        return self._designed_prism_magnitude

    @property
    def designed_prism_base(self):
        """
        Designed prism base setting (degrees) (LDPRVA).
        """
        return self._designed_prism_base

    @property
    def blank_true_fron_curve(self):
        """
        Blank true front curve for power calculations (TIND diopters) (FRNT).
        """
        return self._blank_true_fron_curve

    @property
    def finished_centre_thickness(self):
        """
        Finished center thickness (mm at distance O.C.) (CTHICK).
        """
        return self._finished_centre_thickness

    @property
    def generator_cross_curve(self):
        """
        Generator cross curve. (rounded, TIND diopters) (GCROS).
        """
        return self._generator_cross_curve

    @property
    def measured_prism_magnitude_at_prp(self):
        """
        Measured prism magnitude at Prism Reference Point (LIND or
        “natural” diopters) (INSPRPRVM).
        """
        return self._measured_prism_magnitude_at_prp

    @property
    def measured_prism_base_at_prp(self):
        """
        Measured prism base setting at Prism Reference Point (degrees)
        (INSPRPRVA).
        """
        return self._measured_prism_base_at_prp

    @property
    def diopter_curve_refractive_index(self):
        """
        Index of refraction used for all diopter curves in a packet.
        In the absence of a TIND record or field value, 1.53 may be
        presumed (TIND).
        """
        return self._diopter_curve_refractive_index

    @property
    def thickest_edge_thickness(self):
        """
        Thickest edge thickness on finished edge (mm) (THKP).
        """
        return self._thickest_edge_thickness

    @property
    def thick_point_angle(self):
        """
        Thick point angle—the meridian at which THKP occurs (degrees) (THKA).
        """
        return self._thick_point_angle

    @property
    def thinnest_edge_point(self):
        """
        Thinnest edge thickness on finished edge (mm) (THNP).
        """
        return self._thinnest_edge_point

    @property
    def horizontal_size_lens_box(self):
        """
        Horizontal boxed lens size of frame (mm) (HBOX).
        """
        return self._horizontal_size_lens_box

    @property
    def vertical_size_lens_box(self):
        """
        Vertical boxed lens size of frame (mm) (VBOX).
        """
        return self._vertical_size_lens_box

    @property
    def blank_diameter(self):
        """
        Blank diameter (mm) (DIA).
        """
        return self._blank_diameter

    @property
    def horizontal_frame_tilt(self):
        """
        Side-to-side or horizontal tilt of frame as traced. (ZTILT).
        """
        return self._horizontal_frame_tilt

    @property
    def blank_geometric_centre_to_prp_up(self):
        """
        Blank geometrical center to Prism Reference Point (PRP) In &
        Down (mm). Useful for uncuts where frame information is not
        available.  +IN means PRP towards nasal with respect to the
        geometrical blank center.  -IN means PRP towards temporal
        with respect to the geometrical blank center. +UP means PRP is
        above the geometrical blank center. -UP means PRP is below the
        geometrical blank center (BCOCUP).
        """
        return self._blank_geometric_centre_to_prp_up

    @property
    def blank_geometric_centre_to_prp_in(self):
        """
        Blank geometrical center to Prism Reference Point (PRP) In &
        Down (mm). Useful for uncuts where frame information is not
        available.  +IN means PRP towards nasal with respect to the
        geometrical blank center.  -IN means PRP towards temporal
        with respect to the geometrical blank center. +UP means PRP is
        above the geometrical blank center. -UP means PRP is below the
        geometrical blank center (BCOCIN).
        """
        return self._blank_geometric_centre_to_prp_in

    @property
    def blank_geometric_centre_to_lrp_up(self):
        """
        Blank geometrical center to Layout Reference Point (LRP) In &
        Down (mm), i.e. manufacturer’s stated segment position relative
        to geometric center of the blank. +IN means LRP towards nasal
        from the blank center. -IN means LRP towards temporal from the
        blank center. +UP means LRP is above the blank center.  -UP means
        LRP is below the blank center (BCSGUP).
        """
        return self._blank_geometric_centre_to_lrp_up

    @property
    def blank_geometric_centre_to_lrp_in(self):
        """
        Blank geometrical center to Layout Reference Point (LRP) In &
        Down (mm), i.e. manufacturer’s stated segment position relative
        to geometric center of the blank. +IN means LRP towards nasal
        from the blank center. -IN means LRP towards temporal from the
        blank center. +UP means LRP is above the blank center.  -UP means
        LRP is below the blank center (BCSGIN).
        """
        return self._blank_geometric_centre_to_lrp_in

    @property
    def lrp_to_prp_up(self):
        """
        Layout reference point to prism reference point vectors. Can be
        used to locate the PRP for multifocals. +IN means PRP is towards
        nasal relative to the LRP. -IN means PRP is towards temporal
        relative to the LRP. +UP means PRP is above the LRP. -UP means
        PRP is below the LRP (SGOCUP).
        """
        return self._lrp_to_prp_up

    @property
    def lrp_to_prp_in(self):
        """
        Layout reference point to prism reference point vectors. Can be
        used to locate the PRP for multifocals. +IN means PRP is towards
        nasal relative to the LRP. -IN means PRP is towards temporal
        relative to the LRP. +UP means PRP is above the LRP. -UP means
        PRP is below the LRP (SGOCIN).
        """
        return self._lrp_to_prp_in

    @property
    def surface_block_to_prp_up(self):
        """
        Surface block to prism reference point vectors. Instructs the
        generator to grind the prism reference point at a position
        relative to the surface block. +IN means PRP is towards nasal
        relative to the surface block. -IN means PRP is towards temporal
        relative to the surface block. +UP means PRP is above the surface
        block. -UP means PRP is below the surface block (SBOCUP).
        """
        return self._surface_block_to_prp_up

    @property
    def surface_block_to_prp_in(self):
        """
        Surface block to prism reference point vectors. Instructs the
        generator to grind the prism reference point at a position
        relative to the surface block. +IN means PRP is towards nasal
        relative to the surface block. -IN means PRP is towards temporal
        relative to the surface block. +UP means PRP is above the surface
        block. -UP means PRP is below the surface block (SBOCIN).
        """
        return self._surface_block_to_prp_in

    @property
    def frame_center_to_prp_up(self):
        """
        Frame center to prism reference point vectors, i.e., O.C. Inset and
        Drop (mm).  +IN means PRP is towards nasal with respect to the frame
        center.  -IN means PRP is towards temporal with respect to the frame
        center. +UP means PRP is above the frame center. -UP means PRP s
        below the frame center (FCOCUP).
        """
        return self._frame_center_to_prp_up

    @property
    def frame_center_to_prp_in(self):
        """
        Frame center to prism reference point vectors, i.e., O.C. Inset and
        Drop (mm).  +IN means PRP is towards nasal with respect to the frame
        center.  -IN means PRP is towards temporal with respect to the frame
        center. +UP means PRP is above the frame center. -UP means PRP s
        below the frame center (FCOCIN).
        """
        return self._frame_center_to_prp_in

    @property
    def frame_center_to_lrp_up(self):
        """
       Frame center to layout reference point vectors, i.e. seg inset & drop
       (mm). +IN means LRP is towards nasal with respect to the frame center.
       -IN means LRP is towards temporal with respect to the frame center.
       +UP means LRP is above the frame center. -UP means LRP is below the
       frame center. (FCSGUP).
        """
        return self._frame_center_to_lrp_up

    @property
    def frame_center_to_lrp_in(self):
        """
       Frame center to layout reference point vectors, i.e. seg inset & drop
       (mm). +IN means LRP is towards nasal with respect to the frame center.
       -IN means LRP is towards temporal with respect to the frame center.
       +UP means LRP is above the frame center. -UP means LRP is below the
       frame center. (FCSGIN).
        """
        return self._frame_center_to_lrp_in

    @property
    def engraving_point_to_lrp_up(self):
        """
       Engraving reference point (ERP) to layout reference point (LRP)
       vectors  +IN means LRP is towards nasal relative to the ERP.
       -IN means LRP reference point is  towards temporal relative to
       the  ERP. +UP means LRP reference point is  above the ERP.  -UP
       means LRP reference point is below the ERP. 
        """
        return self._engraving_point_to_lrp_up

    @property
    def engraving_point_to_lrp_in(self):
        """
       Engraving reference point (ERP) to layout reference point (LRP)
       vectors  +IN means LRP is towards nasal relative to the ERP.
       -IN means LRP reference point is  towards temporal relative to
       the  ERP. +UP means LRP reference point is  above the ERP.  -UP
       means LRP reference point is below the ERP. 
        """
        return self._engraving_point_to_lrp_in

    @property
    def engraving_point_to_nrp_up(self):
        """
       Engraving reference point (ERP) to near reference point (NRP)
       vectors. +IN means NRP is towards nasal relative to the ERP.
       -IN means NRP is towards temporal relative to the ERP.  +UP
       means NRP is above the ERP. -UP means NRP is below the ERP.
        """
        return self._engraving_point_to_nrp_up

    @property
    def engraving_point_to_nrp_in(self):
        """
       Engraving reference point (ERP) to near reference point (NRP)
       vectors. +IN means NRP is towards nasal relative to the ERP.
       -IN means NRP is towards temporal relative to the ERP.  +UP
       means NRP is above the ERP. -UP means NRP is below the ERP.
        """
        return self._engraving_point_to_nrp_in


class OmaPrescription:
    """
    A prescription loaded from an OMA file.
    """

    def __init__(self, filename):
        """
        Loads data from an oma file used in an LMS for a specific job.  Could
        be either a glazed or unglazed job.

        Parameters
        ----------
        filename : string
            Full path of OMA file to load.

        Returns
        -------
        None.
        """
        text = None
        with open(filename, "r") as file:
            text = file.readlines()
        left = {}
        right = {}
        # get lens processing flag
        for line in text:
            # if there is no = in line then line contains no data
            if "=" not in line:
                continue
            # we get data related to whole spectacles in this section
            label, right_eye, left_eye = self._extract_data_from_string(line)
            if label == "DO":
                self._lens_data_contents = LensDataContents.none
                if right_eye == "B":
                    self._lens_data_contents = LensDataContents.both
                    self._left = OmaPrescriptionSingleLens()
                    self._right = OmaPrescriptionSingleLens()
                elif right_eye == "L":
                    self._lens_data_contents = LensDataContents.left
                    self._left = OmaPrescriptionSingleLens()
                elif right_eye == "R":
                    self._lens_data_contents = LensDataContents.right
                    self._right = OmaPrescriptionSingleLens()
            else:
                continue

        # process data
        for line in text:
            # if there is no = in line then line contains no data
            if "=" not in line:
                continue
            # we get data related to whole spectacles in this section
            label, right_eye, left_eye = self._extract_data_from_string(line)
            # all lines with data are of the format
            # label=right_eye_data;left_eye_data
            # this large if statement sorts the data to the correct properties.
            if label == "ACCN":
                self._account = right_eye
            elif label == "JOB":
                self._job = right_eye
            elif label == "RXNB":
                self._prescription_number = right_eye
            elif label == "DBL":
                self._distance_between_lenses = float(right_eye)
            elif label == "FTYP":
                ftyp = int(right_eye)
                if ftyp == FrameType.undefined.value:
                    self._frame_type = FrameType.undefined
                elif ftyp == FrameType.plastic.value:
                    self._frame_type = FrameType.plastic
                elif ftyp == FrameType.metal.value:
                    self._frame_type = FrameType.metal
                elif ftyp == FrameType.rimless.value:
                    self._frame_type = FrameType.rimless
                elif ftyp == FrameType.optyl.value:
                    self._frame_type = FrameType.optyl
                else:
                    msg = "OmaPrescription.__init__: Unrecognised frame type."
                    raise ValueError(msg)
            elif label == "POLAR":
                if right_eye == "1":
                    self._is_job_polarized = True
                elif right_eye == "0":
                    self._is_job_polarized = False
                else:
                    msg = "OmaPrescription.__init__: Unrecognised frame type."
                    raise ValueError(msg)
            elif label == "LMS_VENDOR":
                self._lms_vendor = int(right_eye)
            elif label == "MEASUREMENT_POINTS_MODE":
                self._measurment_points_mode = int(right_eye)
            else:
                # we get data for individual lenses in this section
                if self._lens_data_contents == LensDataContents.both:
                    left[label] = left_eye
                    right[label] = right_eye
                elif self._lens_data_contents == LensDataContents.left:
                    left[label] = left_eye
                elif self._lens_data_contents == LensDataContents.right:
                    right[label] = right_eye
        if self._lens_data_contents == LensDataContents.both:
            self._left.initalise(left)
            self._right.initalise(right)
        elif self._lens_data_contents == LensDataContents.left:
            self._left.initalise(left)
        elif self._lens_data_contents == LensDataContents.right:
            self._right.initalise(right)

    def _extract_data_from_string(self, line):
        """
        Extracts data as a string and label from a line in the OMA file

        Parameters
        ----------
        line : string
            A line from the OMA file.

        Returns
        -------
        label : string
            The OMA file label defined in DCS-v3_12_FINAL.pdf found here:
                C:\\Users\\RhysPoolman\\OneDrive - EYOTO GROUP LTD\\Documents
                \\Standards
        right_eye : string or None
            The data from the right associated with the label.
        left_eye : string or None
            The data from the left associated with the label.

        """
        line_values = line.split("=")
        label = line_values[0].strip()
        right_eye = None
        left_eye = None
        if line_values[1].startswith(";"):
            left_eye = line_values[1].strip()
        elif line_values[1].endswith(";") or ";" not in line_values[1]:
            right_eye = line_values[1].strip()
        else:
            data = line_values[1].split(";")
            right_eye = data[0].strip()
            left_eye = data[1].strip()
        return (label, right_eye, left_eye)

    @property
    def lens_data_contents(self):
        """
        An enum to indicate whether this instance will contain left,
        right, both or none of the lens data. Called DO in oma file
        """
        return self._lens_data_contents

    @property
    def account_number(self):
        """
        The account number of the job, called ACCN in oma file.
        """
        return self._account

    @property
    def job_number(self):
        """
        The job number, called JOB in oma file.
        """
        return self._job

    @property
    def prescription_number(self):
        """
        The number of the prescription, called RXNB in oma file.
        """
        return self._prescription_number

    @property
    def distance_between_lenses(self):
        """
        The distance between lens, called DBL in oma file. The
        DCS-v3_12_FINAL data communication standards doesn't
        say what points are measured to get this distance.
        """
        return self._distance_between_lenses

    @property
    def frame_type(self):
        """
        The material the frame is made if there is one.
        """
        return self._frame_type

    @property
    def is_job_polarized(self):
        """
        A binary flag indicating whether the job is polarized
        or not.
        """
        return self._is_job_polarized

    @property
    def lms_vendor(self):
        """
        A integer indicating what lms vendor is used.  Need to
        find a string describing each of the integers.
        """
        return self._lms_vendor

    @property
    def measurement_points_mode(self):
        """
        A integer flag, don't understand what it indicates yet.
        """
        return self._measurment_points_mode

    @property
    def left(self):
        """
        A getter property that allows access to the left lens data.
        """
        return self._left

    @property
    def right(self):
        """
        A getter property that allows access to the right lens data.
        """
        return self._right
