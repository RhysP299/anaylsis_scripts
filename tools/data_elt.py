"""
General functions to extract, load and transform data from database using Pandas
"""
import pandas as pd 
import numpy as np
import pyodbc
import os
import yaml
from datetime import datetime
from typing import Iterable
import csv
import time
import tools.utilities as utl
import logging

logger = logging.getLogger(__name__)


def flatten(items):
    """Yield items from any nested iterable"""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x
            
def load_yaml(file_path, filename):
    """
    Load yaml file given path and filename
    Parameters
    ---------
    path: str
        path to yaml file directory
    filename: str
        filename of yaml file
    
    Returns
    ---------
    yaml_dict: dict
        yaml as dictionary
        
    """
    try:
        with open(os.path.join(file_path, filename)) as stream:
            yaml_dict = yaml.safe_load(stream)
    except OSError as e:
        logger.error(e)
        raise
        
    return yaml_dict

def csv_to_df(**kwargs):   
    """
    Load csv data from path and transform for use in validation study
    Note csv format expected is that exported from eyoto-portal
    Parameters
    ---------
    path: str
        path to raw data file directory
    filename: str
        filename of raw data file
    column_mappings: dict
        dictionary of column names to be imported (values)
    
    Returns
    ---------
    data: dict
        data as dictionary of 2 dataframes, separated by lens location
        
    """
    path = kwargs['path']
    filename = kwargs['filename']
    
    column_mappings = kwargs['columns_mapping']
    
    try:
        raw_data = pd.read_csv(os.path.join(path, filename))
    except OSError as e:
        logger.error(e)
        raise
        
    # TODO check required cols exist else raise err
    raw_data = raw_data.astype({"JobID": str})
     
    raw_data_jobid_index = raw_data.set_index("JobID")
    raw_data_jobid_index = add_prism_col_to_db_data(raw_data_jobid_index)
    
    od_str = "OculusDexter"
    os_str = "OculusSinister"

    data = split_db_data_by_lens_location(raw_data_jobid_index)
    
    data[od_str] = data[od_str][[column for column in column_mappings.values()]]
    data[os_str] = data[os_str][[column for column in column_mappings.values()]]

    return data

def sql_query_to_df(**kwargs):
    """ Run sql query to retrieve selected database entries, given a start date, end date and accountids
    
    Parameters:
    ---------
    server: str
    db: str
    user: str
    query: str
    start_date: str
    end_date: str
    client: list(str)
    data_labels: dict6666666
    
    Returns: 
    ---------
    df: (Pandas DataFrame)
    """

    server = kwargs['server']
    db = kwargs['db']
    user = kwargs['user']
    start_date = pd.to_datetime(kwargs['start_date'])
    end_date = pd.to_datetime(kwargs['end_date'])
    client = kwargs['client']
    db_portal_dict = kwargs['data_labels']
    
    accountids = [v for v in db_portal_dict[client].values()]

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";Authentication=ActiveDirectoryInteractive")

    query = kwargs['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    try:
        df = pd.read_sql_query(query, conn,  params=params)

    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    conn.close()
    
    df = df.astype({"JobID": str})
        
    df = df.set_index("JobID")
    df = add_prism_col_to_db_data(df)
    
    df = db_to_portal_labels(df, db_portal_dict)
    
    # if kwargs['split_by_lens_location']:
    df = split_db_data_by_lens_location(df)

    return df

def xlsx_to_df(**kwargs):
    
    """
    Load data from path and transform for use in validation study
    Parameters
    ---------
    path: str
        path to raw data file directory
    filename: str
        raw data filename
    
    Returns
    ---------
    data: dict
        nested dictionary of DataFrames, dictionary keys lens location and user/device ids
        DataFrames with measurements values for prescriptive measurements
        
    """
    try:
        path = kwargs['path']
        filename = kwargs['filename']
        worksheet_names = kwargs['worksheet_names']
    except KeyError as e:
        logger.error(e)
        raise
        
    print(worksheet_names)
    raw_data = {worksheet_name: pd.read_excel(os.path.join(path, filename),
                                                     sheet_name=worksheet_name)
                       for worksheet_name in worksheet_names}
    columns = list(raw_data[worksheet_names[0]].columns)

    # organise data from file
    for key, entry in raw_data.items():
        # drop enty first row and relabel index from 0
        entry.drop(axis=0, labels=[0], inplace=True)
        num_rows = len(entry.index)
        entry.index = np.linspace(0, num_rows, num_rows, dtype=int)
        # duplicate data in odd rows of combined prism and pass/fail columns from
        # even rows
        # row_to_duplicate = None
        for idx, row in entry.iterrows():
            if idx % 2 == 0:  # index is even
                value_to_duplicate = row
            else:
                entry.loc[idx, columns[-6:-2] + [columns[-1]]] = \
                    value_to_duplicate[columns[-6:-2] +
                                       [columns[-1]]]

    # set job id as index
    data_jobid_index = {key: entry.astype({"JobID": int})
                               for key, entry in raw_data.items()}
    data_jobid_index = {key: entry.astype({"JobID": str})
                               for key, entry in data_jobid_index.items()}
    data_jobid_index = {key: entry.set_index("JobID")
                               for key, entry in data_jobid_index.items()}
    
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    data = {}
    data[od_str] = \
        {name: sheet.loc[list(sheet["Side"].str.strip() == "OD"), :]
         for name, sheet in data_jobid_index.items()}
    data[os_str] = \
        {name: sheet.loc[list(sheet["Side"].str.strip() == "OS"), :]
         for name, sheet in data_jobid_index.items()}
        
    return data

def db_to_portal_labels(df, db_portal_dict):
    """ Convert database values to recognised portal values using dictionary 
    
    Inputs:
    df: (Pandas DataFrame)
    db_portal_dict: (dict) db_to_portal dictionary

    Returns: 
    df: (Pandas DataFrame)
    """

    for step in db_portal_dict:
        if step in df.columns:
            db_portal_dict[step] = {k: 'NaN' if not v else v for k, v in db_portal_dict[step].items()}
            df[step] = df[step].replace(db_portal_dict[step])
    return df

def add_prism_col_to_db_data(data):
    """ Convert database / csv prism values into separate H and V components
    
    Inputs:
    data: (Pandas DataFrame)

    Returns: 
    data: (Pandas DataFrame)
    """
    diopters = np.array([float(value.split("/")[0])
                          if len(value) > 3 else np.nan
                          for value in data["MPPrism"].values]) \
        if "MPPrism" in data.keys() else data["MPPrismDiopters"].values
    bases = np.array([float(value.split("/")[1])
                      if len(value) > 3 else np.nan
                      for value in data["MPPrism"].values]) \
        if "MPPrism" in data.keys() else data["MPPrismBaseAngleDegrees"].values
    h, v = \
        utl.calculate_components(diopters, bases)
    data = data.assign(prism_h=h)
    data = data.assign(prism_v=v)
    return data

def split_db_data_by_lens_location(df):
    """
    Takes a DataFrame with an MPLensLocation column and creates a dictionary of
    two DataFrames.  One DataFrame is the oculus dexter rows and the other is
    the oculuis sinister.

    Parameters
    ----------
    df : pandas.DataFrame
        A data frame with as column headed EPLensLocation.

    Returns
    -------
    data : dictionary of two DataFrames
        A dictionary with two DataFrames as entries one the data for oculus
        dexter and the other for oculus sinister.
    """
    od_str = "OculusDexter"
    os_str = "OculusSinister"
    lens_location_col = "MPLensLocation"
    od_condition = \
        list(df[lens_location_col].str.strip() == od_str)
    os_condition = \
        list(df[lens_location_col].str.strip() == os_str)
    data = {od_str: df.loc[od_condition, :], os_str: df.loc[os_condition, :]}
    return data

def sql_query_to_df_all_accounts(config, start_date, end_date, record_time=None):
    """ Run sql query to retrieve selected database entries, given a start date, end date
    
    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns: 
    df: (Pandas DataFrame)
    """

    server = config['sql_configs']['server']
    db = config['sql_configs']['db']
    user = config['sql_configs']['user']
    pwd = config['sql_configs']['pwd']
    accounts = config['accounts']
    clients = config['clients']

    accountids = []

    for client in config['clients']:
        if isinstance(config['accounts'][client],dict):
            accountids.append(list(config['accounts'][client].values()))
        else:
            accountids.append(config['accounts'][client])

    accountids = [x for x in flatten(accountids)]

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server="+server+";database="+db+";UID="+user+";PWD="+pwd+";")

    query = config['sql_configs']['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    
    days = (end_date - start_date).days
    try:
        start = time.time()
        df = pd.read_sql_query(query, conn,  params=params)
        end = time.time()
        start_time = datetime.fromtimestamp(start)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    if record_time is not None:
        if df is None:
            size_df = None
        else:
            size_df = len(df)
        path = record_time['path']
        filename = record_time['filename']
        append_time_csv(path, filename, start_time, end-start, size_df, days)
    conn.close()

    return df



def append_time_csv(path, filename, start, duration, size_df, days):

    row = {'start_time':start, 'duration': duration, 'file_size': size_df, 'days': days}
    row['start_time'] = row['start_time'].strftime('%Y/%m/%d %H:%M:%S')

    if not os.path.exists(os.path.join(path, filename)):

        with open(os.path.join(path, filename), 'w', newline='') as csvfile:
            fieldnames = ['start_time','duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow(row)
    else:
        with open(os.path.join(path, filename), 'a', newline='') as csvfile:
            fieldnames = ['start_time', 'duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writerow(row)


def lens_pairs_df(df, ignorena=False, cols=None, status_type=False, status_list=[], status_type_list=[]):
    """ Consolidate individual lens measurements into lens pairs

    Inputs:
    df: (Pandas DataFrame) Contains separate rows for left and right lenses 
    ignorena: (bool) If True, 
    cols: list(str) 
    status_type: (bool) If True, separate results into Automatic and Manual
    status_list: list(str)
    status_type_list: list(str)

    Returns: 
    df_lr: (Pandas DataFrame)
    
    """

    df_left = df[df['LensLocation']=='OculusSinister']
    df_right = df[df['LensLocation']=='OculusDexter']
    df_left = df_left.add_prefix('left_')
    df_left = df_left.rename(columns={'left_SerialId': 'SerialId', 'left_MeasurementDate': 'MeasurementDate'})
    df_right = df_right.add_prefix('right_')
    df_right = df_right.rename(columns={'right_SerialId': 'SerialId', 'right_MeasurementDate': 'MeasurementDate'})

    df_lr = pd.merge(df_left, df_right, on=['SerialId', 'MeasurementDate'], how='inner')
    if cols is not None:
        for col in cols:
            df_lr = df_lr.rename(columns={f'right_{col}': f'{col}'})
    
    for status in status_list:
        left_col = 'left_'+ status
        right_col = 'right_'+ status
        df_lr[status] = df_lr.apply(lambda x: calculate_status(x, [left_col, right_col], ignorena), axis=1)

    if status_type:
        for status_type in status_type_list:
            left_col = 'left_'+ status_type
            right_col = 'right_'+ status_type
            df_lr[status_type] = df_lr.apply(lambda x: calculate_type(x, [left_col, right_col]), axis=1)

    return df_lr

def calculate_type(x, columns):

    status_type = 'Automatic'
    for column in columns:
        if x[column] == 'Manual':
            status_type = 'Manual'
            return status_type
    return status_type

def calculate_status(x, columns, ignorena=False):
    """ Calculate status for multiple columns, ie all individual statuses to Pass for combined status Pass

    Inputs:
    x: (Pandas Series)  
    columns: list(str) Names of columns to be used for combined status 
    ignorena: (bool) If True, entries with NaN automatically Pass 
    status_type: (bool) If True, separate results into Automatic and Manual

    Returns: 
    df_lr: (Pandas DataFrame)
    
    """

    status_fail_options = ['Automatic Fail', 'Manual Fail', 'Fail']
    status_na_options = []
    returnstatus = 'Pass'
    if ignorena == False:
        status_na_options.extend(['NaN', 'None'])
    for column in columns:
        if ignorena == False:
            if (isinstance(x[column], float)) or (x[column] in status_na_options):
                current_status = 'NaN'
            elif (x[column] in status_fail_options):
                current_status = 'Fail'
            else:
                current_status = 'Pass'
        else:
            if (x[column] in status_fail_options):
                current_status  = 'Fail'
            else:
                current_status = 'Pass'
        if (current_status == 'NaN') or (returnstatus == 'NaN'):
            returnstatus = 'NaN'
        elif (current_status == 'Fail') or (returnstatus == 'Fail'):
            returnstatus = 'Fail'
        else:
            returnstatus = 'Pass'
    return returnstatus

def get_average_measurements_per_hour(df):

    df = measurement_hour(df)

    df = df_working_hours(df)

    average_measurements_per_hour = len(df) / 10

    return average_measurements_per_hour

def df_working_hours(df):

    working_hours = {12, 13, 14, 15, 16, 17, 18, 19, 20, 21}

    df = df[df['MeasurementHour'].isin(working_hours)]

    return df

def measurement_hour(df):

    df['MeasurementHour'] = pd.to_datetime(df['MeasurementDate']).dt.hour.astype('category')

    return df

# def check_cols_in_df(df):
def whitespace_remover(dataframe):
   
    # iterating over the columns
    for i in dataframe.columns:
         
        # checking datatype of each columns
        if dataframe[i].dtype == 'object':
             
            # applying strip function on column
            dataframe[i] = dataframe[i].map(str.strip)
        else:
             
            # if condn. is False then it will do nothing.
            pass   
    