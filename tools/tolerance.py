# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 10:56:32 2021
A selection of classes that encode the tolerances from various standards for
the prescription outputs.
@author: RhysPoolman
"""
import numpy as np
import matplotlib.pyplot as plt
from . import utilities
from enum import Enum


class AddTolerance:
    class AddVariableName(Enum):
        add_l = "add_l"
        add_r = "add_r"

    # tolerance values
    _add_low_power_limit = 4.0  # diopters
    _add_low_power_tolerance = 0.12  # diopters
    _add_hi_power_tolerance = 0.18  # diopters

    def add_tolerance(cls, add_power):
        """
        Returns the appropriate absolute value of the +/- add tolerance for the
        given power.

        Parameters
        ----------
        add_power : float
            The absolute value for the +/- add tolerance for the given add
            power in diopters.

        Returns
        -------
        float
            The tolerance in diopters.

        """
        return cls._add_hi_power_tolernace \
            if add_power > cls._add_low_power_limit \
            else cls._add_hi_power_tolerance

    def plot_trace(self, cal_data, cal_variable_name, axis_variable_name):
        """
        A function to plot the traces of the difference in prism values as a
        propotion of the prism tolerance allowance against difference of stitct
        parameters to their baseline values.

        Parameters
        ----------
        data : dict of pandas.DataFrames
            The data from the excel spreadsheet.
        cal_variable_name : CalVariableName
            One of the names of the calibration data for an independent
            variable.
        axis_variable_name : AxisVariableName
            One of the names of the axis data for an dependent variable.

        Returns
        -------
        None.
        """
        baseline = cal_data["baseline"]
        cal_string = cal_variable_name.value
        add_string = axis_variable_name.value
        plot_data = cal_data[cal_string]
        tol = self.add_tolerance(baseline.loc[0, add_string])
        x = plot_data.loc[:, cal_string] - baseline.loc[0, cal_string]
        x = utilities.convert_pixel_to_mm(x)
        y = 100*(plot_data[add_string] - baseline.loc[0, add_string])/tol
        plt.plot(x, y, label=cal_string, linestyle='None', marker="o")


class AxisTolerance:
    class AxisVariableName(Enum):
        """
        The name of the dependent variable.
        """
        axis_l = "axis_l"
        axis_r = "axis_r"

    # tolerance values
    _axis_first_tolerance_band_limit = -0.12  # diopters
    _axis_second_tolerance_band_limit = -0.25  # diopters
    _axis_third_tolerance_band_limit = -0.5  # diopters
    _axis_fourth_tolerance_band_limit = -0.75  # diopters
    _axis_fifth_tolerance_band_limit = -1.5  # diopters
    _axis_first_tolerance_band_value = 14  # degrees
    _axis_second_tolerance_band_value = 7  # degrees
    _axis_third_tolerance_band_value = 5  # degrees
    _axis_fourth_tolerance_band_value = 3  # degrees
    _axis_fifth_tolerance_band_value = 2  # degrees

    def axis_tolerance(cls, axis_power):
        """
        Assess the number of diopters of axis power given in the argument and
        returns the correct absolute value of the +/- tolerance in diopters.

        Parameters
        ----------
        axis_power : float
            The axis power for which a tolerance is sought.

        Returns
        -------
        tol : float
            The absolute value of the +/- axis tolerance in diopeters.
        """
        tol = None
        if axis_power > cls._axis_fifth_tolerance_band_limit:
            tol = cls._axis_fifth_tolerance_band_value
        elif axis_power > cls._axis_fourth_tolerance_band_limit:
            tol = cls._axis_fourth_tolerance_band_value
        elif axis_power > cls._axis_third_tolerance_band_limit:
            tol = cls._axis_third_tolerance_band_value
        elif axis_power > cls._axis_second_tolerance_band_limit:
            tol = cls._axis_second_tolerance_band_value
        elif axis_power < cls._axis_first_tolerance_band_limit:
            # note that this clause is accessed with a less than boolean
            # operator and corresponds to the lowest power entry in the
            # axis tolerance table this is to avoid use of greater than OR
            # EQUAL TO operators see table 3 in the ANSI Z80.1-2015 Quick
            # Reference Guide for table structure
            pass
        else:
            # note that despite this clause being the last entry in the
            # if/elif/else statement it corresponds to the SECOND lowest power
            # entry in the axis tolerance table this is to avoid use of greater
            # than OR EQUAL TO operators see table 3 in the ANSI Z80.1-2015
            # Quick Reference Guide for table structure
            tol = cls._axis_first_tolerance_band_value
        return tol

    def plot_trace(self, cal_data, cal_variable_name, axis_variable_name):
        """
        A function to plot the traces of the difference in prism values as a
        proportion of the prism tolerance allowance against difference of
        stitct parameters to their baseline values.

        Parameters
        ----------
        data : dict of pandas.DataFrames
            The data from the excel spreadsheet.
        cal_variable_name : CalVariableName
            One of the names of the calibration data for an independent
            variable.
        axis_variable_name : AxisVariableName
            One of the names of the axis data for an dependent variable.

        Returns
        -------
        None.
        """
        baseline = cal_data["baseline"]
        cal_string = cal_variable_name.value
        axis_string = axis_variable_name.value
        cylinder_power_string = "cylinder_power_l" if "l" in axis_string \
            else "cylinder_power_r"
        plot_data = cal_data[cal_string]
        tol = self.axis_tolerance(baseline.loc[0, cylinder_power_string])
        x = plot_data.loc[:, cal_string] - baseline.loc[0, cal_string]
        x = utilities.convert_pixel_to_mm(x)
        y = 100*(plot_data[axis_string] - baseline.loc[0, axis_string])/tol
        plt.plot(x, y, label=cal_string, linestyle='None', marker="o")


class PrismTolerance:
    class PrismVariableNameHorizontalVertical(Enum):
        """
        The namee of the depenent variable.
        """
        horizontal_l = "horizontal_l"
        vertical_l = "vertical_l"
        horizontal_r = "horizontal_r"
        vertical_r = "vertical_r"

    # class variables
    _single_vision_vertical_tolerance = 0.33  # diopters
    _single_vision_horizontal_tolerance = 0.33  # diopters
    _progressive_vertical_tolerance = 0.33  # diopters
    _progressive_horizontal_tolerance = 0.67  # diopters
    _single_vision_vertical_upper_limits = 3.375  # diopters
    _single_vision_horizontal_upper_limits = 2.75  # diopters
    _progressive_vertical_upper_limits = 3.375  # diopters
    _progressive_horizontal_upper_limits = 3.75  # diopters

    def single_vision_vertical_tolerance(cls, value):
        """
        Returns hard coded values for the single vision vertical tolerance for
        a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._single_vision_vertical_tolerance \
                if value <= cls._single_vision_vertical_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._single_vision_vertical_tolerance \
                    if entry <= cls._single_vision_vertical_upper_limits else \
                    None
        return rtv

    def single_vision_horizontal_tolerance(cls, value):
        """
        Returns hard coded values for the single vision horizontal tolerance
        for a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._single_vision_horizontal_tolerance \
                if value <= cls._single_vision_horizontal_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._single_vision_horizontal_tolerance \
                    if entry <= cls._single_vision_horizontal_upper_limits\
                    else None
        return rtv

    def progressive_vertical_tolerance(cls, value):
        """
        Returns hard coded values for the progressive vertical tolerance for
        a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._progressive_vertical_tolerance \
                if value <= cls._progressive_vertical_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._progressive_vertical_tolerance \
                    if entry <= cls._progressive_vertical_upper_limits else \
                    None
        return rtv

    def progressive_horizontal_tolerance(cls, value):
        """
        Returns hard coded values for the progressive horizontal tolerance for
        a given value.

        Parameters
        ----------
        value : float or collection of floats
            The value(s) that will define which tolerance valeus are used.

        Returns
        -------
        float or collection of floats
            The tolerance values.
        """
        rtv = None
        try:
            rtv = cls._progressive_horizontal_tolerance \
                if value <= cls._progressive_horizontal_upper_limits else \
                None
        except ValueError:
            rtv = np.zeros(len(value))
            for ii, entry in enumerate(value):
                rtv[ii] = cls._progressive_horizontal_tolerance \
                    if entry <= cls._progressive_horizontal_upper_limits else \
                    None
        return rtv

    def plot_trace(self, cal_data, cal_variable_name, prism_variable_name,
                   is_progressive=False):
        """
        A function to plot the traces of the difference in prism values as a
        propotion of the prism tolerance allowance against difference of stitct
        parameters to their baseline values.

        Parameters
        ----------
        data : dict of pandas.DataFrames
            The data from the excel spreadsheet.
        cal_variable_name : CalVariableName
            One of the names of the calibration data for an independent
            variable.
        prism_variable_name : PrismVariableNameHorizontalVertical
            One of the names of the prism data for an dependent variable.
        is_progressive : boolean
            If True the toleranceances used in he plot are for progressive
            lenses, otherwise single vision tolerances are used.  Defaults to
            False.

        Returns
        -------
        None.
        """
        baseline = cal_data["baseline"]
        cal_string = cal_variable_name.value
        prism_string = prism_variable_name.value
        plot_data = cal_data[cal_string]
        tol = None
        prism = plot_data[prism_string]
        baseline_prism = baseline[prism_string]
        if prism_string == \
                self.PrismVariableNameHorizontalVertical.horizontal_l.value:
            tol = self.progressive_horizontal_tolerance(baseline_prism) \
                if is_progressive else \
                self.single_vision_horizontal_tolerance(baseline_prism)
        elif prism_string == \
                self.PrismVariableNameHorizontalVertical.vertical_l.value:
            tol = self.progressive_vertical_tolerance(baseline_prism) \
                if is_progressive else \
                self.single_vision_vertical_tolerance(baseline_prism)
        elif prism_string == \
                self.PrismVariableNameHorizontalVertical.horizontal_r.value:
            tol = self.progressive_horizontal_tolerance(baseline_prism) \
                if is_progressive else \
                self.single_vision_horizontal_tolerance(baseline_prism)
        elif prism_string == \
                self.PrismVariableNameHorizontalVertical.vertical_r.value:
            tol = self.progressive_vertical_tolerance(baseline_prism) \
                if is_progressive else \
                self.single_vision_vertical_tolerance(baseline_prism)
        else:
            msg = "PrismTolerance.plot_trace: Unrecognised prism_variable_name"
            msg += ", is the value a PrismVariableNameHorizontalVertical enum?"
            raise ValueError(msg)
        x = plot_data.loc[:, cal_string] - baseline.loc[0, cal_string]
        x = utilities.convert_pixel_to_mm(x)
        y = 100*(plot_data[prism_string] - baseline.loc[0, prism_string])/tol
        plt.plot(x, y, label=cal_string, linestyle='None', marker="o")


class ProgressiveTolerance:
    # WARNING: UNTESTED
    # tolerance values
    # PAY ATTENTION some tolerances are diopeters others are percentages
    _spherical_median_power_limits = 8.0  # diopters
    _cylinder_low_power_upper_limit = 0.0  # diopters
    _cylinder_low_power_lower_limit = -2.0  # diopters
    _cylinder_mid_power_lower_limit = -3.5  # diopters
    _spherical_low_tolerance = 0.16  # diopters
    _spherical_hi_tolerance = 0.02  # *100 percentage
    _cylinder_low_tolerance = 0.16  # diopters
    _cylinder_mid_tolerance = 0.18  # diopters
    _cylinder_hi_tolerance = 0.05  # *100 percentage

    def spherical_tolerance(cls, spherical_meridian_power):
        """
        Returns the tolerance on a specific sphere meridian power as the
        absolute value of a +/- tolerance.  See pp.23 of ANSI Z80.1-2015 for
        table the figure were taken from or Table 2 in the associted Quick
        Reference Guide.

        Parameters
        ----------
        spherical_meridian_power : float
            The spherical meridian power in diopters.

        Returns
        -------
        float
            The absolute value of a +/- tolerance in diopeters for the given
            power.
        """
        return cls._spherical_hi_tolerance*spherical_meridian_power \
            if spherical_meridian_power > cls._spherical_median_power_limits \
            else cls._spherical_low_tolerance

    def cylinder_tolerance(cls, cylinder_power):
        """
        Returns the tolerance on a specific cylinder power as the absolute
        value of a +/- tolerance.  See pp.23 of ANSI Z80.1-2015 for
        table the figure were taken from or Table 2 in the associted Quick
        Reference Guide.

        Parameters
        ----------
        cylinder_power : float
            The cylinder power in diopters.

        Returns
        -------
        tol: float
            The absolute value of a +/- tolerance in diopeters for the given
            power.
        """
        tol = None
        if cylinder_power <= cls._cylinder_low_power_upper_limit or \
                cylinder_power >= cls._cylinder_low_power_lower_limit:
            tol = cls._cylinder_low_tolerance
        elif cylinder_power < cls._cylinder_low_power_lower_limit or \
                cylinder_power >= cls._cylinder_mid_power_lower_limit:
            tol = cls._cylinder_mid_tolerance
        elif cylinder_power > cls._cylinder_mid_power_lower_limit:
            tol = cls._clinder_hi_tolerance*cylinder_power
        return tol
