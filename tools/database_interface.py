# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 11:22:24 2022
Taken from Fiona Loftus work on a python interface to the database.
@author: RhysPoolman
"""
import pandas as pd
import pyodbc
import os
import yaml
from typing import Iterable
import csv
import time
from pathlib import Path
from datetime import datetime


def get_project_root() -> Path:
    return Path(__file__).parent.parent


def flatten(items):
    """Yield items from any nested iterable"""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x


def sql_query_to_df(config, start_date, end_date, accountids,
                    record_time=None):
    """ Run sql query to retrieve selected database entries, given a star
    date, end date and accountids

    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns:
    df: (Pandas DataFrame)
    """

    server = config['sql_configs']['server']
    db = config['sql_configs']['db']
    user = config['sql_configs']['user']
    pwd = config['sql_configs']['pwd']

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server=" +
                          server + ";database=" + db + ";UID=" + user +
                          ";PWD=" + pwd + ";")

    query = config['sql_configs']['query']
    query = query.format('?', ','.join('?' * len(accountids)))
    days = (end_date - start_date).days
    try:
        start = time.time()
        df = pd.read_sql_query(query, conn,  params=params)
        end = time.time()
        start_time = datetime.fromtimestamp(start)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    if record_time is not None:
        if df is None:
            size_df = None
        else:
            size_df = len(df)
        path = record_time['path']
        filename = record_time['filename']
        append_time_csv(path, filename, start_time, end-start, size_df, days)
    conn.close()

    return df


def sql_query_to_df_all_accounts(config, start_date, end_date,
                                 record_time=None):
    """ Run sql query to retrieve selected database entries, given a start
    date, end date.

    Inputs:
    config: (dict)
        server:
        db:
        user:
        pwd:
        query:
    start_date: (str)
    end_date: (str)
    accountids: list(str)

    Returns:
    df: (Pandas DataFrame)
    """

    server = config['sql_configs']['server']
    db = config['sql_configs']['db']
    user = config['sql_configs']['user']
    pwd = config['sql_configs']['pwd']
    accounts = config['accounts']
    clients = config['clients']

    accountids = []

    for client in config['clients']:
        if isinstance(config['accounts'][client], dict):
            accountids.append(list(config['accounts'][client].values()))
        else:
            accountids.append(config['accounts'][client])

    accountids = [x for x in flatten(accountids)]

    params = tuple(flatten((accountids, start_date, end_date)))

    conn = pyodbc.connect("driver={ODBC Driver 17 for SQL Server};server=" +
                          server + ";database=" + db + ";UID=" + user + ";PWD="
                          + pwd + ";")

    query = config['sql_configs']['query']
    query = query.format('?', ','.join('?' * len(accountids)))

    days = (end_date - start_date).days
    try:
        start = time.time()
        df = pd.read_sql_query(query, conn,  params=params)
        end = time.time()
        start_time = datetime.fromtimestamp(start)
    except Exception as ex:
        print('Exception:')
        print(ex)
        df = None
    if record_time is not None:
        if df is None:
            size_df = None
        else:
            size_df = len(df)
        path = record_time['path']
        filename = record_time['filename']
        append_time_csv(path, filename, start_time, end-start, size_df, days)
    conn.close()

    return df


def append_time_csv(path, filename, start, duration, size_df, days):
    row = {'start_time': start, 'duration': duration, 'file_size': size_df,
           'days': days}
    row['start_time'] = row['start_time'].strftime('%Y/%m/%d %H:%M:%S')

    if not os.path.exists(os.path.join(path, filename)):

        with open(os.path.join(path, filename), 'w', newline='') as csvfile:
            fieldnames = ['start_time', 'duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow(row)
    else:
        with open(os.path.join(path, filename), 'a', newline='') as csvfile:
            fieldnames = ['start_time', 'duration', 'file_size', 'days']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writerow(row)


def db_to_portal_labels(df, db_portal_dict_yml, path):
    """ Convert database values to recognised portal values using dictionary

    Inputs:
    df: (Pandas DataFrame)
    db_portal_dict_yml: (str) Name of config file with db_to_portal dictionary
    path: (str) Path to directory with config file

    Returns:
    df: (Pandas DataFrame)
    """

    with open(os.path.join(path, db_portal_dict_yml)) as stream:
        db_portal_dict = yaml.safe_load(stream)

    for step in db_portal_dict:
        if step in df.columns:
            db_portal_dict[step] = {k: 'NaN' if not v else v
                                    for k, v in db_portal_dict[step].items()}
            df[step] = df[step].replace(db_portal_dict[step])
    return df


def lens_pairs_df(df, ignorena=False, cols=None, status_type=False,
                  status_list=[], status_type_list=[]):
    """ Consolidate individual lens measurements into lens pairs

    Inputs:
    df: (Pandas DataFrame) Contains separate rows for left and right lenses
    ignorena: (bool) If True,
    cols: list(str)
    status_type: (bool) If True, separate results into Automatic and Manual
    status_list: list(str)
    status_type_list: list(str)

    Returns:
    df_lr: (Pandas DataFrame)
    """

    df_left = df[df['LensLocation'] == 'OculusSinister']
    df_right = df[df['LensLocation'] == 'OculusDexter']
    df_left = df_left.add_prefix('left_')
    df_left = \
        df_left.rename(columns={'left_SerialId': 'SerialId',
                                'left_MeasurementDate': 'MeasurementDate'})
    df_right = df_right.add_prefix('right_')
    df_right = \
        df_right.rename(columns={'right_SerialId': 'SerialId',
                                 'right_MeasurementDate': 'MeasurementDate'})

    df_lr = pd.merge(df_left, df_right, on=['SerialId', 'MeasurementDate'],
                     how='inner')
    if cols is not None:
        for col in cols:
            df_lr = df_lr.rename(columns={f'right_{col}': f'{col}'})

    for status in status_list:
        left_col = 'left_' + status
        right_col = 'right_' + status
        df_lr[status] = \
            df_lr.apply(lambda x: calculate_status(x, [left_col, right_col],
                                                   ignorena), axis=1)

    if status_type:
        for status_type in status_type_list:
            left_col = 'left_' + status_type
            right_col = 'right_' + status_type
            df_lr[status_type] = \
                df_lr.apply(lambda x: calculate_type(x, [left_col, right_col]),
                            axis=1)

    return df_lr


def calculate_type(x, columns):

    status_type = 'Automatic'
    for column in columns:
        if x[column] == 'Manual':
            status_type = 'Manual'
            return status_type
    return status_type


def calculate_status(x, columns, ignorena=False):
    """ Calculate status for multiple columns, ie all individual statuses to
    Pass for combined status Pass.

    Inputs:
    x: (Pandas Series)
    columns: list(str) Names of columns to be used for combined status
    ignorena: (bool) If True, entries with NaN automatically Pass
    status_type: (bool) If True, separate results into Automatic and Manual

    Returns:
    df_lr: (Pandas DataFrame)
    """
    status_fail_options = ['Automatic Fail', 'Manual Fail', 'Fail']
    status_na_options = []
    returnstatus = 'Pass'
    if not ignorena:
        status_na_options.extend(['NaN', 'None'])
    for column in columns:
        if not ignorena:
            if (isinstance(x[column], float)) or \
                    (x[column] in status_na_options):
                current_status = 'NaN'
            elif (x[column] in status_fail_options):
                current_status = 'Fail'
            else:
                current_status = 'Pass'
        else:
            if (x[column] in status_fail_options):
                current_status = 'Fail'
            else:
                current_status = 'Pass'
        if (current_status == 'NaN') or (returnstatus == 'NaN'):
            returnstatus = 'NaN'
        elif (current_status == 'Fail') or (returnstatus == 'Fail'):
            returnstatus = 'Fail'
        else:
            returnstatus = 'Pass'
    return returnstatus


def get_average_measurements_per_hour(df):

    df = measurement_hour(df)

    df = df_working_hours(df)

    average_measurements_per_hour = len(df) / 10

    return average_measurements_per_hour


def df_working_hours(df):

    working_hours = {12, 13, 14, 15, 16, 17, 18, 19, 20, 21}

    df = df[df['MeasurementHour'].isin(working_hours)]

    return df


def measurement_hour(df):

    df['MeasurementHour'] = \
        pd.to_datetime(df['MeasurementDate']).dt.hour.astype('category')

    return df
