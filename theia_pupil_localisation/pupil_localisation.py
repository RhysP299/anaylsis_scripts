# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 08:50:01 2022

Scripts for testing pupil localisation algorithms

@author: FionaLoftus
"""
# %%

import os
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import distance_matrix
import h5py
import yaml
import imagej
import pandas as pd
# MODEL_DICT: {'model1': method_1_cdf,
#              'model2': method_2_projections,
#              'model3a': method_3a_edge,
#              'model3b': method_3b_edge,
#              'model5': method_5_gradient_dot}

# %%
# def check_cols_in_df(df):
def whitespace_remover(dataframe):
   
    # iterating over the columns
    for i in dataframe.columns:
         
        # checking datatype of each columns
        if dataframe[i].dtype == 'object':
             
            # applying strip function on column
            dataframe[i] = dataframe[i].map(str.strip)
        else:
             
            # if condn. is False then it will do nothing.
            pass

    return dataframe

def load_yaml(file_path, filename):
    """
    Load yaml file given path and filename
    Parameters
    ---------
    path: str
        path to yaml file directory
    filename: str
        filename of yaml file
    
    Returns
    ---------
    yaml_dict: dict
        yaml as dictionary
        
    """
    try:
        with open(os.path.join(file_path, filename)) as stream:
            yaml_dict = yaml.safe_load(stream)
    except OSError as e:
        # logger.error(e)
        raise
        
    return yaml_dict
def load_convert_video_image(vid_link, frame_num):
    
    vid1 = cv.VideoCapture(vid_link)
    vid1.set(1, frame_num)
    success,image = vid1.read()
    
    image = img_to_gray(image)
    
    return image

def img_to_gray(image):
    
    image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
    image_gs = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
    
    return image_gs

def method_1_cdf(image, params):
    
    image = img_to_gray(image)
    # 1. filter image based on CDF
    image_1 = cdf_filter(image, params['cdf_thresh'])
    # fig, ax = plt.subplots()
    # ax.imshow(image_1, cmap='gray')
    # 2. apply erosion
    erosion_shape = cv.MORPH_RECT
    erosion_size = params['erosion_radius']
    erosion_element = cv.getStructuringElement(erosion_shape,\
                                               (2 * erosion_size + 1, 2 * erosion_size + 1), \
                                                  (erosion_size, erosion_size))
    image_2 = cv.erode(image_1, erosion_element)
    fig, ax = plt.subplots()
    ax.imshow(image_2, cmap='gray')

    # 3. find minimum intensity pixel in masked region
    image_3 = cv.bitwise_and(image,image,mask=image_2)
    image_3_m = np.ma.masked_equal(image_3, 0)
    # coordinates of minimum pixel value
    coords_pmi = np.unravel_index(np.argmin(image_3_m), image_3_m.shape)
    
    # 4. average intensity in original image nxn around PMI
    average_radius = params['average_radius']
    mask = np.zeros(image.shape[:2], dtype="uint8")
    mask_rect = \
        cv.rectangle(mask,(coords_pmi[1]-average_radius,coords_pmi[0]-average_radius),\
                     (coords_pmi[1]+average_radius,coords_pmi[0]+average_radius), 255, -1)
    mask_rect_image = cv.bitwise_and(image,image,mask=mask_rect)
    mask_rect_image = np.ma.masked_equal(mask_rect_image, 0)
    average_iris = np.mean(mask_rect_image)
   
    # 5. erosion original image
    erosion_shape = cv.MORPH_RECT
    erosion_size = params['erosion_radius_2']
    erosion_element = cv.getStructuringElement(erosion_shape,\
                                               (2 * erosion_size + 1, 2 * erosion_size + 1), \
                                                  (erosion_size, erosion_size))
    image_4 = cv.erode(image, erosion_element)
    # 6. threshold nxn region around PMI in original image
    threshold_radius = params['threshold_radius']
    
    # threshold whole image
    thr, image_thr = cv.threshold(image_4, average_iris, 255,cv.THRESH_BINARY_INV)
    
    mask_2 = np.zeros(image_4.shape[:2], dtype="uint8")
    cv.rectangle(mask_2,(coords_pmi[1]-threshold_radius,coords_pmi[0]-threshold_radius),\
                     (coords_pmi[1]+threshold_radius,coords_pmi[0]+threshold_radius), 255, -1)
    image_mask_thr = cv.bitwise_and(image_thr,image_thr,mask=mask_2)
    
    # 7. find gravitational centre
    # calculate moments of binary image
    M = cv.moments(image_mask_thr)
    # calculate x,y coordinate of center
    cX = int(M["m10"] / M["m00"])

    cY = int(M["m01"] / M["m00"])
    
    fig, ax = plt.subplots()
    pos = ax.imshow(image)
    ax.plot(cX,cY,'*r')
    fig.colorbar(pos)
    
    return (cX, cY)

def method_2_projections(image, params):
    
    image = img_to_gray(image)

    ipf_y = np.sum(image, axis=0) / image.shape[0]
    ipf_x = np.sum(image, axis=1) / image.shape[1]

    image_ipf_y = np.broadcast_to(ipf_y,(image.shape[0], image.shape[1]))
    image_ipf_diff_y = np.subtract(image, image_ipf_y)
    image_ipf_diff_y_square = np.square(image_ipf_diff_y)
    vpf_y = np.sum(image_ipf_diff_y_square, axis=0) / image_ipf_diff_y_square.shape[0]

    image_ipf_x = np.broadcast_to(np.array(ipf_x, ndmin=2).transpose(),(image.shape[0], image.shape[1]))
    image_ipf_diff_x = np.subtract(image, image_ipf_x)
    image_ipf_diff_x_square = np.square(image_ipf_diff_x)
    vpf_x = np.sum(image_ipf_diff_x_square, axis=1) / image_ipf_diff_x_square.shape[1]
    alpha = params['alpha'] # 0.1
    grad_thr = params['grad_thr'] # 2
    gpf_y = (1-alpha)*ipf_y + alpha*vpf_y
    gpf_x = (1-alpha)*ipf_x + alpha*vpf_x
    # gradient of gpf
    diff_gpf_y = np.pad(np.diff(gpf_y, 1), (0, 1), 'edge')
    diff_gpf_x = np.pad(np.diff(gpf_x, 1), (0, 1), 'edge')

    diff2_gpf_y = np.pad(np.diff(diff_gpf_y, 1), (0, 1), 'edge')
    diff2_gpf_x = np.pad(np.diff(diff_gpf_x, 1), (0, 1), 'edge')

    # ID points where grad > thr
    x_len_thr = params['x_len_thr'] # 5
    x_poss = np.argwhere(diff_gpf_y > grad_thr).flatten()
    y_poss = np.argwhere(diff_gpf_x > grad_thr).flatten()
    # grad2_x = diff2_gpf_y[x_poss]
    # grad2_y = diff2_gpf_x[y_poss]
    # x1 = x_poss[0]
    # x2 = x_poss[-1]
    x_poss_indices_diff = np.pad(np.diff(x_poss), (0, 1), 'constant', constant_values=0)
    x_indices = x_poss[x_poss_indices_diff > x_len_thr]
    x3 = x_indices[1]
    x4 = x_indices[-2]
    y1 = y_poss[0]
    y2 = y_poss[-1]

    cX = (x3+x4)/2
    cY = (y1+y2)/2

    fig, ax = plt.subplots()
    ax.imshow(image, cmap='gray')
    ax.plot(cX, cY, 'r*')
    
    return (cX, cY)

def method_3a_edge(image, params):
    
    # 2. Canny edge detection
    # mean intensity of image
    edge_1_coeff = params['edge_1_coeff']
    edge_2_coeff = params['edge_2_coeff']
    
    image_3 = cv.Canny(image, edge_1_coeff, edge_2_coeff)
    fig, ax = plt.subplots()
    ax.imshow(image)
    cX, cY = top_3_lines_median_intersection(image_3)
    # plt.vlines(h_3, 0, image_3.shape[0])
    # plt.hlines(v_3, 0, image_3.shape[1])
    plt.plot(cX,cY,'r*')
    return (cX, cY)
    
def method_3b_edge(image, params):
    
    image = img_to_gray(image)
    # 1. Gaussian blur
    smooth_radius = params['smooth_radius']
    image_2 = cv.GaussianBlur(image, [2*smooth_radius+1, 2*smooth_radius+1], 0)
    fig, ax = plt.subplots()
    
    # 2. Canny edge detection
    # mean intensity of image
    edge_1_coeff = params['edge_1_coeff']
    edge_2_coeff = params['edge_2_coeff']
    image_mean = np.mean(image_2)
    
    image_3 = cv.Canny(image_2, edge_1_coeff*image_mean, edge_2_coeff*image_mean)
    ax.imshow(image)
    
    cX, cY = top_2_lines_midpoint(image_3)
    # plt.vlines(h_3, 0, image_3.shape[0])
    # plt.hlines(v_3, 0, image_3.shape[1])
    plt.plot(cX,cY,'r*')
    return (cX, cY)
    
def method_4_gradient_flow(image, params):
    
    image = img_to_gray(image)
    # 1. find x and y gradient of image (magnitude and direction)
    kx = np.array([[-1, 0, 1], 
               [-2, 0, 2], 
               [-1, 0, 1]])
    ky = np.array([[1,   2,  1], 
               [0,   0,  0], 
              [-1,  -2, -1]])
    ix = cv.filter2D(img, -1, kx)
    iy = cv.filter2D(img, -1, ky)
    G = np.hypot(ix, iy)
    # 2. x and y gradient directions where magnitude > threshold
    grad_thresh = np.mean(G)
    print(grad_thresh)
    G_thresh = np.ma.masked_greater(G, grad_thresh)
    G_thresh_binary = np.ma.make_mask(G_thresh, copy=True).astype('uint8')
    ix_thr = cv.bitwise_and(ix, ix, mask=G_thresh_binary)
    iy_thr = cv.bitwise_and(iy, iy, mask=G_thresh_binary)
    # 3. start and end coordinates of lines in direction against gradients
    lines_coords = []
    for x in np.arange(0, np.shape(image)[0]):
        for y in np.arange(0, np.shape(image)[1]):
            if G_thresh[x,y] != 0:
                x0 = x
                y0 = y
                lines_coords.append([x0,y0])
    # 4. initialise grid with zeros
    
    # 4. for each line, Bresenham's algorithm to get intersecting pixel coordinates
    
    # 5. add 1 to each pixel sharing line coordinates
    
    # 6. image convolution with nxn average kernel
    
def method_5_gradient_dot(image, params):
    
    image = img_to_gray(image)
    
    scale_percent = params['scale_percent'] # percent of original size (20)
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)
    dim = (width, height)
      
    # resize image
    image = cv.resize(image, dim, interpolation = cv.INTER_LINEAR)
    
    kx = np.array([[-1, 0, 1], 
               [-2, 0, 2], 
               [-1, 0, 1]])
    ky = np.array([[1,   2,  1], 
               [0,   0,  0], 
              [-1,  -2, -1]])
    ix = cv.filter2D(image, -1, kx)
    iy = cv.filter2D(image, -1, ky)
    g_mag = np.hypot(ix, iy)
    # 2. x and y gradient directions where magnitude > threshold
    # grad_thresh = np.mean(g_mag)
    grad_thresh = params['grad_thresh'] # (10)
    g_thresh = np.ma.masked_less(g_mag, grad_thresh)
    g_thresh = np.ma.filled(g_thresh,0)
    g_thresh_binary = np.ma.make_mask(g_thresh, copy=True).astype('uint8')
 
    # 3. Initialise c-grid with zeros
    c_grid = np.zeros(image.shape)
 
    x = np.arange(0, image.shape[1])
    y = np.arange(0, image.shape[0])
    xc, yc = np.meshgrid(x,y)
 
    xg = xc[g_thresh_binary==1]
    yg = yc[g_thresh_binary==1]
 
    c_coords = [xc.flatten(), yc.flatten()]
    g_coords = np.array([xg,yg]).transpose()
    g_vec_x = ix[g_thresh_binary==1]
    g_vec_y = iy[g_thresh_binary==1]
    g_vec = np.array([g_vec_x,g_vec_y]).transpose()
    norms = np.apply_along_axis(np.linalg.norm, 1, g_vec)
    g_vec = g_vec / norms.reshape(1,-1).T
    # 4. calculation
    # loop over entire image
    for xcc in np.arange(0, image.shape[1]):
        for ycc in np.arange(0, image.shape[0]):
            c_coords = np.repeat([[xcc,ycc]],np.shape(g_vec)[0], axis=0)
            disp_vecs = np.subtract(c_coords, g_coords)
            disp_norms = np.hypot(disp_vecs[:,0],disp_vecs[:,1])
            # disp_norms = np.apply_along_axis(np.linalg.norm, 1, disp_vecs)
            disp_norms[disp_norms==0] = 1
            disp_vecs = disp_vecs / disp_norms.reshape(1,-1).T
            dot_prod = np.sum(abs(disp_vecs*g_vec),axis=1)
            c_grid[ycc,xcc] = np.sum(dot_prod)
    
    cc = (np.argmax(c_grid))
    (cY, cX) = np.unravel_index(cc, image.shape)
    return c_grid
    
    
def top_3_lines_median_intersection(image):
    
    v_sum_indices, h_sum_indices = order_lines_by_sum(image)
    
    # indices of top 3 lines
    v_top_3 = v_sum_indices[-4:-1]
    h_top_3 = h_sum_indices[-4:-1]
    
    # eye centre as middle line
    cY = v_top_3[1]
    cX = h_top_3[1]
    
    return cX, cY

def top_2_lines_midpoint(image):
    
    v_sum_indices, h_sum_indices = order_lines_by_sum(image)
    
    v_top_2 = v_sum_indices[-3:-1]
    h_top_2 = h_sum_indices[-3:-1]
    
    # eye centre as mid point
    cY = (v_top_2[0] + v_top_2[1])/2
    cX = (h_top_2[0] + h_top_2[1])/2
    
    return cX, cY

def order_lines_by_sum(image):
    
    # 1. sum over vertical and horizontal
    h_sum = np.sum(image, 1)
    v_sum = np.sum(image, 0)
    
    # 2. sort indices by sum
    h_sum_sort_indices = np.argsort(h_sum)
    v_sum_sort_indices = np.argsort(v_sum)
    
    return h_sum_sort_indices, v_sum_sort_indices

def cdf_filter(image, value):
    
    hist,bins = np.histogram(image.flatten(),256,[0,256])

    cdf = hist.cumsum()
    cdf_normalised = cdf / cdf[-1]
    cdf_norm_thresh = np.ma.masked_greater(cdf_normalised, 0.05)
    cdf_norm_thresh = np.ma.filled(cdf_norm_thresh,0)
    cdf_thresh_binary = np.ma.make_mask(cdf_norm_thresh, copy=True)
    
    img2 = cdf_thresh_binary[image].astype('uint8')
    
    return img2

# function for line generation
def bresenham(x1,y1,x2, y2):
 
    m_new = 2 * (y2 - y1)
    slope_error_new = m_new - (x2 - x1)
 
    y=y1
    for x in range(x1,x2+1):
     
        print("(",x ,",",y ,")\n")
 
        # Add slope to increment angle formed
        slope_error_new =slope_error_new + m_new
 
        # Slope error reached limit, time to
        # increment y and update slope error.
        if (slope_error_new >= 0):
            y=y+1
            slope_error_new =slope_error_new - 2 * (x2 - x1)
            
def train_evaluate(search_params):
    
    # Load all data
    # im, mcx,ycx
    # for all images, find difference between mcx and ccx and mcy and ccy
    # objective function is mean distance over all images
    return 0 
            
# Define objective function to minimise
def objective(**params):
    all_params = {**params, **STATIC_PARAMS}
    return train_evaluate(all_params)

def load_image_data():
    
    img_dict = {}
    
    folder_dir = 0
# %%
###################################################################################################
# METHOD FOR SINGLE IMAGE
# 1. Load image
data_path = 'D:\\Theia_data'
config_path = 'D:\\python_code\\anaylsis_scripts\\theia_pupil_localisation'

config = load_yaml(config_path, 'pupil_config.yml')
im_name =  'V003'
eye1_path = os.path.join(data_path, config['d2_w'], f'{im_name}.tif')

image = cv.imread(eye1_path)
image = img_to_gray(image)

# 2. Import annotations
ij = imagej.init(mode='interactive')

imp = ij.IJ.openImage(eye1_path)
ij.IJ.run(imp, 'List Elements', '')
ij.IJ.saveAs("Results", os.path.join(data_path, config['d2_w'], f'{im_name}_overlay.csv'))
#%%
overlay_df = pd.read_csv(os.path.join(data_path, config['d2_w'], f'{im_name}_overlay.csv'), skipinitialspace=True)
# remove whitespace
overlay_df = whitespace_remover(overlay_df)
# %%
# 3. Check for a) pupil or b) limbus annotation
if 'Pupil' in overlay_df['Name'].values:
    idx = overlay_df.index[overlay_df['Name']=='Pupil'].values[0]
elif 'Limbus' in overlay_df['Name']:
    idx = overlay_df.index[overlay_df['Name']=='Limbus'].values[0]
    # reject image
# 4. Compute measured centre mcx, mcy
annotation_LX = overlay_df.iloc[idx].loc['X']
annotation_LY = overlay_df.iloc[idx].loc['Y']
annotation_width = overlay_df.iloc[idx].loc['Width']
annotation_height = overlay_df.iloc[idx].loc['Height']

mcx = annotation_LX + annotation_width/2
mcy = annotation_LY + annotation_height/2

#%%
fig, ax = plt.subplots()
ax.imshow(image, cmap='gray')
ax.plot(mcx, mcy, 'r*')
# 5. Run image through method
# 6. Compute error
# 7. For debug, show image and points
# %%
data_path = 'D:\\Theia_data'
config_path = 'D:\\python_code\\anaylsis_scripts\\theia_pupil_localisation'

config = load_yaml(config_path, 'pupil_config.yml')

eye1_path = os.path.join(data_path, config['d2_w'], 'V003.tif')
# theia_path = os.path.join(data_path, config['dt'], '290622', 'Video_1')

# theia_vid = os.path.join(theia_path, 'Video_1.mp4')

image = cv.imread(eye1_path)
image = img_to_gray(image)
mcx = 1607+int(779/2)
mcy = 936+int(773/2)
ccx = 1307+int(779/2)
ccy = 946+int(773/2)
# image = load_convert_image(theia_vid)
# %%
from PIL import Image
from PIL.TiffTags import TAGS

with Image.open(eye1_path) as img:
    meta_dict = {TAGS[key] : img.tag[key] for key in img.tag_v2}
# %%
fig, ax = plt.subplots()
ax.imshow(image, cmap='gray')
ax.plot(mcx, mcy, 'r*')
ax.plot(ccx, ccy, 'b*')

# %%
# THEIA IMAGES
annotations_path = os.path.join(data_path, config['d2'],  'annotations.mat')
f = h5py.File(annotations_path, 'r')
print(f.keys())
data = f.get('#refs#')
data = np.array(data)
# %%
# load first frame from each folder
vids_paths = {'vid1': os.path.join(data_path, config['dt'], '040722', '01', 'Video_3.mp4'),
              'vid2': os.path.join(data_path, config['dt'], '040722', '01', 'Video_4.mp4'),
              'vid3': os.path.join(data_path, config['dt'], '040722', '02', 'Video_9.mp4'),
              'vid4': os.path.join(data_path, config['dt'], '040722', '02', 'Video_10.mp4'),
              'vid5': os.path.join(data_path, config['dt'], '040722', '03', 'Video_6.mp4'),
              'vid6': os.path.join(data_path, config['dt'], '040722', '03', 'Video_8.mp4'),
              'vid7': os.path.join(data_path, config['dt'], '040722', '04', 'Video_1.mp4'),
              'vid8': os.path.join(data_path, config['dt'], '040722', '04', 'Video_2.mp4'),
              'vid9': os.path.join(data_path, config['dt'], '080722', '01', 'Video_3.mp4'),
              'vid10': os.path.join(data_path, config['dt'], '080722', '01', 'Video_7.mp4'),
              'vid11': os.path.join(data_path, config['dt'], '080722', '02', 'Video_4.mp4')}

vids = {}
for key, value in vids_paths.items():
    vids[key] = load_convert_video_image(value, 1)

# %%
for im in vids.values():
    fig, ax = plt.subplots()
    ax.imshow(im, cmap='gray')

# %%
image = vids['vid1']
fig, ax = plt.subplots()
ax.imshow(image)
# %%
method_1_params = {'cdf_thresh': 0.05,
                   'erosion_radius': 1,
                   'average_radius': 10,
                   'erosion_radius_2': 1,
                   'threshold_radius': 15}
method_1_cdf(image, method_1_params)
# %%
method_2_params = {'alpha': 0.1,
                   'grad_thresh': 2}
method_2_projections(image, method_2_params)
# %%
method_3a_params ={'edge_1_coeff': 3,
                  'edge_2_coeff': 20}
img = method_3a_edge(image, method_3a_params)

method_3b_params ={'smooth_radius': 0,
                  'edge_1_coeff': 0.1,
                  'edge_2_coeff': 0.8}
img = method_3b_edge(image, method_3b_params)

# %%
method_5_params = {'scale_percent': 20,
                   'grad_thresh': 2}
c_grid = method_5_gradient_dot(image, method_5_params)
# %%
scale_percent = 20 # percent of original size
width = int(image.shape[1] * scale_percent / 100)
height = int(image.shape[0] * scale_percent / 100)
dim = (width, height)
  
# resize image
img_red = cv.resize(image, dim, interpolation = cv.INTER_LINEAR)
cc = (np.argmax(c_grid))
cY, cX = np.unravel_index(cc, img_red.shape)
fig, ax = plt.subplots()
ax.imshow(c_grid)
ax.plot(cX, cY, '*r')

# %%

            
# %%
image = img_to_gray(image)
scale_percent = 20 # percent of original size
width = int(image.shape[1] * scale_percent / 100)
height = int(image.shape[0] * scale_percent / 100)
dim = (width, height)
  
# resize image
image = cv.resize(image, dim, interpolation = cv.INTER_LINEAR)
# %%

# %%

# %%


# %%
vid1 = cv.VideoCapture(theia_vid)
success,image = vid1.read()
count = 0
# image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
cv.imwrite(os.path.join(theia_path, "frame%d.jpg" % count), image)
fig, ax = plt.subplots()
image = cv.cvtColor(image, cv.COLOR_BGR2RGB)
image_gs = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
# ax.imshow(image_gs, cmap='gray')
# while success:
#   cv.imwrite(os.path.join(theia_path, "frame%d.jpg" % count), image)     # save frame as JPEG file      
#   success,image = vid1.read()
#   print('Read a new frame: ', success)
#   count += 1
image_hist = cv.equalizeHist(image_gs)
# ax.imshow(image_hist, cmap='gray')
hist,bins = np.histogram(image_gs.flatten(),256,[0,256])

cdf = hist.cumsum()
cdf_normalised = cdf / cdf[-1]
cdf_norm_thresh = np.ma.masked_less(cdf_normalised, 0.05)
cdf_norm_thresh = np.ma.filled(cdf_norm_thresh,0)
cdf_thresh_binary = np.ma.make_mask(cdf_norm_thresh, copy=True)

cdf_m = np.ma.masked_equal(cdf,0)
cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
cdf = np.ma.filled(cdf_m,0).astype('uint8')

img2 = cdf_thresh_binary[image_gs]
ax.imshow(img2, cmap='gray')

# plt.plot(np.arange(0,256), cdf_normalised, color = 'b')
# plt.hist(image_gs.flatten(),256,[0,256], color = 'r')
# cdf_m = np.ma.masked_equal(cdf,0)
# plt.xlim([0,256])
# plt.legend(('cdf','histogram'), loc = 'upper left')
# plt.show()
# %%
eye1 = cv.imread(eye1_path)
eye1 = cv.cvtColor(eye1, cv.COLOR_BGR2RGB)
eye1_gs = cv.cvtColor(eye1, cv.COLOR_RGB2GRAY)

fig, ax = plt.subplots()

ax.imshow(eye1_gs, cmap='gray')
pc = [1812,1108]
ax.plot(pc[0],pc[1],'*r')
# plt.imshow(eye1)

# 1. Manually identify centre of pupil
# 2. code up method
# 3. Compare 'ground truth' with computed centre
# %%
# TESTING LOADING ANNOTAIONS
import tifffile
import numpy as np
from ijroipytiff.ij_roi import Roi
from ijroipytiff.ijpython_decoder import decode_ij_roi


data_path = 'D:\\Theia_data'
config_path = 'D:\\python_code\\anaylsis_scripts\\theia_pupil_localisation'

config = load_yaml(config_path, 'pupil_config.yml')


eye1_path = os.path.join(data_path, config['d2_w'], 'V003.tif')

tfile = tifffile.TiffFile(eye1_path)
img_shape = tfile.asarray().shape


for page in tfile.pages:
    for tag in page.tags:
        tag_name, tag_value = tag.name, tag.value
        print(tag_name)
        # print(tag_value)
        
# %%
print(tag_value['Overlays'][0][0:1].decode())
check_byte0 = int.from_bytes(tag_value['Overlays'][0][0:1], byteorder='big')
check_byte1 = int.from_bytes(tag_value['Overlays'][0][1:2], byteorder='big')
mutable_bytes = tag_value['Overlays'][0][0:4].decode('utf-8')
# %%
import imagej

# %%
# initialize ImageJ2
ij = imagej.init(mode='interactive')
print(f"ImageJ2 version: {ij.getVersion()}")
# %%
data_path = 'D:\\Theia_data'
config_path = 'D:\\python_code\\anaylsis_scripts\\theia_pupil_localisation'

config = load_yaml(config_path, 'pupil_config.yml')

eye1_path = os.path.join(data_path, config['d2_w'], 'V003.tif')
# %%
dataset = ij.io().open(eye1_path)
#%%
img = ij.IJ.openImage(eye1_path)
# %%
print(ij.IJ.run(img, 'List Elements', ''))
# %%
test = ij.IJ.run(img, 'List Elements', '')
ij.IJ.saveAs("Results", "D:/Theia_data/duke.app.box.comsiess8rryyn6oo5607aj83tl1hg3o49kk/White_Light/Overlay Elements of V003.csv")
# %%
test=(ij.ResultsTable)
# %%
ij.ui().show(dataset, cmap='gray')
imp1 = ij.py.active_imageplus()
# %%
test = img.getOverlay()
# %%
