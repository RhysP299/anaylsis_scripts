# -*- coding: utf-8 -*-
"""
Created on Tue Aug 24 09:52:24 2021
A file for all the utility and math classess
@author: RhysPoolman
"""
import numpy as np
import itertools as itt
import clr

clr.AddReference("EyeTech.Core")
from EyeTech import Core


class Point2f:
    def __init__(self, x, y):
        """
        The construct for a point in 2D space USING C# FLOATS.

        Parameters
        ----------
        x : float
            The x coordiante.
        y : float
            The y coordinate.

        Returns
        -------
        None.
        """
        self._object = Core.Primitives.Maths.Struct.Point2d(x, y)

    @classmethod
    def from_2_element_iterable(cls, point):
        """
        A classmethod constructor using a two element iterable.

        Parameters
        ----------
        point : 2 element iterable of floats
            The iterable containing the x and y coordiantes.

        Returns
        -------
        Point2f
            A instance of the Point2f class.
        """
        return cls(point[0], point[1])

    @property
    def x(self):
        """
        The x-coordinate of the point.

        Returns
        -------
        float
            The coordinate value.
        """
        return self._object.X

    @property
    def y(self):
        """
        The y-coordinate of the point.

        Returns
        -------
        float
            The coordinate value.
        """
        return self._object.Y


def image_array_2_ndarray_uint32(image_array):
    """
    Converts the C# ImageArray type to a numpy.ndarray of uint32.

    Parameters
    ----------
    image_array : ImageArray
        The input data.

    Returns
    -------
    data : 2d numpy.ndarray of uint32
        The output data.
    """
    height = image_array.height
    width = image_array.width
    data = np.zeros((height, width), dtype=np.uint32)
    for ii in range(width):
        for jj in range(height):
            data[ii, jj] = image_array.Image[ii][jj]
    return data


def array1d_2_ndarray(array):
    """
    Creates a 1d numpy.ndarray from another iterable of numbers.

    Parameters
    ----------
    array : iterable if numbers
        The input.

    Returns
    -------
    1d numpy.ndarray of floats
        The output.
    """
    return np.ndarray([value for value in array])


def mat_2_ndarray(mat):
    """
    Converts a C# OpenCV Mat object to a numpy.ndarray.

    Parameters
    ----------
    mat : C# OpenCV Mat object
        The input object.

    Returns
    -------
    ret_val : numpy.ndarray
        The output object.
    """
    mat_size = [range(size) for size in mat.size]
    prod = itt.product(*mat_size)
    ret_val = np.zeros(mat_size)
    for idx in prod:
        ret_val[idx] = mat.Data[idx]
    return ret_val


class Rect:
    def __init__(self, rect):
        """
        Stores the C# OpenCV Rect object as an instance attribute.

        Parameters
        ----------
        rect : C# OpenCV Rect object
            The object to be stored.

        Returns
        -------
        None.

        """
        self._x = rect.X
        self._y = rect.Y
        self._width = rect.Width
        self._height = rect.Height

    @property
    def x(self):
        """
        The x-coordinate of the upper-left corner of the rectangle.

        Returns
        -------
        integer
            The x-coordinate value.
        """
        return self._x

    @property
    def y(self):
        """
        The y-coordinate of the upper-left corner of the rectangle.

        Returns
        -------
        integer
            The y-coordinate value.
        """
        return self._y

    @property
    def width(self):
        """
        The width of the rectangle.

        Returns
        -------
        int
            The width value.
        """
        return self._width

    @property
    def height(self):
        """
        The height of the rectangle.

        Returns
        -------
        int
            The height value.
        """
        return self._height


class Point2d:
    def __init__(self, x, y):
        """
        The construct for a point in 2D space USING C# DOUBLES.

        Parameters
        ----------
        x : float
            The x coordiante.
        y : float
            The y coordinate.

        Returns
        -------
        None.
        """
        self._object = Core.Primitives.Maths.Struct.Point2d(x, y)

    @classmethod
    def from_2_element_iterable(cls, point):
        """
        A classmethod constructor using a two element iterable.

        Parameters
        ----------
        point : 2 element iterable of floats
            The iterable containing the x and y coordiantes.

        Returns
        -------
        Point2f
            A instance of the Point2f class.
        """
        return cls(point[0], point[1])

    @property
    def x(self):
        """
        The x-coordinate of the point.

        Returns
        -------
        float
            The coordinate value.
        """
        return self._object.X

    @property
    def y(self):
        """
        The y-coordinate of the point.

        Returns
        -------
        float
            The coordinate value.
        """
        return self._object.Y
