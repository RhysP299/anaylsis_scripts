# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 12:07:04 2021
The interface to the lens meter measurement controller.
@author: RhysPoolman
"""
from eMap_interface import options as opt
import clr

clr.AddReference("EyeTech.Device")
clr.AddReference("EyeTech.Recipes")
from EyeTech import Device
from EyeTech import Recipes


class DualIndustrialLensmeter:
    def __init__(self):
        self._object = \
            Device.Lensmeter.Hardware.Industrial.Dual.DualIndustrialLensmeter()

    @property
    def device_type(self):
        return self._object.DeviceType

def MeasureLensPrism():
    real_time_image_processor = \
        Recipes.Lensmeter.Processing.RealTime.Industrial.Dual.RealTimeImageProcessorDualIndustrialLensmeter
    return real_time_image_processor.MeasureLensPrism()
