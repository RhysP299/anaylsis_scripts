# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 13:49:10 2021
File contains an interface to the measurement settings.
@author: RhysPoolman
"""
import numpy as np
import clr
from enum import Enum
import eMap_interface.utilities as utl

clr.AddReference("EyeTech.Recipes")
clr.AddReference("EyeTech.Recipes.Lensmeter")
from EyeTech import Recipes
from EyeTech.Recipes import Lensmeter


class LensLocations(Enum):
    """
    Defines the lens locations.  Values taken from C# class.
    """
    # Was not set by caller (undefined location)
    undefined = Recipes.Lensmeter.Portable.Enums.LensLocations.Undefined,
    # Lens location is not known - not yet used but ensures consistency with
    # other enums that is using this Undefined and Unknown pattern.
    unknown = Recipes.Lensmeter.Portable.Enums.LensLocations.Unknown,
    # Oculus sinister is left Eye or left lens.
    oculus_sinister = \
        Recipes.Lensmeter.Portable.Enums.LensLocations.OculusSinister,
    # Oculus dexter is right Eye or right lens.
    oculus_dexter = Recipes.Lensmeter.Portable.Enums.LensLocations.OculusDexter


class LensType(Enum):
    """
    Possible different types of lenses e.g. Progressive, Bifocal etc.  Values
    taken from C# class.
    """
    # It is not set by caller. This can also be indicated as a failure for a
    # result e.g. unable to find reference points or detect correct lens type.
    undefined = Recipes.Lensmeter.Portable.Enums.LensType.Undefined,
    # Defines an unknown lenstype.
    unknown = Recipes.Lensmeter.Portable.Enums.LensType.Unknown,
    # Single vision lens.
    single = Recipes.Lensmeter.Portable.Enums.LensType.Single,
    # Progressive lens.
    progressive = Recipes.Lensmeter.Portable.Enums.LensType.Progressive,
    # Bifocal lens.
    bifocal = Recipes.Lensmeter.Portable.Enums.LensType.Bifocal,
    # degressive lens
    degressive = Recipes.Lensmeter.Portable.Enums.LensType.Degressive


class TintedType(Enum):
    """
    The tinted lens type identifiers.  Values taken from C# class.
    """
    # It is not set by caller.
    undefined = Recipes.Lensmeter.Portable.Enums.TintedType.Undefined,
    # Clear lens, no tint
    clear = Recipes.Lensmeter.Portable.Enums.TintedType.Clear,
    # Tinted lens
    tinted = Recipes.Lensmeter.Portable.Enums.TintedType.Tinted,


class LMSServiceVendors(Enum):
    """
    Which Vendors system is the device connecting to.
    """
    # An undefined system.
    undefined = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.Undefined
    # A FileShare based LMS system.
    anna_purna = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.AnnaPurna
    # A TCP/IP based LMS system.
    ocuco = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.Ocuco
    # A TCP/IP based LMS system.  This follows the same ISO standards as Ocuco.
    lens_ware = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.LensWare
    # A TCP/IP based LMS system.  This follows the same ISO standards as Ocuco.
    dvi = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.DVI
    # A Generic LMS Flag. NOT USED AS LMS DEVICE.  If set in portal eMap will
    # use LMS settings from "SystemSettings.ini" file
    generic_lms = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.GenericLMS
    # A FileShare based LMS system.
    iot = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.IOT
    # A TCP/IP based LMS system The camelCase is deliberate because it will
    # get displayed, e.g. LMSVendorType.ToString().  The camelCase comment
    # refers to C#
    opto_vision = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.optoVision
    # A FileShare based Generic LMS system. This follows the same ISO
    # standards as Ocuco.
    eyoto = \
        Recipes.Lensmeter.Portable.Enums.LMSServiceVendors.Eyoto


class ToleranceStandards(Enum):
    # Not used.
    undefined = Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.Undefined,
    # ISO 21987.
    iso_21987 = Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ISO_21987,
    # ISO 21987 custom (user entered) values.
    iso_21987_Custom = \
        Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ISO_21987_Custom,
    # ISO 8980.
    iso_8980 = Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ISO_8980,
    # ISO 8980 custom (user entered) values.
    iso_8980_custom = \
        Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ISO_8980_Custom,
    # US ANSI standard.
    ansi = Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ANSI,
    # US ANSI standard - custom (user entered) values.
    ansi_custom = \
        Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ANSI_Custom,
    # US ANSI standard - uncut
    ansi_uncut = \
        Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.ANSI_Uncut,
    # New Jersey standard - custom (user entered) values.
    new_jersey_custom = \
        Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.NewJersey_Custom,
    # New Jersey standard - uncut
    new_jersey_uncut = \
        Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.NewJersey_Uncut,
    # New Jersey standard
    new_jersey = Recipes.Lensmeter.Imaging.Enums.ToleranceStandards.NewJersey


class CylindricalNotations(Enum):
    undefined = 0,
    # Uses the negative notation, default. This is the new format.
    negative = 1,
    # Uses the positive notation. This is the old skool format.
    positive = 2


class PrescriptionToleranceContainer:
    def __init__(self, tolerance_standards):
        """
        The constructor for PrescriptionToleranceContainer object.  Note not
        all property and methods of the C# class have been exposed in this
        interface.

        Parameters
        ----------
        tolerance_standards : EyeTech.Recipes.Lensmeter.Imaging.Enums.
                ToleranceStandards
            The type of tolerance that is being worked to.

        Returns
        -------
        None.
        """
        tols = Recipes.Lensmeter.Imaging.Measurements.Tolerances
        self._object = tols.PrescriptionToleranceContainer(tolerance_standards)

    @property
    def add_format_steps(self):
        """
        Get addition prescription type format steps.

        Returns
        -------
        float
            Format steps.
        """
        return self._object.AddFormatSteps

    @property
    def cylindircal_format_steps(self):
        """
        Get cylindrical prescription type format steps.

        Returns
        -------
        float
            Format steps.
        """
        return self._object.CylindricalFormatSteps

    @property
    def sphere_format_steps(self):
        """
        Get sphere prescription type format steps.

        Returns
        -------
        float
            Format steps.
        """
        return self._object.SphereFormatSteps

    @property
    def spherical_equivalent_format_steps(self):
        """
        Get spherical equivalent prescription type format steps.

        Returns
        -------
        float
            Format steps.
        """
        return self._object.SphericalEquivalentFormatSteps

    @property
    def prism_format_steps(self):
        """
        Get prism prescription type format steps.

        Returns
        -------
        float
            Format steps.
        """
        return self._object.PrismFormatSteps

    @property
    def current_tolerance_standard(self):
        """
        The tolerance standard currently being used.

        Returns
        -------
        ToleranceStandards
            The tolerance currently set.
        """
        return self._object.CurrentToleranceStandard


class PrescriptionMetadata:
    def __init__(self, is_entered_prescription_compensated=False,
                 prescription_tolerance_container=None,
                 prescription_sphere=np.nan, prescription_cylinder=np.nan,
                 prescription_add=np.nan):
        """
        The cosntructor for PrescriptionMetadata object.

        Parameters
        ----------
        is_entered_prescription_compensated : boolean, optional
            Denotes whether this metadata is fully in the context of
            compensated or nominal. The default is False.
        prescription_tolerance_container : EyeTech.Recipes.Lensmeter.Imaging.
                Measurements.Tolerances.PrescriptionToleranceContainer,
                optional
            The prescription tolerance container. The default is None.
        prescription_sphere : float, optional
            The sphere value of Rx. If is_entered_prescription_compensated is
            true then this is compensated. Otherwise, it is nominal.  The
            default is np.nan.
        prescription_cylinder : float, optional
            The cylindrical value of Rx. If is_entered_prescription_compensated
            is true then this is compensated. Otherwise, it is nominal.  The
            default is np.nan.
        prescription_add : float, optional
            The Add value of Rx. If is_entered_prescription_compensated is true
            then this is compensated. Otherwise, it is nominal.  This property
            is applicable for both ADD and DEG.  The default is np.nan.

        Returns
        -------
        None.
        """
        meas_specs = \
            Recipes.Lensmeter.Imaging.Measurements.MeasurementSpecifications
        self._object = \
            meas_specs.PrescriptionMetadata(is_entered_prescription_compensated,
                                            prescription_tolerance_container,
                                            prescription_sphere,
                                            prescription_cylinder,
                                            prescription_add=np.nan)

    @property
    def is_distance_rx_valid(self):
        """
        Denotes whether this metadata is valid. An invalid metadata cannot
        help with making a better DISTANT Rx.

        Returns
        -------
        boolean
            The flag value.
        """
        return self._object.IsDistantRxValid

    @property
    def is_add_rx_valid(self):
        """
        Denotes whether this metadata is valid. An invalid metadata cannot
        help with making a better ADD Rx.

        Returns
        -------
        boolean
            The flag value.
        """
        return self._object.IsAddRxValid

    @property
    def is_entered_prescription_compensated(self):
        """
        Denotes whether this metadata is fully in the context of compensated
        or nominal.

        Returns
        -------
        boolean
            The flag value.
        """
        return self._object.IsEnteredPrescriptionCompensated

    @property
    def passprescription_tolerance_container(self):
        """
        The prescription tolerance container.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Imaging. Measurements.Tolerances.
                PrescriptionToleranceContainer
            An object with the tolerance values.
        """
        return self._object.PrescriptionToleranceContainer

    @property
    def prescription_sphere(self):
        """
        The sphere value of Rx. If is_entered_prescription_compensated is true
        then this is compensated. Otherwise, it is nominal.

        Returns
        -------
        float
            The prescription sphere value.
        """
        return self._object.PrescriptionSphere

    @property
    def prescription_cylinder(self):
        """
        The cylindrical value of Rx. If is_entered_prescription_compensated is
        true then this is compensated. Otherwise, it is nominal.

        Returns
        -------
        float
            The prescription cylinder value.
        """
        return self._object.PrescriptionCylinder

    @property
    def prescription_add(self):
        """
        The add value of Rx. If is_entered_prescription_compensated is
        true then this is compensated. Otherwise, it is nominal.

        Returns
        -------
        float
            The prescription add value.
        """
        return self._object.PrescriptionAdd


class IndustrialLensmeterLensMeasurementSpecification:
    def __init__(self, lens_location, lens_type_hint_cam_1,
                 lens_type_hint_cam_2, lens_tint_cam1, lens_tint_cam2,
                 pd_cam1, pd_cam2, is_cam1_pd_default, is_cam2_pd_default,
                 lens_base_curve_cam1, lens_base_curve_cam2,
                 lens_center_thickness_cam1, lens_center_thickness_cam2,
                 lens_refractive_index_cam1, lens_refractive_index_cam2,
                 drp_height_cam1, drp_height_cam2, near_pd_cam1, near_pd_cam2,
                 distant_tuning, add_tuning, prescription_metadata_cam1,
                 prescription_metadata_cam2,
                 raw_lms_comparative_prescription_file_contents,
                 lms_service_vendor):
        """
        The constructor for a measurement specification object

        Parameters
        ----------
        lens_location : EyeTech.Recipes.Lensmeter.Portable.Enums.LensLocations
            An enum defined in the eMap C# software.  Spoecifies which lens is
            being measured.
        lens_type_hint_cam_1 : EyeTech.Recipes.Lensmeter.Portable.Enums.
                LensType
            Specifies a broad class for the kind of lens under investigation in
            camera 1 i.e. bifocal, progessive, etc.
        lens_type_hint_cam_2 : EyeTech.Recipes.Lensmeter.Portable.Enums.
                LensType
            Specifies a broad class for the kind of lens under investigation in
            camera 2 i.e. bifocal, progessive, etc.
        lens_tint_cam1 : EyeTech.Recipes.Lensmeter.Portable.Enums.TintedType
            Is the lens clear or tinted or unknown.
        lens_tint_cam2 : EyeTech.Recipes.Lensmeter.Portable.Enums.TintedType
            Is the lens clear or tinted or unknown.
        pd_cam1 : float
            The monocular pupillary distance in mm for the Cam1 measurement.
        pd_cam2 : float
            The monocular pupillary distance in mm for the Cam2 measurement.
        is_cam1_pd_default : boolean
            A flag indicating if the PD on Cam1 is the default value.  I don't
            know what the actual value is.
        is_cam2_pd_default : boolean
            A flag indicating if the PD on Cam2 is the default value.  I don't
            know what the actual value is yet.
        lens_base_curve_cam1 : float
            The base curvature of the left lens (Cam1) in diopters.
            If not applicable, use NaN (and we will try to approximate this
            from the measurement).  This is used to perform effective power to
            back power conversion.
        lens_base_curve_cam2 : float
            The base curvature of the right lens (Cam2) in diopters.
            If not applicable, use NaN (and we will try to approximate this
            from the measurement).  This is used to perform effective power to
            back power conversion.
        lens_center_thickness_cam1 : float
            The center thickness of a lens in mm. If not applicable, use NaN
            (and we will try to approximate this from the measurement).  This
            is used to perform effective power to back power conversion.
        lens_center_thickness_cam2 : float
            The center thickness of a lens in mm. If not applicable, use NaN
            (and we will try to approximate this from the measurement).  This
            is used to perform effective power to back power conversion.
        lens_refractive_index_cam1 : float
            The lens refractive index. If not applicable, use NaN (and we will
            use default value as approximation when performing conversion
            from effective power to back power).
        lens_refractive_index_cam2 : float
            The lens refractive index. If not applicable, use NaN (and we will
            use default value as approximation when performing conversion
            from effective power to back power).
        drp_height_cam1 : float
            DRP height in MM left lens.
        drp_height_cam2 : float
            DRP height in MM right lens.
        near_pd_cam1 : float
            Near PD in MM left lens.  I don't know what the near signifies yet.
        near_pd_cam2 : float
            Near PD in MM right lens.  I don't know what the near signifies
            yet.
        distant_tuning : int
            The percentage of the distance tuning.  I don't understand this
            yet.
        add_tuning : int
            The percentage of the add tuning.  I don't understand this
            yet.
        prescription_metadata_cam1 : EyeTech.Recipes.Lensmeter.Imaging.
                Measurements.MeasureentSpecifications.PrescriptionMetadata
            Prescription metadata for Cam1 measurement, to support LMS if
            applicable.
        prescription_metadata_cam2 : EyeTech.Recipes.Lensmeter.Imaging.
                Measurements.MeasureentSpecifications.PrescriptionMetadata
            Prescription metadata for Cam2 measurement, to support LMS if
            applicable.
        raw_lms_comparative_prescription_file_contents : string
            Data from LMS shared network file associated with entered batch ID.
        lms_service_vendor : EyeTech.Recipes.Lensmeter.Portable.Enums.
                LMSServiceVendors
            The LMS vendor for parsing
            RawLMSComparativePrescriptionFileContents.

        Returns
        -------
        None.
        """
        meas_specs = \
            Recipes.Lensmeter.Imaging.Measurements.MeasurementSpecifications
        self._object = \
            meas_specs.IndustrialLensmeterLensMeasurementSpecification(
                lens_location, lens_type_hint_cam_1, lens_type_hint_cam_2,
                lens_tint_cam1, lens_tint_cam2, pd_cam1, pd_cam2,
                is_cam1_pd_default, is_cam2_pd_default, lens_base_curve_cam1,
                lens_base_curve_cam2, lens_center_thickness_cam1,
                lens_center_thickness_cam2, lens_refractive_index_cam1,
                lens_refractive_index_cam2, drp_height_cam1, drp_height_cam2,
                near_pd_cam1, near_pd_cam2, distant_tuning, add_tuning,
                prescription_metadata_cam1._object,
                prescription_metadata_cam2._object,
                raw_lms_comparative_prescription_file_contents,
                lms_service_vendor)

    @property
    def lens_location(self):
        """
        Spoecifies which lens is being measured.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums.LensLocations
            An enum defined in the eMap C# software.
        """
        return self._object.LensLocation

    @property
    def lens_type_hint_cam_1(self):
        """
        Specifies a broad class for the kind of lens under investigation in
        camera 2 i.e. bifocal, progessive, etc.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums. LensType
           An enum defined in the eMap C# software.
        """
        return self._object.LensTypeHintCam1

    @property
    def lens_type_hint_cam_2(self):
        """
        Specifies a broad class for the kind of lens under investigation in
        camera 2 i.e. bifocal, progessive, etc.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums. LensType
           An enum defined in the eMap C# software.
        """
        return self._object.LensTypeHintCam2

    @property
    def lens_tint_cam1(self):
        """
        Is the lens clear or tinted or unknown.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums.TintedType
            An enum defined in the eMap C# software.
        """
        return self._object.LensTintCam1

    @property
    def lens_tint_cam2(self):
        """
        Is the lens clear or tinted or unknown.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums.TintedType
            An enum defined in the eMap C# software.
        """
        return self._object.LensTintCam2

    @property
    def pd(self):
        """
        The binocular pupillary distance in mm. E.g. 63.0 for the average
        binocular PD.

        Returns
        -------
        float
            The PD value.  Note that in the C# code this does not appear to be
            referenced by or reference anything.
        """
        return self._object.PD

    @property
    def pd_cam1(self):
        """
        The monocular pupillary distance in mm for the Cam1 measurement.

        Returns
        -------
        float
            The monocular pupillary distance value.
        """
        return self._object.PDCam1

    @property
    def pd_cam2(self):
        """
        The monocular pupillary distance in mm for the Cam2 measurement.

        Returns
        -------
        float
            The monocular pupillary distance value.
        """
        return self._object.PDCam2

    @property
    def is_cam1_pd_default(self):
        """
        A flag indicating if the PD on Cam2 is the default value.  I don't
        know what the actual value is yet.

        Returns
        -------
        boolean
            A falg indicating id the pupillary distance is the default value.
        """
        return self._object.IsCam1PdDefault

    @property
    def is_cam2_pd_default(self):
        """
        A flag indicating if the PD on Cam2 is the default value.  I don't
        know what the actual value is yet.

        Returns
        -------
        boolean
            A falg indicating id the pupillary distance is the default value.
        """
        return self._object.IsCam2PdDefault

    @property
    def lens_base_curve_cam1(self):
        """
        The base curvature of the left lens (Cam1) in diopters. If not
        applicable, use NaN (and we will try to approximate this from the
        measurement).  This is used to perform effective power to back power
        conversion.

        Returns
        -------
        float
            float representing the base curavature of the left lens.
        """
        return self._object.LensBaseCurveCam1

    @property
    def lens_base_curve_cam2(self):
        """
        The base curvature of the right lens (Cam2) in diopters. If not
        applicable, use NaN (and we will try to approximate this from the
        measurement).  This is used to perform effective power to back power
        conversion.

        Returns
        -------
        float
            float representing the base curavature of the right lens.
        """
        return self._object.LensBaseCurveCam2

    @property
    def lens_center_thickness_cam1(self):
        """
        The center thickness of a lens in mm. If not applicable, use NaN (and
        we will try to approximate this from the measurement).  This is used
        to perform effective power to back power conversion.

        Returns
        -------
        float
            The center thickness.
        """
        return self._object.LensCenterThicknessCam1

    @property
    def lens_center_thickness_cam2(self):
        """
        The center thickness of a lens in mm. If not applicable, use NaN (and
        we will try to approximate this from the measurement).  This is used
        to perform effective power to back power conversion.

        Returns
        -------
        float
            The center thickness.
        """
        return self._object.LensCenterThicknessCam2

    @property
    def lens_refractive_index_cam1(self):
        """
        The lens refractive index. If not applicable, use NaN (and we will use
        default value as approximation when performing conversion from
        effective power to back power).

        Returns
        -------
        float
            The index.
        """
        return self._object.RefractiveIndexLensCam1

    @property
    def lens_refractive_index_cam2(self):
        """
        The lens refractive index. If not applicable, use NaN (and we will use
        default value as approximation when performing conversion from
        effective power to back power).

        Returns
        -------
        float
            The index.
        """
        return self._object.RefractiveIndexLensCam2

    @property
    def distance_reference_point_height_cam1(self):
        """
        The left distance reference point height.

        Returns
        -------
        float
        DRP height in MM left lens.
        """
        return self._object.DistanceReferencePointHeightCam1

    @property
    def distance_reference_point_height_cam2(self):
        """
        The right distance reference point height.

        Returns
        -------
        float
        DRP height in MM right lens.
        """
        return self._object.DistanceReferencePointHeightCam2

    @property
    def near_pd_cam1(self):
        """
         Near PD in MM left lens.  I don't know what the near signifies yet.

        Returns
        -------
        float
            The left pupillary distance.
        """
        return self._object.NearPdCam1

    @property
    def near_pd_cam2(self):
        """
         Near PD in MM right lens.  I don't know what the near signifies yet.

        Returns
        -------
        float
            The pupillary distance.
        """
        return self._object.NearPdCam2

    @property
    def distant_tuning(self):
        """
        The percentage of the distant tuning.  I don't understand this yet.

        Returns
        -------
        interger
            An integer no greater than 100
        """
        return self._object.DistantTuning

    @property
    def add_tuning(self):
        """
        The percentage of the add tuning.  I don't understand this yet.

        Returns
        -------
        interger
            An integer no greater than 100
        """
        return self._object.AddTuning

    @property
    def prescription_metadata_cam1(self):
        """
        Prescription metadata for Cam1 measurement, to support LMS if
        applicable.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Imaging. Measurements.
                MeasureentSpecifications.PrescriptionMetadata
            An enum defined in the eMap C# software.
        """
        return self._object.PrescriptionMetadataCam1

    @property
    def prescription_metadata_cam2(self):
        """
        Prescription metadata for Cam2 measurement, to support LMS if
        applicable.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Imaging. Measurements.
                MeasureentSpecifications.PrescriptionMetadata
            An enum defined in the eMap C# software.
        """
        return self._object.PrescriptionMetadataCam2

    @property
    def raw_lms_comparative_prescription_file_contents(self):
        """
        Data from LMS shared network file associated with entered batch ID.

        Returns
        -------
        String
            File contents.
        """
        return self._object.RawLMSComparativePrescriptionFileContents

    @property
    def lms_service_vendor(self):
        """
        The LMS vendor for parsing RawLMSComparativePrescriptionFileContents.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums.LMSServiceVendors
            An enum defined in the eMap C# software.
        """
        return self._object.LMSServiceVendor

    @property
    def refractive_index_air(self):
        """
        The refractive index of air.

        Returns
        -------
        float
            Default 1.0003.
        """
        return self._object.RefractiveIndexAir


class LensPrismMeasurementArgs:
    def __init__(self, lens_location, lens_type,
                 pixel_interval_between_search_lines,
                 search_lines_bounding_box_margin_percent,
                 index_line_smoothing_kernel_size, index_line_smoothing_sigma,
                 index_line_threshold_tolerance, index_line_erosion_iterations,
                 all_lines_smoothing_kernel_size,
                 all_lines_threshold_tolerance,
                 all_lines_erosion_iterations,
                 scaled_prism_reference_point_from_lms,
                 scaled_lens_distance_reference_point,
                 scaled_lens_near_reference_point,
                 image_scaler_reduction_factor,
                 prism_reference_point_offset_from_distance_reference_point_mm,
                 near_reference_point_inset_mm, camera_sensor_mm_to_pixels,
                 camera_lens_magnification):
        """
        The constructor for a C#
        EyeTech.Recipes.Lensmeter.Imaging.Measurements.LensPrismMeasurementArgs
        object.

        Parameters
        ----------
        lens_location : LensLocations
            Is it a left lens or a right lens?  Use this to choose the correct
            candidate for the Prism Reference Point for Progressive lens type.
        lens_type : LensType
            The position of the Prism Reference Point changes when the lens
            type changes.
        pixel_interval_between_search_lines : bool
            The gap in pixels between the coordinates of each calculated
            search line.
        search_lines_bounding_box_margin_percent : integer
            Defines an additional margin inside the area of the detected lens
            region.  The search lines are generated within this smaller region.
        index_line_smoothing_kernel_size : integer
            The kernel size to apply when smoothing the image before processing
            to extract the grey index line.
        index_line_smoothing_sigma : float
            The sigma to apply when smoothing the image before processing to
            extract the grey index line.
        index_line_threshold_tolerance : integer
            The image thresholding tolerance value to use to extract the grey
            index line.
        index_line_erosion_iterations : float
            The number of times to erode to clean up the grey index line after
            extraction.
        all_lines_smoothing_kernel_size : integer
            The kernel size to apply when smoothing the image before processing
            to extract all lines.
        all_lines_threshold_tolerance : integer
            The image thresholding tolerance value to use to extract all lines.
        all_lines_erosion_iterations : integer
            The number of times to erode to clean up the image after extracting
            all lines.
        scaled_prism_reference_point_from_lms : integer
            The order of the polynomial for curve fitting all pixel shift point
            coordinates and magnitudes.
        scaled_lens_distance_reference_point : 2 float numpy.ndarray
            A scaled verions of the lens distance reference point.  Scale is
            removed by multiplication with image_scaler_reduction_factor.
        scaled_lens_near_reference_point : 2 float numpy.ndarray
            A scaled verions of the lens near reference point.  Scale is
            removed by multiplication with image_scaler_reduction_factor.
        image_scaler_reduction_factor : integer
            The factor that removes the scaling from the
            scaled_lens_distance_reference_point and
            scaled_lens_near_reference_point.
        prism_reference_point_offset_from_distance_reference_point_mm : float
            The Prism Reference Point is offset from the lens Distance
            Reference Point by a manufacturer-defined number of millimeters.
        near_reference_point_inset_mm : float
            By definition, the Near Reference Point is not on the same axis as
            the Distance Reference Point It is "inset" ( horizontally offset )
            from the Distance Reference Point by a manufacturer-defined number
            of millimeters.
        camera_sensor_mm_to_pixels : float
            The pixel density of the camera's sensor.  How many mm is a pixel
            on the sensor. E.g. IDS UI-3884LE-M-GL is 2.4um/px which is
            0.0024mm/px which has the dimension mm/px.  Stored to perform MM ->
            Pixels conversion and Pixels -> MM conversion
        camera_lens_magnification : float
            The magnification of the camera's lens. E.g. TC13048 has 0.098.
            Stored to perform MM -> Pixels conversion and Pixels ->
            MM conversion.

        Returns
        -------
        None.
        """
        scaled_prism_reference_point_from_lms_2point = \
            utl.Point2f.from_2_element_iterable(scaled_prism_reference_point_from_lms)
        scaled_lens_distance_reference_point_2point =  \
            utl.Point2f.from_2_element_iterable(scaled_lens_distance_reference_point)
        scaled_lens_near_reference_point_2point = \
            utl.Point2f.from_2_element_iterable(scaled_lens_near_reference_point)
        meas = Recipes.Lensmeter.Imaging.Measurements
        self._object = \
            meas.LensPrismMeasurementArgs(lens_location, lens_type,
                                          pixel_interval_between_search_lines,
                                          search_lines_bounding_box_margin_percent,
                                          index_line_smoothing_kernel_size,
                                          index_line_smoothing_sigma,
                                          index_line_threshold_tolerance,
                                          index_line_erosion_iterations,
                                          all_lines_smoothing_kernel_size,
                                          all_lines_threshold_tolerance,
                                          all_lines_erosion_iterations,
                                          scaled_prism_reference_point_from_lms_2point,
                                          scaled_lens_distance_reference_point_2point,
                                          scaled_lens_near_reference_point_2point,
                                          image_scaler_reduction_factor,
                                          prism_reference_point_offset_from_distance_reference_point_mm,
                                          near_reference_point_inset_mm,
                                          camera_sensor_mm_to_pixels,
                                          camera_lens_magnification)

    @property
    def lens_location(self):
        """
        Is it a left lens or a right lens?  Use this to choose the correct
        candidate for the Prism Reference Point for Progressive lens type.

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums.LensLocations
            An enum defined in the eMap C# software.
        """
        return self._object.LensLocation

    @property
    def lens_type(self):
        """
        The position of the Prism Reference Point changes when the lens type
        changes

        Returns
        -------
        EyeTech.Recipes.Lensmeter.Portable.Enums. LensType
           An enum defined in the eMap C# software.
        """
        return self._object.LensType

    @property
    def can_calculate_focal_length(self):
        """
        Flag for whether we were able to calculate the focal length of the
        lens.  This indicates the "Confidence Level" that we have in the
        ( x, y ) coordinate position of the point of least prism diopters
        change.  If we cannot calculate lens focal length, then we cannot
        confidently use the ( x, y ) position of the point of least prism
        diopters change to drive prescription measurement logic.

        Returns
        -------
        boolean
            The flag value.
        """
        return self._object.CanCalculateLensFocalLength

    @property
    def image_scaler_reduction_factor(self):
        """
        The image scaler reduction factor to be applied to the Prism Reference
        Point to scale it for display.

        Returns
        -------
        integer
            The factor value
        """
        return self._object.ImageScalerReductionFactor

    @property
    def pixel_interval_between_search_lines(self):
        """
        The gap in pixels between the coordinates of each calculated search
        line.

        Returns
        -------
        integer
            The interval value.
        """
        return self._object.PixelIntervalBetweenSearchLines

    @property
    def search_line_bounding_box_margin_percent(self):
        """
        Defines an additional margin inside the area of the detected lens
        region.  The search lines are generated within this smaller region.

        Returns
        -------
        float
            Margin as a percentage of the total.
        """
        return self._object.SearchLinesBoundingBoxMarginPercent

    @property
    def index_line_smoothing_kernel_size(self):
        """
        The kernel size to apply when smoothing the image before processing to
        extract the grey index line.

        Returns
        -------
        integer
            Kernal size value.
        """
        return self._object.IndexLineSmoothingKernelSize

    @property
    def index_line_smoothing_sigma(self):
        """
        The sigma to apply when smoothing the image before processing to
        extract the grey index line.

        Returns
        -------
        float
            The sigma size value.
        """
        return self._object.IndexLineSmoothingSigma

    @property
    def index_line_threshold_tolerance(self):
        """
        The image thresholding tolerance value to use to extract the grey
        index line.

        Returns
        -------
        integer
            The tolerance value.
        """
        return self._object.IndexLineThresholdTolerance

    @property
    def index_line_erosion_iterations(self):
        """
        The number of times to erode to clean up the grey index line after
        extraction.

        Returns
        -------
        integer
            The value of the number of erodtion iteration times.
        """
        return self._object.IndexLineErosionIterations

    @property
    def all_lines_smoothing_kernel_size(self):
        """
        The kernel size to apply when smoothing the image before processing to
        extract all lines.

        Returns
        -------
        integer
            The size of the pre-processing kernal.
        """
        return self._object.AllLinesSmoothingKernelSize

    @property
    def all_lines_threshold_tolerance(self):
        """
        The image thresholding tolerance value to use to extract all lines.

        Returns
        -------
        integer
            The thresholding tolerance value.
        """
        return self._object.AllLinesThresholdTolerance

    @property
    def all_lines_erosion_iterations(self):
        """
        The number of times to erode to clean up the image after extracting all
        lines.

        Returns
        -------
        integer
            The number of times.
        """
        return self._object.AllLinesErosionIterations

    @property
    def prism_shift_curve_fitting_polynomial_order(self):
        """
        The order of the polynomial for curve fitting all pixel shift point
        coordinates and magnitudes.

        Return
        -------
        integer
            The polynomial order.
        """
        return self._object.PrismShiftCurveFittingPolynomialOrder

    @property
    def lens_distance_reference_point(self):
        """
        The measured Distance Reference Point from the lens, used to calculate
        the Prism Reference Point.

        Returns
        -------
        2 element numpy.ndarray of floats
            The lens distance reference point value.
        """
        return self._object.LensDistanceReferencePoint

    @property
    def lens_near_reference_point(self):
        """
        The measured Near Reference Point from the lens, used to calculate the
        Prism Reference Point.

        Returns
        -------
        2 element numpy.ndarray of floats
            The lens near reference point value.
        """
        return self._object.LensNearReferencePoint

    def lens_prism_reference_point(self):
        """
        The calculated Prism Reference Point from the lens.

        Returns
        -------
        2 element numpy.ndarray of floats
            The lens prism reference point value.
        """
        return self._object.LensPrismReferencePoint

    def lens_prism_reference_point_from_lms(self):
        """
        The prism reference point (PRP) from LMS where prism measurement is
        taken. If NaN then the fallback method (offset downwards by DRP) is
        used.

        Returns
        -------
        2 element numpy.ndarray of floats
            The lens prism reference point from lms value.
        """
        return self._object.LensPrismReferencePoint

    @property
    def prism_reference_point_offset_from_distance_reference_point_mm(self):
        """
        The Prism Reference Point is offset from the lens Distance Reference
        Point by a manufacturer-defined number of millimeters.

        Returns
        -------
        float
            The PRP/LDR offset value.
        """
        return self._object.PrismReferencePointOffsetFromDistanceReferencePointMM

    @property
    def near_reference_point_inset_mm(self):
        """
         By definition, the Near Reference Point is not on the same axis as the
         Distance Reference Point.  It is "inset" ( horizontally offset ) from
         the Distance Reference Point by a manufacturer-defined number of
         millimeters.

        Returns
        -------
        float
            The offset in mm.
        """
        return self._object.NearReferencePointInsetMM

    @property
    def distance_between_measuring_lens_and_screen_mm(self):
        """
         The distance in millimeters between the lens we are measuring and the
         physical hardware device's screen.  Required to convert prism pixel
         shift magnitudes into prism diopters.  In the technical document this
         is called "Object Distance".

        Returns
        -------
        float
            The object distance value.
        """
        return self._object.DistanceBetweenMeasuringLensAndScreenMM

    @property
    def lens_power_at_prism_reference_point(self):
        """
        The spherical equivalent power of the lens at LensPrismReferencePoint.
        It is used to calculate DistanceBetweenMeasuringLensAndScreenMM.

        Returns
        -------
        float
            The power value.
        """
        return self._object.LensPowerAtPrismReferencePoint

    @property
    def lens_magnification_at_prism_reference_point(self):
        """
        The magnification of the lens at LensPrismReferencePoint is used to
        calculate DistanceBetweenMeasuringLensAndScreenMM.

        Returns
        -------
        float
            The lens magnification.
        """
        return self._object.LensMagnificationAtPrismReferencePoint

    @property
    def camera_sensor_mm_to_pixels(self):
        """
        The pixel density of the camera's sensor:
            How many mm is a pixel on the sensor. E.g. IDS UI-3884LE-M-GL is
            2.4um/px which is 0.0024mm/px which has the dimension mm/px.
        Stored to perform MM -> Pixels conversion and Pixels -> MM conversion.

        Returns
        -------
        float
            The pixel density value.
        """
        return self._object.CameraSensorMMToPixels

    @property
    def camera_lens_magnification(self):
        """
        The magnification of the camera's lens. E.g. TC13048 has 0.098.
        Stored to perform MM -> Pixels conversion and Pixels -> MM conversion.

        Returns
        -------
        float
            The camera lens magnification value.
        """
        return self._object.CameraLensMagnification


class LensPowerDetectionArgs:
    def __init__(self, min_threshold, max_threshold, min_pixels_area,
                 max_pixels_area, min_elongation_ratio, max_elongation_ratio,
                 adjusted_margin_offset,
                 edge_dots_hexagonal_max_distance_threshold,
                 max_allowed_ellipse_radius_value,
                 min_dots_circularity_threshold, calibration_model,
                 lens_power_maths_args):
        self._object = \
            Lensmeter.Imaging.Detection.LensPowerDetectionArgs(min_threshold,
                                                               max_threshold,
                                                               min_pixels_area,
                                                               max_pixels_area,
                                                               min_elongation_ratio,
                                                               max_elongation_ratio,
                                                               adjusted_margin_offset,
                                                               edge_dots_hexagonal_max_distance_threshold,
                                                               max_allowed_ellipse_radius_value,
                                                               min_dots_circularity_threshold, calibration_model,
                                                               lens_power_maths_args)

    @property
    def threshold_min_value(self):
        """
        The lower bound for the thresholding range defined as [min, max].

        Returns
        -------
        int
           The lower bound value
        """
        return self._object.ThresholdMinValue

    @property
    def threshold_max_value(self):
        """
        The upper bound for the thresholding range defined as [min, max].

        Returns
        -------
        int
           The upper bound value
        """
        return self._object.ThresholdMaxValue

    @property
    def pixels_area_min_value(self):
        """
        The lower bound for the pixels area defined as [min, max].

        Returns
        -------
        int
            The lower bound value.
        """
        return self._object.PixelsAreaMinValue

    @property
    def pixels_area_max_value(self):
        """
        The upper bound for the pixels area defined as [min, max].

        Returns
        -------
        int
            The upper bound value.
        """
        return self._object.PixelsAreaMaxValue

    @property
    def elongation_ratio_min_value(self):
        """
        The lower bound for the elongation ratio defined as [min, max].

        Returns
        -------
        float
            The lower bound value.
        """
        return self._object.ElongationRatioMinValue

    @property
    def elongation_ratio_max_value(self):
        """
        The upper bound for the elongation ratio defined as [min, max].

        Returns
        -------
        float
            The upper bound value.
        """
        return self._object.ElongationRatioMaxValue

    @property
    def adjusted_margin_offset_left(self):
        """
        The left offset to help with adjusting the image rect so that detected
        particles outside of this rect will be ignored.

        Returns
        -------
        int
            The offset value
        """
        return self._object.AdjustedMarginOffsetLeft

    @property
    def adjusted_margin_offset_top(self):
        """
        The top offset to help with adjusting the image rect so that detected
        particles outside of this rect will be ignored.

        Returns
        -------
        int
            The offset value
        """
        return self._object.AdjustedMarginOffsetTop

    @property
    def adjusted_margin_offset_right(self):
        """
        The right offset to help with adjusting the image rect so that detected
        particles outside of this rect will be ignored.

        Returns
        -------
        int
            The offset value
        """
        return self._object.AdjustedMarginOffsetRight

    @property
    def adjusted_margin_offset_bottom(self):
        """
        The bottom offset to help with adjusting the image rect so that
        detected particles outside of this rect will be ignored.

        Returns
        -------
        int
            The offset value
        """
        return self._object.AdjustedMarginOffsetBottom

    @property
    def edge_dots_hexagonal_max_distance_threshold(self):
        """
        The max limit value that if an edge/bordering dot (hexagonal pattern)
        is located during image processing, that dot along with its
        neighbouring hexagonal vertices are rejected.  This value (ratio) is
        the max value allowed based on averaging distances in a potential
        hexagonal candidate ellipse. It means the grouping of hexagonal dots in
        this candidate is nonsense.  A group is allowed up to or equals to this
        value.

        Returns
        -------
        float
            The max value.
        """
        return self._object.EdgeDotsHexagonalMaxDistanceThreshold

    @property
    def max_allowed_ellipse_radius_value(self):
        """
        The max allowed value for the radius of the ellipse. If this value is
        used (i.e. non-negative values) then if any curve fit that leads to
        pixels radii being same or greater than this value then the ellipse is
        considered to be invalid.

        Returns
        -------
        float
            The max value.
        """
        return self._object.MaxAllowedEllipseRadiusValue

    @property
    def min_max_ellipse_radii_ratio_threshold(self):
        """
        Constraint to check whether there is weird radii ratio as a result from
        fitting a set of points forming on an ellipse.  The
        MaxAllowedEllipseRadiusValue helps to get rid of bad fitting e.g. 44px
        and 983px where its min/max ratio is near to 0.  However, this provides
        a dynamic value to help reject nonsensical fitting since
        MaxAllowedEllipseRadiusValue is arbitrarily and probably depends on
        patterns on screen and lens used.

        Returns
        -------
        float
            The value and the constaint.
        """
        return self._object.MinMaxEllipseRadiiRatioThreshold

    @property
    def min_dots_circularity_threshold(self):
        """
        Constraint to check dots (forming an ellipse) is circular (or oval)
        enough and not a weird shape due to it being abruptedly cut-off, etc.
        Value of 0.75 seems like a good number where bad dots are about 0.5,
        0.71 and most good dots are about 0.8 and above. However, there can be
        bad dots that can be above 0.83 e.g. similar to a waxing gibbous moon
        where its ratio is about 0.833 or a cut off 3/4 dot with 0.819. Also
        there can be good dots with 0.767.

        Returns
        -------
        float
            Value of the circularity threshold.
        """
        return self._object.MinDotsCircularityThreshold

    @property
    def calibration_model(self):
        """
        The calibration model to be used with LensPowerMathsArgs to determine
        the diopters.

        Returns
        -------
        LensmeterDiopterCalibrationModel
            The calibration model used.
        """
        return self._object.CalibrationModel

    @property
    def lens_power_maths_args(self):
        """
        The calibration model to be used with LensPowerMathsArgs to determine
        the diopters.   Default: empty non-null instance.

        Returns
        -------
        LensPowerMathsArgs
            An instance of LensPowerMathsArgs with the used options.
        """
        return self._object.LensPowerMathsArgs

class LensPowerMathsArgs:
    def __init__(self, prism_coeff, zero_offset, cylindrical_notation,
                 unencoded_period_calibration, encoded_period_type):
        """
        Constructor for the lens power mathamtics options.

        Parameters
        ----------
        prism_coeff : float
            The multiplier to help determine prism diopters. This is therefore
            defined as 'diopters to pixels ratio'.  Default NaN.  NOTE: as part
            of a calibration process (somewhere), this value is determined then
            stored in file. E.g. this is usually done via checking how many
            pixels are displaced when a +5D prism only lens is placed.
        zero_offset : Point2d
            Applicable only for spot measurements. For powermap function (at
            the time of writing), this is always 0.  Default NaN. This property
            helps to determine prism for spot measurement.  NOTE: for the
            portable device, when spot mode is started, there must be a use
            case that user does not have any lens under test. This allows us to
            take the first frame (cropped hexagon) in which we determine where
            the zero offset is.  Subsequently when user puts a prismatic lens,
            the shift caused by it creates a delta from this zero offset thus
            allowing us to work out.
        cylindrical_notation : CylindricalNotations
            The cylindrical notation to use.
        unencoded_period_calibration : double
            This is the unencoded period value used during calibration (in
            pixels).   This is later used in LensPowerMaths.CalculateToDiopters
            to calculate the magnification required to the current value of the
            unencoded period value at measurement time so that radii pixels
            values are in the same basin as the unencoded period value used
            during calibration.  I don't understand this yet.
        encoded_period_type : int
            The cylindrical notation to use.  I don't know what this is yet or
            why it is of a different type to the previous argument.

        Returns
        -------
        None.
        """
        self._object = \
            Lensmeter.Imaging.Measurements.LensPowerMathsArgs(prism_coeff,
                                                              zero_offset,
                                                              cylindrical_notation,
                                                              unencoded_period_calibration,
                                                              encoded_period_type)

    @property
    def prism_coefficient(self):
        """
        The multiplier to help determine prism diopters. This is therefore
        defined as 'diopters to pixels ratio'.  Default NaN.  NOTE: as part of
        a calibration process (somewhere), this value is determined then stored
        in file. E.g. this is usually done via checking how many pixels are
        displaced when a +5D prism only lens is placed.

        Returns
        -------
        float
            The coefficent value.
        """
        return self._object.PrismCoefficient

    @property
    def zero_offset(self):
        """
        Applicable only for spot measurements. For powermap function (at the
        time of writing), this is always 0.  Default NaN. This property helps
        to determine prism for spot measurement.  NOTE: for the portable
        device, when spot mode is started, there must be a use case that user
        does not have any lens under test. This allows us to take the first
        frame (cropped hexagon) in which we determine where the zero offset is.
        Subsequently when user puts a prismatic lens, the shift caused by it
        creates a delta from this zero offset thus allowing us to work out.

        Returns
        -------
        Point2d
            The spot coordinate value.
        """
        return self._object.ZeroOffset

    @property
    def cylindrical_notation(self):
        """
        The cylindrical notation to use.

        Returns
        -------
        CylindricalNotations
            The notation type.
        """
        return self._object.CylindricalNotation

    @property
    def unencoded_period_calibration(self):
        """
        This is the unencoded period value used during calibration (in pixels).
        This is later used in LensPowerMaths.CalculateToDiopters to calculate
        the magnification required to the current value of the unencoded period
        value at measurement time so that radii pixels values are in the same
        basin as the unencoded period value used during calibration.

        Returns
        -------
        double
            The value of the double.
        """
        return self._object.UnencodedPeriodCalibration

    @property
    def encoded_period_type(self):
        """
        Follows LensmeterPatternsGeneratorArgs.EncodedPeriodType.

        Returns
        -------
        int
            The value of the attribute.
        """
        return self._object.EncodedPeriodType


class DualLensmeterRealWorldArgs:
    def __init__(self, screen_mm_to_pixels, camera_mm_to_pixels,
                 lens_magnification):
        """
        Construct an options class with camera and monitor data.

        Parameters
        ----------
        screen_mm_to_pixels : float
            The pixel density of the camera's sensor:
                How many mm is a pixel on the sensor. E.g. IDS UI-3884LE-M-GL
                is 2.4um/px which is 0.0024mm/px which has the dimension mm/px.
        camera_mm_to_pixels : float
            The pixel density of the camera's sensor:
                How many mm is a pixel on the sensor. E.g. IDS UI-3884LE-M-GL
                is 2.4um/px which is 0.0024mm/px which has the dimension mm/px.
        lens_magnification : float
            The magnification of the camera's lens. E.g. TC13048 has 0.098.

        Returns
        -------
        None.
        """
        self._object = \
            Lensmeter.Calibration.DualLensmeterRealWorldArgs(screen_mm_to_pixels,
                                                             camera_mm_to_pixels,
                                                             lens_magnification)

    @property
    def screen_mm_to_pixels(self):
        """
        The pixel density of the screen:
            How many mm is a pixel on the physical screen and dimension is in
            mm/px.

        Returns
        -------
        float
            The conversion value.
        """
        return self._object.ScreenMMToPixels

    @property
    def camera_mm_to_pixels(self):
        """
        The pixel density of the camera's sensor:
            How many mm is a pixel on the sensor. E.g. IDS UI-3884LE-M-GL is
            2.4um/px which is 0.0024mm/px which has the dimension mm/px.

        Returns
        -------
        float
           The conversion value.
        """
        return self._object.CameraSensorMMToPixels

    @property
    def lens_magnification(self):
        """
        The magnification of the camera's lens. E.g. TC13048 has 0.098.

        Returns
        -------
        float
            The magnification value.
        """
        return self._object.LensMagnification

    @property
    def camera_scale(self):
        """
        The camera scale is calculated from the pixel density of the sensor and
        the lens magnification. This dimension is also in mm/px.

        Returns
        -------
        float
            The magnification value.
        """
        return self._object.CameraScale
