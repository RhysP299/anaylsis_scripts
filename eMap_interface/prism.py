# -*- coding: utf-8 -*-
"""
Created on Mon Sep 13 08:44:19 2021
A file containing interfaces to prism classes.
@author: RhysPoolman
"""
import numpy as np
import clr

clr.AddReference("EyeTech.Recipes.Lensmeter")
clr.AddReference("OpenCvSharp")
from EyeTech.Recipes import Lensmeter
import OpenCvSharp as ocs


class LensPrismMeasurementResults:
    def __init__(self):
        self._object = \
            Lensmeter.Imaging.Measurements.LensPrismMeasurementResults()

    @property
    def prism_heatmap_raw_float_data(self):
        """
        The raw floating point values for Prism Diopters and Prism Base Angle
        at every pixel of PrismHeatmapImage.  This is a 2-channel Mat of type
        CV_32F Channel 1 data = Measured Prism Diopters at
        ( colIndex, rowIndex ), equivalent to diopters measurement value at
        ( x, y ) image coordinate.  Channel 2 data = Measured Prism Base Angles
        at ( colIndex, rowIndex ), equivalent to base angle measurement value
        at ( x, y ) image coordinate.

        Returns
        -------
        numpy.ndarray of floats
            An array of the raw prism diopter heatmap data.
        """
        # prep variables
        mat_dims = self._object.PrismHeatmapRawFloatData.dims
        size = None
        data = None
        if mat_dims > 3:
            msg = "LensPrismMeasurementResults.prism_heatmap_raw_float_data: "
            msg += "Mat that contains prism data has more dimensions than "
            msg += "expected."
            raise ValueError(msg)
        elif mat_dims >= 2:
            size = self._object.PrismHeatmapRawFloatData.size
            data = np.zeros(size)
        else:
            msg = "LensPrismMeasurementResults.prism_heatmap_raw_float_data: "
            msg += "Mat is null."
            raise ValueError(msg)
        # create generic Get function interface in python
        get = getattr(self._object.PrismHeatmapRawFloatData,
                      "Get<MatType.CV_32F>")
        for ii in range(size[0]):
            for jj in range(size[1]):
                data[ii, jj] = get[ii, jj]
        return data