# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 11:33:26 2021
The interface for the mask processing in the eMap software.
@author: RhysPoolman
"""
import eMap_interface.utilities as utl
import eMap_interface.results as rst
import clr

clr.AddReference("EyeTech.Recipes.Lensmeter")
from EyeTech.Recipes import Lensmeter


def dynamic_tidy_lens_region_detection_mask(dynamic_tidy_mask_lens_region_results_list_cam_1, 
                                            dynamic_tidy_mask_lens_region_results_list_cam_2,
                                            data_points_mask_lens_region_results_list_cam_1,
                                            data_points_mask_lens_region_results_list_cam_2,
                                            dots_source_images_cam_1,
                                            dots_source_images_cam_2,
                                            image_alignment_calibration_data,
                                            detected_lens_region_results_cam_1,
                                            detection_block_cam_1,
                                            detected_lens_region_results_cam_2,
                                            detection_args_cam_2,
                                            detection_block_cam_2,
                                            processing_options):
    dual = Lensmeter.Processing.Real_time.Industrial.Dual
    ret_val = \
        dual.DynamicTidyLensRegionDetectionMask(dynamic_tidy_mask_lens_region_results_list_cam_1, 
                                                dynamic_tidy_mask_lens_region_results_list_cam_2,
                                                data_points_mask_lens_region_results_list_cam_1,
                                                data_points_mask_lens_region_results_list_cam_2,
                                                dots_source_images_cam_1,
                                                dots_source_images_cam_2,
                                                image_alignment_calibration_data,
                                                detected_lens_region_results_cam_1,
                                                detection_block_cam_1,
                                                detected_lens_region_results_cam_2,
                                                detection_args_cam_2,
                                                detection_block_cam_2,
                                                processing_options)
    return ret_val
