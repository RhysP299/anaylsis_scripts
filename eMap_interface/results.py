# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 14:02:37 2021
A module that contains interfaces to results object from C#.
@author: RhysPoolman
"""
import clr
from enum import Enum
import eMap_interface.utilities as utl

clr.AddReference("EyeTech.Recipes")
clr.AddReference("EyeTech.Recipes.Lensmeter")
from EyeTech import Recipes
from EyeTech.Recipes import Lensmeter


class Result(Enum):
    # Default fail state.
    big_fail = \
        Lensmeter.Imaging.Measurements.LensPrismMeasurement.Result.BigFail,
    # Image processing failed.
    not_found = \
        Lensmeter.Imaging.Measurements.LensPrismMeasurement.Result.NotFound,
    # Success, obtained measurements.
    success =  \
        Lensmeter.Imaging.Measurements.LensPrismMeasurement.Result.Success


def _get_result_enum(result):
    """
    Converts a C# Lensmeter.Imaging.Measurements.LensPrismMeasurement.Result
    value to a python Result value.  If the input does not have an equivalent
    value in the python Result enum then a ValueError exception is raised.

    Parameters
    ----------
    result : Lensmeter.Imaging.Measurements.LensPrismMeasurement.Result
        The C# input value.

    Returns
    -------
    ret_val : Result
        The python output value.
    """
    ret_val = None
    if result == Result.big_fail:
        ret_val = Result.big_fail
    elif result == Result.not_found:
        ret_val = Result.not_found
    elif result == Result.success:
        ret_val = Result.success
    else:
        msg = "results._get_result_enum:  The function argument does not " + \
            "have a result.Results equivalent."
        raise ValueError(msg)
    return ret_val


class LensPrismMeasurementResults:
    def __init__(self):
        """
        Constructor that creates an empty empty instance  of the C# class
        LensPrismMeasurementResults.  This will be filled as the prism
        measurement proceedures.

        Returns
        -------
        None.
        """
        self._object = \
            Lensmeter.Imaging.Measurements.LensPrismMeasurementResults()

    @property
    def result(self):
        """
        The result of the lens prism measurement - success / failure.

        Returns
        -------
        Result
            The results error code.
        """
        return _get_result_enum(self._object.Result)

    @property
    def horizontal_lines_pixel_shift_debug_image(self):
        """
        This image visualises the horizontal pixel shift magnitude in pixels at
        every edge point and should only be used for debugging.  This is a
        grayscale image, pixel values are 0-255 Pixels closer to 255 in this
        debug image represent a larger pixel shift magnitude.

        Returns
        -------
        2d numpy.ndarray of floats
            Horizontal pixel shift results
        """
        data = self._object.HorizontalLinesPixelShiftDebugImage
        return utl.mat_2_ndarray(data)

    @property
    def vertical_lines_pixel_shift_debug_image(self):
        """
        This image visualises the vertical pixel shift magnitude in pixels at
        every edge point and should only be used for debugging.  This is a
        grayscale image, pixel values are 0-255 Pixels closer to 255 in this
        debug image represent a larger pixel shift magnitude.

        Returns
        -------
        2d numpy.ndarray of floats
            Vertical pixel shift results
        """
        data = self._object.VerticalLinesPixelShiftDebugImage
        return utl.mat_2_ndarray(data)

    @property
    def can_calculate_lens_focal_length(self):
        """
        Flag for whether we were able to calculate a focal length for the lens.
        It is not possible to calculate a meaningful focal length for a lens
        with a spherical power close to zero diopters.  If the focal length of
        the lens can be calculated, it means that there is greater "confidence"
        in the prism diopters measurement at PointOfLeastPrismChange.  This
        flag indicates whether the ( x, y ) location of PointOfLeastPrismChange
        can be used as an alternative reference point from which to measure the
        prescription for a single vision lens.

        Returns
        -------
        boolean
            The value of the flag.
        """
        return self._object.CanCalculateLensFocalLength

    @property
    def prism_heatmap_image(self):
        """
        Prism heatmap image.  Prism measurements are carried out on unscaled
        source images.  This heatmap image is the full unscaled camera captured
        image size.

        Returns
        -------
        2d numpy.ndarray of floats
            The heatmap values.
        """
        data = utl.mat_2_ndarray(self._object.PrismHeatmapImage)
        return data

    @property
    def prism_heatmap_image_float_data(self):
        """
        The raw floating point values for Prism Diopters and Prism Base Angle
        at every pixel of PrismHeatmapImage.  This is a 2-channel Mat of type
        CV_32F Channel 1 data = Measured Prism Diopters at
        ( colIndex, rowIndex ), equivalent to diopters measurement value at
        ( x, y ) image coordinate.  Channel 2 data = Measured Prism Base Angles
        at ( colIndex, rowIndex ), equivalent to base angle measurement value
        at ( x, y ) image coordinate.

        Returns
        -------
        2d numpy.ndarray of floats
            The heatmap values.
        """
        data = utl.mat_2_ndarray(self._object.PrismHeatmapImage)
        return data

    @property
    def prism_diopters_values(self):
        """
        Raw 1-dimensional array of prism diopters measured values.

        Returns
        -------
        1D numpy.ndarray of floats
            The raw prism values.
        """
        data = utl.array1d_2_ndarray_floats(self._object.PrismDioptersValues)
        return data

    @property
    def prism_base_angles_values(self):
        """
        Raw 1-dimensional array of prism base angles measured values.

        Returns
        -------
        data : 1d numpy.ndarray of flaots
            The base angle values.
        """
        data = utl.array1d_2_ndarray_floats(self._object.PrismBaseAnglesValues)
        return data

    @property
    def prism_heatmap_legend(self):
        """
        The legend image for PrismHeatmapImage.

        Returns
        -------
        data : numpy.ndarray of integers
            The values of the lengend.
        """
        data = \
            utl.image_array_2_ndarray_uint32(self._object.PrismHeatmapLegend)
        return data

    @property
    def prism_heatmap_legend_range(self):
        """
        The range [min, max] values for PrismHeatmapLegend.

        Returns
        -------
        float
            The min value.
        float
            The max value.
        """
        return (self._object.PrismHeatmapLegendRange.Start,
                self._object.PrismHeatmapLegendRange.End)

    @property
    def prism_heatmap_bounding_rect(self):
        """
        The bounding rectangle of the area of the lens that was measured.  Only
        part of the lens is measured for prism.

        Returns
        -------
        data : Rect
            A dictionary with the values describing the rectangle.
        """
        data = utl.Rect(self._object.PrismHeatmapBoundingRect)
        return data

    @property
    def prism_reference_point(self):
        """
        The Prism Reference Point is the point on the lens at which the prism
        prescription should be measured.

        Returns
        -------
        point : Point2f
            The PRP value.
        """
        point = utl.Point2f(self._objec.PrismReferencePoint.X,
                            self._object.PrismReferencePoint.Y)
        return point

    @property
    def scaled_prism_reference_point(self):
        """
        The point PrismReferencePoint after applying the image scaler reduction
        factor used for the measurement.  Used for visualisation on heatmaps
        which have been scaled by the image scaler reduction factor.

        Returns
        -------
        point : Point2f
            The scale PRP position.
        """
        point = utl.Point2f(self._object.ScaledPrismReferencePoint.X,
                            self._object.ScaledPrismReferencePoint.Y)
        return point

    @property
    def point_of_least_prism_change(self):
        """
        The point at which there is least change in the lens prism diopters
        values.

        Returns
        -------
        point : Point2f
            The point coordiante values.
        """
        point = utl.Point2f(self._object.PointOfLeastPrismChange.X,
                            self._object.PointOfLeastPrismChange.Y)
        return point

    @property
    def scaled_point_of_least_prism_change(self):
        """
        The point PointOfLeastPrismChange after applying the image scaler
        reduction factor.  Used for visualisation on heatmaps which have been
        scaled by the image scaler reduction factor.

        Returns
        -------
        point : Point2f
            The point coordiante values.
        """
        point = utl.Point2f(self._object.ScaledPointOfLeastPrismChange.X,
                            self._object.ScaledPointOfLeastPrismChange.Y)
        return point

    @property
    def prism_reference_point_offset_from_distance_reference_point_mm(self):
        """
        The Prism Reference Point is offset from the lens Distance Reference
        Point by a manufacturer-defined number of millimeters.  This value was
        given to LensPrismMeasurementArgs and copied through to results to
        preserve the state of the prism measurement.

        Returns
        -------
        float
            The value in mm.
        """
        return \
            self._object.PrismReferencePointOffsetFromDistanceReferencePointMM

    @property
    def near_reference_point_inset_mm(self):
        """
        By definition, the Near Reference Point is not on the same axis as the
        Distance Reference Point.  It is "inset" ( horizontally offset ) from
        the Distance Reference Point by a manufacturer-defined number of
        millimeters.  This value was given to LensPrismMeasurementArgs and
        copied through to results to preserve the state of the prism
        measurement.

        Returns
        -------
        float
            The value in mm.
        """
        return self._object.NearReferencePointInsetMM

    @property
    def distance_between_measuring_lens_and_screen_mm(self):
        """
        The distance in millimeters between the lens we are measuring and the
        physical device's screen.  Required to convert prism pixel shift
        magnitudes into prism diopters.  In the technical document this is
        called "Object Distance".  This value was calculated by
        LensPrismMeasurementArgs and copied through to results to preserve the
        state of the prism measurement.

        Returns
        -------
        float
            The value in mm.
        """
        return self._object.DistanceBetweenMeasuringLensAndScreenMM


class DualLensmeterImageAlignmentResults:
    def __init__(self):
        """
        Constructor that creates an empty instance of
        DualLensmeterImageAlignmentResults whichwill be filled as the prism
        measurement proceedures.

        Returns
        -------
        None.
        """
        self._object = \
            Lensmeter.Imaging.Detection.DualLensmeterImageAlignmentResults()

    @property
    def aligned_image(self):
        """
        Cam1 and Cam2 images aligned according to calibration data and placed
        side by side as one image.

        Returns
        -------
        numpy.ndarray
            The aligned images
        """
        return utl.mat_2_ndarray(self._object.AlignedImage)

    @property
    def cam_1_image(self):
        """
        Cam1 image from the result of the image alignment.  NOTE: This is a
        reference to the Cam1 side of AlignedImage.  NOTE: Any modification of
        this Mat will also modify AlignedImage.

        Returns
        -------
        numpy.ndarray
            The image data.
        """
        return utl.mat_2_ndarray(self._object.Cam1Image)

    @property
    def cam_2_image(self):
        """
        Cam2 image from the result of the image alignment.  NOTE: This is a
        reference to the Cam2 side of AlignedImage.  NOTE: Any modification of
        this Mat will also modify AlignedImage.

        Returns
        -------
        numpy.ndarray
            The image data.
        """
        return utl.mat_2_ndarray(self._object.Cam2Image)

    @property
    def left_image(self):
        """
         Getter for left side image according to LeftImageCameraLocation.
         NOTE: This is returning a reference to the left-hand side of
         AlignedImage.  NOTE: Any modification of this Mat will also modify
         AlignedImage.

        Returns
        -------
        numpy.ndarray
            The left image data.
        """
        return utl.mat_2_ndarray(self._object.LeftImage)

    @property
    def right_image(self):
        """
         Getter for right side image according to RightImageCameraLocation.
         NOTE: This is returning a reference to the right-hand side of
         AlignedImage.  NOTE: Any modification of this Mat will also modify
         AlignedImage.

        Returns
        -------
        numpy.ndarray
            The left image data.
        """
        return utl.mat_2_ndarray(self._object.RightImage)


class LensRegionDetectionResults:
    def __init__(self):
        """
        Constructor that creates an empty instance of
        DualLensmeterImageAlignmentResults whichwill be filled as the prism
        measurement proceedures.

        Returns
        -------
        None.
        """
        self._object = \
            Lensmeter.Imaging.Detection.LensRegionDetectionResults()

    @property
    def result(self):
        """
        The result of the lens region detection - success / failure.

        Returns
        -------
        Result
            The enum value.
        """
        return _get_result_enum(self._object.Result)

    @property
    def detected_lens_region(self):
        """
        The detected lens region image is the output image after the
        DetectedLensRegionMask has been applied to the source image.

        Returns
        -------
        numpy.ndarray
            The detected lens region data.
        """
        return utl.mat_2_ndarray(self._object.DetectedLensRegion)

    @property
    def detected_lens_region_mask(self):
        """
        The detected lens region mask is a binary mask image containing only
        the detected lens region area of interest.

        Returns
        -------
        numpy.ndarray
            The detected lens region mask data.
        """
        return utl.mat_2_ndarray(self._object.DetectedLensRegionMask)

    @property
    def edge_detection_result(self):
        """
        The result of the edge detection on the input image, saved for
        debugging purposes.

        Returns
        -------
        numpy.ndarray
            The edge detection data.
        """
        return utl.mat_2_ndarray(self._object.EdgeDetectionResult)


class DualCameraAlignmentCalibrationResults:
    def __init__(self):
        self._object = \
            Lensmeter.Calibration.Dual.DualCameraAlignmentCalibrationResults()

    @property
    def result_value(self):
        """
        Did the calibration succeed and does this instance contain valid data?

        Returns
        -------
        Result
            The success or failure of the result.
        """
        return _get_result_enum(utl.self._object.ResultValue)

    @property
    def cam_1_image_transformation_matrix(self):
        """
        The transformation matrix to apply to the image from Camera 1.  If
        identity matrix, then no transformation.

        Returns
        -------
        numpy.ndarray
            The iamge data.
        """
        return utl.mat_2_ndarray(self._object.Cam1ImageTransformationMatrix)

    @property
    def cam_2_image_transformation_matrix(self):
        """
        The transformation matrix to apply to the image from Camera 2.  If
        identity matrix, then no transformation.

        Returns
        -------
        numpy.ndarray
            The image data
        """
        return utl.mat_2_ndarray(self._object.Cam2ImageTransformationMatrix)

    @property
    def cam_1_image_rotated_size(self):
        """
        The new size of the image from Camera 1 after rotation, for
        Cv2.WarpAffine.

        Returns
        -------
        tuple of ints
            The size as a tuple (width, height).
        """
        return (self._object.Cam1ImageRotatedSize.Width,
                self._object.Cam1ImageRotatedSize.Height)

    @property
    def cam_2_image_rotated_size(self):
        """
        The new size of the image from Camera 2 after rotation, for
        Cv2.WarpAffine.

        Returns
        -------
        tuple of ints
            The size as a tuple (width, height).
        """
        return (self._object.Cam12mageRotatedSize.Width,
                self._object.Cam12mageRotatedSize.Height)

    @property
    def cam_1_delta_x(self):
        """
        The delta X in pixels for Cam1 image.

        Returns
        -------
        int
            The value in pixels.
        """
        return self._object.Cam1DeltaX

    @property
    def cam_2_delta_x(self):
        """
        The delta X in pixels for Cam2 image.

        Returns
        -------
        int
            The value in pixels.
        """
        return self._object.Cam2DeltaX

    @property
    def delta_y(self):
        """
        The delta Y in pixels for Cam1 image.

        Returns
        -------
        int
            The value in pixels.
        """
        return self._object.DeltaY

    @property
    def real_world_args(self):
        """
        The real-world args applied during
        DualCameraAlignmentCalibration.Calibrate. The args is stored as part of
        the results due to the calibration process.

        Returns
        -------
        DualLensmeterRealWorldArgs
            The value of the telecetnric lens, monitor and camera options.
        """
        return self._object.RealWorldArgs

    @property
    def clipping_margins(self):
        """
        The clipping margins (on source image just before rotate and stitch)
        applied during DualCameraAlignmentCalibration.Calibrate. The args is
        stored as part of the results due to the calibration process.

        Returns
        -------
        DualCameraAlignmentImageMargins
            The value of the applied margins.
        """
        return self._object.ClippingMargins

    @property
    def left_image_camera_location(self):
        """
        Defines whether Cam1 or Cam2 captures the left source image.

        Returns
        -------
        CameraLocations
            Which camera is used. 
        """
        return self._object.LeftImageCameraLocation

    @property
    def calibration_time_stamp(self):
        """
        A UTC timestamp to indicate the time of calibration and it uses
        ISO_8601_DATE_TIME_ZULU_FORMAT. Default DateTime.MinValue.

        Returns
        -------
        DateTime
            The time of calibration.
        """
        return self._object.CalibrationTimeStamp

    @property
    def cam_1_image_thresholded(self):
        """
        The image from Camera 1 after it has been thresholded, for debugging.
        NOTE: at the moment this is not saved to disk.

        Returns
        -------
        numpy.ndarray
            The image data
        """
        return utl.mat_2_ndarray(self._object.Cam1ImageThresholded)

    @property
    def cam_2_image_thresholded(self):
        """
        The image from Camera 2 after it has been thresholded, for debugging.
        NOTE: at the moment this is not saved to disk.

        Returns
        -------
        numpy.ndarray
            The image data
        """
        return utl.mat_2_ndarray(self._object.Cam2ImageThresholded)

    @property
    def cam_1Image_cropped(self):
        """
        The image from Camera 1 after it has been cropped, for debugging.
        NOTE: at the moment this is not saved to disk.

        Returns
        -------
        numpy.ndarray
            The image data.
        """
        return utl.mat_2_ndarray(self._object.Cam1ImageCropped)

    @property
    def cam_2Image_cropped(self):
        """
        The image from Camera 2 after it has been cropped, for debugging.
        NOTE: at the moment this is not saved to disk.

        Returns
        -------
        numpy.ndarray
            The image data.
        """
        return utl.mat_2_ndarray(self._object.Cam2ImageCropped)


def detect_lens_region(image_alignment_calibration_data, image_source_1,
                       image_source_2, lens_region_detection_args_1,
                       lens_region_detection_args_2):
    """
    Performs the lens region detection.  Method rotates and stitches both
    images prior to lens region detection (if any failure then method does not
    proceed to lens region detection).  Return either success or BigFail to the
    caller if any step fails.

    Parameters
    ----------
    image_alignment_calibration_data : DualCameraAlignmentCalibrationResults
        The config (results) structure containing the calibration data for how
        to align two independent images (from each camera) so that they can be
        rotated and stitched at the latter part of the algorithm..
    image_source_1 : numpy.ndarray
        DESCRIPTION.
    image_source_2 : numpy.ndarray
        DESCRIPTION.
    lens_region_detection_args_1 : LensRegionDetectionArgs
        The arguments for the lens region detection for camera 1.
    lens_region_detection_args_2 : LensRegionDetectionArgs
        The arguments for the lens region detection for camera 2.

    Returns
    -------
    None.

    """
    # create the results objects
    detection_results_cam_1 = None
    detection_results_cam_2 = None
    image_alignment_results = None

    # calculate the results
    Lensmeter.DetectLensRegion(detection_results_cam_1,
                               detection_results_cam_2,
                               image_alignment_results,
                               image_alignment_calibration_data,
                               image_source_1, image_source_2,
                               lens_region_detection_args_1,
                               lens_region_detection_args_2)
    
    return detection_results_cam_1, detection_results_cam_2,
        image_alignment_results
    
